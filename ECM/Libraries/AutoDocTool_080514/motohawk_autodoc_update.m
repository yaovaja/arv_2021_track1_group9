%% --------------------------------
%% function MOTOHAWK_AUTODOC_UPDATE
%% --------------------------------
%%
function motohawk_autodoc_update
% --- Adds and/or updates AutoDoc blocks on single model page

global model islibrary Layout pgwd lngthscrn lngthlines lngthnamtxt lngthunttxt lngthhlptxt lngthgrptxt

% ----------------------------
% --- Display Format Variables
% ----------------------------

% page dimensions (for Portrait and Landscape layout)
pgwd = [1060; 1450];
pght = [1450; 1060];

% left border positions (for Portrait and Landscape layout)
brdrpos{1,1} = [0 0 0 1435];
brdrpos{1,2} = [0 0 0 1045];

% right border positions (for Portrait and Landscape layout)
brdrpos{2,1} = [1075 0 1075 1435];
brdrpos{2,2} = [1465 0 1465 1045];

% title block format variables
lngthscrn = [130; 180];
lngthscrnlf = [52; 72];

% calibration and probe display format variables
lngthlines = [518; 714];
lngthnamtxt = 27;
lngthunttxt = 12;
lngthhlptxt = [92; 140];
lngthgrptxt = [77; 125];

model = bdroot;
syspth = gcs;
blk = gcb;
islibrary = strcmp(get_param(model, 'LibraryType'), 'BlockLibrary');

% ---------------------
% --- Retrieve UserData
% ---------------------

UserData = get_param(blk, 'UserData');
Layout = UserData.Layout;
CalPrbDisp = UserData.CalPrbDisp;
ObjCheck = UserData.ObjCheck;
ObjText = UserData.ObjText;
DescCheck = UserData.DescCheck;
DescText = UserData.DescText;
EqnCheck = UserData.EqnCheck;
EqnText = UserData.EqnText;
clear UserData

% -------------------------
% --- Delete AutoDoc Blocks
% -------------------------

docblklst = {'autodoc_titleblk'; 'autodoc_leftborder'; 'autodoc_rightborder'; 'autodoc_calsprbsdisp'; 'autodoc_calsprbspage'};
if ~ strcmp(model, 'AutoDoc_lib')
    for i = 1:length(docblklst)
        blklst = find_system(syspth, 'SearchDepth', 1, 'LookUnderMasks', 'all', 'ReferenceBlock', ['AutoDoc_lib/' docblklst{i}]);
        for j = 1:length(blklst)
            delete_block(blklst{j});
        end
    end
end

% ----------------------
% --- Add AutoDoc Blocks
% ----------------------

docblkreq = [1 1 1 (CalPrbDisp == 1) (CalPrbDisp == 2)] & ~ strcmp(model, 'AutoDoc_lib');
load_system('AutoDoc_lib');
for i = 1:length(docblklst)
    if docblkreq(i)
        try
            currblk = [syspth '/' docblklst{i}];
            add_block(['AutoDoc_lib/' docblklst{i}], currblk);
        catch
            disp('Error: Non-AutoDoc blocks cannot share the same name as AutoDoc blocks.');
            disp(['Please rename ' syspth '/' docblklst{i} ' to a unique name.']);
            return
        end
        
% ----------------------------------
% --- Set UserData To AutoDoc Blocks
% ----------------------------------
        
        switch i
            
            % title block
            case 1
                % docpath
                [LowLevelPos DocPath] = docpath(syspth);
                UserData = get_param(blk, 'UserData');
                UserData.LowLevelPos = str2num(LowLevelPos);
                UserData.DocPath = DocPath;
                motohawk_set_param(blk, 'UserData', UserData);
                clear UserData
                % objective, description, and equation text
                FirstLbl = ''; FirstTxt = ''; SecondLbl = ''; SecondTxt = '';
                objnl = 0; descnl = 0; lblnl = 0;
                if ObjCheck
                    FirstLbl = 'Objective';
                    FirstTxt = txtspacing(ObjText);
                    objnl = 1 + length(strfind(char(FirstTxt), '\newline'));
                    lblnl = 1;
                end
                if DescCheck || EqnCheck
                    if DescCheck && EqnCheck
                        DescEqnLbl = 'Description & Equations';
                        DescEqnTxt = [txtspacing(DescText) '\newline\newline' EqnText];
                    elseif DescCheck && ~ EqnCheck
                        DescEqnLbl = 'Description';
                        DescEqnTxt = txtspacing(DescText);
                    elseif ~ DescCheck && EqnCheck
                        DescEqnLbl = 'Equations';
                        DescEqnTxt = EqnText;
                    end
                    descnl = 1 + length(strfind(char(DescEqnTxt), '\newline'));
                    lblnl = lblnl + 1;
                    if ~ ObjCheck
                        FirstLbl = DescEqnLbl;
                        FirstTxt = DescEqnTxt;
                    elseif ObjCheck
                        SecondLbl = DescEqnLbl;
                        SecondTxt = DescEqnTxt;
                    end
                end
                Ht = 68 + 42*lblnl + 19*(objnl + descnl);
                Lines = '.';
                for j = 1:lngthlines(Layout)
                    Lines = strcat(Lines, '.');
                end
                % doc path display
                sysnam = oneline(get_param(syspth, 'Name'));
                if strcmp(DocPath, '') || strncmp(DocPath, '00', 2)
                    DocPathDisp = sysnam;
                else
                    DocPathDisp = strrep(DocPath, '.0', '.');
                    if strcmp(DocPathDisp(1), '0')
                        DocPathDisp = DocPathDisp(2:length(DocPathDisp));
                    end
                    DocPathDisp = [DocPathDisp ' ' sysnam];
                end
                DocPathDisp = textend(DocPathDisp, lngthscrnlf(Layout), 0);
                DocPathDisp = strrep(DocPathDisp, '_', '\_');
                % system
                Sys = oneline(syspth);
                Sys = strrep(Sys, '/', ' / ');
                Sys = textend(Sys, lngthscrn(Layout), 1);
                Sys = strrep(Sys, '_', '\_');
                % UserData
                UserData.FirstLbl = FirstLbl;
                UserData.FirstTxt = FirstTxt;
                UserData.SecondLbl = SecondLbl;
                UserData.SecondTxt = SecondTxt;
                UserData.DispLines = Lines;
                UserData.Wd = pgwd(Layout);
                UserData.Ht = Ht;
                UserData.DocPathDisp = DocPathDisp;
                UserData.Sys = Sys;
                motohawk_set_param(currblk, 'Position', [0 0 UserData.Wd UserData.Ht]);

            % left and right borders
            case {2,3}
                pos = brdrpos{i-1, Layout};
                UserData.Wd = pos(3) - pos(1);
                UserData.Ht = pos(4) - pos(2);
                motohawk_set_param(currblk, 'Position', pos);
                
            % calibrations and probes display    
            case 4
                [caldata prbdata] = calprbdata(syspth, 1, 'all', 'off');
                UserData = feval('calprbdisp', caldata, prbdata);
                motohawk_set_param(currblk, 'Position', [0 (pght(Layout)-UserData.Ht) UserData.Wd pght(Layout)]);
            
            % calibrations and probes page
            case 5
                [caldata prbdata] = calprbdata(syspth, 1, 'all', 'off');
                prevlayout = Layout;
                Layout = 1;
                UserData = feval('calprbdisp', caldata, prbdata);
                Layout = prevlayout;
                UserData.Wd = pgwd(Layout);
                UserData.Ht = 20;
                motohawk_set_param(currblk, 'Position', [0 (pght(Layout) - 20) pgwd(Layout) pght(Layout)]);
                
        end
        motohawk_set_param(currblk, 'UserDataPersistent', 'on');
        motohawk_set_param(currblk, 'UserData', UserData);
        set_param(currblk, 'MaskInitialization', 'UserData = get_param(gcb, ''UserData'');');
        clear UserData
    end
end

end
       

%% --------------------
%% function DOCPATH
%% --------------------
%%
function [LowLevelPos DocPath] = docpath(syspth)
% --- Determines LowLevelPos and DocPath variables

global model islibrary

% -------------
% --- If bdroot
% -------------

if strcmp(model, syspth)
    LowLevelPos = '00';
    DocPath = '00';

else

% ------------------------------
% --- Else, List Paths to bdroot
% ------------------------------

    pthlst{1} = syspth;
    parpth = get_param(syspth, 'Parent');
    count = 1;
    while ~ strcmp(parpth, '')
        count = count + 1;
        pthlst{count} = parpth;
        parpth = get_param(parpth, 'Parent');
    end
    pthlst = fliplr(pthlst);
    % remove bdroot from pthlst
    pthlst(1) = [];

% -------------------
% --- Compile DocPath
% -------------------

    DocPath = '00';

    for i = 1:length(pthlst)
        [pos lensyslst] = lowlevelpos(pthlst{i});
        % reset DocPath
        if strcmp(pos, '')
            DocPath = '00';
        end
        if ~ islibrary && strncmp(fliplr(DocPath), '00', 2) && lensyslst == 1
            LowLevelPos = '00';
            DocPath = [DocPath '.00'];
        else
            if i == length(pthlst)
                LowLevelPos = pos;
            end
            DocPath = strrep([DocPath '.' pos], '00.', '');
        end
    end

end

end


%% --------------------
%% function LOWLEVELPOS
%% --------------------
%%
function [LowLevelPos lensyslst] = lowlevelpos(syspth)
% --- Finds low-level position of subsystem with respect to other subsystems sharing the same parent
% --- according to aphabetical order, horizontal & vertical position, and/or explicit position
% --- (Note: If no other subsystems, LowLevelPos = 1.  If no subsystems, LowLevelPos = '')

global islibrary model

% --------------------------------------------------
% --- List of Children Subsystems with AutoDoc Block
% --------------------------------------------------

if strcmp(model, syspth)
    autodocblklst = find_system(syspth, 'SearchDepth', 1, 'LookUnderMasks', 'all', 'ReferenceBlock', 'AutoDoc_lib/motohawk_autodoc');
else
    autodocblklst = find_system(get_param(syspth, 'Parent'), 'SearchDepth', 2, 'LookUnderMasks', 'all', 'ReferenceBlock', 'AutoDoc_lib/motohawk_autodoc');
    % remove parent autodoc block path from list
    parautodocblklst = find_system(get_param(syspth, 'Parent'), 'SearchDepth', 1, 'LookUnderMasks', 'all', 'ReferenceBlock', 'AutoDoc_lib/motohawk_autodoc');
    if ~ isempty(parautodocblklst)
        idx = find(strcmp(autodocblklst, parautodocblklst{1}));
        autodocblklst(idx) = [];
    end
end
% only include if DocOption == "Include in documentation"
count = 0;
autodocblklst_mod = {};
for i = 1:length(autodocblklst)
    UserData = get_param(autodocblklst{i}, 'UserData');
    if UserData.DocOption == 1
        count = count + 1;
        autodocblklst_mod{count} = autodocblklst{i};
    end
    clear UserData
end
autodocblklst = autodocblklst_mod;
syslst = get_param(autodocblklst, 'Parent');
lensyslst = length(syslst);

if lensyslst == 0 || isempty(find(strcmp(syslst, syspth)))
    LowLevelPos = '';
    
elseif lensyslst == 1
    LowLevelPos = '01';
    
else

% ----------------------------------------------------------
% --- If Library (and Top-Level Blocks), Sort Alphabetically
% ----------------------------------------------------------

    if islibrary && strcmp(get_param(syspth, 'Parent'), model)
        [syslst idx] = sort(syslst);
        autodocblklst = autodocblklst(idx);

% ------------------------------------------------------------
% --- Else, Sort According to Horizontal Position of Left Side
% ------------------------------------------------------------

    else
        hblkpos = [];
        vblkpos = [];
        for i = 1:length(syslst)
            blkpos = get_param(syslst{i}, 'Position');
            hblkpos(i) = blkpos(1);
            vblkpos(i) = blkpos(2);
        end
        [hblkpos idx] = sort(hblkpos);
        syslst = syslst(idx);
        autodocblklst = autodocblklst(idx);
        vblkpos = vblkpos(idx);

% ------------------------------------------------------------------------------------
% --- If Any Same Horizontal Position, Sort According to Vertical Position of Top Side
% ------------------------------------------------------------------------------------     

        idx_mod = [1:length(syslst)];
        for i = 1:length(syslst)
            matchidx = find(hblkpos(i) == hblkpos);
            if length(matchidx) > 1
                [vblkposgrp vgrpidx] = sort(vblkpos(min(matchidx):max(matchidx)));
                idx_mod(min(matchidx):max(matchidx)) = min(matchidx) - 1 + vgrpidx;
            end
        end
        idx = idx_mod;
        syslst = syslst(idx);
        autodocblklst = autodocblklst(idx);
    end

% ---------------------------------------------------
% --- If Automatic Numbering Turned Off, Adjust Order
% ---------------------------------------------------

    idx_mod = [1:length(syslst)];
    for i = 1:length(syslst)
        UserData = get_param(autodocblklst{i}, 'UserData');
        if UserData.NumCheck == 0
            pos = min([max([1 UserData.LowLevelPos]) length(syslst)]);
            idx_mod(find(idx_mod == i)) = [];
            idx_mod = [idx_mod(1:(pos - 1)) i idx_mod(pos:length(idx_mod))];
        end
        clear UserData
    end
    idx = idx_mod;
    syslst = syslst(idx);

    LowLevelPos = find(strcmp(syslst, syspth));
    if LowLevelPos < 10
        LowLevelPos = ['0' num2str(LowLevelPos)];
    else
        LowLevelPos = num2str(LowLevelPos);
    end

end

end


%% -------------------
%% function TXTSPACING
%% -------------------
%%
function spcdtxt = txtspacing(txt)
% --- Spaces text and adds \newlines

global lngthscrn Layout

% -------------------------------
% --- Eliminate \newline Commands
% -------------------------------

txt = strrep(txt, '\newline', '');

% ---------------------------------------------------
% --- Put {} Around Single Words for LaTex Font Style
% ---------------------------------------------------

ltxcmds = {'{\it', '{\bf'};
for i = 1:length(ltxcmds)
    beginidx = strfind(txt, char(ltxcmds(i)));
    endidx = strfind(txt, '}');
    if ~ isempty(beginidx)
        txtnew = txt(1:(beginidx(1)-1));
        for j = 1:length(beginidx)
            endidxidx = min(find(strfind(txt, '}') > beginidx(j)));
            txtnew = [ txtnew ...
                strrep(txt(beginidx(j):endidx(endidxidx)), ' ', ['} ' char(ltxcmds(i))]) ];
            if j < length(beginidx)
                txtnew = [txtnew txt((endidx(endidxidx)+1):(beginidx(j+1)-1))];
            else
                txt = [txtnew txt((endidx(endidxidx)+1):length(txt))];
            end
        end
    end
end

% -------------------
% --- Find All Blanks
% -------------------

blnkidx_all = strfind(txt, ' ');

% -------------------------------------------------------------
% --- Eliminate Double Blank at End of Sentance From Blank List
% -------------------------------------------------------------

blnkidx = [];
count = 0;
for i = 1:(length(blnkidx_all) - 1)
    if blnkidx_all(i) ~= (blnkidx_all(i+1) - 1)
        count = count + 1;
        blnkidx(count) = blnkidx_all(i);
    end
end
blnkidx = [blnkidx blnkidx_all(i+1)];

% --------------------------------------
% --- Eliminate LaTex Command Characters
% --------------------------------------

txtmod = strrep(txt, '{\it', '');
txtmod = strrep(txtmod, '{\bf', '');
txtmod = strrep(txtmod, '}', '');

% -----------------------------------
% --- Find All Blanks in Modified Text
% ------------------------------------

blnkmodidx_all = strfind(txtmod, ' ');

% -------------------------------------------------------------
% --- Eliminate Double Blank at End of Sentance From Blank List
% -------------------------------------------------------------

blnkmodidx = [];
count = 0;
for i = 1:(length(blnkmodidx_all) - 1)
    if blnkmodidx_all(i) ~= (blnkmodidx_all(i+1) - 1)
        count = count + 1;
        blnkmodidx(count) = blnkmodidx_all(i);
    end
end
blnkmodidx = [blnkmodidx blnkmodidx_all(i+1)];

% -------------------------------------------
% --- Find \newline Indicies in Modified Text
% -------------------------------------------

if length(txtmod) <= lngthscrn(Layout)
    stop = 1;
else
    stop = 0;
end
newlineidx = [];
count = 0;
while stop == 0
    count = count + 1;
    if count == 1
        newlineidx(count) = blnkmodidx(max(find(blnkmodidx < lngthscrn(Layout))));
    else
        newlineidx(count) = blnkmodidx(max(find(blnkmodidx < (lngthscrn(Layout) + newlineidx(count - 1)))));
    end
    if max(newlineidx) + lngthscrn(Layout) > length(txtmod)
        stop = 1;
    end
end

% ----------------------------------
% --- Find \newline Indicies in Text
% ----------------------------------

for i = 1:length(newlineidx)
   newlineidx(i) = blnkidx(find(blnkmodidx == newlineidx(i))); 
end

% ----------------------
% --- Create Text String
% ----------------------

for i = 1:length(newlineidx)
    txt = [txt(1:newlineidx(i) + 8*(i-1)) '\newline' txt(newlineidx(i) + 1 + 8*(i-1):length(txt))];
end
spcdtxt = txt;

end


%% -------------------
%% function CALPRBDISP
%% -------------------
%%
function dispdata = calprbdisp(caldata, prbdata)
% --- Formats calibration & display data

global Layout pgwd lngthlines lngthnamtxt lngthunttxt lngthhlptxt lngthgrptxt

% -------------------
% --- Line Formatting
% -------------------

lines = '.';
for i = 1:lngthlines(Layout)
    lines = [lines, '.'];
end
displine = ['\fontsize{8}', lines, '\fontsize{14}\newline'];
blankline = ['\fontsize{8}', '', '\fontsize{14}\newline'];

% -----------------------------
% --- Calibrations Display Text
% -----------------------------

try
    numcals = length(caldata.nam);
catch
    numcals = 0;
end
try
    numprbs = length(prbdata.nam);
catch
    numprbs = 0;
end
if numcals ~= 0
    calstitle = '\fontsize{20}\fontname{Bitstream Vera Sans Mono}\bf\itCalibrations';
    dispnam = [calstitle, '\fontsize{12}\rm\newline\fontname{Arial}', displine];
    dispunt = ['\fontsize{20}\fontname{Bitstream Vera Sans Mono}\bf \fontsize{12}\rm\newline', blankline];
    disphlp = ['\fontsize{20}\fontname{Bitstream Vera Sans Mono}\bf \fontsize{12}\rm\newline', blankline];
    dispgrp = ['\fontsize{20}\fontname{Bitstream Vera Sans Mono}\bf \fontsize{12}\rm\newline', blankline];
    for i = 1:numcals
        nam = caldata.nam{i};
        if i == numcals && numprbs == 0
            dispnam = [dispnam, '\fontname{Bitstream Vera Sans Mono}', textend(nam, lngthnamtxt, 0), '\newline\newline', blankline];
        else
            dispnam = [dispnam, '\fontname{Bitstream Vera Sans Mono}\fontsize{14}', textend(nam, lngthnamtxt, 0), '\newline\newline\fontname{Arial}', displine];
        end
        dispunt = [dispunt, textend(caldata.units{i}, lngthunttxt, 0), '\newline\newline', blankline];
        disphlp = [disphlp, textend(caldata.help{i}, lngthhlptxt(Layout), 0), '\newline', 'MotoTune Path:', '\newline', blankline];
        dispgrp = [dispgrp, '', '\newline', textend(caldata.group{i}, lngthgrptxt(Layout), 0), '\newline', blankline];
    end
else
    dispnam = '';
    dispunt = '';
    disphlp = '';
    dispgrp = '';
end

% -----------------------
% --- Probes Display Text
% -----------------------

if numprbs ~= 0
    prbstitle = '\fontsize{20}\fontname{Bitstream Vera Sans Mono}\bf\itProbes';
    dispnam = [dispnam, '\fontsize{6}\newline', prbstitle, '\fontsize{12}\rm\newline\fontname{Arial}', displine];
    dispunt = [dispunt, '\fontsize{6}\newline\fontsize{20}\fontname{Bitstream Vera Sans Mono}\bf \fontsize{12}\rm\newline', blankline];
    disphlp = [disphlp, '\fontsize{6}\newline\fontsize{20}\fontname{Bitstream Vera Sans Mono}\bf \fontsize{12}\rm\newline', blankline];
    dispgrp = [dispgrp, '\fontsize{6}\newline\fontsize{20}\fontname{Bitstream Vera Sans Mono}\bf \fontsize{12}\rm\newline', blankline];
    for i = 1:numprbs
        nam = prbdata.nam{i};
        if i == numprbs
            dispnam = [dispnam, '\fontname{Bitstream Vera Sans Mono}', textend(nam, lngthnamtxt, 0), '\newline\newline', blankline];
        else
            dispnam = [dispnam, '\fontname{Bitstream Vera Sans Mono}', textend(nam, lngthnamtxt, 0), '\newline\newline\fontname{Arial}', displine];
        end
        dispunt = [dispunt, textend(prbdata.units{i}, lngthunttxt, 0), '\newline\newline', blankline];
        disphlp = [disphlp, textend(prbdata.help{i}, lngthhlptxt(Layout), 0), '\newline', 'MotoTune Path:', '\newline', blankline];
        dispgrp = [dispgrp, '', '\newline', textend(prbdata.group{i}, lngthgrptxt(Layout), 0), '\newline', blankline];
    end
end
% preventing subscript in LaTex
dispdata.DispNam = strrep(dispnam, '_', '\_');
dispdata.DispUnt = strrep(dispunt, '_', '\_');
dispdata.DispHlp = strrep(disphlp, '_', '\_');
dispdata.DispGrp = strrep(dispgrp, '_', '\_');

% --------------------
% --- Width and Height
% --------------------

% if fontsize changed in display, the numbers below must be altered
if (numcals + numprbs) >= 1
    ht = 58 * (numcals + numprbs) + 42 * (numcals >= 1) + 42 * (numprbs >= 1);
else
    ht = 20;
end
dispdata.Wd = pgwd(Layout);
dispdata.Ht = ht;

end


%% ---------------
%% function TXTEND
%% ---------------
%%
function outtxt = textend(intxt, maxlength, dir)
% --- Limits length of text
if length(intxt) > maxlength
    % limit at end of string
    if dir == 0
        outtxt = [intxt(1:(maxlength - 3)) '...'];
    % limit at beginning of string
    else
        outtxt = ['...' intxt((end - maxlength + 4):end)];
    end
else
    outtxt = intxt;
end

end


%% ----------------
%% function ONELINE
%% ----------------
%%
function outtxt = oneline(intxt)
% --- Places text on a single line
nonchar = find(~ isstrprop(intxt, 'graphic'));
blank = strfind(intxt, ' ');
for i = 1:length(nonchar)
    if isempty(find(blank == nonchar(i)))
        intxt(nonchar(i)) = ' ';
    end
end
outtxt = intxt;

end