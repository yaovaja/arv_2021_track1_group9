function [caldata prbdata] = calprbdata(path, depth, masks, links)
% --- Gathers data on calibrations & probes

% ---------
% --- Notes
% ---------

% path == system path
% depth == find_system parameter SearchDepth (0, 1, 2, etc.)
% masks == find_system parameter LookUnderMasks ('none', 'all', etc.)
% links == find_system parameter FollowLinks ('on' or 'off')

% ------------------------------
% --- Find Calibrations & Probes
% ------------------------------

% use ReferenceBlock for lookup tables bacause of S-functions in motohawk_table_1d and motohawk_table_2d
tblblklst = {'MotoHawk_lib/Lookup Tables/motohawk_prelookup'; ...
    'MotoHawk_lib/Lookup Tables/motohawk_interpolation_1d'; 'MotoHawk_lib/Lookup Tables/motohawk_interpolation_2d';...
    'MotoHawk_lib/Lookup Tables/motohawk_table_1d'; 'MotoHawk_lib/Lookup Tables/motohawk_table_2d'; ...
    'MotoHawk_lib/Fixed Point Blocks/Fixed Point Prelookup'; ...
    'MotoHawk_lib/Fixed Point Blocks/Fixed Point Interpolation 1D'; 'MotoHawk_lib/Fixed Point Blocks/Fixed Point Interpolation 2D'};
ioblklst = {'motohawk_sfun_ain'; 'motohawk_sfun_din'; 'motohawk_sfun_freq_in'; 'motohawk_sfun_dout'; 'motohawk_sfun_pwm'; 'motohawk_sfun_pwm_injector'};
callst = find_system(path, 'SearchDepth', depth, 'LookUnderMasks', masks, 'FollowLinks', links, 'FunctionName', 'motohawk_sfun_calibration', 'storage', 'Calibration');
callst = [callst; find_system(path, 'SearchDepth', depth, 'LookUnderMasks', masks, 'FollowLinks', links, 'FunctionName', 'motohawk_sfun_calibration', 'storage', 'Calibration NV')];
for i = 1:length(tblblklst)
    callst = [callst; find_system(path, 'SearchDepth', depth, 'LookUnderMasks', masks, 'FollowLinks', links, 'ReferenceBlock', tblblklst{i})];
end
for i = 1:length(ioblklst)
    callst = [callst; find_system(path, 'SearchDepth', depth, 'LookUnderMasks', masks, 'FollowLinks', links, 'FunctionName', ioblklst{i}, 'use_vardec', 'on')];
end
prblst = find_system(path, 'SearchDepth', depth, 'LookUnderMasks', masks, 'FollowLinks', links, 'FunctionName', 'motohawk_sfun_probe');
prblst = [prblst; find_system(path, 'SearchDepth', depth, 'LookUnderMasks', masks, 'FollowLinks', links, 'FunctionName', 'motohawk_sfun_calibration', 'storage', 'Display')];
prblst = [prblst; find_system(path, 'SearchDepth', depth, 'LookUnderMasks', masks, 'FollowLinks', links, 'FunctionName', 'motohawk_sfun_calibration', 'storage', 'Display NV')];

% add data def blocks and structure vardec blocks to lists
datadefstructlst = find_system(path, 'SearchDepth', depth, 'LookUnderMasks', masks, 'FollowLinks', links, 'FunctionName', 'motohawk_sfun_data_def', 'use_vardec', 'on');
datadefstructlst = [datadefstructlst; find_system(path, 'SearchDepth', depth, 'LookUnderMasks', masks, 'FollowLinks', links, 'FunctionName', 'motohawk_sfun_struct_vardec')];
for i = 1:length(datadefstructlst)
    if strcmp(get_param(datadefstructlst{i}, 'window'), 'Calibration')
        callst = [callst; datadefstructlst(i)];
    elseif strcmp(get_param(datadefstructlst{i}, 'window'), 'Display')
        prblst = [prblst; datadefstructlst(i)];
    end
end

% -------------------------
% --- Names & Alphabetizing
% -------------------------

calnams = {};
for i = 1:length(callst)
    try
        if strcmp(get_param(callst{i}, 'ReferenceBlock'), 'MotoHawk_lib/Lookup Tables/motohawk_prelookup') || ...
               strcmp(get_param(callst{i}, 'ReferenceBlock'), 'MotoHawk_lib/Fixed Point Blocks/Fixed Point Prelookup') 
            calnams{i} = [motohawk_get_param_ws(callst{i}, 'nam') 'IdxArr'];
        elseif strcmp(get_param(callst{i}, 'ReferenceBlock'), 'MotoHawk_lib/Lookup Tables/motohawk_interpolation_1d') || ...
                strcmp(get_param(callst{i}, 'ReferenceBlock'), 'MotoHawk_lib/Lookup Tables/motohawk_table_1d') || ...
                strcmp(get_param(callst{i}, 'ReferenceBlock'), 'MotoHawk_lib/Fixed Point Blocks/Fixed Point Interpolation 1D')
            calnams{i} = [motohawk_get_param_ws(callst{i}, 'nam') 'Tbl'];
        elseif strcmp(get_param(callst{i}, 'ReferenceBlock'), 'MotoHawk_lib/Lookup Tables/motohawk_interpolation_2d') || ...
                strcmp(get_param(callst{i}, 'ReferenceBlock'), 'MotoHawk_lib/Lookup Tables/motohawk_table_2d') || ...
                strcmp(get_param(callst{i}, 'ReferenceBlock'), 'MotoHawk_lib/Fixed Point Blocks/Fixed Point Interpolation 2D')
            calnams{i} = [motohawk_get_param_ws(callst{i}, 'nam') 'Map'];
        elseif strcmp(get_param(callst{i}, 'FunctionName'), 'motohawk_sfun_struct_vardec')
            calnams{i} = motohawk_get_param_ws(callst{i}, 'struct_nam');
        elseif strcmp(get_param(callst{i}, 'FunctionName'), 'motohawk_sfun_data_def') && ...
                strcmp(get_param(callst{i}, 'typ'), 'struct container')
            calnams{i} = motohawk_get_param_ws(callst{i}, 'container_nam');
        else
            calnams{i} = motohawk_get_param_ws(callst{i}, 'nam');
        end
    catch
        calnams{i} = motohawk_get_param_ws(callst{i}, 'nam');
    end
    if isempty(calnams{i})
        calnams{i} = 'Could Not Eval. (Update Req.)';
    end
end
prbnams = {};
for i = 1:length(prblst)
    if strcmp(get_param(prblst{i}, 'FunctionName'), 'motohawk_sfun_struct_vardec')
        prbnams{i} = motohawk_get_param_ws(prblst{i}, 'struct_nam');
    elseif strcmp(get_param(prblst{i}, 'FunctionName'), 'motohawk_sfun_data_def') && ...
        strcmp(get_param(prblst{i}, 'typ'), 'struct container')
        prbnams{i} = motohawk_get_param_ws(prblst{i}, 'container_nam');
    else
        prbnams{i} = motohawk_get_param_ws(prblst{i}, 'nam');
    end
    if isempty(prbnams{i})
        prbnams{i} = 'Could Not Eval. (Update Req.)';
    end
end
[calnams calidx] = sort(calnams);
[prbnams prbidx] = sort(prbnams);

% -------------------------------------------------------------------------------
% --- Name, Model Path, Units, Help Text, MotoTune Group, Data Type, Storage Type
% -------------------------------------------------------------------------------

for i = 1:2
    data = {};
    % 1 == calibration data; 2 == probe data
    if i == 1
        lst = callst;
        nams = calnams;
        idx = calidx;
        lngth = length(calnams);
    else
        lst = prblst;
        nams = prbnams;
        idx = prbidx;
        lngth = length(prbnams);
    end
    for j = 1:lngth
        % name
        data.nam(j) = nams(j);
        % model path
        data.mdlpath{j} = oneline(get_param(lst{idx(j)}, 'Parent'));
        % units
        try
            data.units{j} = motohawk_get_param_ws(lst{idx(j)}, 'units');
        catch
            data.units{j} = '';
        end
        try
            if strcmp(get_param(lst{idx(j)}, 'view_as'), 'Enumeration')
                data.units{j} = strtrim([data.units{j} ' (enum.)']);
            end
        catch
        end
        % help text
        try
            data.help{j} = motohawk_get_param_ws(lst{idx(j)}, 'help');
        catch
            data.help{j} = '';
        end
        % MotoTune group
        try
            data.group{j} = motohawk_get_param_ws(lst{idx(j)}, 'mototune_group');
        catch
            try
                data.group{j} = motohawk_get_param_ws(lst{idx(j)}, 'group');
            catch
                data.group{j} = '';
            end
        end
        data.group{j} = strrep(strrep(data.group{j}, ' | ', '|'), '|', ' | ');
        % data type
        try
            data.datatyp{j} = get_param(lst{idx(j)}, 'typ');
        catch
            try
                data.datatyp{j} = get_param(lst{idx(j)}, 'data_type');
            catch
                try
                    data.datatyp{j} = ['int16 (FP B' num2str(motohawk_get_param_ws(lst{idx(j)}, 'bscale')) ')'];
                catch
                    data.datatyp{j} = '(inherited)';
                end
            end
        end
        if strncmpi(data.datatyp{j}, 'Inherit', 7) == 1
            data.datatyp{j} = '(inherited)';
        end
        % storage type
        try
            data.stortyp{j} = lower(get_param(lst{idx(j)}, 'storage'));
            if strcmp(data.stortyp{j}, 'display')
                data.stortyp{j} = 'volatile';
            end
        catch
            data.stortyp{j} = 'volatile';
        end
    end
    if i == 1
        caldata = data;
    else
        prbdata = data;
    end
end

end


function outtxt = oneline(intxt)
% --- Places text on a single line
nonchar = find(~isstrprop(intxt, 'graphic'));
blank = strfind(intxt, ' ');
for i = 1:length(nonchar)
    if isempty(find(blank == nonchar(i)))
        intxt(nonchar(i)) = ' ';
    end
end
outtxt = intxt;

end