%% -------------------------
%% function MOTOHAWK_AUTODOC
%% -------------------------
%%
function motohawk_autodoc(ModelName, p1, v1, p2, v2)
% --- Takes screen shots of model

% --------------
% --- Load Model
% --------------

try
    ModelName = ModelName;
catch
    ModelName = bdroot;
end
try
    open_system(ModelName);
    disp(['AutoDoc process started for: ' ModelName]);
catch
    disp('Error: The specified model is not open and/or cannot be found on the current path');
    return
end
islibrary = strcmp(get_param(ModelName, 'LibraryType'), 'BlockLibrary');
if islibrary && strcmp(get_param(ModelName, 'Lock'), 'on')
    disp('Error: Model is a locked library; please unlock before proceeding.');
    return
end
load_system('AutoDoc_lib');

% ----------------------
% --- Create directories
% ----------------------

[fldr, nam, ext, ver] = fileparts(which(ModelName));
pth = [fldr '\AutoDoc_' ModelName];
if ~ isdir(pth)
    mkdir(pth);
end
copyfile(which('AutoDoc_SummaryTemplate.xls'), [pth '\AutoDoc_Summary.xls']);

% ----------------------------------------------
% --- Automatically Fill Model with Title Blocks
% ----------------------------------------------

try
    autofill = (strcmp(p1, 'AutoFill') && strcmp(v1, 'on')) || (strcmp(p2, 'AutoFill') && strcmp(v2, 'on'));
catch
    autofill = 0;
end
if autofill
    blklst = [ModelName; find_system(ModelName, 'LookUnderMasks', 'all', 'BlockType', 'SubSystem', 'ReferenceBlock', '')];
    for i = 1:length(blklst)
        if isempty(find_system(blklst{i}, 'SearchDepth', 1, 'LookUnderMasks', 'all', 'ReferenceBlock', 'AutoDoc_lib/motohawk_autodoc'))
            add_block('AutoDoc_lib/motohawk_autodoc', [blklst{i} '/motohawk_autodoc']);
        end
    end
end

% --------------------------------------
% --- Find and sort documentation blocks
% --------------------------------------

autodocblklst = find_system(ModelName, 'LookUnderMasks', 'all', 'ReferenceBlock', 'AutoDoc_lib/motohawk_autodoc');
count = 0;
autodocblklst_indoc = {};
for i = 1:length(autodocblklst)
    UserData = get_param(autodocblklst{i}, 'UserData');
    if UserData.DocOption == 1
        count = count + 1;
        autodocblklst_indoc{count} = autodocblklst{i};
    end
    clear UserData
end
autodocblklst = autodocblklst_indoc;

% -----------------
% --- Print screens
% -----------------

disp('Taking screen shots...');

docpath = {};
for i = 1:length(autodocblklst)
    
    % update AutoDoc blocks
    open_system(autodocblklst{i});
    scrnpth = get_param(autodocblklst{i}, 'Parent');
    UserData = get_param(autodocblklst{i}, 'UserData');
    docpath{i} = UserData.DocPath;
    clear UserData    
    
    % retrieve docpathdisp from title block for table of contents
    titleblkpth = find_system(scrnpth, 'SearchDepth', 1, 'LookUnderMasks', 'all', 'ReferenceBlock', 'AutoDoc_lib/autodoc_titleblk');
    UserData = get_param(titleblkpth{1}, 'UserData');
    docpathdisp{i} = UserData.DocPathDisp;
    docpathdisp{i} = strrep(docpathdisp{i}, '\_', '_');
    clear UserData;
    
    % open and print
    open_system(scrnpth, 'force');
    h = get_param(scrnpth, 'Handle');
    try
        print(h, '-v', '-dmeta', [pth, '\', docpath{i}, '.emf']);
    catch
    end
    docpathpages(i) = 1;
    
    % find cals and probes page
    cpblkpth = find_system(scrnpth, 'SearchDepth', 1, 'LookUnderMasks', 'all', 'ReferenceBlock', 'AutoDoc_lib/autodoc_calsprbspage');
    
    % open, print, and close cals and probes page
    if ~ isempty(cpblkpth)
        open_system(cpblkpth{1}, 'force');
        h = get_param(cpblkpth{1}, 'Handle');
        print(h, '-v', '-dmeta', [pth, '\', docpath{i}, '(cp).emf']);
        close_system(cpblkpth{1});
        docpathpages(i) = docpathpages(i) + 1;
    end
    
    % find Stateflow diagrams
    sfblkpths = find_system(scrnpth, 'SearchDepth', 1, 'LookUnderMasks', 'all', 'MaskType', 'Stateflow', 'MaskDescription', 'Stateflow diagram');
    
    % open, print, and close Stateflow diagrams
    currdir = cd;
    cd(pth);
    for j = 1:length(sfblkpths)
        sfpicfilenam = [docpath{i}, '(sf', num2str(j), ').emf'];
        open_system(sfblkpths{j});
        sfprint(sfblkpths{j}, 'meta', sfpicfilenam);
        sfclose;
        docpathpages(i) = docpathpages(i) + 1;
    end
    cd(currdir);
    
    % close
    if ~ strcmp(ModelName, scrnpth)
        close_system(scrnpth)
    end
    
end

% ----------------------------
% --- Create Table of Contents
% ----------------------------

disp('Creating table of contents document...');

% display strings
dotnum = 4 * fliplr([26:46]);
dots = {};
for i = 1:length(dotnum)
    dotdisp = '';
    for j = 1:dotnum(i)
        dotdisp = [dotdisp '.'];
    end
    dots{i} = dotdisp;
end

% sort by docpath
[docpath, idx] = sort(docpath);

% starting page
try
    if strcmp(p1, 'StartPg')
        page = v1;
    elseif strcmp(p2, 'StartPg')
        page = v2;
    end
catch
    page = 1;
end

% print lines
fid = fopen([pth, '\AutoDoc_TableOfContents.doc'], 'w+');
for i = 1:length(docpath)
    if strcmp(docpath{i}, '00')
        level = 0;
    elseif strncmp(docpath{i}, '00', 2)
        level = 1;
    else
        level = length(strfind(docpath{i}, '.')) + 1;
    end
    indent = '';
    for j = 1:level
        indent = [indent '    '];
    end
    fprintf(fid, '%s \n', [indent docpathdisp{idx(i)}]);
    fprintf(fid, '%s %3.0f\n', [indent dots{level + 1}], page);
    page = page + docpathpages(idx(i));
end
    
fclose(fid);

% ----------------------------------------------
% --- Create Calibration, Probe, & Fault Summary
% ----------------------------------------------

fn = [pth, '\AutoDoc_Summary'];

% display format variables
lngthcalnam = 64;
lngthprbnam = 46;
lngthunits = 18;
lngthhelp = 114;

% clear current data
xlswrite(fn, cellstr(''), 1, 'B6:E4997');
xlswrite(fn, cellstr(''), 2, 'B6:F4997');
xlswrite(fn, cellstr(''), 3, 'B4:E499');
xlswrite(fn, cellstr(''), 3, 'G4:H99');

if ~ islibrary
    
    disp('Creating calibrations, probes, & faults summary spreadsheet...');

    % gather data
    [caldata prbdata] = calprbdata(ModelName, 50, 'all', 'on');
    fltlst = find_system(ModelName, 'LookUnderMasks', 'all', 'FollowLinks', 'on', 'ReferenceBlock', 'MotoHawk_lib/Fault Management Blocks/motohawk_fault_def');
    fltactnlst = find_system(ModelName, 'LookUnderMasks', 'all', 'FollowLinks', 'on', 'ReferenceBlock', 'MotoHawk_lib/Fault Management Blocks/motohawk_fault_action');
    for i = 1:2
        clear data
        if i == 1
            lst = fltlst;
        else
            lst = fltactnlst;
        end
        data = [];
        for j = 1:length(lst)
            data.nam{j} = motohawk_get_param_ws(lst{j}, 'nam');
            if isempty(data.nam{j})
                data.nam{j} = 'Could Not Eval. (Update Req.)';
            end
        end
        if ~isempty(data)
            [data.nam idx] = sort(data.nam);
        end
        for j = 1:length(lst)
            data.mdlpath{j} = oneline(get_param(lst{idx(j)}, 'Parent'));
            if i == 1
                try
                    data.dwnsmplcnt{j} = motohawk_get_param_ws(lst{idx(j)}, 'downsample_count');
                catch
                    data.dwnsmplcnt{j} = [];
                end
                if strcmp(get_param(lst{idx(j)}, 'indeterminate'), 'off')
                    data.indetflt{j} = 'N';
                else
                    data.indetflt{j} = 'Y';
                end
            end
        end
        if i == 1
            fltdata = data;
        else
            fltactndata = data;
        end
    end

    % arrange and process data
    for i = 1:4
        clear txt
        if i == 1
            data = caldata;
            lngthnam = lngthcalnam;
        elseif i == 2
            data = prbdata;
            lngthnam = lngthprbnam;
        elseif i == 3
            data = fltdata;
            lngthnam = lngthcalnam;
        else
            data = fltactndata;
            lngthnam = lngthcalnam;
        end
        count = 0;
        if ~ isempty(data)
            for j = 1:length(data.nam)
                count = count + 1;
                txt{count,1} = num2str(j);
                txt{count,2} = textend(data.nam{j}, lngthnam, 0);
                if i == 1 || i == 2
                    txt{count,3} = textend(data.units{j}, lngthunits, 0);
                    txt{count,4} = textend(data.datatyp{j}, lngthunits, 0);
                end
                if i == 3
                    txt{count,3} = num2str(data.indetflt{j});
                    txt{count,4} = num2str(data.dwnsmplcnt{j});
                end
                if i == 2
                    txt{count,5} = textend(data.stortyp{j}, lngthunits, 0);
                end
                if i == 1 || i == 2
                    count = count + 1;
                    txt{count,2} = textend(data.help{j}, lngthhelp, 0);
                end
                count = count + 1;
                txt{count,2} = data.mdlpath{j};
                path = data.mdlpath{j};

                % add "Library Block" designation to model path for cals/probes in a library block
                if ~ strcmp(ModelName, data.mdlpath{j})
                    if ~ strcmp(get_param(data.mdlpath{j}, 'ReferenceBlock'), '')
                        parpath = get_param(data.mdlpath{j}, 'Parent');
                        while ~ strcmp(ModelName, parpath) && ~ strcmp(get_param(parpath, 'ReferenceBlock'), '')
                            path = parpath;
                            parpath = get_param(parpath, 'Parent');
                        end
                        txt{count,2} = [path ' (Library Block)'];
                        path = parpath;
                    end
                end
                txt{count,2} = strrep(strrep(txt{count,2}, ' / ', '/'), '/', ' / ');

                % add documentation number to front of model path text
                try
                    autodocblklst = find_system(path, 'SearchDepth', 1, 'LookUnderMasks', 'all', 'ReferenceBlock', 'AutoDoc_lib/motohawk_autodoc');
                    UserData = get_param(autodocblklst{1}, 'UserData');
                    docpath = strrep(UserData.DocPath, '.0', '.');
                    clear UserData
                    if ~ strncmp(docpath, '00', 2)
                        if strncmp(docpath, '0', 1)
                            docpath = docpath(2:end);
                        end
                        txt{count,2} = [docpath ' ' textend(txt{count,2}, (lngthhelp - length(docpath) - 1), 1)];
                    else
                        txt{count,2} = textend(txt{count,2}, lngthhelp, 1);
                    end
                catch
                    txt{count,2} = textend(txt{count,2}, lngthhelp, 1);
                end
                if i == 1 || i == 2
                    count = count + 1;
                    txt{count,2} = textend(data.group{j}, lngthhelp, 0);
                end
            end
            if i == 1
                xlswrite(fn, txt, 1, ['B6:E', num2str(count + 5)]);
            elseif i == 2
                xlswrite(fn, txt, 2, ['B6:F', num2str(count + 5)]);
            elseif i == 3
                xlswrite(fn, txt, 3, ['B4:D', num2str(count + 3)]);
            elseif i == 4
                xlswrite(fn, txt, 3, ['G4:H', num2str(count + 3)]);
            end
        end
    end
end

% % ----------------------
% % --- Create CAN Summary
% % ----------------------
%  Create a CAN summary in Excel, similar to calibration, probe, and fault summaries

disp(['     (files saved in ' pth ')']);
disp('AutoDoc is complete.');

end


%% ---------------
%% function TXTEND
%% ---------------
%%
function outtxt = textend(intxt, maxlength, dir)
% --- Limits length of text
if length(intxt) > maxlength
    % limit at end of string
    if dir == 0
        outtxt = [intxt(1:(maxlength - 3)) '...'];
    % limit at beginning of string
    else
        outtxt = ['...' intxt((end - maxlength + 4):end)];
    end
else
    outtxt = intxt;
end

end


%% ---------------
%% function ONELINE
%% ---------------
%%
function outtxt = oneline(intxt)
% --- Places text on a single line
nonchar = find(~isstrprop(intxt, 'graphic'));
blank = strfind(intxt, ' ');
for i = 1:length(nonchar)
    if isempty(find(blank == nonchar(i)))
        intxt(nonchar(i)) = ' ';
    end
end
outtxt = intxt;

end