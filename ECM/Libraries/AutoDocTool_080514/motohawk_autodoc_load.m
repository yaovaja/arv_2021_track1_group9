%% ------------------------------
%% function MOTOHAWK_AUTODOC_LOAD
%% ------------------------------
%%
function motohawk_autodoc_load(ModelName)
% --- Loads / updates AutoDoc blocks into model

% --------------
% --- Load Model
% --------------

try
    ModelName = ModelName;
catch
    ModelName = bdroot;
end
try
    open_system(ModelName);
catch
    disp('Error: The specified model is not open and/or cannot be found on the current path');
    return
end
islibrary = strcmp(get_param(ModelName, 'LibraryType'), 'BlockLibrary');
if islibrary && strcmp(get_param(ModelName, 'Lock'), 'on')
    disp('Error: Model is a locked library; please unlock before proceeding.');
    return
end
load_system('AutoDoc_lib');

% --------------------------------
% --- Open motohawk_autodoc Blocks
% --------------------------------

if ~ strcmp(ModelName, 'AutoDoc_lib')
    autodocblklst = find_system(ModelName, 'LookUnderMasks', 'all', 'ReferenceBlock', 'AutoDoc_lib/motohawk_autodoc');
    for i = 1:length(autodocblklst)
        open_system(autodocblklst{i});
        parsys = get_param(autodocblklst{i}, 'Parent');
        if ~ strcmp(ModelName, parsys)
            close_system(parsys);
        end
    end
end