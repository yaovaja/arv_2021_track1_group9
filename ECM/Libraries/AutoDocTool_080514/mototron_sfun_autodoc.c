
#define S_FUNCTION_NAME mototron_sfun_autodoc
#define NUM_PARAMS	0
#define NUM_INPUTS	0
#define NUM_OUTPUTS	0

/*
Template for Simulink mex files
The following parameters need to be defined:
	S_FUNCTION_NAME
	NUM_PARAMS
	NUM_INPUTS
	NUM_OUTPUTS

The following parameters are optional:
	EXTRA_INIT				if defined, will call extraInit(S) from mdlInitializeSizes(S)
	EXTRA_INIT_SAMPLE_TIMES	if defined, will call extraInitSampleTimes(S) from mdlInitializeSampleTimes(S)
	FUNCTION_TRIGGER		if defined, will insert code to call 1st output as function
	NO_MDL_function_name	if defined, caller must create this function
	NO_INPUT_WIDTH			if defined, will not set input widths, otherwise use 1
	NO_OUTPUT_WIDTH			if defined, will not set output widths, otherwise use 1
	NO_SS_OPTIONS			if defined, will not set call ssSetOptions(S) with default values
	NO_CODE_REUSE			if defined, will not set the SS_OPTION_WORKS_WITH_CODE_REUSE flag
*/

/*-----------------------------
    File Scope Variables
-----------------------------*/
#define S_FUNCTION_LEVEL 2

#include <math.h>
#include "simstruc.h"

/* Added for backward compatibility with R12 */
#ifndef SS_OPTION_WORKS_WITH_CODE_REUSE
#define SS_OPTION_WORKS_WITH_CODE_REUSE (0)
#endif

/* Added for backward compability with R13 */
#ifndef ssSetAsyncTimerAttributes
#define ssGetElapseTime(S,dataPtr) (0)
#define ssSetAsyncTaskPriorities(S,width,str)
#define ssSetNeedElapseTime(S,n) (0)
#define ssSetTimeSource(S,src)
#define ssSetAsyncTimerAttributes(S,attr)
#define SELF (0)
#endif

#ifndef EXTRA_SS_OPTIONS
#define EXTRA_SS_OPTIONS (0)
#endif

static uint32_T DYNAMIC_SS_OPTIONS;

#ifndef NO_MDL_INIT_SIZES
static void mdlInitializeSizes(SimStruct *S)
/*============================================================================*/
{
	int i;
	uint32_T reuse_option;

	ssSetNumSFcnParams(S, NUM_PARAMS);

	ssSetNumSampleTimes(S, 1);
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

	ssSetNumIWork(S, 0);
    ssSetNumRWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

	DYNAMIC_SS_OPTIONS = 0;

	#ifdef NO_CODE_REUSE
	reuse_option = 0;
	#else
	reuse_option = SS_OPTION_WORKS_WITH_CODE_REUSE;
	#endif

	#ifndef NO_SS_OPTIONS
	ssSetOptions(S,	SS_OPTION_SFUNCTION_INLINED_FOR_RTW     |
					SS_OPTION_DISALLOW_CONSTANT_SAMPLE_TIME |
					SS_OPTION_RUNTIME_EXCEPTION_FREE_CODE   |
					SS_OPTION_CAN_BE_CALLED_CONDITIONALLY	|
					SS_OPTION_CALL_TERMINATE_ON_EXIT		|
					EXTRA_SS_OPTIONS						|
					DYNAMIC_SS_OPTIONS						|
					reuse_option
					);
	#endif

    return;
}
#endif

#ifndef NO_MDL_INIT_SAMPLE_TIMES
static void mdlInitializeSampleTimes(SimStruct *S)
/*============================================================================*/
{
    ssSetSampleTime(S, 0, INHERITED_SAMPLE_TIME);
    ssSetOffsetTime(S, 0, FIXED_IN_MINOR_STEP_OFFSET);

    return;
}
#endif

#ifndef NO_MDL_OUTPUTS
static void mdlOutputs(SimStruct *S, int_T tid)
/*============================================================================*/
{
    return;
}
#endif


#ifndef NO_MDL_TERMINATE
#define MDL_TERMINATE
static void mdlTerminate(SimStruct *S)
/*============================================================================*/
{
	return;
}
#endif

#ifndef EXTRA_INIT
#ifndef EXTRA_INIT_SAMPLE_TIMES
#ifndef NO_INCLUDE_SIMULINK

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif

#endif
#endif
#endif