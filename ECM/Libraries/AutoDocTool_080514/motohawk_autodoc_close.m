%% -------------------------------
%% function MOTOHAWK_AUTODOC_CLOSE
%% -------------------------------
%%
function motohawk_autodoc_close(ModelName)
% --- Closes all AutoDoc blocks in model

% --------------
% --- Load Model
% --------------

try
    ModelName = ModelName;
catch
    ModelName = bdroot;
end
try
    open_system(ModelName);
catch
    disp('Error: The specified model is not open and/or cannot be found on the current path');
    return
end
islibrary = strcmp(get_param(ModelName, 'LibraryType'), 'BlockLibrary');
if islibrary && strcmp(get_param(ModelName, 'Lock'), 'on')
    disp('Error: Model is a locked library; please unlock before proceeding.');
    return
end

% -------------------------
% --- Delete AutoDoc Blocks
% -------------------------

docblklst = {'autodoc_titleblk'; 'autodoc_leftborder'; 'autodoc_rightborder'; 'autodoc_calsprbsdisp'; 'autodoc_calsprbspage'};
if ~ strcmp(ModelName, 'AutoDoc_lib')
    for i = 1:length(docblklst)
        blklst = find_system(ModelName, 'LookUnderMasks', 'all', 'ReferenceBlock', ['AutoDoc_lib/' docblklst{i}]);
        for j = 1:length(blklst)
            delete_block(blklst{j});
        end
    end
end