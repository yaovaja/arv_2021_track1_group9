%% -----------------------------
%% function AUTODOC_TITLEBLK_GUI
%% -----------------------------
%%
function varargout = autodoc_titleblk_gui(varargin)
% AUTODOC_TITLEBLK_GUI M-file for autodoc_titleblk_gui.fig
%      AUTODOC_TITLEBLK_GUI, by itself, creates a new AUTODOC_TITLEBLK_GUI
%      or raises the existing
%      singleton*.
%
%      H = AUTODOC_TITLEBLK_GUI returns the handle to a new AUTODOC_TITLEBLK_GUI or the handle to
%      the existing singleton*.
%
%      AUTODOC_TITLEBLK_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in AUTODOC_TITLEBLK_GUI.M with the given input arguments.
%
%      AUTODOC_TITLEBLK_GUI('Property','Value',...) creates a new AUTODOC_TITLEBLK_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before autodoc_titleblk_gui_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to autodoc_titleblk_gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help autodoc_titleblk_gui

% Last Modified by GUIDE v2.5 23-Apr-2007 15:02:38

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @autodoc_titleblk_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @autodoc_titleblk_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before autodoc_titleblk_gui is made visible.
function autodoc_titleblk_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to autodoc_titleblk_gui (see VARARGIN)

if strcmp(bdroot, 'AutoDoc_lib')
    handles.autodocblk = [gcs '/motohawk_autodoc'];
else
    autodocblks = find_system(gcs, 'SearchDepth', 1, 'LookUnderMasks', 'all', 'FollowLinks', 'on', 'ReferenceBlock', 'AutoDoc_lib/motohawk_autodoc');
    if isempty(autodocblks)
        handles.autodocblk = [gcs '/motohawk_autodoc'];
        add_block('AutoDoc_lib/motohawk_autodoc', handles.autodocblk);
    else
        handles.autodocblk = autodocblks{1};
    end
end

% --------------------------------------------------
% --- Retrieves Data from UserData and Sets into GUI
% --------------------------------------------------

UserData = get_param(handles.autodocblk, 'UserData');

fields = {'DocOption'; 'Layout'; 'CalPrbDisp'; 'NumCheck'; 'LowLevelPos'; ...
    'ObjCheck'; 'ObjText'; 'DescCheck'; 'DescText'; 'EqnCheck'; 'EqnText'};
fieldtyp = {'Value'; 'Value'; 'Value'; 'Value'; 'String';...
    'Value'; 'String'; 'Value'; 'String'; 'Value'; 'String'};
defaultval = {1; 1; 1; 1; '0';...
    1; 'Write objectives and requirements here.'; 1; 'Write description here.'; 1; 'Write equations here.'};
for i = 1:length(fields)
    try,
        set(eval(['handles.' fields{i}]), fieldtyp{i}, eval(['UserData.' fields{i}]));
    catch,
        disp('');
        set(eval(['handles.' fields{i}]), fieldtyp{i}, defaultval{i});
    end
end

DisableInput(handles.LowLevelPos, [handles.NumCheck handles.DocOption], [1 2]);
DisableInput(handles.ObjText, handles.ObjCheck, 0);
DisableInput(handles.DescText, handles.DescCheck, 0);
DisableInput(handles.EqnText, handles.EqnCheck, 0);

% Choose default command line output for autodoc_titleblk_gui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes autodoc_titleblk_gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = autodoc_titleblk_gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in DocOption.
function DocOption_Callback(hObject, eventdata, handles)
% hObject    handle to DocOption (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns DocOption contents as cell array
%        contents{get(hObject,'Value')} returns selected item from DocOption
DisableInput(handles.LowLevelPos, [handles.NumCheck handles.DocOption], [1 2]);


% --- Executes during object creation, after setting all properties.
function DocOption_CreateFcn(hObject, eventdata, handles)
% hObject    handle to DocOption (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in NumCheck.
function NumCheck_Callback(hObject, eventdata, handles)
% hObject    handle to NumCheck (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of NumCheck
DisableInput(handles.LowLevelPos, [handles.NumCheck handles.DocOption], [1 2]);


function LowLevelPos_Callback(hObject, eventdata, handles)
% hObject    handle to LowLevelPos (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of LowLevelPos as text
%        str2double(get(hObject,'String')) returns contents of LowLevelPos
%        as a double


% --- Executes during object creation, after setting all properties.
function LowLevelPos_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LowLevelPos (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in Layout.
function Layout_Callback(hObject, eventdata, handles)
% hObject    handle to Layout (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns Layout contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Layout


% --- Executes during object creation, after setting all properties.
function Layout_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Layout (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in CalPrbDisp.
function CalPrbDisp_Callback(hObject, eventdata, handles)
% hObject    handle to CalPrbDisp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns CalPrbDisp contents as cell array
%        contents{get(hObject,'Value')} returns selected item from CalPrbDisp


% --- Executes during object creation, after setting all properties.
function CalPrbDisp_CreateFcn(hObject, eventdata, handles)
% hObject    handle to CalPrbDisp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ObjCheck.
function ObjCheck_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ObjCheck
DisableInput(handles.ObjText, handles.ObjCheck, 0);


function ObjText_Callback(hObject, eventdata, handles)
% hObject    handle to ObjText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ObjText as text
%        str2double(get(hObject,'String')) returns contents of ObjText as a double


% --- Executes during object creation, after setting all properties.
function ObjText_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ObjText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in DescCheck.
function DescCheck_Callback(hObject, eventdata, handles)
% hObject    handle to DescCheck (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of DescCheck
DisableInput(handles.DescText, handles.DescCheck, 0);


function DescText_Callback(hObject, eventdata, handles)
% hObject    handle to DescText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of DescText as text
%        str2double(get(hObject,'String')) returns contents of DescText as a double


% --- Executes during object creation, after setting all properties.
function DescText_CreateFcn(hObject, eventdata, handles)
% hObject    handle to DescText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in EqnCheck.
function EqnCheck_Callback(hObject, eventdata, handles)
% hObject    handle to EqnCheck (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of EqnCheck
DisableInput(handles.EqnText, handles.EqnCheck, 0);


function EqnText_Callback(hObject, eventdata, handles)
% hObject    handle to EqnText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EqnText as text
%        str2double(get(hObject,'String')) returns contents of EqnText as a double


% --- Executes during object creation, after setting all properties.
function EqnText_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EqnText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in okButton.
function okButton_Callback(hObject, eventdata, handles)
% hObject    handle to okButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
store_data(handles);
delete(handles.figure1);


% --- Executes on button press in cancelButton.
function cancelButton_Callback(hObject, eventdata, handles)
% hObject    handle to cancelButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
delete(handles.figure1);


% --- Executes on button press in applyButton.
function applyButton_Callback(hObject, eventdata, handles)
% hObject    handle to applyButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
store_data(handles);


function store_data(handles)
% --- Stores GUI data to UserData

UserData.DocOption = get(handles.DocOption, 'Value');
UserData.Layout = get(handles.Layout, 'Value');
UserData.CalPrbDisp = get(handles.CalPrbDisp, 'Value');
UserData.NumCheck = get(handles.NumCheck, 'Value');
UserData.LowLevelPos = str2num(get(handles.LowLevelPos, 'String'));
UserData.ObjCheck = get(handles.ObjCheck, 'Value');
UserData.ObjText = get(handles.ObjText, 'String');
UserData.DescCheck = get(handles.DescCheck, 'Value');
UserData.DescText = get(handles.DescText, 'String');
UserData.EqnCheck = get(handles.EqnCheck, 'Value');
UserData.EqnText = get(handles.EqnText, 'String');

motohawk_set_param(handles.autodocblk, 'UserData', UserData);


function DisableInput(depenparam, indepenparam, disablecond)
% --- Disable input on GUI
enable = 'on';
for i = 1:length(indepenparam)
    if get(indepenparam(i), 'Value') == disablecond(i)
        enable = 'off';
    end
end
set(depenparam, 'Enable', enable)