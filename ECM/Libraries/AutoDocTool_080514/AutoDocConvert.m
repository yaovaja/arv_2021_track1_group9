%% -----------------------
%% function AUTODOCCONVERT
%% -----------------------
%%
function AutoDocConvert(ModelName)
% --- Converts to new AutoDoc blocks
% --- Delete old AutoDoc folder, place new AutoDoc folder on MATLAB path, and run this function

islibrary = strcmp(get_param(ModelName, 'LibraryType'), 'BlockLibrary');
if islibrary
    set_param(ModelName, 'Lock', 'off');
end
load_system('AutoDoc_lib');

% ---------------------
% --- Delete Old Blocks
% ---------------------

lst = find_system(ModelName, 'LookUnderMasks', 'all', 'SourceBlock', 'AutoDoc_lib/Documentation_Calibrations_Probes_Block');
lst = [lst; find_system(ModelName, 'LookUnderMasks', 'all', 'SourceBlock', 'AutoDoc_lib/Documentation_Left_Border')];
lst = [lst; find_system(ModelName, 'LookUnderMasks', 'all', 'SourceBlock', 'AutoDoc_lib/Documentation_Right_Border')];
for i = 1:length(lst)
    delete_block(lst{i});
end
clear lst

% -------------------------------------------------------
% --- Find Old Title Block Blocks, Change Reference Block
% -------------------------------------------------------

lst = find_system(ModelName, 'LookUnderMasks', 'all', 'SourceBlock', 'AutoDoc_lib/Documentation_Title_Block');
lst = [lst; find_system(ModelName, 'LookUnderMasks', 'all', 'ReferenceBlock', 'AutoDoc_lib/autodoc_titleblk')];
adlst = find_system(ModelName, 'LookUnderMasks', 'all', 'ReferenceBlock', 'AutoDoc_lib/motohawk_autodoc');
paradlst = get_param(adlst, 'Parent');
for i = 1:length(lst)
    if isempty(find(strcmp(paradlst, get_param(lst{i}, 'Parent'))))
        try
            motohawk_set_param(lst{i}, 'SourceBlock', 'AutoDoc_lib/autodoc_titleblk');
        catch
        end
        UserData1 = get_param(lst{i}, 'UserData');
        if UserData1.DocOption == 3
            if islibrary
                UserData.DocOption = 1;
            else
                UserData.DocOption = 2;
            end
        else
            UserData.DocOption = UserData1.DocOption;
        end
        UserData.Layout = UserData1.Layout;
        % if AutoDoc_lib/Cals_and_Probes_Page present, set UserData.CalPrbDisp == 2
        try
            UserData.CalPrbDisp = UserData1.CalPrbDisp;
        catch
            lst2 = find_system(get_param(lst{i}, 'Parent'), 'SearchDepth', 1, 'LookUnderMasks', 'all', 'SourceBlock', 'AutoDoc_lib/Cals_and_Probes_Page');
            if ~ isempty(lst2)
                for j = 1:length(lst2)
                    delete_block(lst2{j});
                end
                UserData.CalPrbDisp = 2;
            else
                UserData.CalPrbDisp = 1;
            end
        end
        try
            UserData.NumCheck = UserData1.NumCheck;
            UserData.LowLevelPos = UserData1.LowLevelPos;
        catch
            UserData.NumCheck = 1;
            UserData.LowLevelPos = 0;
        end
        UserData.ObjCheck = UserData1.ObjCheck;
        UserData.ObjText = UserData1.ObjText;
        UserData.DescCheck = UserData1.DescCheck;
        UserData.DescText = UserData1.DescText;
        UserData.EqnCheck = UserData1.EqnCheck;
        UserData.EqnText = UserData1.EqnText;
        currblk = [get_param(lst{i}, 'Parent') '/motohawk_autodoc'];
        try
            add_block('AutoDoc_lib/motohawk_autodoc', currblk);
            set_param(currblk, 'UserDataPersistent', 'on');
            set_param(currblk, 'UserData', UserData);
        catch
        end
        clear UserData UserData1
    end
end

motohawk_autodoc_load(ModelName)