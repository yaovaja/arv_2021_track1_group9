/*
 * File: Model.c
 *
 * Code generated for Simulink model 'CRVLAB_ECU_Project'.
 *
 * Model version                  : 1.812
 * Simulink Coder version         : 8.0 (R2011a) 09-Mar-2011
 * TLC version                    : 8.0 (Feb  3 2011)
 * C/C++ source code generated on : Tue Apr 10 17:20:14 2012
 *
 * Target selection: motohawk_ert_rtw.tlc
 * Embedded hardware selection: Specified
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "Model.h"

/* Include model header file for global data */
#include "CRVLAB_ECU_Project.h"
#include "CRVLAB_ECU_Project_private.h"

/* Named constants for Stateflow: '<S3>/Chart1' */
#define CRVLAB_ECU_Project_IN_Crank    (1U)
#define CRVLAB_ECU_Project_IN_Cut      (2U)
#define CRVLAB_ECU_Project_IN_FuelCut  (1U)
#define CRVLAB_ECU_Project_IN_Idle     (2U)
#define CRVLAB_ECU_Project_IN_NO_ACTIVE_CHILD (0U)
#define CRVLAB_ECU_Project_IN_Run      (3U)
#define CRVLAB_ECU_Project_IN_RunDDF   (1U)
#define CRVLAB_ECU_Project_IN_RunDiesel (2U)
#define CRVLAB_ECU_Project_IN_Stall    (4U)

/* Initial conditions for function-call system: '<Root>/Model' */
void CRVLAB_ECU_Project_Model_Init(void)
{
  /* S-Function Block: <S23>/motohawk_delta_time */
  {
    uint32_T now = 0;
    extern uint32_T Timer_FreeRunningCounter_GetDeltaUpdateReference_us(uint32_T
      * pReference_lower32Bits, uint32_T *pReference_upper32Bits);
    extern uint32_T Timer_FreeRunningCounter_GetRawTicksFromTime(uint32_T
      u32Time_us);
    Timer_FreeRunningCounter_GetDeltaUpdateReference_us(&now, NULL);
    CRVLAB_ECU_Project_DWork.s23_motohawk_delta_time_DWORK1 = now -
      Timer_FreeRunningCounter_GetRawTicksFromTime(0.0);
  }

  /* InitializeConditions for Stateflow: '<S3>/Chart1' */
  CRVLAB_ECU_Project_DWork.s26_is_Cut = 0U;
  CRVLAB_ECU_Project_DWork.s26_is_Run = 0U;
  CRVLAB_ECU_Project_DWork.s26_is_active_c3_CRVLAB_ECU_Project = 0U;
  CRVLAB_ECU_Project_DWork.s26_is_c3_CRVLAB_ECU_Project = 0U;
  CRVLAB_ECU_Project_B.s26_State = 0.0;

  /* S-Function Block: <S61>/motohawk_delta_time */
  {
    uint32_T now = 0;
    extern uint32_T Timer_FreeRunningCounter_GetDeltaUpdateReference_us(uint32_T
      * pReference_lower32Bits, uint32_T *pReference_upper32Bits);
    extern uint32_T Timer_FreeRunningCounter_GetRawTicksFromTime(uint32_T
      u32Time_us);
    Timer_FreeRunningCounter_GetDeltaUpdateReference_us(&now, NULL);
    CRVLAB_ECU_Project_DWork.s61_motohawk_delta_time_DWORK1 = now -
      Timer_FreeRunningCounter_GetRawTicksFromTime(-1);
  }

  /* S-Function Block: <S62>/motohawk_delta_time */
  {
    uint32_T now = 0;
    extern uint32_T Timer_FreeRunningCounter_GetDeltaUpdateReference_us(uint32_T
      * pReference_lower32Bits, uint32_T *pReference_upper32Bits);
    extern uint32_T Timer_FreeRunningCounter_GetRawTicksFromTime(uint32_T
      u32Time_us);
    Timer_FreeRunningCounter_GetDeltaUpdateReference_us(&now, NULL);
    CRVLAB_ECU_Project_DWork.s62_motohawk_delta_time_DWORK1 = now -
      Timer_FreeRunningCounter_GetRawTicksFromTime(-1);
  }

  /* S-Function Block: <S59>/motohawk_delta_time */
  {
    uint32_T now = 0;
    extern uint32_T Timer_FreeRunningCounter_GetDeltaUpdateReference_us(uint32_T
      * pReference_lower32Bits, uint32_T *pReference_upper32Bits);
    extern uint32_T Timer_FreeRunningCounter_GetRawTicksFromTime(uint32_T
      u32Time_us);
    Timer_FreeRunningCounter_GetDeltaUpdateReference_us(&now, NULL);
    CRVLAB_ECU_Project_DWork.s59_motohawk_delta_time_DWORK1 = now -
      Timer_FreeRunningCounter_GetRawTicksFromTime(-1);
  }

  /* S-Function Block: <S60>/motohawk_delta_time */
  {
    uint32_T now = 0;
    extern uint32_T Timer_FreeRunningCounter_GetDeltaUpdateReference_us(uint32_T
      * pReference_lower32Bits, uint32_T *pReference_upper32Bits);
    extern uint32_T Timer_FreeRunningCounter_GetRawTicksFromTime(uint32_T
      u32Time_us);
    Timer_FreeRunningCounter_GetDeltaUpdateReference_us(&now, NULL);
    CRVLAB_ECU_Project_DWork.s60_motohawk_delta_time_DWORK1 = now -
      Timer_FreeRunningCounter_GetRawTicksFromTime(-1);
  }

  /* S-Function Block: <S63>/motohawk_delta_time */
  {
    uint32_T now = 0;
    extern uint32_T Timer_FreeRunningCounter_GetDeltaUpdateReference_us(uint32_T
      * pReference_lower32Bits, uint32_T *pReference_upper32Bits);
    extern uint32_T Timer_FreeRunningCounter_GetRawTicksFromTime(uint32_T
      u32Time_us);
    Timer_FreeRunningCounter_GetDeltaUpdateReference_us(&now, NULL);
    CRVLAB_ECU_Project_DWork.s63_motohawk_delta_time_DWORK1 = now -
      Timer_FreeRunningCounter_GetRawTicksFromTime(-1);
  }

  /* S-Function Block: <S106>/motohawk_delta_time */
  {
    uint32_T now = 0;
    extern uint32_T Timer_FreeRunningCounter_GetDeltaUpdateReference_us(uint32_T
      * pReference_lower32Bits, uint32_T *pReference_upper32Bits);
    extern uint32_T Timer_FreeRunningCounter_GetRawTicksFromTime(uint32_T
      u32Time_us);
    Timer_FreeRunningCounter_GetDeltaUpdateReference_us(&now, NULL);
    CRVLAB_ECU_Project_DWork.s106_motohawk_delta_time_DWORK1 = now -
      Timer_FreeRunningCounter_GetRawTicksFromTime(5000.0);
  }
}

/* Start for function-call system: '<Root>/Model' */
void CRVLAB_ECU_Project_Model_Start(void)
{
  /* Start for S-Function (motohawk_sfun_injector): '<S96>/motohawk_injector' */

  /* Create Injector Sequence Out: INJ1D */
  {
    NativeError_S sErrorResult;
    S_SeqOutAttributes DynamicObj;
    S_InjSeqCreateAttributes CreateObj;
    CreateObj.uValidAttributesMask = 0;
    CreateObj.u1NumPulsesToCreate = 1;
    CreateObj.s2PulseOffsetDegATDC1Arr[0] = 0;
    DynamicObj.uMinPulseDurationInMicroSecs = 1500;
    CreateObj.uValidAttributesMask |= USE_SEQ_GRANULARITY;
    CreateObj.eGranularity = FINE_GRANULARITY;
    CRVLAB_ECU_Project_DWork.s96_motohawk_injector_IWORK = ((int16_T) RES_INJ1D);
    sErrorResult = CreateResource((E_ModuleResource)
      (CRVLAB_ECU_Project_DWork.s96_motohawk_injector_IWORK), &CreateObj,
      BEHAVIOUR_INJ_SEQ);
    if (SUCCESS(sErrorResult)) {
      DynamicObj.u1AffectedPulse = 0;
      DynamicObj.uValidAttributesMask =
        USE_SEQ_PULSE_COND | USE_SEQ_PEAK | USE_SEQ_UPDATE_MODE |
        USE_SEQ_ENDEVENT_REPORT_COND | USE_SEQ_TIMING | USE_SEQ_MIN_DURATION;
      DynamicObj.eUpdateMode = SEQ_UPDATE_OS_PROTECTED;
      DynamicObj.u2PeakTimeInMicroSecs = 0;
      DynamicObj.eResourceCondition = SEQ_DISABLED;
      DynamicObj.eReportCond = RES_ENABLED;
      DynamicObj.TimingObj.s2StartAngle = 0;
      DynamicObj.TimingObj.s2StopAngle = 0;
      DynamicObj.TimingObj.u4Duration = 0;
      DynamicObj.TimingObj.eCtrlMode = DURATION_CTRL;
      sErrorResult = SetResourceAttributes((E_ModuleResource)
        (CRVLAB_ECU_Project_DWork.s96_motohawk_injector_IWORK), &DynamicObj,
        BEHAVIOUR_INJ_SEQ);
    }

    {
      extern uint8_T seq_create_INJ1D;
      if (SUCCESS(sErrorResult))
        seq_create_INJ1D = 0;
      else
        seq_create_INJ1D = (uint8_T) GetErrorCode(sErrorResult);
    }

    CRVLAB_ECU_Project_DWork.s96_motohawk_injector_DWORK1 = 0;
    CRVLAB_ECU_Project_DWork.s96_motohawk_injector_DWORK2 = 0;
  }

  /* Start for S-Function (motohawk_sfun_injector): '<S96>/motohawk_injector1' */

  /* Create Injector Sequence Out: INJ2D */
  {
    NativeError_S sErrorResult;
    S_SeqOutAttributes DynamicObj;
    S_InjSeqCreateAttributes CreateObj;
    CreateObj.uValidAttributesMask = 0;
    CreateObj.u1NumPulsesToCreate = 1;
    CreateObj.s2PulseOffsetDegATDC1Arr[0] = 8640;
    DynamicObj.uMinPulseDurationInMicroSecs = 1500;
    CreateObj.uValidAttributesMask |= USE_SEQ_GRANULARITY;
    CreateObj.eGranularity = FINE_GRANULARITY;
    CRVLAB_ECU_Project_DWork.s96_motohawk_injector1_IWORK = ((int16_T) RES_INJ2D);
    sErrorResult = CreateResource((E_ModuleResource)
      (CRVLAB_ECU_Project_DWork.s96_motohawk_injector1_IWORK), &CreateObj,
      BEHAVIOUR_INJ_SEQ);
    if (SUCCESS(sErrorResult)) {
      DynamicObj.u1AffectedPulse = 0;
      DynamicObj.uValidAttributesMask =
        USE_SEQ_PULSE_COND | USE_SEQ_PEAK | USE_SEQ_UPDATE_MODE |
        USE_SEQ_ENDEVENT_REPORT_COND | USE_SEQ_TIMING | USE_SEQ_MIN_DURATION;
      DynamicObj.eUpdateMode = SEQ_UPDATE_OS_PROTECTED;
      DynamicObj.u2PeakTimeInMicroSecs = 0;
      DynamicObj.eResourceCondition = SEQ_DISABLED;
      DynamicObj.eReportCond = RES_ENABLED;
      DynamicObj.TimingObj.s2StartAngle = 0;
      DynamicObj.TimingObj.s2StopAngle = 0;
      DynamicObj.TimingObj.u4Duration = 0;
      DynamicObj.TimingObj.eCtrlMode = DURATION_CTRL;
      sErrorResult = SetResourceAttributes((E_ModuleResource)
        (CRVLAB_ECU_Project_DWork.s96_motohawk_injector1_IWORK), &DynamicObj,
        BEHAVIOUR_INJ_SEQ);
    }

    {
      extern uint8_T seq_create_INJ2D;
      if (SUCCESS(sErrorResult))
        seq_create_INJ2D = 0;
      else
        seq_create_INJ2D = (uint8_T) GetErrorCode(sErrorResult);
    }

    CRVLAB_ECU_Project_DWork.s96_motohawk_injector1_DWORK1 = 0;
    CRVLAB_ECU_Project_DWork.s96_motohawk_injector1_DWORK2 = 0;
  }

  /* Start for S-Function (motohawk_sfun_injector): '<S96>/motohawk_injector2' */

  /* Create Injector Sequence Out: INJ3D */
  {
    NativeError_S sErrorResult;
    S_SeqOutAttributes DynamicObj;
    S_InjSeqCreateAttributes CreateObj;
    CreateObj.uValidAttributesMask = 0;
    CreateObj.u1NumPulsesToCreate = 1;
    CreateObj.s2PulseOffsetDegATDC1Arr[0] = 2880;
    DynamicObj.uMinPulseDurationInMicroSecs = 1500;
    CreateObj.uValidAttributesMask |= USE_SEQ_GRANULARITY;
    CreateObj.eGranularity = FINE_GRANULARITY;
    CRVLAB_ECU_Project_DWork.s96_motohawk_injector2_IWORK = ((int16_T) RES_INJ3D);
    sErrorResult = CreateResource((E_ModuleResource)
      (CRVLAB_ECU_Project_DWork.s96_motohawk_injector2_IWORK), &CreateObj,
      BEHAVIOUR_INJ_SEQ);
    if (SUCCESS(sErrorResult)) {
      DynamicObj.u1AffectedPulse = 0;
      DynamicObj.uValidAttributesMask =
        USE_SEQ_PULSE_COND | USE_SEQ_PEAK | USE_SEQ_UPDATE_MODE |
        USE_SEQ_ENDEVENT_REPORT_COND | USE_SEQ_TIMING | USE_SEQ_MIN_DURATION;
      DynamicObj.eUpdateMode = SEQ_UPDATE_OS_PROTECTED;
      DynamicObj.u2PeakTimeInMicroSecs = 0;
      DynamicObj.eResourceCondition = SEQ_DISABLED;
      DynamicObj.eReportCond = RES_ENABLED;
      DynamicObj.TimingObj.s2StartAngle = 0;
      DynamicObj.TimingObj.s2StopAngle = 0;
      DynamicObj.TimingObj.u4Duration = 0;
      DynamicObj.TimingObj.eCtrlMode = DURATION_CTRL;
      sErrorResult = SetResourceAttributes((E_ModuleResource)
        (CRVLAB_ECU_Project_DWork.s96_motohawk_injector2_IWORK), &DynamicObj,
        BEHAVIOUR_INJ_SEQ);
    }

    {
      extern uint8_T seq_create_INJ3D;
      if (SUCCESS(sErrorResult))
        seq_create_INJ3D = 0;
      else
        seq_create_INJ3D = (uint8_T) GetErrorCode(sErrorResult);
    }

    CRVLAB_ECU_Project_DWork.s96_motohawk_injector2_DWORK1 = 0;
    CRVLAB_ECU_Project_DWork.s96_motohawk_injector2_DWORK2 = 0;
  }

  /* Start for S-Function (motohawk_sfun_injector): '<S96>/motohawk_injector3' */

  /* Create Injector Sequence Out: INJ4D */
  {
    NativeError_S sErrorResult;
    S_SeqOutAttributes DynamicObj;
    S_InjSeqCreateAttributes CreateObj;
    CreateObj.uValidAttributesMask = 0;
    CreateObj.u1NumPulsesToCreate = 1;
    CreateObj.s2PulseOffsetDegATDC1Arr[0] = 5760;
    DynamicObj.uMinPulseDurationInMicroSecs = 1500;
    CreateObj.uValidAttributesMask |= USE_SEQ_GRANULARITY;
    CreateObj.eGranularity = FINE_GRANULARITY;
    CRVLAB_ECU_Project_DWork.s96_motohawk_injector3_IWORK = ((int16_T) RES_INJ4D);
    sErrorResult = CreateResource((E_ModuleResource)
      (CRVLAB_ECU_Project_DWork.s96_motohawk_injector3_IWORK), &CreateObj,
      BEHAVIOUR_INJ_SEQ);
    if (SUCCESS(sErrorResult)) {
      DynamicObj.u1AffectedPulse = 0;
      DynamicObj.uValidAttributesMask =
        USE_SEQ_PULSE_COND | USE_SEQ_PEAK | USE_SEQ_UPDATE_MODE |
        USE_SEQ_ENDEVENT_REPORT_COND | USE_SEQ_TIMING | USE_SEQ_MIN_DURATION;
      DynamicObj.eUpdateMode = SEQ_UPDATE_OS_PROTECTED;
      DynamicObj.u2PeakTimeInMicroSecs = 0;
      DynamicObj.eResourceCondition = SEQ_DISABLED;
      DynamicObj.eReportCond = RES_ENABLED;
      DynamicObj.TimingObj.s2StartAngle = 0;
      DynamicObj.TimingObj.s2StopAngle = 0;
      DynamicObj.TimingObj.u4Duration = 0;
      DynamicObj.TimingObj.eCtrlMode = DURATION_CTRL;
      sErrorResult = SetResourceAttributes((E_ModuleResource)
        (CRVLAB_ECU_Project_DWork.s96_motohawk_injector3_IWORK), &DynamicObj,
        BEHAVIOUR_INJ_SEQ);
    }

    {
      extern uint8_T seq_create_INJ4D;
      if (SUCCESS(sErrorResult))
        seq_create_INJ4D = 0;
      else
        seq_create_INJ4D = (uint8_T) GetErrorCode(sErrorResult);
    }

    CRVLAB_ECU_Project_DWork.s96_motohawk_injector3_DWORK1 = 0;
    CRVLAB_ECU_Project_DWork.s96_motohawk_injector3_DWORK2 = 0;
  }

  /* S-Function (motohawk_sfun_probe): '<S114>/motohawk_probe1' */
  (cyl_DataStore()) = ((uint8_T)1U);
}

/* Output and update for function-call system: '<Root>/Model' */
void CRVLAB_ECU_Project_Model(void)
{
  /* local block i/o variables */
  real_T rtb_Sum1;
  real_T rtb_Sum2;
  real_T rtb_Sum2_c;
  real_T rtb_RPM;
  real_T rtb_Sum1_d;
  real_T rtb_Gain_b;
  real_T rtb_Sum1_p;
  real_T rtb_Saturation;
  real_T rtb_Subtract1;
  real_T rtb_Sum2_b;
  real_T rtb_motohawk_data_read1_b;
  real_T rtb_motohawk_data_read2;
  real_T rtb_Add;
  real_T rtb_motohawk_data_read1_k;
  real_T rtb_motohawk_data_read2_p;
  real_T rtb_Merge;
  real_T rtb_motohawk_data_read1_bt;
  real_T rtb_motohawk_data_read2_j;
  real_T rtb_Merge_p;
  real_T rtb_motohawk_data_read1_d;
  real_T rtb_motohawk_data_read2_k;
  real_T rtb_motohawk_data_read1_l;
  real_T rtb_motohawk_data_read2_o;
  real_T rtb_motohawk_data_read1_j;
  real_T rtb_motohawk_data_read2_f;
  real_T rtb_motohawk_data_read1_h;
  real_T rtb_motohawk_data_read2_c;
  real_T rtb_motohawk_data_read1_p;
  real_T rtb_motohawk_data_read2_d;
  real_T rtb_motohawk_data_read1_o;
  real_T rtb_motohawk_data_read2_kx;
  real_T rtb_motohawk_data_read1_a;
  real_T rtb_motohawk_data_read2_dx;
  real_T rtb_motohawk_interpolation_2d2;
  real_T rtb_Merge_d;
  real_T rtb_motohawk_data_read1_am;
  real_T rtb_motohawk_data_read2_a;
  real_T rtb_Merge_l;
  real_T rtb_motohawk_delta_time;
  real_T rtb_motohawk_delta_time_n;
  real_T rtb_motohawk_delta_time_c;
  real_T rtb_motohawk_delta_time_d;
  real_T rtb_Add_hi;
  real_T rtb_motohawk_delta_time_l;
  real_T rtb_motohawk_delta_time_j;
  real_T rtb_motohawk_data_read1_bo;
  real_T rtb_motohawk_data_read2_b;
  real_T rtb_Saturation_n;
  real_T rtb_motohawk_data_read1_n;
  real_T rtb_motohawk_data_read4_j;
  real_T rtb_motohawk_data_read2_oa;
  real_T rtb_motohawk_data_read5_j;
  real_T rtb_motohawk_data_read3_n;
  real_T rtb_motohawk_data_read6_m;
  real_T rtb_motohawk_data_read1_i;
  real_T rtb_motohawk_interpolation_2d1;
  real_T rtb_motohawk_interpolation_2d2_o;
  real_T rtb_motohawk_interpolation_2d1_k;
  real_T rtb_motohawk_interpolation_1d4;
  real_T rtb_motohawk_interpolation_2d1_m;
  real_T rtb_motohawk_interpolation_2d2_e;
  real_T rtb_motohawk_interpolation_1d;
  real_T rtb_motohawk_interpolation_2d1_f;
  real_T rtb_motohawk_interpolation_2d2_d;
  real_T rtb_motohawk_interpolation_1d1;
  real_T rtb_motohawk_interpolation_2d1_j;
  real_T rtb_motohawk_interpolation_2d2_i;
  real_T rtb_motohawk_data_read6_a;
  real_T rtb_motohawk_interpolation_2d3;
  real_T rtb_motohawk_interpolation_2d1_o;
  real_T rtb_motohawk_interpolation_2d2_g;
  real_T rtb_motohawk_interpolation_1d2;
  real_T rtb_motohawk_interpolation_2d1_km;
  real_T rtb_motohawk_interpolation_2d2_h;
  real_T rtb_motohawk_data_read6_n;
  real_T rtb_motohawk_interpolation_2d3_d;
  real_T rtb_motohawk_interpolation_2d1_c;
  real_T rtb_motohawk_interpolation_2d2_ox;
  real_T rtb_motohawk_interpolation_2d1_h;
  real_T rtb_motohawk_interpolation_2d2_dz;
  real_T rtb_motohawk_interpolation_2d1_ja;
  real_T rtb_motohawk_interpolation_2d2_k;
  real_T rtb_motohawk_interpolation_2d1_d;
  real_T rtb_motohawk_interpolation_2d2_kp;
  real_T rtb_motohawk_interpolation_2d3_dh;
  real_T rtb_motohawk_interpolation_2d4;
  real_T rtb_Sum5;
  real_T rtb_motohawk_data_read1_bu;
  real_T rtb_Gain5;
  uint32_T rtb_DataTypeConversion1;
  uint32_T rtb_TmpSignalConversionAtMuxPSPInport3[3];
  uint32_T rtb_TmpSignalConversionAtMuxPSPInport3_k[3];
  uint32_T rtb_TmpSignalConversionAtMuxPSPInport3_ki[3];
  uint32_T rtb_TmpSignalConversionAtMuxPSPInport3_c[3];
  uint32_T rtb_DataTypeConversion3;
  uint32_T rtb_DataTypeConversion6;
  uint32_T rtb_DataTypeConversion2;
  uint32_T rtb_DataTypeConversion3_n;
  uint32_T rtb_DataTypeConversion2_i;
  uint32_T rtb_DataTypeConversion3_p;
  uint32_T rtb_DataTypeConversion5;
  uint32_T rtb_DataTypeConversion3_ng;
  int16_T rtb_DataTypeConversion3_l;
  int16_T rtb_DataTypeConversion_d;
  int16_T rtb_TmpSignalConversionAtMuxPSPInport2[3];
  int16_T rtb_TmpSignalConversionAtMuxPSPInport2_g[3];
  int16_T rtb_TmpSignalConversionAtMuxPSPInport2_e[3];
  int16_T rtb_TmpSignalConversionAtMuxPSPInport2_h[3];
  int16_T rtb_DataTypeConversion1_j;
  int16_T rtb_DataTypeConversion1_m;
  int16_T rtb_DataTypeConversion1_ea;
  int16_T rtb_DataTypeConversion1_d;
  int16_T rtb_DataTypeConversion1_f;
  int16_T rtb_DataTypeConversion4;
  uint16_T rtb_motohawk_ain5;
  uint16_T rtb_motohawk_ain1;
  uint16_T rtb_motohawk_ain2;
  uint16_T rtb_motohawk_ain3;
  uint16_T rtb_motohawk_ain4;
  uint16_T rtb_RPM_l;
  uint16_T rtb_DataTypeConversion2_ib;
  index_T rtb_motohawk_prelookup;
  index_T rtb_motohawk_prelookup4;
  index_T rtb_motohawk_prelookup1;
  index_T rtb_motohawk_prelookup2;
  index_T rtb_motohawk_prelookup3;
  index_T rtb_motohawk_prelookup1_d;
  index_T rtb_motohawk_prelookup4_g;
  index_T rtb_motohawk_prelookup2_n;
  index_T rtb_motohawk_prelookup3_j;
  index_T rtb_motohawk_prelookup7;
  index_T rtb_motohawk_prelookup2_i;
  index_T rtb_motohawk_prelookup3_o;
  index_T rtb_motohawk_prelookup1_e;
  index_T rtb_motohawk_prelookup4_a;
  index_T rtb_motohawk_prelookup_a;
  index_T rtb_motohawk_prelookup2_c;
  index_T rtb_motohawk_prelookup3_b;
  index_T rtb_motohawk_prelookup1_k;
  index_T rtb_motohawk_prelookup4_d;
  index_T rtb_motohawk_prelookup5;
  index_T rtb_motohawk_prelookup2_j;
  index_T rtb_motohawk_prelookup3_jo;
  index_T rtb_motohawk_prelookup1_el;
  index_T rtb_motohawk_prelookup4_b;
  index_T rtb_motohawk_prelookup5_g;
  index_T rtb_motohawk_prelookup6;
  index_T rtb_motohawk_prelookup2_k;
  index_T rtb_motohawk_prelookup3_d;
  index_T rtb_motohawk_prelookup1_p;
  index_T rtb_motohawk_prelookup4_ay;
  index_T rtb_motohawk_prelookup6_f;
  index_T rtb_motohawk_prelookup2_c3;
  index_T rtb_motohawk_prelookup3_g;
  index_T rtb_motohawk_prelookup1_kh;
  index_T rtb_motohawk_prelookup4_h;
  index_T rtb_motohawk_prelookup5_m;
  index_T rtb_motohawk_prelookup6_e;
  index_T rtb_motohawk_prelookup2_d;
  index_T rtb_motohawk_prelookup3_a;
  index_T rtb_motohawk_prelookup1_eo;
  index_T rtb_motohawk_prelookup4_dz;
  index_T rtb_motohawk_prelookup2_b;
  index_T rtb_motohawk_prelookup3_f;
  index_T rtb_motohawk_prelookup1_o;
  index_T rtb_motohawk_prelookup4_i;
  index_T rtb_motohawk_prelookup2_kp;
  index_T rtb_motohawk_prelookup3_jm;
  index_T rtb_motohawk_prelookup1_d2;
  index_T rtb_motohawk_prelookup4_o;
  index_T rtb_motohawk_prelookup2_f;
  index_T rtb_motohawk_prelookup3_j3;
  index_T rtb_motohawk_prelookup1_l;
  index_T rtb_motohawk_prelookup4_p;
  index_T rtb_motohawk_prelookup6_n;
  index_T rtb_motohawk_prelookup7_d;
  index_T rtb_motohawk_prelookup5_d;
  index_T rtb_motohawk_prelookup8;
  uint8_T rtb_motohawk_data_read1_e;
  boolean_T rtb_Compare_j;
  boolean_T rtb_Compare_mk;
  boolean_T rtb_Compare_al;
  boolean_T rtb_Compare_et;
  boolean_T rtb_motohawk_data_read4_b;
  boolean_T rtb_DataTypeConversion_h;
  boolean_T rtb_motohawk_data_read6_j;
  boolean_T rtb_motohawk_data_read8_n;
  boolean_T rtb_motohawk_data_read6_i;
  boolean_T rtb_motohawk_data_read8_j;
  boolean_T rtb_motohawk_data_read6_p;
  boolean_T rtb_motohawk_data_read8_ng;
  real_T rtb_DataTypeConversion_n;
  boolean_T rtb_LogicalOperator;
  real_T rtb_Sum4;
  real_T rtb_UnitDelay1_d;
  real_T rtb_Sum4_a;
  real_T rtb_UnitDelay1_e;
  real_T rtb_Sum4_j;
  real_T rtb_UnitDelay1_g;
  real_T rtb_Sum4_m;
  real_T rtb_UnitDelay1_i;
  uint8_T rtb_Compare_o;
  real_T rtb_Sum4_e;
  real_T rtb_UnitDelay1_em;
  real_T rtb_Gain4;
  real_T rtb_Add_h;
  real_T rtb_Product_da;
  boolean_T rtb_Switch2;
  real_T rtb_Sum_g;
  real_T rtb_Sum_n;
  real32_T tmp;
  int16_T rtb_DataTypeConversion2_p_0;
  int16_T rtb_DataTypeConversion_ds_0;
  uint32_T rtb_DataTypeConversion1_e_0;

  /* S-Function Block: <S8>/motohawk_ain1 Resource: AN8M */
  {
    extern NativeError_S AN8M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
    AN8M_AnalogInput_Get(&CRVLAB_ECU_Project_B.s8_motohawk_ain1, NULL);
  }

  /* RelationalOperator: '<S19>/Compare' incorporates:
   *  Constant: '<S19>/Constant'
   *  DataTypeConversion: '<S8>/Data Type Conversion'
   */
  rtb_Compare_j = ((CRVLAB_ECU_Project_B.s8_motohawk_ain1 < 500));

  /* S-Function (motohawk_sfun_data_write): '<S8>/motohawk_data_write' */
  /* Write to Data Storage as scalar: AC_On */
  {
    AC_On_DataStore() = rtb_Compare_j;
  }

  /* S-Function Block: <S9>/motohawk_ain5 Resource: AN6M */
  {
    extern NativeError_S AN6M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
    AN6M_AnalogInput_Get(&rtb_motohawk_ain5, NULL);
  }

  /* DataTypeConversion: '<S9>/Data Type Conversion' incorporates:
   *  S-Function (motohawk_sfun_ain): '<S9>/motohawk_ain5'
   */
  rtb_DataTypeConversion_n = (real_T)rtb_motohawk_ain5;

  /* Product: '<S9>/Product1' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S9>/motohawk_calibration'
   */
  rtb_DataTypeConversion_n *= (CNGPGain_kPa_ADC_DataStore());

  /* Sum: '<S9>/Sum1' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S9>/motohawk_calibration1'
   */
  rtb_Sum1 = rtb_DataTypeConversion_n + (CNGPOffset_kPa_DataStore());

  /* S-Function (motohawk_sfun_data_write): '<S9>/motohawk_data_write' */
  /* Write to Data Storage as scalar: CNGP_kPa */
  {
    CNGP_kPa_DataStore() = rtb_Sum1;
  }

  /* S-Function Block: <S10>/motohawk_ain1 Resource: AN11M */
  {
    extern NativeError_S AN11M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
    AN11M_AnalogInput_Get(&rtb_motohawk_ain1, NULL);
  }

  /* RelationalOperator: '<S20>/Compare' incorporates:
   *  Constant: '<S20>/Constant'
   *  S-Function (motohawk_sfun_ain): '<S10>/motohawk_ain1'
   */
  rtb_Compare_mk = ((rtb_motohawk_ain1 > 500));

  /* S-Function (motohawk_sfun_data_write): '<S10>/motohawk_data_write1' */
  /* Write to Data Storage as scalar: Power_On */
  {
    Power_On_DataStore() = rtb_Compare_mk;
  }

  /* S-Function Block: <S10>/motohawk_ain2 Resource: AN12M */
  {
    extern NativeError_S AN12M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
    AN12M_AnalogInput_Get(&rtb_motohawk_ain2, NULL);
  }

  /* RelationalOperator: '<S21>/Compare' incorporates:
   *  Constant: '<S21>/Constant'
   *  S-Function (motohawk_sfun_ain): '<S10>/motohawk_ain2'
   */
  rtb_Compare_al = ((rtb_motohawk_ain2 > 500));

  /* S-Function (motohawk_sfun_data_write): '<S10>/motohawk_data_write2' */
  /* Write to Data Storage as scalar: Fuel_On */
  {
    Fuel_On_DataStore() = rtb_Compare_al;
  }

  /* S-Function Block: <S10>/motohawk_ain3 Resource: AN13M */
  {
    extern NativeError_S AN13M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
    AN13M_AnalogInput_Get(&rtb_motohawk_ain3, NULL);
  }

  /* RelationalOperator: '<S22>/Compare' incorporates:
   *  Constant: '<S22>/Constant'
   *  S-Function (motohawk_sfun_ain): '<S10>/motohawk_ain3'
   */
  rtb_Compare_et = ((rtb_motohawk_ain3 > 500));

  /* S-Function (motohawk_sfun_data_write): '<S10>/motohawk_data_write3' */
  /* Write to Data Storage as scalar: Normal_On */
  {
    Normal_On_DataStore() = rtb_Compare_et;
  }

  /* S-Function Block: <S10>/motohawk_ain4 Resource: AN9M */
  {
    extern NativeError_S AN9M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
    AN9M_AnalogInput_Get(&rtb_motohawk_ain4, NULL);
  }

  /* S-Function (motohawk_sfun_data_write): '<S10>/motohawk_data_write' */
  /* Write to Data Storage as scalar: Poten_ADC */
  {
    Poten_ADC_DataStore() = rtb_motohawk_ain4;
  }

  /* S-Function Block: <S11>/motohawk_ain Resource: AN5M */
  {
    extern NativeError_S AN5M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
    AN5M_AnalogInput_Get(&rtb_DataTypeConversion2_ib, NULL);
  }

  /* DataTypeConversion: '<S11>/Data Type Conversion1' */
  CRVLAB_ECU_Project_B.s11_DataTypeConversion1 = (real_T)
    rtb_DataTypeConversion2_ib;

  /* Product: '<S11>/Product1' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S11>/motohawk_calibration2'
   */
  rtb_DataTypeConversion_n = CRVLAB_ECU_Project_B.s11_DataTypeConversion1 *
    (ECTGain_C_ADC_DataStore());

  /* Sum: '<S11>/Sum2' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S11>/motohawk_calibration3'
   */
  rtb_Sum2 = rtb_DataTypeConversion_n + (ECTOffset_C_DataStore());

  /* S-Function (motohawk_sfun_data_write): '<S11>/motohawk_data_write' */
  /* Write to Data Storage as scalar: ECT_C */
  {
    ECT_C_DataStore() = rtb_Sum2;
  }

  /* S-Function Block: <S12>/motohawk_ain1 Resource: AN3M */
  {
    extern NativeError_S AN3M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
    AN3M_AnalogInput_Get(&rtb_DataTypeConversion2_ib, NULL);
  }

  /* DataTypeConversion: '<S12>/Data Type Conversion1' */
  CRVLAB_ECU_Project_B.s12_DataTypeConversion1 = (real_T)
    rtb_DataTypeConversion2_ib;

  /* Product: '<S12>/Product1' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S12>/motohawk_calibration2'
   */
  rtb_DataTypeConversion_n = CRVLAB_ECU_Project_B.s12_DataTypeConversion1 *
    (EGRGain_Pct_ADC_DataStore());

  /* Sum: '<S12>/Sum2' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S12>/motohawk_calibration3'
   */
  rtb_Sum2_c = rtb_DataTypeConversion_n + (EGROffset_Pct_DataStore());

  /* S-Function (motohawk_sfun_data_write): '<S12>/motohawk_data_write' */
  /* Write to Data Storage as scalar: EGR_Pct */
  {
    EGR_Pct_DataStore() = rtb_Sum2_c;
  }

  /* S-Function Block: <S13>/motohawk_encoder_average_rpm */
  {
    rtb_RPM_l = GetEncoderResourceAverageRPM();
  }

  /* DataTypeConversion: '<S13>/Data Type Conversion' incorporates:
   *  S-Function (motohawk_sfun_encoder_average_rpm): '<S13>/motohawk_encoder_average_rpm'
   */
  rtb_RPM = (real_T)rtb_RPM_l;

  /* S-Function (motohawk_sfun_data_write): '<S13>/motohawk_data_write' */
  /* Write to Data Storage as scalar: EngineSpeed_rpm */
  {
    EngineSpeed_rpm_DataStore() = rtb_RPM;
  }

  /* DataTypeConversion: '<S13>/Data Type Conversion3' incorporates:
   *  Gain: '<S13>/Gain'
   *  S-Function (motohawk_sfun_calibration): '<S13>/motohawk_calibration2'
   */
  rtb_DataTypeConversion_n = 16.0 * (EncoderTDCOffset_DataStore());
  if (rtIsNaN(rtb_DataTypeConversion_n) || rtIsInf(rtb_DataTypeConversion_n)) {
    rtb_DataTypeConversion_n = 0.0;
  } else {
    rtb_DataTypeConversion_n = fmod(rtb_DataTypeConversion_n < 0.0 ? ceil
      (rtb_DataTypeConversion_n) : floor(rtb_DataTypeConversion_n), 65536.0);
  }

  rtb_DataTypeConversion3_l = (int16_T)(rtb_DataTypeConversion_n < 0.0 ?
    (int16_T)-(int16_T)(uint16_T)-rtb_DataTypeConversion_n : (int16_T)(uint16_T)
    rtb_DataTypeConversion_n);

  /* End of DataTypeConversion: '<S13>/Data Type Conversion3' */
  /* S-Function Block: <S13>/motohawk_encoder_offset */
  {
    S_EncoderResourceAttributes encoder_attributes;
    encoder_attributes.uValidAttributesMask = USE_ENC_TDC_OFFSET;
    encoder_attributes.s2TDCOffset = rtb_DataTypeConversion3_l;
    SetResourceAttributes(RES_ENCODER, &encoder_attributes, BEHAVIOUR_ENCODER);
  }

  /* S-Function Block: <S14>/motohawk_ain14 Resource: DRVP */
  {
    extern NativeError_S DRVP_AnalogInput_Get(uint16_T *adc, uint16_T *status);
    DRVP_AnalogInput_Get(&CRVLAB_ECU_Project_B.s14_motohawk_ain14, NULL);
  }

  /* DataTypeConversion: '<S14>/Data Type Conversion' */
  rtb_DataTypeConversion_n = (real_T)CRVLAB_ECU_Project_B.s14_motohawk_ain14;

  /* Product: '<S14>/Product1' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S14>/motohawk_calibration'
   */
  rtb_DataTypeConversion_n *= (KeySwGain_Volt_ADC_DataStore());

  /* Sum: '<S14>/Sum1' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S14>/motohawk_calibration1'
   */
  rtb_Sum1_d = rtb_DataTypeConversion_n + (KeySwOffset_Volt_DataStore());

  /* S-Function (motohawk_sfun_data_write): '<S14>/motohawk_data_write' */
  /* Write to Data Storage as scalar: KeySw_Volt */
  {
    KeySw_Volt_DataStore() = rtb_Sum1_d;
  }

  /* S-Function Block: <S15>/motohawk_ain4 Resource: AN7M */
  {
    extern NativeError_S AN7M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
    AN7M_AnalogInput_Get(&rtb_DataTypeConversion2_ib, NULL);
  }

  /* DataTypeConversion: '<S15>/Data Type Conversion1' */
  CRVLAB_ECU_Project_B.s15_DataTypeConversion1 = (real_T)
    rtb_DataTypeConversion2_ib;

  /* Gain: '<S15>/Gain' */
  rtb_Gain_b = 0.0048828125 * CRVLAB_ECU_Project_B.s15_DataTypeConversion1;

  /* S-Function Block: <S15>/motohawk_prelookup */
  {
    extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
      ordarr[], uint32_T sz, uint16_T prev);
    (MAF_volt_in_MAF_volt_MAF_g_s_mapIn_DataStore()) = rtb_Gain_b;
    (MAF_volt_in_MAF_volt_MAF_g_s_mapIdx_DataStore()) = TablePrelookup_real_T
      (rtb_Gain_b, (MAF_volt_in_MAF_volt_MAF_g_s_mapIdxArr_DataStore()), 8,
       (MAF_volt_in_MAF_volt_MAF_g_s_mapIdx_DataStore()));
    rtb_motohawk_prelookup = (MAF_volt_in_MAF_volt_MAF_g_s_mapIdx_DataStore());
  }

  /* S-Function Block: <S15>/motohawk_interpolation_1d */
  {
    extern real_T TableInterpolation1D_real_T(uint16_T idx, real_T *tbl_data,
      uint32_T sz);
    CRVLAB_ECU_Project_B.s15_motohawk_interpolation_1d =
      TableInterpolation1D_real_T(rtb_motohawk_prelookup, (real_T *)
      ((MAF_g_s_in_MAF_volt_MAF_g_s_mapTbl_DataStore())), 8);
    (MAF_g_s_in_MAF_volt_MAF_g_s_map_DataStore()) =
      CRVLAB_ECU_Project_B.s15_motohawk_interpolation_1d;
  }

  /* S-Function Block: <S23>/motohawk_delta_time */
  {
    uint32_T delta;
    extern uint32_T Timer_FreeRunningCounter_GetDeltaUpdateReference_us(uint32_T
      * pReference_lower32Bits, uint32_T *pReference_upper32Bits);
    delta = Timer_FreeRunningCounter_GetDeltaUpdateReference_us
      (&CRVLAB_ECU_Project_DWork.s23_motohawk_delta_time_DWORK1, NULL);
    rtb_Sum5 = ((real_T) delta) * 0.000001;
  }

  /* Product: '<S23>/Product' incorporates:
   *  MinMax: '<S23>/MinMax'
   *  S-Function (motohawk_sfun_calibration): '<S23>/motohawk_calibration'
   */
  rtb_Sum5 /= (rtb_Sum5 >= (MAF_FiltConst_DataStore())) || rtIsNaN
    ((MAF_FiltConst_DataStore())) ? rtb_Sum5 : (MAF_FiltConst_DataStore());

  /* Sum: '<S24>/Sum1' incorporates:
   *  Constant: '<S24>/Constant'
   *  Product: '<S24>/Product'
   *  Product: '<S24>/Product1'
   *  Sum: '<S24>/Sum'
   *  UnitDelay: '<S24>/Unit Delay'
   */
  rtb_Sum1_p = (1.0 - rtb_Sum5) * CRVLAB_ECU_Project_DWork.s24_UnitDelay_DSTATE
    + CRVLAB_ECU_Project_B.s15_motohawk_interpolation_1d * rtb_Sum5;

  /* S-Function (motohawk_sfun_data_write): '<S15>/motohawk_data_write' */
  /* Write to Data Storage as scalar: MAF_g_s */
  {
    MAF_g_s_DataStore() = rtb_Sum1_p;
  }

  /* S-Function Block: <S16>/motohawk_ain Resource: AN1M */
  {
    extern NativeError_S AN1M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
    AN1M_AnalogInput_Get(&rtb_DataTypeConversion2_ib, NULL);
  }

  /* DataTypeConversion: '<S16>/Data Type Conversion' */
  CRVLAB_ECU_Project_B.s16_DataTypeConversion = (real_T)
    rtb_DataTypeConversion2_ib;

  /* Polyval: '<S16>/Polynomial' */
  rtb_Sum5 = -8.482E-7;
  rtb_Sum5 = rtb_Sum5 * CRVLAB_ECU_Project_B.s16_DataTypeConversion + 0.001026;
  rtb_Sum5 = rtb_Sum5 * CRVLAB_ECU_Project_B.s16_DataTypeConversion + -0.1615;
  rtb_Sum5 = rtb_Sum5 * CRVLAB_ECU_Project_B.s16_DataTypeConversion + 2.12;

  /* Saturate: '<S16>/Saturation' */
  rtb_Saturation = rtb_Sum5 >= 100.0 ? 100.0 : rtb_Sum5 <= 0.0 ? 0.0 : rtb_Sum5;

  /* S-Function (motohawk_sfun_data_write): '<S16>/motohawk_data_write' */
  /* Write to Data Storage as scalar: Pedal_Pct */
  {
    Pedal_Pct_DataStore() = rtb_Saturation;
  }

  /* S-Function Block: <S17>/motohawk_ain1 Resource: AN4M */
  {
    extern NativeError_S AN4M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
    AN4M_AnalogInput_Get(&CRVLAB_ECU_Project_B.s17_motohawk_ain1, NULL);
  }

  /* DataTypeConversion: '<S17>/Data Type Conversion' */
  rtb_DataTypeConversion_n = (real_T)CRVLAB_ECU_Project_B.s17_motohawk_ain1;

  /* Product: '<S17>/Product1' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S17>/motohawk_calibration'
   */
  rtb_DataTypeConversion_n *= (RailPGain_MPa_DataStore());

  /* Sum: '<S17>/Sum1' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S17>/motohawk_calibration1'
   */
  CRVLAB_ECU_Project_B.s17_Sum1 = rtb_DataTypeConversion_n +
    (RailPOffset_MPa_DataStore());

  /* UnitDelay: '<S25>/Unit Delay1' */
  rtb_DataTypeConversion_n = CRVLAB_ECU_Project_DWork.s25_UnitDelay1_DSTATE;

  /* Sum: '<S25>/Subtract1' incorporates:
   *  Constant: '<S17>/Constant3'
   *  Constant: '<S25>/Constant'
   *  Constant: '<S25>/Constant1'
   *  Constant: '<S25>/Constant10'
   *  Constant: '<S25>/Constant2'
   *  Product: '<S25>/Divide'
   *  Product: '<S25>/Divide1'
   *  Product: '<S25>/Divide2'
   *  Product: '<S25>/Divide3'
   *  S-Function (motohawk_sfun_calibration): '<S17>/motohawk_calibration2'
   *  Sum: '<S25>/Subtract3'
   *  UnitDelay: '<S25>/Unit Delay'
   */
  rtb_Subtract1 = (1.0 - 0.01 / (1.0 / ((RailPFilt_Hz_DataStore()) * 2.0 *
    3.1416))) * CRVLAB_ECU_Project_DWork.s25_UnitDelay_DSTATE +
    (CRVLAB_ECU_Project_B.s17_Sum1 - rtb_DataTypeConversion_n);

  /* S-Function (motohawk_sfun_data_write): '<S17>/motohawk_data_write' */
  /* Write to Data Storage as scalar: RailP_MPa */
  {
    RailP_MPa_DataStore() = rtb_Subtract1;
  }

  /* S-Function Block: <S18>/motohawk_ain1 Resource: AN2M */
  {
    extern NativeError_S AN2M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
    AN2M_AnalogInput_Get(&rtb_DataTypeConversion2_ib, NULL);
  }

  /* DataTypeConversion: '<S18>/Data Type Conversion1' */
  CRVLAB_ECU_Project_B.s18_DataTypeConversion1 = (real_T)
    rtb_DataTypeConversion2_ib;

  /* Product: '<S18>/Product2' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S18>/motohawk_calibration4'
   */
  rtb_DataTypeConversion_n = CRVLAB_ECU_Project_B.s18_DataTypeConversion1 *
    (TPSGain_Pct_ADC_DataStore());

  /* Sum: '<S18>/Sum2' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S18>/motohawk_calibration5'
   */
  rtb_Sum2_b = rtb_DataTypeConversion_n - (TPSOffset_Pct_DataStore());

  /* S-Function (motohawk_sfun_data_write): '<S18>/motohawk_data_write' */
  /* Write to Data Storage as scalar: TPS_Pct */
  {
    TPS_Pct_DataStore() = rtb_Sum2_b;
  }

  /* Logic: '<S3>/Logical Operator' incorporates:
   *  S-Function (motohawk_sfun_data_read): '<S3>/motohawk_data_read2'
   */
  rtb_LogicalOperator = !Normal_On_DataStore();

  /* Stateflow: '<S3>/Chart1' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S3>/motohawk_calibration1'
   *  S-Function (motohawk_sfun_calibration): '<S3>/motohawk_calibration3'
   *  S-Function (motohawk_sfun_calibration): '<S3>/motohawk_calibration4'
   *  S-Function (motohawk_sfun_calibration): '<S3>/motohawk_calibration5'
   *  S-Function (motohawk_sfun_calibration): '<S3>/motohawk_calibration6'
   *  S-Function (motohawk_sfun_calibration): '<S3>/motohawk_calibration7'
   *  S-Function (motohawk_sfun_calibration): '<S3>/motohawk_calibration8'
   *  S-Function (motohawk_sfun_data_read): '<S3>/motohawk_data_read'
   *  S-Function (motohawk_sfun_data_read): '<S3>/motohawk_data_read1'
   *  S-Function (motohawk_sfun_data_read): '<S3>/motohawk_data_read3'
   */

  /* Gateway: Model/2 Stateflow
     Chart/Chart1 */
  /* During: Model/2 Stateflow
     Chart/Chart1 */
  if (CRVLAB_ECU_Project_DWork.s26_is_active_c3_CRVLAB_ECU_Project == 0) {
    /* Entry: Model/2 Stateflow
       Chart/Chart1 */
    CRVLAB_ECU_Project_DWork.s26_is_active_c3_CRVLAB_ECU_Project = 1U;

    /* Transition: '<S26>:14' */
    CRVLAB_ECU_Project_DWork.s26_is_c3_CRVLAB_ECU_Project =
      CRVLAB_ECU_Project_IN_Stall;

    /* Entry 'Stall': '<S26>:13' */
    CRVLAB_ECU_Project_B.s26_State = 1.0;
  } else {
    switch (CRVLAB_ECU_Project_DWork.s26_is_c3_CRVLAB_ECU_Project) {
     case CRVLAB_ECU_Project_IN_Crank:
      /* During 'Crank': '<S26>:11' */
      if (EngineSpeed_rpm_DataStore() < (Stall_rpm_DataStore())) {
        /* Transition: '<S26>:16' */
        CRVLAB_ECU_Project_DWork.s26_is_c3_CRVLAB_ECU_Project =
          CRVLAB_ECU_Project_IN_Stall;

        /* Entry 'Stall': '<S26>:13' */
        CRVLAB_ECU_Project_B.s26_State = 1.0;
      } else {
        if (EngineSpeed_rpm_DataStore() > 900.0) {
          /* Transition: '<S26>:100' */
          CRVLAB_ECU_Project_DWork.s26_is_c3_CRVLAB_ECU_Project =
            CRVLAB_ECU_Project_IN_Run;

          /* Transition: '<S26>:102' */
          CRVLAB_ECU_Project_DWork.s26_is_Run = CRVLAB_ECU_Project_IN_RunDiesel;

          /* Entry 'RunDiesel': '<S26>:10' */
          CRVLAB_ECU_Project_B.s26_State = 3.0;
        }
      }
      break;

     case CRVLAB_ECU_Project_IN_Cut:
      /* During 'Cut': '<S26>:103' */
      if (Pedal_Pct_DataStore() > (PedalHigh_DataStore())) {
        /* Transition: '<S26>:106' */
        CRVLAB_ECU_Project_DWork.s26_is_Cut = (uint8_T)
          CRVLAB_ECU_Project_IN_NO_ACTIVE_CHILD;
        CRVLAB_ECU_Project_DWork.s26_is_c3_CRVLAB_ECU_Project =
          CRVLAB_ECU_Project_IN_Run;

        /* Transition: '<S26>:102' */
        CRVLAB_ECU_Project_DWork.s26_is_Run = CRVLAB_ECU_Project_IN_RunDiesel;

        /* Entry 'RunDiesel': '<S26>:10' */
        CRVLAB_ECU_Project_B.s26_State = 3.0;
      } else if (EngineSpeed_rpm_DataStore() < (Stall_rpm_DataStore())) {
        /* Transition: '<S26>:108' */
        CRVLAB_ECU_Project_DWork.s26_is_Cut = (uint8_T)
          CRVLAB_ECU_Project_IN_NO_ACTIVE_CHILD;
        CRVLAB_ECU_Project_DWork.s26_is_c3_CRVLAB_ECU_Project =
          CRVLAB_ECU_Project_IN_Stall;

        /* Entry 'Stall': '<S26>:13' */
        CRVLAB_ECU_Project_B.s26_State = 1.0;
      } else {
        switch (CRVLAB_ECU_Project_DWork.s26_is_Cut) {
         case CRVLAB_ECU_Project_IN_FuelCut:
          /* During 'FuelCut': '<S26>:97' */
          if (EngineSpeed_rpm_DataStore() < (EngineSpeedIdle_DataStore())) {
            /* Transition: '<S26>:93' */
            CRVLAB_ECU_Project_DWork.s26_is_Cut = CRVLAB_ECU_Project_IN_Idle;

            /* Entry 'Idle': '<S26>:91' */
            CRVLAB_ECU_Project_B.s26_State = 5.0;
          }
          break;

         case CRVLAB_ECU_Project_IN_Idle:
          /* During 'Idle': '<S26>:91' */
          if (EngineSpeed_rpm_DataStore() >= (EngineSpeedIdle_DataStore())) {
            /* Transition: '<S26>:94' */
            CRVLAB_ECU_Project_DWork.s26_is_Cut = CRVLAB_ECU_Project_IN_FuelCut;

            /* Entry 'FuelCut': '<S26>:97' */
            CRVLAB_ECU_Project_B.s26_State = 6.0;
          }
          break;

         default:
          /* Transition: '<S26>:104' */
          CRVLAB_ECU_Project_DWork.s26_is_Cut = CRVLAB_ECU_Project_IN_FuelCut;

          /* Entry 'FuelCut': '<S26>:97' */
          CRVLAB_ECU_Project_B.s26_State = 6.0;
          break;
        }
      }
      break;

     case CRVLAB_ECU_Project_IN_Run:
      /* During 'Run': '<S26>:98' */
      if (EngineSpeed_rpm_DataStore() < (Stall_rpm_DataStore())) {
        /* Transition: '<S26>:101' */
        CRVLAB_ECU_Project_DWork.s26_is_Run = (uint8_T)
          CRVLAB_ECU_Project_IN_NO_ACTIVE_CHILD;
        CRVLAB_ECU_Project_DWork.s26_is_c3_CRVLAB_ECU_Project =
          CRVLAB_ECU_Project_IN_Stall;

        /* Entry 'Stall': '<S26>:13' */
        CRVLAB_ECU_Project_B.s26_State = 1.0;
      } else if (Pedal_Pct_DataStore() < (PedalLow_DataStore())) {
        /* Transition: '<S26>:105' */
        CRVLAB_ECU_Project_DWork.s26_is_Run = (uint8_T)
          CRVLAB_ECU_Project_IN_NO_ACTIVE_CHILD;
        CRVLAB_ECU_Project_DWork.s26_is_c3_CRVLAB_ECU_Project =
          CRVLAB_ECU_Project_IN_Cut;

        /* Transition: '<S26>:104' */
        CRVLAB_ECU_Project_DWork.s26_is_Cut = CRVLAB_ECU_Project_IN_FuelCut;

        /* Entry 'FuelCut': '<S26>:97' */
        CRVLAB_ECU_Project_B.s26_State = 6.0;
      } else {
        switch (CRVLAB_ECU_Project_DWork.s26_is_Run) {
         case CRVLAB_ECU_Project_IN_RunDDF:
          /* During 'RunDDF': '<S26>:26' */
          if ((ECT_C_DataStore() < (ECTset_DataStore())) || ((real_T)
               rtb_LogicalOperator < 0.5) || (EngineSpeed_rpm_DataStore() >
               (ddf2diesel_rpm_DataStore()))) {
            /* Transition: '<S26>:25' */
            CRVLAB_ECU_Project_DWork.s26_is_Run =
              CRVLAB_ECU_Project_IN_RunDiesel;

            /* Entry 'RunDiesel': '<S26>:10' */
            CRVLAB_ECU_Project_B.s26_State = 3.0;
          }
          break;

         case CRVLAB_ECU_Project_IN_RunDiesel:
          /* During 'RunDiesel': '<S26>:10' */
          if ((ECT_C_DataStore() >= (ECTset_DataStore())) && ((real_T)
               rtb_LogicalOperator >= 0.5) && (EngineSpeed_rpm_DataStore() <=
               (diesel2ddf_rpm_DataStore()))) {
            /* Transition: '<S26>:21' */
            CRVLAB_ECU_Project_DWork.s26_is_Run = CRVLAB_ECU_Project_IN_RunDDF;

            /* Entry 'RunDDF': '<S26>:26' */
            CRVLAB_ECU_Project_B.s26_State = 4.0;
          }
          break;

         default:
          /* Transition: '<S26>:102' */
          CRVLAB_ECU_Project_DWork.s26_is_Run = CRVLAB_ECU_Project_IN_RunDiesel;

          /* Entry 'RunDiesel': '<S26>:10' */
          CRVLAB_ECU_Project_B.s26_State = 3.0;
          break;
        }
      }
      break;

     case CRVLAB_ECU_Project_IN_Stall:
      /* During 'Stall': '<S26>:13' */
      if (EngineSpeed_rpm_DataStore() > (Stall_rpm_DataStore())) {
        /* Transition: '<S26>:15' */
        CRVLAB_ECU_Project_DWork.s26_is_c3_CRVLAB_ECU_Project =
          CRVLAB_ECU_Project_IN_Crank;

        /* Entry 'Crank': '<S26>:11' */
        CRVLAB_ECU_Project_B.s26_State = 2.0;
      }
      break;

     default:
      /* Transition: '<S26>:14' */
      CRVLAB_ECU_Project_DWork.s26_is_c3_CRVLAB_ECU_Project =
        CRVLAB_ECU_Project_IN_Stall;

      /* Entry 'Stall': '<S26>:13' */
      CRVLAB_ECU_Project_B.s26_State = 1.0;
      break;
    }
  }

  /* End of Stateflow: '<S3>/Chart1' */

  /* DataTypeConversion: '<S3>/Data Type Conversion' */
  if (rtIsNaN(CRVLAB_ECU_Project_B.s26_State) || rtIsInf
      (CRVLAB_ECU_Project_B.s26_State)) {
    rtb_DataTypeConversion_n = 0.0;
  } else {
    rtb_DataTypeConversion_n = fmod(floor(CRVLAB_ECU_Project_B.s26_State), 256.0);
  }

  rtb_motohawk_data_read1_e = (uint8_T)(rtb_DataTypeConversion_n < 0.0 ?
    (uint8_T)(int32_T)(int8_T)-(int8_T)(uint8_T)-rtb_DataTypeConversion_n :
    (uint8_T)rtb_DataTypeConversion_n);

  /* End of DataTypeConversion: '<S3>/Data Type Conversion' */

  /* S-Function (motohawk_sfun_data_write): '<S3>/motohawk_data_write' */
  /* Write to Data Storage as scalar: State */
  {
    State_DataStore() = rtb_motohawk_data_read1_e;
  }

  /* S-Function (motohawk_sfun_data_read): '<S27>/motohawk_data_read1' */
  rtb_motohawk_data_read1_b = EngineSpeed_rpm_DataStore();

  /* S-Function (motohawk_sfun_data_read): '<S27>/motohawk_data_read2' */
  rtb_motohawk_data_read2 = Pedal_Pct_DataStore();

  /* S-Function (motohawk_sfun_data_read): '<S27>/motohawk_data_read4' */
  rtb_motohawk_data_read4_b = AC_On_DataStore();

  /* RelationalOperator: '<S27>/Relational Operator' incorporates:
   *  Constant: '<S27>/Constant6'
   *  S-Function (motohawk_sfun_data_read): '<S27>/motohawk_data_read3'
   */
  rtb_LogicalOperator = (50.0 < ECT_C_DataStore());

  /* Logic: '<S27>/Logical Operator' incorporates:
   *  Logic: '<S27>/Logical Operator2'
   *  S-Function (motohawk_sfun_data_read): '<S27>/motohawk_data_read5'
   */
  rtb_LogicalOperator = (rtb_LogicalOperator && (!Normal_On_DataStore()));

  /* MultiPortSwitch: '<S27>/MultiSwitch' incorporates:
   *  Constant: '<S27>/Constant'
   *  Constant: '<S27>/Constant1'
   *  Constant: '<S27>/Constant2'
   *  S-Function (motohawk_sfun_data_read): '<S27>/motohawk_data_read'
   *  S-Function (motohawk_sfun_data_read): '<S27>/motohawk_data_read1'
   *  S-Function (motohawk_sfun_data_read): '<S27>/motohawk_data_read2'
   *  S-Function (motohawk_sfun_interpolation_2d): '<S27>/motohawk_interpolation_2d1'
   *  S-Function (motohawk_sfun_interpolation_2d): '<S27>/motohawk_interpolation_2d2'
   *  S-Function (motohawk_sfun_prelookup): '<S27>/motohawk_prelookup1'
   *  S-Function (motohawk_sfun_prelookup): '<S27>/motohawk_prelookup2'
   *  S-Function (motohawk_sfun_prelookup): '<S27>/motohawk_prelookup3'
   *  S-Function (motohawk_sfun_prelookup): '<S27>/motohawk_prelookup4'
   */
  switch (((uint8_T)State_DataStore())) {
   case 1:
    rtb_Sum5 = 0.0;
    break;

   case 2:
    rtb_Sum5 = 0.0;
    break;

   case 3:
    /* S-Function Block: <S27>/motohawk_prelookup2 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (Pedal_pct_for_CNGFueling_ms_in_RunDieselIn_DataStore()) =
        rtb_motohawk_data_read2;
      (Pedal_pct_for_CNGFueling_ms_in_RunDieselIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read2,
        (Pedal_pct_for_CNGFueling_ms_in_RunDieselIdxArr_DataStore()), 10,
        (Pedal_pct_for_CNGFueling_ms_in_RunDieselIdx_DataStore()));
      rtb_motohawk_prelookup2_f =
        (Pedal_pct_for_CNGFueling_ms_in_RunDieselIdx_DataStore());
    }

    /* S-Function Block: <S27>/motohawk_prelookup3 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_CNGFueling_ms_in_RunDieselIn_DataStore()) =
        rtb_motohawk_data_read1_b;
      (EngineSpeed_rpm_for_CNGFueling_ms_in_RunDieselIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_b,
        (EngineSpeed_rpm_for_CNGFueling_ms_in_RunDieselIdxArr_DataStore()), 8,
        (EngineSpeed_rpm_for_CNGFueling_ms_in_RunDieselIdx_DataStore()));
      rtb_motohawk_prelookup3_j3 =
        (EngineSpeed_rpm_for_CNGFueling_ms_in_RunDieselIdx_DataStore());
    }

    /* S-Function Block: <S27>/motohawk_interpolation_2d1 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d1_d = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup3_j3, rtb_motohawk_prelookup2_f, (real_T *)
         ((CNGFueling_ms_in_RunDieselMap_DataStore())), 8, 10);
      (CNGFueling_ms_in_RunDiesel_DataStore()) =
        rtb_motohawk_interpolation_2d1_d;
    }

    rtb_Sum5 = rtb_motohawk_interpolation_2d1_d;
    break;

   case 4:
    /* S-Function Block: <S27>/motohawk_prelookup1 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (Pedal_pct_for_CNGFueling_ms_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read2;
      (Pedal_pct_for_CNGFueling_ms_in_RunDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read2,
        (Pedal_pct_for_CNGFueling_ms_in_RunDDFIdxArr_DataStore()), 10,
        (Pedal_pct_for_CNGFueling_ms_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup1_l =
        (Pedal_pct_for_CNGFueling_ms_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S27>/motohawk_prelookup4 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_CNGFueling_ms_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read1_b;
      (EngineSpeed_rpm_for_CNGFueling_ms_in_RunDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_b,
        (EngineSpeed_rpm_for_CNGFueling_ms_in_RunDDFIdxArr_DataStore()), 8,
        (EngineSpeed_rpm_for_CNGFueling_ms_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup4_p =
        (EngineSpeed_rpm_for_CNGFueling_ms_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S27>/motohawk_interpolation_2d2 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d2_kp = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup4_p, rtb_motohawk_prelookup1_l, (real_T *)
         ((CNGFueling_ms_in_RunDDFMap_DataStore())), 8, 10);
      (CNGFueling_ms_in_RunDDF_DataStore()) = rtb_motohawk_interpolation_2d2_kp;
    }

    rtb_Sum5 = rtb_motohawk_interpolation_2d2_kp;
    break;

   case 5:
    /* MultiPortSwitch: '<S39>/Multiport Switch' incorporates:
     *  Constant: '<S39>/Constant'
     *  Product: '<S39>/Product'
     *  S-Function (motohawk_sfun_calibration): '<S39>/motohawk_calibration1'
     *  S-Function (motohawk_sfun_calibration): '<S39>/motohawk_calibration2'
     *  S-Function (motohawk_sfun_calibration): '<S39>/motohawk_calibration3'
     *  S-Function (motohawk_sfun_calibration): '<S39>/motohawk_calibration4'
     *  Sum: '<S39>/Add'
     */
    switch (rtb_motohawk_data_read4_b * 2 + rtb_LogicalOperator) {
     case 0:
      rtb_Sum5 = (CNGFuelingIdleACOffDDFOff_ms_DataStore());
      break;

     case 1:
      rtb_Sum5 = (CNGFuelingIdleACOffDDFOn_ms_DataStore());
      break;

     case 2:
      rtb_Sum5 = (CNGFuelingIdleACOnDDFOff_ms_DataStore());
      break;

     case 3:
      rtb_Sum5 = (CNGFuelingIdleACOnDDFOn_ms_DataStore());
      break;

     default:
      rtb_Sum5 = (CNGFuelingIdleACOffDDFOff_ms_DataStore());
      break;
    }

    /* End of MultiPortSwitch: '<S39>/Multiport Switch' */
    break;

   default:
    rtb_Sum5 = 0.0;
    break;
  }

  /* End of MultiPortSwitch: '<S27>/MultiSwitch' */

  /* Product: '<S27>/Divide' incorporates:
   *  Constant: '<S27>/Constant4'
   *  Constant: '<S27>/Constant5'
   *  Product: '<S27>/Product'
   *  Product: '<S27>/Product1'
   *  S-Function (motohawk_sfun_data_read): '<S27>/motohawk_data_read1'
   */
  rtb_DataTypeConversion_n = rtb_Sum5 * rtb_motohawk_data_read1_b * 360.0 /
    60000.0;

  /* MultiPortSwitch: '<S27>/MultiSwitch1' incorporates:
   *  Constant: '<S27>/Constant7'
   *  Constant: '<S27>/Constant8'
   *  Constant: '<S27>/Constant9'
   *  S-Function (motohawk_sfun_data_read): '<S27>/motohawk_data_read'
   *  S-Function (motohawk_sfun_data_read): '<S27>/motohawk_data_read1'
   *  S-Function (motohawk_sfun_data_read): '<S27>/motohawk_data_read2'
   *  S-Function (motohawk_sfun_interpolation_2d): '<S27>/motohawk_interpolation_2d3'
   *  S-Function (motohawk_sfun_interpolation_2d): '<S27>/motohawk_interpolation_2d4'
   *  S-Function (motohawk_sfun_prelookup): '<S27>/motohawk_prelookup5'
   *  S-Function (motohawk_sfun_prelookup): '<S27>/motohawk_prelookup6'
   *  S-Function (motohawk_sfun_prelookup): '<S27>/motohawk_prelookup7'
   *  S-Function (motohawk_sfun_prelookup): '<S27>/motohawk_prelookup8'
   */
  switch (((uint8_T)State_DataStore())) {
   case 1:
    rtb_Gain5 = 0.0;
    break;

   case 2:
    rtb_Gain5 = 0.0;
    break;

   case 3:
    /* S-Function Block: <S27>/motohawk_prelookup6 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (Pedal_pct_for_CNGEOI_degBTDC_in_RunDieselIn_DataStore()) =
        rtb_motohawk_data_read2;
      (Pedal_pct_for_CNGEOI_degBTDC_in_RunDieselIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read2,
        (Pedal_pct_for_CNGEOI_degBTDC_in_RunDieselIdxArr_DataStore()), 10,
        (Pedal_pct_for_CNGEOI_degBTDC_in_RunDieselIdx_DataStore()));
      rtb_motohawk_prelookup6_n =
        (Pedal_pct_for_CNGEOI_degBTDC_in_RunDieselIdx_DataStore());
    }

    /* S-Function Block: <S27>/motohawk_prelookup7 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_CNGEOI_degBTDC_in_RunDieselIn_DataStore()) =
        rtb_motohawk_data_read1_b;
      (EngineSpeed_rpm_for_CNGEOI_degBTDC_in_RunDieselIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_b,
        (EngineSpeed_rpm_for_CNGEOI_degBTDC_in_RunDieselIdxArr_DataStore()), 8,
                              
        (EngineSpeed_rpm_for_CNGEOI_degBTDC_in_RunDieselIdx_DataStore()));
      rtb_motohawk_prelookup7_d =
        (EngineSpeed_rpm_for_CNGEOI_degBTDC_in_RunDieselIdx_DataStore());
    }

    /* S-Function Block: <S27>/motohawk_interpolation_2d3 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d3_dh = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup7_d, rtb_motohawk_prelookup6_n, (real_T *)
         ((CNGEOI_degBTDC_in_RunDieselMap_DataStore())), 8, 10);
      (CNGEOI_degBTDC_in_RunDiesel_DataStore()) =
        rtb_motohawk_interpolation_2d3_dh;
    }

    rtb_Gain5 = rtb_motohawk_interpolation_2d3_dh;
    break;

   case 4:
    /* S-Function Block: <S27>/motohawk_prelookup5 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (Pedal_pct_for_CNGEOI_degBTDC_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read2;
      (Pedal_pct_for_CNGEOI_degBTDC_in_RunDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read2,
        (Pedal_pct_for_CNGEOI_degBTDC_in_RunDDFIdxArr_DataStore()), 10,
        (Pedal_pct_for_CNGEOI_degBTDC_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup5_d =
        (Pedal_pct_for_CNGEOI_degBTDC_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S27>/motohawk_prelookup8 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_CNGEOI_degBTDC_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read1_b;
      (EngineSpeed_rpm_for_CNGEOI_degBTDC_in_RunDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_b,
        (EngineSpeed_rpm_for_CNGEOI_degBTDC_in_RunDDFIdxArr_DataStore()), 8,
        (EngineSpeed_rpm_for_CNGEOI_degBTDC_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup8 =
        (EngineSpeed_rpm_for_CNGEOI_degBTDC_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S27>/motohawk_interpolation_2d4 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d4 = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup8, rtb_motohawk_prelookup5_d, (real_T *)
         ((CNGEOI_degBTDC_in_RunDDFMap_DataStore())), 8, 10);
      (CNGEOI_degBTDC_in_RunDDF_DataStore()) = rtb_motohawk_interpolation_2d4;
    }

    rtb_Gain5 = rtb_motohawk_interpolation_2d4;
    break;

   case 5:
    /* MultiPortSwitch: '<S40>/Multiport Switch' incorporates:
     *  Constant: '<S40>/Constant'
     *  Product: '<S40>/Product'
     *  S-Function (motohawk_sfun_calibration): '<S40>/motohawk_calibration1'
     *  S-Function (motohawk_sfun_calibration): '<S40>/motohawk_calibration2'
     *  S-Function (motohawk_sfun_calibration): '<S40>/motohawk_calibration3'
     *  S-Function (motohawk_sfun_calibration): '<S40>/motohawk_calibration4'
     *  Sum: '<S40>/Add'
     */
    switch (rtb_motohawk_data_read4_b * 2 + rtb_LogicalOperator) {
     case 0:
      rtb_Gain5 = (CNGEOIIdleACOffDDFOff_degBTDC_DataStore());
      break;

     case 1:
      rtb_Gain5 = (CNGEOIIdleACOffDDFOn_degBTDC_DataStore());
      break;

     case 2:
      rtb_Gain5 = (CNGEOIIdleACOnDDFOff_degBTDC_DataStore());
      break;

     case 3:
      rtb_Gain5 = (CNGEOIIdleACOnDDFOn_degBTDC_DataStore());
      break;

     default:
      rtb_Gain5 = (CNGEOIIdleACOffDDFOff_degBTDC_DataStore());
      break;
    }

    /* End of MultiPortSwitch: '<S40>/Multiport Switch' */
    break;

   default:
    rtb_Gain5 = 0.0;
    break;
  }

  /* End of MultiPortSwitch: '<S27>/MultiSwitch1' */

  /* Sum: '<S27>/Add' */
  rtb_Add = rtb_DataTypeConversion_n + rtb_Gain5;

  /* S-Function (motohawk_sfun_data_write): '<S27>/motohawk_data_write' */
  /* Write to Data Storage as scalar: CNGSOI_degBTDC */
  {
    CNGSOI_degBTDC_DataStore() = rtb_Add;
  }

  /* S-Function (motohawk_sfun_data_write): '<S27>/motohawk_data_write1' */
  /* Write to Data Storage as scalar: CNGFueling_ms */
  {
    CNGFueling_ms_DataStore() = rtb_Sum5;
  }

  /* S-Function (motohawk_sfun_data_read): '<S28>/motohawk_data_read1' */
  rtb_motohawk_data_read1_k = EngineSpeed_rpm_DataStore();

  /* S-Function (motohawk_sfun_data_read): '<S28>/motohawk_data_read2' */
  rtb_motohawk_data_read2_p = Pedal_Pct_DataStore();

  /* MultiPortSwitch: '<S28>/MultiSwitch' incorporates:
   *  Constant: '<S28>/Constant'
   *  Constant: '<S28>/Constant1'
   *  Constant: '<S28>/Constant2'
   *  Constant: '<S28>/Constant3'
   *  S-Function (motohawk_sfun_data_read): '<S28>/motohawk_data_read'
   *  S-Function (motohawk_sfun_data_read): '<S28>/motohawk_data_read1'
   *  S-Function (motohawk_sfun_data_read): '<S28>/motohawk_data_read2'
   *  S-Function (motohawk_sfun_interpolation_2d): '<S28>/motohawk_interpolation_2d1'
   *  S-Function (motohawk_sfun_interpolation_2d): '<S28>/motohawk_interpolation_2d2'
   *  S-Function (motohawk_sfun_prelookup): '<S28>/motohawk_prelookup1'
   *  S-Function (motohawk_sfun_prelookup): '<S28>/motohawk_prelookup2'
   *  S-Function (motohawk_sfun_prelookup): '<S28>/motohawk_prelookup3'
   *  S-Function (motohawk_sfun_prelookup): '<S28>/motohawk_prelookup4'
   */
  switch (((uint8_T)State_DataStore())) {
   case 1:
    rtb_DataTypeConversion_n = 0.0;
    break;

   case 2:
    rtb_DataTypeConversion_n = 0.0;
    break;

   case 3:
    /* S-Function Block: <S28>/motohawk_prelookup2 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (Pedal_pct_for_EGR_Pct_in_RunDieselIn_DataStore()) =
        rtb_motohawk_data_read2_p;
      (Pedal_pct_for_EGR_Pct_in_RunDieselIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read2_p,
        (Pedal_pct_for_EGR_Pct_in_RunDieselIdxArr_DataStore()), 21,
        (Pedal_pct_for_EGR_Pct_in_RunDieselIdx_DataStore()));
      rtb_motohawk_prelookup2_kp =
        (Pedal_pct_for_EGR_Pct_in_RunDieselIdx_DataStore());
    }

    /* S-Function Block: <S28>/motohawk_prelookup3 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_EGR_Pct_in_RunDieselIn_DataStore()) =
        rtb_motohawk_data_read1_k;
      (EngineSpeed_rpm_for_EGR_Pct_in_RunDieselIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_k,
        (EngineSpeed_rpm_for_EGR_Pct_in_RunDieselIdxArr_DataStore()), 20,
        (EngineSpeed_rpm_for_EGR_Pct_in_RunDieselIdx_DataStore()));
      rtb_motohawk_prelookup3_jm =
        (EngineSpeed_rpm_for_EGR_Pct_in_RunDieselIdx_DataStore());
    }

    /* S-Function Block: <S28>/motohawk_interpolation_2d1 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d1_ja = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup3_jm, rtb_motohawk_prelookup2_kp, (real_T *)
         ((EGR_Pct_in_RunDieselMap_DataStore())), 20, 21);
      (EGR_Pct_in_RunDiesel_DataStore()) = rtb_motohawk_interpolation_2d1_ja;
    }

    rtb_DataTypeConversion_n = rtb_motohawk_interpolation_2d1_ja;
    break;

   case 4:
    /* S-Function Block: <S28>/motohawk_prelookup1 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (Pedal_pct_for_EGR_Pct_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read2_p;
      (Pedal_pct_for_EGR_Pct_in_RunDDFIdx_DataStore()) = TablePrelookup_real_T
        (rtb_motohawk_data_read2_p,
         (Pedal_pct_for_EGR_Pct_in_RunDDFIdxArr_DataStore()), 21,
         (Pedal_pct_for_EGR_Pct_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup1_d2 = (Pedal_pct_for_EGR_Pct_in_RunDDFIdx_DataStore
                                    ());
    }

    /* S-Function Block: <S28>/motohawk_prelookup4 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_EGR_Pct_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read1_k;
      (EngineSpeed_rpm_for_EGR_Pct_in_RunDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_k,
        (EngineSpeed_rpm_for_EGR_Pct_in_RunDDFIdxArr_DataStore()), 20,
        (EngineSpeed_rpm_for_EGR_Pct_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup4_o =
        (EngineSpeed_rpm_for_EGR_Pct_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S28>/motohawk_interpolation_2d2 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d2_k = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup4_o, rtb_motohawk_prelookup1_d2, (real_T *)
         ((EGR_Pct_in_RunDDFMap_DataStore())), 20, 21);
      (EGR_Pct_in_RunDDF_DataStore()) = rtb_motohawk_interpolation_2d2_k;
    }

    rtb_DataTypeConversion_n = rtb_motohawk_interpolation_2d2_k;
    break;

   case 5:
    rtb_DataTypeConversion_n = 0.0;
    break;

   default:
    rtb_DataTypeConversion_n = 50.0;
    break;
  }

  /* End of MultiPortSwitch: '<S28>/MultiSwitch' */
  /* If: '<S41>/If' incorporates:
   *  Inport: '<S42>/In1'
   *  Inport: '<S43>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S41>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S41>/override_enable'
   */
  if ((EGRSPOverride_Pct_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S41>/NewValue' incorporates:
     *  ActionPort: '<S42>/Action Port'
     */
    rtb_Merge = (EGRSPOverride_Pct_new_DataStore());

    /* End of Outputs for SubSystem: '<S41>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S41>/OldValue' incorporates:
     *  ActionPort: '<S43>/Action Port'
     */
    rtb_Merge = rtb_DataTypeConversion_n;

    /* End of Outputs for SubSystem: '<S41>/OldValue' */
  }

  /* End of If: '<S41>/If' */

  /* S-Function (motohawk_sfun_data_write): '<S28>/motohawk_data_write' */
  /* Write to Data Storage as scalar: EGRSP_Pct */
  {
    EGRSP_Pct_DataStore() = rtb_Merge;
  }

  /* Switch: '<S29>/Switch' incorporates:
   *  Constant: '<S29>/Constant'
   *  S-Function (motohawk_sfun_calibration): '<S29>/motohawk_calibration2'
   *  S-Function (motohawk_sfun_data_read): '<S29>/motohawk_data_read'
   *  Sum: '<S29>/Add'
   */
  if (AC_On_DataStore()) {
    rtb_Gain5 = 50.0 + (real_T)(IdleSpeedSPACOff_rpm_DataStore());
  } else {
    rtb_Gain5 = (IdleSpeedSPACOff_rpm_DataStore());
  }

  /* End of Switch: '<S29>/Switch' */

  /* S-Function (motohawk_sfun_data_write): '<S29>/motohawk_data_write' */
  /* Write to Data Storage as scalar: IdleSpeedSP_rpm */
  {
    IdleSpeedSP_rpm_DataStore() = rtb_Gain5;
  }

  /* S-Function (motohawk_sfun_data_read): '<S30>/motohawk_data_read1' */
  rtb_motohawk_data_read1_bt = EngineSpeed_rpm_DataStore();

  /* S-Function (motohawk_sfun_data_read): '<S30>/motohawk_data_read2' */
  rtb_motohawk_data_read2_j = Pedal_Pct_DataStore();

  /* MultiPortSwitch: '<S30>/MultiSwitch' incorporates:
   *  Constant: '<S30>/Constant'
   *  Constant: '<S30>/Constant1'
   *  Constant: '<S30>/Constant2'
   *  Constant: '<S30>/Constant3'
   *  S-Function (motohawk_sfun_data_read): '<S30>/motohawk_data_read'
   *  S-Function (motohawk_sfun_data_read): '<S30>/motohawk_data_read1'
   *  S-Function (motohawk_sfun_data_read): '<S30>/motohawk_data_read2'
   *  S-Function (motohawk_sfun_interpolation_2d): '<S30>/motohawk_interpolation_2d1'
   *  S-Function (motohawk_sfun_interpolation_2d): '<S30>/motohawk_interpolation_2d2'
   *  S-Function (motohawk_sfun_prelookup): '<S30>/motohawk_prelookup1'
   *  S-Function (motohawk_sfun_prelookup): '<S30>/motohawk_prelookup2'
   *  S-Function (motohawk_sfun_prelookup): '<S30>/motohawk_prelookup3'
   *  S-Function (motohawk_sfun_prelookup): '<S30>/motohawk_prelookup4'
   */
  switch (((uint8_T)State_DataStore())) {
   case 1:
    rtb_DataTypeConversion_n = 10.0;
    break;

   case 2:
    rtb_DataTypeConversion_n = 10.0;
    break;

   case 3:
    /* S-Function Block: <S30>/motohawk_prelookup2 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (Pedal_pct_for_MAFsp_g_s_in_RunDieselIn_DataStore()) =
        rtb_motohawk_data_read2_j;
      (Pedal_pct_for_MAFsp_g_s_in_RunDieselIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read2_j,
        (Pedal_pct_for_MAFsp_g_s_in_RunDieselIdxArr_DataStore()), 21,
        (Pedal_pct_for_MAFsp_g_s_in_RunDieselIdx_DataStore()));
      rtb_motohawk_prelookup2_b =
        (Pedal_pct_for_MAFsp_g_s_in_RunDieselIdx_DataStore());
    }

    /* S-Function Block: <S30>/motohawk_prelookup3 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_MAFsp_g_s_in_RunDieselIn_DataStore()) =
        rtb_motohawk_data_read1_bt;
      (EngineSpeed_rpm_for_MAFsp_g_s_in_RunDieselIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_bt,
        (EngineSpeed_rpm_for_MAFsp_g_s_in_RunDieselIdxArr_DataStore()), 20,
        (EngineSpeed_rpm_for_MAFsp_g_s_in_RunDieselIdx_DataStore()));
      rtb_motohawk_prelookup3_f =
        (EngineSpeed_rpm_for_MAFsp_g_s_in_RunDieselIdx_DataStore());
    }

    /* S-Function Block: <S30>/motohawk_interpolation_2d1 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d1_h = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup3_f, rtb_motohawk_prelookup2_b, (real_T *)
         ((MAFsp_g_s_in_RunDieselMap_DataStore())), 20, 21);
      (MAFsp_g_s_in_RunDiesel_DataStore()) = rtb_motohawk_interpolation_2d1_h;
    }

    rtb_DataTypeConversion_n = rtb_motohawk_interpolation_2d1_h;
    break;

   case 4:
    /* S-Function Block: <S30>/motohawk_prelookup1 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (Pedal_pct_for_MAFsp_g_s_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read2_j;
      (Pedal_pct_for_MAFsp_g_s_in_RunDDFIdx_DataStore()) = TablePrelookup_real_T
        (rtb_motohawk_data_read2_j,
         (Pedal_pct_for_MAFsp_g_s_in_RunDDFIdxArr_DataStore()), 21,
         (Pedal_pct_for_MAFsp_g_s_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup1_o =
        (Pedal_pct_for_MAFsp_g_s_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S30>/motohawk_prelookup4 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_MAFsp_g_s_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read1_bt;
      (EngineSpeed_rpm_for_MAFsp_g_s_in_RunDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_bt,
        (EngineSpeed_rpm_for_MAFsp_g_s_in_RunDDFIdxArr_DataStore()), 20,
        (EngineSpeed_rpm_for_MAFsp_g_s_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup4_i =
        (EngineSpeed_rpm_for_MAFsp_g_s_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S30>/motohawk_interpolation_2d2 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d2_dz = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup4_i, rtb_motohawk_prelookup1_o, (real_T *)
         ((MAFsp_g_s_in_RunDDFMap_DataStore())), 20, 21);
      (MAFsp_g_s_in_RunDDF_DataStore()) = rtb_motohawk_interpolation_2d2_dz;
    }

    rtb_DataTypeConversion_n = rtb_motohawk_interpolation_2d2_dz;
    break;

   case 5:
    rtb_DataTypeConversion_n = 10.0;
    break;

   default:
    rtb_DataTypeConversion_n = 10.0;
    break;
  }

  /* End of MultiPortSwitch: '<S30>/MultiSwitch' */
  /* If: '<S44>/If' incorporates:
   *  Inport: '<S45>/In1'
   *  Inport: '<S46>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S44>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S44>/override_enable'
   */
  if ((MAFSPOverride_g_s_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S44>/NewValue' incorporates:
     *  ActionPort: '<S45>/Action Port'
     */
    rtb_Merge_p = (MAFSPOverride_g_s_new_DataStore());

    /* End of Outputs for SubSystem: '<S44>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S44>/OldValue' incorporates:
     *  ActionPort: '<S46>/Action Port'
     */
    rtb_Merge_p = rtb_DataTypeConversion_n;

    /* End of Outputs for SubSystem: '<S44>/OldValue' */
  }

  /* End of If: '<S44>/If' */

  /* S-Function (motohawk_sfun_data_write): '<S30>/motohawk_data_write' */
  /* Write to Data Storage as scalar: MAFSP_g_s */
  {
    MAFSP_g_s_DataStore() = rtb_Merge_p;
  }

  /* S-Function (motohawk_sfun_data_read): '<S31>/motohawk_data_read1' */
  rtb_motohawk_data_read1_d = EngineSpeed_rpm_DataStore();

  /* S-Function (motohawk_sfun_data_read): '<S31>/motohawk_data_read2' */
  rtb_motohawk_data_read2_k = Pedal_Pct_DataStore();

  /* MultiPortSwitch: '<S31>/MultiSwitch' incorporates:
   *  Constant: '<S31>/Constant'
   *  Constant: '<S31>/Constant1'
   *  Constant: '<S31>/Constant2'
   *  S-Function (motohawk_sfun_data_read): '<S31>/motohawk_data_read'
   *  S-Function (motohawk_sfun_data_read): '<S31>/motohawk_data_read1'
   *  S-Function (motohawk_sfun_data_read): '<S31>/motohawk_data_read2'
   *  S-Function (motohawk_sfun_data_read): '<S31>/motohawk_data_read6'
   *  S-Function (motohawk_sfun_interpolation_2d): '<S31>/motohawk_interpolation_2d1'
   *  S-Function (motohawk_sfun_interpolation_2d): '<S31>/motohawk_interpolation_2d2'
   *  S-Function (motohawk_sfun_interpolation_2d): '<S31>/motohawk_interpolation_2d3'
   *  S-Function (motohawk_sfun_prelookup): '<S31>/motohawk_prelookup1'
   *  S-Function (motohawk_sfun_prelookup): '<S31>/motohawk_prelookup2'
   *  S-Function (motohawk_sfun_prelookup): '<S31>/motohawk_prelookup3'
   *  S-Function (motohawk_sfun_prelookup): '<S31>/motohawk_prelookup4'
   *  S-Function (motohawk_sfun_prelookup): '<S31>/motohawk_prelookup5'
   *  S-Function (motohawk_sfun_prelookup): '<S31>/motohawk_prelookup6'
   */
  switch (((uint8_T)State_DataStore())) {
   case 1:
    rtb_Gain5 = 0.0;
    break;

   case 2:
    /* S-Function (motohawk_sfun_data_read): '<S31>/motohawk_data_read6' */
    rtb_motohawk_data_read6_n = RailP_MPa_DataStore();

    /* S-Function Block: <S31>/motohawk_prelookup5 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (railpressure_in_totalfuel_railpressure_fuelpw_map2In_DataStore()) =
        rtb_motohawk_data_read6_n;
      (railpressure_in_totalfuel_railpressure_fuelpw_map2Idx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read6_n,
        (railpressure_in_totalfuel_railpressure_fuelpw_map2IdxArr_DataStore()),
        12, (railpressure_in_totalfuel_railpressure_fuelpw_map2Idx_DataStore()));
      rtb_motohawk_prelookup5_m =
        (railpressure_in_totalfuel_railpressure_fuelpw_map2Idx_DataStore());
    }

    /* S-Function Block: <S31>/motohawk_prelookup6 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (totalfuel_in_totalfuel_railpressure_fuelpw_map2In_DataStore()) = 30.0;
      (totalfuel_in_totalfuel_railpressure_fuelpw_map2Idx_DataStore()) =
        TablePrelookup_real_T(30.0,
        (totalfuel_in_totalfuel_railpressure_fuelpw_map2IdxArr_DataStore()), 14,
                              
        (totalfuel_in_totalfuel_railpressure_fuelpw_map2Idx_DataStore()));
      rtb_motohawk_prelookup6_e =
        (totalfuel_in_totalfuel_railpressure_fuelpw_map2Idx_DataStore());
    }

    /* S-Function Block: <S31>/motohawk_interpolation_2d3 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d3_d = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup6_e, rtb_motohawk_prelookup5_m, (real_T *)
         ((fuelpw_in_totalfuel_railpressure_fuelpw_map2Map_DataStore())), 14, 12);
      (fuelpw_in_totalfuel_railpressure_fuelpw_map2_DataStore()) =
        rtb_motohawk_interpolation_2d3_d;
    }

    rtb_Gain5 = rtb_motohawk_interpolation_2d3_d;
    break;

   case 3:
    /* S-Function Block: <S31>/motohawk_prelookup2 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (Pedal_pct_for_MainFueling_ms_in_RunDieselIn_DataStore()) =
        rtb_motohawk_data_read2_k;
      (Pedal_pct_for_MainFueling_ms_in_RunDieselIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read2_k,
        (Pedal_pct_for_MainFueling_ms_in_RunDieselIdxArr_DataStore()), 21,
        (Pedal_pct_for_MainFueling_ms_in_RunDieselIdx_DataStore()));
      rtb_motohawk_prelookup2_d =
        (Pedal_pct_for_MainFueling_ms_in_RunDieselIdx_DataStore());
    }

    /* S-Function Block: <S31>/motohawk_prelookup3 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_MainFueling_ms_in_RunDieselIn_DataStore()) =
        rtb_motohawk_data_read1_d;
      (EngineSpeed_rpm_for_MainFueling_ms_in_RunDieselIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_d,
        (EngineSpeed_rpm_for_MainFueling_ms_in_RunDieselIdxArr_DataStore()), 20,
                              
        (EngineSpeed_rpm_for_MainFueling_ms_in_RunDieselIdx_DataStore()));
      rtb_motohawk_prelookup3_a =
        (EngineSpeed_rpm_for_MainFueling_ms_in_RunDieselIdx_DataStore());
    }

    /* S-Function Block: <S31>/motohawk_interpolation_2d1 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d1_c = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup3_a, rtb_motohawk_prelookup2_d, (real_T *)
         ((MainFueling_ms_in_RunDieselMap_DataStore())), 20, 21);
      (MainFueling_ms_in_RunDiesel_DataStore()) =
        rtb_motohawk_interpolation_2d1_c;
    }

    rtb_Gain5 = rtb_motohawk_interpolation_2d1_c;
    break;

   case 4:
    /* S-Function Block: <S31>/motohawk_prelookup1 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (Pedal_pct_for_MainFueling_ms_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read2_k;
      (Pedal_pct_for_MainFueling_ms_in_RunDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read2_k,
        (Pedal_pct_for_MainFueling_ms_in_RunDDFIdxArr_DataStore()), 10,
        (Pedal_pct_for_MainFueling_ms_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup1_eo =
        (Pedal_pct_for_MainFueling_ms_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S31>/motohawk_prelookup4 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_MainFueling_ms_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read1_d;
      (EngineSpeed_rpm_for_MainFueling_ms_in_RunDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_d,
        (EngineSpeed_rpm_for_MainFueling_ms_in_RunDDFIdxArr_DataStore()), 8,
        (EngineSpeed_rpm_for_MainFueling_ms_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup4_dz =
        (EngineSpeed_rpm_for_MainFueling_ms_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S31>/motohawk_interpolation_2d2 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d2_ox = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup4_dz, rtb_motohawk_prelookup1_eo, (real_T *)
         ((MainFueling_ms_in_RunDDFMap_DataStore())), 8, 10);
      (MainFueling_ms_in_RunDDF_DataStore()) = rtb_motohawk_interpolation_2d2_ox;
    }

    rtb_Gain5 = rtb_motohawk_interpolation_2d2_ox;
    break;

   case 5:
    /* Logic: '<S31>/Logical Operator3' incorporates:
     *  S-Function (motohawk_sfun_data_read): '<S31>/motohawk_data_read9'
     */
    rtb_LogicalOperator = !Normal_On_DataStore();

    /* Logic: '<S31>/Logical Operator1' incorporates:
     *  Constant: '<S31>/Constant6'
     *  RelationalOperator: '<S31>/Relational Operator1'
     *  S-Function (motohawk_sfun_data_read): '<S31>/motohawk_data_read7'
     */
    rtb_LogicalOperator = ((50.0 < ECT_C_DataStore()) && rtb_LogicalOperator);

    /* S-Function (motohawk_sfun_data_read): '<S31>/motohawk_data_read8' */
    rtb_motohawk_data_read8_ng = AC_On_DataStore();

    /* MultiPortSwitch: '<S47>/Multiport Switch' incorporates:
     *  Constant: '<S47>/Constant'
     *  Product: '<S47>/Product'
     *  S-Function (motohawk_sfun_calibration): '<S47>/motohawk_calibration1'
     *  S-Function (motohawk_sfun_calibration): '<S47>/motohawk_calibration2'
     *  S-Function (motohawk_sfun_calibration): '<S47>/motohawk_calibration3'
     *  S-Function (motohawk_sfun_calibration): '<S47>/motohawk_calibration4'
     *  Sum: '<S47>/Add'
     */
    switch (rtb_motohawk_data_read8_ng * 2 + rtb_LogicalOperator) {
     case 0:
      rtb_Gain5 = (MainFuelingIdleACOffDDFOff_ms_DataStore());
      break;

     case 1:
      rtb_Gain5 = (MainFuelingIdleACOffDDFOn_ms_DataStore());
      break;

     case 2:
      rtb_Gain5 = (MainFuelingIdleACOnDDFOff_ms_DataStore());
      break;

     case 3:
      rtb_Gain5 = (MainFuelingIdleACOnDDFOn_ms_DataStore());
      break;

     default:
      rtb_Gain5 = (MainFuelingIdleACOffDDFOff_ms_DataStore());
      break;
    }

    /* End of MultiPortSwitch: '<S47>/Multiport Switch' */
    break;

   default:
    rtb_Gain5 = 0.0;
    break;
  }

  /* End of MultiPortSwitch: '<S31>/MultiSwitch' */

  /* S-Function (motohawk_sfun_data_write): '<S31>/motohawk_data_write' */
  /* Write to Data Storage as scalar: MainFueling_ms */
  {
    MainFueling_ms_DataStore() = rtb_Gain5;
  }

  /* S-Function (motohawk_sfun_data_read): '<S32>/motohawk_data_read1' */
  rtb_motohawk_data_read1_l = EngineSpeed_rpm_DataStore();

  /* S-Function (motohawk_sfun_data_read): '<S32>/motohawk_data_read2' */
  rtb_motohawk_data_read2_o = Pedal_Pct_DataStore();

  /* MultiPortSwitch: '<S32>/MultiSwitch' incorporates:
   *  Constant: '<S32>/Constant'
   *  Constant: '<S32>/Constant1'
   *  S-Function (motohawk_sfun_data_read): '<S32>/motohawk_data_read'
   *  S-Function (motohawk_sfun_data_read): '<S32>/motohawk_data_read1'
   *  S-Function (motohawk_sfun_data_read): '<S32>/motohawk_data_read2'
   *  S-Function (motohawk_sfun_interpolation_1d): '<S32>/motohawk_interpolation_1d2'
   *  S-Function (motohawk_sfun_interpolation_2d): '<S32>/motohawk_interpolation_2d1'
   *  S-Function (motohawk_sfun_interpolation_2d): '<S32>/motohawk_interpolation_2d2'
   *  S-Function (motohawk_sfun_prelookup): '<S32>/motohawk_prelookup1'
   *  S-Function (motohawk_sfun_prelookup): '<S32>/motohawk_prelookup2'
   *  S-Function (motohawk_sfun_prelookup): '<S32>/motohawk_prelookup3'
   *  S-Function (motohawk_sfun_prelookup): '<S32>/motohawk_prelookup4'
   *  S-Function (motohawk_sfun_prelookup): '<S32>/motohawk_prelookup6'
   */
  switch (((uint8_T)State_DataStore())) {
   case 1:
    rtb_Gain5 = 0.0;
    break;

   case 2:
    /* S-Function Block: <S32>/motohawk_prelookup6 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_MainSOI_degBTDC_in_CrankIn_DataStore()) =
        rtb_motohawk_data_read1_l;
      (EngineSpeed_rpm_for_MainSOI_degBTDC_in_CrankIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_l,
        (EngineSpeed_rpm_for_MainSOI_degBTDC_in_CrankIdxArr_DataStore()), 8,
        (EngineSpeed_rpm_for_MainSOI_degBTDC_in_CrankIdx_DataStore()));
      rtb_motohawk_prelookup6_f =
        (EngineSpeed_rpm_for_MainSOI_degBTDC_in_CrankIdx_DataStore());
    }

    /* S-Function Block: <S32>/motohawk_interpolation_1d2 */
    {
      extern real_T TableInterpolation1D_real_T(uint16_T idx, real_T *tbl_data,
        uint32_T sz);
      rtb_motohawk_interpolation_1d2 = TableInterpolation1D_real_T
        (rtb_motohawk_prelookup6_f, (real_T *)
         ((MainSOI_degBTDC_in_CrankTbl_DataStore())), 8);
      (MainSOI_degBTDC_in_Crank_DataStore()) = rtb_motohawk_interpolation_1d2;
    }

    rtb_Gain5 = rtb_motohawk_interpolation_1d2;
    break;

   case 3:
    /* S-Function Block: <S32>/motohawk_prelookup2 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (Pedal_pct_for_MainSOI_degBTDC_in_RunDieselIn_DataStore()) =
        rtb_motohawk_data_read2_o;
      (Pedal_pct_for_MainSOI_degBTDC_in_RunDieselIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read2_o,
        (Pedal_pct_for_MainSOI_degBTDC_in_RunDieselIdxArr_DataStore()), 21,
        (Pedal_pct_for_MainSOI_degBTDC_in_RunDieselIdx_DataStore()));
      rtb_motohawk_prelookup2_c3 =
        (Pedal_pct_for_MainSOI_degBTDC_in_RunDieselIdx_DataStore());
    }

    /* S-Function Block: <S32>/motohawk_prelookup3 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_MainSOI_degBTDC_in_RunDieselIn_DataStore()) =
        rtb_motohawk_data_read1_l;
      (EngineSpeed_rpm_for_MainSOI_degBTDC_in_RunDieselIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_l,
        (EngineSpeed_rpm_for_MainSOI_degBTDC_in_RunDieselIdxArr_DataStore()), 20,
                              
        (EngineSpeed_rpm_for_MainSOI_degBTDC_in_RunDieselIdx_DataStore()));
      rtb_motohawk_prelookup3_g =
        (EngineSpeed_rpm_for_MainSOI_degBTDC_in_RunDieselIdx_DataStore());
    }

    /* S-Function Block: <S32>/motohawk_interpolation_2d1 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d1_km = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup3_g, rtb_motohawk_prelookup2_c3, (real_T *)
         ((MainSOI_degBTDC_in_RunDieselMap_DataStore())), 20, 21);
      (MainSOI_degBTDC_in_RunDiesel_DataStore()) =
        rtb_motohawk_interpolation_2d1_km;
    }

    rtb_Gain5 = rtb_motohawk_interpolation_2d1_km;
    break;

   case 4:
    /* S-Function Block: <S32>/motohawk_prelookup1 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (Pedal_pct_for_MainSOI_degBTDC_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read2_o;
      (Pedal_pct_for_MainSOI_degBTDC_in_RunDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read2_o,
        (Pedal_pct_for_MainSOI_degBTDC_in_RunDDFIdxArr_DataStore()), 21,
        (Pedal_pct_for_MainSOI_degBTDC_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup1_kh =
        (Pedal_pct_for_MainSOI_degBTDC_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S32>/motohawk_prelookup4 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_MainSOI_degBTDC_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read1_l;
      (EngineSpeed_rpm_for_MainSOI_degBTDC_in_RunDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_l,
        (EngineSpeed_rpm_for_MainSOI_degBTDC_in_RunDDFIdxArr_DataStore()), 20,
        (EngineSpeed_rpm_for_MainSOI_degBTDC_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup4_h =
        (EngineSpeed_rpm_for_MainSOI_degBTDC_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S32>/motohawk_interpolation_2d2 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d2_h = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup4_h, rtb_motohawk_prelookup1_kh, (real_T *)
         ((MainSOI_degBTDC_in_RunDDFMap_DataStore())), 20, 21);
      (MainSOI_degBTDC_in_RunDDF_DataStore()) = rtb_motohawk_interpolation_2d2_h;
    }

    rtb_Gain5 = rtb_motohawk_interpolation_2d2_h;
    break;

   case 5:
    /* Logic: '<S32>/Logical Operator3' incorporates:
     *  S-Function (motohawk_sfun_data_read): '<S32>/motohawk_data_read7'
     */
    rtb_LogicalOperator = !Normal_On_DataStore();

    /* Logic: '<S32>/Logical Operator1' incorporates:
     *  Constant: '<S32>/Constant6'
     *  RelationalOperator: '<S32>/Relational Operator1'
     *  S-Function (motohawk_sfun_data_read): '<S32>/motohawk_data_read4'
     */
    rtb_LogicalOperator = ((50.0 < ECT_C_DataStore()) && rtb_LogicalOperator);

    /* S-Function (motohawk_sfun_data_read): '<S32>/motohawk_data_read6' */
    rtb_motohawk_data_read6_p = AC_On_DataStore();

    /* MultiPortSwitch: '<S48>/Multiport Switch' incorporates:
     *  Constant: '<S48>/Constant'
     *  Product: '<S48>/Product'
     *  S-Function (motohawk_sfun_calibration): '<S48>/motohawk_calibration1'
     *  S-Function (motohawk_sfun_calibration): '<S48>/motohawk_calibration2'
     *  S-Function (motohawk_sfun_calibration): '<S48>/motohawk_calibration3'
     *  S-Function (motohawk_sfun_calibration): '<S48>/motohawk_calibration4'
     *  Sum: '<S48>/Add'
     */
    switch (rtb_motohawk_data_read6_p * 2 + rtb_LogicalOperator) {
     case 0:
      rtb_Gain5 = (MainSOIIdleACOffDDFOff_degBTDC_DataStore());
      break;

     case 1:
      rtb_Gain5 = (MainSOIIdleACOffDDFOn_degBTDC_DataStore());
      break;

     case 2:
      rtb_Gain5 = (MainSOIIdleACOnDDFOff_degBTDC_DataStore());
      break;

     case 3:
      rtb_Gain5 = (MainSOIIdleACOnDDFOn_degBTDC_DataStore());
      break;

     default:
      rtb_Gain5 = (MainSOIIdleACOffDDFOff_degBTDC_DataStore());
      break;
    }

    /* End of MultiPortSwitch: '<S48>/Multiport Switch' */
    break;

   default:
    rtb_Gain5 = 0.0;
    break;
  }

  /* End of MultiPortSwitch: '<S32>/MultiSwitch' */

  /* S-Function (motohawk_sfun_data_write): '<S32>/motohawk_data_write' */
  /* Write to Data Storage as scalar: MainSOI_degBTDC */
  {
    MainSOI_degBTDC_DataStore() = rtb_Gain5;
  }

  /* S-Function (motohawk_sfun_data_read): '<S33>/motohawk_data_read1' */
  rtb_motohawk_data_read1_j = EngineSpeed_rpm_DataStore();

  /* S-Function (motohawk_sfun_data_read): '<S33>/motohawk_data_read2' */
  rtb_motohawk_data_read2_f = Pedal_Pct_DataStore();

  /* MultiPortSwitch: '<S33>/MultiSwitch' incorporates:
   *  Constant: '<S33>/Constant'
   *  Constant: '<S33>/Constant2'
   *  Constant: '<S33>/Constant4'
   *  S-Function (motohawk_sfun_data_read): '<S33>/motohawk_data_read'
   *  S-Function (motohawk_sfun_data_read): '<S33>/motohawk_data_read1'
   *  S-Function (motohawk_sfun_data_read): '<S33>/motohawk_data_read2'
   *  S-Function (motohawk_sfun_data_read): '<S33>/motohawk_data_read6'
   *  S-Function (motohawk_sfun_interpolation_2d): '<S33>/motohawk_interpolation_2d2'
   *  S-Function (motohawk_sfun_interpolation_2d): '<S33>/motohawk_interpolation_2d3'
   *  S-Function (motohawk_sfun_prelookup): '<S33>/motohawk_prelookup1'
   *  S-Function (motohawk_sfun_prelookup): '<S33>/motohawk_prelookup4'
   *  S-Function (motohawk_sfun_prelookup): '<S33>/motohawk_prelookup5'
   *  S-Function (motohawk_sfun_prelookup): '<S33>/motohawk_prelookup6'
   */
  switch (((uint8_T)State_DataStore())) {
   case 1:
    rtb_Gain5 = 0.0;
    break;

   case 2:
    /* S-Function (motohawk_sfun_data_read): '<S33>/motohawk_data_read6' */
    rtb_motohawk_data_read6_a = RailP_MPa_DataStore();

    /* S-Function Block: <S33>/motohawk_prelookup5 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (railpressure_in_totalfuel_railpressure_fuelpw_map1In_DataStore()) =
        rtb_motohawk_data_read6_a;
      (railpressure_in_totalfuel_railpressure_fuelpw_map1Idx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read6_a,
        (railpressure_in_totalfuel_railpressure_fuelpw_map1IdxArr_DataStore()),
        12, (railpressure_in_totalfuel_railpressure_fuelpw_map1Idx_DataStore()));
      rtb_motohawk_prelookup5_g =
        (railpressure_in_totalfuel_railpressure_fuelpw_map1Idx_DataStore());
    }

    /* S-Function Block: <S33>/motohawk_prelookup6 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (totalfuel_in_totalfuel_railpressure_fuelpw_map1In_DataStore()) = 5.0;
      (totalfuel_in_totalfuel_railpressure_fuelpw_map1Idx_DataStore()) =
        TablePrelookup_real_T(5.0,
        (totalfuel_in_totalfuel_railpressure_fuelpw_map1IdxArr_DataStore()), 14,
                              
        (totalfuel_in_totalfuel_railpressure_fuelpw_map1Idx_DataStore()));
      rtb_motohawk_prelookup6 =
        (totalfuel_in_totalfuel_railpressure_fuelpw_map1Idx_DataStore());
    }

    /* S-Function Block: <S33>/motohawk_interpolation_2d3 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d3 = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup6, rtb_motohawk_prelookup5_g, (real_T *)
         ((fuelpw_in_totalfuel_railpressure_fuelpw_map1Map_DataStore())), 14, 12);
      (fuelpw_in_totalfuel_railpressure_fuelpw_map1_DataStore()) =
        rtb_motohawk_interpolation_2d3;
    }

    rtb_Gain5 = rtb_motohawk_interpolation_2d3;
    break;

   case 3:
    /* Switch: '<S33>/Switch' incorporates:
     *  Constant: '<S33>/Constant1'
     *  S-Function (motohawk_sfun_data_read): '<S33>/motohawk_data_read1'
     *  S-Function (motohawk_sfun_data_read): '<S33>/motohawk_data_read2'
     *  S-Function (motohawk_sfun_interpolation_2d): '<S33>/motohawk_interpolation_2d1'
     *  S-Function (motohawk_sfun_prelookup): '<S33>/motohawk_prelookup2'
     *  S-Function (motohawk_sfun_prelookup): '<S33>/motohawk_prelookup3'
     */
    if (rtb_motohawk_data_read1_j > 3500.0) {
      rtb_Gain5 = 0.0;
    } else {
      /* S-Function Block: <S33>/motohawk_prelookup2 */
      {
        extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
          ordarr[], uint32_T sz, uint16_T prev);
        (Pedal_pct_for_PilotFueling_ms_in_RunDieselIn_DataStore()) =
          rtb_motohawk_data_read2_f;
        (Pedal_pct_for_PilotFueling_ms_in_RunDieselIdx_DataStore()) =
          TablePrelookup_real_T(rtb_motohawk_data_read2_f,
          (Pedal_pct_for_PilotFueling_ms_in_RunDieselIdxArr_DataStore()), 21,
          (Pedal_pct_for_PilotFueling_ms_in_RunDieselIdx_DataStore()));
        rtb_motohawk_prelookup2_k =
          (Pedal_pct_for_PilotFueling_ms_in_RunDieselIdx_DataStore());
      }

      /* S-Function Block: <S33>/motohawk_prelookup3 */
      {
        extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
          ordarr[], uint32_T sz, uint16_T prev);
        (EngineSpeed_rpm_for_PilotFueling_ms_in_RunDieselIn_DataStore()) =
          rtb_motohawk_data_read1_j;
        (EngineSpeed_rpm_for_PilotFueling_ms_in_RunDieselIdx_DataStore()) =
          TablePrelookup_real_T(rtb_motohawk_data_read1_j,
          (EngineSpeed_rpm_for_PilotFueling_ms_in_RunDieselIdxArr_DataStore()),
          20, (EngineSpeed_rpm_for_PilotFueling_ms_in_RunDieselIdx_DataStore()));
        rtb_motohawk_prelookup3_d =
          (EngineSpeed_rpm_for_PilotFueling_ms_in_RunDieselIdx_DataStore());
      }

      /* S-Function Block: <S33>/motohawk_interpolation_2d1 */
      {
        extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T
          col_in, real_T *map_data, uint32_T row_sz, uint32_T col_sz);
        rtb_motohawk_interpolation_2d1_o = TableInterpolation2D_real_T
          (rtb_motohawk_prelookup3_d, rtb_motohawk_prelookup2_k, (real_T *)
           ((PilotFueling_ms_in_RunDieselMap_DataStore())), 20, 21);
        (PilotFueling_ms_in_RunDiesel_DataStore()) =
          rtb_motohawk_interpolation_2d1_o;
      }

      rtb_Gain5 = rtb_motohawk_interpolation_2d1_o;
    }

    /* End of Switch: '<S33>/Switch' */
    break;

   case 4:
    /* S-Function Block: <S33>/motohawk_prelookup1 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (Pedal_pct_for_PilotFueling_ms_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read2_f;
      (Pedal_pct_for_PilotFueling_ms_in_RunDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read2_f,
        (Pedal_pct_for_PilotFueling_ms_in_RunDDFIdxArr_DataStore()), 10,
        (Pedal_pct_for_PilotFueling_ms_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup1_p =
        (Pedal_pct_for_PilotFueling_ms_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S33>/motohawk_prelookup4 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_PilotFueling_ms_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read1_j;
      (EngineSpeed_rpm_for_PilotFueling_ms_in_RunDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_j,
        (EngineSpeed_rpm_for_PilotFueling_ms_in_RunDDFIdxArr_DataStore()), 8,
        (EngineSpeed_rpm_for_PilotFueling_ms_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup4_ay =
        (EngineSpeed_rpm_for_PilotFueling_ms_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S33>/motohawk_interpolation_2d2 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d2_g = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup4_ay, rtb_motohawk_prelookup1_p, (real_T *)
         ((PilotFueling_ms_in_RunDDFMap_DataStore())), 8, 10);
      (PilotFueling_ms_in_RunDDF_DataStore()) = rtb_motohawk_interpolation_2d2_g;
    }

    rtb_Gain5 = rtb_motohawk_interpolation_2d2_g;
    break;

   case 5:
    /* Logic: '<S33>/Logical Operator3' incorporates:
     *  S-Function (motohawk_sfun_data_read): '<S33>/motohawk_data_read9'
     */
    rtb_LogicalOperator = !Normal_On_DataStore();

    /* Logic: '<S33>/Logical Operator1' incorporates:
     *  Constant: '<S33>/Constant7'
     *  RelationalOperator: '<S33>/Relational Operator1'
     *  S-Function (motohawk_sfun_data_read): '<S33>/motohawk_data_read7'
     */
    rtb_LogicalOperator = ((50.0 < ECT_C_DataStore()) && rtb_LogicalOperator);

    /* S-Function (motohawk_sfun_data_read): '<S33>/motohawk_data_read8' */
    rtb_motohawk_data_read8_j = AC_On_DataStore();

    /* MultiPortSwitch: '<S49>/Multiport Switch' incorporates:
     *  Constant: '<S49>/Constant'
     *  Product: '<S49>/Product'
     *  S-Function (motohawk_sfun_calibration): '<S49>/motohawk_calibration1'
     *  S-Function (motohawk_sfun_calibration): '<S49>/motohawk_calibration2'
     *  S-Function (motohawk_sfun_calibration): '<S49>/motohawk_calibration3'
     *  S-Function (motohawk_sfun_calibration): '<S49>/motohawk_calibration4'
     *  Sum: '<S49>/Add'
     */
    switch (rtb_motohawk_data_read8_j * 2 + rtb_LogicalOperator) {
     case 0:
      rtb_Gain5 = (PilotIdleFuelingACOffDDFOff_ms_DataStore());
      break;

     case 1:
      rtb_Gain5 = (PilotIdleFuelingACOffDDFOn_ms_DataStore());
      break;

     case 2:
      rtb_Gain5 = (PilotIdleFuelingACOnDDFOff_ms_DataStore());
      break;

     case 3:
      rtb_Gain5 = (PilotIdleFuelingACOnDDFOn_ms_DataStore());
      break;

     default:
      rtb_Gain5 = (PilotIdleFuelingACOffDDFOff_ms_DataStore());
      break;
    }

    /* End of MultiPortSwitch: '<S49>/Multiport Switch' */
    break;

   default:
    rtb_Gain5 = 0.0;
    break;
  }

  /* End of MultiPortSwitch: '<S33>/MultiSwitch' */

  /* S-Function (motohawk_sfun_data_write): '<S33>/motohawk_data_write' */
  /* Write to Data Storage as scalar: PilotFueling_ms */
  {
    PilotFueling_ms_DataStore() = rtb_Gain5;
  }

  /* S-Function (motohawk_sfun_data_read): '<S34>/motohawk_data_read1' */
  rtb_motohawk_data_read1_h = EngineSpeed_rpm_DataStore();

  /* S-Function (motohawk_sfun_data_read): '<S34>/motohawk_data_read2' */
  rtb_motohawk_data_read2_c = Pedal_Pct_DataStore();

  /* MultiPortSwitch: '<S34>/MultiSwitch' incorporates:
   *  Constant: '<S34>/Constant'
   *  Constant: '<S34>/Constant1'
   *  S-Function (motohawk_sfun_data_read): '<S34>/motohawk_data_read'
   *  S-Function (motohawk_sfun_data_read): '<S34>/motohawk_data_read1'
   *  S-Function (motohawk_sfun_data_read): '<S34>/motohawk_data_read2'
   *  S-Function (motohawk_sfun_interpolation_1d): '<S34>/motohawk_interpolation_1d1'
   *  S-Function (motohawk_sfun_interpolation_2d): '<S34>/motohawk_interpolation_2d2'
   *  S-Function (motohawk_sfun_prelookup): '<S34>/motohawk_prelookup1'
   *  S-Function (motohawk_sfun_prelookup): '<S34>/motohawk_prelookup4'
   *  S-Function (motohawk_sfun_prelookup): '<S34>/motohawk_prelookup5'
   */
  switch (((uint8_T)State_DataStore())) {
   case 1:
    rtb_Gain5 = 0.0;
    break;

   case 2:
    /* S-Function Block: <S34>/motohawk_prelookup5 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_PilotSOI_degBTDC_in_CrankIn_DataStore()) =
        rtb_motohawk_data_read1_h;
      (EngineSpeed_rpm_for_PilotSOI_degBTDC_in_CrankIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_h,
        (EngineSpeed_rpm_for_PilotSOI_degBTDC_in_CrankIdxArr_DataStore()), 8,
        (EngineSpeed_rpm_for_PilotSOI_degBTDC_in_CrankIdx_DataStore()));
      rtb_motohawk_prelookup5 =
        (EngineSpeed_rpm_for_PilotSOI_degBTDC_in_CrankIdx_DataStore());
    }

    /* S-Function Block: <S34>/motohawk_interpolation_1d1 */
    {
      extern real_T TableInterpolation1D_real_T(uint16_T idx, real_T *tbl_data,
        uint32_T sz);
      rtb_motohawk_interpolation_1d1 = TableInterpolation1D_real_T
        (rtb_motohawk_prelookup5, (real_T *)
         ((PilotSOI_degBTDC_in_CrankTbl_DataStore())), 8);
      (PilotSOI_degBTDC_in_Crank_DataStore()) = rtb_motohawk_interpolation_1d1;
    }

    rtb_Gain5 = rtb_motohawk_interpolation_1d1;
    break;

   case 3:
    /* Switch: '<S34>/Switch' incorporates:
     *  Constant: '<S34>/Constant2'
     *  S-Function (motohawk_sfun_data_read): '<S34>/motohawk_data_read1'
     *  S-Function (motohawk_sfun_data_read): '<S34>/motohawk_data_read2'
     *  S-Function (motohawk_sfun_interpolation_2d): '<S34>/motohawk_interpolation_2d1'
     *  S-Function (motohawk_sfun_prelookup): '<S34>/motohawk_prelookup2'
     *  S-Function (motohawk_sfun_prelookup): '<S34>/motohawk_prelookup3'
     */
    if (rtb_motohawk_data_read1_h > 3500.0) {
      rtb_Gain5 = 34.0;
    } else {
      /* S-Function Block: <S34>/motohawk_prelookup2 */
      {
        extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
          ordarr[], uint32_T sz, uint16_T prev);
        (Pedal_pct_for_PilotSOI_degBTDC_in_RunDieselIn_DataStore()) =
          rtb_motohawk_data_read2_c;
        (Pedal_pct_for_PilotSOI_degBTDC_in_RunDieselIdx_DataStore()) =
          TablePrelookup_real_T(rtb_motohawk_data_read2_c,
          (Pedal_pct_for_PilotSOI_degBTDC_in_RunDieselIdxArr_DataStore()), 21,
          (Pedal_pct_for_PilotSOI_degBTDC_in_RunDieselIdx_DataStore()));
        rtb_motohawk_prelookup2_j =
          (Pedal_pct_for_PilotSOI_degBTDC_in_RunDieselIdx_DataStore());
      }

      /* S-Function Block: <S34>/motohawk_prelookup3 */
      {
        extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
          ordarr[], uint32_T sz, uint16_T prev);
        (EngineSpeed_rpm_for_PilotSOI_degBTDC_in_RunDieselIn_DataStore()) =
          rtb_motohawk_data_read1_h;
        (EngineSpeed_rpm_for_PilotSOI_degBTDC_in_RunDieselIdx_DataStore()) =
          TablePrelookup_real_T(rtb_motohawk_data_read1_h,
          (EngineSpeed_rpm_for_PilotSOI_degBTDC_in_RunDieselIdxArr_DataStore()),
          20, (EngineSpeed_rpm_for_PilotSOI_degBTDC_in_RunDieselIdx_DataStore()));
        rtb_motohawk_prelookup3_jo =
          (EngineSpeed_rpm_for_PilotSOI_degBTDC_in_RunDieselIdx_DataStore());
      }

      /* S-Function Block: <S34>/motohawk_interpolation_2d1 */
      {
        extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T
          col_in, real_T *map_data, uint32_T row_sz, uint32_T col_sz);
        rtb_motohawk_interpolation_2d1_j = TableInterpolation2D_real_T
          (rtb_motohawk_prelookup3_jo, rtb_motohawk_prelookup2_j, (real_T *)
           ((PilotSOI_degBTDC_in_RunDieselMap_DataStore())), 20, 21);
        (PilotSOI_degBTDC_in_RunDiesel_DataStore()) =
          rtb_motohawk_interpolation_2d1_j;
      }

      rtb_Gain5 = rtb_motohawk_interpolation_2d1_j;
    }

    /* End of Switch: '<S34>/Switch' */
    break;

   case 4:
    /* S-Function Block: <S34>/motohawk_prelookup1 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (Pedal_pct_for_PilotSOI_degBTDC_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read2_c;
      (Pedal_pct_for_PilotSOI_degBTDC_in_RunDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read2_c,
        (Pedal_pct_for_PilotSOI_degBTDC_in_RunDDFIdxArr_DataStore()), 21,
        (Pedal_pct_for_PilotSOI_degBTDC_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup1_el =
        (Pedal_pct_for_PilotSOI_degBTDC_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S34>/motohawk_prelookup4 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_PilotSOI_degBTDC_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read1_h;
      (EngineSpeed_rpm_for_PilotSOI_degBTDC_in_RunDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_h,
        (EngineSpeed_rpm_for_PilotSOI_degBTDC_in_RunDDFIdxArr_DataStore()), 20,
                              
        (EngineSpeed_rpm_for_PilotSOI_degBTDC_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup4_b =
        (EngineSpeed_rpm_for_PilotSOI_degBTDC_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S34>/motohawk_interpolation_2d2 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d2_i = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup4_b, rtb_motohawk_prelookup1_el, (real_T *)
         ((PilotSOI_degBTDC_in_RunDDFMap_DataStore())), 20, 21);
      (PilotSOI_degBTDC_in_RunDDF_DataStore()) =
        rtb_motohawk_interpolation_2d2_i;
    }

    rtb_Gain5 = rtb_motohawk_interpolation_2d2_i;
    break;

   case 5:
    /* Logic: '<S34>/Logical Operator3' incorporates:
     *  S-Function (motohawk_sfun_data_read): '<S34>/motohawk_data_read7'
     */
    rtb_LogicalOperator = !Normal_On_DataStore();

    /* Logic: '<S34>/Logical Operator1' incorporates:
     *  Constant: '<S34>/Constant4'
     *  RelationalOperator: '<S34>/Relational Operator1'
     *  S-Function (motohawk_sfun_data_read): '<S34>/motohawk_data_read4'
     */
    rtb_LogicalOperator = ((50.0 < ECT_C_DataStore()) && rtb_LogicalOperator);

    /* S-Function (motohawk_sfun_data_read): '<S34>/motohawk_data_read6' */
    rtb_motohawk_data_read6_i = AC_On_DataStore();

    /* MultiPortSwitch: '<S50>/Multiport Switch' incorporates:
     *  Constant: '<S50>/Constant'
     *  Product: '<S50>/Product'
     *  S-Function (motohawk_sfun_calibration): '<S50>/motohawk_calibration1'
     *  S-Function (motohawk_sfun_calibration): '<S50>/motohawk_calibration2'
     *  S-Function (motohawk_sfun_calibration): '<S50>/motohawk_calibration3'
     *  S-Function (motohawk_sfun_calibration): '<S50>/motohawk_calibration4'
     *  Sum: '<S50>/Add'
     */
    switch (rtb_motohawk_data_read6_i * 2 + rtb_LogicalOperator) {
     case 0:
      rtb_Gain5 = (PilotSOIIdleACOffDDFOff_degBTDC_DataStore());
      break;

     case 1:
      rtb_Gain5 = (PilotSOIIdleACOffDDFOn_degBTDC_DataStore());
      break;

     case 2:
      rtb_Gain5 = (PilotSOIIdleACOnDDFOff_degBTDC_DataStore());
      break;

     case 3:
      rtb_Gain5 = (PilotSOIIdleACOnDDFOn_degBTDC_DataStore());
      break;

     default:
      rtb_Gain5 = (PilotSOIIdleACOffDDFOff_degBTDC_DataStore());
      break;
    }

    /* End of MultiPortSwitch: '<S50>/Multiport Switch' */
    break;

   default:
    rtb_Gain5 = 34.0;
    break;
  }

  /* End of MultiPortSwitch: '<S34>/MultiSwitch' */

  /* S-Function (motohawk_sfun_data_write): '<S34>/motohawk_data_write' */
  /* Write to Data Storage as scalar: PilotSOI_degBTDC */
  {
    PilotSOI_degBTDC_DataStore() = rtb_Gain5;
  }

  /* S-Function (motohawk_sfun_data_read): '<S35>/motohawk_data_read1' */
  rtb_motohawk_data_read1_p = EngineSpeed_rpm_DataStore();

  /* S-Function (motohawk_sfun_data_read): '<S35>/motohawk_data_read2' */
  rtb_motohawk_data_read2_d = Pedal_Pct_DataStore();

  /* MultiPortSwitch: '<S35>/MultiSwitch' incorporates:
   *  Constant: '<S35>/Constant'
   *  Constant: '<S35>/Constant2'
   *  S-Function (motohawk_sfun_data_read): '<S35>/motohawk_data_read'
   *  S-Function (motohawk_sfun_data_read): '<S35>/motohawk_data_read1'
   *  S-Function (motohawk_sfun_data_read): '<S35>/motohawk_data_read2'
   *  S-Function (motohawk_sfun_interpolation_1d): '<S35>/motohawk_interpolation_1d'
   *  S-Function (motohawk_sfun_interpolation_2d): '<S35>/motohawk_interpolation_2d2'
   *  S-Function (motohawk_sfun_prelookup): '<S35>/motohawk_prelookup'
   *  S-Function (motohawk_sfun_prelookup): '<S35>/motohawk_prelookup1'
   *  S-Function (motohawk_sfun_prelookup): '<S35>/motohawk_prelookup4'
   */
  switch (((uint8_T)State_DataStore())) {
   case 1:
    rtb_Gain5 = 0.0;
    break;

   case 2:
    /* S-Function Block: <S35>/motohawk_prelookup */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (speed_in_speed_predurationcranking_mapIn_DataStore()) =
        rtb_motohawk_data_read1_p;
      (speed_in_speed_predurationcranking_mapIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_p,
        (speed_in_speed_predurationcranking_mapIdxArr_DataStore()), 8,
        (speed_in_speed_predurationcranking_mapIdx_DataStore()));
      rtb_motohawk_prelookup_a =
        (speed_in_speed_predurationcranking_mapIdx_DataStore());
    }

    /* S-Function Block: <S35>/motohawk_interpolation_1d */
    {
      extern real_T TableInterpolation1D_real_T(uint16_T idx, real_T *tbl_data,
        uint32_T sz);
      rtb_motohawk_interpolation_1d = TableInterpolation1D_real_T
        (rtb_motohawk_prelookup_a, (real_T *)
         ((predurationcranking_in_speed_predurationcranking_mapTbl_DataStore())),
         8);
      (predurationcranking_in_speed_predurationcranking_map_DataStore()) =
        rtb_motohawk_interpolation_1d;
    }

    rtb_Gain5 = rtb_motohawk_interpolation_1d;
    break;

   case 3:
    /* Switch: '<S35>/Switch' incorporates:
     *  Constant: '<S35>/Constant1'
     *  S-Function (motohawk_sfun_data_read): '<S35>/motohawk_data_read1'
     *  S-Function (motohawk_sfun_data_read): '<S35>/motohawk_data_read2'
     *  S-Function (motohawk_sfun_interpolation_2d): '<S35>/motohawk_interpolation_2d1'
     *  S-Function (motohawk_sfun_prelookup): '<S35>/motohawk_prelookup2'
     *  S-Function (motohawk_sfun_prelookup): '<S35>/motohawk_prelookup3'
     */
    if (rtb_motohawk_data_read1_p > 2000.0) {
      rtb_Gain5 = 0.0;
    } else {
      /* S-Function Block: <S35>/motohawk_prelookup2 */
      {
        extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
          ordarr[], uint32_T sz, uint16_T prev);
        (Pedal_pct_for_PreFueling_ms_in_RunDieselIn_DataStore()) =
          rtb_motohawk_data_read2_d;
        (Pedal_pct_for_PreFueling_ms_in_RunDieselIdx_DataStore()) =
          TablePrelookup_real_T(rtb_motohawk_data_read2_d,
          (Pedal_pct_for_PreFueling_ms_in_RunDieselIdxArr_DataStore()), 21,
          (Pedal_pct_for_PreFueling_ms_in_RunDieselIdx_DataStore()));
        rtb_motohawk_prelookup2_c =
          (Pedal_pct_for_PreFueling_ms_in_RunDieselIdx_DataStore());
      }

      /* S-Function Block: <S35>/motohawk_prelookup3 */
      {
        extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
          ordarr[], uint32_T sz, uint16_T prev);
        (EngineSpeed_rpm_for_PreFueling_ms_in_RunDieselIn_DataStore()) =
          rtb_motohawk_data_read1_p;
        (EngineSpeed_rpm_for_PreFueling_ms_in_RunDieselIdx_DataStore()) =
          TablePrelookup_real_T(rtb_motohawk_data_read1_p,
          (EngineSpeed_rpm_for_PreFueling_ms_in_RunDieselIdxArr_DataStore()), 20,
                                
          (EngineSpeed_rpm_for_PreFueling_ms_in_RunDieselIdx_DataStore()));
        rtb_motohawk_prelookup3_b =
          (EngineSpeed_rpm_for_PreFueling_ms_in_RunDieselIdx_DataStore());
      }

      /* S-Function Block: <S35>/motohawk_interpolation_2d1 */
      {
        extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T
          col_in, real_T *map_data, uint32_T row_sz, uint32_T col_sz);
        rtb_motohawk_interpolation_2d1_f = TableInterpolation2D_real_T
          (rtb_motohawk_prelookup3_b, rtb_motohawk_prelookup2_c, (real_T *)
           ((PreFueling_ms_in_RunDieselMap_DataStore())), 20, 21);
        (PreFueling_ms_in_RunDiesel_DataStore()) =
          rtb_motohawk_interpolation_2d1_f;
      }

      rtb_Gain5 = rtb_motohawk_interpolation_2d1_f;
    }

    /* End of Switch: '<S35>/Switch' */
    break;

   case 4:
    /* S-Function Block: <S35>/motohawk_prelookup1 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (Pedal_pct_for_PreFueling_ms_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read2_d;
      (Pedal_pct_for_PreFueling_ms_in_RunDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read2_d,
        (Pedal_pct_for_PreFueling_ms_in_RunDDFIdxArr_DataStore()), 10,
        (Pedal_pct_for_PreFueling_ms_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup1_k =
        (Pedal_pct_for_PreFueling_ms_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S35>/motohawk_prelookup4 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_PreFueling_ms_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read1_p;
      (EngineSpeed_rpm_for_PreFueling_ms_in_RunDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_p,
        (EngineSpeed_rpm_for_PreFueling_ms_in_RunDDFIdxArr_DataStore()), 8,
        (EngineSpeed_rpm_for_PreFueling_ms_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup4_d =
        (EngineSpeed_rpm_for_PreFueling_ms_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S35>/motohawk_interpolation_2d2 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d2_d = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup4_d, rtb_motohawk_prelookup1_k, (real_T *)
         ((PreFueling_ms_in_RunDDFMap_DataStore())), 8, 10);
      (PreFueling_ms_in_RunDDF_DataStore()) = rtb_motohawk_interpolation_2d2_d;
    }

    rtb_Gain5 = rtb_motohawk_interpolation_2d2_d;
    break;

   case 5:
    /* Logic: '<S35>/Logical Operator3' incorporates:
     *  S-Function (motohawk_sfun_data_read): '<S35>/motohawk_data_read9'
     */
    rtb_LogicalOperator = !Normal_On_DataStore();

    /* Logic: '<S35>/Logical Operator1' incorporates:
     *  Constant: '<S35>/Constant4'
     *  RelationalOperator: '<S35>/Relational Operator1'
     *  S-Function (motohawk_sfun_data_read): '<S35>/motohawk_data_read7'
     */
    rtb_LogicalOperator = ((50.0 < ECT_C_DataStore()) && rtb_LogicalOperator);

    /* S-Function (motohawk_sfun_data_read): '<S35>/motohawk_data_read8' */
    rtb_motohawk_data_read8_n = AC_On_DataStore();

    /* MultiPortSwitch: '<S51>/Multiport Switch' incorporates:
     *  Constant: '<S51>/Constant'
     *  Product: '<S51>/Product'
     *  S-Function (motohawk_sfun_calibration): '<S51>/motohawk_calibration1'
     *  S-Function (motohawk_sfun_calibration): '<S51>/motohawk_calibration2'
     *  S-Function (motohawk_sfun_calibration): '<S51>/motohawk_calibration3'
     *  S-Function (motohawk_sfun_calibration): '<S51>/motohawk_calibration4'
     *  Sum: '<S51>/Add'
     */
    switch (rtb_motohawk_data_read8_n * 2 + rtb_LogicalOperator) {
     case 0:
      rtb_Gain5 = (PreFuelingIdleACOffDDFOff_ms_DataStore());
      break;

     case 1:
      rtb_Gain5 = (PreFuelingIdleACOffDDFOn_ms_DataStore());
      break;

     case 2:
      rtb_Gain5 = (PreFuelingIdleACOnDDFOff_ms_DataStore());
      break;

     case 3:
      rtb_Gain5 = (PreFuelingIdleACOnDDFOn_ms_DataStore());
      break;

     default:
      rtb_Gain5 = (PreFuelingIdleACOffDDFOff_ms_DataStore());
      break;
    }

    /* End of MultiPortSwitch: '<S51>/Multiport Switch' */
    break;

   default:
    rtb_Gain5 = 0.0;
    break;
  }

  /* End of MultiPortSwitch: '<S35>/MultiSwitch' */

  /* S-Function (motohawk_sfun_data_write): '<S35>/motohawk_data_write' */
  /* Write to Data Storage as scalar: PreFueling_ms */
  {
    PreFueling_ms_DataStore() = rtb_Gain5;
  }

  /* S-Function (motohawk_sfun_data_read): '<S36>/motohawk_data_read1' */
  rtb_motohawk_data_read1_o = EngineSpeed_rpm_DataStore();

  /* S-Function (motohawk_sfun_data_read): '<S36>/motohawk_data_read2' */
  rtb_motohawk_data_read2_kx = Pedal_Pct_DataStore();

  /* MultiPortSwitch: '<S36>/MultiSwitch' incorporates:
   *  Constant: '<S36>/Constant'
   *  Constant: '<S36>/Constant1'
   *  S-Function (motohawk_sfun_data_read): '<S36>/motohawk_data_read'
   *  S-Function (motohawk_sfun_data_read): '<S36>/motohawk_data_read1'
   *  S-Function (motohawk_sfun_data_read): '<S36>/motohawk_data_read2'
   *  S-Function (motohawk_sfun_interpolation_1d): '<S36>/motohawk_interpolation_1d4'
   *  S-Function (motohawk_sfun_interpolation_2d): '<S36>/motohawk_interpolation_2d2'
   *  S-Function (motohawk_sfun_prelookup): '<S36>/motohawk_prelookup1'
   *  S-Function (motohawk_sfun_prelookup): '<S36>/motohawk_prelookup4'
   *  S-Function (motohawk_sfun_prelookup): '<S36>/motohawk_prelookup7'
   */
  switch (((uint8_T)State_DataStore())) {
   case 1:
    rtb_Gain5 = 0.0;
    break;

   case 2:
    /* S-Function Block: <S36>/motohawk_prelookup7 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_PreSOI_degBTDC_in_CrankIn_DataStore()) =
        rtb_motohawk_data_read1_o;
      (EngineSpeed_rpm_for_PreSOI_degBTDC_in_CrankIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_o,
        (EngineSpeed_rpm_for_PreSOI_degBTDC_in_CrankIdxArr_DataStore()), 8,
        (EngineSpeed_rpm_for_PreSOI_degBTDC_in_CrankIdx_DataStore()));
      rtb_motohawk_prelookup7 =
        (EngineSpeed_rpm_for_PreSOI_degBTDC_in_CrankIdx_DataStore());
    }

    /* S-Function Block: <S36>/motohawk_interpolation_1d4 */
    {
      extern real_T TableInterpolation1D_real_T(uint16_T idx, real_T *tbl_data,
        uint32_T sz);
      rtb_motohawk_interpolation_1d4 = TableInterpolation1D_real_T
        (rtb_motohawk_prelookup7, (real_T *)
         ((PreSOI_degBTDC_in_CrankTbl_DataStore())), 8);
      (PreSOI_degBTDC_in_Crank_DataStore()) = rtb_motohawk_interpolation_1d4;
    }

    rtb_Gain5 = rtb_motohawk_interpolation_1d4;
    break;

   case 3:
    /* Switch: '<S36>/Switch' incorporates:
     *  Constant: '<S36>/Constant3'
     *  S-Function (motohawk_sfun_data_read): '<S36>/motohawk_data_read1'
     *  S-Function (motohawk_sfun_data_read): '<S36>/motohawk_data_read2'
     *  S-Function (motohawk_sfun_interpolation_2d): '<S36>/motohawk_interpolation_2d1'
     *  S-Function (motohawk_sfun_prelookup): '<S36>/motohawk_prelookup2'
     *  S-Function (motohawk_sfun_prelookup): '<S36>/motohawk_prelookup3'
     */
    if (rtb_motohawk_data_read1_o > 2000.0) {
      rtb_Gain5 = 38.0;
    } else {
      /* S-Function Block: <S36>/motohawk_prelookup2 */
      {
        extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
          ordarr[], uint32_T sz, uint16_T prev);
        (Pedal_pct_for_PreSOI_degBTDC_in_RunDieselIn_DataStore()) =
          rtb_motohawk_data_read2_kx;
        (Pedal_pct_for_PreSOI_degBTDC_in_RunDieselIdx_DataStore()) =
          TablePrelookup_real_T(rtb_motohawk_data_read2_kx,
          (Pedal_pct_for_PreSOI_degBTDC_in_RunDieselIdxArr_DataStore()), 21,
          (Pedal_pct_for_PreSOI_degBTDC_in_RunDieselIdx_DataStore()));
        rtb_motohawk_prelookup2_i =
          (Pedal_pct_for_PreSOI_degBTDC_in_RunDieselIdx_DataStore());
      }

      /* S-Function Block: <S36>/motohawk_prelookup3 */
      {
        extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
          ordarr[], uint32_T sz, uint16_T prev);
        (EngineSpeed_rpm_for_PreSOI_degBTDC_in_RunDieselIn_DataStore()) =
          rtb_motohawk_data_read1_o;
        (EngineSpeed_rpm_for_PreSOI_degBTDC_in_RunDieselIdx_DataStore()) =
          TablePrelookup_real_T(rtb_motohawk_data_read1_o,
          (EngineSpeed_rpm_for_PreSOI_degBTDC_in_RunDieselIdxArr_DataStore()),
          20, (EngineSpeed_rpm_for_PreSOI_degBTDC_in_RunDieselIdx_DataStore()));
        rtb_motohawk_prelookup3_o =
          (EngineSpeed_rpm_for_PreSOI_degBTDC_in_RunDieselIdx_DataStore());
      }

      /* S-Function Block: <S36>/motohawk_interpolation_2d1 */
      {
        extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T
          col_in, real_T *map_data, uint32_T row_sz, uint32_T col_sz);
        rtb_motohawk_interpolation_2d1_m = TableInterpolation2D_real_T
          (rtb_motohawk_prelookup3_o, rtb_motohawk_prelookup2_i, (real_T *)
           ((PreSOI_degBTDC_in_RunDieselMap_DataStore())), 20, 21);
        (PreSOI_degBTDC_in_RunDiesel_DataStore()) =
          rtb_motohawk_interpolation_2d1_m;
      }

      rtb_Gain5 = rtb_motohawk_interpolation_2d1_m;
    }

    /* End of Switch: '<S36>/Switch' */
    break;

   case 4:
    /* S-Function Block: <S36>/motohawk_prelookup1 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (Pedal_pct_for_PreSOI_degBTDC_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read2_kx;
      (Pedal_pct_for_PreSOI_degBTDC_in_RunDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read2_kx,
        (Pedal_pct_for_PreSOI_degBTDC_in_RunDDFIdxArr_DataStore()), 21,
        (Pedal_pct_for_PreSOI_degBTDC_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup1_e =
        (Pedal_pct_for_PreSOI_degBTDC_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S36>/motohawk_prelookup4 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_PreSOI_degBTDC_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read1_o;
      (EngineSpeed_rpm_for_PreSOI_degBTDC_in_RunDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_o,
        (EngineSpeed_rpm_for_PreSOI_degBTDC_in_RunDDFIdxArr_DataStore()), 20,
        (EngineSpeed_rpm_for_PreSOI_degBTDC_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup4_a =
        (EngineSpeed_rpm_for_PreSOI_degBTDC_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S36>/motohawk_interpolation_2d2 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d2_e = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup4_a, rtb_motohawk_prelookup1_e, (real_T *)
         ((PreSOI_degBTDC_in_RunDDFMap_DataStore())), 20, 21);
      (PreSOI_degBTDC_in_RunDDF_DataStore()) = rtb_motohawk_interpolation_2d2_e;
    }

    rtb_Gain5 = rtb_motohawk_interpolation_2d2_e;
    break;

   case 5:
    /* Logic: '<S36>/Logical Operator3' incorporates:
     *  S-Function (motohawk_sfun_data_read): '<S36>/motohawk_data_read7'
     */
    rtb_LogicalOperator = !Normal_On_DataStore();

    /* Logic: '<S36>/Logical Operator1' incorporates:
     *  Constant: '<S36>/Constant4'
     *  RelationalOperator: '<S36>/Relational Operator1'
     *  S-Function (motohawk_sfun_data_read): '<S36>/motohawk_data_read4'
     */
    rtb_LogicalOperator = ((50.0 < ECT_C_DataStore()) && rtb_LogicalOperator);

    /* S-Function (motohawk_sfun_data_read): '<S36>/motohawk_data_read6' */
    rtb_motohawk_data_read6_j = AC_On_DataStore();

    /* MultiPortSwitch: '<S52>/Multiport Switch' incorporates:
     *  Constant: '<S52>/Constant'
     *  Product: '<S52>/Product'
     *  S-Function (motohawk_sfun_calibration): '<S52>/motohawk_calibration1'
     *  S-Function (motohawk_sfun_calibration): '<S52>/motohawk_calibration2'
     *  S-Function (motohawk_sfun_calibration): '<S52>/motohawk_calibration3'
     *  S-Function (motohawk_sfun_calibration): '<S52>/motohawk_calibration4'
     *  Sum: '<S52>/Add'
     */
    switch (rtb_motohawk_data_read6_j * 2 + rtb_LogicalOperator) {
     case 0:
      rtb_Gain5 = (PreSOIIdleACOffDDFOff_degBTDC_DataStore());
      break;

     case 1:
      rtb_Gain5 = (PreSOIIdleACOffDDFOn_degBTDC_DataStore());
      break;

     case 2:
      rtb_Gain5 = (PreSOIIdleACOnDDFOff_degBTDC_DataStore());
      break;

     case 3:
      rtb_Gain5 = (PreSOIIdleACOnDDFOn_degBTDC_DataStore());
      break;

     default:
      rtb_Gain5 = (PreSOIIdleACOffDDFOff_degBTDC_DataStore());
      break;
    }

    /* End of MultiPortSwitch: '<S52>/Multiport Switch' */
    break;

   default:
    rtb_Gain5 = 38.0;
    break;
  }

  /* End of MultiPortSwitch: '<S36>/MultiSwitch' */

  /* S-Function (motohawk_sfun_data_write): '<S36>/motohawk_data_write' */
  /* Write to Data Storage as scalar: PreSOI_degBTDC */
  {
    PreSOI_degBTDC_DataStore() = rtb_Gain5;
  }

  /* S-Function (motohawk_sfun_data_read): '<S37>/motohawk_data_read1' */
  rtb_motohawk_data_read1_a = EngineSpeed_rpm_DataStore();

  /* S-Function (motohawk_sfun_data_read): '<S37>/motohawk_data_read2' */
  rtb_motohawk_data_read2_dx = Pedal_Pct_DataStore();

  /* S-Function Block: <S37>/motohawk_prelookup4 */
  {
    extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
      ordarr[], uint32_T sz, uint16_T prev);
    (EngineSpeed_rpm_for_RailP_MPa_in_RunDDFIn_DataStore()) =
      rtb_motohawk_data_read1_a;
    (EngineSpeed_rpm_for_RailP_MPa_in_RunDDFIdx_DataStore()) =
      TablePrelookup_real_T(rtb_motohawk_data_read1_a,
      (EngineSpeed_rpm_for_RailP_MPa_in_RunDDFIdxArr_DataStore()), 20,
      (EngineSpeed_rpm_for_RailP_MPa_in_RunDDFIdx_DataStore()));
    rtb_motohawk_prelookup4 =
      (EngineSpeed_rpm_for_RailP_MPa_in_RunDDFIdx_DataStore());
  }

  /* S-Function Block: <S37>/motohawk_prelookup1 */
  {
    extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
      ordarr[], uint32_T sz, uint16_T prev);
    (Pedal_pct_for_RailP_MPa_in_RunDDFIn_DataStore()) =
      rtb_motohawk_data_read2_dx;
    (Pedal_pct_for_RailP_MPa_in_RunDDFIdx_DataStore()) = TablePrelookup_real_T
      (rtb_motohawk_data_read2_dx,
       (Pedal_pct_for_RailP_MPa_in_RunDDFIdxArr_DataStore()), 21,
       (Pedal_pct_for_RailP_MPa_in_RunDDFIdx_DataStore()));
    rtb_motohawk_prelookup1 = (Pedal_pct_for_RailP_MPa_in_RunDDFIdx_DataStore());
  }

  /* S-Function Block: <S37>/motohawk_interpolation_2d2 */
  {
    extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
      real_T *map_data, uint32_T row_sz, uint32_T col_sz);
    rtb_motohawk_interpolation_2d2 = TableInterpolation2D_real_T
      (rtb_motohawk_prelookup4, rtb_motohawk_prelookup1, (real_T *)
       ((RailP_MPa_in_RunDDFMap_DataStore())), 20, 21);
    (RailP_MPa_in_RunDDF_DataStore()) = rtb_motohawk_interpolation_2d2;
  }

  /* MultiPortSwitch: '<S37>/MultiSwitch' incorporates:
   *  Constant: '<S37>/Constant'
   *  Constant: '<S37>/Constant2'
   *  S-Function (motohawk_sfun_calibration): '<S37>/motohawk_calibration'
   *  S-Function (motohawk_sfun_data_read): '<S37>/motohawk_data_read'
   *  S-Function (motohawk_sfun_data_read): '<S37>/motohawk_data_read1'
   *  S-Function (motohawk_sfun_data_read): '<S37>/motohawk_data_read2'
   *  S-Function (motohawk_sfun_interpolation_2d): '<S37>/motohawk_interpolation_2d1'
   *  S-Function (motohawk_sfun_interpolation_2d): '<S37>/motohawk_interpolation_2d2'
   *  S-Function (motohawk_sfun_prelookup): '<S37>/motohawk_prelookup1'
   *  S-Function (motohawk_sfun_prelookup): '<S37>/motohawk_prelookup2'
   *  S-Function (motohawk_sfun_prelookup): '<S37>/motohawk_prelookup3'
   *  S-Function (motohawk_sfun_prelookup): '<S37>/motohawk_prelookup4'
   */
  switch (((uint8_T)State_DataStore())) {
   case 1:
    rtb_DataTypeConversion_n = 0.0;
    break;

   case 2:
    rtb_DataTypeConversion_n = (RailPSPCrank_MPa_DataStore());
    break;

   case 3:
    /* S-Function Block: <S37>/motohawk_prelookup2 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (Pedal_pct_for_RailP_MPa_in_RunDieselIn_DataStore()) =
        rtb_motohawk_data_read2_dx;
      (Pedal_pct_for_RailP_MPa_in_RunDieselIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read2_dx,
        (Pedal_pct_for_RailP_MPa_in_RunDieselIdxArr_DataStore()), 21,
        (Pedal_pct_for_RailP_MPa_in_RunDieselIdx_DataStore()));
      rtb_motohawk_prelookup2_n =
        (Pedal_pct_for_RailP_MPa_in_RunDieselIdx_DataStore());
    }

    /* S-Function Block: <S37>/motohawk_prelookup3 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_RailP_MPa_in_RunDieselIn_DataStore()) =
        rtb_motohawk_data_read1_a;
      (EngineSpeed_rpm_for_RailP_MPa_in_RunDieselIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_a,
        (EngineSpeed_rpm_for_RailP_MPa_in_RunDieselIdxArr_DataStore()), 20,
        (EngineSpeed_rpm_for_RailP_MPa_in_RunDieselIdx_DataStore()));
      rtb_motohawk_prelookup3_j =
        (EngineSpeed_rpm_for_RailP_MPa_in_RunDieselIdx_DataStore());
    }

    /* S-Function Block: <S37>/motohawk_interpolation_2d1 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d1_k = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup3_j, rtb_motohawk_prelookup2_n, (real_T *)
         ((RailP_MPa_in_RunDieselMap_DataStore())), 20, 21);
      (RailP_MPa_in_RunDiesel_DataStore()) = rtb_motohawk_interpolation_2d1_k;
    }

    rtb_DataTypeConversion_n = rtb_motohawk_interpolation_2d1_k;
    break;

   case 4:
    rtb_DataTypeConversion_n = rtb_motohawk_interpolation_2d2;
    break;

   case 5:
    rtb_DataTypeConversion_n = 33.0;
    break;

   default:
    rtb_DataTypeConversion_n = rtb_motohawk_interpolation_2d2;
    break;
  }

  /* End of MultiPortSwitch: '<S37>/MultiSwitch' */
  /* If: '<S53>/If' incorporates:
   *  Inport: '<S54>/In1'
   *  Inport: '<S55>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S53>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S53>/override_enable'
   */
  if ((RailPSPOverride_MPa_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S53>/NewValue' incorporates:
     *  ActionPort: '<S54>/Action Port'
     */
    rtb_Merge_d = (RailPSPOverride_MPa_new_DataStore());

    /* End of Outputs for SubSystem: '<S53>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S53>/OldValue' incorporates:
     *  ActionPort: '<S55>/Action Port'
     */
    rtb_Merge_d = rtb_DataTypeConversion_n;

    /* End of Outputs for SubSystem: '<S53>/OldValue' */
  }

  /* End of If: '<S53>/If' */

  /* S-Function (motohawk_sfun_data_write): '<S37>/motohawk_data_write' */
  /* Write to Data Storage as scalar: RailPSP_MPa */
  {
    RailPSP_MPa_DataStore() = rtb_Merge_d;
  }

  /* S-Function (motohawk_sfun_data_read): '<S38>/motohawk_data_read1' */
  rtb_motohawk_data_read1_am = EngineSpeed_rpm_DataStore();

  /* S-Function (motohawk_sfun_data_read): '<S38>/motohawk_data_read2' */
  rtb_motohawk_data_read2_a = Pedal_Pct_DataStore();

  /* MultiPortSwitch: '<S38>/MultiSwitch' incorporates:
   *  Constant: '<S38>/Constant'
   *  Constant: '<S38>/Constant1'
   *  Constant: '<S38>/Constant2'
   *  Constant: '<S38>/Constant3'
   *  S-Function (motohawk_sfun_data_read): '<S38>/motohawk_data_read'
   *  S-Function (motohawk_sfun_data_read): '<S38>/motohawk_data_read1'
   *  S-Function (motohawk_sfun_data_read): '<S38>/motohawk_data_read2'
   *  S-Function (motohawk_sfun_interpolation_2d): '<S38>/motohawk_interpolation_2d1'
   *  S-Function (motohawk_sfun_interpolation_2d): '<S38>/motohawk_interpolation_2d2'
   *  S-Function (motohawk_sfun_prelookup): '<S38>/motohawk_prelookup1'
   *  S-Function (motohawk_sfun_prelookup): '<S38>/motohawk_prelookup2'
   *  S-Function (motohawk_sfun_prelookup): '<S38>/motohawk_prelookup3'
   *  S-Function (motohawk_sfun_prelookup): '<S38>/motohawk_prelookup4'
   */
  switch (((uint8_T)State_DataStore())) {
   case 1:
    rtb_DataTypeConversion_n = 0.0;
    break;

   case 2:
    rtb_DataTypeConversion_n = 100.0;
    break;

   case 3:
    /* S-Function Block: <S38>/motohawk_prelookup2 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (Pedal_pct_for_TPS_Pct_in_RunDieselIn_DataStore()) =
        rtb_motohawk_data_read2_a;
      (Pedal_pct_for_TPS_Pct_in_RunDieselIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read2_a,
        (Pedal_pct_for_TPS_Pct_in_RunDieselIdxArr_DataStore()), 21,
        (Pedal_pct_for_TPS_Pct_in_RunDieselIdx_DataStore()));
      rtb_motohawk_prelookup2 = (Pedal_pct_for_TPS_Pct_in_RunDieselIdx_DataStore
                                 ());
    }

    /* S-Function Block: <S38>/motohawk_prelookup3 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_TPS_Pct_in_RunDieselIn_DataStore()) =
        rtb_motohawk_data_read1_am;
      (EngineSpeed_rpm_for_TPS_Pct_in_RunDieselIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_am,
        (EngineSpeed_rpm_for_TPS_Pct_in_RunDieselIdxArr_DataStore()), 20,
        (EngineSpeed_rpm_for_TPS_Pct_in_RunDieselIdx_DataStore()));
      rtb_motohawk_prelookup3 =
        (EngineSpeed_rpm_for_TPS_Pct_in_RunDieselIdx_DataStore());
    }

    /* S-Function Block: <S38>/motohawk_interpolation_2d1 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d1 = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup3, rtb_motohawk_prelookup2, (real_T *)
         ((TPS_Pct_in_RunDieselMap_DataStore())), 20, 21);
      (TPS_Pct_in_RunDiesel_DataStore()) = rtb_motohawk_interpolation_2d1;
    }

    rtb_DataTypeConversion_n = rtb_motohawk_interpolation_2d1;
    break;

   case 4:
    /* S-Function Block: <S38>/motohawk_prelookup1 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (Pedal_pct_for_TPS_Pct_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read2_a;
      (Pedal_pct_for_TPS_Pct_in_RunDDFIdx_DataStore()) = TablePrelookup_real_T
        (rtb_motohawk_data_read2_a,
         (Pedal_pct_for_TPS_Pct_in_RunDDFIdxArr_DataStore()), 21,
         (Pedal_pct_for_TPS_Pct_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup1_d = (Pedal_pct_for_TPS_Pct_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S38>/motohawk_prelookup4 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_TPS_Pct_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read1_am;
      (EngineSpeed_rpm_for_TPS_Pct_in_RunDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_am,
        (EngineSpeed_rpm_for_TPS_Pct_in_RunDDFIdxArr_DataStore()), 20,
        (EngineSpeed_rpm_for_TPS_Pct_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup4_g =
        (EngineSpeed_rpm_for_TPS_Pct_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S38>/motohawk_interpolation_2d2 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d2_o = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup4_g, rtb_motohawk_prelookup1_d, (real_T *)
         ((TPS_Pct_in_RunDDFMap_DataStore())), 20, 21);
      (TPS_Pct_in_RunDDF_DataStore()) = rtb_motohawk_interpolation_2d2_o;
    }

    rtb_DataTypeConversion_n = rtb_motohawk_interpolation_2d2_o;
    break;

   case 5:
    rtb_DataTypeConversion_n = 36.0;
    break;

   default:
    rtb_DataTypeConversion_n = 36.0;
    break;
  }

  /* End of MultiPortSwitch: '<S38>/MultiSwitch' */
  /* If: '<S56>/If' incorporates:
   *  Inport: '<S57>/In1'
   *  Inport: '<S58>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S56>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S56>/override_enable'
   */
  if ((TPSSPOverride_Pct_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S56>/NewValue' incorporates:
     *  ActionPort: '<S57>/Action Port'
     */
    rtb_Merge_l = (TPSSPOverride_Pct_new_DataStore());

    /* End of Outputs for SubSystem: '<S56>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S56>/OldValue' incorporates:
     *  ActionPort: '<S58>/Action Port'
     */
    rtb_Merge_l = rtb_DataTypeConversion_n;

    /* End of Outputs for SubSystem: '<S56>/OldValue' */
  }

  /* End of If: '<S56>/If' */

  /* S-Function (motohawk_sfun_data_write): '<S38>/motohawk_data_write' */
  /* Write to Data Storage as scalar: TPSSP_Pct */
  {
    TPSSP_Pct_DataStore() = rtb_Merge_l;
  }

  /* Switch: '<S61>/Switch1' incorporates:
   *  Constant: '<S78>/Constant'
   *  RelationalOperator: '<S78>/Compare'
   *  S-Function (motohawk_sfun_calibration): '<S61>/motohawk_calibration'
   *  S-Function (motohawk_sfun_calibration): '<S61>/motohawk_calibration8'
   *  S-Function (motohawk_sfun_data_read): '<S5>/motohawk_data_read'
   */
  if ((((uint8_T)State_DataStore()) == 2) > 0) {
    rtb_Gain5 = (RailP_k_pcrank_DataStore());
  } else {
    rtb_Gain5 = (RailP_k_p_DataStore());
  }

  /* End of Switch: '<S61>/Switch1' */
  /* Sum: '<S61>/Sum4' incorporates:
   *  S-Function (motohawk_sfun_data_read): '<S5>/motohawk_data_read2'
   *  S-Function (motohawk_sfun_data_read): '<S5>/motohawk_data_read3'
   */
  rtb_Sum4 = RailPSP_MPa_DataStore() - RailP_MPa_DataStore();

  /* Product: '<S61>/Product2' */
  CRVLAB_ECU_Project_B.s61_Product2 = rtb_Gain5 * rtb_Sum4;

  /* UnitDelay: '<S61>/Unit Delay1' */
  rtb_UnitDelay1_d = CRVLAB_ECU_Project_DWork.s61_UnitDelay1_DSTATE;

  /* Product: '<S61>/Product3' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S61>/motohawk_calibration1'
   *  UnitDelay: '<S61>/Unit Delay1'
   */
  CRVLAB_ECU_Project_B.s61_Product3 = (RailP_k_i_DataStore()) *
    CRVLAB_ECU_Project_DWork.s61_UnitDelay1_DSTATE;

  /* Sum: '<S61>/Sum2' incorporates:
   *  UnitDelay: '<S61>/Unit Delay'
   */
  rtb_DataTypeConversion_n = rtb_Sum4 -
    CRVLAB_ECU_Project_DWork.s61_UnitDelay_DSTATE;

  /* S-Function Block: <S61>/motohawk_delta_time */
  {
    uint32_T delta;
    extern uint32_T Timer_FreeRunningCounter_GetDeltaUpdateReference_us(uint32_T
      * pReference_lower32Bits, uint32_T *pReference_upper32Bits);
    delta = Timer_FreeRunningCounter_GetDeltaUpdateReference_us
      (&CRVLAB_ECU_Project_DWork.s61_motohawk_delta_time_DWORK1, NULL);
    rtb_motohawk_delta_time = ((real_T) delta) * 0.000001;
  }

  /* Product: '<S61>/Product4' incorporates:
   *  Product: '<S61>/Product'
   *  S-Function (motohawk_sfun_calibration): '<S61>/motohawk_calibration2'
   *  S-Function (motohawk_sfun_delta_time): '<S61>/motohawk_delta_time'
   */
  CRVLAB_ECU_Project_B.s61_Product4 = rtb_DataTypeConversion_n /
    rtb_motohawk_delta_time * (RailP_k_d_DataStore());

  /* Sum: '<S61>/Add' */
  rtb_DataTypeConversion_n = (CRVLAB_ECU_Project_B.s61_Product2 +
    CRVLAB_ECU_Project_B.s61_Product3) + CRVLAB_ECU_Project_B.s61_Product4;

  /* Sum: '<S61>/Sum5' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S61>/motohawk_calibration7'
   */
  rtb_Gain5 = rtb_DataTypeConversion_n + (RailP_Mean_DC_DataStore());

  /* MinMax: '<S80>/MinMax' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S61>/motohawk_calibration5'
   */
  rtb_Sum5 = (rtb_Gain5 >= (RailP_u_min_DataStore())) || rtIsNaN
    ((RailP_u_min_DataStore())) ? rtb_Gain5 : (RailP_u_min_DataStore());

  /* MinMax: '<S80>/MinMax1' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S61>/motohawk_calibration6'
   */
  rtb_DataTypeConversion_n = (rtb_Sum5 <= (RailP_u_max_DataStore())) || rtIsNaN
    ((RailP_u_max_DataStore())) ? rtb_Sum5 : (RailP_u_max_DataStore());

  /* If: '<S81>/If' incorporates:
   *  Inport: '<S82>/In1'
   *  Inport: '<S83>/In1'
   *  MinMax: '<S80>/MinMax1'
   *  S-Function (motohawk_sfun_calibration): '<S81>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S81>/override_enable'
   */
  if ((RailP_u_Override_Absolute_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S81>/NewValue' incorporates:
     *  ActionPort: '<S82>/Action Port'
     */
    CRVLAB_ECU_Project_B.s81_Merge = (RailP_u_Override_Absolute_new_DataStore());

    /* End of Outputs for SubSystem: '<S81>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S81>/OldValue' incorporates:
     *  ActionPort: '<S83>/Action Port'
     */
    CRVLAB_ECU_Project_B.s81_Merge = rtb_DataTypeConversion_n;

    /* End of Outputs for SubSystem: '<S81>/OldValue' */
  }

  /* End of If: '<S81>/If' */

  /* S-Function (motohawk_sfun_data_write): '<S5>/motohawk_data_write' */
  /* Write to Data Storage as scalar: RailP_DC */
  {
    RailP_DC_DataStore() = CRVLAB_ECU_Project_B.s81_Merge;
  }

  /* Sum: '<S62>/Sum4' incorporates:
   *  S-Function (motohawk_sfun_data_read): '<S5>/motohawk_data_read4'
   *  S-Function (motohawk_sfun_data_read): '<S5>/motohawk_data_read5'
   */
  rtb_Sum4_a = TPSSP_Pct_DataStore() - TPS_Pct_DataStore();

  /* Product: '<S62>/Product2' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S62>/motohawk_calibration'
   */
  CRVLAB_ECU_Project_B.s62_Product2 = (Throttle_k_p_DataStore()) * rtb_Sum4_a;

  /* UnitDelay: '<S62>/Unit Delay1' */
  rtb_UnitDelay1_e = CRVLAB_ECU_Project_DWork.s62_UnitDelay1_DSTATE;

  /* Product: '<S62>/Product3' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S62>/motohawk_calibration1'
   *  UnitDelay: '<S62>/Unit Delay1'
   */
  CRVLAB_ECU_Project_B.s62_Product3 = (Throttle_k_i_DataStore()) *
    CRVLAB_ECU_Project_DWork.s62_UnitDelay1_DSTATE;

  /* Sum: '<S62>/Sum2' incorporates:
   *  UnitDelay: '<S62>/Unit Delay'
   */
  rtb_DataTypeConversion_n = rtb_Sum4_a -
    CRVLAB_ECU_Project_DWork.s62_UnitDelay_DSTATE;

  /* S-Function Block: <S62>/motohawk_delta_time */
  {
    uint32_T delta;
    extern uint32_T Timer_FreeRunningCounter_GetDeltaUpdateReference_us(uint32_T
      * pReference_lower32Bits, uint32_T *pReference_upper32Bits);
    delta = Timer_FreeRunningCounter_GetDeltaUpdateReference_us
      (&CRVLAB_ECU_Project_DWork.s62_motohawk_delta_time_DWORK1, NULL);
    rtb_motohawk_delta_time_n = ((real_T) delta) * 0.000001;
  }

  /* Product: '<S62>/Product4' incorporates:
   *  Product: '<S62>/Product'
   *  S-Function (motohawk_sfun_calibration): '<S62>/motohawk_calibration2'
   *  S-Function (motohawk_sfun_delta_time): '<S62>/motohawk_delta_time'
   */
  CRVLAB_ECU_Project_B.s62_Product4 = rtb_DataTypeConversion_n /
    rtb_motohawk_delta_time_n * (Throttle_k_d_DataStore());

  /* Sum: '<S62>/Add' */
  rtb_DataTypeConversion_n = (CRVLAB_ECU_Project_B.s62_Product2 +
    CRVLAB_ECU_Project_B.s62_Product3) + CRVLAB_ECU_Project_B.s62_Product4;

  /* Sum: '<S62>/Sum5' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S62>/motohawk_calibration7'
   */
  rtb_Sum5 = rtb_DataTypeConversion_n + (Throttle_Mean_DC_DataStore());

  /* MinMax: '<S86>/MinMax' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S62>/motohawk_calibration5'
   */
  rtb_DataTypeConversion_n = (rtb_Sum5 >= (Throttle_u_min_DataStore())) ||
    rtIsNaN((Throttle_u_min_DataStore())) ? rtb_Sum5 : (Throttle_u_min_DataStore
    ());

  /* MinMax: '<S86>/MinMax1' incorporates:
   *  MinMax: '<S86>/MinMax'
   *  S-Function (motohawk_sfun_calibration): '<S62>/motohawk_calibration6'
   */
  rtb_DataTypeConversion_n = (rtb_DataTypeConversion_n <=
    (Throttle_u_max_DataStore())) || rtIsNaN((Throttle_u_max_DataStore())) ?
    rtb_DataTypeConversion_n : (Throttle_u_max_DataStore());

  /* If: '<S87>/If' incorporates:
   *  Inport: '<S88>/In1'
   *  Inport: '<S89>/In1'
   *  MinMax: '<S86>/MinMax1'
   *  S-Function (motohawk_sfun_calibration): '<S87>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S87>/override_enable'
   */
  if ((Throttle_u_Override_Absolute_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S87>/NewValue' incorporates:
     *  ActionPort: '<S88>/Action Port'
     */
    CRVLAB_ECU_Project_B.s87_Merge = (Throttle_u_Override_Absolute_new_DataStore
                                      ());

    /* End of Outputs for SubSystem: '<S87>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S87>/OldValue' incorporates:
     *  ActionPort: '<S89>/Action Port'
     */
    CRVLAB_ECU_Project_B.s87_Merge = rtb_DataTypeConversion_n;

    /* End of Outputs for SubSystem: '<S87>/OldValue' */
  }

  /* End of If: '<S87>/If' */

  /* S-Function (motohawk_sfun_data_write): '<S5>/motohawk_data_write1' */
  /* Write to Data Storage as scalar: Throttle_ADC */
  {
    Throttle_ADC_DataStore() = CRVLAB_ECU_Project_B.s87_Merge;
  }

  /* Sum: '<S59>/Sum4' incorporates:
   *  S-Function (motohawk_sfun_data_read): '<S5>/motohawk_data_read6'
   *  S-Function (motohawk_sfun_data_read): '<S5>/motohawk_data_read7'
   */
  rtb_Sum4_j = EGRSP_Pct_DataStore() - EGR_Pct_DataStore();

  /* Product: '<S59>/Product2' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S59>/motohawk_calibration'
   */
  CRVLAB_ECU_Project_B.s59_Product2 = (EGR_k_p_DataStore()) * rtb_Sum4_j;

  /* UnitDelay: '<S59>/Unit Delay1' */
  rtb_UnitDelay1_g = CRVLAB_ECU_Project_DWork.s59_UnitDelay1_DSTATE;

  /* Product: '<S59>/Product3' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S59>/motohawk_calibration1'
   *  UnitDelay: '<S59>/Unit Delay1'
   */
  CRVLAB_ECU_Project_B.s59_Product3 = (EGR_k_i_DataStore()) *
    CRVLAB_ECU_Project_DWork.s59_UnitDelay1_DSTATE;

  /* Sum: '<S59>/Sum2' incorporates:
   *  UnitDelay: '<S59>/Unit Delay'
   */
  rtb_DataTypeConversion_n = rtb_Sum4_j -
    CRVLAB_ECU_Project_DWork.s59_UnitDelay_DSTATE;

  /* S-Function Block: <S59>/motohawk_delta_time */
  {
    uint32_T delta;
    extern uint32_T Timer_FreeRunningCounter_GetDeltaUpdateReference_us(uint32_T
      * pReference_lower32Bits, uint32_T *pReference_upper32Bits);
    delta = Timer_FreeRunningCounter_GetDeltaUpdateReference_us
      (&CRVLAB_ECU_Project_DWork.s59_motohawk_delta_time_DWORK1, NULL);
    rtb_motohawk_delta_time_c = ((real_T) delta) * 0.000001;
  }

  /* Product: '<S59>/Product4' incorporates:
   *  Product: '<S59>/Product'
   *  S-Function (motohawk_sfun_calibration): '<S59>/motohawk_calibration2'
   *  S-Function (motohawk_sfun_delta_time): '<S59>/motohawk_delta_time'
   */
  CRVLAB_ECU_Project_B.s59_Product4 = rtb_DataTypeConversion_n /
    rtb_motohawk_delta_time_c * (EGR_k_d_DataStore());

  /* Sum: '<S59>/Add' */
  rtb_DataTypeConversion_n = (CRVLAB_ECU_Project_B.s59_Product2 +
    CRVLAB_ECU_Project_B.s59_Product3) + CRVLAB_ECU_Project_B.s59_Product4;

  /* Sum: '<S59>/Sum5' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S59>/motohawk_calibration7'
   */
  rtb_Gain4 = rtb_DataTypeConversion_n + (EGR_Mean_DC_DataStore());

  /* MinMax: '<S66>/MinMax' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S59>/motohawk_calibration5'
   */
  rtb_DataTypeConversion_n = (rtb_Gain4 >= (EGR_u_min_DataStore())) || rtIsNaN
    ((EGR_u_min_DataStore())) ? rtb_Gain4 : (EGR_u_min_DataStore());

  /* MinMax: '<S66>/MinMax1' incorporates:
   *  MinMax: '<S66>/MinMax'
   *  S-Function (motohawk_sfun_calibration): '<S59>/motohawk_calibration6'
   */
  rtb_DataTypeConversion_n = (rtb_DataTypeConversion_n <= (EGR_u_max_DataStore()))
    || rtIsNaN((EGR_u_max_DataStore())) ? rtb_DataTypeConversion_n :
    (EGR_u_max_DataStore());

  /* If: '<S67>/If' incorporates:
   *  Inport: '<S68>/In1'
   *  Inport: '<S69>/In1'
   *  MinMax: '<S66>/MinMax1'
   *  S-Function (motohawk_sfun_calibration): '<S67>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S67>/override_enable'
   */
  if ((EGR_u_Override_Absolute_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S67>/NewValue' incorporates:
     *  ActionPort: '<S68>/Action Port'
     */
    CRVLAB_ECU_Project_B.s67_Merge = (EGR_u_Override_Absolute_new_DataStore());

    /* End of Outputs for SubSystem: '<S67>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S67>/OldValue' incorporates:
     *  ActionPort: '<S69>/Action Port'
     */
    CRVLAB_ECU_Project_B.s67_Merge = rtb_DataTypeConversion_n;

    /* End of Outputs for SubSystem: '<S67>/OldValue' */
  }

  /* End of If: '<S67>/If' */

  /* S-Function (motohawk_sfun_data_write): '<S5>/motohawk_data_write2' */
  /* Write to Data Storage as scalar: EGR_ADC */
  {
    EGR_ADC_DataStore() = CRVLAB_ECU_Project_B.s67_Merge;
  }

  /* Gain: '<S60>/Gain' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S60>/motohawk_calibration'
   */
  rtb_DataTypeConversion_n = 0.001 * (IdleSpeed_k_p_DataStore());

  /* Sum: '<S60>/Sum4' incorporates:
   *  S-Function (motohawk_sfun_data_read): '<S5>/motohawk_data_read1'
   *  S-Function (motohawk_sfun_data_read): '<S5>/motohawk_data_read8'
   */
  rtb_Sum4_m = IdleSpeedSP_rpm_DataStore() - EngineSpeed_rpm_DataStore();

  /* Product: '<S60>/Product2' */
  CRVLAB_ECU_Project_B.s60_Product2 = rtb_DataTypeConversion_n * rtb_Sum4_m;

  /* UnitDelay: '<S60>/Unit Delay1' */
  rtb_UnitDelay1_i = CRVLAB_ECU_Project_DWork.s60_UnitDelay1_DSTATE;

  /* Product: '<S60>/Product3' incorporates:
   *  Gain: '<S60>/Gain1'
   *  S-Function (motohawk_sfun_calibration): '<S60>/motohawk_calibration1'
   *  UnitDelay: '<S60>/Unit Delay1'
   */
  CRVLAB_ECU_Project_B.s60_Product3 = 0.001 * (IdleSpeed_k_i_DataStore()) *
    CRVLAB_ECU_Project_DWork.s60_UnitDelay1_DSTATE;

  /* Sum: '<S60>/Sum2' incorporates:
   *  UnitDelay: '<S60>/Unit Delay'
   */
  rtb_DataTypeConversion_n = rtb_Sum4_m -
    CRVLAB_ECU_Project_DWork.s60_UnitDelay_DSTATE;

  /* S-Function Block: <S60>/motohawk_delta_time */
  {
    uint32_T delta;
    extern uint32_T Timer_FreeRunningCounter_GetDeltaUpdateReference_us(uint32_T
      * pReference_lower32Bits, uint32_T *pReference_upper32Bits);
    delta = Timer_FreeRunningCounter_GetDeltaUpdateReference_us
      (&CRVLAB_ECU_Project_DWork.s60_motohawk_delta_time_DWORK1, NULL);
    rtb_motohawk_delta_time_d = ((real_T) delta) * 0.000001;
  }

  /* Product: '<S60>/Product4' incorporates:
   *  Product: '<S60>/Product'
   *  S-Function (motohawk_sfun_calibration): '<S60>/motohawk_calibration2'
   *  S-Function (motohawk_sfun_delta_time): '<S60>/motohawk_delta_time'
   */
  CRVLAB_ECU_Project_B.s60_Product4 = rtb_DataTypeConversion_n /
    rtb_motohawk_delta_time_d * (IdleSpeed_k_d_DataStore());

  /* Sum: '<S60>/Add' */
  rtb_Add_h = (CRVLAB_ECU_Project_B.s60_Product2 +
               CRVLAB_ECU_Project_B.s60_Product3) +
    CRVLAB_ECU_Project_B.s60_Product4;

  /* RelationalOperator: '<S71>/Compare' incorporates:
   *  Constant: '<S71>/Constant'
   *  S-Function (motohawk_sfun_data_read): '<S5>/motohawk_data_read'
   */
  rtb_Compare_o = (((uint8_T)State_DataStore()) == 5);

  /* Switch: '<S60>/Switch1' incorporates:
   *  Constant: '<S60>/Constant1'
   *  Logic: '<S60>/Logical Operator1'
   *  S-Function (motohawk_sfun_calibration): '<S60>/motohawk_calibration7'
   */
  if ((rtb_Compare_o != 0) && ((IdleSpeed_on_DataStore()) != 0.0)) {
    /* MinMax: '<S73>/MinMax1' incorporates:
     *  MinMax: '<S73>/MinMax'
     *  S-Function (motohawk_sfun_calibration): '<S60>/motohawk_calibration5'
     *  S-Function (motohawk_sfun_calibration): '<S60>/motohawk_calibration6'
     */
    rtb_DataTypeConversion_n = (rtb_Add_h >= (IdleSpeed_u_min_DataStore())) ||
      rtIsNaN((IdleSpeed_u_min_DataStore())) ? rtb_Add_h :
      (IdleSpeed_u_min_DataStore());
    rtb_DataTypeConversion_n = (rtb_DataTypeConversion_n <=
      (IdleSpeed_u_max_DataStore())) || rtIsNaN((IdleSpeed_u_max_DataStore())) ?
      rtb_DataTypeConversion_n : (IdleSpeed_u_max_DataStore());
  } else {
    rtb_DataTypeConversion_n = 0.0;
  }

  /* End of Switch: '<S60>/Switch1' */
  /* If: '<S74>/If' incorporates:
   *  Inport: '<S75>/In1'
   *  Inport: '<S76>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S74>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S74>/override_enable'
   */
  if ((IdleSpeed_u_Override_Absolute_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S74>/NewValue' incorporates:
     *  ActionPort: '<S75>/Action Port'
     */
    CRVLAB_ECU_Project_B.s74_Merge =
      (IdleSpeed_u_Override_Absolute_new_DataStore());

    /* End of Outputs for SubSystem: '<S74>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S74>/OldValue' incorporates:
     *  ActionPort: '<S76>/Action Port'
     */
    CRVLAB_ECU_Project_B.s74_Merge = rtb_DataTypeConversion_n;

    /* End of Outputs for SubSystem: '<S74>/OldValue' */
  }

  /* End of If: '<S74>/If' */

  /* Sum: '<S5>/Add' incorporates:
   *  S-Function (motohawk_sfun_data_read): '<S5>/motohawk_data_read9'
   */
  rtb_Add_hi = CRVLAB_ECU_Project_B.s74_Merge + MainFueling_ms_DataStore();

  /* S-Function (motohawk_sfun_data_write): '<S5>/motohawk_data_write3' */
  /* Write to Data Storage as scalar: MainFueling_ms */
  {
    MainFueling_ms_DataStore() = rtb_Add_hi;
  }

  /* Sum: '<S63>/Sum4' incorporates:
   *  S-Function (motohawk_sfun_data_read): '<S5>/motohawk_data_read10'
   *  S-Function (motohawk_sfun_data_read): '<S5>/motohawk_data_read11'
   */
  rtb_Sum4_e = MAFSP_g_s_DataStore() - MAF_g_s_DataStore();

  /* Product: '<S63>/Product2' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S63>/motohawk_calibration'
   */
  CRVLAB_ECU_Project_B.s63_Product2 = (VNT_k_p_DataStore()) * rtb_Sum4_e;

  /* UnitDelay: '<S63>/Unit Delay1' */
  rtb_UnitDelay1_em = CRVLAB_ECU_Project_DWork.s63_UnitDelay1_DSTATE;

  /* Product: '<S63>/Product3' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S63>/motohawk_calibration1'
   *  UnitDelay: '<S63>/Unit Delay1'
   */
  CRVLAB_ECU_Project_B.s63_Product3 = (VNT_k_i_DataStore()) *
    CRVLAB_ECU_Project_DWork.s63_UnitDelay1_DSTATE;

  /* Sum: '<S63>/Sum2' incorporates:
   *  UnitDelay: '<S63>/Unit Delay'
   */
  rtb_DataTypeConversion_n = rtb_Sum4_e -
    CRVLAB_ECU_Project_DWork.s63_UnitDelay_DSTATE;

  /* S-Function Block: <S63>/motohawk_delta_time */
  {
    uint32_T delta;
    extern uint32_T Timer_FreeRunningCounter_GetDeltaUpdateReference_us(uint32_T
      * pReference_lower32Bits, uint32_T *pReference_upper32Bits);
    delta = Timer_FreeRunningCounter_GetDeltaUpdateReference_us
      (&CRVLAB_ECU_Project_DWork.s63_motohawk_delta_time_DWORK1, NULL);
    rtb_motohawk_delta_time_l = ((real_T) delta) * 0.000001;
  }

  /* Product: '<S63>/Product4' incorporates:
   *  Product: '<S63>/Product'
   *  S-Function (motohawk_sfun_calibration): '<S63>/motohawk_calibration2'
   *  S-Function (motohawk_sfun_delta_time): '<S63>/motohawk_delta_time'
   */
  CRVLAB_ECU_Project_B.s63_Product4 = rtb_DataTypeConversion_n /
    rtb_motohawk_delta_time_l * (VNT_k_d_DataStore());

  /* Sum: '<S63>/Add' */
  rtb_DataTypeConversion_n = (CRVLAB_ECU_Project_B.s63_Product2 +
    CRVLAB_ECU_Project_B.s63_Product3) + CRVLAB_ECU_Project_B.s63_Product4;

  /* Sum: '<S63>/Sum5' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S63>/motohawk_calibration7'
   */
  rtb_Product_da = rtb_DataTypeConversion_n + (VNT_Mean_DC_DataStore());

  /* MinMax: '<S92>/MinMax' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S63>/motohawk_calibration5'
   */
  rtb_motohawk_data_read1_bu = (rtb_Product_da >= (VNT_u_min_DataStore())) ||
    rtIsNaN((VNT_u_min_DataStore())) ? rtb_Product_da : (VNT_u_min_DataStore());

  /* MinMax: '<S92>/MinMax1' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S63>/motohawk_calibration6'
   */
  rtb_DataTypeConversion_n = (rtb_motohawk_data_read1_bu <= (VNT_u_max_DataStore
                               ())) || rtIsNaN((VNT_u_max_DataStore())) ?
    rtb_motohawk_data_read1_bu : (VNT_u_max_DataStore());

  /* If: '<S93>/If' incorporates:
   *  Inport: '<S94>/In1'
   *  Inport: '<S95>/In1'
   *  MinMax: '<S92>/MinMax1'
   *  S-Function (motohawk_sfun_calibration): '<S93>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S93>/override_enable'
   */
  if ((VNT_u_Override_Absolute_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S93>/NewValue' incorporates:
     *  ActionPort: '<S94>/Action Port'
     */
    CRVLAB_ECU_Project_B.s93_Merge = (VNT_u_Override_Absolute_new_DataStore());

    /* End of Outputs for SubSystem: '<S93>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S93>/OldValue' incorporates:
     *  ActionPort: '<S95>/Action Port'
     */
    CRVLAB_ECU_Project_B.s93_Merge = rtb_DataTypeConversion_n;

    /* End of Outputs for SubSystem: '<S93>/OldValue' */
  }

  /* End of If: '<S93>/If' */

  /* S-Function (motohawk_sfun_data_write): '<S5>/motohawk_data_write4' */
  /* Write to Data Storage as scalar: VNT_ADC */
  {
    VNT_ADC_DataStore() = CRVLAB_ECU_Project_B.s93_Merge;
  }

  /* RelationalOperator: '<S64>/Compare' incorporates:
   *  Constant: '<S64>/Constant'
   *  S-Function (motohawk_sfun_data_read): '<S5>/motohawk_data_read'
   */
  rtb_Compare_o = (((uint8_T)State_DataStore()) == 1);

  /* Logic: '<S59>/Logical Operator' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S59>/motohawk_calibration3'
   */
  rtb_LogicalOperator = (((EGR_reset_DataStore()) != 0.0) || (rtb_Compare_o != 0));

  /* MinMax: '<S65>/MinMax' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S59>/motohawk_calibration5'
   */
  rtb_motohawk_data_read1_bu = (rtb_Gain4 >= (EGR_u_min_DataStore())) || rtIsNaN
    ((EGR_u_min_DataStore())) ? rtb_Gain4 : (EGR_u_min_DataStore());

  /* MinMax: '<S65>/MinMax1' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S59>/motohawk_calibration6'
   */
  rtb_DataTypeConversion_n = (rtb_motohawk_data_read1_bu <= (EGR_u_max_DataStore
                               ())) || rtIsNaN((EGR_u_max_DataStore())) ?
    rtb_motohawk_data_read1_bu : (EGR_u_max_DataStore());

  /* Switch: '<S59>/Switch' incorporates:
   *  Constant: '<S59>/Constant'
   */
  if (rtb_LogicalOperator) {
    rtb_motohawk_data_read1_bu = 0.0;
  } else {
    rtb_motohawk_data_read1_bu = rtb_UnitDelay1_g;
  }

  /* End of Switch: '<S59>/Switch' */

  /* Sum: '<S59>/Sum' incorporates:
   *  MinMax: '<S65>/MinMax1'
   *  Product: '<S59>/Product1'
   *  Product: '<S59>/Product5'
   *  S-Function (motohawk_sfun_calibration): '<S59>/motohawk_calibration4'
   *  S-Function (motohawk_sfun_delta_time): '<S59>/motohawk_delta_time'
   *  Sum: '<S59>/Sum1'
   *  Sum: '<S59>/Sum3'
   */
  rtb_Sum_g = ((rtb_DataTypeConversion_n - rtb_Gain4) * (EGR_k_w_DataStore()) +
               rtb_Sum4_j) * rtb_motohawk_delta_time_c +
    rtb_motohawk_data_read1_bu;

  /* RelationalOperator: '<S70>/Compare' incorporates:
   *  Constant: '<S70>/Constant'
   *  S-Function (motohawk_sfun_data_read): '<S5>/motohawk_data_read'
   */
  rtb_Compare_o = (((uint8_T)State_DataStore()) != 5);

  /* Logic: '<S60>/Logical Operator' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S60>/motohawk_calibration3'
   */
  rtb_LogicalOperator = (((IdleSpeed_reset_DataStore()) != 0.0) ||
    (rtb_Compare_o != 0));

  /* MinMax: '<S72>/MinMax' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S60>/motohawk_calibration5'
   */
  rtb_motohawk_data_read1_bu = (rtb_Add_h >= (IdleSpeed_u_min_DataStore())) ||
    rtIsNaN((IdleSpeed_u_min_DataStore())) ? rtb_Add_h :
    (IdleSpeed_u_min_DataStore());

  /* MinMax: '<S72>/MinMax1' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S60>/motohawk_calibration6'
   */
  rtb_DataTypeConversion_n = (rtb_motohawk_data_read1_bu <=
    (IdleSpeed_u_max_DataStore())) || rtIsNaN((IdleSpeed_u_max_DataStore())) ?
    rtb_motohawk_data_read1_bu : (IdleSpeed_u_max_DataStore());

  /* Switch: '<S60>/Switch' incorporates:
   *  Constant: '<S60>/Constant'
   */
  if (rtb_LogicalOperator) {
    rtb_motohawk_data_read1_bu = 0.0;
  } else {
    rtb_motohawk_data_read1_bu = rtb_UnitDelay1_i;
  }

  /* End of Switch: '<S60>/Switch' */

  /* Sum: '<S60>/Sum' incorporates:
   *  MinMax: '<S72>/MinMax1'
   *  Product: '<S60>/Product1'
   *  Product: '<S60>/Product5'
   *  S-Function (motohawk_sfun_calibration): '<S60>/motohawk_calibration4'
   *  S-Function (motohawk_sfun_delta_time): '<S60>/motohawk_delta_time'
   *  Sum: '<S60>/Sum1'
   *  Sum: '<S60>/Sum3'
   */
  rtb_Sum_n = ((rtb_DataTypeConversion_n - rtb_Add_h) * (IdleSpeed_k_w_DataStore
                ()) + rtb_Sum4_m) * rtb_motohawk_delta_time_d +
    rtb_motohawk_data_read1_bu;

  /* RelationalOperator: '<S77>/Compare' incorporates:
   *  Constant: '<S77>/Constant'
   *  S-Function (motohawk_sfun_data_read): '<S5>/motohawk_data_read'
   */
  rtb_Compare_o = (((uint8_T)State_DataStore()) == 1);

  /* Logic: '<S61>/Logical Operator' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S61>/motohawk_calibration3'
   */
  rtb_LogicalOperator = (((RailP_reset_DataStore()) != 0.0) || (rtb_Compare_o !=
    0));

  /* MinMax: '<S79>/MinMax' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S61>/motohawk_calibration5'
   */
  rtb_motohawk_data_read1_bu = (rtb_Gain5 >= (RailP_u_min_DataStore())) ||
    rtIsNaN((RailP_u_min_DataStore())) ? rtb_Gain5 : (RailP_u_min_DataStore());

  /* Sum: '<S61>/Sum1' incorporates:
   *  MinMax: '<S79>/MinMax1'
   *  S-Function (motohawk_sfun_calibration): '<S61>/motohawk_calibration6'
   */
  rtb_DataTypeConversion_n = ((rtb_motohawk_data_read1_bu <=
    (RailP_u_max_DataStore())) || rtIsNaN((RailP_u_max_DataStore())) ?
    rtb_motohawk_data_read1_bu : (RailP_u_max_DataStore())) - rtb_Gain5;

  /* Switch: '<S61>/Switch' incorporates:
   *  Constant: '<S61>/Constant'
   */
  if (rtb_LogicalOperator) {
    rtb_motohawk_data_read1_bu = 0.0;
  } else {
    rtb_motohawk_data_read1_bu = rtb_UnitDelay1_d;
  }

  /* End of Switch: '<S61>/Switch' */

  /* Sum: '<S61>/Sum' incorporates:
   *  Product: '<S61>/Product1'
   *  Product: '<S61>/Product5'
   *  S-Function (motohawk_sfun_calibration): '<S61>/motohawk_calibration4'
   *  S-Function (motohawk_sfun_delta_time): '<S61>/motohawk_delta_time'
   *  Sum: '<S61>/Sum3'
   */
  rtb_UnitDelay1_g = (rtb_DataTypeConversion_n * (RailP_k_w_DataStore()) +
                      rtb_Sum4) * rtb_motohawk_delta_time +
    rtb_motohawk_data_read1_bu;

  /* RelationalOperator: '<S84>/Compare' incorporates:
   *  Constant: '<S84>/Constant'
   *  S-Function (motohawk_sfun_data_read): '<S5>/motohawk_data_read'
   */
  rtb_Compare_o = (((uint8_T)State_DataStore()) == 1);

  /* Logic: '<S62>/Logical Operator' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S62>/motohawk_calibration3'
   */
  rtb_LogicalOperator = (((Throttle_reset_DataStore()) != 0.0) || (rtb_Compare_o
    != 0));

  /* MinMax: '<S85>/MinMax' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S62>/motohawk_calibration5'
   */
  rtb_motohawk_data_read1_bu = (rtb_Sum5 >= (Throttle_u_min_DataStore())) ||
    rtIsNaN((Throttle_u_min_DataStore())) ? rtb_Sum5 : (Throttle_u_min_DataStore
    ());

  /* Sum: '<S62>/Sum1' incorporates:
   *  MinMax: '<S85>/MinMax1'
   *  S-Function (motohawk_sfun_calibration): '<S62>/motohawk_calibration6'
   */
  rtb_DataTypeConversion_n = ((rtb_motohawk_data_read1_bu <=
    (Throttle_u_max_DataStore())) || rtIsNaN((Throttle_u_max_DataStore())) ?
    rtb_motohawk_data_read1_bu : (Throttle_u_max_DataStore())) - rtb_Sum5;

  /* Switch: '<S62>/Switch' incorporates:
   *  Constant: '<S62>/Constant'
   */
  if (rtb_LogicalOperator) {
    rtb_motohawk_data_read1_bu = 0.0;
  } else {
    rtb_motohawk_data_read1_bu = rtb_UnitDelay1_e;
  }

  /* End of Switch: '<S62>/Switch' */

  /* Sum: '<S62>/Sum' incorporates:
   *  Product: '<S62>/Product1'
   *  Product: '<S62>/Product5'
   *  S-Function (motohawk_sfun_calibration): '<S62>/motohawk_calibration4'
   *  S-Function (motohawk_sfun_delta_time): '<S62>/motohawk_delta_time'
   *  Sum: '<S62>/Sum3'
   */
  rtb_UnitDelay1_i = (rtb_DataTypeConversion_n * (Throttle_k_w_DataStore()) +
                      rtb_Sum4_a) * rtb_motohawk_delta_time_n +
    rtb_motohawk_data_read1_bu;

  /* RelationalOperator: '<S90>/Compare' incorporates:
   *  Constant: '<S90>/Constant'
   *  S-Function (motohawk_sfun_data_read): '<S5>/motohawk_data_read'
   */
  rtb_Compare_o = (((uint8_T)State_DataStore()) == 1);

  /* Logic: '<S63>/Logical Operator' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S63>/motohawk_calibration3'
   */
  rtb_LogicalOperator = (((VNT_reset_DataStore()) != 0.0) || (rtb_Compare_o != 0));

  /* MinMax: '<S91>/MinMax' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S63>/motohawk_calibration5'
   */
  rtb_motohawk_data_read1_bu = (rtb_Product_da >= (VNT_u_min_DataStore())) ||
    rtIsNaN((VNT_u_min_DataStore())) ? rtb_Product_da : (VNT_u_min_DataStore());

  /* MinMax: '<S91>/MinMax1' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S63>/motohawk_calibration6'
   */
  rtb_DataTypeConversion_n = (rtb_motohawk_data_read1_bu <= (VNT_u_max_DataStore
                               ())) || rtIsNaN((VNT_u_max_DataStore())) ?
    rtb_motohawk_data_read1_bu : (VNT_u_max_DataStore());

  /* Switch: '<S63>/Switch' incorporates:
   *  Constant: '<S63>/Constant'
   */
  if (rtb_LogicalOperator) {
    rtb_motohawk_data_read1_bu = 0.0;
  } else {
    rtb_motohawk_data_read1_bu = rtb_UnitDelay1_em;
  }

  /* End of Switch: '<S63>/Switch' */

  /* Sum: '<S63>/Sum' incorporates:
   *  MinMax: '<S91>/MinMax1'
   *  Product: '<S63>/Product1'
   *  Product: '<S63>/Product5'
   *  S-Function (motohawk_sfun_calibration): '<S63>/motohawk_calibration4'
   *  S-Function (motohawk_sfun_delta_time): '<S63>/motohawk_delta_time'
   *  Sum: '<S63>/Sum1'
   *  Sum: '<S63>/Sum3'
   */
  rtb_Add_h = ((rtb_DataTypeConversion_n - rtb_Product_da) * (VNT_k_w_DataStore())
               + rtb_Sum4_e) * rtb_motohawk_delta_time_l +
    rtb_motohawk_data_read1_bu;

  /* S-Function Block: <S106>/motohawk_delta_time */
  {
    uint32_T delta;
    extern uint32_T Timer_FreeRunningCounter_GetDeltaUpdateReference_us(uint32_T
      * pReference_lower32Bits, uint32_T *pReference_upper32Bits);
    delta = Timer_FreeRunningCounter_GetDeltaUpdateReference_us
      (&CRVLAB_ECU_Project_DWork.s106_motohawk_delta_time_DWORK1, NULL);
    rtb_motohawk_delta_time_j = ((real_T) delta) * 0.000001;
  }

  /* S-Function (motohawk_sfun_data_read): '<S96>/motohawk_data_read' */
  rtb_motohawk_data_read1_e = State_DataStore();

  /* Switch: '<S106>/Switch' incorporates:
   *  Constant: '<S106>/Constant'
   *  Constant: '<S96>/Constant'
   *  RelationalOperator: '<S96>/Relational Operator1'
   *  S-Function (motohawk_sfun_data_read): '<S106>/motohawk_data_read'
   *  S-Function (motohawk_sfun_delta_time): '<S106>/motohawk_delta_time'
   *  Sum: '<S106>/Sum'
   */
  if (rtb_motohawk_data_read1_e >= 2) {
    rtb_motohawk_data_read1_bu = rtb_motohawk_delta_time_j +
      TimeSinceCngInjEnabledl_DataStore();
  } else {
    rtb_motohawk_data_read1_bu = 0.0;
  }

  /* End of Switch: '<S106>/Switch' */
  /* RelationalOperator: '<S96>/Relational Operator5' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S96>/motohawk_calibration3'
   */
  CRVLAB_ECU_Project_B.s96_RelationalOperator5 = ((rtb_motohawk_data_read1_bu >=
    (TimeBeforeOnGasInj_DataStore())));

  /* S-Function (motohawk_sfun_data_read): '<S96>/motohawk_data_read1' */
  rtb_motohawk_data_read1_bo = CNGSOI_degBTDC_DataStore();

  /* If: '<S107>/If' incorporates:
   *  Inport: '<S109>/In1'
   *  Inport: '<S110>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S107>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S107>/override_enable'
   */
  if ((CNGSOIOverride_degBTDC_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S107>/NewValue' incorporates:
     *  ActionPort: '<S109>/Action Port'
     */
    rtb_DataTypeConversion_n = (CNGSOIOverride_degBTDC_new_DataStore());

    /* End of Outputs for SubSystem: '<S107>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S107>/OldValue' incorporates:
     *  ActionPort: '<S110>/Action Port'
     */
    rtb_DataTypeConversion_n = rtb_motohawk_data_read1_bo;

    /* End of Outputs for SubSystem: '<S107>/OldValue' */
  }

  /* End of If: '<S107>/If' */

  /* DataTypeConversion: '<S96>/Data Type Conversion' incorporates:
   *  Gain: '<S96>/Gain'
   */
  rtb_DataTypeConversion_n *= 16.0;
  if (rtb_DataTypeConversion_n < 32768.0) {
    if (rtb_DataTypeConversion_n >= -32768.0) {
      rtb_DataTypeConversion4 = (int16_T)rtb_DataTypeConversion_n;
    } else {
      rtb_DataTypeConversion4 = MIN_int16_T;
    }
  } else {
    rtb_DataTypeConversion4 = MAX_int16_T;
  }

  /* End of DataTypeConversion: '<S96>/Data Type Conversion' */

  /* S-Function (motohawk_sfun_data_read): '<S96>/motohawk_data_read2' */
  rtb_motohawk_data_read2_b = CNGFueling_ms_DataStore();

  /* If: '<S108>/If' incorporates:
   *  Inport: '<S111>/In1'
   *  Inport: '<S112>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S108>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S108>/override_enable'
   */
  if ((CNGFuelingOverride_ms_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S108>/NewValue' incorporates:
     *  ActionPort: '<S111>/Action Port'
     */
    rtb_DataTypeConversion_n = (CNGFuelingOverride_ms_new_DataStore());

    /* End of Outputs for SubSystem: '<S108>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S108>/OldValue' incorporates:
     *  ActionPort: '<S112>/Action Port'
     */
    rtb_DataTypeConversion_n = rtb_motohawk_data_read2_b;

    /* End of Outputs for SubSystem: '<S108>/OldValue' */
  }

  /* End of If: '<S108>/If' */

  /* DataTypeConversion: '<S96>/Data Type Conversion1' incorporates:
   *  Gain: '<S96>/Gain1'
   */
  rtb_DataTypeConversion_n *= 1000.0;
  if (rtb_DataTypeConversion_n < 4.294967296E+9) {
    if (rtb_DataTypeConversion_n >= 0.0) {
      rtb_DataTypeConversion5 = (uint32_T)rtb_DataTypeConversion_n;
    } else {
      rtb_DataTypeConversion5 = 0U;
    }
  } else {
    rtb_DataTypeConversion5 = MAX_uint32_T;
  }

  /* End of DataTypeConversion: '<S96>/Data Type Conversion1' */
  /* DataTypeConversion: '<S96>/Data Type Conversion2' incorporates:
   *  Gain: '<S96>/Gain6'
   *  S-Function (motohawk_sfun_calibration): '<S96>/motohawk_calibration1'
   */
  rtb_DataTypeConversion_n = 1000.0 * (Gas_Peak_Duration_DataStore());
  if (rtb_DataTypeConversion_n < 65536.0) {
    if (rtb_DataTypeConversion_n >= 0.0) {
      rtb_DataTypeConversion2_ib = (uint16_T)rtb_DataTypeConversion_n;
    } else {
      rtb_DataTypeConversion2_ib = 0U;
    }
  } else {
    rtb_DataTypeConversion2_ib = MAX_uint16_T;
  }

  /* End of DataTypeConversion: '<S96>/Data Type Conversion2' */
  /* DataTypeConversion: '<S96>/Data Type Conversion3' incorporates:
   *  Gain: '<S96>/Gain4'
   *  S-Function (motohawk_sfun_calibration): '<S96>/motohawk_calibration2'
   */
  rtb_DataTypeConversion_n = 1000.0 * (Gas_Mech_Offset_DataStore());
  if (rtb_DataTypeConversion_n < 4.294967296E+9) {
    if (rtb_DataTypeConversion_n >= 0.0) {
      rtb_DataTypeConversion3_ng = (uint32_T)rtb_DataTypeConversion_n;
    } else {
      rtb_DataTypeConversion3_ng = 0U;
    }
  } else {
    rtb_DataTypeConversion3_ng = MAX_uint32_T;
  }

  /* End of DataTypeConversion: '<S96>/Data Type Conversion3' */

  /* S-Function Block: <S96>/motohawk_injector */
  {
    S_SeqOutPulseTiming TimingObj;
    E_SeqOutCond seq_enable;
    uint32_T elec_duration;
    boolean_T enable;
    S_SeqOutAttributes DynamicObj;

    /* INJ1D Injector Behavior */
    if ((rtb_DataTypeConversion2_ib) !=
        (CRVLAB_ECU_Project_DWork.s96_motohawk_injector_DWORK2)) {
      SetSeqOutInjPeakTime((E_ModuleResource)
                           (CRVLAB_ECU_Project_DWork.s96_motohawk_injector_IWORK),
                           rtb_DataTypeConversion2_ib);
      CRVLAB_ECU_Project_DWork.s96_motohawk_injector_DWORK2 =
        rtb_DataTypeConversion2_ib;
    }

    enable = (CRVLAB_ECU_Project_B.s96_RelationalOperator5 &&
              (GetEncoderResourceInstantRPM() > 10));
    if ((enable) != (CRVLAB_ECU_Project_DWork.s96_motohawk_injector_DWORK1)) {
      seq_enable = (enable) ? SEQ_ENABLED : SEQ_DISNEXT;
      SetSeqOutCond((E_ModuleResource)
                    (CRVLAB_ECU_Project_DWork.s96_motohawk_injector_IWORK), 0,
                    seq_enable);
      CRVLAB_ECU_Project_DWork.s96_motohawk_injector_DWORK1 = enable;
    }

    /* INJ1D Primary Pulse */
    elec_duration = (rtb_DataTypeConversion5) + (rtb_DataTypeConversion3_ng);
    DynamicObj.uValidAttributesMask = USE_SEQ_TIMING ;

    /* Primary Pulse */
    DynamicObj.u1AffectedPulse = 0;
    DynamicObj.TimingObj.s2StartAngle = rtb_DataTypeConversion4;
    DynamicObj.TimingObj.u4Duration = elec_duration;
    DynamicObj.TimingObj.eCtrlMode = DURATION_CTRL;
    SetResourceAttributes((E_ModuleResource)
                          (CRVLAB_ECU_Project_DWork.s96_motohawk_injector_IWORK),
                          &DynamicObj, BEHAVIOUR_INJ_SEQ);
  }

  /* S-Function Block: <S96>/motohawk_injector1 */
  {
    S_SeqOutPulseTiming TimingObj;
    E_SeqOutCond seq_enable;
    uint32_T elec_duration;
    boolean_T enable;
    S_SeqOutAttributes DynamicObj;

    /* INJ2D Injector Behavior */
    if ((rtb_DataTypeConversion2_ib) !=
        (CRVLAB_ECU_Project_DWork.s96_motohawk_injector1_DWORK2)) {
      SetSeqOutInjPeakTime((E_ModuleResource)
                           (CRVLAB_ECU_Project_DWork.s96_motohawk_injector1_IWORK),
                           rtb_DataTypeConversion2_ib);
      CRVLAB_ECU_Project_DWork.s96_motohawk_injector1_DWORK2 =
        rtb_DataTypeConversion2_ib;
    }

    enable = (CRVLAB_ECU_Project_B.s96_RelationalOperator5 &&
              (GetEncoderResourceInstantRPM() > 10));
    if ((enable) != (CRVLAB_ECU_Project_DWork.s96_motohawk_injector1_DWORK1)) {
      seq_enable = (enable) ? SEQ_ENABLED : SEQ_DISNEXT;
      SetSeqOutCond((E_ModuleResource)
                    (CRVLAB_ECU_Project_DWork.s96_motohawk_injector1_IWORK), 0,
                    seq_enable);
      CRVLAB_ECU_Project_DWork.s96_motohawk_injector1_DWORK1 = enable;
    }

    /* INJ2D Primary Pulse */
    elec_duration = (rtb_DataTypeConversion5) + (rtb_DataTypeConversion3_ng);
    DynamicObj.uValidAttributesMask = USE_SEQ_TIMING ;

    /* Primary Pulse */
    DynamicObj.u1AffectedPulse = 0;
    DynamicObj.TimingObj.s2StartAngle = rtb_DataTypeConversion4;
    DynamicObj.TimingObj.u4Duration = elec_duration;
    DynamicObj.TimingObj.eCtrlMode = DURATION_CTRL;
    SetResourceAttributes((E_ModuleResource)
                          (CRVLAB_ECU_Project_DWork.s96_motohawk_injector1_IWORK),
                          &DynamicObj, BEHAVIOUR_INJ_SEQ);
  }

  /* S-Function Block: <S96>/motohawk_injector2 */
  {
    S_SeqOutPulseTiming TimingObj;
    E_SeqOutCond seq_enable;
    uint32_T elec_duration;
    boolean_T enable;
    S_SeqOutAttributes DynamicObj;

    /* INJ3D Injector Behavior */
    if ((rtb_DataTypeConversion2_ib) !=
        (CRVLAB_ECU_Project_DWork.s96_motohawk_injector2_DWORK2)) {
      SetSeqOutInjPeakTime((E_ModuleResource)
                           (CRVLAB_ECU_Project_DWork.s96_motohawk_injector2_IWORK),
                           rtb_DataTypeConversion2_ib);
      CRVLAB_ECU_Project_DWork.s96_motohawk_injector2_DWORK2 =
        rtb_DataTypeConversion2_ib;
    }

    enable = (CRVLAB_ECU_Project_B.s96_RelationalOperator5 &&
              (GetEncoderResourceInstantRPM() > 10));
    if ((enable) != (CRVLAB_ECU_Project_DWork.s96_motohawk_injector2_DWORK1)) {
      seq_enable = (enable) ? SEQ_ENABLED : SEQ_DISNEXT;
      SetSeqOutCond((E_ModuleResource)
                    (CRVLAB_ECU_Project_DWork.s96_motohawk_injector2_IWORK), 0,
                    seq_enable);
      CRVLAB_ECU_Project_DWork.s96_motohawk_injector2_DWORK1 = enable;
    }

    /* INJ3D Primary Pulse */
    elec_duration = (rtb_DataTypeConversion5) + (rtb_DataTypeConversion3_ng);
    DynamicObj.uValidAttributesMask = USE_SEQ_TIMING ;

    /* Primary Pulse */
    DynamicObj.u1AffectedPulse = 0;
    DynamicObj.TimingObj.s2StartAngle = rtb_DataTypeConversion4;
    DynamicObj.TimingObj.u4Duration = elec_duration;
    DynamicObj.TimingObj.eCtrlMode = DURATION_CTRL;
    SetResourceAttributes((E_ModuleResource)
                          (CRVLAB_ECU_Project_DWork.s96_motohawk_injector2_IWORK),
                          &DynamicObj, BEHAVIOUR_INJ_SEQ);
  }

  /* S-Function Block: <S96>/motohawk_injector3 */
  {
    S_SeqOutPulseTiming TimingObj;
    E_SeqOutCond seq_enable;
    uint32_T elec_duration;
    boolean_T enable;
    S_SeqOutAttributes DynamicObj;

    /* INJ4D Injector Behavior */
    if ((rtb_DataTypeConversion2_ib) !=
        (CRVLAB_ECU_Project_DWork.s96_motohawk_injector3_DWORK2)) {
      SetSeqOutInjPeakTime((E_ModuleResource)
                           (CRVLAB_ECU_Project_DWork.s96_motohawk_injector3_IWORK),
                           rtb_DataTypeConversion2_ib);
      CRVLAB_ECU_Project_DWork.s96_motohawk_injector3_DWORK2 =
        rtb_DataTypeConversion2_ib;
    }

    enable = (CRVLAB_ECU_Project_B.s96_RelationalOperator5 &&
              (GetEncoderResourceInstantRPM() > 10));
    if ((enable) != (CRVLAB_ECU_Project_DWork.s96_motohawk_injector3_DWORK1)) {
      seq_enable = (enable) ? SEQ_ENABLED : SEQ_DISNEXT;
      SetSeqOutCond((E_ModuleResource)
                    (CRVLAB_ECU_Project_DWork.s96_motohawk_injector3_IWORK), 0,
                    seq_enable);
      CRVLAB_ECU_Project_DWork.s96_motohawk_injector3_DWORK1 = enable;
    }

    /* INJ4D Primary Pulse */
    elec_duration = (rtb_DataTypeConversion5) + (rtb_DataTypeConversion3_ng);
    DynamicObj.uValidAttributesMask = USE_SEQ_TIMING ;

    /* Primary Pulse */
    DynamicObj.u1AffectedPulse = 0;
    DynamicObj.TimingObj.s2StartAngle = rtb_DataTypeConversion4;
    DynamicObj.TimingObj.u4Duration = elec_duration;
    DynamicObj.TimingObj.eCtrlMode = DURATION_CTRL;
    SetResourceAttributes((E_ModuleResource)
                          (CRVLAB_ECU_Project_DWork.s96_motohawk_injector3_IWORK),
                          &DynamicObj, BEHAVIOUR_INJ_SEQ);
  }

  /* Saturate: '<S106>/Saturation' */
  rtb_Saturation_n = rtb_motohawk_data_read1_bu >= 16000.0 ? 16000.0 :
    rtb_motohawk_data_read1_bu <= 0.0 ? 0.0 : rtb_motohawk_data_read1_bu;

  /* S-Function (motohawk_sfun_data_write): '<S106>/motohawk_data_write' */
  /* Write to Data Storage as scalar: TimeSinceCngInjEnabledl */
  {
    TimeSinceCngInjEnabledl_DataStore() = rtb_Saturation_n;
  }

  /* DataTypeConversion: '<S97>/Data Type Conversion' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S97>/motohawk_calibration2'
   */
  rtb_DataTypeConversion_h = ((CNGLevel_DataStore()) != 0.0);

  /* Switch: '<S98>/Switch2' incorporates:
   *  Constant: '<S113>/Constant'
   *  Constant: '<S98>/Constant1'
   *  Constant: '<S98>/Constant2'
   *  Product: '<S98>/Product'
   *  RelationalOperator: '<S113>/Compare'
   *  S-Function (motohawk_sfun_data_read): '<S98>/motohawk_data_read'
   *  S-Function (motohawk_sfun_data_read): '<S98>/motohawk_data_read1'
   */
  if ((((uint8_T)State_DataStore()) == 1) > 0) {
    rtb_motohawk_data_read1_bu = 0.0;
  } else {
    rtb_motohawk_data_read1_bu = RailP_DC_DataStore() * 40.96;
  }

  /* End of Switch: '<S98>/Switch2' */

  /* DataTypeConversion: '<S98>/Data Type Conversion' */
  if (rtIsNaN(rtb_motohawk_data_read1_bu) || rtIsInf(rtb_motohawk_data_read1_bu))
  {
    rtb_DataTypeConversion_n = 0.0;
  } else {
    rtb_DataTypeConversion_n = fmod(floor(rtb_motohawk_data_read1_bu), 65536.0);
  }

  rtb_DataTypeConversion_d = (int16_T)(rtb_DataTypeConversion_n < 0.0 ? (int16_T)
    -(int16_T)(uint16_T)-rtb_DataTypeConversion_n : (int16_T)(uint16_T)
    rtb_DataTypeConversion_n);

  /* End of DataTypeConversion: '<S98>/Data Type Conversion' */
  /* DataTypeConversion: '<S98>/Data Type Conversion1' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S98>/motohawk_calibration2'
   */
  if (rtIsNaNF((SCV_Frequency_DataStore())) || rtIsInfF((SCV_Frequency_DataStore
        ()))) {
    tmp = 0.0F;
  } else {
    tmp = (real32_T)fmod((real32_T)floor((SCV_Frequency_DataStore())),
                         4.2949673E+9F);
  }

  rtb_DataTypeConversion1 = tmp < 0.0F ? (uint32_T)-(int32_T)(uint32_T)-tmp :
    (uint32_T)tmp;

  /* End of DataTypeConversion: '<S98>/Data Type Conversion1' */
  /* Switch: '<S99>/Switch2' incorporates:
   *  Constant: '<S118>/Constant'
   *  Constant: '<S99>/Constant1'
   *  Constant: '<S99>/Constant3'
   *  RelationalOperator: '<S118>/Compare'
   *  S-Function (motohawk_sfun_data_read): '<S99>/motohawk_data_read'
   */
  if ((((uint8_T)State_DataStore()) == 1) > 0) {
    rtb_Switch2 = FALSE;
  } else {
    rtb_Switch2 = TRUE;
  }

  /* End of Switch: '<S99>/Switch2' */
  /* Logic: '<S114>/Logical Operator' incorporates:
   *  Constant: '<S114>/Constant1'
   *  RelationalOperator: '<S114>/Relational Operator'
   *  S-Function (motohawk_sfun_encoder_num_cylinders): '<S114>/motohawk_encoder_num_cylinders'
   */
  CRVLAB_ECU_Project_B.s114_LogicalOperator = ((rtb_Switch2 && (1 <= ((uint8_T)
    (EncoderNumCylinders_DataStore())))));

  /* S-Function (motohawk_sfun_data_read): '<S99>/motohawk_data_read1' */
  rtb_motohawk_data_read1_n = PreSOI_degBTDC_DataStore();

  /* If: '<S125>/If' incorporates:
   *  Inport: '<S134>/In1'
   *  Inport: '<S135>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S125>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S125>/override_enable'
   */
  if ((Pre_SOI_Override_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S125>/NewValue' incorporates:
     *  ActionPort: '<S134>/Action Port'
     */
    CRVLAB_ECU_Project_B.s125_Merge = (Pre_SOI_Override_new_DataStore());

    /* End of Outputs for SubSystem: '<S125>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S125>/OldValue' incorporates:
     *  ActionPort: '<S135>/Action Port'
     */
    CRVLAB_ECU_Project_B.s125_Merge = rtb_motohawk_data_read1_n;

    /* End of Outputs for SubSystem: '<S125>/OldValue' */
  }

  /* End of If: '<S125>/If' */

  /* Abs: '<S119>/Abs' */
  rtb_motohawk_data_read1_bu = fabs(CRVLAB_ECU_Project_B.s125_Merge);

  /* RelationalOperator: '<S119>/Relational Operator' incorporates:
   *  Constant: '<S119>/Constant'
   */
  rtb_LogicalOperator = (rtb_motohawk_data_read1_bu < 0.001);

  /* S-Function (motohawk_sfun_data_read): '<S99>/motohawk_data_read4' */
  rtb_motohawk_data_read4_j = PreFueling_ms_DataStore();

  /* If: '<S122>/If' incorporates:
   *  Inport: '<S128>/In1'
   *  Inport: '<S129>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S122>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S122>/override_enable'
   */
  if ((Pre_Duration_Override_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S122>/NewValue' incorporates:
     *  ActionPort: '<S128>/Action Port'
     */
    CRVLAB_ECU_Project_B.s122_Merge = (Pre_Duration_Override_new_DataStore());

    /* End of Outputs for SubSystem: '<S122>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S122>/OldValue' incorporates:
     *  ActionPort: '<S129>/Action Port'
     */
    CRVLAB_ECU_Project_B.s122_Merge = rtb_motohawk_data_read4_j;

    /* End of Outputs for SubSystem: '<S122>/OldValue' */
  }

  /* End of If: '<S122>/If' */

  /* Abs: '<S119>/Abs1' */
  rtb_motohawk_data_read1_bu = fabs(CRVLAB_ECU_Project_B.s122_Merge);

  /* Switch: '<S119>/Switch' incorporates:
   *  Constant: '<S119>/Constant1'
   *  Constant: '<S119>/Constant2'
   *  Logic: '<S119>/Logical Operator'
   *  RelationalOperator: '<S119>/Relational Operator1'
   *  Sum: '<S119>/Add'
   */
  if (rtb_LogicalOperator && (rtb_motohawk_data_read1_bu < 0.001)) {
    rtb_motohawk_data_read1_bu = 100.0 + CRVLAB_ECU_Project_B.s125_Merge;
  } else {
    rtb_motohawk_data_read1_bu = CRVLAB_ECU_Project_B.s125_Merge;
  }

  /* End of Switch: '<S119>/Switch' */

  /* DataTypeConversion: '<S114>/Data Type Conversion' incorporates:
   *  Gain: '<S114>/Gain'
   */
  rtb_DataTypeConversion_n = 16.0 * rtb_motohawk_data_read1_bu;
  if (rtb_DataTypeConversion_n < 32768.0) {
    if (rtb_DataTypeConversion_n >= -32768.0) {
      rtb_DataTypeConversion4 = (int16_T)rtb_DataTypeConversion_n;
    } else {
      rtb_DataTypeConversion4 = MIN_int16_T;
    }
  } else {
    rtb_DataTypeConversion4 = MAX_int16_T;
  }

  /* End of DataTypeConversion: '<S114>/Data Type Conversion' */

  /* S-Function (motohawk_sfun_data_read): '<S99>/motohawk_data_read2' */
  rtb_motohawk_data_read2_oa = PilotSOI_degBTDC_DataStore();

  /* If: '<S126>/If' incorporates:
   *  Inport: '<S136>/In1'
   *  Inport: '<S137>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S126>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S126>/override_enable'
   */
  if ((Pilot_SOI_Override_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S126>/NewValue' incorporates:
     *  ActionPort: '<S136>/Action Port'
     */
    CRVLAB_ECU_Project_B.s126_Merge = (Pilot_SOI_Override_new_DataStore());

    /* End of Outputs for SubSystem: '<S126>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S126>/OldValue' incorporates:
     *  ActionPort: '<S137>/Action Port'
     */
    CRVLAB_ECU_Project_B.s126_Merge = rtb_motohawk_data_read2_oa;

    /* End of Outputs for SubSystem: '<S126>/OldValue' */
  }

  /* End of If: '<S126>/If' */

  /* RelationalOperator: '<S120>/Relational Operator' incorporates:
   *  Abs: '<S120>/Abs'
   *  Constant: '<S120>/Constant'
   */
  rtb_LogicalOperator = (fabs(CRVLAB_ECU_Project_B.s126_Merge) < 0.001);

  /* S-Function (motohawk_sfun_data_read): '<S99>/motohawk_data_read5' */
  rtb_motohawk_data_read5_j = PilotFueling_ms_DataStore();

  /* If: '<S123>/If' incorporates:
   *  Inport: '<S130>/In1'
   *  Inport: '<S131>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S123>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S123>/override_enable'
   */
  if ((Pilot_Duration_Override_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S123>/NewValue' incorporates:
     *  ActionPort: '<S130>/Action Port'
     */
    CRVLAB_ECU_Project_B.s123_Merge = (Pilot_Duration_Override_new_DataStore());

    /* End of Outputs for SubSystem: '<S123>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S123>/OldValue' incorporates:
     *  ActionPort: '<S131>/Action Port'
     */
    CRVLAB_ECU_Project_B.s123_Merge = rtb_motohawk_data_read5_j;

    /* End of Outputs for SubSystem: '<S123>/OldValue' */
  }

  /* End of If: '<S123>/If' */

  /* Switch: '<S120>/Switch' incorporates:
   *  Abs: '<S120>/Abs1'
   *  Constant: '<S120>/Constant1'
   *  Constant: '<S120>/Constant2'
   *  Logic: '<S120>/Logical Operator'
   *  RelationalOperator: '<S120>/Relational Operator1'
   *  Sum: '<S120>/Add'
   */
  if (rtb_LogicalOperator && (fabs(CRVLAB_ECU_Project_B.s123_Merge) < 0.001)) {
    rtb_Product_da = 90.0 + CRVLAB_ECU_Project_B.s126_Merge;
  } else {
    rtb_Product_da = CRVLAB_ECU_Project_B.s126_Merge;
  }

  /* End of Switch: '<S120>/Switch' */

  /* DataTypeConversion: '<S114>/Data Type Conversion2' incorporates:
   *  Gain: '<S114>/Gain2'
   */
  rtb_DataTypeConversion_n = 16.0 * rtb_Product_da;
  if (rtb_DataTypeConversion_n < 32768.0) {
    if (rtb_DataTypeConversion_n >= -32768.0) {
      rtb_DataTypeConversion2_p_0 = (int16_T)rtb_DataTypeConversion_n;
    } else {
      rtb_DataTypeConversion2_p_0 = MIN_int16_T;
    }
  } else {
    rtb_DataTypeConversion2_p_0 = MAX_int16_T;
  }

  /* End of DataTypeConversion: '<S114>/Data Type Conversion2' */

  /* S-Function (motohawk_sfun_data_read): '<S99>/motohawk_data_read3' */
  rtb_motohawk_data_read3_n = MainSOI_degBTDC_DataStore();

  /* If: '<S127>/If' incorporates:
   *  Inport: '<S138>/In1'
   *  Inport: '<S139>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S127>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S127>/override_enable'
   */
  if ((Main_SOI_Override_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S127>/NewValue' incorporates:
     *  ActionPort: '<S138>/Action Port'
     */
    CRVLAB_ECU_Project_B.s127_Merge = (Main_SOI_Override_new_DataStore());

    /* End of Outputs for SubSystem: '<S127>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S127>/OldValue' incorporates:
     *  ActionPort: '<S139>/Action Port'
     */
    CRVLAB_ECU_Project_B.s127_Merge = rtb_motohawk_data_read3_n;

    /* End of Outputs for SubSystem: '<S127>/OldValue' */
  }

  /* End of If: '<S127>/If' */

  /* RelationalOperator: '<S121>/Relational Operator' incorporates:
   *  Abs: '<S121>/Abs'
   *  Constant: '<S121>/Constant'
   */
  rtb_LogicalOperator = (fabs(CRVLAB_ECU_Project_B.s127_Merge) < 0.001);

  /* S-Function (motohawk_sfun_data_read): '<S99>/motohawk_data_read6' */
  rtb_motohawk_data_read6_m = MainFueling_ms_DataStore();

  /* If: '<S124>/If' incorporates:
   *  Inport: '<S132>/In1'
   *  Inport: '<S133>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S124>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S124>/override_enable'
   */
  if ((Main_Duration_Override_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S124>/NewValue' incorporates:
     *  ActionPort: '<S132>/Action Port'
     */
    CRVLAB_ECU_Project_B.s124_Merge = (Main_Duration_Override_new_DataStore());

    /* End of Outputs for SubSystem: '<S124>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S124>/OldValue' incorporates:
     *  ActionPort: '<S133>/Action Port'
     */
    CRVLAB_ECU_Project_B.s124_Merge = rtb_motohawk_data_read6_m;

    /* End of Outputs for SubSystem: '<S124>/OldValue' */
  }

  /* End of If: '<S124>/If' */

  /* Switch: '<S121>/Switch' incorporates:
   *  Abs: '<S121>/Abs1'
   *  Constant: '<S121>/Constant1'
   *  Constant: '<S121>/Constant2'
   *  Logic: '<S121>/Logical Operator'
   *  RelationalOperator: '<S121>/Relational Operator1'
   *  Sum: '<S121>/Add'
   */
  if (rtb_LogicalOperator && (fabs(CRVLAB_ECU_Project_B.s124_Merge) < 0.001)) {
    rtb_Gain4 = -100.0 + CRVLAB_ECU_Project_B.s127_Merge;
  } else {
    rtb_Gain4 = CRVLAB_ECU_Project_B.s127_Merge;
  }

  /* End of Switch: '<S121>/Switch' */

  /* Gain: '<S114>/Gain4' */
  rtb_Gain5 = 16.0 * rtb_Gain4;

  /* SignalConversion: '<S114>/TmpSignal ConversionAtMux PSPInport2' incorporates:
   *  DataTypeConversion: '<S114>/Data Type Conversion2'
   */
  rtb_TmpSignalConversionAtMuxPSPInport2[0] = rtb_DataTypeConversion4;
  rtb_TmpSignalConversionAtMuxPSPInport2[1] = rtb_DataTypeConversion2_p_0;

  /* DataTypeConversion: '<S114>/Data Type Conversion4' */
  if (rtb_Gain5 < 32768.0) {
    if (rtb_Gain5 >= -32768.0) {
      rtb_DataTypeConversion2_p_0 = (int16_T)rtb_Gain5;
    } else {
      rtb_DataTypeConversion2_p_0 = MIN_int16_T;
    }
  } else {
    rtb_DataTypeConversion2_p_0 = MAX_int16_T;
  }

  /* End of DataTypeConversion: '<S114>/Data Type Conversion4' */

  /* SignalConversion: '<S114>/TmpSignal ConversionAtMux PSPInport2' incorporates:
   *  DataTypeConversion: '<S114>/Data Type Conversion4'
   */
  rtb_TmpSignalConversionAtMuxPSPInport2[2] = rtb_DataTypeConversion2_p_0;

  /* Gain: '<S114>/Gain1' */
  rtb_Gain5 = 1000.0 * CRVLAB_ECU_Project_B.s122_Merge;

  /* DataTypeConversion: '<S114>/Data Type Conversion1' */
  if (rtb_Gain5 < 4.294967296E+9) {
    if (rtb_Gain5 >= 0.0) {
      rtb_DataTypeConversion3_ng = (uint32_T)rtb_Gain5;
    } else {
      rtb_DataTypeConversion3_ng = 0U;
    }
  } else {
    rtb_DataTypeConversion3_ng = MAX_uint32_T;
  }

  /* End of DataTypeConversion: '<S114>/Data Type Conversion1' */

  /* Gain: '<S114>/Gain3' */
  rtb_Gain5 = 1000.0 * CRVLAB_ECU_Project_B.s123_Merge;

  /* DataTypeConversion: '<S114>/Data Type Conversion3' */
  if (rtb_Gain5 < 4.294967296E+9) {
    if (rtb_Gain5 >= 0.0) {
      rtb_DataTypeConversion5 = (uint32_T)rtb_Gain5;
    } else {
      rtb_DataTypeConversion5 = 0U;
    }
  } else {
    rtb_DataTypeConversion5 = MAX_uint32_T;
  }

  /* End of DataTypeConversion: '<S114>/Data Type Conversion3' */

  /* Gain: '<S114>/Gain5' */
  rtb_Gain5 = 1000.0 * CRVLAB_ECU_Project_B.s124_Merge;

  /* SignalConversion: '<S114>/TmpSignal ConversionAtMux PSPInport3' */
  rtb_TmpSignalConversionAtMuxPSPInport3[0] = rtb_DataTypeConversion3_ng;
  rtb_TmpSignalConversionAtMuxPSPInport3[1] = rtb_DataTypeConversion5;

  /* DataTypeConversion: '<S114>/Data Type Conversion5' */
  if (rtb_Gain5 < 4.294967296E+9) {
    if (rtb_Gain5 >= 0.0) {
      rtb_DataTypeConversion1_e_0 = (uint32_T)rtb_Gain5;
    } else {
      rtb_DataTypeConversion1_e_0 = 0U;
    }
  } else {
    rtb_DataTypeConversion1_e_0 = MAX_uint32_T;
  }

  /* End of DataTypeConversion: '<S114>/Data Type Conversion5' */

  /* SignalConversion: '<S114>/TmpSignal ConversionAtMux PSPInport3' incorporates:
   *  DataTypeConversion: '<S114>/Data Type Conversion5'
   */
  rtb_TmpSignalConversionAtMuxPSPInport3[2] = rtb_DataTypeConversion1_e_0;

  /* S-Function (motohawk_sfun_probe): '<S114>/motohawk_probe2' */
  (NoCyl_DataStore()) = (EncoderNumCylinders_DataStore());

  /* Logic: '<S115>/Logical Operator' incorporates:
   *  Constant: '<S115>/Constant1'
   *  RelationalOperator: '<S115>/Relational Operator'
   *  S-Function (motohawk_sfun_encoder_num_cylinders): '<S115>/motohawk_encoder_num_cylinders'
   */
  CRVLAB_ECU_Project_B.s115_LogicalOperator = ((rtb_Switch2 && (2 <= ((uint8_T)
    (EncoderNumCylinders_DataStore())))));

  /* Gain: '<S115>/Gain' */
  rtb_Gain5 = 16.0 * rtb_motohawk_data_read1_bu;

  /* DataTypeConversion: '<S115>/Data Type Conversion' */
  if (rtb_Gain5 < 32768.0) {
    if (rtb_Gain5 >= -32768.0) {
      rtb_DataTypeConversion_ds_0 = (int16_T)rtb_Gain5;
    } else {
      rtb_DataTypeConversion_ds_0 = MIN_int16_T;
    }
  } else {
    rtb_DataTypeConversion_ds_0 = MAX_int16_T;
  }

  /* End of DataTypeConversion: '<S115>/Data Type Conversion' */

  /* Gain: '<S115>/Gain2' */
  rtb_Gain5 = 16.0 * rtb_Product_da;

  /* DataTypeConversion: '<S115>/Data Type Conversion2' */
  if (rtb_Gain5 < 32768.0) {
    if (rtb_Gain5 >= -32768.0) {
      rtb_DataTypeConversion2_p_0 = (int16_T)rtb_Gain5;
    } else {
      rtb_DataTypeConversion2_p_0 = MIN_int16_T;
    }
  } else {
    rtb_DataTypeConversion2_p_0 = MAX_int16_T;
  }

  /* End of DataTypeConversion: '<S115>/Data Type Conversion2' */

  /* Gain: '<S115>/Gain4' */
  rtb_Gain5 = 16.0 * rtb_Gain4;

  /* DataTypeConversion: '<S115>/Data Type Conversion4' */
  if (rtb_Gain5 < 32768.0) {
    if (rtb_Gain5 >= -32768.0) {
      rtb_DataTypeConversion4 = (int16_T)rtb_Gain5;
    } else {
      rtb_DataTypeConversion4 = MIN_int16_T;
    }
  } else {
    rtb_DataTypeConversion4 = MAX_int16_T;
  }

  /* End of DataTypeConversion: '<S115>/Data Type Conversion4' */

  /* SignalConversion: '<S115>/TmpSignal ConversionAtMux PSPInport2' incorporates:
   *  DataTypeConversion: '<S115>/Data Type Conversion'
   *  DataTypeConversion: '<S115>/Data Type Conversion2'
   */
  rtb_TmpSignalConversionAtMuxPSPInport2_g[0] = rtb_DataTypeConversion_ds_0;
  rtb_TmpSignalConversionAtMuxPSPInport2_g[1] = rtb_DataTypeConversion2_p_0;
  rtb_TmpSignalConversionAtMuxPSPInport2_g[2] = rtb_DataTypeConversion4;

  /* Gain: '<S115>/Gain1' */
  rtb_Gain5 = 1000.0 * CRVLAB_ECU_Project_B.s122_Merge;

  /* DataTypeConversion: '<S115>/Data Type Conversion1' */
  if (rtb_Gain5 < 4.294967296E+9) {
    if (rtb_Gain5 >= 0.0) {
      rtb_DataTypeConversion1_e_0 = (uint32_T)rtb_Gain5;
    } else {
      rtb_DataTypeConversion1_e_0 = 0U;
    }
  } else {
    rtb_DataTypeConversion1_e_0 = MAX_uint32_T;
  }

  /* End of DataTypeConversion: '<S115>/Data Type Conversion1' */

  /* Gain: '<S115>/Gain3' */
  rtb_Gain5 = 1000.0 * CRVLAB_ECU_Project_B.s123_Merge;

  /* DataTypeConversion: '<S115>/Data Type Conversion3' */
  if (rtb_Gain5 < 4.294967296E+9) {
    if (rtb_Gain5 >= 0.0) {
      rtb_DataTypeConversion3_ng = (uint32_T)rtb_Gain5;
    } else {
      rtb_DataTypeConversion3_ng = 0U;
    }
  } else {
    rtb_DataTypeConversion3_ng = MAX_uint32_T;
  }

  /* End of DataTypeConversion: '<S115>/Data Type Conversion3' */

  /* Gain: '<S115>/Gain5' */
  rtb_Gain5 = 1000.0 * CRVLAB_ECU_Project_B.s124_Merge;

  /* DataTypeConversion: '<S115>/Data Type Conversion5' */
  if (rtb_Gain5 < 4.294967296E+9) {
    if (rtb_Gain5 >= 0.0) {
      rtb_DataTypeConversion5 = (uint32_T)rtb_Gain5;
    } else {
      rtb_DataTypeConversion5 = 0U;
    }
  } else {
    rtb_DataTypeConversion5 = MAX_uint32_T;
  }

  /* End of DataTypeConversion: '<S115>/Data Type Conversion5' */

  /* SignalConversion: '<S115>/TmpSignal ConversionAtMux PSPInport3' incorporates:
   *  DataTypeConversion: '<S115>/Data Type Conversion1'
   */
  rtb_TmpSignalConversionAtMuxPSPInport3_k[0] = rtb_DataTypeConversion1_e_0;
  rtb_TmpSignalConversionAtMuxPSPInport3_k[1] = rtb_DataTypeConversion3_ng;
  rtb_TmpSignalConversionAtMuxPSPInport3_k[2] = rtb_DataTypeConversion5;

  /* Logic: '<S116>/Logical Operator' incorporates:
   *  Constant: '<S116>/Constant1'
   *  RelationalOperator: '<S116>/Relational Operator'
   *  S-Function (motohawk_sfun_encoder_num_cylinders): '<S116>/motohawk_encoder_num_cylinders'
   */
  CRVLAB_ECU_Project_B.s116_LogicalOperator = ((rtb_Switch2 && (3 <= ((uint8_T)
    (EncoderNumCylinders_DataStore())))));

  /* Gain: '<S116>/Gain' */
  rtb_Gain5 = 16.0 * rtb_motohawk_data_read1_bu;

  /* DataTypeConversion: '<S116>/Data Type Conversion' */
  if (rtb_Gain5 < 32768.0) {
    if (rtb_Gain5 >= -32768.0) {
      rtb_DataTypeConversion_ds_0 = (int16_T)rtb_Gain5;
    } else {
      rtb_DataTypeConversion_ds_0 = MIN_int16_T;
    }
  } else {
    rtb_DataTypeConversion_ds_0 = MAX_int16_T;
  }

  /* End of DataTypeConversion: '<S116>/Data Type Conversion' */

  /* Gain: '<S116>/Gain2' */
  rtb_Gain5 = 16.0 * rtb_Product_da;

  /* DataTypeConversion: '<S116>/Data Type Conversion2' */
  if (rtb_Gain5 < 32768.0) {
    if (rtb_Gain5 >= -32768.0) {
      rtb_DataTypeConversion2_p_0 = (int16_T)rtb_Gain5;
    } else {
      rtb_DataTypeConversion2_p_0 = MIN_int16_T;
    }
  } else {
    rtb_DataTypeConversion2_p_0 = MAX_int16_T;
  }

  /* End of DataTypeConversion: '<S116>/Data Type Conversion2' */

  /* Gain: '<S116>/Gain4' */
  rtb_Gain5 = 16.0 * rtb_Gain4;

  /* DataTypeConversion: '<S116>/Data Type Conversion4' */
  if (rtb_Gain5 < 32768.0) {
    if (rtb_Gain5 >= -32768.0) {
      rtb_DataTypeConversion4 = (int16_T)rtb_Gain5;
    } else {
      rtb_DataTypeConversion4 = MIN_int16_T;
    }
  } else {
    rtb_DataTypeConversion4 = MAX_int16_T;
  }

  /* End of DataTypeConversion: '<S116>/Data Type Conversion4' */

  /* SignalConversion: '<S116>/TmpSignal ConversionAtMux PSPInport2' incorporates:
   *  DataTypeConversion: '<S116>/Data Type Conversion'
   *  DataTypeConversion: '<S116>/Data Type Conversion2'
   */
  rtb_TmpSignalConversionAtMuxPSPInport2_e[0] = rtb_DataTypeConversion_ds_0;
  rtb_TmpSignalConversionAtMuxPSPInport2_e[1] = rtb_DataTypeConversion2_p_0;
  rtb_TmpSignalConversionAtMuxPSPInport2_e[2] = rtb_DataTypeConversion4;

  /* Gain: '<S116>/Gain1' */
  rtb_Gain5 = 1000.0 * CRVLAB_ECU_Project_B.s122_Merge;

  /* DataTypeConversion: '<S116>/Data Type Conversion1' */
  if (rtb_Gain5 < 4.294967296E+9) {
    if (rtb_Gain5 >= 0.0) {
      rtb_DataTypeConversion1_e_0 = (uint32_T)rtb_Gain5;
    } else {
      rtb_DataTypeConversion1_e_0 = 0U;
    }
  } else {
    rtb_DataTypeConversion1_e_0 = MAX_uint32_T;
  }

  /* End of DataTypeConversion: '<S116>/Data Type Conversion1' */

  /* Gain: '<S116>/Gain3' */
  rtb_Gain5 = 1000.0 * CRVLAB_ECU_Project_B.s123_Merge;

  /* DataTypeConversion: '<S116>/Data Type Conversion3' */
  if (rtb_Gain5 < 4.294967296E+9) {
    if (rtb_Gain5 >= 0.0) {
      rtb_DataTypeConversion3_ng = (uint32_T)rtb_Gain5;
    } else {
      rtb_DataTypeConversion3_ng = 0U;
    }
  } else {
    rtb_DataTypeConversion3_ng = MAX_uint32_T;
  }

  /* End of DataTypeConversion: '<S116>/Data Type Conversion3' */

  /* Gain: '<S116>/Gain5' */
  rtb_Gain5 = 1000.0 * CRVLAB_ECU_Project_B.s124_Merge;

  /* DataTypeConversion: '<S116>/Data Type Conversion5' */
  if (rtb_Gain5 < 4.294967296E+9) {
    if (rtb_Gain5 >= 0.0) {
      rtb_DataTypeConversion5 = (uint32_T)rtb_Gain5;
    } else {
      rtb_DataTypeConversion5 = 0U;
    }
  } else {
    rtb_DataTypeConversion5 = MAX_uint32_T;
  }

  /* End of DataTypeConversion: '<S116>/Data Type Conversion5' */

  /* SignalConversion: '<S116>/TmpSignal ConversionAtMux PSPInport3' incorporates:
   *  DataTypeConversion: '<S116>/Data Type Conversion1'
   */
  rtb_TmpSignalConversionAtMuxPSPInport3_ki[0] = rtb_DataTypeConversion1_e_0;
  rtb_TmpSignalConversionAtMuxPSPInport3_ki[1] = rtb_DataTypeConversion3_ng;
  rtb_TmpSignalConversionAtMuxPSPInport3_ki[2] = rtb_DataTypeConversion5;

  /* Logic: '<S117>/Logical Operator' incorporates:
   *  Constant: '<S117>/Constant1'
   *  RelationalOperator: '<S117>/Relational Operator'
   *  S-Function (motohawk_sfun_encoder_num_cylinders): '<S117>/motohawk_encoder_num_cylinders'
   */
  CRVLAB_ECU_Project_B.s117_LogicalOperator = ((rtb_Switch2 && (4 <= ((uint8_T)
    (EncoderNumCylinders_DataStore())))));

  /* Gain: '<S117>/Gain' */
  rtb_motohawk_data_read1_bu *= 16.0;

  /* Gain: '<S117>/Gain2' */
  rtb_Product_da *= 16.0;

  /* Gain: '<S117>/Gain4' */
  rtb_Gain4 *= 16.0;

  /* DataTypeConversion: '<S117>/Data Type Conversion4' */
  if (rtb_Gain4 < 32768.0) {
    if (rtb_Gain4 >= -32768.0) {
      rtb_DataTypeConversion4 = (int16_T)rtb_Gain4;
    } else {
      rtb_DataTypeConversion4 = MIN_int16_T;
    }
  } else {
    rtb_DataTypeConversion4 = MAX_int16_T;
  }

  /* End of DataTypeConversion: '<S117>/Data Type Conversion4' */

  /* DataTypeConversion: '<S117>/Data Type Conversion' */
  if (rtb_motohawk_data_read1_bu < 32768.0) {
    if (rtb_motohawk_data_read1_bu >= -32768.0) {
      rtb_DataTypeConversion2_p_0 = (int16_T)rtb_motohawk_data_read1_bu;
    } else {
      rtb_DataTypeConversion2_p_0 = MIN_int16_T;
    }
  } else {
    rtb_DataTypeConversion2_p_0 = MAX_int16_T;
  }

  /* End of DataTypeConversion: '<S117>/Data Type Conversion' */

  /* SignalConversion: '<S117>/TmpSignal ConversionAtMux PSPInport2' incorporates:
   *  DataTypeConversion: '<S117>/Data Type Conversion'
   */
  rtb_TmpSignalConversionAtMuxPSPInport2_h[0] = rtb_DataTypeConversion2_p_0;

  /* DataTypeConversion: '<S117>/Data Type Conversion2' */
  if (rtb_Product_da < 32768.0) {
    if (rtb_Product_da >= -32768.0) {
      rtb_DataTypeConversion2_p_0 = (int16_T)rtb_Product_da;
    } else {
      rtb_DataTypeConversion2_p_0 = MIN_int16_T;
    }
  } else {
    rtb_DataTypeConversion2_p_0 = MAX_int16_T;
  }

  /* End of DataTypeConversion: '<S117>/Data Type Conversion2' */

  /* SignalConversion: '<S117>/TmpSignal ConversionAtMux PSPInport2' incorporates:
   *  DataTypeConversion: '<S117>/Data Type Conversion2'
   */
  rtb_TmpSignalConversionAtMuxPSPInport2_h[1] = rtb_DataTypeConversion2_p_0;
  rtb_TmpSignalConversionAtMuxPSPInport2_h[2] = rtb_DataTypeConversion4;

  /* Gain: '<S117>/Gain1' */
  rtb_motohawk_data_read1_bu = 1000.0 * CRVLAB_ECU_Project_B.s122_Merge;

  /* DataTypeConversion: '<S117>/Data Type Conversion1' */
  if (rtb_motohawk_data_read1_bu < 4.294967296E+9) {
    if (rtb_motohawk_data_read1_bu >= 0.0) {
      rtb_DataTypeConversion1_e_0 = (uint32_T)rtb_motohawk_data_read1_bu;
    } else {
      rtb_DataTypeConversion1_e_0 = 0U;
    }
  } else {
    rtb_DataTypeConversion1_e_0 = MAX_uint32_T;
  }

  /* End of DataTypeConversion: '<S117>/Data Type Conversion1' */

  /* Gain: '<S117>/Gain3' */
  rtb_motohawk_data_read1_bu = 1000.0 * CRVLAB_ECU_Project_B.s123_Merge;

  /* DataTypeConversion: '<S117>/Data Type Conversion3' */
  if (rtb_motohawk_data_read1_bu < 4.294967296E+9) {
    if (rtb_motohawk_data_read1_bu >= 0.0) {
      rtb_DataTypeConversion3_ng = (uint32_T)rtb_motohawk_data_read1_bu;
    } else {
      rtb_DataTypeConversion3_ng = 0U;
    }
  } else {
    rtb_DataTypeConversion3_ng = MAX_uint32_T;
  }

  /* End of DataTypeConversion: '<S117>/Data Type Conversion3' */

  /* Gain: '<S117>/Gain5' */
  rtb_motohawk_data_read1_bu = 1000.0 * CRVLAB_ECU_Project_B.s124_Merge;

  /* DataTypeConversion: '<S117>/Data Type Conversion5' */
  if (rtb_motohawk_data_read1_bu < 4.294967296E+9) {
    if (rtb_motohawk_data_read1_bu >= 0.0) {
      rtb_DataTypeConversion5 = (uint32_T)rtb_motohawk_data_read1_bu;
    } else {
      rtb_DataTypeConversion5 = 0U;
    }
  } else {
    rtb_DataTypeConversion5 = MAX_uint32_T;
  }

  /* End of DataTypeConversion: '<S117>/Data Type Conversion5' */

  /* SignalConversion: '<S117>/TmpSignal ConversionAtMux PSPInport3' incorporates:
   *  DataTypeConversion: '<S117>/Data Type Conversion1'
   */
  rtb_TmpSignalConversionAtMuxPSPInport3_c[0] = rtb_DataTypeConversion1_e_0;
  rtb_TmpSignalConversionAtMuxPSPInport3_c[1] = rtb_DataTypeConversion3_ng;
  rtb_TmpSignalConversionAtMuxPSPInport3_c[2] = rtb_DataTypeConversion5;

  /* S-Function (motohawk_sfun_data_read): '<S100>/motohawk_data_read1' */
  rtb_motohawk_data_read1_bu = EGR_ADC_DataStore();

  /* DataTypeConversion: '<S100>/Data Type Conversion1' */
  if (rtIsNaN(rtb_motohawk_data_read1_bu) || rtIsInf(rtb_motohawk_data_read1_bu))
  {
    rtb_DataTypeConversion_n = 0.0;
  } else {
    rtb_DataTypeConversion_n = fmod(floor(rtb_motohawk_data_read1_bu), 65536.0);
  }

  rtb_DataTypeConversion1_j = (int16_T)(rtb_DataTypeConversion_n < 0.0 ?
    (int16_T)-(int16_T)(uint16_T)-rtb_DataTypeConversion_n : (int16_T)(uint16_T)
    rtb_DataTypeConversion_n);

  /* End of DataTypeConversion: '<S100>/Data Type Conversion1' */
  /* DataTypeConversion: '<S100>/Data Type Conversion3' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S100>/motohawk_calibration4'
   */
  if (rtIsNaNF((EGR_Frequency_DataStore())) || rtIsInfF((EGR_Frequency_DataStore
        ()))) {
    tmp = 0.0F;
  } else {
    tmp = (real32_T)fmod((real32_T)floor((EGR_Frequency_DataStore())),
                         4.2949673E+9F);
  }

  rtb_DataTypeConversion3 = tmp < 0.0F ? (uint32_T)-(int32_T)(uint32_T)-tmp :
    (uint32_T)tmp;

  /* End of DataTypeConversion: '<S100>/Data Type Conversion3' */

  /* S-Function (motohawk_sfun_data_read): '<S101>/motohawk_data_read1' */
  rtb_motohawk_data_read1_e = State_DataStore();

  /* RelationalOperator: '<S140>/Relational Operator1' incorporates:
   *  Constant: '<S140>/Constant'
   */
  rtb_LogicalOperator = (rtb_motohawk_data_read1_e >= 2);

  /* If: '<S142>/If' incorporates:
   *  Inport: '<S147>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S142>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S142>/override_enable'
   */
  if ((Gas_Relay_Override_Absolute_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S142>/NewValue' incorporates:
     *  ActionPort: '<S147>/Action Port'
     */
    rtb_LogicalOperator = (Gas_Relay_Override_Absolute_new_DataStore());

    /* End of Outputs for SubSystem: '<S142>/NewValue' */
  } else {
  }

  /* End of If: '<S142>/If' */

  /* RelationalOperator: '<S146>/Compare' incorporates:
   *  Constant: '<S146>/Constant'
   */
  rtb_Compare_o = (rtb_motohawk_data_read1_e == 2);

  /* Logic: '<S141>/Logical Operator1' incorporates:
   *  Constant: '<S141>/Constant1'
   *  RelationalOperator: '<S141>/Relational Operator2'
   *  S-Function (motohawk_sfun_data_read): '<S101>/motohawk_data_read7'
   */
  CRVLAB_ECU_Project_B.s141_LogicalOperator1 = (((rtb_Compare_o != 0) &&
    (ECT_C_DataStore() < 85.0)));

  /* If: '<S143>/If' incorporates:
   *  Inport: '<S149>/In1'
   *  Inport: '<S150>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S143>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S143>/override_enable'
   */
  if ((Glow_Relay_Override_Absolute_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S143>/NewValue' incorporates:
     *  ActionPort: '<S149>/Action Port'
     */
    rtb_Switch2 = (Glow_Relay_Override_Absolute_new_DataStore());

    /* End of Outputs for SubSystem: '<S143>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S143>/OldValue' incorporates:
     *  ActionPort: '<S150>/Action Port'
     */
    rtb_Switch2 = CRVLAB_ECU_Project_B.s141_LogicalOperator1;

    /* End of Outputs for SubSystem: '<S143>/OldValue' */
  }

  /* End of If: '<S143>/If' */

  /* MultiPortSwitch: '<S101>/Multiport Switch' incorporates:
   *  Constant: '<S101>/Constant1'
   *  Constant: '<S101>/Constant4'
   *  Constant: '<S101>/Constant5'
   *  Constant: '<S101>/Constant6'
   *  Sum: '<S101>/Subtract'
   */
  switch ((rtb_LogicalOperator - rtb_Switch2) + 1) {
   case 0:
    rtb_DataTypeConversion_n = 4096.0;
    break;

   case 1:
    rtb_DataTypeConversion_n = 0.0;
    break;

   default:
    rtb_DataTypeConversion_n = -4096.0;
    break;
  }

  /* End of MultiPortSwitch: '<S101>/Multiport Switch' */
  /* If: '<S145>/If' incorporates:
   *  Inport: '<S153>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S145>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S145>/override_enable'
   */
  if ((H_bridge1_DC_Override_Absolute_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S145>/NewValue' incorporates:
     *  ActionPort: '<S153>/Action Port'
     */
    rtb_DataTypeConversion_n = (H_bridge1_DC_Override_Absolute_new_DataStore());

    /* End of Outputs for SubSystem: '<S145>/NewValue' */
  } else {
  }

  /* End of If: '<S145>/If' */

  /* DataTypeConversion: '<S101>/Data Type Conversion4' */
  if (rtIsNaN(rtb_DataTypeConversion_n) || rtIsInf(rtb_DataTypeConversion_n)) {
    rtb_DataTypeConversion_n = 0.0;
  } else {
    rtb_DataTypeConversion_n = fmod(floor(rtb_DataTypeConversion_n), 65536.0);
  }

  CRVLAB_ECU_Project_B.s101_DataTypeConversion4 = (int16_T)
    (rtb_DataTypeConversion_n < 0.0 ? (int16_T)-(int16_T)(uint16_T)
     -rtb_DataTypeConversion_n : (int16_T)(uint16_T)rtb_DataTypeConversion_n);

  /* End of DataTypeConversion: '<S101>/Data Type Conversion4' */

  /* If: '<S144>/If' incorporates:
   *  Inport: '<S151>/In1'
   *  Inport: '<S152>/In1'
   *  Logic: '<S101>/Logical Operator'
   *  S-Function (motohawk_sfun_calibration): '<S144>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S144>/override_enable'
   */
  if ((H_bridge1_Enable_Override_Absolute_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S144>/NewValue' incorporates:
     *  ActionPort: '<S151>/Action Port'
     */
    CRVLAB_ECU_Project_B.s144_Merge =
      (H_bridge1_Enable_Override_Absolute_new_DataStore());

    /* End of Outputs for SubSystem: '<S144>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S144>/OldValue' incorporates:
     *  ActionPort: '<S152>/Action Port'
     */
    CRVLAB_ECU_Project_B.s144_Merge = ((rtb_LogicalOperator || rtb_Switch2));

    /* End of Outputs for SubSystem: '<S144>/OldValue' */
  }

  /* End of If: '<S144>/If' */

  /* DataTypeConversion: '<S101>/Data Type Conversion6' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S101>/motohawk_calibration3'
   */
  if (rtIsNaN((H_bridge1_Freq_DataStore())) || rtIsInf((H_bridge1_Freq_DataStore
        ()))) {
    rtb_DataTypeConversion_n = 0.0;
  } else {
    rtb_DataTypeConversion_n = fmod(floor((H_bridge1_Freq_DataStore())),
      4.294967296E+9);
  }

  rtb_DataTypeConversion6 = rtb_DataTypeConversion_n < 0.0 ? (uint32_T)-(int32_T)
    (uint32_T)-rtb_DataTypeConversion_n : (uint32_T)rtb_DataTypeConversion_n;

  /* End of DataTypeConversion: '<S101>/Data Type Conversion6' */

  /* RelationalOperator: '<S101>/Relational Operator' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S101>/motohawk_calibration7'
   *  S-Function (motohawk_sfun_data_read): '<S101>/motohawk_data_read'
   */
  CRVLAB_ECU_Project_B.s101_RelationalOperator = ((KeySw_Volt_DataStore() >=
    (VoltRefForRelay_DataStore())));

  /* DataTypeConversion: '<S102>/Data Type Conversion1' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S102>/motohawk_calibration'
   */
  if (rtIsNaN((TACHDC_ADC_DataStore())) || rtIsInf((TACHDC_ADC_DataStore()))) {
    rtb_DataTypeConversion_n = 0.0;
  } else {
    rtb_DataTypeConversion_n = fmod(floor((TACHDC_ADC_DataStore())), 65536.0);
  }

  rtb_DataTypeConversion1_m = (int16_T)(rtb_DataTypeConversion_n < 0.0 ?
    (int16_T)-(int16_T)(uint16_T)-rtb_DataTypeConversion_n : (int16_T)(uint16_T)
    rtb_DataTypeConversion_n);

  /* End of DataTypeConversion: '<S102>/Data Type Conversion1' */

  /* S-Function (motohawk_sfun_data_read): '<S102>/motohawk_data_read1' */
  rtb_motohawk_data_read1_bu = EngineSpeed_rpm_DataStore();

  /* DataTypeConversion: '<S102>/Data Type Conversion2' incorporates:
   *  Product: '<S102>/Product'
   *  S-Function (motohawk_sfun_calibration): '<S102>/motohawk_calibration1'
   */
  rtb_DataTypeConversion_n = rtb_motohawk_data_read1_bu * (real_T)
    (TACHFrequencyGain_DataStore());
  if (rtIsNaN(rtb_DataTypeConversion_n) || rtIsInf(rtb_DataTypeConversion_n)) {
    rtb_DataTypeConversion_n = 0.0;
  } else {
    rtb_DataTypeConversion_n = fmod(floor(rtb_DataTypeConversion_n),
      4.294967296E+9);
  }

  rtb_DataTypeConversion2 = rtb_DataTypeConversion_n < 0.0 ? (uint32_T)-(int32_T)
    (uint32_T)-rtb_DataTypeConversion_n : (uint32_T)rtb_DataTypeConversion_n;

  /* End of DataTypeConversion: '<S102>/Data Type Conversion2' */

  /* Product: '<S103>/Product' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S103>/motohawk_calibration2'
   *  S-Function (motohawk_sfun_data_read): '<S103>/motohawk_data_read1'
   */
  rtb_DataTypeConversion_n = ECT_C_DataStore() * (THWOGain_ADC_C_DataStore());

  /* Sum: '<S103>/Add' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S103>/motohawk_calibration3'
   */
  rtb_motohawk_data_read1_bu = rtb_DataTypeConversion_n +
    (THWOOffset_ADC_DataStore());

  /* Saturate: '<S103>/Saturation' */
  rtb_motohawk_data_read1_bu = rtb_motohawk_data_read1_bu >= 4000.0 ? 4000.0 :
    rtb_motohawk_data_read1_bu <= 0.0 ? 0.0 : rtb_motohawk_data_read1_bu;

  /* DataTypeConversion: '<S103>/Data Type Conversion1' */
  if (rtIsNaN(rtb_motohawk_data_read1_bu) || rtIsInf(rtb_motohawk_data_read1_bu))
  {
    rtb_DataTypeConversion_n = 0.0;
  } else {
    rtb_DataTypeConversion_n = fmod(floor(rtb_motohawk_data_read1_bu), 65536.0);
  }

  rtb_DataTypeConversion1_ea = (int16_T)(rtb_DataTypeConversion_n < 0.0 ?
    (int16_T)-(int16_T)(uint16_T)-rtb_DataTypeConversion_n : (int16_T)(uint16_T)
    rtb_DataTypeConversion_n);

  /* End of DataTypeConversion: '<S103>/Data Type Conversion1' */
  /* DataTypeConversion: '<S103>/Data Type Conversion3' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S103>/motohawk_calibration4'
   */
  if (rtIsNaN((THWO_Frequency_DataStore())) || rtIsInf((THWO_Frequency_DataStore
        ()))) {
    rtb_DataTypeConversion_n = 0.0;
  } else {
    rtb_DataTypeConversion_n = fmod(floor((THWO_Frequency_DataStore())),
      4.294967296E+9);
  }

  rtb_DataTypeConversion3_n = rtb_DataTypeConversion_n < 0.0 ? (uint32_T)
    -(int32_T)(uint32_T)-rtb_DataTypeConversion_n : (uint32_T)
    rtb_DataTypeConversion_n;

  /* End of DataTypeConversion: '<S103>/Data Type Conversion3' */

  /* S-Function (motohawk_sfun_data_read): '<S104>/motohawk_data_read1' */
  rtb_motohawk_data_read1_bu = Throttle_ADC_DataStore();

  /* DataTypeConversion: '<S104>/Data Type Conversion1' */
  if (rtIsNaN(rtb_motohawk_data_read1_bu) || rtIsInf(rtb_motohawk_data_read1_bu))
  {
    rtb_DataTypeConversion_n = 0.0;
  } else {
    rtb_DataTypeConversion_n = fmod(floor(rtb_motohawk_data_read1_bu), 65536.0);
  }

  rtb_DataTypeConversion1_d = (int16_T)(rtb_DataTypeConversion_n < 0.0 ?
    (int16_T)-(int16_T)(uint16_T)-rtb_DataTypeConversion_n : (int16_T)(uint16_T)
    rtb_DataTypeConversion_n);

  /* End of DataTypeConversion: '<S104>/Data Type Conversion1' */
  /* DataTypeConversion: '<S104>/Data Type Conversion2' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S104>/motohawk_calibration'
   */
  if (rtIsNaNF((ETC_Frequency_DataStore())) || rtIsInfF((ETC_Frequency_DataStore
        ()))) {
    tmp = 0.0F;
  } else {
    tmp = (real32_T)fmod((real32_T)floor((ETC_Frequency_DataStore())),
                         4.2949673E+9F);
  }

  rtb_DataTypeConversion2_i = tmp < 0.0F ? (uint32_T)-(int32_T)(uint32_T)-tmp :
    (uint32_T)tmp;

  /* End of DataTypeConversion: '<S104>/Data Type Conversion2' */

  /* S-Function (motohawk_sfun_data_read): '<S105>/motohawk_data_read1' */
  rtb_motohawk_data_read1_i = VNT_ADC_DataStore();

  /* If: '<S155>/If' incorporates:
   *  Inport: '<S156>/In1'
   *  Inport: '<S157>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S155>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S155>/override_enable'
   */
  if ((VNTOverride_ADC_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S155>/NewValue' incorporates:
     *  ActionPort: '<S156>/Action Port'
     */
    rtb_DataTypeConversion_n = (VNTOverride_ADC_new_DataStore());

    /* End of Outputs for SubSystem: '<S155>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S155>/OldValue' incorporates:
     *  ActionPort: '<S157>/Action Port'
     */
    rtb_DataTypeConversion_n = rtb_motohawk_data_read1_i;

    /* End of Outputs for SubSystem: '<S155>/OldValue' */
  }

  /* End of If: '<S155>/If' */

  /* DataTypeConversion: '<S105>/Data Type Conversion1' */
  if (rtIsNaN(rtb_DataTypeConversion_n) || rtIsInf(rtb_DataTypeConversion_n)) {
    rtb_DataTypeConversion_n = 0.0;
  } else {
    rtb_DataTypeConversion_n = fmod(floor(rtb_DataTypeConversion_n), 65536.0);
  }

  rtb_DataTypeConversion1_f = (int16_T)(rtb_DataTypeConversion_n < 0.0 ?
    (int16_T)-(int16_T)(uint16_T)-rtb_DataTypeConversion_n : (int16_T)(uint16_T)
    rtb_DataTypeConversion_n);

  /* End of DataTypeConversion: '<S105>/Data Type Conversion1' */
  /* DataTypeConversion: '<S105>/Data Type Conversion3' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S105>/motohawk_calibration4'
   */
  if (rtIsNaN((VNTFrequency_DataStore())) || rtIsInf((VNTFrequency_DataStore())))
  {
    rtb_DataTypeConversion_n = 0.0;
  } else {
    rtb_DataTypeConversion_n = fmod(floor((VNTFrequency_DataStore())),
      4.294967296E+9);
  }

  rtb_DataTypeConversion3_p = rtb_DataTypeConversion_n < 0.0 ? (uint32_T)
    -(int32_T)(uint32_T)-rtb_DataTypeConversion_n : (uint32_T)
    rtb_DataTypeConversion_n;

  /* End of DataTypeConversion: '<S105>/Data Type Conversion3' */

  /* Update for UnitDelay: '<S24>/Unit Delay' */
  CRVLAB_ECU_Project_DWork.s24_UnitDelay_DSTATE = rtb_Sum1_p;

  /* Update for UnitDelay: '<S25>/Unit Delay1' */
  CRVLAB_ECU_Project_DWork.s25_UnitDelay1_DSTATE = CRVLAB_ECU_Project_B.s17_Sum1;

  /* Update for UnitDelay: '<S25>/Unit Delay' */
  CRVLAB_ECU_Project_DWork.s25_UnitDelay_DSTATE = rtb_Subtract1;

  /* Update for UnitDelay: '<S61>/Unit Delay1' */
  CRVLAB_ECU_Project_DWork.s61_UnitDelay1_DSTATE = rtb_UnitDelay1_g;

  /* Update for UnitDelay: '<S61>/Unit Delay' */
  CRVLAB_ECU_Project_DWork.s61_UnitDelay_DSTATE = rtb_Sum4;

  /* Update for UnitDelay: '<S62>/Unit Delay1' */
  CRVLAB_ECU_Project_DWork.s62_UnitDelay1_DSTATE = rtb_UnitDelay1_i;

  /* Update for UnitDelay: '<S62>/Unit Delay' */
  CRVLAB_ECU_Project_DWork.s62_UnitDelay_DSTATE = rtb_Sum4_a;

  /* Update for UnitDelay: '<S59>/Unit Delay1' */
  CRVLAB_ECU_Project_DWork.s59_UnitDelay1_DSTATE = rtb_Sum_g;

  /* Update for UnitDelay: '<S59>/Unit Delay' */
  CRVLAB_ECU_Project_DWork.s59_UnitDelay_DSTATE = rtb_Sum4_j;

  /* Update for UnitDelay: '<S60>/Unit Delay1' */
  CRVLAB_ECU_Project_DWork.s60_UnitDelay1_DSTATE = rtb_Sum_n;

  /* Update for UnitDelay: '<S60>/Unit Delay' */
  CRVLAB_ECU_Project_DWork.s60_UnitDelay_DSTATE = rtb_Sum4_m;

  /* Update for UnitDelay: '<S63>/Unit Delay1' */
  CRVLAB_ECU_Project_DWork.s63_UnitDelay1_DSTATE = rtb_Add_h;

  /* Update for UnitDelay: '<S63>/Unit Delay' */
  CRVLAB_ECU_Project_DWork.s63_UnitDelay_DSTATE = rtb_Sum4_e;

  /* Update for S-Function (motohawk_sfun_dout): '<S97>/motohawk_dout' */

  /* S-Function Block: DOut1918p0001 */
  {
    DOut1918p0001_DiscreteOutput_Set(rtb_DataTypeConversion_h);
  }

  /* Update for S-Function (motohawk_sfun_pwm): '<S98>/motohawk_pwm1' */

  /* S-Function Block: LSD10_PWMOutput */
  LSD10_PWMOutput_PWMOutput_Set(rtb_DataTypeConversion1,
    rtb_DataTypeConversion_d, 0, 1);

  /* S-Function Block: <S114>/Mux PSP MuxPSP1 */
  {
    extern void MuxPSP1_MuxPSP_HardStart_SoftDuration_Set(uint8_T const state[],
      boolean_T use_state_min, int16_T const start_angle[], boolean_T
      use_start_angle_min, uint32_T const duration[], boolean_T use_dur_min,
      uint32_T const* min_dur);
    MuxPSP1_MuxPSP_HardStart_SoftDuration_Set
      ((&CRVLAB_ECU_Project_B.s114_LogicalOperator), 1,
       (rtb_TmpSignalConversionAtMuxPSPInport2), 0,
       (rtb_TmpSignalConversionAtMuxPSPInport3), 0, NULL);
  }

  /* S-Function Block: <S115>/Mux PSP MuxPSP2 */
  {
    extern void MuxPSP2_MuxPSP_HardStart_SoftDuration_Set(uint8_T const state[],
      boolean_T use_state_min, int16_T const start_angle[], boolean_T
      use_start_angle_min, uint32_T const duration[], boolean_T use_dur_min,
      uint32_T const* min_dur);
    MuxPSP2_MuxPSP_HardStart_SoftDuration_Set
      ((&CRVLAB_ECU_Project_B.s115_LogicalOperator), 1,
       (rtb_TmpSignalConversionAtMuxPSPInport2_g), 0,
       (rtb_TmpSignalConversionAtMuxPSPInport3_k), 0, NULL);
  }

  /* S-Function Block: <S116>/Mux PSP MuxPSP3 */
  {
    extern void MuxPSP3_MuxPSP_HardStart_SoftDuration_Set(uint8_T const state[],
      boolean_T use_state_min, int16_T const start_angle[], boolean_T
      use_start_angle_min, uint32_T const duration[], boolean_T use_dur_min,
      uint32_T const* min_dur);
    MuxPSP3_MuxPSP_HardStart_SoftDuration_Set
      ((&CRVLAB_ECU_Project_B.s116_LogicalOperator), 1,
       (rtb_TmpSignalConversionAtMuxPSPInport2_e), 0,
       (rtb_TmpSignalConversionAtMuxPSPInport3_ki), 0, NULL);
  }

  /* S-Function Block: <S117>/Mux PSP MuxPSP4 */
  {
    extern void MuxPSP4_MuxPSP_HardStart_SoftDuration_Set(uint8_T const state[],
      boolean_T use_state_min, int16_T const start_angle[], boolean_T
      use_start_angle_min, uint32_T const duration[], boolean_T use_dur_min,
      uint32_T const* min_dur);
    MuxPSP4_MuxPSP_HardStart_SoftDuration_Set
      ((&CRVLAB_ECU_Project_B.s117_LogicalOperator), 1,
       (rtb_TmpSignalConversionAtMuxPSPInport2_h), 0,
       (rtb_TmpSignalConversionAtMuxPSPInport3_c), 0, NULL);
  }

  /* Update for S-Function (motohawk_sfun_pwm): '<S100>/motohawk_pwm' */

  /* S-Function Block: EGR_Command */
  EGR_Command_PWMOutput_Set(rtb_DataTypeConversion3, rtb_DataTypeConversion1_j,
    0, 1);

  /* Update for S-Function (motohawk_sfun_pwm): '<S101>/motohawk_pwm1' */

  /* S-Function Block: H1A_Out */
  H1A_Out_PWMOutput_Set(rtb_DataTypeConversion6,
                        CRVLAB_ECU_Project_B.s101_DataTypeConversion4, 0,
                        CRVLAB_ECU_Project_B.s144_Merge);

  /* Update for S-Function (motohawk_sfun_dout): '<S101>/motohawk_dout4' */

  /* S-Function Block: DOut2397p0004 */
  {
    DOut2397p0004_DiscreteOutput_Set
      (CRVLAB_ECU_Project_B.s101_RelationalOperator);
  }

  /* Update for S-Function (motohawk_sfun_pwm): '<S102>/motohawk_pwm' */

  /* S-Function Block: TACH_PWMOutput */
  TACH_PWMOutput_PWMOutput_Set(rtb_DataTypeConversion2,
    rtb_DataTypeConversion1_m, 0, 1);

  /* Update for S-Function (motohawk_sfun_pwm): '<S103>/motohawk_pwm' */

  /* S-Function Block: THWO_Command */
  THWO_Command_PWMOutput_Set(rtb_DataTypeConversion3_n,
    rtb_DataTypeConversion1_ea, 0, 1);

  /* Update for S-Function (motohawk_sfun_pwm): '<S104>/motohawk_pwm' */

  /* S-Function Block: LSD9_PWMOutput */
  LSD9_PWMOutput_PWMOutput_Set(rtb_DataTypeConversion2_i,
    rtb_DataTypeConversion1_d, 0, 1);

  /* Update for S-Function (motohawk_sfun_pwm): '<S105>/motohawk_pwm' */

  /* S-Function Block: VNTO */
  VNTO_PWMOutput_Set(rtb_DataTypeConversion3_p, rtb_DataTypeConversion1_f, 0, 1);
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
