#ifndef VARDECS_H
#define VARDECS_H

/* Name: VarDecEnum_0 ClassID:ENUMDEC EnumDflt:"Unknown" */
typedef enum {
  VarDecEnum_0_0 = 0,                  /* EnumTxt:"Rising Edge" */
  VarDecEnum_0_1 = 1,                  /* EnumTxt:"Falling Edge" */
} VarDecEnum_0;

/* Name: VarDecEnum_1 ClassID:ENUMDEC EnumDflt:"Unknown" */
typedef enum {
  VarDecEnum_1_0 = 0,                  /* EnumTxt:"Weak Pullup" */
  VarDecEnum_1_1 = 1,                  /* EnumTxt:"Strong Pullup" */
} VarDecEnum_1;

/* Name: VarDecEnum_2 ClassID:ENUMDEC EnumDflt:"Unknown" */
typedef enum {
  VarDecEnum_2_0 = 0,                  /* EnumTxt:"Pass-Through" */
  VarDecEnum_2_1 = 1,                  /* EnumTxt:"Override" */
} VarDecEnum_2;

#endif
