/*
 * MotoHawk_IO.c
 *
 * Code generation for model "CRVLAB_ECU_Project.mdl".
 *
 * Model version              : 1.812
 * Simulink Coder version : 8.0 (R2011a) 09-Mar-2011
 * C source code generated on : Tue Apr 10 17:20:14 2012
 *
 * Target selection: motohawk_ert_rtw.tlc
 * Embedded hardware selection: Specified
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "MotoHawk_IO.h"

/* S-Function Block: <S8>/motohawk_ain1 Resource: AN8M */
NativeError_S AN8M_AnalogInput_Create(void)
{
  NativeError_S sErrorResult = ERROR_RESOURCE_NOT_CREATED;
  S_AnalogInCreateAttributes CreateInfo;
  CreateInfo.DynamicObj.eResourceCondition = RES_ENABLED;
  CreateInfo.DynamicObj.uValidAttributesMask = USE_ANALOG_CONDITION;
  CreateInfo.uValidAttributesMask = USE_ANALOG_DYNAMIC_ON_CREATE;
  (init_resource_AN8M_DataStore()) = -1;
  if (((int32_T) RES_AN8M) >= 0) {
    sErrorResult = CreateResource((E_ModuleResource) (((int32_T) RES_AN8M)),
      &CreateInfo, BEHAVIOUR_ANALOGIN);
    if (SUCCESS(sErrorResult)) {
      (init_resource_AN8M_DataStore()) = ((int32_T) RES_AN8M);
    } else {
      LogNativeError(sErrorResult);
    }

    {
      extern uint8_T ain_create_AN8M;
      if (SUCCESS(sErrorResult))
        ain_create_AN8M = 0;
      else
        ain_create_AN8M = (uint8_T) GetErrorCode(sErrorResult);
    }
  }

  return sErrorResult;
}

NativeError_S AN8M_AnalogInput_Get(uint16_T *adc, uint16_T *status)
{
  NativeError_S sErrorResult = ERROR_FAIL;
  uint16_T Result;
  if ((init_resource_AN8M_DataStore()) >= 0) {
    S_AnalogHowToGet HowToGetObj;
    S_AnalogResult AnalogResultObj;
    HowToGetObj.uValidAttributesMask = 0;
    sErrorResult = GetResourceValueBEHAVIOUR_ANALOGIN((E_ModuleResource)
      ((init_resource_AN8M_DataStore())), &HowToGetObj, &AnalogResultObj);
    if (SUCCESS(sErrorResult)) {
      *adc = AnalogResultObj.uADCValue ;
    } else {
      *adc = 0;
    }

    {
      extern uint8_T ain_read_AN8M;
      if (SUCCESS(sErrorResult))
        ain_read_AN8M = 0;
      else
        ain_read_AN8M = (uint8_T) GetErrorCode(sErrorResult);
    }
  }

  return sErrorResult;
}

/* S-Function Block: <S9>/motohawk_ain5 Resource: AN6M */
NativeError_S AN6M_AnalogInput_Create(void)
{
  NativeError_S sErrorResult = ERROR_RESOURCE_NOT_CREATED;
  S_AnalogInCreateAttributes CreateInfo;
  CreateInfo.DynamicObj.eResourceCondition = RES_ENABLED;
  CreateInfo.DynamicObj.uValidAttributesMask = USE_ANALOG_CONDITION;
  CreateInfo.uValidAttributesMask = USE_ANALOG_DYNAMIC_ON_CREATE;
  (init_resource_AN6M_DataStore()) = -1;
  if (((int32_T) RES_AN6M) >= 0) {
    sErrorResult = CreateResource((E_ModuleResource) (((int32_T) RES_AN6M)),
      &CreateInfo, BEHAVIOUR_ANALOGIN);
    if (SUCCESS(sErrorResult)) {
      (init_resource_AN6M_DataStore()) = ((int32_T) RES_AN6M);
    } else {
      LogNativeError(sErrorResult);
    }

    {
      extern uint8_T ain_create_AN6M;
      if (SUCCESS(sErrorResult))
        ain_create_AN6M = 0;
      else
        ain_create_AN6M = (uint8_T) GetErrorCode(sErrorResult);
    }
  }

  return sErrorResult;
}

NativeError_S AN6M_AnalogInput_Get(uint16_T *adc, uint16_T *status)
{
  NativeError_S sErrorResult = ERROR_FAIL;
  uint16_T Result;
  if ((init_resource_AN6M_DataStore()) >= 0) {
    S_AnalogHowToGet HowToGetObj;
    S_AnalogResult AnalogResultObj;
    HowToGetObj.uValidAttributesMask = 0;
    sErrorResult = GetResourceValueBEHAVIOUR_ANALOGIN((E_ModuleResource)
      ((init_resource_AN6M_DataStore())), &HowToGetObj, &AnalogResultObj);
    if (SUCCESS(sErrorResult)) {
      *adc = AnalogResultObj.uADCValue ;
    } else {
      *adc = 0;
    }

    {
      extern uint8_T ain_read_AN6M;
      if (SUCCESS(sErrorResult))
        ain_read_AN6M = 0;
      else
        ain_read_AN6M = (uint8_T) GetErrorCode(sErrorResult);
    }
  }

  return sErrorResult;
}

/* S-Function Block: <S10>/motohawk_ain1 Resource: AN11M */
NativeError_S AN11M_AnalogInput_Create(void)
{
  NativeError_S sErrorResult = ERROR_RESOURCE_NOT_CREATED;
  S_AnalogInCreateAttributes CreateInfo;
  CreateInfo.DynamicObj.eResourceCondition = RES_ENABLED;
  CreateInfo.DynamicObj.uValidAttributesMask = USE_ANALOG_CONDITION;
  CreateInfo.uValidAttributesMask = USE_ANALOG_DYNAMIC_ON_CREATE;
  (init_resource_AN11M_DataStore()) = -1;
  if (((int32_T) RES_AN11M) >= 0) {
    sErrorResult = CreateResource((E_ModuleResource) (((int32_T) RES_AN11M)),
      &CreateInfo, BEHAVIOUR_ANALOGIN);
    if (SUCCESS(sErrorResult)) {
      (init_resource_AN11M_DataStore()) = ((int32_T) RES_AN11M);
    } else {
      LogNativeError(sErrorResult);
    }

    {
      extern uint8_T ain_create_AN11M;
      if (SUCCESS(sErrorResult))
        ain_create_AN11M = 0;
      else
        ain_create_AN11M = (uint8_T) GetErrorCode(sErrorResult);
    }
  }

  return sErrorResult;
}

NativeError_S AN11M_AnalogInput_Get(uint16_T *adc, uint16_T *status)
{
  NativeError_S sErrorResult = ERROR_FAIL;
  uint16_T Result;
  if ((init_resource_AN11M_DataStore()) >= 0) {
    S_AnalogHowToGet HowToGetObj;
    S_AnalogResult AnalogResultObj;
    HowToGetObj.uValidAttributesMask = 0;
    sErrorResult = GetResourceValueBEHAVIOUR_ANALOGIN((E_ModuleResource)
      ((init_resource_AN11M_DataStore())), &HowToGetObj, &AnalogResultObj);
    if (SUCCESS(sErrorResult)) {
      *adc = AnalogResultObj.uADCValue ;
    } else {
      *adc = 0;
    }

    {
      extern uint8_T ain_read_AN11M;
      if (SUCCESS(sErrorResult))
        ain_read_AN11M = 0;
      else
        ain_read_AN11M = (uint8_T) GetErrorCode(sErrorResult);
    }
  }

  return sErrorResult;
}

/* S-Function Block: <S10>/motohawk_ain2 Resource: AN12M */
NativeError_S AN12M_AnalogInput_Create(void)
{
  NativeError_S sErrorResult = ERROR_RESOURCE_NOT_CREATED;
  S_AnalogInCreateAttributes CreateInfo;
  CreateInfo.DynamicObj.eResourceCondition = RES_ENABLED;
  CreateInfo.DynamicObj.uValidAttributesMask = USE_ANALOG_CONDITION;
  CreateInfo.uValidAttributesMask = USE_ANALOG_DYNAMIC_ON_CREATE;
  (init_resource_AN12M_DataStore()) = -1;
  if (((int32_T) RES_AN12M) >= 0) {
    sErrorResult = CreateResource((E_ModuleResource) (((int32_T) RES_AN12M)),
      &CreateInfo, BEHAVIOUR_ANALOGIN);
    if (SUCCESS(sErrorResult)) {
      (init_resource_AN12M_DataStore()) = ((int32_T) RES_AN12M);
    } else {
      LogNativeError(sErrorResult);
    }

    {
      extern uint8_T ain_create_AN12M;
      if (SUCCESS(sErrorResult))
        ain_create_AN12M = 0;
      else
        ain_create_AN12M = (uint8_T) GetErrorCode(sErrorResult);
    }
  }

  return sErrorResult;
}

NativeError_S AN12M_AnalogInput_Get(uint16_T *adc, uint16_T *status)
{
  NativeError_S sErrorResult = ERROR_FAIL;
  uint16_T Result;
  if ((init_resource_AN12M_DataStore()) >= 0) {
    S_AnalogHowToGet HowToGetObj;
    S_AnalogResult AnalogResultObj;
    HowToGetObj.uValidAttributesMask = 0;
    sErrorResult = GetResourceValueBEHAVIOUR_ANALOGIN((E_ModuleResource)
      ((init_resource_AN12M_DataStore())), &HowToGetObj, &AnalogResultObj);
    if (SUCCESS(sErrorResult)) {
      *adc = AnalogResultObj.uADCValue ;
    } else {
      *adc = 0;
    }

    {
      extern uint8_T ain_read_AN12M;
      if (SUCCESS(sErrorResult))
        ain_read_AN12M = 0;
      else
        ain_read_AN12M = (uint8_T) GetErrorCode(sErrorResult);
    }
  }

  return sErrorResult;
}

/* S-Function Block: <S10>/motohawk_ain3 Resource: AN13M */
NativeError_S AN13M_AnalogInput_Create(void)
{
  NativeError_S sErrorResult = ERROR_RESOURCE_NOT_CREATED;
  S_AnalogInCreateAttributes CreateInfo;
  CreateInfo.DynamicObj.eResourceCondition = RES_ENABLED;
  CreateInfo.DynamicObj.uValidAttributesMask = USE_ANALOG_CONDITION;
  CreateInfo.uValidAttributesMask = USE_ANALOG_DYNAMIC_ON_CREATE;
  (init_resource_AN13M_DataStore()) = -1;
  if (((int32_T) RES_AN13M) >= 0) {
    sErrorResult = CreateResource((E_ModuleResource) (((int32_T) RES_AN13M)),
      &CreateInfo, BEHAVIOUR_ANALOGIN);
    if (SUCCESS(sErrorResult)) {
      (init_resource_AN13M_DataStore()) = ((int32_T) RES_AN13M);
    } else {
      LogNativeError(sErrorResult);
    }

    {
      extern uint8_T ain_create_AN13M;
      if (SUCCESS(sErrorResult))
        ain_create_AN13M = 0;
      else
        ain_create_AN13M = (uint8_T) GetErrorCode(sErrorResult);
    }
  }

  return sErrorResult;
}

NativeError_S AN13M_AnalogInput_Get(uint16_T *adc, uint16_T *status)
{
  NativeError_S sErrorResult = ERROR_FAIL;
  uint16_T Result;
  if ((init_resource_AN13M_DataStore()) >= 0) {
    S_AnalogHowToGet HowToGetObj;
    S_AnalogResult AnalogResultObj;
    HowToGetObj.uValidAttributesMask = 0;
    sErrorResult = GetResourceValueBEHAVIOUR_ANALOGIN((E_ModuleResource)
      ((init_resource_AN13M_DataStore())), &HowToGetObj, &AnalogResultObj);
    if (SUCCESS(sErrorResult)) {
      *adc = AnalogResultObj.uADCValue ;
    } else {
      *adc = 0;
    }

    {
      extern uint8_T ain_read_AN13M;
      if (SUCCESS(sErrorResult))
        ain_read_AN13M = 0;
      else
        ain_read_AN13M = (uint8_T) GetErrorCode(sErrorResult);
    }
  }

  return sErrorResult;
}

/* S-Function Block: <S10>/motohawk_ain4 Resource: AN9M */
NativeError_S AN9M_AnalogInput_Create(void)
{
  NativeError_S sErrorResult = ERROR_RESOURCE_NOT_CREATED;
  S_AnalogInCreateAttributes CreateInfo;
  CreateInfo.DynamicObj.eResourceCondition = RES_ENABLED;
  CreateInfo.DynamicObj.uValidAttributesMask = USE_ANALOG_CONDITION;
  CreateInfo.uValidAttributesMask = USE_ANALOG_DYNAMIC_ON_CREATE;
  (init_resource_AN9M_DataStore()) = -1;
  if (((int32_T) RES_AN9M) >= 0) {
    sErrorResult = CreateResource((E_ModuleResource) (((int32_T) RES_AN9M)),
      &CreateInfo, BEHAVIOUR_ANALOGIN);
    if (SUCCESS(sErrorResult)) {
      (init_resource_AN9M_DataStore()) = ((int32_T) RES_AN9M);
    } else {
      LogNativeError(sErrorResult);
    }

    {
      extern uint8_T ain_create_AN9M;
      if (SUCCESS(sErrorResult))
        ain_create_AN9M = 0;
      else
        ain_create_AN9M = (uint8_T) GetErrorCode(sErrorResult);
    }
  }

  return sErrorResult;
}

NativeError_S AN9M_AnalogInput_Get(uint16_T *adc, uint16_T *status)
{
  NativeError_S sErrorResult = ERROR_FAIL;
  uint16_T Result;
  if ((init_resource_AN9M_DataStore()) >= 0) {
    S_AnalogHowToGet HowToGetObj;
    S_AnalogResult AnalogResultObj;
    HowToGetObj.uValidAttributesMask = 0;
    sErrorResult = GetResourceValueBEHAVIOUR_ANALOGIN((E_ModuleResource)
      ((init_resource_AN9M_DataStore())), &HowToGetObj, &AnalogResultObj);
    if (SUCCESS(sErrorResult)) {
      *adc = AnalogResultObj.uADCValue ;
    } else {
      *adc = 0;
    }

    {
      extern uint8_T ain_read_AN9M;
      if (SUCCESS(sErrorResult))
        ain_read_AN9M = 0;
      else
        ain_read_AN9M = (uint8_T) GetErrorCode(sErrorResult);
    }
  }

  return sErrorResult;
}

/* S-Function Block: <S11>/motohawk_ain Resource: AN5M */
NativeError_S AN5M_AnalogInput_Create(void)
{
  NativeError_S sErrorResult = ERROR_RESOURCE_NOT_CREATED;
  S_AnalogInCreateAttributes CreateInfo;
  CreateInfo.DynamicObj.eResourceCondition = RES_ENABLED;
  CreateInfo.DynamicObj.uValidAttributesMask = USE_ANALOG_CONDITION;
  CreateInfo.uValidAttributesMask = USE_ANALOG_DYNAMIC_ON_CREATE;
  (init_resource_AN5M_DataStore()) = -1;
  if (((int32_T) RES_AN5M) >= 0) {
    sErrorResult = CreateResource((E_ModuleResource) (((int32_T) RES_AN5M)),
      &CreateInfo, BEHAVIOUR_ANALOGIN);
    if (SUCCESS(sErrorResult)) {
      (init_resource_AN5M_DataStore()) = ((int32_T) RES_AN5M);
    } else {
      LogNativeError(sErrorResult);
    }

    {
      extern uint8_T ain_create_AN5M;
      if (SUCCESS(sErrorResult))
        ain_create_AN5M = 0;
      else
        ain_create_AN5M = (uint8_T) GetErrorCode(sErrorResult);
    }
  }

  return sErrorResult;
}

NativeError_S AN5M_AnalogInput_Get(uint16_T *adc, uint16_T *status)
{
  NativeError_S sErrorResult = ERROR_FAIL;
  uint16_T Result;
  if ((init_resource_AN5M_DataStore()) >= 0) {
    S_AnalogHowToGet HowToGetObj;
    S_AnalogResult AnalogResultObj;
    HowToGetObj.uValidAttributesMask = 0;
    sErrorResult = GetResourceValueBEHAVIOUR_ANALOGIN((E_ModuleResource)
      ((init_resource_AN5M_DataStore())), &HowToGetObj, &AnalogResultObj);
    if (SUCCESS(sErrorResult)) {
      *adc = AnalogResultObj.uADCValue ;
    } else {
      *adc = 0;
    }

    {
      extern uint8_T ain_read_AN5M;
      if (SUCCESS(sErrorResult))
        ain_read_AN5M = 0;
      else
        ain_read_AN5M = (uint8_T) GetErrorCode(sErrorResult);
    }
  }

  return sErrorResult;
}

/* S-Function Block: <S12>/motohawk_ain1 Resource: AN3M */
NativeError_S AN3M_AnalogInput_Create(void)
{
  NativeError_S sErrorResult = ERROR_RESOURCE_NOT_CREATED;
  S_AnalogInCreateAttributes CreateInfo;
  CreateInfo.DynamicObj.eResourceCondition = RES_ENABLED;
  CreateInfo.DynamicObj.uValidAttributesMask = USE_ANALOG_CONDITION;
  CreateInfo.uValidAttributesMask = USE_ANALOG_DYNAMIC_ON_CREATE;
  (init_resource_AN3M_DataStore()) = -1;
  if (((int32_T) RES_AN3M) >= 0) {
    sErrorResult = CreateResource((E_ModuleResource) (((int32_T) RES_AN3M)),
      &CreateInfo, BEHAVIOUR_ANALOGIN);
    if (SUCCESS(sErrorResult)) {
      (init_resource_AN3M_DataStore()) = ((int32_T) RES_AN3M);
    } else {
      LogNativeError(sErrorResult);
    }

    {
      extern uint8_T ain_create_AN3M;
      if (SUCCESS(sErrorResult))
        ain_create_AN3M = 0;
      else
        ain_create_AN3M = (uint8_T) GetErrorCode(sErrorResult);
    }
  }

  return sErrorResult;
}

NativeError_S AN3M_AnalogInput_Get(uint16_T *adc, uint16_T *status)
{
  NativeError_S sErrorResult = ERROR_FAIL;
  uint16_T Result;
  if ((init_resource_AN3M_DataStore()) >= 0) {
    S_AnalogHowToGet HowToGetObj;
    S_AnalogResult AnalogResultObj;
    HowToGetObj.uValidAttributesMask = 0;
    sErrorResult = GetResourceValueBEHAVIOUR_ANALOGIN((E_ModuleResource)
      ((init_resource_AN3M_DataStore())), &HowToGetObj, &AnalogResultObj);
    if (SUCCESS(sErrorResult)) {
      *adc = AnalogResultObj.uADCValue ;
    } else {
      *adc = 0;
    }

    {
      extern uint8_T ain_read_AN3M;
      if (SUCCESS(sErrorResult))
        ain_read_AN3M = 0;
      else
        ain_read_AN3M = (uint8_T) GetErrorCode(sErrorResult);
    }
  }

  return sErrorResult;
}

/* S-Function Block: <S14>/motohawk_ain14 Resource: DRVP */
NativeError_S DRVP_AnalogInput_Create(void)
{
  NativeError_S sErrorResult = ERROR_RESOURCE_NOT_CREATED;
  S_AnalogInCreateAttributes CreateInfo;
  CreateInfo.DynamicObj.eResourceCondition = RES_ENABLED;
  CreateInfo.DynamicObj.uValidAttributesMask = USE_ANALOG_CONDITION;
  CreateInfo.uValidAttributesMask = USE_ANALOG_DYNAMIC_ON_CREATE;
  (init_resource_DRVP_DataStore()) = -1;
  if (((int32_T) RES_DRVP) >= 0) {
    sErrorResult = CreateResource((E_ModuleResource) (((int32_T) RES_DRVP)),
      &CreateInfo, BEHAVIOUR_ANALOGIN);
    if (SUCCESS(sErrorResult)) {
      (init_resource_DRVP_DataStore()) = ((int32_T) RES_DRVP);
    } else {
      LogNativeError(sErrorResult);
    }

    {
      extern uint8_T ain_create_DRVP;
      if (SUCCESS(sErrorResult))
        ain_create_DRVP = 0;
      else
        ain_create_DRVP = (uint8_T) GetErrorCode(sErrorResult);
    }
  }

  return sErrorResult;
}

NativeError_S DRVP_AnalogInput_Get(uint16_T *adc, uint16_T *status)
{
  NativeError_S sErrorResult = ERROR_FAIL;
  uint16_T Result;
  if ((init_resource_DRVP_DataStore()) >= 0) {
    S_AnalogHowToGet HowToGetObj;
    S_AnalogResult AnalogResultObj;
    HowToGetObj.uValidAttributesMask = 0;
    sErrorResult = GetResourceValueBEHAVIOUR_ANALOGIN((E_ModuleResource)
      ((init_resource_DRVP_DataStore())), &HowToGetObj, &AnalogResultObj);
    if (SUCCESS(sErrorResult)) {
      *adc = AnalogResultObj.uADCValue ;
    } else {
      *adc = 0;
    }

    {
      extern uint8_T ain_read_DRVP;
      if (SUCCESS(sErrorResult))
        ain_read_DRVP = 0;
      else
        ain_read_DRVP = (uint8_T) GetErrorCode(sErrorResult);
    }
  }

  return sErrorResult;
}

/* S-Function Block: <S15>/motohawk_ain4 Resource: AN7M */
NativeError_S AN7M_AnalogInput_Create(void)
{
  NativeError_S sErrorResult = ERROR_RESOURCE_NOT_CREATED;
  S_AnalogInCreateAttributes CreateInfo;
  CreateInfo.DynamicObj.eResourceCondition = RES_ENABLED;
  CreateInfo.DynamicObj.uValidAttributesMask = USE_ANALOG_CONDITION;
  CreateInfo.uValidAttributesMask = USE_ANALOG_DYNAMIC_ON_CREATE;
  (init_resource_AN7M_DataStore()) = -1;
  if (((int32_T) RES_AN7M) >= 0) {
    sErrorResult = CreateResource((E_ModuleResource) (((int32_T) RES_AN7M)),
      &CreateInfo, BEHAVIOUR_ANALOGIN);
    if (SUCCESS(sErrorResult)) {
      (init_resource_AN7M_DataStore()) = ((int32_T) RES_AN7M);
    } else {
      LogNativeError(sErrorResult);
    }

    {
      extern uint8_T ain_create_AN7M;
      if (SUCCESS(sErrorResult))
        ain_create_AN7M = 0;
      else
        ain_create_AN7M = (uint8_T) GetErrorCode(sErrorResult);
    }
  }

  return sErrorResult;
}

NativeError_S AN7M_AnalogInput_Get(uint16_T *adc, uint16_T *status)
{
  NativeError_S sErrorResult = ERROR_FAIL;
  uint16_T Result;
  if ((init_resource_AN7M_DataStore()) >= 0) {
    S_AnalogHowToGet HowToGetObj;
    S_AnalogResult AnalogResultObj;
    HowToGetObj.uValidAttributesMask = 0;
    sErrorResult = GetResourceValueBEHAVIOUR_ANALOGIN((E_ModuleResource)
      ((init_resource_AN7M_DataStore())), &HowToGetObj, &AnalogResultObj);
    if (SUCCESS(sErrorResult)) {
      *adc = AnalogResultObj.uADCValue ;
    } else {
      *adc = 0;
    }

    {
      extern uint8_T ain_read_AN7M;
      if (SUCCESS(sErrorResult))
        ain_read_AN7M = 0;
      else
        ain_read_AN7M = (uint8_T) GetErrorCode(sErrorResult);
    }
  }

  return sErrorResult;
}

/* S-Function Block: <S16>/motohawk_ain Resource: AN1M */
NativeError_S AN1M_AnalogInput_Create(void)
{
  NativeError_S sErrorResult = ERROR_RESOURCE_NOT_CREATED;
  S_AnalogInCreateAttributes CreateInfo;
  CreateInfo.DynamicObj.eResourceCondition = RES_ENABLED;
  CreateInfo.DynamicObj.uValidAttributesMask = USE_ANALOG_CONDITION;
  CreateInfo.uValidAttributesMask = USE_ANALOG_DYNAMIC_ON_CREATE;
  (init_resource_AN1M_DataStore()) = -1;
  if (((int32_T) RES_AN1M) >= 0) {
    sErrorResult = CreateResource((E_ModuleResource) (((int32_T) RES_AN1M)),
      &CreateInfo, BEHAVIOUR_ANALOGIN);
    if (SUCCESS(sErrorResult)) {
      (init_resource_AN1M_DataStore()) = ((int32_T) RES_AN1M);
    } else {
      LogNativeError(sErrorResult);
    }

    {
      extern uint8_T ain_create_AN1M;
      if (SUCCESS(sErrorResult))
        ain_create_AN1M = 0;
      else
        ain_create_AN1M = (uint8_T) GetErrorCode(sErrorResult);
    }
  }

  return sErrorResult;
}

NativeError_S AN1M_AnalogInput_Get(uint16_T *adc, uint16_T *status)
{
  NativeError_S sErrorResult = ERROR_FAIL;
  uint16_T Result;
  if ((init_resource_AN1M_DataStore()) >= 0) {
    S_AnalogHowToGet HowToGetObj;
    S_AnalogResult AnalogResultObj;
    HowToGetObj.uValidAttributesMask = 0;
    sErrorResult = GetResourceValueBEHAVIOUR_ANALOGIN((E_ModuleResource)
      ((init_resource_AN1M_DataStore())), &HowToGetObj, &AnalogResultObj);
    if (SUCCESS(sErrorResult)) {
      *adc = AnalogResultObj.uADCValue ;
    } else {
      *adc = 0;
    }

    {
      extern uint8_T ain_read_AN1M;
      if (SUCCESS(sErrorResult))
        ain_read_AN1M = 0;
      else
        ain_read_AN1M = (uint8_T) GetErrorCode(sErrorResult);
    }
  }

  return sErrorResult;
}

/* S-Function Block: <S17>/motohawk_ain1 Resource: AN4M */
NativeError_S AN4M_AnalogInput_Create(void)
{
  NativeError_S sErrorResult = ERROR_RESOURCE_NOT_CREATED;
  S_AnalogInCreateAttributes CreateInfo;
  CreateInfo.DynamicObj.eResourceCondition = RES_ENABLED;
  CreateInfo.DynamicObj.uValidAttributesMask = USE_ANALOG_CONDITION;
  CreateInfo.uValidAttributesMask = USE_ANALOG_DYNAMIC_ON_CREATE;
  (init_resource_AN4M_DataStore()) = -1;
  if (((int32_T) RES_AN4M) >= 0) {
    sErrorResult = CreateResource((E_ModuleResource) (((int32_T) RES_AN4M)),
      &CreateInfo, BEHAVIOUR_ANALOGIN);
    if (SUCCESS(sErrorResult)) {
      (init_resource_AN4M_DataStore()) = ((int32_T) RES_AN4M);
    } else {
      LogNativeError(sErrorResult);
    }

    {
      extern uint8_T ain_create_AN4M;
      if (SUCCESS(sErrorResult))
        ain_create_AN4M = 0;
      else
        ain_create_AN4M = (uint8_T) GetErrorCode(sErrorResult);
    }
  }

  return sErrorResult;
}

NativeError_S AN4M_AnalogInput_Get(uint16_T *adc, uint16_T *status)
{
  NativeError_S sErrorResult = ERROR_FAIL;
  uint16_T Result;
  if ((init_resource_AN4M_DataStore()) >= 0) {
    S_AnalogHowToGet HowToGetObj;
    S_AnalogResult AnalogResultObj;
    HowToGetObj.uValidAttributesMask = 0;
    sErrorResult = GetResourceValueBEHAVIOUR_ANALOGIN((E_ModuleResource)
      ((init_resource_AN4M_DataStore())), &HowToGetObj, &AnalogResultObj);
    if (SUCCESS(sErrorResult)) {
      *adc = AnalogResultObj.uADCValue ;
    } else {
      *adc = 0;
    }

    {
      extern uint8_T ain_read_AN4M;
      if (SUCCESS(sErrorResult))
        ain_read_AN4M = 0;
      else
        ain_read_AN4M = (uint8_T) GetErrorCode(sErrorResult);
    }
  }

  return sErrorResult;
}

/* S-Function Block: <S18>/motohawk_ain1 Resource: AN2M */
NativeError_S AN2M_AnalogInput_Create(void)
{
  NativeError_S sErrorResult = ERROR_RESOURCE_NOT_CREATED;
  S_AnalogInCreateAttributes CreateInfo;
  CreateInfo.DynamicObj.eResourceCondition = RES_ENABLED;
  CreateInfo.DynamicObj.uValidAttributesMask = USE_ANALOG_CONDITION;
  CreateInfo.uValidAttributesMask = USE_ANALOG_DYNAMIC_ON_CREATE;
  (init_resource_AN2M_DataStore()) = -1;
  if (((int32_T) RES_AN2M) >= 0) {
    sErrorResult = CreateResource((E_ModuleResource) (((int32_T) RES_AN2M)),
      &CreateInfo, BEHAVIOUR_ANALOGIN);
    if (SUCCESS(sErrorResult)) {
      (init_resource_AN2M_DataStore()) = ((int32_T) RES_AN2M);
    } else {
      LogNativeError(sErrorResult);
    }

    {
      extern uint8_T ain_create_AN2M;
      if (SUCCESS(sErrorResult))
        ain_create_AN2M = 0;
      else
        ain_create_AN2M = (uint8_T) GetErrorCode(sErrorResult);
    }
  }

  return sErrorResult;
}

NativeError_S AN2M_AnalogInput_Get(uint16_T *adc, uint16_T *status)
{
  NativeError_S sErrorResult = ERROR_FAIL;
  uint16_T Result;
  if ((init_resource_AN2M_DataStore()) >= 0) {
    S_AnalogHowToGet HowToGetObj;
    S_AnalogResult AnalogResultObj;
    HowToGetObj.uValidAttributesMask = 0;
    sErrorResult = GetResourceValueBEHAVIOUR_ANALOGIN((E_ModuleResource)
      ((init_resource_AN2M_DataStore())), &HowToGetObj, &AnalogResultObj);
    if (SUCCESS(sErrorResult)) {
      *adc = AnalogResultObj.uADCValue ;
    } else {
      *adc = 0;
    }

    {
      extern uint8_T ain_read_AN2M;
      if (SUCCESS(sErrorResult))
        ain_read_AN2M = 0;
      else
        ain_read_AN2M = (uint8_T) GetErrorCode(sErrorResult);
    }
  }

  return sErrorResult;
}

/* S-Function Block: <S97>/motohawk_dout Resource: DOut1918p0001 */
NativeError_S DOut1918p0001_DiscreteOutput_Create(void)
{
  NativeError_S sErrorResult;
  S_DiscreteOutCreateAttributes CreateInfo;
  CreateInfo.DynamicObj.eState = RES_OFF;
  CreateInfo.DynamicObj.eResourceCondition = RES_ENABLED;
  CreateInfo.DynamicObj.uValidAttributesMask = USE_DISCRETE_CONDITION |
    USE_DISCRETE_STATE;
  CreateInfo.uValidAttributesMask = USE_DISCRETE_DYNAMIC_ON_CREATE;
  sErrorResult = CreateResource((E_ModuleResource) (((int16_T) 52)), &CreateInfo,
    BEHAVIOUR_DISCRETE_OUT);
  if (SUCCESS(sErrorResult)) {
    (init_resource_DOut1918p0001_DataStore()) = ((int16_T) 52);
  } else {
    (init_resource_DOut1918p0001_DataStore()) = -1;
    LogNativeError(sErrorResult);
  }

  {
    extern uint8_T dout_create_DOut1918p0001;
    if (SUCCESS(sErrorResult))
      dout_create_DOut1918p0001 = 0;
    else
      dout_create_DOut1918p0001 = (uint8_T) GetErrorCode(sErrorResult);
  }

  return sErrorResult;
}

NativeError_S DOut1918p0001_DiscreteOutput_Set(boolean_T in)
{
  if ((init_resource_DOut1918p0001_DataStore()) >= 0) {
    return SetDiscreteOutState((E_ModuleResource)
      ((init_resource_DOut1918p0001_DataStore())), (in) ? RES_ON : RES_OFF);
  }

  return ERROR_FAIL;                   /* Return an error */
}

NativeError_S DOut1918p0001_DiscreteOutputPushPull_Set(int8_T in)
{
  if ((init_resource_DOut1918p0001_DataStore()) >= 0) {
    return SetDiscreteOutState((E_ModuleResource)
      ((init_resource_DOut1918p0001_DataStore())), ((in) < 0) ? RES_ON_REVERSE :
                               (((in) > 0) ? RES_ON : RES_OFF));
  }

  return ERROR_FAIL;                   /* Return an error */
}

void LSD10_PWMOutput_PWMOutput_Set(uint32_T freq, int16_T duty, boolean_T brake,
  boolean_T enable)
{
  if (((init_resource_LSD10_PWMOutput_DataStore()) < 0) && (((int16_T) RES_LSD10)
       >= 0)) {
    /* Initialize PWM Output with initial frequency and duty_cycle */
    S_PWMCreateResourceAttributes CreateInfo;
    NativeError_S sErrorResult;
    CreateInfo.uValidAttributesMask = USE_PWM_DYNAMIC_ON_CREATE |
      USE_PWM_MIN_FREQUENCY;
    CreateInfo.DynamicObj.uValidAttributesMask = USE_PWM_DUTYCYCLE |
      USE_PWM_FREQUENCY | USE_PWM_CONDITION;
    CreateInfo.pfDiagCback = 0;
    CreateInfo.uDiagCbackUserData = 0;
    CreateInfo.uValidAttributesMask |= USE_PWM_PARENT;
    CreateInfo.u4MinFrequency = 200;
    CreateInfo.DynamicObj.eResourceCondition = RES_ENABLED;
    CreateInfo.DynamicObj.u4Frequency = freq;
    CreateInfo.DynamicObj.s2DutyCycle = duty;
    sErrorResult = CreateResource((E_ModuleResource) (((int16_T) RES_LSD10)),
      &CreateInfo, BEHAVIOUR_PWM);
    if (SUCCESS(sErrorResult)) {
      (init_resource_LSD10_PWMOutput_DataStore()) = ((int16_T) RES_LSD10);
    } else {
      (init_resource_LSD10_PWMOutput_DataStore()) = -1;
      LogNativeError(sErrorResult);
    }

    {
      extern uint8_T pwm_create_LSD10_PWMOutput;
      if (SUCCESS(sErrorResult))
        pwm_create_LSD10_PWMOutput = 0;
      else
        pwm_create_LSD10_PWMOutput = (uint8_T) GetErrorCode(sErrorResult);
    }
  } else if ((init_resource_LSD10_PWMOutput_DataStore()) >= 0) {
    NativeError_S sErrorResult;

    /* Update PWM output with modified frequency and duty_cycle */
    S_PWMResourceAttributes DynamicObj;
    DynamicObj.uValidAttributesMask = USE_PWM_DUTYCYCLE | USE_PWM_FREQUENCY;
    DynamicObj.u4Frequency = freq;
    DynamicObj.s2DutyCycle = duty;
    sErrorResult = SetResourceAttributes((E_ModuleResource)
      ((init_resource_LSD10_PWMOutput_DataStore())), &DynamicObj, BEHAVIOUR_PWM);

    {
      extern uint8_T pwm_set_LSD10_PWMOutput;
      if (SUCCESS(sErrorResult))
        pwm_set_LSD10_PWMOutput = 0;
      else
        pwm_set_LSD10_PWMOutput = (uint8_T) GetErrorCode(sErrorResult);
    }
  }
}

void LSD10_PWMOutput_PWMOutput_Stop()
{
  S_PWMResourceAttributes DynamicObj;
  DynamicObj.uValidAttributesMask = USE_PWM_CONDITION;
  DynamicObj.eResourceCondition = RES_DISABLED;
  SetResourceAttributes((E_ModuleResource)
                        ((init_resource_LSD10_PWMOutput_DataStore())),
                        &DynamicObj, BEHAVIOUR_PWM);
}

void LSD10_PWMOutput_PWMOutput_Create()
{
  (init_resource_LSD10_PWMOutput_DataStore()) = -1;
}

void EGR_Command_PWMOutput_Set(uint32_T freq, int16_T duty, boolean_T brake,
  boolean_T enable)
{
  if (((init_resource_EGR_Command_DataStore()) < 0) && (((int16_T) RES_LSD6) >=
       0)) {
    /* Initialize PWM Output with initial frequency and duty_cycle */
    S_PWMCreateResourceAttributes CreateInfo;
    NativeError_S sErrorResult;
    CreateInfo.uValidAttributesMask = USE_PWM_DYNAMIC_ON_CREATE |
      USE_PWM_MIN_FREQUENCY;
    CreateInfo.DynamicObj.uValidAttributesMask = USE_PWM_DUTYCYCLE |
      USE_PWM_FREQUENCY | USE_PWM_CONDITION;
    CreateInfo.pfDiagCback = 0;
    CreateInfo.uDiagCbackUserData = 0;
    CreateInfo.u4MinFrequency = 200;
    CreateInfo.DynamicObj.eResourceCondition = RES_ENABLED;
    CreateInfo.DynamicObj.u4Frequency = freq;
    CreateInfo.DynamicObj.s2DutyCycle = duty;
    sErrorResult = CreateResource((E_ModuleResource) (((int16_T) RES_LSD6)),
      &CreateInfo, BEHAVIOUR_PWM);
    if (SUCCESS(sErrorResult)) {
      (init_resource_EGR_Command_DataStore()) = ((int16_T) RES_LSD6);
    } else {
      (init_resource_EGR_Command_DataStore()) = -1;
      LogNativeError(sErrorResult);
    }

    {
      extern uint8_T pwm_create_EGR_Command;
      if (SUCCESS(sErrorResult))
        pwm_create_EGR_Command = 0;
      else
        pwm_create_EGR_Command = (uint8_T) GetErrorCode(sErrorResult);
    }
  } else if ((init_resource_EGR_Command_DataStore()) >= 0) {
    NativeError_S sErrorResult;

    /* Update PWM output with modified frequency and duty_cycle */
    S_PWMResourceAttributes DynamicObj;
    DynamicObj.uValidAttributesMask = USE_PWM_DUTYCYCLE | USE_PWM_FREQUENCY;
    DynamicObj.u4Frequency = freq;
    DynamicObj.s2DutyCycle = duty;
    sErrorResult = SetResourceAttributes((E_ModuleResource)
      ((init_resource_EGR_Command_DataStore())), &DynamicObj, BEHAVIOUR_PWM);

    {
      extern uint8_T pwm_set_EGR_Command;
      if (SUCCESS(sErrorResult))
        pwm_set_EGR_Command = 0;
      else
        pwm_set_EGR_Command = (uint8_T) GetErrorCode(sErrorResult);
    }
  }
}

void EGR_Command_PWMOutput_Stop()
{
  S_PWMResourceAttributes DynamicObj;
  DynamicObj.uValidAttributesMask = USE_PWM_CONDITION;
  DynamicObj.eResourceCondition = RES_DISABLED;
  SetResourceAttributes((E_ModuleResource) ((init_resource_EGR_Command_DataStore
                          ())), &DynamicObj, BEHAVIOUR_PWM);
}

void EGR_Command_PWMOutput_Create()
{
  (init_resource_EGR_Command_DataStore()) = -1;
}

void H1A_Out_PWMOutput_Set(uint32_T freq, int16_T duty, boolean_T brake,
  boolean_T enable)
{
  if (((init_resource_H1A_Out_DataStore()) < 0) && (((int16_T) RES_H1) >= 0)) {
    /* Initialize PWM Output with initial frequency and duty_cycle */
    S_PWMCreateResourceAttributes CreateInfo;
    NativeError_S sErrorResult;
    CreateInfo.uValidAttributesMask = USE_PWM_DYNAMIC_ON_CREATE |
      USE_PWM_MIN_FREQUENCY;
    CreateInfo.DynamicObj.uValidAttributesMask = USE_PWM_DUTYCYCLE |
      USE_PWM_FREQUENCY | USE_PWM_CONDITION;
    CreateInfo.pfDiagCback = 0;
    CreateInfo.uDiagCbackUserData = 0;
    CreateInfo.u4MinFrequency = 200;
    CreateInfo.DynamicObj.eResourceCondition = (enable) ? RES_ENABLED :
      RES_DISABLED;
    CreateInfo.DynamicObj.u4Frequency = freq;
    CreateInfo.DynamicObj.s2DutyCycle = duty;
    sErrorResult = CreateResource((E_ModuleResource) (((int16_T) RES_H1)),
      &CreateInfo, BEHAVIOUR_PWM);
    if (SUCCESS(sErrorResult)) {
      (init_resource_H1A_Out_DataStore()) = ((int16_T) RES_H1);
    } else {
      (init_resource_H1A_Out_DataStore()) = -1;
      LogNativeError(sErrorResult);
    }

    {
      extern uint8_T pwm_create_H1A_Out;
      if (SUCCESS(sErrorResult))
        pwm_create_H1A_Out = 0;
      else
        pwm_create_H1A_Out = (uint8_T) GetErrorCode(sErrorResult);
    }
  } else if ((init_resource_H1A_Out_DataStore()) >= 0) {
    NativeError_S sErrorResult;

    /* Update PWM output with modified frequency and duty_cycle */
    S_PWMResourceAttributes DynamicObj;
    DynamicObj.uValidAttributesMask = USE_PWM_DUTYCYCLE | USE_PWM_FREQUENCY;
    DynamicObj.u4Frequency = freq;
    DynamicObj.s2DutyCycle = duty;
    DynamicObj.uValidAttributesMask |= USE_PWM_CONDITION;
    DynamicObj.eResourceCondition = (enable) ? RES_ENABLED : RES_DISABLED;
    sErrorResult = SetResourceAttributes((E_ModuleResource)
      ((init_resource_H1A_Out_DataStore())), &DynamicObj, BEHAVIOUR_PWM);

    {
      extern uint8_T pwm_set_H1A_Out;
      if (SUCCESS(sErrorResult))
        pwm_set_H1A_Out = 0;
      else
        pwm_set_H1A_Out = (uint8_T) GetErrorCode(sErrorResult);
    }
  }
}

void H1A_Out_PWMOutput_Stop()
{
  S_PWMResourceAttributes DynamicObj;
  DynamicObj.uValidAttributesMask = USE_PWM_CONDITION;
  DynamicObj.eResourceCondition = RES_DISABLED;
  SetResourceAttributes((E_ModuleResource) ((init_resource_H1A_Out_DataStore())),
                        &DynamicObj, BEHAVIOUR_PWM);
}

void H1A_Out_PWMOutput_Create()
{
  (init_resource_H1A_Out_DataStore()) = -1;
}

/* S-Function Block: <S101>/motohawk_dout4 Resource: DOut2397p0004 */
NativeError_S DOut2397p0004_DiscreteOutput_Create(void)
{
  NativeError_S sErrorResult;
  S_DiscreteOutCreateAttributes CreateInfo;
  CreateInfo.DynamicObj.eState = RES_OFF;
  CreateInfo.DynamicObj.eResourceCondition = RES_ENABLED;
  CreateInfo.DynamicObj.uValidAttributesMask = USE_DISCRETE_CONDITION |
    USE_DISCRETE_STATE;
  CreateInfo.uValidAttributesMask = USE_DISCRETE_DYNAMIC_ON_CREATE;
  sErrorResult = CreateResource((E_ModuleResource) (((int16_T) 29)), &CreateInfo,
    BEHAVIOUR_DISCRETE_OUT);
  if (SUCCESS(sErrorResult)) {
    (init_resource_DOut2397p0004_DataStore()) = ((int16_T) 29);
  } else {
    (init_resource_DOut2397p0004_DataStore()) = -1;
    LogNativeError(sErrorResult);
  }

  {
    extern uint8_T dout_create_DOut2397p0004;
    if (SUCCESS(sErrorResult))
      dout_create_DOut2397p0004 = 0;
    else
      dout_create_DOut2397p0004 = (uint8_T) GetErrorCode(sErrorResult);
  }

  return sErrorResult;
}

NativeError_S DOut2397p0004_DiscreteOutput_Set(boolean_T in)
{
  if ((init_resource_DOut2397p0004_DataStore()) >= 0) {
    return SetDiscreteOutState((E_ModuleResource)
      ((init_resource_DOut2397p0004_DataStore())), (in) ? RES_ON : RES_OFF);
  }

  return ERROR_FAIL;                   /* Return an error */
}

NativeError_S DOut2397p0004_DiscreteOutputPushPull_Set(int8_T in)
{
  if ((init_resource_DOut2397p0004_DataStore()) >= 0) {
    return SetDiscreteOutState((E_ModuleResource)
      ((init_resource_DOut2397p0004_DataStore())), ((in) < 0) ? RES_ON_REVERSE :
                               (((in) > 0) ? RES_ON : RES_OFF));
  }

  return ERROR_FAIL;                   /* Return an error */
}

void TACH_PWMOutput_PWMOutput_Set(uint32_T freq, int16_T duty, boolean_T brake,
  boolean_T enable)
{
  if (((init_resource_TACH_PWMOutput_DataStore()) < 0) && (((int16_T) RES_TACH) >=
       0)) {
    /* Initialize PWM Output with initial frequency and duty_cycle */
    S_PWMCreateResourceAttributes CreateInfo;
    NativeError_S sErrorResult;
    CreateInfo.uValidAttributesMask = USE_PWM_DYNAMIC_ON_CREATE |
      USE_PWM_MIN_FREQUENCY;
    CreateInfo.DynamicObj.uValidAttributesMask = USE_PWM_DUTYCYCLE |
      USE_PWM_FREQUENCY | USE_PWM_CONDITION;
    CreateInfo.pfDiagCback = 0;
    CreateInfo.uDiagCbackUserData = 0;
    CreateInfo.u4MinFrequency = 200;
    CreateInfo.DynamicObj.eResourceCondition = RES_ENABLED;
    CreateInfo.DynamicObj.u4Frequency = freq;
    CreateInfo.DynamicObj.s2DutyCycle = duty;
    sErrorResult = CreateResource((E_ModuleResource) (((int16_T) RES_TACH)),
      &CreateInfo, BEHAVIOUR_PWM);
    if (SUCCESS(sErrorResult)) {
      (init_resource_TACH_PWMOutput_DataStore()) = ((int16_T) RES_TACH);
    } else {
      (init_resource_TACH_PWMOutput_DataStore()) = -1;
      LogNativeError(sErrorResult);
    }

    {
      extern uint8_T pwm_create_TACH_PWMOutput;
      if (SUCCESS(sErrorResult))
        pwm_create_TACH_PWMOutput = 0;
      else
        pwm_create_TACH_PWMOutput = (uint8_T) GetErrorCode(sErrorResult);
    }
  } else if ((init_resource_TACH_PWMOutput_DataStore()) >= 0) {
    NativeError_S sErrorResult;

    /* Update PWM output with modified frequency and duty_cycle */
    S_PWMResourceAttributes DynamicObj;
    DynamicObj.uValidAttributesMask = USE_PWM_DUTYCYCLE | USE_PWM_FREQUENCY;
    DynamicObj.u4Frequency = freq;
    DynamicObj.s2DutyCycle = duty;
    sErrorResult = SetResourceAttributes((E_ModuleResource)
      ((init_resource_TACH_PWMOutput_DataStore())), &DynamicObj, BEHAVIOUR_PWM);

    {
      extern uint8_T pwm_set_TACH_PWMOutput;
      if (SUCCESS(sErrorResult))
        pwm_set_TACH_PWMOutput = 0;
      else
        pwm_set_TACH_PWMOutput = (uint8_T) GetErrorCode(sErrorResult);
    }
  }
}

void TACH_PWMOutput_PWMOutput_Stop()
{
  S_PWMResourceAttributes DynamicObj;
  DynamicObj.uValidAttributesMask = USE_PWM_CONDITION;
  DynamicObj.eResourceCondition = RES_DISABLED;
  SetResourceAttributes((E_ModuleResource)
                        ((init_resource_TACH_PWMOutput_DataStore())),
                        &DynamicObj, BEHAVIOUR_PWM);
}

void TACH_PWMOutput_PWMOutput_Create()
{
  (init_resource_TACH_PWMOutput_DataStore()) = -1;
}

void THWO_Command_PWMOutput_Set(uint32_T freq, int16_T duty, boolean_T brake,
  boolean_T enable)
{
  if (((init_resource_THWO_Command_DataStore()) < 0) && (((int16_T) RES_LSD8) >=
       0)) {
    /* Initialize PWM Output with initial frequency and duty_cycle */
    S_PWMCreateResourceAttributes CreateInfo;
    NativeError_S sErrorResult;
    CreateInfo.uValidAttributesMask = USE_PWM_DYNAMIC_ON_CREATE |
      USE_PWM_MIN_FREQUENCY;
    CreateInfo.DynamicObj.uValidAttributesMask = USE_PWM_DUTYCYCLE |
      USE_PWM_FREQUENCY | USE_PWM_CONDITION;
    CreateInfo.pfDiagCback = 0;
    CreateInfo.uDiagCbackUserData = 0;
    CreateInfo.u4MinFrequency = 200;
    CreateInfo.DynamicObj.eResourceCondition = RES_ENABLED;
    CreateInfo.DynamicObj.u4Frequency = freq;
    CreateInfo.DynamicObj.s2DutyCycle = duty;
    sErrorResult = CreateResource((E_ModuleResource) (((int16_T) RES_LSD8)),
      &CreateInfo, BEHAVIOUR_PWM);
    if (SUCCESS(sErrorResult)) {
      (init_resource_THWO_Command_DataStore()) = ((int16_T) RES_LSD8);
    } else {
      (init_resource_THWO_Command_DataStore()) = -1;
      LogNativeError(sErrorResult);
    }

    {
      extern uint8_T pwm_create_THWO_Command;
      if (SUCCESS(sErrorResult))
        pwm_create_THWO_Command = 0;
      else
        pwm_create_THWO_Command = (uint8_T) GetErrorCode(sErrorResult);
    }
  } else if ((init_resource_THWO_Command_DataStore()) >= 0) {
    NativeError_S sErrorResult;

    /* Update PWM output with modified frequency and duty_cycle */
    S_PWMResourceAttributes DynamicObj;
    DynamicObj.uValidAttributesMask = USE_PWM_DUTYCYCLE | USE_PWM_FREQUENCY;
    DynamicObj.u4Frequency = freq;
    DynamicObj.s2DutyCycle = duty;
    sErrorResult = SetResourceAttributes((E_ModuleResource)
      ((init_resource_THWO_Command_DataStore())), &DynamicObj, BEHAVIOUR_PWM);

    {
      extern uint8_T pwm_set_THWO_Command;
      if (SUCCESS(sErrorResult))
        pwm_set_THWO_Command = 0;
      else
        pwm_set_THWO_Command = (uint8_T) GetErrorCode(sErrorResult);
    }
  }
}

void THWO_Command_PWMOutput_Stop()
{
  S_PWMResourceAttributes DynamicObj;
  DynamicObj.uValidAttributesMask = USE_PWM_CONDITION;
  DynamicObj.eResourceCondition = RES_DISABLED;
  SetResourceAttributes((E_ModuleResource)
                        ((init_resource_THWO_Command_DataStore())), &DynamicObj,
                        BEHAVIOUR_PWM);
}

void THWO_Command_PWMOutput_Create()
{
  (init_resource_THWO_Command_DataStore()) = -1;
}

void LSD9_PWMOutput_PWMOutput_Set(uint32_T freq, int16_T duty, boolean_T brake,
  boolean_T enable)
{
  if (((init_resource_LSD9_PWMOutput_DataStore()) < 0) && (((int16_T) RES_LSD9) >=
       0)) {
    /* Initialize PWM Output with initial frequency and duty_cycle */
    S_PWMCreateResourceAttributes CreateInfo;
    NativeError_S sErrorResult;
    CreateInfo.uValidAttributesMask = USE_PWM_DYNAMIC_ON_CREATE |
      USE_PWM_MIN_FREQUENCY;
    CreateInfo.DynamicObj.uValidAttributesMask = USE_PWM_DUTYCYCLE |
      USE_PWM_FREQUENCY | USE_PWM_CONDITION;
    CreateInfo.pfDiagCback = 0;
    CreateInfo.uDiagCbackUserData = 0;
    CreateInfo.u4MinFrequency = 200;
    CreateInfo.DynamicObj.eResourceCondition = RES_ENABLED;
    CreateInfo.DynamicObj.u4Frequency = freq;
    CreateInfo.DynamicObj.s2DutyCycle = duty;
    sErrorResult = CreateResource((E_ModuleResource) (((int16_T) RES_LSD9)),
      &CreateInfo, BEHAVIOUR_PWM);
    if (SUCCESS(sErrorResult)) {
      (init_resource_LSD9_PWMOutput_DataStore()) = ((int16_T) RES_LSD9);
    } else {
      (init_resource_LSD9_PWMOutput_DataStore()) = -1;
      LogNativeError(sErrorResult);
    }

    {
      extern uint8_T pwm_create_LSD9_PWMOutput;
      if (SUCCESS(sErrorResult))
        pwm_create_LSD9_PWMOutput = 0;
      else
        pwm_create_LSD9_PWMOutput = (uint8_T) GetErrorCode(sErrorResult);
    }
  } else if ((init_resource_LSD9_PWMOutput_DataStore()) >= 0) {
    NativeError_S sErrorResult;

    /* Update PWM output with modified frequency and duty_cycle */
    S_PWMResourceAttributes DynamicObj;
    DynamicObj.uValidAttributesMask = USE_PWM_DUTYCYCLE | USE_PWM_FREQUENCY;
    DynamicObj.u4Frequency = freq;
    DynamicObj.s2DutyCycle = duty;
    sErrorResult = SetResourceAttributes((E_ModuleResource)
      ((init_resource_LSD9_PWMOutput_DataStore())), &DynamicObj, BEHAVIOUR_PWM);

    {
      extern uint8_T pwm_set_LSD9_PWMOutput;
      if (SUCCESS(sErrorResult))
        pwm_set_LSD9_PWMOutput = 0;
      else
        pwm_set_LSD9_PWMOutput = (uint8_T) GetErrorCode(sErrorResult);
    }
  }
}

void LSD9_PWMOutput_PWMOutput_Stop()
{
  S_PWMResourceAttributes DynamicObj;
  DynamicObj.uValidAttributesMask = USE_PWM_CONDITION;
  DynamicObj.eResourceCondition = RES_DISABLED;
  SetResourceAttributes((E_ModuleResource)
                        ((init_resource_LSD9_PWMOutput_DataStore())),
                        &DynamicObj, BEHAVIOUR_PWM);
}

void LSD9_PWMOutput_PWMOutput_Create()
{
  (init_resource_LSD9_PWMOutput_DataStore()) = -1;
}

void VNTO_PWMOutput_Set(uint32_T freq, int16_T duty, boolean_T brake, boolean_T
  enable)
{
  if (((init_resource_VNTO_DataStore()) < 0) && (((int16_T) RES_LSD11) >= 0)) {
    /* Initialize PWM Output with initial frequency and duty_cycle */
    S_PWMCreateResourceAttributes CreateInfo;
    NativeError_S sErrorResult;
    CreateInfo.uValidAttributesMask = USE_PWM_DYNAMIC_ON_CREATE |
      USE_PWM_MIN_FREQUENCY;
    CreateInfo.DynamicObj.uValidAttributesMask = USE_PWM_DUTYCYCLE |
      USE_PWM_FREQUENCY | USE_PWM_CONDITION;
    CreateInfo.pfDiagCback = 0;
    CreateInfo.uDiagCbackUserData = 0;
    CreateInfo.u4MinFrequency = 200;
    CreateInfo.DynamicObj.eResourceCondition = RES_ENABLED;
    CreateInfo.DynamicObj.u4Frequency = freq;
    CreateInfo.DynamicObj.s2DutyCycle = duty;
    sErrorResult = CreateResource((E_ModuleResource) (((int16_T) RES_LSD11)),
      &CreateInfo, BEHAVIOUR_PWM);
    if (SUCCESS(sErrorResult)) {
      (init_resource_VNTO_DataStore()) = ((int16_T) RES_LSD11);
    } else {
      (init_resource_VNTO_DataStore()) = -1;
      LogNativeError(sErrorResult);
    }

    {
      extern uint8_T pwm_create_VNTO;
      if (SUCCESS(sErrorResult))
        pwm_create_VNTO = 0;
      else
        pwm_create_VNTO = (uint8_T) GetErrorCode(sErrorResult);
    }
  } else if ((init_resource_VNTO_DataStore()) >= 0) {
    NativeError_S sErrorResult;

    /* Update PWM output with modified frequency and duty_cycle */
    S_PWMResourceAttributes DynamicObj;
    DynamicObj.uValidAttributesMask = USE_PWM_DUTYCYCLE | USE_PWM_FREQUENCY;
    DynamicObj.u4Frequency = freq;
    DynamicObj.s2DutyCycle = duty;
    sErrorResult = SetResourceAttributes((E_ModuleResource)
      ((init_resource_VNTO_DataStore())), &DynamicObj, BEHAVIOUR_PWM);

    {
      extern uint8_T pwm_set_VNTO;
      if (SUCCESS(sErrorResult))
        pwm_set_VNTO = 0;
      else
        pwm_set_VNTO = (uint8_T) GetErrorCode(sErrorResult);
    }
  }
}

void VNTO_PWMOutput_Stop()
{
  S_PWMResourceAttributes DynamicObj;
  DynamicObj.uValidAttributesMask = USE_PWM_CONDITION;
  DynamicObj.eResourceCondition = RES_DISABLED;
  SetResourceAttributes((E_ModuleResource) ((init_resource_VNTO_DataStore())),
                        &DynamicObj, BEHAVIOUR_PWM);
}

void VNTO_PWMOutput_Create()
{
  (init_resource_VNTO_DataStore()) = -1;
}

/* We need to keep the pulse offsets in order to enable in order (global const) */
int16_T const MuxPSP1_PulseOffsetDegATDC1Arr[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };

/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
   :: MuxPSP1_MuxPSP_HardStart_SoftDuration_Set
   :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
/* Manages:
   - updating of timings
   - disabling if pulses can't be scheduled
   - enabling pulses in the correct order
 */
void MuxPSP1_MuxPSP_HardStart_SoftDuration_Set(uint8_T const state[], boolean_T
  use_state_min, int16_T const start_angle[], boolean_T use_start_angle_min,
  uint32_T const duration[], boolean_T use_dur_min, uint32_T const* min_dur)
{
  S_SeqOutAttributes in_pAttribsObj;
  NativeError_S sError;
  NativeError_S sErrorResult = SUCCESS_OK;
  E_SeqOutCond condition;
  int idx, count;
  uint8_T set_state;
  in_pAttribsObj.uValidAttributesMask = USE_SEQ_PULSE_COND | USE_SEQ_TIMING;
  if (min_dur) {
    in_pAttribsObj.uValidAttributesMask |= USE_SEQ_MIN_DURATION;
    in_pAttribsObj.uMinPulseDurationInMicroSecs = *min_dur;
  }

  in_pAttribsObj.TimingObj.eCtrlMode = DURATION_CTRL;

  /* Ensure pulses are disabled if pulses can't be scheduled */
  /* And Enabled in the correct order if pulses can be scheduled */
  {
    /* Use SEQ_DISABLED if encoder is not sync-ed */
    {
      extern uint8_T motohawk_encoder_status;
      condition = SEQ_DISABLED;
      if (motohawk_encoder_status >= (uint8_T) ENC_SYNC_CAM_UNKNOWN) {
        condition = SEQ_DISNEXT;
      }
    }

    idx = 0;
    if (condition != SEQ_DISABLED) {
      int32_T next_index = 0;
      int16_T max_angle_diff = 0;
      int16_T angle_diff;
      int16_T current_angle;
      int16_T desired_angle;
      int16_T cylinder_angle;
      current_angle = 720*16 - GetEncoderEngAngle();
      for (idx=0; idx < 3; idx++) {
        desired_angle = (use_start_angle_min) ? start_angle[0] : start_angle[idx];
        cylinder_angle = MuxPSP1_PulseOffsetDegATDC1Arr[idx] - desired_angle;
        angle_diff = current_angle - cylinder_angle;
        while (angle_diff < 0) {
          angle_diff += 720*16;
        }

        while (angle_diff >= 720*16) {
          angle_diff -= 720*16;
        }

        if (angle_diff > max_angle_diff) {
          max_angle_diff = angle_diff;
          next_index = idx;
        }
      }

      idx = next_index;
    }

    DISABLE();
    for (count=0; count < 3; count++) {
      boolean_T bValid = 1;

      /* Determine what the new state should be for THIS pulse */
      set_state = (use_state_min) ? state[0] : state[idx];
      bValid = (idx < MAX_MUX_PULSES_SUPPORTED);
      if (bValid) {
        in_pAttribsObj.u1AffectedPulse = idx;
        in_pAttribsObj.TimingObj.s2StartAngle = (use_start_angle_min) ?
          start_angle[0] : start_angle[idx];
        in_pAttribsObj.TimingObj.u4Duration = (use_dur_min) ? duration[0] :
          duration[idx];
        if (condition != SEQ_DISABLED) {
          in_pAttribsObj.eResourceCondition = set_state ? SEQ_ENABLED :
            SEQ_DISNEXT;
        } else {
          in_pAttribsObj.eResourceCondition = condition;
        }

        sError = SetResourceAttributesSequenceOut((E_ModuleResource) (36),
          &in_pAttribsObj, BEHAVIOUR_MUX_EST_SEQ);
      } else {
        sError = ERROR_BAD_ATTRIBUTE_DATA;
      }

      if FAILED_OR_WARNING(sError) {
        sErrorResult = sError;
      }

      idx ++;
      if (idx >= 3) {
        idx = 0;
      }
    }

    UNDISABLE();

    {
      extern uint8_T muxpsp_set_MuxPSP1;
      if (SUCCESS(sErrorResult))
        muxpsp_set_MuxPSP1 = 0;
      else
        muxpsp_set_MuxPSP1 = (uint8_T) GetErrorCode(sErrorResult);
    }
  }
}

/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
   :: MuxPSP1_MuxPSP_Create()
   :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
/* Create MuxPSP resource
 */
void MuxPSP1_MuxPSP_Create(void)
{
  S_MuxESTSeqCreateAttributes CreateESTObj;
  S_SeqOutPulseTiming s_ESTTimingObj;
  NativeError_S sError;
  NativeVar_U i;
  CreateESTObj.uValidAttributesMask = USE_SEQ_GRANULARITY | USE_SEQ_CTRLMODE;
  CreateESTObj.eGranularity = FINE_GRANULARITY;
  CreateESTObj.eCtrlMode = DURATION_CTRL;

  /* Required attributes */
  CreateESTObj.u1NumPulsesToCreate = 3;
  memcpy(CreateESTObj.s2PulseOffsetDegATDC1Arr, MuxPSP1_PulseOffsetDegATDC1Arr,
         sizeof(CreateESTObj.s2PulseOffsetDegATDC1Arr));
  sError = CreateResourceBEHAVIOUR_MUX_EST_SEQ((E_ModuleResource) (36),
    &CreateESTObj);

  {
    extern uint8_T muxpsp_create_MuxPSP1;
    if (SUCCESS(sError))
      muxpsp_create_MuxPSP1 = 0;
    else
      muxpsp_create_MuxPSP1 = (uint8_T) GetErrorCode(sError);
  }

  if (SUCCESS(sError)) {
    uint8_T state = 0;
    int16_T s2StartAngle = 0;
    uint32_T u4Duration = 0;
    uint32_T min_duration = 200;
    MuxPSP1_MuxPSP_HardStart_SoftDuration_Set(&state, 1, &s2StartAngle, 1,
      &u4Duration, 1, &min_duration);
  }
}

/* We need to keep the pulse offsets in order to enable in order (global const) */
int16_T const MuxPSP2_PulseOffsetDegATDC1Arr[8] = { 8640, 8640, 8640, 0, 0, 0, 0,
  0 };

/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
   :: MuxPSP2_MuxPSP_HardStart_SoftDuration_Set
   :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
/* Manages:
   - updating of timings
   - disabling if pulses can't be scheduled
   - enabling pulses in the correct order
 */
void MuxPSP2_MuxPSP_HardStart_SoftDuration_Set(uint8_T const state[], boolean_T
  use_state_min, int16_T const start_angle[], boolean_T use_start_angle_min,
  uint32_T const duration[], boolean_T use_dur_min, uint32_T const* min_dur)
{
  S_SeqOutAttributes in_pAttribsObj;
  NativeError_S sError;
  NativeError_S sErrorResult = SUCCESS_OK;
  E_SeqOutCond condition;
  int idx, count;
  uint8_T set_state;
  in_pAttribsObj.uValidAttributesMask = USE_SEQ_PULSE_COND | USE_SEQ_TIMING;
  if (min_dur) {
    in_pAttribsObj.uValidAttributesMask |= USE_SEQ_MIN_DURATION;
    in_pAttribsObj.uMinPulseDurationInMicroSecs = *min_dur;
  }

  in_pAttribsObj.TimingObj.eCtrlMode = DURATION_CTRL;

  /* Ensure pulses are disabled if pulses can't be scheduled */
  /* And Enabled in the correct order if pulses can be scheduled */
  {
    /* Use SEQ_DISABLED if encoder is not sync-ed */
    {
      extern uint8_T motohawk_encoder_status;
      condition = SEQ_DISABLED;
      if (motohawk_encoder_status >= (uint8_T) ENC_SYNC_CAM_UNKNOWN) {
        condition = SEQ_DISNEXT;
      }
    }

    idx = 0;
    if (condition != SEQ_DISABLED) {
      int32_T next_index = 0;
      int16_T max_angle_diff = 0;
      int16_T angle_diff;
      int16_T current_angle;
      int16_T desired_angle;
      int16_T cylinder_angle;
      current_angle = 720*16 - GetEncoderEngAngle();
      for (idx=0; idx < 3; idx++) {
        desired_angle = (use_start_angle_min) ? start_angle[0] : start_angle[idx];
        cylinder_angle = MuxPSP2_PulseOffsetDegATDC1Arr[idx] - desired_angle;
        angle_diff = current_angle - cylinder_angle;
        while (angle_diff < 0) {
          angle_diff += 720*16;
        }

        while (angle_diff >= 720*16) {
          angle_diff -= 720*16;
        }

        if (angle_diff > max_angle_diff) {
          max_angle_diff = angle_diff;
          next_index = idx;
        }
      }

      idx = next_index;
    }

    DISABLE();
    for (count=0; count < 3; count++) {
      boolean_T bValid = 1;

      /* Determine what the new state should be for THIS pulse */
      set_state = (use_state_min) ? state[0] : state[idx];
      bValid = (idx < MAX_MUX_PULSES_SUPPORTED);
      if (bValid) {
        in_pAttribsObj.u1AffectedPulse = idx;
        in_pAttribsObj.TimingObj.s2StartAngle = (use_start_angle_min) ?
          start_angle[0] : start_angle[idx];
        in_pAttribsObj.TimingObj.u4Duration = (use_dur_min) ? duration[0] :
          duration[idx];
        if (condition != SEQ_DISABLED) {
          in_pAttribsObj.eResourceCondition = set_state ? SEQ_ENABLED :
            SEQ_DISNEXT;
        } else {
          in_pAttribsObj.eResourceCondition = condition;
        }

        sError = SetResourceAttributesSequenceOut((E_ModuleResource) (37),
          &in_pAttribsObj, BEHAVIOUR_MUX_EST_SEQ);
      } else {
        sError = ERROR_BAD_ATTRIBUTE_DATA;
      }

      if FAILED_OR_WARNING(sError) {
        sErrorResult = sError;
      }

      idx ++;
      if (idx >= 3) {
        idx = 0;
      }
    }

    UNDISABLE();

    {
      extern uint8_T muxpsp_set_MuxPSP2;
      if (SUCCESS(sErrorResult))
        muxpsp_set_MuxPSP2 = 0;
      else
        muxpsp_set_MuxPSP2 = (uint8_T) GetErrorCode(sErrorResult);
    }
  }
}

/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
   :: MuxPSP2_MuxPSP_Create()
   :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
/* Create MuxPSP resource
 */
void MuxPSP2_MuxPSP_Create(void)
{
  S_MuxESTSeqCreateAttributes CreateESTObj;
  S_SeqOutPulseTiming s_ESTTimingObj;
  NativeError_S sError;
  NativeVar_U i;
  CreateESTObj.uValidAttributesMask = USE_SEQ_GRANULARITY | USE_SEQ_CTRLMODE;
  CreateESTObj.eGranularity = FINE_GRANULARITY;
  CreateESTObj.eCtrlMode = DURATION_CTRL;

  /* Required attributes */
  CreateESTObj.u1NumPulsesToCreate = 3;
  memcpy(CreateESTObj.s2PulseOffsetDegATDC1Arr, MuxPSP2_PulseOffsetDegATDC1Arr,
         sizeof(CreateESTObj.s2PulseOffsetDegATDC1Arr));
  sError = CreateResourceBEHAVIOUR_MUX_EST_SEQ((E_ModuleResource) (37),
    &CreateESTObj);

  {
    extern uint8_T muxpsp_create_MuxPSP2;
    if (SUCCESS(sError))
      muxpsp_create_MuxPSP2 = 0;
    else
      muxpsp_create_MuxPSP2 = (uint8_T) GetErrorCode(sError);
  }

  if (SUCCESS(sError)) {
    uint8_T state = 0;
    int16_T s2StartAngle = 0;
    uint32_T u4Duration = 0;
    uint32_T min_duration = 200;
    MuxPSP2_MuxPSP_HardStart_SoftDuration_Set(&state, 1, &s2StartAngle, 1,
      &u4Duration, 1, &min_duration);
  }
}

/* We need to keep the pulse offsets in order to enable in order (global const) */
int16_T const MuxPSP3_PulseOffsetDegATDC1Arr[8] = { 2880, 2880, 2880, 0, 0, 0, 0,
  0 };

/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
   :: MuxPSP3_MuxPSP_HardStart_SoftDuration_Set
   :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
/* Manages:
   - updating of timings
   - disabling if pulses can't be scheduled
   - enabling pulses in the correct order
 */
void MuxPSP3_MuxPSP_HardStart_SoftDuration_Set(uint8_T const state[], boolean_T
  use_state_min, int16_T const start_angle[], boolean_T use_start_angle_min,
  uint32_T const duration[], boolean_T use_dur_min, uint32_T const* min_dur)
{
  S_SeqOutAttributes in_pAttribsObj;
  NativeError_S sError;
  NativeError_S sErrorResult = SUCCESS_OK;
  E_SeqOutCond condition;
  int idx, count;
  uint8_T set_state;
  in_pAttribsObj.uValidAttributesMask = USE_SEQ_PULSE_COND | USE_SEQ_TIMING;
  if (min_dur) {
    in_pAttribsObj.uValidAttributesMask |= USE_SEQ_MIN_DURATION;
    in_pAttribsObj.uMinPulseDurationInMicroSecs = *min_dur;
  }

  in_pAttribsObj.TimingObj.eCtrlMode = DURATION_CTRL;

  /* Ensure pulses are disabled if pulses can't be scheduled */
  /* And Enabled in the correct order if pulses can be scheduled */
  {
    /* Use SEQ_DISABLED if encoder is not sync-ed */
    {
      extern uint8_T motohawk_encoder_status;
      condition = SEQ_DISABLED;
      if (motohawk_encoder_status >= (uint8_T) ENC_SYNC_CAM_UNKNOWN) {
        condition = SEQ_DISNEXT;
      }
    }

    idx = 0;
    if (condition != SEQ_DISABLED) {
      int32_T next_index = 0;
      int16_T max_angle_diff = 0;
      int16_T angle_diff;
      int16_T current_angle;
      int16_T desired_angle;
      int16_T cylinder_angle;
      current_angle = 720*16 - GetEncoderEngAngle();
      for (idx=0; idx < 3; idx++) {
        desired_angle = (use_start_angle_min) ? start_angle[0] : start_angle[idx];
        cylinder_angle = MuxPSP3_PulseOffsetDegATDC1Arr[idx] - desired_angle;
        angle_diff = current_angle - cylinder_angle;
        while (angle_diff < 0) {
          angle_diff += 720*16;
        }

        while (angle_diff >= 720*16) {
          angle_diff -= 720*16;
        }

        if (angle_diff > max_angle_diff) {
          max_angle_diff = angle_diff;
          next_index = idx;
        }
      }

      idx = next_index;
    }

    DISABLE();
    for (count=0; count < 3; count++) {
      boolean_T bValid = 1;

      /* Determine what the new state should be for THIS pulse */
      set_state = (use_state_min) ? state[0] : state[idx];
      bValid = (idx < MAX_MUX_PULSES_SUPPORTED);
      if (bValid) {
        in_pAttribsObj.u1AffectedPulse = idx;
        in_pAttribsObj.TimingObj.s2StartAngle = (use_start_angle_min) ?
          start_angle[0] : start_angle[idx];
        in_pAttribsObj.TimingObj.u4Duration = (use_dur_min) ? duration[0] :
          duration[idx];
        if (condition != SEQ_DISABLED) {
          in_pAttribsObj.eResourceCondition = set_state ? SEQ_ENABLED :
            SEQ_DISNEXT;
        } else {
          in_pAttribsObj.eResourceCondition = condition;
        }

        sError = SetResourceAttributesSequenceOut((E_ModuleResource) (42),
          &in_pAttribsObj, BEHAVIOUR_MUX_EST_SEQ);
      } else {
        sError = ERROR_BAD_ATTRIBUTE_DATA;
      }

      if FAILED_OR_WARNING(sError) {
        sErrorResult = sError;
      }

      idx ++;
      if (idx >= 3) {
        idx = 0;
      }
    }

    UNDISABLE();

    {
      extern uint8_T muxpsp_set_MuxPSP3;
      if (SUCCESS(sErrorResult))
        muxpsp_set_MuxPSP3 = 0;
      else
        muxpsp_set_MuxPSP3 = (uint8_T) GetErrorCode(sErrorResult);
    }
  }
}

/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
   :: MuxPSP3_MuxPSP_Create()
   :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
/* Create MuxPSP resource
 */
void MuxPSP3_MuxPSP_Create(void)
{
  S_MuxESTSeqCreateAttributes CreateESTObj;
  S_SeqOutPulseTiming s_ESTTimingObj;
  NativeError_S sError;
  NativeVar_U i;
  CreateESTObj.uValidAttributesMask = USE_SEQ_GRANULARITY | USE_SEQ_CTRLMODE;
  CreateESTObj.eGranularity = COARSE_GRANULARITY;
  CreateESTObj.eCtrlMode = DURATION_CTRL;

  /* Required attributes */
  CreateESTObj.u1NumPulsesToCreate = 3;
  memcpy(CreateESTObj.s2PulseOffsetDegATDC1Arr, MuxPSP3_PulseOffsetDegATDC1Arr,
         sizeof(CreateESTObj.s2PulseOffsetDegATDC1Arr));
  sError = CreateResourceBEHAVIOUR_MUX_EST_SEQ((E_ModuleResource) (42),
    &CreateESTObj);

  {
    extern uint8_T muxpsp_create_MuxPSP3;
    if (SUCCESS(sError))
      muxpsp_create_MuxPSP3 = 0;
    else
      muxpsp_create_MuxPSP3 = (uint8_T) GetErrorCode(sError);
  }

  if (SUCCESS(sError)) {
    uint8_T state = 0;
    int16_T s2StartAngle = 0;
    uint32_T u4Duration = 0;
    uint32_T min_duration = 200;
    MuxPSP3_MuxPSP_HardStart_SoftDuration_Set(&state, 1, &s2StartAngle, 1,
      &u4Duration, 1, &min_duration);
  }
}

/* We need to keep the pulse offsets in order to enable in order (global const) */
int16_T const MuxPSP4_PulseOffsetDegATDC1Arr[8] = { 5760, 5760, 5760, 0, 0, 0, 0,
  0 };

/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
   :: MuxPSP4_MuxPSP_HardStart_SoftDuration_Set
   :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
/* Manages:
   - updating of timings
   - disabling if pulses can't be scheduled
   - enabling pulses in the correct order
 */
void MuxPSP4_MuxPSP_HardStart_SoftDuration_Set(uint8_T const state[], boolean_T
  use_state_min, int16_T const start_angle[], boolean_T use_start_angle_min,
  uint32_T const duration[], boolean_T use_dur_min, uint32_T const* min_dur)
{
  S_SeqOutAttributes in_pAttribsObj;
  NativeError_S sError;
  NativeError_S sErrorResult = SUCCESS_OK;
  E_SeqOutCond condition;
  int idx, count;
  uint8_T set_state;
  in_pAttribsObj.uValidAttributesMask = USE_SEQ_PULSE_COND | USE_SEQ_TIMING;
  if (min_dur) {
    in_pAttribsObj.uValidAttributesMask |= USE_SEQ_MIN_DURATION;
    in_pAttribsObj.uMinPulseDurationInMicroSecs = *min_dur;
  }

  in_pAttribsObj.TimingObj.eCtrlMode = DURATION_CTRL;

  /* Ensure pulses are disabled if pulses can't be scheduled */
  /* And Enabled in the correct order if pulses can be scheduled */
  {
    /* Use SEQ_DISABLED if encoder is not sync-ed */
    {
      extern uint8_T motohawk_encoder_status;
      condition = SEQ_DISABLED;
      if (motohawk_encoder_status >= (uint8_T) ENC_SYNC_CAM_UNKNOWN) {
        condition = SEQ_DISNEXT;
      }
    }

    idx = 0;
    if (condition != SEQ_DISABLED) {
      int32_T next_index = 0;
      int16_T max_angle_diff = 0;
      int16_T angle_diff;
      int16_T current_angle;
      int16_T desired_angle;
      int16_T cylinder_angle;
      current_angle = 720*16 - GetEncoderEngAngle();
      for (idx=0; idx < 3; idx++) {
        desired_angle = (use_start_angle_min) ? start_angle[0] : start_angle[idx];
        cylinder_angle = MuxPSP4_PulseOffsetDegATDC1Arr[idx] - desired_angle;
        angle_diff = current_angle - cylinder_angle;
        while (angle_diff < 0) {
          angle_diff += 720*16;
        }

        while (angle_diff >= 720*16) {
          angle_diff -= 720*16;
        }

        if (angle_diff > max_angle_diff) {
          max_angle_diff = angle_diff;
          next_index = idx;
        }
      }

      idx = next_index;
    }

    DISABLE();
    for (count=0; count < 3; count++) {
      boolean_T bValid = 1;

      /* Determine what the new state should be for THIS pulse */
      set_state = (use_state_min) ? state[0] : state[idx];
      bValid = (idx < MAX_MUX_PULSES_SUPPORTED);
      if (bValid) {
        in_pAttribsObj.u1AffectedPulse = idx;
        in_pAttribsObj.TimingObj.s2StartAngle = (use_start_angle_min) ?
          start_angle[0] : start_angle[idx];
        in_pAttribsObj.TimingObj.u4Duration = (use_dur_min) ? duration[0] :
          duration[idx];
        if (condition != SEQ_DISABLED) {
          in_pAttribsObj.eResourceCondition = set_state ? SEQ_ENABLED :
            SEQ_DISNEXT;
        } else {
          in_pAttribsObj.eResourceCondition = condition;
        }

        sError = SetResourceAttributesSequenceOut((E_ModuleResource) (43),
          &in_pAttribsObj, BEHAVIOUR_MUX_EST_SEQ);
      } else {
        sError = ERROR_BAD_ATTRIBUTE_DATA;
      }

      if FAILED_OR_WARNING(sError) {
        sErrorResult = sError;
      }

      idx ++;
      if (idx >= 3) {
        idx = 0;
      }
    }

    UNDISABLE();

    {
      extern uint8_T muxpsp_set_MuxPSP4;
      if (SUCCESS(sErrorResult))
        muxpsp_set_MuxPSP4 = 0;
      else
        muxpsp_set_MuxPSP4 = (uint8_T) GetErrorCode(sErrorResult);
    }
  }
}

/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
   :: MuxPSP4_MuxPSP_Create()
   :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
/* Create MuxPSP resource
 */
void MuxPSP4_MuxPSP_Create(void)
{
  S_MuxESTSeqCreateAttributes CreateESTObj;
  S_SeqOutPulseTiming s_ESTTimingObj;
  NativeError_S sError;
  NativeVar_U i;
  CreateESTObj.uValidAttributesMask = USE_SEQ_GRANULARITY | USE_SEQ_CTRLMODE;
  CreateESTObj.eGranularity = FINE_GRANULARITY;
  CreateESTObj.eCtrlMode = DURATION_CTRL;

  /* Required attributes */
  CreateESTObj.u1NumPulsesToCreate = 3;
  memcpy(CreateESTObj.s2PulseOffsetDegATDC1Arr, MuxPSP4_PulseOffsetDegATDC1Arr,
         sizeof(CreateESTObj.s2PulseOffsetDegATDC1Arr));
  sError = CreateResourceBEHAVIOUR_MUX_EST_SEQ((E_ModuleResource) (43),
    &CreateESTObj);

  {
    extern uint8_T muxpsp_create_MuxPSP4;
    if (SUCCESS(sError))
      muxpsp_create_MuxPSP4 = 0;
    else
      muxpsp_create_MuxPSP4 = (uint8_T) GetErrorCode(sError);
  }

  if (SUCCESS(sError)) {
    uint8_T state = 0;
    int16_T s2StartAngle = 0;
    uint32_T u4Duration = 0;
    uint32_T min_duration = 200;
    MuxPSP4_MuxPSP_HardStart_SoftDuration_Set(&state, 1, &s2StartAngle, 1,
      &u4Duration, 1, &min_duration);
  }
}
