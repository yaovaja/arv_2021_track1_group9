/*
 * Trigger_FGND_RTI_PERIODIC_2509p0004.c
 *
 * Code generation for model "CRVLAB_ECU_Project.mdl".
 *
 * Model version              : 1.812
 * Simulink Coder version : 8.0 (R2011a) 09-Mar-2011
 * C source code generated on : Tue Apr 10 17:20:14 2012
 *
 * Target selection: motohawk_ert_rtw.tlc
 * Embedded hardware selection: Specified
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "CRVLAB_ECU_Project.h"
#include "CRVLAB_ECU_Project_private.h"

void Trigger_FGND_RTI_PERIODIC_2509p0004(void)
{
  {
    CRVLAB_ECU_Project_Model();
  }
}
