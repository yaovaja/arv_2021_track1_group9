/*
 * File: CRVLAB_ECU_Project.h
 *
 * Code generated for Simulink model 'CRVLAB_ECU_Project'.
 *
 * Model version                  : 1.812
 * Simulink Coder version         : 8.0 (R2011a) 09-Mar-2011
 * TLC version                    : 8.0 (Feb  3 2011)
 * C/C++ source code generated on : Tue Apr 10 17:20:14 2012
 *
 * Target selection: motohawk_ert_rtw.tlc
 * Embedded hardware selection: Specified
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_CRVLAB_ECU_Project_h_
#define RTW_HEADER_CRVLAB_ECU_Project_h_
#ifndef CRVLAB_ECU_Project_COMMON_INCLUDES_
# define CRVLAB_ECU_Project_COMMON_INCLUDES_
#include <stddef.h>
#include <math.h>
#include <string.h>
#include "rtwtypes.h"
#include "Application.h"
#include "rt_nonfinite.h"
#endif                                 /* CRVLAB_ECU_Project_COMMON_INCLUDES_ */

#include "CRVLAB_ECU_Project_types.h"

/* Child system includes */
#include "Model.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((void*) 0)
#endif

/* Block signals (auto storage) */
typedef struct {
  real_T s11_DataTypeConversion1;      /* '<S11>/Data Type Conversion1' */
  real_T s12_DataTypeConversion1;      /* '<S12>/Data Type Conversion1' */
  real_T s15_DataTypeConversion1;      /* '<S15>/Data Type Conversion1' */
  real_T s15_motohawk_interpolation_1d;/* '<S15>/motohawk_interpolation_1d' */
  real_T s16_DataTypeConversion;       /* '<S16>/Data Type Conversion' */
  real_T s17_Sum1;                     /* '<S17>/Sum1' */
  real_T s18_DataTypeConversion1;      /* '<S18>/Data Type Conversion1' */
  real_T s61_Product2;                 /* '<S61>/Product2' */
  real_T s61_Product3;                 /* '<S61>/Product3' */
  real_T s61_Product4;                 /* '<S61>/Product4' */
  real_T s81_Merge;                    /* '<S81>/Merge' */
  real_T s62_Product2;                 /* '<S62>/Product2' */
  real_T s62_Product3;                 /* '<S62>/Product3' */
  real_T s62_Product4;                 /* '<S62>/Product4' */
  real_T s87_Merge;                    /* '<S87>/Merge' */
  real_T s59_Product2;                 /* '<S59>/Product2' */
  real_T s59_Product3;                 /* '<S59>/Product3' */
  real_T s59_Product4;                 /* '<S59>/Product4' */
  real_T s67_Merge;                    /* '<S67>/Merge' */
  real_T s60_Product2;                 /* '<S60>/Product2' */
  real_T s60_Product3;                 /* '<S60>/Product3' */
  real_T s60_Product4;                 /* '<S60>/Product4' */
  real_T s74_Merge;                    /* '<S74>/Merge' */
  real_T s63_Product2;                 /* '<S63>/Product2' */
  real_T s63_Product3;                 /* '<S63>/Product3' */
  real_T s63_Product4;                 /* '<S63>/Product4' */
  real_T s93_Merge;                    /* '<S93>/Merge' */
  real_T s125_Merge;                   /* '<S125>/Merge' */
  real_T s122_Merge;                   /* '<S122>/Merge' */
  real_T s126_Merge;                   /* '<S126>/Merge' */
  real_T s123_Merge;                   /* '<S123>/Merge' */
  real_T s127_Merge;                   /* '<S127>/Merge' */
  real_T s124_Merge;                   /* '<S124>/Merge' */
  real_T s26_State;                    /* '<S3>/Chart1' */
  int16_T s101_DataTypeConversion4;    /* '<S101>/Data Type Conversion4' */
  uint16_T s8_motohawk_ain1;           /* '<S8>/motohawk_ain1' */
  uint16_T s14_motohawk_ain14;         /* '<S14>/motohawk_ain14' */
  uint16_T s17_motohawk_ain1;          /* '<S17>/motohawk_ain1' */
  boolean_T s96_RelationalOperator5;   /* '<S96>/Relational Operator5' */
  boolean_T s114_LogicalOperator;      /* '<S114>/Logical Operator' */
  boolean_T s115_LogicalOperator;      /* '<S115>/Logical Operator' */
  boolean_T s116_LogicalOperator;      /* '<S116>/Logical Operator' */
  boolean_T s117_LogicalOperator;      /* '<S117>/Logical Operator' */
  boolean_T s141_LogicalOperator1;     /* '<S141>/Logical Operator1' */
  boolean_T s144_Merge;                /* '<S144>/Merge' */
  boolean_T s101_RelationalOperator;   /* '<S101>/Relational Operator' */
} BlockIO_CRVLAB_ECU_Project;

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  real_T s24_UnitDelay_DSTATE;         /* '<S24>/Unit Delay' */
  real_T s25_UnitDelay1_DSTATE;        /* '<S25>/Unit Delay1' */
  real_T s25_UnitDelay_DSTATE;         /* '<S25>/Unit Delay' */
  real_T s61_UnitDelay1_DSTATE;        /* '<S61>/Unit Delay1' */
  real_T s61_UnitDelay_DSTATE;         /* '<S61>/Unit Delay' */
  real_T s62_UnitDelay1_DSTATE;        /* '<S62>/Unit Delay1' */
  real_T s62_UnitDelay_DSTATE;         /* '<S62>/Unit Delay' */
  real_T s59_UnitDelay1_DSTATE;        /* '<S59>/Unit Delay1' */
  real_T s59_UnitDelay_DSTATE;         /* '<S59>/Unit Delay' */
  real_T s60_UnitDelay1_DSTATE;        /* '<S60>/Unit Delay1' */
  real_T s60_UnitDelay_DSTATE;         /* '<S60>/Unit Delay' */
  real_T s63_UnitDelay1_DSTATE;        /* '<S63>/Unit Delay1' */
  real_T s63_UnitDelay_DSTATE;         /* '<S63>/Unit Delay' */
  uint32_T s23_motohawk_delta_time_DWORK1;/* '<S23>/motohawk_delta_time' */
  uint32_T s61_motohawk_delta_time_DWORK1;/* '<S61>/motohawk_delta_time' */
  uint32_T s62_motohawk_delta_time_DWORK1;/* '<S62>/motohawk_delta_time' */
  uint32_T s59_motohawk_delta_time_DWORK1;/* '<S59>/motohawk_delta_time' */
  uint32_T s60_motohawk_delta_time_DWORK1;/* '<S60>/motohawk_delta_time' */
  uint32_T s63_motohawk_delta_time_DWORK1;/* '<S63>/motohawk_delta_time' */
  uint32_T s106_motohawk_delta_time_DWORK1;/* '<S106>/motohawk_delta_time' */
  int_T s96_motohawk_injector_IWORK;   /* '<S96>/motohawk_injector' */
  int_T s96_motohawk_injector1_IWORK;  /* '<S96>/motohawk_injector1' */
  int_T s96_motohawk_injector2_IWORK;  /* '<S96>/motohawk_injector2' */
  int_T s96_motohawk_injector3_IWORK;  /* '<S96>/motohawk_injector3' */
  uint16_T s96_motohawk_injector_DWORK2;/* '<S96>/motohawk_injector' */
  uint16_T s96_motohawk_injector1_DWORK2;/* '<S96>/motohawk_injector1' */
  uint16_T s96_motohawk_injector2_DWORK2;/* '<S96>/motohawk_injector2' */
  uint16_T s96_motohawk_injector3_DWORK2;/* '<S96>/motohawk_injector3' */
  uint8_T s26_is_active_c3_CRVLAB_ECU_Project;/* '<S3>/Chart1' */
  uint8_T s26_is_c3_CRVLAB_ECU_Project;/* '<S3>/Chart1' */
  uint8_T s26_is_Run;                  /* '<S3>/Chart1' */
  uint8_T s26_is_Cut;                  /* '<S3>/Chart1' */
  boolean_T s96_motohawk_injector_DWORK1;/* '<S96>/motohawk_injector' */
  boolean_T s96_motohawk_injector1_DWORK1;/* '<S96>/motohawk_injector1' */
  boolean_T s96_motohawk_injector2_DWORK1;/* '<S96>/motohawk_injector2' */
  boolean_T s96_motohawk_injector3_DWORK1;/* '<S96>/motohawk_injector3' */
} D_Work_CRVLAB_ECU_Project;

/* Real-time Model Data Structure */
struct RT_MODEL_CRVLAB_ECU_Project {
  const char_T *errorStatus;
};

/* Block signals (auto storage) */
extern BlockIO_CRVLAB_ECU_Project CRVLAB_ECU_Project_B;

/* Block states (auto storage) */
extern D_Work_CRVLAB_ECU_Project CRVLAB_ECU_Project_DWork;

/* Model entry point functions */
extern void CRVLAB_ECU_Project_initialize(boolean_T firstTime);
extern void CRVLAB_ECU_Project_step(void);
extern void CRVLAB_ECU_Project_terminate(void);

/* Real-time Model object */
extern struct RT_MODEL_CRVLAB_ECU_Project *const CRVLAB_ECU_Project_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : CRVLAB_ECU_Project
 * '<S1>'   : CRVLAB_ECU_Project/Model
 * '<S2>'   : CRVLAB_ECU_Project/Model/1 Sensor
 * '<S3>'   : CRVLAB_ECU_Project/Model/2 Stateflow Chart
 * '<S4>'   : CRVLAB_ECU_Project/Model/3 Set-point Generator
 * '<S5>'   : CRVLAB_ECU_Project/Model/4 Control Algorithm
 * '<S6>'   : CRVLAB_ECU_Project/Model/5 Actuator
 * '<S7>'   : CRVLAB_ECU_Project/Model/6 Safety
 * '<S8>'   : CRVLAB_ECU_Project/Model/1 Sensor/AC_On
 * '<S9>'   : CRVLAB_ECU_Project/Model/1 Sensor/CNGP_kPa
 * '<S10>'  : CRVLAB_ECU_Project/Model/1 Sensor/DriverInputs
 * '<S11>'  : CRVLAB_ECU_Project/Model/1 Sensor/ECT_C
 * '<S12>'  : CRVLAB_ECU_Project/Model/1 Sensor/EGR_Pct
 * '<S13>'  : CRVLAB_ECU_Project/Model/1 Sensor/EngineSpeed_rpm
 * '<S14>'  : CRVLAB_ECU_Project/Model/1 Sensor/KeySw_Volt
 * '<S15>'  : CRVLAB_ECU_Project/Model/1 Sensor/MAF_g_s
 * '<S16>'  : CRVLAB_ECU_Project/Model/1 Sensor/Pedal_Pct
 * '<S17>'  : CRVLAB_ECU_Project/Model/1 Sensor/RailP_MPa
 * '<S18>'  : CRVLAB_ECU_Project/Model/1 Sensor/TPS_Pct
 * '<S19>'  : CRVLAB_ECU_Project/Model/1 Sensor/AC_On/Compare To Constant
 * '<S20>'  : CRVLAB_ECU_Project/Model/1 Sensor/DriverInputs/Compare To Constant1
 * '<S21>'  : CRVLAB_ECU_Project/Model/1 Sensor/DriverInputs/Compare To Constant2
 * '<S22>'  : CRVLAB_ECU_Project/Model/1 Sensor/DriverInputs/Compare To Constant3
 * '<S23>'  : CRVLAB_ECU_Project/Model/1 Sensor/MAF_g_s/First Order Low Pass
 * '<S24>'  : CRVLAB_ECU_Project/Model/1 Sensor/MAF_g_s/First Order Low Pass/First Order Low Pass
 * '<S25>'  : CRVLAB_ECU_Project/Model/1 Sensor/RailP_MPa/High-Pass Filter Withit C
 * '<S26>'  : CRVLAB_ECU_Project/Model/2 Stateflow Chart/Chart1
 * '<S27>'  : CRVLAB_ECU_Project/Model/3 Set-point Generator/CNGSOI_degBTDC CNGFueling_ms
 * '<S28>'  : CRVLAB_ECU_Project/Model/3 Set-point Generator/EGRSP_Pct
 * '<S29>'  : CRVLAB_ECU_Project/Model/3 Set-point Generator/IdleSpeedSP_rpm
 * '<S30>'  : CRVLAB_ECU_Project/Model/3 Set-point Generator/MAFSP_g_s
 * '<S31>'  : CRVLAB_ECU_Project/Model/3 Set-point Generator/MainFueling_ms
 * '<S32>'  : CRVLAB_ECU_Project/Model/3 Set-point Generator/MainSOI_degBTDC
 * '<S33>'  : CRVLAB_ECU_Project/Model/3 Set-point Generator/PilotFueling_ms
 * '<S34>'  : CRVLAB_ECU_Project/Model/3 Set-point Generator/PilotSOI_degBTDC
 * '<S35>'  : CRVLAB_ECU_Project/Model/3 Set-point Generator/PreFueling_ms
 * '<S36>'  : CRVLAB_ECU_Project/Model/3 Set-point Generator/PreSOI_degBTDC
 * '<S37>'  : CRVLAB_ECU_Project/Model/3 Set-point Generator/RailPSP_MPa
 * '<S38>'  : CRVLAB_ECU_Project/Model/3 Set-point Generator/TPSSP_Pct
 * '<S39>'  : CRVLAB_ECU_Project/Model/3 Set-point Generator/CNGSOI_degBTDC CNGFueling_ms/Subsystem
 * '<S40>'  : CRVLAB_ECU_Project/Model/3 Set-point Generator/CNGSOI_degBTDC CNGFueling_ms/Subsystem1
 * '<S41>'  : CRVLAB_ECU_Project/Model/3 Set-point Generator/EGRSP_Pct/motohawk_override_abs7
 * '<S42>'  : CRVLAB_ECU_Project/Model/3 Set-point Generator/EGRSP_Pct/motohawk_override_abs7/NewValue
 * '<S43>'  : CRVLAB_ECU_Project/Model/3 Set-point Generator/EGRSP_Pct/motohawk_override_abs7/OldValue
 * '<S44>'  : CRVLAB_ECU_Project/Model/3 Set-point Generator/MAFSP_g_s/motohawk_override_abs7
 * '<S45>'  : CRVLAB_ECU_Project/Model/3 Set-point Generator/MAFSP_g_s/motohawk_override_abs7/NewValue
 * '<S46>'  : CRVLAB_ECU_Project/Model/3 Set-point Generator/MAFSP_g_s/motohawk_override_abs7/OldValue
 * '<S47>'  : CRVLAB_ECU_Project/Model/3 Set-point Generator/MainFueling_ms/Subsystem
 * '<S48>'  : CRVLAB_ECU_Project/Model/3 Set-point Generator/MainSOI_degBTDC/Subsystem1
 * '<S49>'  : CRVLAB_ECU_Project/Model/3 Set-point Generator/PilotFueling_ms/Subsystem
 * '<S50>'  : CRVLAB_ECU_Project/Model/3 Set-point Generator/PilotSOI_degBTDC/Subsystem1
 * '<S51>'  : CRVLAB_ECU_Project/Model/3 Set-point Generator/PreFueling_ms/Subsystem
 * '<S52>'  : CRVLAB_ECU_Project/Model/3 Set-point Generator/PreSOI_degBTDC/Subsystem1
 * '<S53>'  : CRVLAB_ECU_Project/Model/3 Set-point Generator/RailPSP_MPa/motohawk_override_abs7
 * '<S54>'  : CRVLAB_ECU_Project/Model/3 Set-point Generator/RailPSP_MPa/motohawk_override_abs7/NewValue
 * '<S55>'  : CRVLAB_ECU_Project/Model/3 Set-point Generator/RailPSP_MPa/motohawk_override_abs7/OldValue
 * '<S56>'  : CRVLAB_ECU_Project/Model/3 Set-point Generator/TPSSP_Pct/motohawk_override_abs7
 * '<S57>'  : CRVLAB_ECU_Project/Model/3 Set-point Generator/TPSSP_Pct/motohawk_override_abs7/NewValue
 * '<S58>'  : CRVLAB_ECU_Project/Model/3 Set-point Generator/TPSSP_Pct/motohawk_override_abs7/OldValue
 * '<S59>'  : CRVLAB_ECU_Project/Model/4 Control Algorithm/EGR Controller
 * '<S60>'  : CRVLAB_ECU_Project/Model/4 Control Algorithm/Idle Speed Controller
 * '<S61>'  : CRVLAB_ECU_Project/Model/4 Control Algorithm/RailP Controller
 * '<S62>'  : CRVLAB_ECU_Project/Model/4 Control Algorithm/Throttle Controller
 * '<S63>'  : CRVLAB_ECU_Project/Model/4 Control Algorithm/VNT Controller
 * '<S64>'  : CRVLAB_ECU_Project/Model/4 Control Algorithm/EGR Controller/Compare To Constant
 * '<S65>'  : CRVLAB_ECU_Project/Model/4 Control Algorithm/EGR Controller/Saturation1
 * '<S66>'  : CRVLAB_ECU_Project/Model/4 Control Algorithm/EGR Controller/Saturation2
 * '<S67>'  : CRVLAB_ECU_Project/Model/4 Control Algorithm/EGR Controller/motohawk_override_abs
 * '<S68>'  : CRVLAB_ECU_Project/Model/4 Control Algorithm/EGR Controller/motohawk_override_abs/NewValue
 * '<S69>'  : CRVLAB_ECU_Project/Model/4 Control Algorithm/EGR Controller/motohawk_override_abs/OldValue
 * '<S70>'  : CRVLAB_ECU_Project/Model/4 Control Algorithm/Idle Speed Controller/Compare To Constant
 * '<S71>'  : CRVLAB_ECU_Project/Model/4 Control Algorithm/Idle Speed Controller/Compare To Constant1
 * '<S72>'  : CRVLAB_ECU_Project/Model/4 Control Algorithm/Idle Speed Controller/Saturation1
 * '<S73>'  : CRVLAB_ECU_Project/Model/4 Control Algorithm/Idle Speed Controller/Saturation2
 * '<S74>'  : CRVLAB_ECU_Project/Model/4 Control Algorithm/Idle Speed Controller/motohawk_override_abs
 * '<S75>'  : CRVLAB_ECU_Project/Model/4 Control Algorithm/Idle Speed Controller/motohawk_override_abs/NewValue
 * '<S76>'  : CRVLAB_ECU_Project/Model/4 Control Algorithm/Idle Speed Controller/motohawk_override_abs/OldValue
 * '<S77>'  : CRVLAB_ECU_Project/Model/4 Control Algorithm/RailP Controller/Compare To Constant
 * '<S78>'  : CRVLAB_ECU_Project/Model/4 Control Algorithm/RailP Controller/Compare To Constant1
 * '<S79>'  : CRVLAB_ECU_Project/Model/4 Control Algorithm/RailP Controller/Saturation1
 * '<S80>'  : CRVLAB_ECU_Project/Model/4 Control Algorithm/RailP Controller/Saturation2
 * '<S81>'  : CRVLAB_ECU_Project/Model/4 Control Algorithm/RailP Controller/motohawk_override_abs
 * '<S82>'  : CRVLAB_ECU_Project/Model/4 Control Algorithm/RailP Controller/motohawk_override_abs/NewValue
 * '<S83>'  : CRVLAB_ECU_Project/Model/4 Control Algorithm/RailP Controller/motohawk_override_abs/OldValue
 * '<S84>'  : CRVLAB_ECU_Project/Model/4 Control Algorithm/Throttle Controller/Compare To Constant
 * '<S85>'  : CRVLAB_ECU_Project/Model/4 Control Algorithm/Throttle Controller/Saturation1
 * '<S86>'  : CRVLAB_ECU_Project/Model/4 Control Algorithm/Throttle Controller/Saturation2
 * '<S87>'  : CRVLAB_ECU_Project/Model/4 Control Algorithm/Throttle Controller/motohawk_override_abs
 * '<S88>'  : CRVLAB_ECU_Project/Model/4 Control Algorithm/Throttle Controller/motohawk_override_abs/NewValue
 * '<S89>'  : CRVLAB_ECU_Project/Model/4 Control Algorithm/Throttle Controller/motohawk_override_abs/OldValue
 * '<S90>'  : CRVLAB_ECU_Project/Model/4 Control Algorithm/VNT Controller/Compare To Constant
 * '<S91>'  : CRVLAB_ECU_Project/Model/4 Control Algorithm/VNT Controller/Saturation1
 * '<S92>'  : CRVLAB_ECU_Project/Model/4 Control Algorithm/VNT Controller/Saturation2
 * '<S93>'  : CRVLAB_ECU_Project/Model/4 Control Algorithm/VNT Controller/motohawk_override_abs
 * '<S94>'  : CRVLAB_ECU_Project/Model/4 Control Algorithm/VNT Controller/motohawk_override_abs/NewValue
 * '<S95>'  : CRVLAB_ECU_Project/Model/4 Control Algorithm/VNT Controller/motohawk_override_abs/OldValue
 * '<S96>'  : CRVLAB_ECU_Project/Model/5 Actuator/CNG Injectors
 * '<S97>'  : CRVLAB_ECU_Project/Model/5 Actuator/CNG Level
 * '<S98>'  : CRVLAB_ECU_Project/Model/5 Actuator/Common Rail
 * '<S99>'  : CRVLAB_ECU_Project/Model/5 Actuator/Diesel Injectors
 * '<S100>' : CRVLAB_ECU_Project/Model/5 Actuator/EGR
 * '<S101>' : CRVLAB_ECU_Project/Model/5 Actuator/Relays
 * '<S102>' : CRVLAB_ECU_Project/Model/5 Actuator/SpeedoMeter
 * '<S103>' : CRVLAB_ECU_Project/Model/5 Actuator/THWO
 * '<S104>' : CRVLAB_ECU_Project/Model/5 Actuator/Throttle
 * '<S105>' : CRVLAB_ECU_Project/Model/5 Actuator/VNTO
 * '<S106>' : CRVLAB_ECU_Project/Model/5 Actuator/CNG Injectors/Time Since Enabled (With Input)3
 * '<S107>' : CRVLAB_ECU_Project/Model/5 Actuator/CNG Injectors/motohawk_override_abs2
 * '<S108>' : CRVLAB_ECU_Project/Model/5 Actuator/CNG Injectors/motohawk_override_abs3
 * '<S109>' : CRVLAB_ECU_Project/Model/5 Actuator/CNG Injectors/motohawk_override_abs2/NewValue
 * '<S110>' : CRVLAB_ECU_Project/Model/5 Actuator/CNG Injectors/motohawk_override_abs2/OldValue
 * '<S111>' : CRVLAB_ECU_Project/Model/5 Actuator/CNG Injectors/motohawk_override_abs3/NewValue
 * '<S112>' : CRVLAB_ECU_Project/Model/5 Actuator/CNG Injectors/motohawk_override_abs3/OldValue
 * '<S113>' : CRVLAB_ECU_Project/Model/5 Actuator/Common Rail/Stall
 * '<S114>' : CRVLAB_ECU_Project/Model/5 Actuator/Diesel Injectors/3 Pulse (MuxPSP)1
 * '<S115>' : CRVLAB_ECU_Project/Model/5 Actuator/Diesel Injectors/3 Pulse (MuxPSP)2
 * '<S116>' : CRVLAB_ECU_Project/Model/5 Actuator/Diesel Injectors/3 Pulse (MuxPSP)3
 * '<S117>' : CRVLAB_ECU_Project/Model/5 Actuator/Diesel Injectors/3 Pulse (MuxPSP)4
 * '<S118>' : CRVLAB_ECU_Project/Model/5 Actuator/Diesel Injectors/Compare To Constant
 * '<S119>' : CRVLAB_ECU_Project/Model/5 Actuator/Diesel Injectors/Move SOI Far Away
 * '<S120>' : CRVLAB_ECU_Project/Model/5 Actuator/Diesel Injectors/Move SOI Far Away1
 * '<S121>' : CRVLAB_ECU_Project/Model/5 Actuator/Diesel Injectors/Move SOI Far Away2
 * '<S122>' : CRVLAB_ECU_Project/Model/5 Actuator/Diesel Injectors/motohawk_override_abs
 * '<S123>' : CRVLAB_ECU_Project/Model/5 Actuator/Diesel Injectors/motohawk_override_abs1
 * '<S124>' : CRVLAB_ECU_Project/Model/5 Actuator/Diesel Injectors/motohawk_override_abs10
 * '<S125>' : CRVLAB_ECU_Project/Model/5 Actuator/Diesel Injectors/motohawk_override_abs7
 * '<S126>' : CRVLAB_ECU_Project/Model/5 Actuator/Diesel Injectors/motohawk_override_abs8
 * '<S127>' : CRVLAB_ECU_Project/Model/5 Actuator/Diesel Injectors/motohawk_override_abs9
 * '<S128>' : CRVLAB_ECU_Project/Model/5 Actuator/Diesel Injectors/motohawk_override_abs/NewValue
 * '<S129>' : CRVLAB_ECU_Project/Model/5 Actuator/Diesel Injectors/motohawk_override_abs/OldValue
 * '<S130>' : CRVLAB_ECU_Project/Model/5 Actuator/Diesel Injectors/motohawk_override_abs1/NewValue
 * '<S131>' : CRVLAB_ECU_Project/Model/5 Actuator/Diesel Injectors/motohawk_override_abs1/OldValue
 * '<S132>' : CRVLAB_ECU_Project/Model/5 Actuator/Diesel Injectors/motohawk_override_abs10/NewValue
 * '<S133>' : CRVLAB_ECU_Project/Model/5 Actuator/Diesel Injectors/motohawk_override_abs10/OldValue
 * '<S134>' : CRVLAB_ECU_Project/Model/5 Actuator/Diesel Injectors/motohawk_override_abs7/NewValue
 * '<S135>' : CRVLAB_ECU_Project/Model/5 Actuator/Diesel Injectors/motohawk_override_abs7/OldValue
 * '<S136>' : CRVLAB_ECU_Project/Model/5 Actuator/Diesel Injectors/motohawk_override_abs8/NewValue
 * '<S137>' : CRVLAB_ECU_Project/Model/5 Actuator/Diesel Injectors/motohawk_override_abs8/OldValue
 * '<S138>' : CRVLAB_ECU_Project/Model/5 Actuator/Diesel Injectors/motohawk_override_abs9/NewValue
 * '<S139>' : CRVLAB_ECU_Project/Model/5 Actuator/Diesel Injectors/motohawk_override_abs9/OldValue
 * '<S140>' : CRVLAB_ECU_Project/Model/5 Actuator/Relays/Gas Relay Enabled1
 * '<S141>' : CRVLAB_ECU_Project/Model/5 Actuator/Relays/Glow Plug Enabled
 * '<S142>' : CRVLAB_ECU_Project/Model/5 Actuator/Relays/motohawk_override_abs
 * '<S143>' : CRVLAB_ECU_Project/Model/5 Actuator/Relays/motohawk_override_abs1
 * '<S144>' : CRVLAB_ECU_Project/Model/5 Actuator/Relays/motohawk_override_abs2
 * '<S145>' : CRVLAB_ECU_Project/Model/5 Actuator/Relays/motohawk_override_abs3
 * '<S146>' : CRVLAB_ECU_Project/Model/5 Actuator/Relays/Glow Plug Enabled/Cranking
 * '<S147>' : CRVLAB_ECU_Project/Model/5 Actuator/Relays/motohawk_override_abs/NewValue
 * '<S148>' : CRVLAB_ECU_Project/Model/5 Actuator/Relays/motohawk_override_abs/OldValue
 * '<S149>' : CRVLAB_ECU_Project/Model/5 Actuator/Relays/motohawk_override_abs1/NewValue
 * '<S150>' : CRVLAB_ECU_Project/Model/5 Actuator/Relays/motohawk_override_abs1/OldValue
 * '<S151>' : CRVLAB_ECU_Project/Model/5 Actuator/Relays/motohawk_override_abs2/NewValue
 * '<S152>' : CRVLAB_ECU_Project/Model/5 Actuator/Relays/motohawk_override_abs2/OldValue
 * '<S153>' : CRVLAB_ECU_Project/Model/5 Actuator/Relays/motohawk_override_abs3/NewValue
 * '<S154>' : CRVLAB_ECU_Project/Model/5 Actuator/Relays/motohawk_override_abs3/OldValue
 * '<S155>' : CRVLAB_ECU_Project/Model/5 Actuator/VNTO/motohawk_override_abs
 * '<S156>' : CRVLAB_ECU_Project/Model/5 Actuator/VNTO/motohawk_override_abs/NewValue
 * '<S157>' : CRVLAB_ECU_Project/Model/5 Actuator/VNTO/motohawk_override_abs/OldValue
 */
#endif                                 /* RTW_HEADER_CRVLAB_ECU_Project_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
