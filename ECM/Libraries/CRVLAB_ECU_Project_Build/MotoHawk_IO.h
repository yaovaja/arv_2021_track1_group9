/*
 * MotoHawk_IO.h
 *
 */

#ifndef MOTOHAWK_IO_H
#define MOTOHAWK_IO_H
#include "CommonInclude.h"

/* S-Function Block: <S8>/motohawk_ain1 Resource: AN8M */
NativeError_S AN8M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
NativeError_S AN8M_AnalogInput_Create(void);

/* S-Function Block: <S9>/motohawk_ain5 Resource: AN6M */
NativeError_S AN6M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
NativeError_S AN6M_AnalogInput_Create(void);

/* S-Function Block: <S10>/motohawk_ain1 Resource: AN11M */
NativeError_S AN11M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
NativeError_S AN11M_AnalogInput_Create(void);

/* S-Function Block: <S10>/motohawk_ain2 Resource: AN12M */
NativeError_S AN12M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
NativeError_S AN12M_AnalogInput_Create(void);

/* S-Function Block: <S10>/motohawk_ain3 Resource: AN13M */
NativeError_S AN13M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
NativeError_S AN13M_AnalogInput_Create(void);

/* S-Function Block: <S10>/motohawk_ain4 Resource: AN9M */
NativeError_S AN9M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
NativeError_S AN9M_AnalogInput_Create(void);

/* S-Function Block: <S11>/motohawk_ain Resource: AN5M */
NativeError_S AN5M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
NativeError_S AN5M_AnalogInput_Create(void);

/* S-Function Block: <S12>/motohawk_ain1 Resource: AN3M */
NativeError_S AN3M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
NativeError_S AN3M_AnalogInput_Create(void);

/* S-Function Block: <S14>/motohawk_ain14 Resource: DRVP */
NativeError_S DRVP_AnalogInput_Get(uint16_T *adc, uint16_T *status);
NativeError_S DRVP_AnalogInput_Create(void);

/* S-Function Block: <S15>/motohawk_ain4 Resource: AN7M */
NativeError_S AN7M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
NativeError_S AN7M_AnalogInput_Create(void);

/* S-Function Block: <S16>/motohawk_ain Resource: AN1M */
NativeError_S AN1M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
NativeError_S AN1M_AnalogInput_Create(void);

/* S-Function Block: <S17>/motohawk_ain1 Resource: AN4M */
NativeError_S AN4M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
NativeError_S AN4M_AnalogInput_Create(void);

/* S-Function Block: <S18>/motohawk_ain1 Resource: AN2M */
NativeError_S AN2M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
NativeError_S AN2M_AnalogInput_Create(void);

/* S-Function Block: <S97>/motohawk_dout Resource: MPRD */
extern NativeError_S DOut1918p0001_DiscreteOutput_Set(boolean_T in);
extern NativeError_S DOut1918p0001_DiscreteOutputPushPull_Set(int8_T in);
extern NativeError_S DOut1918p0001_DiscreteOutput_Create(void);

/* S-Function Block: <S98>/motohawk_pwm1 Resource: LSD10_PWMOutput */
void LSD10_PWMOutput_PWMOutput_Create(void);
void LSD10_PWMOutput_PWMOutput_Set(uint32_T freq, int16_T duty, boolean_T brake,
  boolean_T enable);
void LSD10_PWMOutput_PWMOutput_Stop(void);

/* S-Function Block: <S100>/motohawk_pwm Resource: EGR_Command */
void EGR_Command_PWMOutput_Create(void);
void EGR_Command_PWMOutput_Set(uint32_T freq, int16_T duty, boolean_T brake,
  boolean_T enable);
void EGR_Command_PWMOutput_Stop(void);

/* S-Function Block: <S101>/motohawk_pwm1 Resource: H1A_Out */
void H1A_Out_PWMOutput_Create(void);
void H1A_Out_PWMOutput_Set(uint32_T freq, int16_T duty, boolean_T brake,
  boolean_T enable);
void H1A_Out_PWMOutput_Stop(void);

/* S-Function Block: <S101>/motohawk_dout4 Resource: LSD5 */
extern NativeError_S DOut2397p0004_DiscreteOutput_Set(boolean_T in);
extern NativeError_S DOut2397p0004_DiscreteOutputPushPull_Set(int8_T in);
extern NativeError_S DOut2397p0004_DiscreteOutput_Create(void);

/* S-Function Block: <S102>/motohawk_pwm Resource: TACH_PWMOutput */
void TACH_PWMOutput_PWMOutput_Create(void);
void TACH_PWMOutput_PWMOutput_Set(uint32_T freq, int16_T duty, boolean_T brake,
  boolean_T enable);
void TACH_PWMOutput_PWMOutput_Stop(void);

/* S-Function Block: <S103>/motohawk_pwm Resource: THWO_Command */
void THWO_Command_PWMOutput_Create(void);
void THWO_Command_PWMOutput_Set(uint32_T freq, int16_T duty, boolean_T brake,
  boolean_T enable);
void THWO_Command_PWMOutput_Stop(void);

/* S-Function Block: <S104>/motohawk_pwm Resource: LSD9_PWMOutput */
void LSD9_PWMOutput_PWMOutput_Create(void);
void LSD9_PWMOutput_PWMOutput_Set(uint32_T freq, int16_T duty, boolean_T brake,
  boolean_T enable);
void LSD9_PWMOutput_PWMOutput_Stop(void);

/* S-Function Block: <S105>/motohawk_pwm Resource: VNTO */
void VNTO_PWMOutput_Create(void);
void VNTO_PWMOutput_Set(uint32_T freq, int16_T duty, boolean_T brake, boolean_T
  enable);
void VNTO_PWMOutput_Stop(void);
extern void MuxPSP1_MuxPSP_HardStart_SoftDuration_Set(uint8_T const state[],
  boolean_T use_state_min, int16_T const start_angle[], boolean_T
  use_start_angle_min, uint32_T const duration[], boolean_T use_dur_min,
  uint32_T const* min_dur);
extern void MuxPSP1_MuxPSP_Create(void);
extern void MuxPSP2_MuxPSP_HardStart_SoftDuration_Set(uint8_T const state[],
  boolean_T use_state_min, int16_T const start_angle[], boolean_T
  use_start_angle_min, uint32_T const duration[], boolean_T use_dur_min,
  uint32_T const* min_dur);
extern void MuxPSP2_MuxPSP_Create(void);
extern void MuxPSP3_MuxPSP_HardStart_SoftDuration_Set(uint8_T const state[],
  boolean_T use_state_min, int16_T const start_angle[], boolean_T
  use_start_angle_min, uint32_T const duration[], boolean_T use_dur_min,
  uint32_T const* min_dur);
extern void MuxPSP3_MuxPSP_Create(void);
extern void MuxPSP4_MuxPSP_HardStart_SoftDuration_Set(uint8_T const state[],
  boolean_T use_state_min, int16_T const start_angle[], boolean_T
  use_start_angle_min, uint32_T const duration[], boolean_T use_dur_min,
  uint32_T const* min_dur);
extern void MuxPSP4_MuxPSP_Create(void);

#endif
