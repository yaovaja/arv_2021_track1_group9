#ifndef COMMON_INCLUDE_H
#define COMMON_INCLUDE_H

/*---- INCLUDE FILES --------------------------------------------------------------------------------------*/
#include <stdlib.h>
#include <string.h>
#include <typedefn.h>
#include <rtwtypes.h>

/*---- TYPEDEFS -------------------------------------------------------------------------------------------*/

/* index_T used for prelookup index/fraction values */
typedef uint16_T index_T;

#include "MotoCoder.h"

/* fault_T used as an index to reference a Fault */
typedef uint32_T fault_T;

/* DataStorage uses matrix types for each built-in type */
typedef struct {
  uint32_T numCols;
  real_T data[1];
} matrix_real_T;

typedef struct {
  uint32_T numCols;
  real32_T data[1];
} matrix_real32_T;

typedef struct {
  uint32_T numCols;
  int8_T data[1];
} matrix_int8_T;

typedef struct {
  uint32_T numCols;
  uint8_T data[1];
} matrix_uint8_T;

typedef struct {
  uint32_T numCols;
  int16_T data[1];
} matrix_int16_T;

typedef struct {
  uint32_T numCols;
  uint16_T data[1];
} matrix_uint16_T;

typedef struct {
  uint32_T numCols;
  int32_T data[1];
} matrix_int32_T;

typedef struct {
  uint32_T numCols;
  uint32_T data[1];
} matrix_uint32_T;

typedef struct {
  uint32_T numCols;
  boolean_T data[1];
} matrix_boolean_T;

typedef struct {
  uint32_T numCols;
  reference_T data[1];
} matrix_reference_T;

typedef struct {
  uint32_T numCols;
  struct_reference_T data[1];
} matrix_struct_reference_T;

/*---- CUSTOM TYPEDEF CODE ---------------------------------------------------------------------------------*/
#define AC_ADC_DataStore()             (CRVLAB_ECU_Project_B.s8_motohawk_ain1)
#define AC_ADC_DataStore_ELEMS()       (1)
#define AC_ADC_DataStore_BYTES()       sizeof(CRVLAB_ECU_Project_B.s8_motohawk_ain1)
#define AC_ADC_DataStore_ROWS()        (1)
#define AC_ADC_DataStore_COLS()        (1)
#define ECT_ADC_DataStore()            (CRVLAB_ECU_Project_B.s11_DataTypeConversion1)
#define ECT_ADC_DataStore_ELEMS()      (1)
#define ECT_ADC_DataStore_BYTES()      sizeof(CRVLAB_ECU_Project_B.s11_DataTypeConversion1)
#define ECT_ADC_DataStore_ROWS()       (1)
#define ECT_ADC_DataStore_COLS()       (1)
#define EGRProbe_ADC_DataStore()       (CRVLAB_ECU_Project_B.s12_DataTypeConversion1)
#define EGRProbe_ADC_DataStore_ELEMS() (1)
#define EGRProbe_ADC_DataStore_BYTES() sizeof(CRVLAB_ECU_Project_B.s12_DataTypeConversion1)
#define EGRProbe_ADC_DataStore_ROWS()  (1)
#define EGRProbe_ADC_DataStore_COLS()  (1)
#define KeySw_ADC_DataStore()          (CRVLAB_ECU_Project_B.s14_motohawk_ain14)
#define KeySw_ADC_DataStore_ELEMS()    (1)
#define KeySw_ADC_DataStore_BYTES()    sizeof(CRVLAB_ECU_Project_B.s14_motohawk_ain14)
#define KeySw_ADC_DataStore_ROWS()     (1)
#define KeySw_ADC_DataStore_COLS()     (1)
#define MAFBeforeFilter_g_s_DataStore() (CRVLAB_ECU_Project_B.s15_motohawk_interpolation_1d)
#define MAFBeforeFilter_g_s_DataStore_ELEMS() (1)
#define MAFBeforeFilter_g_s_DataStore_BYTES() sizeof(CRVLAB_ECU_Project_B.s15_motohawk_interpolation_1d)
#define MAFBeforeFilter_g_s_DataStore_ROWS() (1)
#define MAFBeforeFilter_g_s_DataStore_COLS() (1)
#define MAF_ADC_DataStore()            (CRVLAB_ECU_Project_B.s15_DataTypeConversion1)
#define MAF_ADC_DataStore_ELEMS()      (1)
#define MAF_ADC_DataStore_BYTES()      sizeof(CRVLAB_ECU_Project_B.s15_DataTypeConversion1)
#define MAF_ADC_DataStore_ROWS()       (1)
#define MAF_ADC_DataStore_COLS()       (1)
#define Pedal_ADC_DataStore()          (CRVLAB_ECU_Project_B.s16_DataTypeConversion)
#define Pedal_ADC_DataStore_ELEMS()    (1)
#define Pedal_ADC_DataStore_BYTES()    sizeof(CRVLAB_ECU_Project_B.s16_DataTypeConversion)
#define Pedal_ADC_DataStore_ROWS()     (1)
#define Pedal_ADC_DataStore_COLS()     (1)
#define RailP_ADC_DataStore()          (CRVLAB_ECU_Project_B.s17_motohawk_ain1)
#define RailP_ADC_DataStore_ELEMS()    (1)
#define RailP_ADC_DataStore_BYTES()    sizeof(CRVLAB_ECU_Project_B.s17_motohawk_ain1)
#define RailP_ADC_DataStore_ROWS()     (1)
#define RailP_ADC_DataStore_COLS()     (1)
#define RailPBeforeFilt_MPa_DataStore() (CRVLAB_ECU_Project_B.s17_Sum1)
#define RailPBeforeFilt_MPa_DataStore_ELEMS() (1)
#define RailPBeforeFilt_MPa_DataStore_BYTES() sizeof(CRVLAB_ECU_Project_B.s17_Sum1)
#define RailPBeforeFilt_MPa_DataStore_ROWS() (1)
#define RailPBeforeFilt_MPa_DataStore_COLS() (1)
#define TPS_ADC_DataStore()            (CRVLAB_ECU_Project_B.s18_DataTypeConversion1)
#define TPS_ADC_DataStore_ELEMS()      (1)
#define TPS_ADC_DataStore_BYTES()      sizeof(CRVLAB_ECU_Project_B.s18_DataTypeConversion1)
#define TPS_ADC_DataStore_ROWS()       (1)
#define TPS_ADC_DataStore_COLS()       (1)
#define EGR_u_kp_DataStore()           (CRVLAB_ECU_Project_B.s59_Product2)
#define EGR_u_kp_DataStore_ELEMS()     (1)
#define EGR_u_kp_DataStore_BYTES()     sizeof(CRVLAB_ECU_Project_B.s59_Product2)
#define EGR_u_kp_DataStore_ROWS()      (1)
#define EGR_u_kp_DataStore_COLS()      (1)
#define EGR_u_ki_DataStore()           (CRVLAB_ECU_Project_B.s59_Product3)
#define EGR_u_ki_DataStore_ELEMS()     (1)
#define EGR_u_ki_DataStore_BYTES()     sizeof(CRVLAB_ECU_Project_B.s59_Product3)
#define EGR_u_ki_DataStore_ROWS()      (1)
#define EGR_u_ki_DataStore_COLS()      (1)
#define EGR_u_kd_DataStore()           (CRVLAB_ECU_Project_B.s59_Product4)
#define EGR_u_kd_DataStore_ELEMS()     (1)
#define EGR_u_kd_DataStore_BYTES()     sizeof(CRVLAB_ECU_Project_B.s59_Product4)
#define EGR_u_kd_DataStore_ROWS()      (1)
#define EGR_u_kd_DataStore_COLS()      (1)
#define EGR_u_DataStore()              (CRVLAB_ECU_Project_B.s67_Merge)
#define EGR_u_DataStore_ELEMS()        (1)
#define EGR_u_DataStore_BYTES()        sizeof(CRVLAB_ECU_Project_B.s67_Merge)
#define EGR_u_DataStore_ROWS()         (1)
#define EGR_u_DataStore_COLS()         (1)
#define IdleSpeed_u_kp_DataStore()     (CRVLAB_ECU_Project_B.s60_Product2)
#define IdleSpeed_u_kp_DataStore_ELEMS() (1)
#define IdleSpeed_u_kp_DataStore_BYTES() sizeof(CRVLAB_ECU_Project_B.s60_Product2)
#define IdleSpeed_u_kp_DataStore_ROWS() (1)
#define IdleSpeed_u_kp_DataStore_COLS() (1)
#define IdleSpeed_u_ki_DataStore()     (CRVLAB_ECU_Project_B.s60_Product3)
#define IdleSpeed_u_ki_DataStore_ELEMS() (1)
#define IdleSpeed_u_ki_DataStore_BYTES() sizeof(CRVLAB_ECU_Project_B.s60_Product3)
#define IdleSpeed_u_ki_DataStore_ROWS() (1)
#define IdleSpeed_u_ki_DataStore_COLS() (1)
#define IdleSpeed_u_kd_DataStore()     (CRVLAB_ECU_Project_B.s60_Product4)
#define IdleSpeed_u_kd_DataStore_ELEMS() (1)
#define IdleSpeed_u_kd_DataStore_BYTES() sizeof(CRVLAB_ECU_Project_B.s60_Product4)
#define IdleSpeed_u_kd_DataStore_ROWS() (1)
#define IdleSpeed_u_kd_DataStore_COLS() (1)
#define IdleSpeed_u_DataStore()        (CRVLAB_ECU_Project_B.s74_Merge)
#define IdleSpeed_u_DataStore_ELEMS()  (1)
#define IdleSpeed_u_DataStore_BYTES()  sizeof(CRVLAB_ECU_Project_B.s74_Merge)
#define IdleSpeed_u_DataStore_ROWS()   (1)
#define IdleSpeed_u_DataStore_COLS()   (1)
#define RailP_u_kp_DataStore()         (CRVLAB_ECU_Project_B.s61_Product2)
#define RailP_u_kp_DataStore_ELEMS()   (1)
#define RailP_u_kp_DataStore_BYTES()   sizeof(CRVLAB_ECU_Project_B.s61_Product2)
#define RailP_u_kp_DataStore_ROWS()    (1)
#define RailP_u_kp_DataStore_COLS()    (1)
#define RailP_u_ki_DataStore()         (CRVLAB_ECU_Project_B.s61_Product3)
#define RailP_u_ki_DataStore_ELEMS()   (1)
#define RailP_u_ki_DataStore_BYTES()   sizeof(CRVLAB_ECU_Project_B.s61_Product3)
#define RailP_u_ki_DataStore_ROWS()    (1)
#define RailP_u_ki_DataStore_COLS()    (1)
#define RailP_u_kd_DataStore()         (CRVLAB_ECU_Project_B.s61_Product4)
#define RailP_u_kd_DataStore_ELEMS()   (1)
#define RailP_u_kd_DataStore_BYTES()   sizeof(CRVLAB_ECU_Project_B.s61_Product4)
#define RailP_u_kd_DataStore_ROWS()    (1)
#define RailP_u_kd_DataStore_COLS()    (1)
#define RailP_u_DataStore()            (CRVLAB_ECU_Project_B.s81_Merge)
#define RailP_u_DataStore_ELEMS()      (1)
#define RailP_u_DataStore_BYTES()      sizeof(CRVLAB_ECU_Project_B.s81_Merge)
#define RailP_u_DataStore_ROWS()       (1)
#define RailP_u_DataStore_COLS()       (1)
#define Throttle_u_kp_DataStore()      (CRVLAB_ECU_Project_B.s62_Product2)
#define Throttle_u_kp_DataStore_ELEMS() (1)
#define Throttle_u_kp_DataStore_BYTES() sizeof(CRVLAB_ECU_Project_B.s62_Product2)
#define Throttle_u_kp_DataStore_ROWS() (1)
#define Throttle_u_kp_DataStore_COLS() (1)
#define Throttle_u_ki_DataStore()      (CRVLAB_ECU_Project_B.s62_Product3)
#define Throttle_u_ki_DataStore_ELEMS() (1)
#define Throttle_u_ki_DataStore_BYTES() sizeof(CRVLAB_ECU_Project_B.s62_Product3)
#define Throttle_u_ki_DataStore_ROWS() (1)
#define Throttle_u_ki_DataStore_COLS() (1)
#define Throttle_u_kd_DataStore()      (CRVLAB_ECU_Project_B.s62_Product4)
#define Throttle_u_kd_DataStore_ELEMS() (1)
#define Throttle_u_kd_DataStore_BYTES() sizeof(CRVLAB_ECU_Project_B.s62_Product4)
#define Throttle_u_kd_DataStore_ROWS() (1)
#define Throttle_u_kd_DataStore_COLS() (1)
#define Throttle_u_DataStore()         (CRVLAB_ECU_Project_B.s87_Merge)
#define Throttle_u_DataStore_ELEMS()   (1)
#define Throttle_u_DataStore_BYTES()   sizeof(CRVLAB_ECU_Project_B.s87_Merge)
#define Throttle_u_DataStore_ROWS()    (1)
#define Throttle_u_DataStore_COLS()    (1)
#define VNT_u_kp_DataStore()           (CRVLAB_ECU_Project_B.s63_Product2)
#define VNT_u_kp_DataStore_ELEMS()     (1)
#define VNT_u_kp_DataStore_BYTES()     sizeof(CRVLAB_ECU_Project_B.s63_Product2)
#define VNT_u_kp_DataStore_ROWS()      (1)
#define VNT_u_kp_DataStore_COLS()      (1)
#define VNT_u_ki_DataStore()           (CRVLAB_ECU_Project_B.s63_Product3)
#define VNT_u_ki_DataStore_ELEMS()     (1)
#define VNT_u_ki_DataStore_BYTES()     sizeof(CRVLAB_ECU_Project_B.s63_Product3)
#define VNT_u_ki_DataStore_ROWS()      (1)
#define VNT_u_ki_DataStore_COLS()      (1)
#define VNT_u_kd_DataStore()           (CRVLAB_ECU_Project_B.s63_Product4)
#define VNT_u_kd_DataStore_ELEMS()     (1)
#define VNT_u_kd_DataStore_BYTES()     sizeof(CRVLAB_ECU_Project_B.s63_Product4)
#define VNT_u_kd_DataStore_ROWS()      (1)
#define VNT_u_kd_DataStore_COLS()      (1)
#define VNT_u_DataStore()              (CRVLAB_ECU_Project_B.s93_Merge)
#define VNT_u_DataStore_ELEMS()        (1)
#define VNT_u_DataStore_BYTES()        sizeof(CRVLAB_ECU_Project_B.s93_Merge)
#define VNT_u_DataStore_ROWS()         (1)
#define VNT_u_DataStore_COLS()         (1)
#define CngInjEnabled_DataStore()      (CRVLAB_ECU_Project_B.s96_RelationalOperator5)
#define CngInjEnabled_DataStore_ELEMS() (1)
#define CngInjEnabled_DataStore_BYTES() sizeof(CRVLAB_ECU_Project_B.s96_RelationalOperator5)
#define CngInjEnabled_DataStore_ROWS() (1)
#define CngInjEnabled_DataStore_COLS() (1)
#define Inj1Enabled_DataStore()        (CRVLAB_ECU_Project_B.s114_LogicalOperator)
#define Inj1Enabled_DataStore_ELEMS()  (1)
#define Inj1Enabled_DataStore_BYTES()  sizeof(CRVLAB_ECU_Project_B.s114_LogicalOperator)
#define Inj1Enabled_DataStore_ROWS()   (1)
#define Inj1Enabled_DataStore_COLS()   (1)
#define Inj2Enabled_DataStore()        (CRVLAB_ECU_Project_B.s115_LogicalOperator)
#define Inj2Enabled_DataStore_ELEMS()  (1)
#define Inj2Enabled_DataStore_BYTES()  sizeof(CRVLAB_ECU_Project_B.s115_LogicalOperator)
#define Inj2Enabled_DataStore_ROWS()   (1)
#define Inj2Enabled_DataStore_COLS()   (1)
#define Inj3Enabled_DataStore()        (CRVLAB_ECU_Project_B.s116_LogicalOperator)
#define Inj3Enabled_DataStore_ELEMS()  (1)
#define Inj3Enabled_DataStore_BYTES()  sizeof(CRVLAB_ECU_Project_B.s116_LogicalOperator)
#define Inj3Enabled_DataStore_ROWS()   (1)
#define Inj3Enabled_DataStore_COLS()   (1)
#define Inj4Enabled_DataStore()        (CRVLAB_ECU_Project_B.s117_LogicalOperator)
#define Inj4Enabled_DataStore_ELEMS()  (1)
#define Inj4Enabled_DataStore_BYTES()  sizeof(CRVLAB_ECU_Project_B.s117_LogicalOperator)
#define Inj4Enabled_DataStore_ROWS()   (1)
#define Inj4Enabled_DataStore_COLS()   (1)
#define MainFuelingProbe_ms_DataStore() (CRVLAB_ECU_Project_B.s124_Merge)
#define MainFuelingProbe_ms_DataStore_ELEMS() (1)
#define MainFuelingProbe_ms_DataStore_BYTES() sizeof(CRVLAB_ECU_Project_B.s124_Merge)
#define MainFuelingProbe_ms_DataStore_ROWS() (1)
#define MainFuelingProbe_ms_DataStore_COLS() (1)
#define PreSOIProbe_degBTDC_DataStore() (CRVLAB_ECU_Project_B.s125_Merge)
#define PreSOIProbe_degBTDC_DataStore_ELEMS() (1)
#define PreSOIProbe_degBTDC_DataStore_BYTES() sizeof(CRVLAB_ECU_Project_B.s125_Merge)
#define PreSOIProbe_degBTDC_DataStore_ROWS() (1)
#define PreSOIProbe_degBTDC_DataStore_COLS() (1)
#define PilotSOIProbe_degBTDC_DataStore() (CRVLAB_ECU_Project_B.s126_Merge)
#define PilotSOIProbe_degBTDC_DataStore_ELEMS() (1)
#define PilotSOIProbe_degBTDC_DataStore_BYTES() sizeof(CRVLAB_ECU_Project_B.s126_Merge)
#define PilotSOIProbe_degBTDC_DataStore_ROWS() (1)
#define PilotSOIProbe_degBTDC_DataStore_COLS() (1)
#define MainSOIProbe_degBTDC_DataStore() (CRVLAB_ECU_Project_B.s127_Merge)
#define MainSOIProbe_degBTDC_DataStore_ELEMS() (1)
#define MainSOIProbe_degBTDC_DataStore_BYTES() sizeof(CRVLAB_ECU_Project_B.s127_Merge)
#define MainSOIProbe_degBTDC_DataStore_ROWS() (1)
#define MainSOIProbe_degBTDC_DataStore_COLS() (1)
#define PreFuelingProbe_ms_DataStore() (CRVLAB_ECU_Project_B.s122_Merge)
#define PreFuelingProbe_ms_DataStore_ELEMS() (1)
#define PreFuelingProbe_ms_DataStore_BYTES() sizeof(CRVLAB_ECU_Project_B.s122_Merge)
#define PreFuelingProbe_ms_DataStore_ROWS() (1)
#define PreFuelingProbe_ms_DataStore_COLS() (1)
#define PilotFuelingProbe_ms_DataStore() (CRVLAB_ECU_Project_B.s123_Merge)
#define PilotFuelingProbe_ms_DataStore_ELEMS() (1)
#define PilotFuelingProbe_ms_DataStore_BYTES() sizeof(CRVLAB_ECU_Project_B.s123_Merge)
#define PilotFuelingProbe_ms_DataStore_ROWS() (1)
#define PilotFuelingProbe_ms_DataStore_COLS() (1)
#define H_bridge1_DC_DataStore()       (CRVLAB_ECU_Project_B.s101_DataTypeConversion4)
#define H_bridge1_DC_DataStore_ELEMS() (1)
#define H_bridge1_DC_DataStore_BYTES() sizeof(CRVLAB_ECU_Project_B.s101_DataTypeConversion4)
#define H_bridge1_DC_DataStore_ROWS()  (1)
#define H_bridge1_DC_DataStore_COLS()  (1)
#define GasGlow_Relays_Enabled_DataStore() (CRVLAB_ECU_Project_B.s144_Merge)
#define GasGlow_Relays_Enabled_DataStore_ELEMS() (1)
#define GasGlow_Relays_Enabled_DataStore_BYTES() sizeof(CRVLAB_ECU_Project_B.s144_Merge)
#define GasGlow_Relays_Enabled_DataStore_ROWS() (1)
#define GasGlow_Relays_Enabled_DataStore_COLS() (1)
#define GlowPlugRelayOn_DataStore()    (CRVLAB_ECU_Project_B.s141_LogicalOperator1)
#define GlowPlugRelayOn_DataStore_ELEMS() (1)
#define GlowPlugRelayOn_DataStore_BYTES() sizeof(CRVLAB_ECU_Project_B.s141_LogicalOperator1)
#define GlowPlugRelayOn_DataStore_ROWS() (1)
#define GlowPlugRelayOn_DataStore_COLS() (1)
#define EDU_Relay_Enabled_DataStore()  (CRVLAB_ECU_Project_B.s101_RelationalOperator)
#define EDU_Relay_Enabled_DataStore_ELEMS() (1)
#define EDU_Relay_Enabled_DataStore_BYTES() sizeof(CRVLAB_ECU_Project_B.s101_RelationalOperator)
#define EDU_Relay_Enabled_DataStore_ROWS() (1)
#define EDU_Relay_Enabled_DataStore_COLS() (1)
#include "Application.h"

/*---- END OF FILE ----------------------------------------------------------------------------------------*/
#endif                                 /* COMMON_INCLUDE_H */
