/*
 * File: CRVLAB_ECU_Project.c
 *
 * Code generated for Simulink model 'CRVLAB_ECU_Project'.
 *
 * Model version                  : 1.812
 * Simulink Coder version         : 8.0 (R2011a) 09-Mar-2011
 * TLC version                    : 8.0 (Feb  3 2011)
 * C/C++ source code generated on : Tue Apr 10 17:20:14 2012
 *
 * Target selection: motohawk_ert_rtw.tlc
 * Embedded hardware selection: Specified
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "CRVLAB_ECU_Project.h"
#include "CRVLAB_ECU_Project_private.h"

/* Block signals (auto storage) */
BlockIO_CRVLAB_ECU_Project CRVLAB_ECU_Project_B;

/* Block states (auto storage) */
D_Work_CRVLAB_ECU_Project CRVLAB_ECU_Project_DWork;

/* Real-time model */
RT_MODEL_CRVLAB_ECU_Project CRVLAB_ECU_Project_M_;
RT_MODEL_CRVLAB_ECU_Project *const CRVLAB_ECU_Project_M = &CRVLAB_ECU_Project_M_;

/* Model step function */
void CRVLAB_ECU_Project_step(void)
{
  /* (no output/update code required) */
}

/* Model initialize function */
void CRVLAB_ECU_Project_initialize(boolean_T firstTime)
{
  (void)firstTime;

  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize error status */
  rtmSetErrorStatus(CRVLAB_ECU_Project_M, (NULL));

  /* block I/O */
  (void) memset(((void *) &CRVLAB_ECU_Project_B), 0,
                sizeof(BlockIO_CRVLAB_ECU_Project));

  {
    CRVLAB_ECU_Project_B.s11_DataTypeConversion1 = 0.0;
    CRVLAB_ECU_Project_B.s12_DataTypeConversion1 = 0.0;
    CRVLAB_ECU_Project_B.s15_DataTypeConversion1 = 0.0;
    CRVLAB_ECU_Project_B.s15_motohawk_interpolation_1d = 0.0;
    CRVLAB_ECU_Project_B.s16_DataTypeConversion = 0.0;
    CRVLAB_ECU_Project_B.s17_Sum1 = 0.0;
    CRVLAB_ECU_Project_B.s18_DataTypeConversion1 = 0.0;
    CRVLAB_ECU_Project_B.s61_Product2 = 0.0;
    CRVLAB_ECU_Project_B.s61_Product3 = 0.0;
    CRVLAB_ECU_Project_B.s61_Product4 = 0.0;
    CRVLAB_ECU_Project_B.s81_Merge = 0.0;
    CRVLAB_ECU_Project_B.s62_Product2 = 0.0;
    CRVLAB_ECU_Project_B.s62_Product3 = 0.0;
    CRVLAB_ECU_Project_B.s62_Product4 = 0.0;
    CRVLAB_ECU_Project_B.s87_Merge = 0.0;
    CRVLAB_ECU_Project_B.s59_Product2 = 0.0;
    CRVLAB_ECU_Project_B.s59_Product3 = 0.0;
    CRVLAB_ECU_Project_B.s59_Product4 = 0.0;
    CRVLAB_ECU_Project_B.s67_Merge = 0.0;
    CRVLAB_ECU_Project_B.s60_Product2 = 0.0;
    CRVLAB_ECU_Project_B.s60_Product3 = 0.0;
    CRVLAB_ECU_Project_B.s60_Product4 = 0.0;
    CRVLAB_ECU_Project_B.s74_Merge = 0.0;
    CRVLAB_ECU_Project_B.s63_Product2 = 0.0;
    CRVLAB_ECU_Project_B.s63_Product3 = 0.0;
    CRVLAB_ECU_Project_B.s63_Product4 = 0.0;
    CRVLAB_ECU_Project_B.s93_Merge = 0.0;
    CRVLAB_ECU_Project_B.s125_Merge = 0.0;
    CRVLAB_ECU_Project_B.s122_Merge = 0.0;
    CRVLAB_ECU_Project_B.s126_Merge = 0.0;
    CRVLAB_ECU_Project_B.s123_Merge = 0.0;
    CRVLAB_ECU_Project_B.s127_Merge = 0.0;
    CRVLAB_ECU_Project_B.s124_Merge = 0.0;
    CRVLAB_ECU_Project_B.s26_State = 0.0;
  }

  /* states (dwork) */
  (void) memset((void *)&CRVLAB_ECU_Project_DWork, 0,
                sizeof(D_Work_CRVLAB_ECU_Project));
  CRVLAB_ECU_Project_DWork.s24_UnitDelay_DSTATE = 0.0;
  CRVLAB_ECU_Project_DWork.s25_UnitDelay1_DSTATE = 0.0;
  CRVLAB_ECU_Project_DWork.s25_UnitDelay_DSTATE = 0.0;
  CRVLAB_ECU_Project_DWork.s61_UnitDelay1_DSTATE = 0.0;
  CRVLAB_ECU_Project_DWork.s61_UnitDelay_DSTATE = 0.0;
  CRVLAB_ECU_Project_DWork.s62_UnitDelay1_DSTATE = 0.0;
  CRVLAB_ECU_Project_DWork.s62_UnitDelay_DSTATE = 0.0;
  CRVLAB_ECU_Project_DWork.s59_UnitDelay1_DSTATE = 0.0;
  CRVLAB_ECU_Project_DWork.s59_UnitDelay_DSTATE = 0.0;
  CRVLAB_ECU_Project_DWork.s60_UnitDelay1_DSTATE = 0.0;
  CRVLAB_ECU_Project_DWork.s60_UnitDelay_DSTATE = 0.0;
  CRVLAB_ECU_Project_DWork.s63_UnitDelay1_DSTATE = 0.0;
  CRVLAB_ECU_Project_DWork.s63_UnitDelay_DSTATE = 0.0;

  /* Start for S-Function (motohawk_sfun_trigger): '<Root>/motohawk_trigger' */
  CRVLAB_ECU_Project_Model_Start();

  /* InitializeConditions for S-Function (motohawk_sfun_trigger): '<Root>/motohawk_trigger' */
  CRVLAB_ECU_Project_Model_Init();
}

/* Model terminate function */
void CRVLAB_ECU_Project_terminate(void)
{
  /* (no terminate code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
