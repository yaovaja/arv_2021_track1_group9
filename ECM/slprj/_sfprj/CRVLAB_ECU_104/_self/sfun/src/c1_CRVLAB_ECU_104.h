#ifndef __c1_CRVLAB_ECU_104_h__
#define __c1_CRVLAB_ECU_104_h__

/* Include files */
#include "sfc_sf.h"
#include "sfc_mex.h"
#include "rtwtypes.h"

/* Type Definitions */
typedef struct {
  SimStruct *S;
  const mxArray *c1_setSimStateSideEffectsInfo;
  int32_T c1_sfEvent;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  boolean_T c1_isStable;
  uint8_T c1_doSetSimStateSideEffects;
  uint8_T c1_is_Cut;
  uint8_T c1_is_Run;
  uint8_T c1_is_active_c1_CRVLAB_ECU_104;
  uint8_T c1_is_c1_CRVLAB_ECU_104;
  uint8_T c1_tp_Crank;
  uint8_T c1_tp_Cut;
  uint8_T c1_tp_FuelCut;
  uint8_T c1_tp_Idle;
  uint8_T c1_tp_Run;
  uint8_T c1_tp_RunDDF;
  uint8_T c1_tp_RunDiesel;
  uint8_T c1_tp_Stall;
  ChartInfoStruct chartInfo;
} SFc1_CRVLAB_ECU_104InstanceStruct;

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c1_CRVLAB_ECU_104_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c1_CRVLAB_ECU_104_get_check_sum(mxArray *plhs[]);
extern void c1_CRVLAB_ECU_104_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif
