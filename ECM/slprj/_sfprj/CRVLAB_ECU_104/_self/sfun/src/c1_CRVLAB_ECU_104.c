/* Include files */

#include "blascompat32.h"
#include "CRVLAB_ECU_104_sfun.h"
#include "c1_CRVLAB_ECU_104.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "CRVLAB_ECU_104_sfun_debug_macros.h"

/* Type Definitions */

/* Named Constants */
#define c1_IN_NO_ACTIVE_CHILD          (0U)
#define c1_IN_RunDiesel                (2U)
#define c1_IN_Crank                    (1U)
#define c1_IN_Idle                     (2U)
#define c1_IN_Stall                    (4U)
#define c1_IN_RunDDF                   (1U)
#define c1_IN_FuelCut                  (1U)
#define c1_IN_Run                      (3U)
#define c1_IN_Cut                      (2U)

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
static void initialize_c1_CRVLAB_ECU_104(SFc1_CRVLAB_ECU_104InstanceStruct
  *chartInstance);
static void initialize_params_c1_CRVLAB_ECU_104
  (SFc1_CRVLAB_ECU_104InstanceStruct *chartInstance);
static void enable_c1_CRVLAB_ECU_104(SFc1_CRVLAB_ECU_104InstanceStruct
  *chartInstance);
static void disable_c1_CRVLAB_ECU_104(SFc1_CRVLAB_ECU_104InstanceStruct
  *chartInstance);
static void c1_update_debugger_state_c1_CRVLAB_ECU_104
  (SFc1_CRVLAB_ECU_104InstanceStruct *chartInstance);
static const mxArray *get_sim_state_c1_CRVLAB_ECU_104
  (SFc1_CRVLAB_ECU_104InstanceStruct *chartInstance);
static void set_sim_state_c1_CRVLAB_ECU_104(SFc1_CRVLAB_ECU_104InstanceStruct
  *chartInstance, const mxArray *c1_st);
static void c1_set_sim_state_side_effects_c1_CRVLAB_ECU_104
  (SFc1_CRVLAB_ECU_104InstanceStruct *chartInstance);
static void finalize_c1_CRVLAB_ECU_104(SFc1_CRVLAB_ECU_104InstanceStruct
  *chartInstance);
static void sf_c1_CRVLAB_ECU_104(SFc1_CRVLAB_ECU_104InstanceStruct
  *chartInstance);
static void c1_chartstep_c1_CRVLAB_ECU_104(SFc1_CRVLAB_ECU_104InstanceStruct
  *chartInstance);
static void initSimStructsc1_CRVLAB_ECU_104(SFc1_CRVLAB_ECU_104InstanceStruct
  *chartInstance);
static void c1_Crank(SFc1_CRVLAB_ECU_104InstanceStruct *chartInstance);
static void c1_exit_internal_Run(SFc1_CRVLAB_ECU_104InstanceStruct
  *chartInstance);
static void c1_exit_internal_Cut(SFc1_CRVLAB_ECU_104InstanceStruct
  *chartInstance);
static void init_script_number_translation(uint32_T c1_machineNumber, uint32_T
  c1_chartNumber);
static const mxArray *c1_sf_marshallOut(void *chartInstanceVoid, void *c1_inData);
static int32_T c1_emlrt_marshallIn(SFc1_CRVLAB_ECU_104InstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId);
static void c1_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static const mxArray *c1_b_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static uint8_T c1_b_emlrt_marshallIn(SFc1_CRVLAB_ECU_104InstanceStruct
  *chartInstance, const mxArray *c1_b_tp_RunDiesel, const char_T *c1_identifier);
static uint8_T c1_c_emlrt_marshallIn(SFc1_CRVLAB_ECU_104InstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId);
static void c1_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static const mxArray *c1_c_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static const mxArray *c1_d_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static real_T c1_d_emlrt_marshallIn(SFc1_CRVLAB_ECU_104InstanceStruct
  *chartInstance, const mxArray *c1_State, const char_T *c1_identifier);
static real_T c1_e_emlrt_marshallIn(SFc1_CRVLAB_ECU_104InstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId);
static void c1_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static const mxArray *c1_f_emlrt_marshallIn(SFc1_CRVLAB_ECU_104InstanceStruct
  *chartInstance, const mxArray *c1_b_setSimStateSideEffectsInfo, const char_T
  *c1_identifier);
static const mxArray *c1_g_emlrt_marshallIn(SFc1_CRVLAB_ECU_104InstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId);
static void init_dsm_address_info(SFc1_CRVLAB_ECU_104InstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c1_CRVLAB_ECU_104(SFc1_CRVLAB_ECU_104InstanceStruct
  *chartInstance)
{
  real_T *c1_State;
  c1_State = (real_T *)ssGetOutputPortSignal(chartInstance->S, 1);
  chartInstance->c1_sfEvent = CALL_EVENT;
  _sfTime_ = (real_T)ssGetT(chartInstance->S);
  chartInstance->c1_doSetSimStateSideEffects = 0U;
  chartInstance->c1_setSimStateSideEffectsInfo = NULL;
  chartInstance->c1_tp_Crank = 0U;
  chartInstance->c1_is_Cut = 0U;
  chartInstance->c1_tp_Cut = 0U;
  chartInstance->c1_tp_FuelCut = 0U;
  chartInstance->c1_tp_Idle = 0U;
  chartInstance->c1_is_Run = 0U;
  chartInstance->c1_tp_Run = 0U;
  chartInstance->c1_tp_RunDDF = 0U;
  chartInstance->c1_tp_RunDiesel = 0U;
  chartInstance->c1_tp_Stall = 0U;
  chartInstance->c1_is_active_c1_CRVLAB_ECU_104 = 0U;
  chartInstance->c1_is_c1_CRVLAB_ECU_104 = 0U;
  if (!(cdrGetOutputPortReusable(chartInstance->S, 1) != 0)) {
    *c1_State = 0.0;
  }
}

static void initialize_params_c1_CRVLAB_ECU_104
  (SFc1_CRVLAB_ECU_104InstanceStruct *chartInstance)
{
}

static void enable_c1_CRVLAB_ECU_104(SFc1_CRVLAB_ECU_104InstanceStruct
  *chartInstance)
{
  _sfTime_ = (real_T)ssGetT(chartInstance->S);
}

static void disable_c1_CRVLAB_ECU_104(SFc1_CRVLAB_ECU_104InstanceStruct
  *chartInstance)
{
  _sfTime_ = (real_T)ssGetT(chartInstance->S);
}

static void c1_update_debugger_state_c1_CRVLAB_ECU_104
  (SFc1_CRVLAB_ECU_104InstanceStruct *chartInstance)
{
  uint32_T c1_prevAniVal;
  c1_prevAniVal = sf_debug_get_animation();
  sf_debug_set_animation(0U);
  if ((int16_T)chartInstance->c1_is_active_c1_CRVLAB_ECU_104 == 1) {
    _SFD_CC_CALL(CHART_ACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
  }

  if (chartInstance->c1_is_Run == c1_IN_RunDiesel) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 6U, chartInstance->c1_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 6U, chartInstance->c1_sfEvent);
  }

  if (chartInstance->c1_is_c1_CRVLAB_ECU_104 == c1_IN_Crank) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
  }

  if (chartInstance->c1_is_Cut == c1_IN_Idle) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 3U, chartInstance->c1_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, chartInstance->c1_sfEvent);
  }

  if (chartInstance->c1_is_c1_CRVLAB_ECU_104 == c1_IN_Stall) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 7U, chartInstance->c1_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 7U, chartInstance->c1_sfEvent);
  }

  if (chartInstance->c1_is_Run == c1_IN_RunDDF) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 5U, chartInstance->c1_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 5U, chartInstance->c1_sfEvent);
  }

  if (chartInstance->c1_is_Cut == c1_IN_FuelCut) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c1_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c1_sfEvent);
  }

  if (chartInstance->c1_is_c1_CRVLAB_ECU_104 == c1_IN_Run) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 4U, chartInstance->c1_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 4U, chartInstance->c1_sfEvent);
  }

  if (chartInstance->c1_is_c1_CRVLAB_ECU_104 == c1_IN_Cut) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c1_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c1_sfEvent);
  }

  sf_debug_set_animation(c1_prevAniVal);
  _SFD_ANIMATE();
}

static const mxArray *get_sim_state_c1_CRVLAB_ECU_104
  (SFc1_CRVLAB_ECU_104InstanceStruct *chartInstance)
{
  const mxArray *c1_st;
  const mxArray *c1_y = NULL;
  real_T c1_hoistedGlobal;
  real_T c1_u;
  const mxArray *c1_b_y = NULL;
  uint8_T c1_b_hoistedGlobal;
  uint8_T c1_b_u;
  const mxArray *c1_c_y = NULL;
  uint8_T c1_c_hoistedGlobal;
  uint8_T c1_c_u;
  const mxArray *c1_d_y = NULL;
  uint8_T c1_d_hoistedGlobal;
  uint8_T c1_d_u;
  const mxArray *c1_e_y = NULL;
  uint8_T c1_e_hoistedGlobal;
  uint8_T c1_e_u;
  const mxArray *c1_f_y = NULL;
  real_T *c1_State;
  c1_State = (real_T *)ssGetOutputPortSignal(chartInstance->S, 1);
  c1_st = NULL;
  c1_st = NULL;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_createcellarray(5));
  c1_hoistedGlobal = *c1_State;
  c1_u = c1_hoistedGlobal;
  c1_b_y = NULL;
  sf_mex_assign(&c1_b_y, sf_mex_create("y", &c1_u, 0, 0U, 0U, 0U, 0));
  sf_mex_setcell(c1_y, 0, c1_b_y);
  c1_b_hoistedGlobal = chartInstance->c1_is_active_c1_CRVLAB_ECU_104;
  c1_b_u = c1_b_hoistedGlobal;
  c1_c_y = NULL;
  sf_mex_assign(&c1_c_y, sf_mex_create("y", &c1_b_u, 3, 0U, 0U, 0U, 0));
  sf_mex_setcell(c1_y, 1, c1_c_y);
  c1_c_hoistedGlobal = chartInstance->c1_is_c1_CRVLAB_ECU_104;
  c1_c_u = c1_c_hoistedGlobal;
  c1_d_y = NULL;
  sf_mex_assign(&c1_d_y, sf_mex_create("y", &c1_c_u, 3, 0U, 0U, 0U, 0));
  sf_mex_setcell(c1_y, 2, c1_d_y);
  c1_d_hoistedGlobal = chartInstance->c1_is_Run;
  c1_d_u = c1_d_hoistedGlobal;
  c1_e_y = NULL;
  sf_mex_assign(&c1_e_y, sf_mex_create("y", &c1_d_u, 3, 0U, 0U, 0U, 0));
  sf_mex_setcell(c1_y, 3, c1_e_y);
  c1_e_hoistedGlobal = chartInstance->c1_is_Cut;
  c1_e_u = c1_e_hoistedGlobal;
  c1_f_y = NULL;
  sf_mex_assign(&c1_f_y, sf_mex_create("y", &c1_e_u, 3, 0U, 0U, 0U, 0));
  sf_mex_setcell(c1_y, 4, c1_f_y);
  sf_mex_assign(&c1_st, c1_y);
  return c1_st;
}

static void set_sim_state_c1_CRVLAB_ECU_104(SFc1_CRVLAB_ECU_104InstanceStruct
  *chartInstance, const mxArray *c1_st)
{
  const mxArray *c1_u;
  real_T *c1_State;
  c1_State = (real_T *)ssGetOutputPortSignal(chartInstance->S, 1);
  c1_u = sf_mex_dup(c1_st);
  *c1_State = c1_d_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell
    (c1_u, 0)), "State");
  chartInstance->c1_is_active_c1_CRVLAB_ECU_104 = c1_b_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c1_u, 1)),
     "is_active_c1_CRVLAB_ECU_104");
  chartInstance->c1_is_c1_CRVLAB_ECU_104 = c1_b_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c1_u, 2)), "is_c1_CRVLAB_ECU_104");
  chartInstance->c1_is_Run = c1_b_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c1_u, 3)), "is_Run");
  chartInstance->c1_is_Cut = c1_b_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c1_u, 4)), "is_Cut");
  sf_mex_assign(&chartInstance->c1_setSimStateSideEffectsInfo,
                c1_f_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell
    (c1_u, 5)), "setSimStateSideEffectsInfo"));
  sf_mex_destroy(&c1_u);
  chartInstance->c1_doSetSimStateSideEffects = 1U;
  c1_update_debugger_state_c1_CRVLAB_ECU_104(chartInstance);
  sf_mex_destroy(&c1_st);
}

static void c1_set_sim_state_side_effects_c1_CRVLAB_ECU_104
  (SFc1_CRVLAB_ECU_104InstanceStruct *chartInstance)
{
  if (chartInstance->c1_doSetSimStateSideEffects != 0) {
    if (chartInstance->c1_is_c1_CRVLAB_ECU_104 == c1_IN_Crank) {
      chartInstance->c1_tp_Crank = 1U;
    } else {
      chartInstance->c1_tp_Crank = 0U;
    }

    if (chartInstance->c1_is_c1_CRVLAB_ECU_104 == c1_IN_Cut) {
      chartInstance->c1_tp_Cut = 1U;
    } else {
      chartInstance->c1_tp_Cut = 0U;
    }

    if (chartInstance->c1_is_Cut == c1_IN_FuelCut) {
      chartInstance->c1_tp_FuelCut = 1U;
    } else {
      chartInstance->c1_tp_FuelCut = 0U;
    }

    if (chartInstance->c1_is_Cut == c1_IN_Idle) {
      chartInstance->c1_tp_Idle = 1U;
    } else {
      chartInstance->c1_tp_Idle = 0U;
    }

    if (chartInstance->c1_is_c1_CRVLAB_ECU_104 == c1_IN_Run) {
      chartInstance->c1_tp_Run = 1U;
    } else {
      chartInstance->c1_tp_Run = 0U;
    }

    if (chartInstance->c1_is_Run == c1_IN_RunDDF) {
      chartInstance->c1_tp_RunDDF = 1U;
    } else {
      chartInstance->c1_tp_RunDDF = 0U;
    }

    if (chartInstance->c1_is_Run == c1_IN_RunDiesel) {
      chartInstance->c1_tp_RunDiesel = 1U;
    } else {
      chartInstance->c1_tp_RunDiesel = 0U;
    }

    if (chartInstance->c1_is_c1_CRVLAB_ECU_104 == c1_IN_Stall) {
      chartInstance->c1_tp_Stall = 1U;
    } else {
      chartInstance->c1_tp_Stall = 0U;
    }

    chartInstance->c1_doSetSimStateSideEffects = 0U;
  }
}

static void finalize_c1_CRVLAB_ECU_104(SFc1_CRVLAB_ECU_104InstanceStruct
  *chartInstance)
{
  sf_mex_destroy(&chartInstance->c1_setSimStateSideEffectsInfo);
}

static void sf_c1_CRVLAB_ECU_104(SFc1_CRVLAB_ECU_104InstanceStruct
  *chartInstance)
{
  real_T *c1_EngineSpeed;
  boolean_T *c1_DDF_On;
  real_T *c1_Pedal;
  real_T *c1_State;
  real_T *c1_PedalHigh;
  real_T *c1_PedalLow;
  real_T *c1_EngineSpeedIdle;
  real_T *c1_ddf2diesel_rpm;
  real_T *c1_diesel2ddf_rpm;
  real_T *c1_Stall_rpm;
  boolean_T *c1_StallLock_On;
  boolean_T *c1_IdleLock_On;
  real_T *c1_EngineSpeedCrankingToIdle;
  c1_EngineSpeedCrankingToIdle = (real_T *)ssGetInputPortSignal(chartInstance->S,
    11);
  c1_IdleLock_On = (boolean_T *)ssGetInputPortSignal(chartInstance->S, 10);
  c1_StallLock_On = (boolean_T *)ssGetInputPortSignal(chartInstance->S, 9);
  c1_Stall_rpm = (real_T *)ssGetInputPortSignal(chartInstance->S, 8);
  c1_diesel2ddf_rpm = (real_T *)ssGetInputPortSignal(chartInstance->S, 7);
  c1_ddf2diesel_rpm = (real_T *)ssGetInputPortSignal(chartInstance->S, 6);
  c1_EngineSpeedIdle = (real_T *)ssGetInputPortSignal(chartInstance->S, 5);
  c1_PedalLow = (real_T *)ssGetInputPortSignal(chartInstance->S, 4);
  c1_PedalHigh = (real_T *)ssGetInputPortSignal(chartInstance->S, 3);
  c1_State = (real_T *)ssGetOutputPortSignal(chartInstance->S, 1);
  c1_Pedal = (real_T *)ssGetInputPortSignal(chartInstance->S, 2);
  c1_DDF_On = (boolean_T *)ssGetInputPortSignal(chartInstance->S, 1);
  c1_EngineSpeed = (real_T *)ssGetInputPortSignal(chartInstance->S, 0);
  c1_set_sim_state_side_effects_c1_CRVLAB_ECU_104(chartInstance);
  _sfTime_ = (real_T)ssGetT(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
  _SFD_DATA_RANGE_CHECK(*c1_EngineSpeed, 0U);
  _SFD_DATA_RANGE_CHECK((real_T)*c1_DDF_On, 1U);
  _SFD_DATA_RANGE_CHECK(*c1_Pedal, 2U);
  _SFD_DATA_RANGE_CHECK(*c1_State, 3U);
  _SFD_DATA_RANGE_CHECK(*c1_PedalHigh, 4U);
  _SFD_DATA_RANGE_CHECK(*c1_PedalLow, 5U);
  _SFD_DATA_RANGE_CHECK(*c1_EngineSpeedIdle, 6U);
  _SFD_DATA_RANGE_CHECK(*c1_ddf2diesel_rpm, 7U);
  _SFD_DATA_RANGE_CHECK(*c1_diesel2ddf_rpm, 8U);
  _SFD_DATA_RANGE_CHECK(*c1_Stall_rpm, 9U);
  _SFD_DATA_RANGE_CHECK((real_T)*c1_StallLock_On, 10U);
  _SFD_DATA_RANGE_CHECK((real_T)*c1_IdleLock_On, 11U);
  _SFD_DATA_RANGE_CHECK(*c1_EngineSpeedCrankingToIdle, 12U);
  chartInstance->c1_sfEvent = CALL_EVENT;
  c1_chartstep_c1_CRVLAB_ECU_104(chartInstance);
  sf_debug_check_for_state_inconsistency(_CRVLAB_ECU_104MachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
}

static void c1_chartstep_c1_CRVLAB_ECU_104(SFc1_CRVLAB_ECU_104InstanceStruct
  *chartInstance)
{
  boolean_T c1_temp;
  boolean_T c1_b_temp;
  boolean_T c1_c_temp;
  boolean_T c1_d_temp;
  boolean_T c1_e_temp;
  boolean_T c1_f_temp;
  boolean_T c1_g_temp;
  boolean_T c1_h_temp;
  real_T *c1_Pedal;
  real_T *c1_PedalHigh;
  boolean_T *c1_IdleLock_On;
  real_T *c1_EngineSpeed;
  real_T *c1_Stall_rpm;
  boolean_T *c1_StallLock_On;
  real_T *c1_EngineSpeedIdle;
  real_T *c1_State;
  real_T *c1_PedalLow;
  boolean_T *c1_DDF_On;
  real_T *c1_ddf2diesel_rpm;
  real_T *c1_diesel2ddf_rpm;
  c1_IdleLock_On = (boolean_T *)ssGetInputPortSignal(chartInstance->S, 10);
  c1_StallLock_On = (boolean_T *)ssGetInputPortSignal(chartInstance->S, 9);
  c1_Stall_rpm = (real_T *)ssGetInputPortSignal(chartInstance->S, 8);
  c1_diesel2ddf_rpm = (real_T *)ssGetInputPortSignal(chartInstance->S, 7);
  c1_ddf2diesel_rpm = (real_T *)ssGetInputPortSignal(chartInstance->S, 6);
  c1_EngineSpeedIdle = (real_T *)ssGetInputPortSignal(chartInstance->S, 5);
  c1_PedalLow = (real_T *)ssGetInputPortSignal(chartInstance->S, 4);
  c1_PedalHigh = (real_T *)ssGetInputPortSignal(chartInstance->S, 3);
  c1_State = (real_T *)ssGetOutputPortSignal(chartInstance->S, 1);
  c1_Pedal = (real_T *)ssGetInputPortSignal(chartInstance->S, 2);
  c1_DDF_On = (boolean_T *)ssGetInputPortSignal(chartInstance->S, 1);
  c1_EngineSpeed = (real_T *)ssGetInputPortSignal(chartInstance->S, 0);
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
  if ((int16_T)chartInstance->c1_is_active_c1_CRVLAB_ECU_104 == 0) {
    _SFD_CC_CALL(CHART_ENTER_ENTRY_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
    chartInstance->c1_is_active_c1_CRVLAB_ECU_104 = 1U;
    _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
    _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 0U, chartInstance->c1_sfEvent);
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
    chartInstance->c1_is_c1_CRVLAB_ECU_104 = c1_IN_Stall;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 7U, chartInstance->c1_sfEvent);
    chartInstance->c1_tp_Stall = 1U;
    *c1_State = 1.0;
    _SFD_DATA_RANGE_CHECK(*c1_State, 3U);
  } else {
    switch (chartInstance->c1_is_c1_CRVLAB_ECU_104) {
     case c1_IN_Crank:
      CV_CHART_EVAL(0, 0, 1);
      c1_Crank(chartInstance);
      break;

     case c1_IN_Cut:
      CV_CHART_EVAL(0, 0, 2);
      _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U,
                   chartInstance->c1_sfEvent);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 11U,
                   chartInstance->c1_sfEvent);
      c1_temp = (_SFD_CCP_CALL(11U, 0, *c1_Pedal > *c1_PedalHigh != 0U,
                  chartInstance->c1_sfEvent) != 0);
      if (c1_temp) {
        c1_temp = (_SFD_CCP_CALL(11U, 1, (real_T)*c1_IdleLock_On < 0.5 != 0U,
                    chartInstance->c1_sfEvent) != 0);
      }

      if (CV_TRANSITION_EVAL(11U, (int32_T)c1_temp)) {
        if (sf_debug_transition_conflict_check_enabled()) {
          unsigned int transitionList[2];
          unsigned int numTransitions = 1;
          transitionList[0] = 11;
          sf_debug_transition_conflict_check_begin();
          if ((*c1_EngineSpeed < *c1_Stall_rpm) || ((real_T)*c1_StallLock_On >
               0.5)) {
            transitionList[numTransitions] = 12;
            numTransitions++;
          }

          sf_debug_transition_conflict_check_end();
          if (numTransitions > 1) {
            _SFD_TRANSITION_CONFLICT(&(transitionList[0]),numTransitions);
          }
        }

        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 11U, chartInstance->c1_sfEvent);
        c1_exit_internal_Cut(chartInstance);
        chartInstance->c1_tp_Cut = 0U;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c1_sfEvent);
        chartInstance->c1_is_c1_CRVLAB_ECU_104 = c1_IN_Run;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 4U, chartInstance->c1_sfEvent);
        chartInstance->c1_tp_Run = 1U;
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 9U,
                     chartInstance->c1_sfEvent);
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 9U, chartInstance->c1_sfEvent);
        chartInstance->c1_is_Run = c1_IN_RunDiesel;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 6U, chartInstance->c1_sfEvent);
        chartInstance->c1_tp_RunDiesel = 1U;
        *c1_State = 3.0;
        _SFD_DATA_RANGE_CHECK(*c1_State, 3U);
      } else {
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 12U,
                     chartInstance->c1_sfEvent);
        c1_b_temp = (_SFD_CCP_CALL(12U, 0, *c1_EngineSpeed < *c1_Stall_rpm != 0U,
          chartInstance->c1_sfEvent) != 0);
        if (!c1_b_temp) {
          c1_b_temp = (_SFD_CCP_CALL(12U, 1, (real_T)*c1_StallLock_On > 0.5 !=
            0U, chartInstance->c1_sfEvent) != 0);
        }

        if (CV_TRANSITION_EVAL(12U, (int32_T)c1_b_temp)) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 12U, chartInstance->c1_sfEvent);
          c1_exit_internal_Cut(chartInstance);
          chartInstance->c1_tp_Cut = 0U;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c1_sfEvent);
          chartInstance->c1_is_c1_CRVLAB_ECU_104 = c1_IN_Stall;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 7U, chartInstance->c1_sfEvent);
          chartInstance->c1_tp_Stall = 1U;
          *c1_State = 1.0;
          _SFD_DATA_RANGE_CHECK(*c1_State, 3U);
        } else {
          switch (chartInstance->c1_is_Cut) {
           case c1_IN_FuelCut:
            CV_STATE_EVAL(1, 0, 1);
            _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 2U,
                         chartInstance->c1_sfEvent);
            _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 5U,
                         chartInstance->c1_sfEvent);
            c1_c_temp = (_SFD_CCP_CALL(5U, 0, *c1_EngineSpeed <
              *c1_EngineSpeedIdle != 0U, chartInstance->c1_sfEvent) != 0);
            if (!c1_c_temp) {
              c1_c_temp = (_SFD_CCP_CALL(5U, 1, (real_T)*c1_IdleLock_On > 0.5 !=
                0U, chartInstance->c1_sfEvent) != 0);
            }

            if (CV_TRANSITION_EVAL(5U, (int32_T)c1_c_temp)) {
              _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 5U, chartInstance->c1_sfEvent);
              chartInstance->c1_tp_FuelCut = 0U;
              _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c1_sfEvent);
              chartInstance->c1_is_Cut = c1_IN_Idle;
              _SFD_CS_CALL(STATE_ACTIVE_TAG, 3U, chartInstance->c1_sfEvent);
              chartInstance->c1_tp_Idle = 1U;
              *c1_State = 5.0;
              _SFD_DATA_RANGE_CHECK(*c1_State, 3U);
            }

            _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 2U, chartInstance->c1_sfEvent);
            break;

           case c1_IN_Idle:
            CV_STATE_EVAL(1, 0, 2);
            _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 3U,
                         chartInstance->c1_sfEvent);
            _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 6U,
                         chartInstance->c1_sfEvent);
            c1_d_temp = (_SFD_CCP_CALL(6U, 0, *c1_EngineSpeed >=
              *c1_EngineSpeedIdle != 0U, chartInstance->c1_sfEvent) != 0);
            if (c1_d_temp) {
              c1_d_temp = (_SFD_CCP_CALL(6U, 1, (real_T)*c1_IdleLock_On < 0.5 !=
                0U, chartInstance->c1_sfEvent) != 0);
            }

            if (CV_TRANSITION_EVAL(6U, (int32_T)c1_d_temp)) {
              _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 6U, chartInstance->c1_sfEvent);
              chartInstance->c1_tp_Idle = 0U;
              _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, chartInstance->c1_sfEvent);
              chartInstance->c1_is_Cut = c1_IN_FuelCut;
              _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c1_sfEvent);
              chartInstance->c1_tp_FuelCut = 1U;
              *c1_State = 6.0;
              _SFD_DATA_RANGE_CHECK(*c1_State, 3U);
            }

            _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 3U, chartInstance->c1_sfEvent);
            break;

           default:
            CV_STATE_EVAL(1, 0, 0);
            chartInstance->c1_is_Cut = (uint8_T)c1_IN_NO_ACTIVE_CHILD;
            _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c1_sfEvent);
            break;
          }
        }
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c1_sfEvent);
      break;

     case c1_IN_Run:
      CV_CHART_EVAL(0, 0, 3);
      _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 4U,
                   chartInstance->c1_sfEvent);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 8U,
                   chartInstance->c1_sfEvent);
      c1_e_temp = (_SFD_CCP_CALL(8U, 0, *c1_EngineSpeed < *c1_Stall_rpm != 0U,
        chartInstance->c1_sfEvent) != 0);
      if (!c1_e_temp) {
        c1_e_temp = (_SFD_CCP_CALL(8U, 1, (real_T)*c1_StallLock_On > 0.5 != 0U,
          chartInstance->c1_sfEvent) != 0);
      }

      if (CV_TRANSITION_EVAL(8U, (int32_T)c1_e_temp)) {
        if (sf_debug_transition_conflict_check_enabled()) {
          unsigned int transitionList[2];
          unsigned int numTransitions = 1;
          transitionList[0] = 8;
          sf_debug_transition_conflict_check_begin();
          if ((*c1_Pedal < *c1_PedalLow) || ((real_T)*c1_IdleLock_On > 0.5)) {
            transitionList[numTransitions] = 10;
            numTransitions++;
          }

          sf_debug_transition_conflict_check_end();
          if (numTransitions > 1) {
            _SFD_TRANSITION_CONFLICT(&(transitionList[0]),numTransitions);
          }
        }

        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 8U, chartInstance->c1_sfEvent);
        c1_exit_internal_Run(chartInstance);
        chartInstance->c1_tp_Run = 0U;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 4U, chartInstance->c1_sfEvent);
        chartInstance->c1_is_c1_CRVLAB_ECU_104 = c1_IN_Stall;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 7U, chartInstance->c1_sfEvent);
        chartInstance->c1_tp_Stall = 1U;
        *c1_State = 1.0;
        _SFD_DATA_RANGE_CHECK(*c1_State, 3U);
      } else {
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 10U,
                     chartInstance->c1_sfEvent);
        c1_f_temp = (_SFD_CCP_CALL(10U, 0, *c1_Pedal < *c1_PedalLow != 0U,
          chartInstance->c1_sfEvent) != 0);
        if (!c1_f_temp) {
          c1_f_temp = (_SFD_CCP_CALL(10U, 1, (real_T)*c1_IdleLock_On > 0.5 != 0U,
            chartInstance->c1_sfEvent) != 0);
        }

        if (CV_TRANSITION_EVAL(10U, (int32_T)c1_f_temp)) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 10U, chartInstance->c1_sfEvent);
          c1_exit_internal_Run(chartInstance);
          chartInstance->c1_tp_Run = 0U;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 4U, chartInstance->c1_sfEvent);
          chartInstance->c1_is_c1_CRVLAB_ECU_104 = c1_IN_Cut;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c1_sfEvent);
          chartInstance->c1_tp_Cut = 1U;
          _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 13U,
                       chartInstance->c1_sfEvent);
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 13U, chartInstance->c1_sfEvent);
          chartInstance->c1_is_Cut = c1_IN_Idle;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 3U, chartInstance->c1_sfEvent);
          chartInstance->c1_tp_Idle = 1U;
          *c1_State = 5.0;
          _SFD_DATA_RANGE_CHECK(*c1_State, 3U);
        } else {
          switch (chartInstance->c1_is_Run) {
           case c1_IN_RunDDF:
            CV_STATE_EVAL(4, 0, 1);
            _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 5U,
                         chartInstance->c1_sfEvent);
            _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 4U,
                         chartInstance->c1_sfEvent);
            c1_g_temp = (_SFD_CCP_CALL(4U, 0, (real_T)*c1_DDF_On < 0.5 != 0U,
              chartInstance->c1_sfEvent) != 0);
            if (!c1_g_temp) {
              c1_g_temp = (_SFD_CCP_CALL(4U, 1, *c1_EngineSpeed >
                *c1_ddf2diesel_rpm != 0U, chartInstance->c1_sfEvent) != 0);
            }

            if (CV_TRANSITION_EVAL(4U, (int32_T)c1_g_temp)) {
              _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 4U, chartInstance->c1_sfEvent);
              chartInstance->c1_tp_RunDDF = 0U;
              _SFD_CS_CALL(STATE_INACTIVE_TAG, 5U, chartInstance->c1_sfEvent);
              chartInstance->c1_is_Run = c1_IN_RunDiesel;
              _SFD_CS_CALL(STATE_ACTIVE_TAG, 6U, chartInstance->c1_sfEvent);
              chartInstance->c1_tp_RunDiesel = 1U;
              *c1_State = 3.0;
              _SFD_DATA_RANGE_CHECK(*c1_State, 3U);
            }

            _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 5U, chartInstance->c1_sfEvent);
            break;

           case c1_IN_RunDiesel:
            CV_STATE_EVAL(4, 0, 2);
            _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 6U,
                         chartInstance->c1_sfEvent);
            _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 3U,
                         chartInstance->c1_sfEvent);
            c1_h_temp = (_SFD_CCP_CALL(3U, 0, (real_T)*c1_DDF_On >= 0.5 != 0U,
              chartInstance->c1_sfEvent) != 0);
            if (c1_h_temp) {
              c1_h_temp = (_SFD_CCP_CALL(3U, 1, *c1_EngineSpeed <=
                *c1_diesel2ddf_rpm != 0U, chartInstance->c1_sfEvent) != 0);
            }

            if (CV_TRANSITION_EVAL(3U, (int32_T)c1_h_temp)) {
              _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 3U, chartInstance->c1_sfEvent);
              chartInstance->c1_tp_RunDiesel = 0U;
              _SFD_CS_CALL(STATE_INACTIVE_TAG, 6U, chartInstance->c1_sfEvent);
              chartInstance->c1_is_Run = c1_IN_RunDDF;
              _SFD_CS_CALL(STATE_ACTIVE_TAG, 5U, chartInstance->c1_sfEvent);
              chartInstance->c1_tp_RunDDF = 1U;
              *c1_State = 4.0;
              _SFD_DATA_RANGE_CHECK(*c1_State, 3U);
            }

            _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 6U, chartInstance->c1_sfEvent);
            break;

           default:
            CV_STATE_EVAL(4, 0, 0);
            chartInstance->c1_is_Run = (uint8_T)c1_IN_NO_ACTIVE_CHILD;
            _SFD_CS_CALL(STATE_INACTIVE_TAG, 5U, chartInstance->c1_sfEvent);
            break;
          }
        }
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 4U, chartInstance->c1_sfEvent);
      break;

     case c1_IN_Stall:
      CV_CHART_EVAL(0, 0, 4);
      _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 7U,
                   chartInstance->c1_sfEvent);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 14U,
                   chartInstance->c1_sfEvent);
      if (CV_TRANSITION_EVAL(14U, (int32_T)_SFD_CCP_CALL(14U, 0, (real_T)
            *c1_StallLock_On > 0.5 != 0U, chartInstance->c1_sfEvent))) {
        if (sf_debug_transition_conflict_check_enabled()) {
          unsigned int transitionList[2];
          unsigned int numTransitions = 1;
          transitionList[0] = 14;
          sf_debug_transition_conflict_check_begin();
          if (*c1_EngineSpeed > *c1_Stall_rpm) {
            transitionList[numTransitions] = 1;
            numTransitions++;
          }

          sf_debug_transition_conflict_check_end();
          if (numTransitions > 1) {
            _SFD_TRANSITION_CONFLICT(&(transitionList[0]),numTransitions);
          }
        }

        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 14U, chartInstance->c1_sfEvent);
        chartInstance->c1_tp_Stall = 0U;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 7U, chartInstance->c1_sfEvent);
        chartInstance->c1_is_c1_CRVLAB_ECU_104 = c1_IN_Stall;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 7U, chartInstance->c1_sfEvent);
        chartInstance->c1_tp_Stall = 1U;
        *c1_State = 1.0;
        _SFD_DATA_RANGE_CHECK(*c1_State, 3U);
      } else {
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 1U,
                     chartInstance->c1_sfEvent);
        if (CV_TRANSITION_EVAL(1U, (int32_T)_SFD_CCP_CALL(1U, 0, *c1_EngineSpeed
              > *c1_Stall_rpm != 0U, chartInstance->c1_sfEvent))) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 1U, chartInstance->c1_sfEvent);
          chartInstance->c1_tp_Stall = 0U;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 7U, chartInstance->c1_sfEvent);
          chartInstance->c1_is_c1_CRVLAB_ECU_104 = c1_IN_Crank;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
          chartInstance->c1_tp_Crank = 1U;
          *c1_State = 2.0;
          _SFD_DATA_RANGE_CHECK(*c1_State, 3U);
        }
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 7U, chartInstance->c1_sfEvent);
      break;

     default:
      CV_CHART_EVAL(0, 0, 0);
      chartInstance->c1_is_c1_CRVLAB_ECU_104 = (uint8_T)c1_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
      break;
    }
  }

  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
}

static void initSimStructsc1_CRVLAB_ECU_104(SFc1_CRVLAB_ECU_104InstanceStruct
  *chartInstance)
{
}

static void c1_Crank(SFc1_CRVLAB_ECU_104InstanceStruct *chartInstance)
{
  boolean_T c1_temp;
  real_T *c1_EngineSpeed;
  real_T *c1_Stall_rpm;
  boolean_T *c1_StallLock_On;
  real_T *c1_EngineSpeedCrankingToIdle;
  real_T *c1_State;
  c1_EngineSpeedCrankingToIdle = (real_T *)ssGetInputPortSignal(chartInstance->S,
    11);
  c1_StallLock_On = (boolean_T *)ssGetInputPortSignal(chartInstance->S, 9);
  c1_Stall_rpm = (real_T *)ssGetInputPortSignal(chartInstance->S, 8);
  c1_State = (real_T *)ssGetOutputPortSignal(chartInstance->S, 1);
  c1_EngineSpeed = (real_T *)ssGetInputPortSignal(chartInstance->S, 0);
  _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
  _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 2U, chartInstance->c1_sfEvent);
  c1_temp = (_SFD_CCP_CALL(2U, 0, *c1_EngineSpeed < *c1_Stall_rpm != 0U,
              chartInstance->c1_sfEvent) != 0);
  if (!c1_temp) {
    c1_temp = (_SFD_CCP_CALL(2U, 1, (real_T)*c1_StallLock_On > 0.5 != 0U,
                chartInstance->c1_sfEvent) != 0);
  }

  if (CV_TRANSITION_EVAL(2U, (int32_T)c1_temp)) {
    if (sf_debug_transition_conflict_check_enabled()) {
      unsigned int transitionList[2];
      unsigned int numTransitions = 1;
      transitionList[0] = 2;
      sf_debug_transition_conflict_check_begin();
      if (*c1_EngineSpeed > *c1_EngineSpeedCrankingToIdle) {
        transitionList[numTransitions] = 7;
        numTransitions++;
      }

      sf_debug_transition_conflict_check_end();
      if (numTransitions > 1) {
        _SFD_TRANSITION_CONFLICT(&(transitionList[0]),numTransitions);
      }
    }

    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 2U, chartInstance->c1_sfEvent);
    chartInstance->c1_tp_Crank = 0U;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
    chartInstance->c1_is_c1_CRVLAB_ECU_104 = c1_IN_Stall;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 7U, chartInstance->c1_sfEvent);
    chartInstance->c1_tp_Stall = 1U;
    *c1_State = 1.0;
    _SFD_DATA_RANGE_CHECK(*c1_State, 3U);
  } else {
    _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 7U, chartInstance->c1_sfEvent);
    if (CV_TRANSITION_EVAL(7U, (int32_T)_SFD_CCP_CALL(7U, 0, *c1_EngineSpeed >
          *c1_EngineSpeedCrankingToIdle != 0U, chartInstance->c1_sfEvent))) {
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 7U, chartInstance->c1_sfEvent);
      chartInstance->c1_tp_Crank = 0U;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
      chartInstance->c1_is_c1_CRVLAB_ECU_104 = c1_IN_Run;
      _SFD_CS_CALL(STATE_ACTIVE_TAG, 4U, chartInstance->c1_sfEvent);
      chartInstance->c1_tp_Run = 1U;
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 9U,
                   chartInstance->c1_sfEvent);
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 9U, chartInstance->c1_sfEvent);
      chartInstance->c1_is_Run = c1_IN_RunDiesel;
      _SFD_CS_CALL(STATE_ACTIVE_TAG, 6U, chartInstance->c1_sfEvent);
      chartInstance->c1_tp_RunDiesel = 1U;
      *c1_State = 3.0;
      _SFD_DATA_RANGE_CHECK(*c1_State, 3U);
    }
  }

  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
}

static void c1_exit_internal_Run(SFc1_CRVLAB_ECU_104InstanceStruct
  *chartInstance)
{
  switch (chartInstance->c1_is_Run) {
   case c1_IN_RunDDF:
    CV_STATE_EVAL(4, 1, 1);
    chartInstance->c1_tp_RunDDF = 0U;
    chartInstance->c1_is_Run = (uint8_T)c1_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 5U, chartInstance->c1_sfEvent);
    break;

   case c1_IN_RunDiesel:
    CV_STATE_EVAL(4, 1, 2);
    chartInstance->c1_tp_RunDiesel = 0U;
    chartInstance->c1_is_Run = (uint8_T)c1_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 6U, chartInstance->c1_sfEvent);
    break;

   default:
    CV_STATE_EVAL(4, 1, 0);
    chartInstance->c1_is_Run = (uint8_T)c1_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 5U, chartInstance->c1_sfEvent);
    break;
  }
}

static void c1_exit_internal_Cut(SFc1_CRVLAB_ECU_104InstanceStruct
  *chartInstance)
{
  switch (chartInstance->c1_is_Cut) {
   case c1_IN_FuelCut:
    CV_STATE_EVAL(1, 1, 1);
    chartInstance->c1_tp_FuelCut = 0U;
    chartInstance->c1_is_Cut = (uint8_T)c1_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c1_sfEvent);
    break;

   case c1_IN_Idle:
    CV_STATE_EVAL(1, 1, 2);
    chartInstance->c1_tp_Idle = 0U;
    chartInstance->c1_is_Cut = (uint8_T)c1_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, chartInstance->c1_sfEvent);
    break;

   default:
    CV_STATE_EVAL(1, 1, 0);
    chartInstance->c1_is_Cut = (uint8_T)c1_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c1_sfEvent);
    break;
  }
}

static void init_script_number_translation(uint32_T c1_machineNumber, uint32_T
  c1_chartNumber)
{
}

const mxArray *sf_c1_CRVLAB_ECU_104_get_eml_resolved_functions_info(void)
{
  const mxArray *c1_nameCaptureInfo = NULL;
  c1_nameCaptureInfo = NULL;
  sf_mex_assign(&c1_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1));
  return c1_nameCaptureInfo;
}

static const mxArray *c1_sf_marshallOut(void *chartInstanceVoid, void *c1_inData)
{
  const mxArray *c1_mxArrayOutData = NULL;
  int32_T c1_u;
  const mxArray *c1_y = NULL;
  SFc1_CRVLAB_ECU_104InstanceStruct *chartInstance;
  chartInstance = (SFc1_CRVLAB_ECU_104InstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_u = *(int32_T *)c1_inData;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", &c1_u, 6, 0U, 0U, 0U, 0));
  sf_mex_assign(&c1_mxArrayOutData, c1_y);
  return c1_mxArrayOutData;
}

static int32_T c1_emlrt_marshallIn(SFc1_CRVLAB_ECU_104InstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId)
{
  int32_T c1_y;
  int32_T c1_i0;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_u), &c1_i0, 1, 6, 0U, 0, 0U, 0);
  c1_y = c1_i0;
  sf_mex_destroy(&c1_u);
  return c1_y;
}

static void c1_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  const mxArray *c1_b_sfEvent;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  int32_T c1_y;
  SFc1_CRVLAB_ECU_104InstanceStruct *chartInstance;
  chartInstance = (SFc1_CRVLAB_ECU_104InstanceStruct *)chartInstanceVoid;
  c1_b_sfEvent = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  c1_y = c1_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_sfEvent), &c1_thisId);
  sf_mex_destroy(&c1_b_sfEvent);
  *(int32_T *)c1_outData = c1_y;
  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_b_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData = NULL;
  uint8_T c1_u;
  const mxArray *c1_y = NULL;
  SFc1_CRVLAB_ECU_104InstanceStruct *chartInstance;
  chartInstance = (SFc1_CRVLAB_ECU_104InstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_u = *(uint8_T *)c1_inData;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", &c1_u, 3, 0U, 0U, 0U, 0));
  sf_mex_assign(&c1_mxArrayOutData, c1_y);
  return c1_mxArrayOutData;
}

static uint8_T c1_b_emlrt_marshallIn(SFc1_CRVLAB_ECU_104InstanceStruct
  *chartInstance, const mxArray *c1_b_tp_RunDiesel, const char_T *c1_identifier)
{
  uint8_T c1_y;
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  c1_y = c1_c_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_tp_RunDiesel),
    &c1_thisId);
  sf_mex_destroy(&c1_b_tp_RunDiesel);
  return c1_y;
}

static uint8_T c1_c_emlrt_marshallIn(SFc1_CRVLAB_ECU_104InstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId)
{
  uint8_T c1_y;
  uint8_T c1_u0;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_u), &c1_u0, 1, 3, 0U, 0, 0U, 0);
  c1_y = c1_u0;
  sf_mex_destroy(&c1_u);
  return c1_y;
}

static void c1_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  const mxArray *c1_b_tp_RunDiesel;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  uint8_T c1_y;
  SFc1_CRVLAB_ECU_104InstanceStruct *chartInstance;
  chartInstance = (SFc1_CRVLAB_ECU_104InstanceStruct *)chartInstanceVoid;
  c1_b_tp_RunDiesel = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  c1_y = c1_c_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_tp_RunDiesel),
    &c1_thisId);
  sf_mex_destroy(&c1_b_tp_RunDiesel);
  *(uint8_T *)c1_outData = c1_y;
  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_c_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData = NULL;
  real_T c1_u;
  const mxArray *c1_y = NULL;
  SFc1_CRVLAB_ECU_104InstanceStruct *chartInstance;
  chartInstance = (SFc1_CRVLAB_ECU_104InstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_u = *(real_T *)c1_inData;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", &c1_u, 0, 0U, 0U, 0U, 0));
  sf_mex_assign(&c1_mxArrayOutData, c1_y);
  return c1_mxArrayOutData;
}

static const mxArray *c1_d_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData = NULL;
  boolean_T c1_u;
  const mxArray *c1_y = NULL;
  SFc1_CRVLAB_ECU_104InstanceStruct *chartInstance;
  chartInstance = (SFc1_CRVLAB_ECU_104InstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_u = *(boolean_T *)c1_inData;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", &c1_u, 11, 0U, 0U, 0U, 0));
  sf_mex_assign(&c1_mxArrayOutData, c1_y);
  return c1_mxArrayOutData;
}

static real_T c1_d_emlrt_marshallIn(SFc1_CRVLAB_ECU_104InstanceStruct
  *chartInstance, const mxArray *c1_State, const char_T *c1_identifier)
{
  real_T c1_y;
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  c1_y = c1_e_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_State), &c1_thisId);
  sf_mex_destroy(&c1_State);
  return c1_y;
}

static real_T c1_e_emlrt_marshallIn(SFc1_CRVLAB_ECU_104InstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId)
{
  real_T c1_y;
  real_T c1_d0;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_u), &c1_d0, 1, 0, 0U, 0, 0U, 0);
  c1_y = c1_d0;
  sf_mex_destroy(&c1_u);
  return c1_y;
}

static void c1_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  const mxArray *c1_State;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  real_T c1_y;
  SFc1_CRVLAB_ECU_104InstanceStruct *chartInstance;
  chartInstance = (SFc1_CRVLAB_ECU_104InstanceStruct *)chartInstanceVoid;
  c1_State = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  c1_y = c1_e_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_State), &c1_thisId);
  sf_mex_destroy(&c1_State);
  *(real_T *)c1_outData = c1_y;
  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_f_emlrt_marshallIn(SFc1_CRVLAB_ECU_104InstanceStruct
  *chartInstance, const mxArray *c1_b_setSimStateSideEffectsInfo, const char_T
  *c1_identifier)
{
  const mxArray *c1_y = NULL;
  emlrtMsgIdentifier c1_thisId;
  c1_y = NULL;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  sf_mex_assign(&c1_y, c1_g_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c1_b_setSimStateSideEffectsInfo), &c1_thisId));
  sf_mex_destroy(&c1_b_setSimStateSideEffectsInfo);
  return c1_y;
}

static const mxArray *c1_g_emlrt_marshallIn(SFc1_CRVLAB_ECU_104InstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId)
{
  const mxArray *c1_y = NULL;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_duplicatearraysafe(&c1_u));
  sf_mex_destroy(&c1_u);
  return c1_y;
}

static void init_dsm_address_info(SFc1_CRVLAB_ECU_104InstanceStruct
  *chartInstance)
{
}

/* SFunction Glue Code */
void sf_c1_CRVLAB_ECU_104_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(399945969U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(256644001U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(1925910203U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(900038592U);
}

mxArray *sf_c1_CRVLAB_ECU_104_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1,1,5,
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateDoubleMatrix(4,1,mxREAL);
    double *pr = mxGetPr(mxChecksum);
    pr[0] = (double)(88188080U);
    pr[1] = (double)(1628819080U);
    pr[2] = (double)(113195372U);
    pr[3] = (double)(849646735U);
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,12,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(1));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,3,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,3,"type",mxType);
    }

    mxSetField(mxData,3,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,4,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,4,"type",mxType);
    }

    mxSetField(mxData,4,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,5,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,5,"type",mxType);
    }

    mxSetField(mxData,5,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,6,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,6,"type",mxType);
    }

    mxSetField(mxData,6,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,7,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,7,"type",mxType);
    }

    mxSetField(mxData,7,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,8,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,8,"type",mxType);
    }

    mxSetField(mxData,8,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,9,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(1));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,9,"type",mxType);
    }

    mxSetField(mxData,9,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,10,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(1));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,10,"type",mxType);
    }

    mxSetField(mxData,10,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,11,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,11,"type",mxType);
    }

    mxSetField(mxData,11,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  return(mxAutoinheritanceInfo);
}

static const mxArray *sf_get_sim_state_info_c1_CRVLAB_ECU_104(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x5'type','srcId','name','auxInfo'{{M[1],M[76],T\"State\",},{M[8],M[0],T\"is_active_c1_CRVLAB_ECU_104\",},{M[9],M[0],T\"is_c1_CRVLAB_ECU_104\",},{M[9],M[98],T\"is_Run\",},{M[9],M[103],T\"is_Cut\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 5, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c1_CRVLAB_ECU_104_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc1_CRVLAB_ECU_104InstanceStruct *chartInstance;
    chartInstance = (SFc1_CRVLAB_ECU_104InstanceStruct *) ((ChartInfoStruct *)
      (ssGetUserData(S)))->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (_CRVLAB_ECU_104MachineNumber_,
           1,
           8,
           15,
           13,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           ssGetPath(S),
           (void *)S);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          init_script_number_translation(_CRVLAB_ECU_104MachineNumber_,
            chartInstance->chartNumber);
          sf_debug_set_chart_disable_implicit_casting
            (_CRVLAB_ECU_104MachineNumber_,chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(_CRVLAB_ECU_104MachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,1,1,0,"EngineSpeed");
          _SFD_SET_DATA_PROPS(1,1,1,0,"DDF_On");
          _SFD_SET_DATA_PROPS(2,1,1,0,"Pedal");
          _SFD_SET_DATA_PROPS(3,2,0,1,"State");
          _SFD_SET_DATA_PROPS(4,1,1,0,"PedalHigh");
          _SFD_SET_DATA_PROPS(5,1,1,0,"PedalLow");
          _SFD_SET_DATA_PROPS(6,1,1,0,"EngineSpeedIdle");
          _SFD_SET_DATA_PROPS(7,1,1,0,"ddf2diesel_rpm");
          _SFD_SET_DATA_PROPS(8,1,1,0,"diesel2ddf_rpm");
          _SFD_SET_DATA_PROPS(9,1,1,0,"Stall_rpm");
          _SFD_SET_DATA_PROPS(10,1,1,0,"StallLock_On");
          _SFD_SET_DATA_PROPS(11,1,1,0,"IdleLock_On");
          _SFD_SET_DATA_PROPS(12,1,1,0,"EngineSpeedCrankingToIdle");
          _SFD_STATE_INFO(0,0,0);
          _SFD_STATE_INFO(1,0,0);
          _SFD_STATE_INFO(2,0,0);
          _SFD_STATE_INFO(3,0,0);
          _SFD_STATE_INFO(4,0,0);
          _SFD_STATE_INFO(5,0,0);
          _SFD_STATE_INFO(6,0,0);
          _SFD_STATE_INFO(7,0,0);
          _SFD_CH_SUBSTATE_COUNT(4);
          _SFD_CH_SUBSTATE_DECOMP(0);
          _SFD_CH_SUBSTATE_INDEX(0,0);
          _SFD_CH_SUBSTATE_INDEX(1,1);
          _SFD_CH_SUBSTATE_INDEX(2,4);
          _SFD_CH_SUBSTATE_INDEX(3,7);
          _SFD_ST_SUBSTATE_COUNT(0,0);
          _SFD_ST_SUBSTATE_COUNT(1,2);
          _SFD_ST_SUBSTATE_INDEX(1,0,2);
          _SFD_ST_SUBSTATE_INDEX(1,1,3);
          _SFD_ST_SUBSTATE_COUNT(2,0);
          _SFD_ST_SUBSTATE_COUNT(3,0);
          _SFD_ST_SUBSTATE_COUNT(4,2);
          _SFD_ST_SUBSTATE_INDEX(4,0,5);
          _SFD_ST_SUBSTATE_INDEX(4,1,6);
          _SFD_ST_SUBSTATE_COUNT(5,0);
          _SFD_ST_SUBSTATE_COUNT(6,0);
          _SFD_ST_SUBSTATE_COUNT(7,0);
        }

        _SFD_CV_INIT_CHART(4,1,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(1,2,1,1,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(2,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(3,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(4,2,1,1,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(5,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(6,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(7,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        {
          static unsigned int sStartGuardMap[] = { 1 };

          static unsigned int sEndGuardMap[] = { 22 };

          static int sPostFixPredicateTree[] = { 0 };

          _SFD_CV_INIT_TRANS(1,1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),1,
                             &(sPostFixPredicateTree[0]));
        }

        {
          static unsigned int sStartGuardMap[] = { 2, 27 };

          static unsigned int sEndGuardMap[] = { 23, 43 };

          static int sPostFixPredicateTree[] = { 0, 1, -2 };

          _SFD_CV_INIT_TRANS(2,2,&(sStartGuardMap[0]),&(sEndGuardMap[0]),3,
                             &(sPostFixPredicateTree[0]));
        }

        {
          static unsigned int sStartGuardMap[] = { 2, 34 };

          static unsigned int sEndGuardMap[] = { 30, 49 };

          static int sPostFixPredicateTree[] = { 0, 1, -3 };

          _SFD_CV_INIT_TRANS(6,2,&(sStartGuardMap[0]),&(sEndGuardMap[0]),3,
                             &(sPostFixPredicateTree[0]));
        }

        {
          static unsigned int sStartGuardMap[] = { 2, 17 };

          static unsigned int sEndGuardMap[] = { 13, 44 };

          static int sPostFixPredicateTree[] = { 0, 1, -3 };

          _SFD_CV_INIT_TRANS(3,2,&(sStartGuardMap[0]),&(sEndGuardMap[0]),3,
                             &(sPostFixPredicateTree[0]));
        }

        {
          static unsigned int sStartGuardMap[] = { 2, 16 };

          static unsigned int sEndGuardMap[] = { 12, 42 };

          static int sPostFixPredicateTree[] = { 0, 1, -2 };

          _SFD_CV_INIT_TRANS(4,2,&(sStartGuardMap[0]),&(sEndGuardMap[0]),3,
                             &(sPostFixPredicateTree[0]));
        }

        {
          static unsigned int sStartGuardMap[] = { 2, 33 };

          static unsigned int sEndGuardMap[] = { 29, 48 };

          static int sPostFixPredicateTree[] = { 0, 1, -2 };

          _SFD_CV_INIT_TRANS(5,2,&(sStartGuardMap[0]),&(sEndGuardMap[0]),3,
                             &(sPostFixPredicateTree[0]));
        }

        {
          static unsigned int sStartGuardMap[] = { 1 };

          static unsigned int sEndGuardMap[] = { 38 };

          static int sPostFixPredicateTree[] = { 0 };

          _SFD_CV_INIT_TRANS(7,1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),1,
                             &(sPostFixPredicateTree[0]));
        }

        {
          static unsigned int sStartGuardMap[] = { 2, 27 };

          static unsigned int sEndGuardMap[] = { 23, 43 };

          static int sPostFixPredicateTree[] = { 0, 1, -2 };

          _SFD_CV_INIT_TRANS(8,2,&(sStartGuardMap[0]),&(sEndGuardMap[0]),3,
                             &(sPostFixPredicateTree[0]));
        }

        _SFD_CV_INIT_TRANS(9,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(13,0,NULL,NULL,0,NULL);

        {
          static unsigned int sStartGuardMap[] = { 2, 20 };

          static unsigned int sEndGuardMap[] = { 16, 35 };

          static int sPostFixPredicateTree[] = { 0, 1, -2 };

          _SFD_CV_INIT_TRANS(10,2,&(sStartGuardMap[0]),&(sEndGuardMap[0]),3,
                             &(sPostFixPredicateTree[0]));
        }

        {
          static unsigned int sStartGuardMap[] = { 2, 21 };

          static unsigned int sEndGuardMap[] = { 17, 36 };

          static int sPostFixPredicateTree[] = { 0, 1, -3 };

          _SFD_CV_INIT_TRANS(11,2,&(sStartGuardMap[0]),&(sEndGuardMap[0]),3,
                             &(sPostFixPredicateTree[0]));
        }

        {
          static unsigned int sStartGuardMap[] = { 2, 27 };

          static unsigned int sEndGuardMap[] = { 23, 43 };

          static int sPostFixPredicateTree[] = { 0, 1, -2 };

          _SFD_CV_INIT_TRANS(12,2,&(sStartGuardMap[0]),&(sEndGuardMap[0]),3,
                             &(sPostFixPredicateTree[0]));
        }

        {
          static unsigned int sStartGuardMap[] = { 1 };

          static unsigned int sEndGuardMap[] = { 17 };

          static int sPostFixPredicateTree[] = { 0 };

          _SFD_CV_INIT_TRANS(14,1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),1,
                             &(sPostFixPredicateTree[0]));
        }

        _SFD_TRANS_COV_WTS(0,0,0,0,0);
        if (chartAlreadyPresent==0) {
          _SFD_TRANS_COV_MAPS(0,
                              0,NULL,NULL,
                              0,NULL,NULL,
                              0,NULL,NULL,
                              0,NULL,NULL);
        }

        _SFD_TRANS_COV_WTS(1,0,1,0,0);
        if (chartAlreadyPresent==0) {
          static unsigned int sStartGuardMap[] = { 1 };

          static unsigned int sEndGuardMap[] = { 22 };

          _SFD_TRANS_COV_MAPS(1,
                              0,NULL,NULL,
                              1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),
                              0,NULL,NULL,
                              0,NULL,NULL);
        }

        _SFD_TRANS_COV_WTS(2,0,2,0,0);
        if (chartAlreadyPresent==0) {
          static unsigned int sStartGuardMap[] = { 2, 27 };

          static unsigned int sEndGuardMap[] = { 23, 43 };

          _SFD_TRANS_COV_MAPS(2,
                              0,NULL,NULL,
                              2,&(sStartGuardMap[0]),&(sEndGuardMap[0]),
                              0,NULL,NULL,
                              0,NULL,NULL);
        }

        _SFD_TRANS_COV_WTS(6,0,2,0,0);
        if (chartAlreadyPresent==0) {
          static unsigned int sStartGuardMap[] = { 2, 34 };

          static unsigned int sEndGuardMap[] = { 30, 49 };

          _SFD_TRANS_COV_MAPS(6,
                              0,NULL,NULL,
                              2,&(sStartGuardMap[0]),&(sEndGuardMap[0]),
                              0,NULL,NULL,
                              0,NULL,NULL);
        }

        _SFD_TRANS_COV_WTS(3,0,2,0,0);
        if (chartAlreadyPresent==0) {
          static unsigned int sStartGuardMap[] = { 2, 17 };

          static unsigned int sEndGuardMap[] = { 13, 44 };

          _SFD_TRANS_COV_MAPS(3,
                              0,NULL,NULL,
                              2,&(sStartGuardMap[0]),&(sEndGuardMap[0]),
                              0,NULL,NULL,
                              0,NULL,NULL);
        }

        _SFD_TRANS_COV_WTS(4,0,2,0,0);
        if (chartAlreadyPresent==0) {
          static unsigned int sStartGuardMap[] = { 2, 16 };

          static unsigned int sEndGuardMap[] = { 12, 42 };

          _SFD_TRANS_COV_MAPS(4,
                              0,NULL,NULL,
                              2,&(sStartGuardMap[0]),&(sEndGuardMap[0]),
                              0,NULL,NULL,
                              0,NULL,NULL);
        }

        _SFD_TRANS_COV_WTS(5,0,2,0,0);
        if (chartAlreadyPresent==0) {
          static unsigned int sStartGuardMap[] = { 2, 33 };

          static unsigned int sEndGuardMap[] = { 29, 48 };

          _SFD_TRANS_COV_MAPS(5,
                              0,NULL,NULL,
                              2,&(sStartGuardMap[0]),&(sEndGuardMap[0]),
                              0,NULL,NULL,
                              0,NULL,NULL);
        }

        _SFD_TRANS_COV_WTS(7,0,1,0,0);
        if (chartAlreadyPresent==0) {
          static unsigned int sStartGuardMap[] = { 1 };

          static unsigned int sEndGuardMap[] = { 38 };

          _SFD_TRANS_COV_MAPS(7,
                              0,NULL,NULL,
                              1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),
                              0,NULL,NULL,
                              0,NULL,NULL);
        }

        _SFD_TRANS_COV_WTS(8,0,2,0,0);
        if (chartAlreadyPresent==0) {
          static unsigned int sStartGuardMap[] = { 2, 27 };

          static unsigned int sEndGuardMap[] = { 23, 43 };

          _SFD_TRANS_COV_MAPS(8,
                              0,NULL,NULL,
                              2,&(sStartGuardMap[0]),&(sEndGuardMap[0]),
                              0,NULL,NULL,
                              0,NULL,NULL);
        }

        _SFD_TRANS_COV_WTS(9,0,0,0,0);
        if (chartAlreadyPresent==0) {
          _SFD_TRANS_COV_MAPS(9,
                              0,NULL,NULL,
                              0,NULL,NULL,
                              0,NULL,NULL,
                              0,NULL,NULL);
        }

        _SFD_TRANS_COV_WTS(13,0,0,0,0);
        if (chartAlreadyPresent==0) {
          _SFD_TRANS_COV_MAPS(13,
                              0,NULL,NULL,
                              0,NULL,NULL,
                              0,NULL,NULL,
                              0,NULL,NULL);
        }

        _SFD_TRANS_COV_WTS(10,0,2,0,0);
        if (chartAlreadyPresent==0) {
          static unsigned int sStartGuardMap[] = { 2, 20 };

          static unsigned int sEndGuardMap[] = { 16, 35 };

          _SFD_TRANS_COV_MAPS(10,
                              0,NULL,NULL,
                              2,&(sStartGuardMap[0]),&(sEndGuardMap[0]),
                              0,NULL,NULL,
                              0,NULL,NULL);
        }

        _SFD_TRANS_COV_WTS(11,0,2,0,0);
        if (chartAlreadyPresent==0) {
          static unsigned int sStartGuardMap[] = { 2, 21 };

          static unsigned int sEndGuardMap[] = { 17, 36 };

          _SFD_TRANS_COV_MAPS(11,
                              0,NULL,NULL,
                              2,&(sStartGuardMap[0]),&(sEndGuardMap[0]),
                              0,NULL,NULL,
                              0,NULL,NULL);
        }

        _SFD_TRANS_COV_WTS(12,0,2,0,0);
        if (chartAlreadyPresent==0) {
          static unsigned int sStartGuardMap[] = { 2, 27 };

          static unsigned int sEndGuardMap[] = { 23, 43 };

          _SFD_TRANS_COV_MAPS(12,
                              0,NULL,NULL,
                              2,&(sStartGuardMap[0]),&(sEndGuardMap[0]),
                              0,NULL,NULL,
                              0,NULL,NULL);
        }

        _SFD_TRANS_COV_WTS(14,0,1,0,0);
        if (chartAlreadyPresent==0) {
          static unsigned int sStartGuardMap[] = { 1 };

          static unsigned int sEndGuardMap[] = { 17 };

          _SFD_TRANS_COV_MAPS(14,
                              0,NULL,NULL,
                              1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),
                              0,NULL,NULL,
                              0,NULL,NULL);
        }

        _SFD_SET_DATA_COMPILED_PROPS(0,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(1,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_d_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(2,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(3,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(4,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(5,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(6,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(7,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(8,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(9,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(10,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_d_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(11,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_d_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(12,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)NULL);

        {
          real_T *c1_EngineSpeed;
          boolean_T *c1_DDF_On;
          real_T *c1_Pedal;
          real_T *c1_State;
          real_T *c1_PedalHigh;
          real_T *c1_PedalLow;
          real_T *c1_EngineSpeedIdle;
          real_T *c1_ddf2diesel_rpm;
          real_T *c1_diesel2ddf_rpm;
          real_T *c1_Stall_rpm;
          boolean_T *c1_StallLock_On;
          boolean_T *c1_IdleLock_On;
          real_T *c1_EngineSpeedCrankingToIdle;
          c1_EngineSpeedCrankingToIdle = (real_T *)ssGetInputPortSignal
            (chartInstance->S, 11);
          c1_IdleLock_On = (boolean_T *)ssGetInputPortSignal(chartInstance->S,
            10);
          c1_StallLock_On = (boolean_T *)ssGetInputPortSignal(chartInstance->S,
            9);
          c1_Stall_rpm = (real_T *)ssGetInputPortSignal(chartInstance->S, 8);
          c1_diesel2ddf_rpm = (real_T *)ssGetInputPortSignal(chartInstance->S, 7);
          c1_ddf2diesel_rpm = (real_T *)ssGetInputPortSignal(chartInstance->S, 6);
          c1_EngineSpeedIdle = (real_T *)ssGetInputPortSignal(chartInstance->S,
            5);
          c1_PedalLow = (real_T *)ssGetInputPortSignal(chartInstance->S, 4);
          c1_PedalHigh = (real_T *)ssGetInputPortSignal(chartInstance->S, 3);
          c1_State = (real_T *)ssGetOutputPortSignal(chartInstance->S, 1);
          c1_Pedal = (real_T *)ssGetInputPortSignal(chartInstance->S, 2);
          c1_DDF_On = (boolean_T *)ssGetInputPortSignal(chartInstance->S, 1);
          c1_EngineSpeed = (real_T *)ssGetInputPortSignal(chartInstance->S, 0);
          _SFD_SET_DATA_VALUE_PTR(0U, c1_EngineSpeed);
          _SFD_SET_DATA_VALUE_PTR(1U, c1_DDF_On);
          _SFD_SET_DATA_VALUE_PTR(2U, c1_Pedal);
          _SFD_SET_DATA_VALUE_PTR(3U, c1_State);
          _SFD_SET_DATA_VALUE_PTR(4U, c1_PedalHigh);
          _SFD_SET_DATA_VALUE_PTR(5U, c1_PedalLow);
          _SFD_SET_DATA_VALUE_PTR(6U, c1_EngineSpeedIdle);
          _SFD_SET_DATA_VALUE_PTR(7U, c1_ddf2diesel_rpm);
          _SFD_SET_DATA_VALUE_PTR(8U, c1_diesel2ddf_rpm);
          _SFD_SET_DATA_VALUE_PTR(9U, c1_Stall_rpm);
          _SFD_SET_DATA_VALUE_PTR(10U, c1_StallLock_On);
          _SFD_SET_DATA_VALUE_PTR(11U, c1_IdleLock_On);
          _SFD_SET_DATA_VALUE_PTR(12U, c1_EngineSpeedCrankingToIdle);
        }
      }
    } else {
      sf_debug_reset_current_state_configuration(_CRVLAB_ECU_104MachineNumber_,
        chartInstance->chartNumber,chartInstance->instanceNumber);
    }
  }
}

static void sf_opaque_initialize_c1_CRVLAB_ECU_104(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc1_CRVLAB_ECU_104InstanceStruct*)
    chartInstanceVar)->S,0);
  initialize_params_c1_CRVLAB_ECU_104((SFc1_CRVLAB_ECU_104InstanceStruct*)
    chartInstanceVar);
  initialize_c1_CRVLAB_ECU_104((SFc1_CRVLAB_ECU_104InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_enable_c1_CRVLAB_ECU_104(void *chartInstanceVar)
{
  enable_c1_CRVLAB_ECU_104((SFc1_CRVLAB_ECU_104InstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c1_CRVLAB_ECU_104(void *chartInstanceVar)
{
  disable_c1_CRVLAB_ECU_104((SFc1_CRVLAB_ECU_104InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_gateway_c1_CRVLAB_ECU_104(void *chartInstanceVar)
{
  sf_c1_CRVLAB_ECU_104((SFc1_CRVLAB_ECU_104InstanceStruct*) chartInstanceVar);
}

extern const mxArray* sf_internal_get_sim_state_c1_CRVLAB_ECU_104(SimStruct* S)
{
  ChartInfoStruct *chartInfo = (ChartInfoStruct*) ssGetUserData(S);
  mxArray *plhs[1] = { NULL };

  mxArray *prhs[4];
  int mxError = 0;
  prhs[0] = mxCreateString("chart_simctx_raw2high");
  prhs[1] = mxCreateDoubleScalar(ssGetSFuncBlockHandle(S));
  prhs[2] = (mxArray*) get_sim_state_c1_CRVLAB_ECU_104
    ((SFc1_CRVLAB_ECU_104InstanceStruct*)chartInfo->chartInstance);/* raw sim ctx */
  prhs[3] = (mxArray*) sf_get_sim_state_info_c1_CRVLAB_ECU_104();/* state var info */
  mxError = sf_mex_call_matlab(1, plhs, 4, prhs, "sfprivate");
  mxDestroyArray(prhs[0]);
  mxDestroyArray(prhs[1]);
  mxDestroyArray(prhs[2]);
  mxDestroyArray(prhs[3]);
  if (mxError || plhs[0] == NULL) {
    sf_mex_error_message("Stateflow Internal Error: \nError calling 'chart_simctx_raw2high'.\n");
  }

  return plhs[0];
}

extern void sf_internal_set_sim_state_c1_CRVLAB_ECU_104(SimStruct* S, const
  mxArray *st)
{
  ChartInfoStruct *chartInfo = (ChartInfoStruct*) ssGetUserData(S);
  mxArray *plhs[1] = { NULL };

  mxArray *prhs[4];
  int mxError = 0;
  prhs[0] = mxCreateString("chart_simctx_high2raw");
  prhs[1] = mxCreateDoubleScalar(ssGetSFuncBlockHandle(S));
  prhs[2] = mxDuplicateArray(st);      /* high level simctx */
  prhs[3] = (mxArray*) sf_get_sim_state_info_c1_CRVLAB_ECU_104();/* state var info */
  mxError = sf_mex_call_matlab(1, plhs, 4, prhs, "sfprivate");
  mxDestroyArray(prhs[0]);
  mxDestroyArray(prhs[1]);
  mxDestroyArray(prhs[2]);
  mxDestroyArray(prhs[3]);
  if (mxError || plhs[0] == NULL) {
    sf_mex_error_message("Stateflow Internal Error: \nError calling 'chart_simctx_high2raw'.\n");
  }

  set_sim_state_c1_CRVLAB_ECU_104((SFc1_CRVLAB_ECU_104InstanceStruct*)
    chartInfo->chartInstance, mxDuplicateArray(plhs[0]));
  mxDestroyArray(plhs[0]);
}

static const mxArray* sf_opaque_get_sim_state_c1_CRVLAB_ECU_104(SimStruct* S)
{
  return sf_internal_get_sim_state_c1_CRVLAB_ECU_104(S);
}

static void sf_opaque_set_sim_state_c1_CRVLAB_ECU_104(SimStruct* S, const
  mxArray *st)
{
  sf_internal_set_sim_state_c1_CRVLAB_ECU_104(S, st);
}

static void sf_opaque_terminate_c1_CRVLAB_ECU_104(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc1_CRVLAB_ECU_104InstanceStruct*) chartInstanceVar)->S;
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
    }

    finalize_c1_CRVLAB_ECU_104((SFc1_CRVLAB_ECU_104InstanceStruct*)
      chartInstanceVar);
    free((void *)chartInstanceVar);
    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc1_CRVLAB_ECU_104((SFc1_CRVLAB_ECU_104InstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c1_CRVLAB_ECU_104(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    initialize_params_c1_CRVLAB_ECU_104((SFc1_CRVLAB_ECU_104InstanceStruct*)
      (((ChartInfoStruct *)ssGetUserData(S))->chartInstance));
  }
}

static void mdlSetWorkWidths_c1_CRVLAB_ECU_104(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(S,"CRVLAB_ECU_104","CRVLAB_ECU_104",1);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,sf_rtw_info_uint_prop(S,"CRVLAB_ECU_104","CRVLAB_ECU_104",1,
                "RTWCG"));
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop(S,"CRVLAB_ECU_104",
      "CRVLAB_ECU_104",1,"gatewayCannotBeInlinedMultipleTimes"));
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 1, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 2, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 3, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 4, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 5, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 6, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 7, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 8, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 9, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 10, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 11, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,"CRVLAB_ECU_104","CRVLAB_ECU_104",1,
        12);
      sf_mark_chart_reusable_outputs(S,"CRVLAB_ECU_104","CRVLAB_ECU_104",1,1);
    }

    sf_set_rtw_dwork_info(S,"CRVLAB_ECU_104","CRVLAB_ECU_104",1);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(3577497774U));
  ssSetChecksum1(S,(2477369926U));
  ssSetChecksum2(S,(2973789249U));
  ssSetChecksum3(S,(3551374437U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
}

static void mdlRTW_c1_CRVLAB_ECU_104(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    sf_write_symbol_mapping(S, "CRVLAB_ECU_104", "CRVLAB_ECU_104",1);
    ssWriteRTWStrParam(S, "StateflowChartType", "Stateflow");
  }
}

static void mdlStart_c1_CRVLAB_ECU_104(SimStruct *S)
{
  SFc1_CRVLAB_ECU_104InstanceStruct *chartInstance;
  chartInstance = (SFc1_CRVLAB_ECU_104InstanceStruct *)malloc(sizeof
    (SFc1_CRVLAB_ECU_104InstanceStruct));
  memset(chartInstance, 0, sizeof(SFc1_CRVLAB_ECU_104InstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 0;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway =
    sf_opaque_gateway_c1_CRVLAB_ECU_104;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c1_CRVLAB_ECU_104;
  chartInstance->chartInfo.terminateChart =
    sf_opaque_terminate_c1_CRVLAB_ECU_104;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c1_CRVLAB_ECU_104;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c1_CRVLAB_ECU_104;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c1_CRVLAB_ECU_104;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c1_CRVLAB_ECU_104;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c1_CRVLAB_ECU_104;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c1_CRVLAB_ECU_104;
  chartInstance->chartInfo.mdlStart = mdlStart_c1_CRVLAB_ECU_104;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c1_CRVLAB_ECU_104;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->S = S;
  ssSetUserData(S,(void *)(&(chartInstance->chartInfo)));/* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  sf_opaque_init_subchart_simstructs(chartInstance->chartInfo.chartInstance);
  chart_debug_initialization(S,1);
}

void c1_CRVLAB_ECU_104_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c1_CRVLAB_ECU_104(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c1_CRVLAB_ECU_104(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c1_CRVLAB_ECU_104(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c1_CRVLAB_ECU_104_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
