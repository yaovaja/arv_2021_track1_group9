#ifndef __c3_CRVLAB_ECU_Project_h__
#define __c3_CRVLAB_ECU_Project_h__

/* Include files */
#include "sfc_sf.h"
#include "sfc_mex.h"
#include "rtwtypes.h"

/* Type Definitions */
typedef struct {
  SimStruct *S;
  const mxArray *c3_setSimStateSideEffectsInfo;
  int32_T c3_sfEvent;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  boolean_T c3_isStable;
  uint8_T c3_doSetSimStateSideEffects;
  uint8_T c3_is_active_c3_CRVLAB_ECU_Project;
  uint8_T c3_is_c3_CRVLAB_ECU_Project;
  uint8_T c3_tp_cranktoidle;
  uint8_T c3_tp_normal;
  uint8_T c3_tp_stall;
  ChartInfoStruct chartInfo;
} SFc3_CRVLAB_ECU_ProjectInstanceStruct;

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c3_CRVLAB_ECU_Project_get_eml_resolved_functions_info
  (void);

/* Function Definitions */
extern void sf_c3_CRVLAB_ECU_Project_get_check_sum(mxArray *plhs[]);
extern void c3_CRVLAB_ECU_Project_method_dispatcher(SimStruct *S, int_T method,
  void *data);

#endif
