/* Include files */

#include "CRVLAB_ECU_Project_sfun.h"
#include "c1_CRVLAB_ECU_Project.h"
#include "c2_CRVLAB_ECU_Project.h"
#include "c3_CRVLAB_ECU_Project.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */
uint32_T _CRVLAB_ECU_ProjectMachineNumber_;
real_T _sfTime_;

/* Function Declarations */

/* Function Definitions */
void CRVLAB_ECU_Project_initializer(void)
{
}

void CRVLAB_ECU_Project_terminator(void)
{
}

/* SFunction Glue Code */
unsigned int sf_CRVLAB_ECU_Project_method_dispatcher(SimStruct *simstructPtr,
  unsigned int chartFileNumber, const char* specsCksum, int_T method, void *data)
{
  if (chartFileNumber==1) {
    c1_CRVLAB_ECU_Project_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==2) {
    c2_CRVLAB_ECU_Project_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==3) {
    c3_CRVLAB_ECU_Project_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  return 0;
}

unsigned int sf_CRVLAB_ECU_Project_process_check_sum_call( int nlhs, mxArray *
  plhs[], int nrhs, const mxArray * prhs[] )
{

#ifdef MATLAB_MEX_FILE

  char commandName[20];
  if (nrhs<1 || !mxIsChar(prhs[0]) )
    return 0;

  /* Possible call to get the checksum */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"sf_get_check_sum"))
    return 0;
  plhs[0] = mxCreateDoubleMatrix( 1,4,mxREAL);
  if (nrhs>1 && mxIsChar(prhs[1])) {
    mxGetString(prhs[1], commandName,sizeof(commandName)/sizeof(char));
    commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
    if (!strcmp(commandName,"machine")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(2474822712U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(2803212953U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(362412841U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(201618702U);
    } else if (!strcmp(commandName,"exportedFcn")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(0U);
    } else if (!strcmp(commandName,"makefile")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(2961773182U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(3674205283U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(232881659U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(520152848U);
    } else if (nrhs==3 && !strcmp(commandName,"chart")) {
      unsigned int chartFileNumber;
      chartFileNumber = (unsigned int)mxGetScalar(prhs[2]);
      switch (chartFileNumber) {
       case 1:
        {
          extern void sf_c1_CRVLAB_ECU_Project_get_check_sum(mxArray *plhs[]);
          sf_c1_CRVLAB_ECU_Project_get_check_sum(plhs);
          break;
        }

       case 2:
        {
          extern void sf_c2_CRVLAB_ECU_Project_get_check_sum(mxArray *plhs[]);
          sf_c2_CRVLAB_ECU_Project_get_check_sum(plhs);
          break;
        }

       case 3:
        {
          extern void sf_c3_CRVLAB_ECU_Project_get_check_sum(mxArray *plhs[]);
          sf_c3_CRVLAB_ECU_Project_get_check_sum(plhs);
          break;
        }

       default:
        ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(0.0);
      }
    } else if (!strcmp(commandName,"target")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3176360410U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(1862911626U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(659157607U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(1884031890U);
    } else {
      return 0;
    }
  } else {
    ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(17917706U);
    ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(425593372U);
    ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(2495053071U);
    ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(745645499U);
  }

  return 1;

#else

  return 0;

#endif

}

unsigned int sf_CRVLAB_ECU_Project_autoinheritance_info( int nlhs, mxArray *
  plhs[], int nrhs, const mxArray * prhs[] )
{

#ifdef MATLAB_MEX_FILE

  char commandName[32];
  if (nrhs<2 || !mxIsChar(prhs[0]) )
    return 0;

  /* Possible call to get the autoinheritance_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_autoinheritance_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        extern mxArray *sf_c1_CRVLAB_ECU_Project_get_autoinheritance_info(void);
        plhs[0] = sf_c1_CRVLAB_ECU_Project_get_autoinheritance_info();
        break;
      }

     case 2:
      {
        extern mxArray *sf_c2_CRVLAB_ECU_Project_get_autoinheritance_info(void);
        plhs[0] = sf_c2_CRVLAB_ECU_Project_get_autoinheritance_info();
        break;
      }

     case 3:
      {
        extern mxArray *sf_c3_CRVLAB_ECU_Project_get_autoinheritance_info(void);
        plhs[0] = sf_c3_CRVLAB_ECU_Project_get_autoinheritance_info();
        break;
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;

#else

  return 0;

#endif

}

unsigned int sf_CRVLAB_ECU_Project_get_eml_resolved_functions_info( int nlhs,
  mxArray * plhs[], int nrhs, const mxArray * prhs[] )
{

#ifdef MATLAB_MEX_FILE

  char commandName[64];
  if (nrhs<2 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the get_eml_resolved_functions_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_eml_resolved_functions_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        extern const mxArray
          *sf_c1_CRVLAB_ECU_Project_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c1_CRVLAB_ECU_Project_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 2:
      {
        extern const mxArray
          *sf_c2_CRVLAB_ECU_Project_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c2_CRVLAB_ECU_Project_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 3:
      {
        extern const mxArray
          *sf_c3_CRVLAB_ECU_Project_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c3_CRVLAB_ECU_Project_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;

#else

  return 0;

#endif

}

void CRVLAB_ECU_Project_debug_initialize(void)
{
  _CRVLAB_ECU_ProjectMachineNumber_ = sf_debug_initialize_machine(
    "CRVLAB_ECU_Project","sfun",0,3,0,0,0);
  sf_debug_set_machine_event_thresholds(_CRVLAB_ECU_ProjectMachineNumber_,0,0);
  sf_debug_set_machine_data_thresholds(_CRVLAB_ECU_ProjectMachineNumber_,0);
}

void CRVLAB_ECU_Project_register_exported_symbols(SimStruct* S)
{
}
