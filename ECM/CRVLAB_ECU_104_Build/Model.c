/*
 * File: Model.c
 *
 * Code generated for Simulink model 'CRVLAB_ECU_104'.
 *
 * Model version                  : 1.1463
 * Simulink Coder version         : 8.0 (R2011a) 09-Mar-2011
 * TLC version                    : 8.0 (Feb  3 2011)
 * C/C++ source code generated on : Fri Sep 21 14:23:19 2018
 *
 * Target selection: motohawk_ert_rtw.tlc
 * Embedded hardware selection: Specified
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "Model.h"

/* Include model header file for global data */
#include "CRVLAB_ECU_104.h"
#include "CRVLAB_ECU_104_private.h"

/* Named constants for Stateflow: '<S24>/Chart2' */
#define CRVLAB_ECU_104_IN_Crank        (1U)
#define CRVLAB_ECU_104_IN_Cut          (2U)
#define CRVLAB_ECU_104_IN_FuelCut      (1U)
#define CRVLAB_ECU_104_IN_Idle         (2U)
#define CRVLAB_ECU_104_IN_NO_ACTIVE_CHILD (0U)
#define CRVLAB_ECU_104_IN_Run          (3U)
#define CRVLAB_ECU_104_IN_RunDDF       (1U)
#define CRVLAB_ECU_104_IN_RunDiesel    (2U)
#define CRVLAB_ECU_104_IN_Stall        (4U)

/* Named constants for Stateflow: '<S59>/Chart' */
#define CRVLAB_ECU_104_IN_cranktoidle  (1U)
#define CRVLAB_ECU_104_IN_normal       (2U)
#define CRVLAB_ECU_104_IN_stall        (3U)

/* Initial conditions for function-call system: '<Root>/Model' */
void CRVLAB_ECU_104_Model_Init(void)
{
  /* S-Function Block: <S56>/motohawk_delta_time */
  {
    uint32_T now = 0;
    extern uint32_T Timer_FreeRunningCounter_GetDeltaUpdateReference_us(uint32_T
      * pReference_lower32Bits, uint32_T *pReference_upper32Bits);
    extern uint32_T Timer_FreeRunningCounter_GetRawTicksFromTime(uint32_T
      u32Time_us);
    Timer_FreeRunningCounter_GetDeltaUpdateReference_us(&now, NULL);
    CRVLAB_ECU_104_DWork.s56_motohawk_delta_time_DWORK1 = now -
      Timer_FreeRunningCounter_GetRawTicksFromTime(5000.0);
  }

  /* InitializeConditions for Stateflow: '<S24>/Chart2' */
  CRVLAB_ECU_104_DWork.s53_is_Cut = 0U;
  CRVLAB_ECU_104_DWork.s53_is_Run = 0U;
  CRVLAB_ECU_104_DWork.s53_is_active_c1_CRVLAB_ECU_104 = 0U;
  CRVLAB_ECU_104_DWork.s53_is_c1_CRVLAB_ECU_104 = 0U;
  CRVLAB_ECU_104_B.s53_State = 0.0;

  /* InitializeConditions for Stateflow: '<S59>/Chart' */
  CRVLAB_ECU_104_DWork.s75_is_active_c2_CRVLAB_ECU_104 = 0U;
  CRVLAB_ECU_104_DWork.s75_is_c2_CRVLAB_ECU_104 = 0U;
  CRVLAB_ECU_104_B.s75_speedsp = 0.0;

  /* InitializeConditions for Stateflow: '<S66>/Chart' */
  CRVLAB_ECU_104_DWork.s85_is_active_c3_CRVLAB_ECU_104 = 0U;
  CRVLAB_ECU_104_DWork.s85_is_c3_CRVLAB_ECU_104 = 0U;
  CRVLAB_ECU_104_B.s85_railsp = 0.0;

  /* S-Function Block: <S98>/motohawk_delta_time */
  {
    uint32_T now = 0;
    extern uint32_T Timer_FreeRunningCounter_GetDeltaUpdateReference_us(uint32_T
      * pReference_lower32Bits, uint32_T *pReference_upper32Bits);
    extern uint32_T Timer_FreeRunningCounter_GetRawTicksFromTime(uint32_T
      u32Time_us);
    Timer_FreeRunningCounter_GetDeltaUpdateReference_us(&now, NULL);
    CRVLAB_ECU_104_DWork.s98_motohawk_delta_time_DWORK1 = now -
      Timer_FreeRunningCounter_GetRawTicksFromTime(-1);
  }

  /* S-Function Block: <S99>/motohawk_delta_time */
  {
    uint32_T now = 0;
    extern uint32_T Timer_FreeRunningCounter_GetDeltaUpdateReference_us(uint32_T
      * pReference_lower32Bits, uint32_T *pReference_upper32Bits);
    extern uint32_T Timer_FreeRunningCounter_GetRawTicksFromTime(uint32_T
      u32Time_us);
    Timer_FreeRunningCounter_GetDeltaUpdateReference_us(&now, NULL);
    CRVLAB_ECU_104_DWork.s99_motohawk_delta_time_DWORK1 = now -
      Timer_FreeRunningCounter_GetRawTicksFromTime(-1);
  }

  /* S-Function Block: <S94>/motohawk_delta_time */
  {
    uint32_T now = 0;
    extern uint32_T Timer_FreeRunningCounter_GetDeltaUpdateReference_us(uint32_T
      * pReference_lower32Bits, uint32_T *pReference_upper32Bits);
    extern uint32_T Timer_FreeRunningCounter_GetRawTicksFromTime(uint32_T
      u32Time_us);
    Timer_FreeRunningCounter_GetDeltaUpdateReference_us(&now, NULL);
    CRVLAB_ECU_104_DWork.s94_motohawk_delta_time_DWORK1 = now -
      Timer_FreeRunningCounter_GetRawTicksFromTime(-1);
  }

  /* S-Function Block: <S95>/motohawk_delta_time */
  {
    uint32_T now = 0;
    extern uint32_T Timer_FreeRunningCounter_GetDeltaUpdateReference_us(uint32_T
      * pReference_lower32Bits, uint32_T *pReference_upper32Bits);
    extern uint32_T Timer_FreeRunningCounter_GetRawTicksFromTime(uint32_T
      u32Time_us);
    Timer_FreeRunningCounter_GetDeltaUpdateReference_us(&now, NULL);
    CRVLAB_ECU_104_DWork.s95_motohawk_delta_time_DWORK1 = now -
      Timer_FreeRunningCounter_GetRawTicksFromTime(-1);
  }

  /* S-Function Block: <S149>/motohawk_delta_time */
  {
    uint32_T now = 0;
    extern uint32_T Timer_FreeRunningCounter_GetDeltaUpdateReference_us(uint32_T
      * pReference_lower32Bits, uint32_T *pReference_upper32Bits);
    extern uint32_T Timer_FreeRunningCounter_GetRawTicksFromTime(uint32_T
      u32Time_us);
    Timer_FreeRunningCounter_GetDeltaUpdateReference_us(&now, NULL);
    CRVLAB_ECU_104_DWork.s149_motohawk_delta_time_DWORK1 = now -
      Timer_FreeRunningCounter_GetRawTicksFromTime(5000.0);
  }
}

/* Start for function-call system: '<Root>/Model' */
void CRVLAB_ECU_104_Model_Start(void)
{
  /* Start for S-Function (motohawk_sfun_injector): '<S137>/motohawk_injector' */

  /* Create Injector Sequence Out: INJ1D */
  {
    NativeError_S sErrorResult;
    S_SeqOutAttributes DynamicObj;
    S_InjSeqCreateAttributes CreateObj;
    CreateObj.uValidAttributesMask = 0;
    CreateObj.u1NumPulsesToCreate = 1;
    CreateObj.s2PulseOffsetDegATDC1Arr[0] = 0;
    DynamicObj.uMinPulseDurationInMicroSecs = 1500;
    CreateObj.uValidAttributesMask |= USE_SEQ_GRANULARITY;
    CreateObj.eGranularity = FINE_GRANULARITY;
    CRVLAB_ECU_104_DWork.s137_motohawk_injector_IWORK = ((int16_T) RES_INJ1D);
    sErrorResult = CreateResource((E_ModuleResource)
      (CRVLAB_ECU_104_DWork.s137_motohawk_injector_IWORK), &CreateObj,
      BEHAVIOUR_INJ_SEQ);
    if (SUCCESS(sErrorResult)) {
      DynamicObj.u1AffectedPulse = 0;
      DynamicObj.uValidAttributesMask =
        USE_SEQ_PULSE_COND | USE_SEQ_PEAK | USE_SEQ_UPDATE_MODE |
        USE_SEQ_ENDEVENT_REPORT_COND | USE_SEQ_TIMING | USE_SEQ_MIN_DURATION;
      DynamicObj.eUpdateMode = SEQ_UPDATE_OS_PROTECTED;
      DynamicObj.u2PeakTimeInMicroSecs = 0;
      DynamicObj.eResourceCondition = SEQ_DISABLED;
      DynamicObj.eReportCond = RES_ENABLED;
      DynamicObj.TimingObj.s2StartAngle = 0;
      DynamicObj.TimingObj.s2StopAngle = 0;
      DynamicObj.TimingObj.u4Duration = 0;
      DynamicObj.TimingObj.eCtrlMode = DURATION_CTRL;
      sErrorResult = SetResourceAttributes((E_ModuleResource)
        (CRVLAB_ECU_104_DWork.s137_motohawk_injector_IWORK), &DynamicObj,
        BEHAVIOUR_INJ_SEQ);
    }

    {
      extern uint8_T seq_create_INJ1D;
      if (SUCCESS(sErrorResult))
        seq_create_INJ1D = 0;
      else
        seq_create_INJ1D = (uint8_T) GetErrorCode(sErrorResult);
    }

    CRVLAB_ECU_104_DWork.s137_motohawk_injector_DWORK1 = 0;
    CRVLAB_ECU_104_DWork.s137_motohawk_injector_DWORK2 = 0;
  }

  /* Start for S-Function (motohawk_sfun_injector): '<S137>/motohawk_injector1' */

  /* Create Injector Sequence Out: INJ2D */
  {
    NativeError_S sErrorResult;
    S_SeqOutAttributes DynamicObj;
    S_InjSeqCreateAttributes CreateObj;
    CreateObj.uValidAttributesMask = 0;
    CreateObj.u1NumPulsesToCreate = 1;
    CreateObj.s2PulseOffsetDegATDC1Arr[0] = 8640;
    DynamicObj.uMinPulseDurationInMicroSecs = 1500;
    CreateObj.uValidAttributesMask |= USE_SEQ_GRANULARITY;
    CreateObj.eGranularity = FINE_GRANULARITY;
    CRVLAB_ECU_104_DWork.s137_motohawk_injector1_IWORK = ((int16_T) RES_INJ2D);
    sErrorResult = CreateResource((E_ModuleResource)
      (CRVLAB_ECU_104_DWork.s137_motohawk_injector1_IWORK), &CreateObj,
      BEHAVIOUR_INJ_SEQ);
    if (SUCCESS(sErrorResult)) {
      DynamicObj.u1AffectedPulse = 0;
      DynamicObj.uValidAttributesMask =
        USE_SEQ_PULSE_COND | USE_SEQ_PEAK | USE_SEQ_UPDATE_MODE |
        USE_SEQ_ENDEVENT_REPORT_COND | USE_SEQ_TIMING | USE_SEQ_MIN_DURATION;
      DynamicObj.eUpdateMode = SEQ_UPDATE_OS_PROTECTED;
      DynamicObj.u2PeakTimeInMicroSecs = 0;
      DynamicObj.eResourceCondition = SEQ_DISABLED;
      DynamicObj.eReportCond = RES_ENABLED;
      DynamicObj.TimingObj.s2StartAngle = 0;
      DynamicObj.TimingObj.s2StopAngle = 0;
      DynamicObj.TimingObj.u4Duration = 0;
      DynamicObj.TimingObj.eCtrlMode = DURATION_CTRL;
      sErrorResult = SetResourceAttributes((E_ModuleResource)
        (CRVLAB_ECU_104_DWork.s137_motohawk_injector1_IWORK), &DynamicObj,
        BEHAVIOUR_INJ_SEQ);
    }

    {
      extern uint8_T seq_create_INJ2D;
      if (SUCCESS(sErrorResult))
        seq_create_INJ2D = 0;
      else
        seq_create_INJ2D = (uint8_T) GetErrorCode(sErrorResult);
    }

    CRVLAB_ECU_104_DWork.s137_motohawk_injector1_DWORK1 = 0;
    CRVLAB_ECU_104_DWork.s137_motohawk_injector1_DWORK2 = 0;
  }

  /* Start for S-Function (motohawk_sfun_injector): '<S137>/motohawk_injector2' */

  /* Create Injector Sequence Out: INJ3D */
  {
    NativeError_S sErrorResult;
    S_SeqOutAttributes DynamicObj;
    S_InjSeqCreateAttributes CreateObj;
    CreateObj.uValidAttributesMask = 0;
    CreateObj.u1NumPulsesToCreate = 1;
    CreateObj.s2PulseOffsetDegATDC1Arr[0] = 2880;
    DynamicObj.uMinPulseDurationInMicroSecs = 1500;
    CreateObj.uValidAttributesMask |= USE_SEQ_GRANULARITY;
    CreateObj.eGranularity = FINE_GRANULARITY;
    CRVLAB_ECU_104_DWork.s137_motohawk_injector2_IWORK = ((int16_T) RES_INJ3D);
    sErrorResult = CreateResource((E_ModuleResource)
      (CRVLAB_ECU_104_DWork.s137_motohawk_injector2_IWORK), &CreateObj,
      BEHAVIOUR_INJ_SEQ);
    if (SUCCESS(sErrorResult)) {
      DynamicObj.u1AffectedPulse = 0;
      DynamicObj.uValidAttributesMask =
        USE_SEQ_PULSE_COND | USE_SEQ_PEAK | USE_SEQ_UPDATE_MODE |
        USE_SEQ_ENDEVENT_REPORT_COND | USE_SEQ_TIMING | USE_SEQ_MIN_DURATION;
      DynamicObj.eUpdateMode = SEQ_UPDATE_OS_PROTECTED;
      DynamicObj.u2PeakTimeInMicroSecs = 0;
      DynamicObj.eResourceCondition = SEQ_DISABLED;
      DynamicObj.eReportCond = RES_ENABLED;
      DynamicObj.TimingObj.s2StartAngle = 0;
      DynamicObj.TimingObj.s2StopAngle = 0;
      DynamicObj.TimingObj.u4Duration = 0;
      DynamicObj.TimingObj.eCtrlMode = DURATION_CTRL;
      sErrorResult = SetResourceAttributes((E_ModuleResource)
        (CRVLAB_ECU_104_DWork.s137_motohawk_injector2_IWORK), &DynamicObj,
        BEHAVIOUR_INJ_SEQ);
    }

    {
      extern uint8_T seq_create_INJ3D;
      if (SUCCESS(sErrorResult))
        seq_create_INJ3D = 0;
      else
        seq_create_INJ3D = (uint8_T) GetErrorCode(sErrorResult);
    }

    CRVLAB_ECU_104_DWork.s137_motohawk_injector2_DWORK1 = 0;
    CRVLAB_ECU_104_DWork.s137_motohawk_injector2_DWORK2 = 0;
  }

  /* Start for S-Function (motohawk_sfun_injector): '<S137>/motohawk_injector3' */

  /* Create Injector Sequence Out: INJ4D */
  {
    NativeError_S sErrorResult;
    S_SeqOutAttributes DynamicObj;
    S_InjSeqCreateAttributes CreateObj;
    CreateObj.uValidAttributesMask = 0;
    CreateObj.u1NumPulsesToCreate = 1;
    CreateObj.s2PulseOffsetDegATDC1Arr[0] = 5760;
    DynamicObj.uMinPulseDurationInMicroSecs = 1500;
    CreateObj.uValidAttributesMask |= USE_SEQ_GRANULARITY;
    CreateObj.eGranularity = FINE_GRANULARITY;
    CRVLAB_ECU_104_DWork.s137_motohawk_injector3_IWORK = ((int16_T) RES_INJ4D);
    sErrorResult = CreateResource((E_ModuleResource)
      (CRVLAB_ECU_104_DWork.s137_motohawk_injector3_IWORK), &CreateObj,
      BEHAVIOUR_INJ_SEQ);
    if (SUCCESS(sErrorResult)) {
      DynamicObj.u1AffectedPulse = 0;
      DynamicObj.uValidAttributesMask =
        USE_SEQ_PULSE_COND | USE_SEQ_PEAK | USE_SEQ_UPDATE_MODE |
        USE_SEQ_ENDEVENT_REPORT_COND | USE_SEQ_TIMING | USE_SEQ_MIN_DURATION;
      DynamicObj.eUpdateMode = SEQ_UPDATE_OS_PROTECTED;
      DynamicObj.u2PeakTimeInMicroSecs = 0;
      DynamicObj.eResourceCondition = SEQ_DISABLED;
      DynamicObj.eReportCond = RES_ENABLED;
      DynamicObj.TimingObj.s2StartAngle = 0;
      DynamicObj.TimingObj.s2StopAngle = 0;
      DynamicObj.TimingObj.u4Duration = 0;
      DynamicObj.TimingObj.eCtrlMode = DURATION_CTRL;
      sErrorResult = SetResourceAttributes((E_ModuleResource)
        (CRVLAB_ECU_104_DWork.s137_motohawk_injector3_IWORK), &DynamicObj,
        BEHAVIOUR_INJ_SEQ);
    }

    {
      extern uint8_T seq_create_INJ4D;
      if (SUCCESS(sErrorResult))
        seq_create_INJ4D = 0;
      else
        seq_create_INJ4D = (uint8_T) GetErrorCode(sErrorResult);
    }

    CRVLAB_ECU_104_DWork.s137_motohawk_injector3_DWORK1 = 0;
    CRVLAB_ECU_104_DWork.s137_motohawk_injector3_DWORK2 = 0;
  }

  /* S-Function (motohawk_sfun_probe): '<S60>/motohawk_probe1' */
  (MainFuelingPotenGain_DataStore()) = 1.0;

  /* S-Function (motohawk_sfun_probe): '<S168>/motohawk_probe1' */
  (cyl_DataStore()) = ((uint8_T)1U);
}

/* Output and update for function-call system: '<Root>/Model' */
void CRVLAB_ECU_104_Model(void)
{
  /* local block i/o variables */
  real_T rtb_motohawk_data_read1;
  real_T rtb_Sum1;
  real_T rtb_Sum2;
  real_T rtb_Sum2_p;
  real_T rtb_Sum2_e;
  real_T rtb_DataTypeConversion1;
  real_T rtb_Sum1_o;
  real_T rtb_Sum2_j;
  real_T rtb_Sum2_c;
  real_T rtb_Saturation;
  real_T rtb_MinMax1;
  real_T rtb_Sum2_d;
  real_T rtb_Sum2_b;
  real_T rtb_motohawk_delta_time;
  real_T rtb_Saturation_e;
  real_T rtb_motohawk_data_read11;
  real_T rtb_motohawk_data_read12;
  real_T rtb_motohawk_data_read16;
  real_T rtb_MinMax1_f;
  real_T rtb_motohawk_data_read1_g;
  real_T rtb_motohawk_data_read2_o;
  real_T rtb_Add_n;
  real_T rtb_motohawk_data_read1_df;
  real_T rtb_motohawk_data_read2_c;
  real_T rtb_Merge_n;
  real_T rtb_motohawk_data_read4;
  real_T rtb_motohawk_data_read1_o;
  real_T rtb_motohawk_interpolation_1d;
  real_T rtb_motohawk_data_read2_k;
  real_T rtb_motohawk_data_read4_j;
  real_T rtb_motohawk_data_read1_n;
  real_T rtb_motohawk_interpolation_1d_p;
  real_T rtb_motohawk_data_read2_cb;
  real_T rtb_motohawk_data_read1_l;
  real_T rtb_motohawk_interpolation_1d_e;
  real_T rtb_motohawk_data_read2_ce;
  real_T rtb_motohawk_data_read4_p;
  real_T rtb_motohawk_data_read1_nl;
  real_T rtb_motohawk_data_read2_e;
  real_T rtb_motohawk_data_read1_n4;
  real_T rtb_motohawk_interpolation_1d_g;
  real_T rtb_motohawk_data_read2_l;
  real_T rtb_motohawk_data_read1_o0;
  real_T rtb_motohawk_data_read2_ca;
  real_T rtb_motohawk_data_read1_m4;
  real_T rtb_motohawk_interpolation_1d_k;
  real_T rtb_motohawk_data_read2_j;
  real_T rtb_MinMax1_d;
  real_T rtb_motohawk_data_read1_j;
  real_T rtb_motohawk_data_read2_jc;
  real_T rtb_Merge_k;
  real_T rtb_motohawk_data_read3;
  real_T rtb_motohawk_delta_time_m;
  real_T rtb_motohawk_data_read10;
  real_T rtb_motohawk_interpolation_1d2;
  real_T rtb_motohawk_interpolation_1d1;
  real_T rtb_motohawk_delta_time_p;
  real_T rtb_motohawk_delta_time_a;
  real_T rtb_motohawk_data_read15;
  real_T rtb_motohawk_delta_time_b;
  real_T rtb_Add2_bt;
  real_T rtb_Add5;
  real_T rtb_motohawk_data_read1_na;
  real_T rtb_motohawk_data_read2_ir;
  real_T rtb_motohawk_delta_time_pa;
  real_T rtb_Saturation_ea;
  real_T rtb_motohawk_data_read1_d3;
  real_T rtb_motohawk_data_read4_l;
  real_T rtb_motohawk_data_read2_g;
  real_T rtb_motohawk_data_read5_a;
  real_T rtb_motohawk_data_read3_h;
  real_T rtb_motohawk_data_read6_g;
  real_T rtb_motohawk_interpolation_2d1;
  real_T rtb_motohawk_interpolation_2d1_o;
  real_T rtb_motohawk_interpolation_1d_f;
  real_T rtb_motohawk_interpolation_2d1_k;
  real_T rtb_motohawk_interpolation_2d1_op;
  real_T rtb_motohawk_interpolation_1d_fh;
  real_T rtb_motohawk_interpolation_2d1_p;
  real_T rtb_motohawk_interpolation_2d1_c;
  real_T rtb_motohawk_interpolation_2d1_j;
  real_T rtb_motohawk_interpolation_2d1_js;
  real_T rtb_motohawk_interpolation_2d2;
  real_T rtb_motohawk_interpolation_2d1_b;
  real_T rtb_motohawk_interpolation_2d2_h;
  real_T rtb_motohawk_interpolation_2d4;
  real_T rtb_motohawk_data_read13_o;
  real_T rtb_motohawk_interpolation_2d2_m;
  real_T rtb_motohawk_data_read15_i;
  real_T rtb_motohawk_interpolation_2d3;
  real_T rtb_motohawk_interpolation_2d2_j;
  real_T rtb_motohawk_interpolation_2d2_l;
  real_T rtb_motohawk_interpolation_2d2_k;
  real_T rtb_motohawk_interpolation_2d2_i;
  real_T rtb_motohawk_interpolation_1d2_a;
  real_T rtb_motohawk_interpolation_1d3;
  real_T rtb_motohawk_interpolation_2d2_m3;
  real_T rtb_motohawk_interpolation_2d2_e;
  real_T rtb_motohawk_interpolation_1d2_e;
  real_T rtb_motohawk_interpolation_1d3_e;
  real_T rtb_motohawk_interpolation_1d2_m;
  real_T rtb_motohawk_interpolation_1d3_i;
  real_T rtb_motohawk_interpolation_2d2_d;
  real_T rtb_motohawk_data_read1_mk;
  real_T rtb_Gain2;
  real_T rtb_Gain4;
  uint32_T rtb_Saturation1;
  uint32_T rtb_Saturation2;
  uint32_T rtb_Saturation3;
  uint32_T rtb_Saturation4;
  uint32_T rtb_DataTypeConversion1_fy;
  uint32_T rtb_Saturation_ek[3];
  uint32_T rtb_Saturation_o[3];
  uint32_T rtb_Saturation_i[3];
  uint32_T rtb_Saturation_od[3];
  uint32_T rtb_DataTypeConversion3;
  uint32_T rtb_DataTypeConversion2;
  uint32_T rtb_DataTypeConversion2_f;
  uint32_T rtb_DataTypeConversion5;
  int16_T rtb_DataTypeConversion3_c;
  int16_T rtb_DataTypeConversion_l;
  int16_T rtb_TmpSignalConversionAtMuxPSPInport2[3];
  int16_T rtb_TmpSignalConversionAtMuxPSPInport2_l[3];
  int16_T rtb_TmpSignalConversionAtMuxPSPInport2_f[3];
  int16_T rtb_TmpSignalConversionAtMuxPSPInport2_m[3];
  int16_T rtb_DataTypeConversion1_p;
  int16_T rtb_DataTypeConversion1_c;
  int16_T rtb_DataTypeConversion1_pt;
  int16_T rtb_DataTypeConversion4;
  uint16_T rtb_motohawk_ain5_e;
  uint16_T rtb_motohawk_ain4;
  uint16_T rtb_RPM;
  uint16_T rtb_motohawk_encoder_instant_rpm;
  uint16_T rtb_DataTypeConversion2_e;
  index_T rtb_motohawk_prelookup1;
  index_T rtb_motohawk_prelookup;
  index_T rtb_motohawk_prelookup3;
  index_T rtb_motohawk_prelookup2;
  index_T rtb_motohawk_prelookup2_k;
  index_T rtb_motohawk_prelookup_k;
  index_T rtb_motohawk_prelookup5;
  index_T rtb_motohawk_prelookup_o;
  index_T rtb_motohawk_prelookup5_i;
  index_T rtb_motohawk_prelookup_b;
  index_T rtb_motohawk_prelookup_n;
  index_T rtb_motohawk_prelookup_h;
  index_T rtb_motohawk_prelookup4;
  index_T rtb_motohawk_prelookup1_e;
  index_T rtb_motohawk_prelookup5_f;
  index_T rtb_motohawk_prelookup1_i;
  index_T rtb_motohawk_prelookup5_j;
  index_T rtb_motohawk_prelookup2_ke;
  index_T rtb_motohawk_prelookup3_d;
  index_T rtb_motohawk_prelookup1_m;
  index_T rtb_motohawk_prelookup4_m;
  index_T rtb_motohawk_prelookup2_kb;
  index_T rtb_motohawk_prelookup3_e;
  index_T rtb_motohawk_prelookup_f;
  index_T rtb_motohawk_prelookup2_a;
  index_T rtb_motohawk_prelookup3_c;
  index_T rtb_motohawk_prelookup1_k;
  index_T rtb_motohawk_prelookup4_o;
  index_T rtb_motohawk_prelookup2_l;
  index_T rtb_motohawk_prelookup3_a;
  index_T rtb_motohawk_prelookup1_ep;
  index_T rtb_motohawk_prelookup4_b;
  index_T rtb_motohawk_prelookup_c;
  index_T rtb_motohawk_prelookup2_kb4;
  index_T rtb_motohawk_prelookup3_ce;
  index_T rtb_motohawk_prelookup1_im;
  index_T rtb_motohawk_prelookup4_p;
  index_T rtb_motohawk_prelookup6;
  index_T rtb_motohawk_prelookup7;
  index_T rtb_motohawk_prelookup2_o;
  index_T rtb_motohawk_prelookup3_b;
  index_T rtb_motohawk_prelookup1_f;
  index_T rtb_motohawk_prelookup4_f;
  index_T rtb_motohawk_prelookup2_f;
  index_T rtb_motohawk_prelookup3_k;
  index_T rtb_motohawk_prelookup1_ka;
  index_T rtb_motohawk_prelookup4_n;
  index_T rtb_motohawk_prelookup6_d;
  index_T rtb_motohawk_prelookup7_p;
  index_T rtb_motohawk_prelookup2_j;
  index_T rtb_motohawk_prelookup3_bf;
  index_T rtb_motohawk_prelookup1_g;
  index_T rtb_motohawk_prelookup4_h;
  index_T rtb_motohawk_prelookup6_f;
  index_T rtb_motohawk_prelookup7_k;
  index_T rtb_motohawk_prelookup2_ab;
  index_T rtb_motohawk_prelookup3_g;
  index_T rtb_motohawk_prelookup1_em;
  index_T rtb_motohawk_prelookup4_l;
  index_T rtb_motohawk_prelookup1_a;
  index_T rtb_motohawk_prelookup4_oq;
  index_T rtb_motohawk_prelookup5_jm;
  index_T rtb_motohawk_prelookup8;
  index_T rtb_motohawk_prelookup1_ga;
  index_T rtb_motohawk_prelookup4_oj;
  index_T rtb_motohawk_prelookup5_g;
  index_T rtb_motohawk_prelookup6_j;
  uint8_T rtb_motohawk_data_read1_nr;
  boolean_T rtb_RelationalOperator_i;
  boolean_T rtb_Compare_og;
  boolean_T rtb_Compare_j;
  boolean_T rtb_Compare_h5;
  boolean_T rtb_Compare_o5;
  boolean_T rtb_LogicalOperator4;
  boolean_T rtb_motohawk_data_read4_m;
  boolean_T rtb_Merge_kz;
  boolean_T rtb_Merge_e;
  boolean_T rtb_Merge_ei;
  boolean_T rtb_Merge_h;
  boolean_T rtb_DataTypeConversion2_g;
  boolean_T rtb_CombinatorialLogic[2];
  boolean_T rtb_motohawk_data_read6_j;
  boolean_T rtb_motohawk_data_read8_e;
  boolean_T rtb_motohawk_data_read6_gv;
  boolean_T rtb_motohawk_data_read8_f;
  boolean_T rtb_motohawk_data_read8_mn;
  boolean_T rtb_motohawk_data_read6_c;
  boolean_T rtb_AboveHiTarget;
  boolean_T rtb_BelowLoTarget;
  real_T rtb_Merge_p;
  real_T rtb_Sum2_h;
  boolean_T rtb_LogicalOperator5;
  boolean_T rtb_RelationalOperator_d;
  boolean_T rtb_LogicalOperator3_a;
  boolean_T rtb_RelationalOperator_h;
  uint8_T rtb_Compare_h;
  real_T rtb_Sum4;
  real_T rtb_UnitDelay1_h;
  real_T rtb_Sum4_e;
  real_T rtb_UnitDelay1_c;
  real_T rtb_Sum4_b;
  real_T rtb_UnitDelay1_g;
  real_T rtb_Sum4_a;
  real_T rtb_UnitDelay1_b;
  real_T rtb_Sum5_i;
  real_T rtb_Add_p;
  uint8_T rtb_Compare_mc;
  real_T rtb_Sum5;
  real_T rtb_Sum2_cw;
  real_T rtb_Sum_g;
  real_T rtb_Sum_b;
  real_T rtb_Switch1_d;
  real32_T rtb_Switch1_b;
  real_T rtb_Add4;
  real_T rtb_Add_i_idx;
  uint32_T u;
  int16_T rtb_DataTypeConversion_h_0;
  uint32_T rtb_DataTypeConversion1_f_0;
  int16_T rtb_DataTypeConversion2_f_0;

  /* S-Function (motohawk_sfun_data_write): '<S28>/motohawk_data_write' incorporates:
   *  S-Function (motohawk_sfun_fault_action): '<S28>/motohawk_fault_action'
   *
   * Regarding '<S28>/motohawk_fault_action':
     Get Fault Action Status: StallLock
   */
  /* Write to Data Storage as scalar: StallLock_On */
  {
    StallLock_On_DataStore() = GetFaultActionStatus(2);
  }

  /* S-Function (motohawk_sfun_data_write): '<S28>/motohawk_data_write1' incorporates:
   *  S-Function (motohawk_sfun_fault_action): '<S28>/motohawk_fault_action1'
   *
   * Regarding '<S28>/motohawk_fault_action1':
     Get Fault Action Status: IdleLock
   */
  /* Write to Data Storage as scalar: IdleLock_On */
  {
    IdleLock_On_DataStore() = GetFaultActionStatus(1);
  }

  /* S-Function (motohawk_sfun_data_write): '<S28>/motohawk_data_write2' incorporates:
   *  S-Function (motohawk_sfun_fault_action): '<S28>/motohawk_fault_action2'
   *
   * Regarding '<S28>/motohawk_fault_action2':
     Get Fault Action Status: DieselLock
   */
  /* Write to Data Storage as scalar: DieselLock_On */
  {
    DieselLock_On_DataStore() = GetFaultActionStatus(0);
  }

  /* S-Function (motohawk_sfun_data_read): '<S226>/motohawk_data_read1' */
  rtb_motohawk_data_read1 = ECT_C_DataStore();

  /* If: '<S227>/If' incorporates:
   *  Inport: '<S228>/In1'
   *  Inport: '<S229>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S227>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S227>/override_enable'
   */
  if ((ECT_C_Override_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S227>/NewValue' incorporates:
     *  ActionPort: '<S228>/Action Port'
     */
    rtb_Merge_p = (ECT_C_Override_new_DataStore());

    /* End of Outputs for SubSystem: '<S227>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S227>/OldValue' incorporates:
     *  ActionPort: '<S229>/Action Port'
     */
    rtb_Merge_p = rtb_motohawk_data_read1;

    /* End of Outputs for SubSystem: '<S227>/OldValue' */
  }

  /* End of If: '<S227>/If' */

  /* RelationalOperator: '<S226>/Relational Operator' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S226>/motohawk_calibration2'
   */
  rtb_RelationalOperator_i = ((rtb_Merge_p > (ECTHighLimit_C_DataStore())));

  /* S-Function (motohawk_sfun_fault_def): '<S226>/motohawk_fault_def' */

  /* Set Fault Suspected Status: ECT_CTooHigh */
  {
    extern void SetFaultSuspected(uint32_T fault, boolean_T val);
    extern void UpdateFault(uint32_T fault);
    SetFaultSuspected(0, rtb_RelationalOperator_i);
    UpdateFault(0);
  }

  /* S-Function (motohawk_sfun_fault_status): '<S226>/motohawk_fault_status'
   *
   * Regarding '<S226>/motohawk_fault_status':
     Status : "Output All States"
   */
  {
    extern boolean_T IsFaultSuspected(uint32_T fault);
    CRVLAB_ECU_104_B.s226_motohawk_fault_status_o1 = IsFaultSuspected(0);
  }

  {
    extern boolean_T IsFaultActive(uint32_T fault);
    CRVLAB_ECU_104_B.s226_motohawk_fault_status_o2 = IsFaultActive(0);
  }

  {
    extern boolean_T IsFaultOccurred(uint32_T fault);
    CRVLAB_ECU_104_B.s226_motohawk_fault_status_o3 = IsFaultOccurred(0);
  }

  /* S-Function Block: <S29>/motohawk_ain1 Resource: AN8M */
  {
    extern NativeError_S AN8M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
    AN8M_AnalogInput_Get(&CRVLAB_ECU_104_B.s29_motohawk_ain1, NULL);
  }

  /* RelationalOperator: '<S41>/Compare' incorporates:
   *  Constant: '<S41>/Constant'
   *  DataTypeConversion: '<S29>/Data Type Conversion'
   */
  rtb_Compare_og = ((CRVLAB_ECU_104_B.s29_motohawk_ain1 > 500));

  /* S-Function (motohawk_sfun_data_write): '<S29>/motohawk_data_write' */
  /* Write to Data Storage as scalar: AC_On */
  {
    AC_On_DataStore() = rtb_Compare_og;
  }

  /* S-Function Block: <S30>/motohawk_ain5 Resource: AN10M */
  {
    extern NativeError_S AN10M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
    AN10M_AnalogInput_Get(&rtb_motohawk_ain5_e, NULL);
  }

  /* DataTypeConversion: '<S30>/Data Type Conversion' incorporates:
   *  S-Function (motohawk_sfun_ain): '<S30>/motohawk_ain5'
   */
  rtb_Merge_p = (real_T)rtb_motohawk_ain5_e;

  /* Product: '<S30>/Product1' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S30>/motohawk_calibration'
   */
  rtb_Merge_p *= (CNGPGain_kPa_ADC_DataStore());

  /* Sum: '<S30>/Sum1' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S30>/motohawk_calibration1'
   */
  rtb_Sum1 = rtb_Merge_p + (CNGPOffset_kPa_DataStore());

  /* S-Function (motohawk_sfun_data_write): '<S30>/motohawk_data_write' */
  /* Write to Data Storage as scalar: CNGP_kPa */
  {
    CNGP_kPa_DataStore() = rtb_Sum1;
  }

  /* S-Function Block: <S31>/motohawk_ain1 Resource: AN13M */
  {
    extern NativeError_S AN13M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
    AN13M_AnalogInput_Get(&CRVLAB_ECU_104_B.s31_motohawk_ain1, NULL);
  }

  /* RelationalOperator: '<S42>/Compare' incorporates:
   *  Constant: '<S42>/Constant'
   */
  rtb_Compare_j = ((CRVLAB_ECU_104_B.s31_motohawk_ain1 > 500));

  /* S-Function (motohawk_sfun_data_write): '<S31>/motohawk_data_write1' */
  /* Write to Data Storage as scalar: Power_On */
  {
    Power_On_DataStore() = rtb_Compare_j;
  }

  /* S-Function Block: <S31>/motohawk_ain3 Resource: AN11M */
  {
    extern NativeError_S AN11M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
    AN11M_AnalogInput_Get(&CRVLAB_ECU_104_B.s31_motohawk_ain3, NULL);
  }

  /* RelationalOperator: '<S43>/Compare' incorporates:
   *  Constant: '<S43>/Constant'
   */
  rtb_Compare_h5 = ((CRVLAB_ECU_104_B.s31_motohawk_ain3 < 500));

  /* S-Function (motohawk_sfun_data_write): '<S31>/motohawk_data_write3' */
  /* Write to Data Storage as scalar: Normal_On */
  {
    Normal_On_DataStore() = rtb_Compare_h5;
  }

  /* S-Function Block: <S31>/motohawk_ain4 Resource: AN9M */
  {
    extern NativeError_S AN9M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
    AN9M_AnalogInput_Get(&rtb_motohawk_ain4, NULL);
  }

  /* S-Function (motohawk_sfun_data_write): '<S31>/motohawk_data_write' */
  /* Write to Data Storage as scalar: Poten_ADC */
  {
    Poten_ADC_DataStore() = rtb_motohawk_ain4;
  }

  /* S-Function (motohawk_sfun_data_write): '<S31>/motohawk_data_write4' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S31>/motohawk_calibration'
   */
  /* Write to Data Storage as scalar: PotenHalf_ADC */
  {
    PotenHalf_ADC_DataStore() = (CNGPotenHalfCal_ADC_DataStore());
  }

  /* S-Function Block: <S32>/motohawk_ain Resource: AN5M */
  {
    extern NativeError_S AN5M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
    AN5M_AnalogInput_Get(&rtb_DataTypeConversion2_e, NULL);
  }

  /* DataTypeConversion: '<S32>/Data Type Conversion1' */
  CRVLAB_ECU_104_B.s32_DataTypeConversion1 = (real_T)rtb_DataTypeConversion2_e;

  /* Product: '<S32>/Product1' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S32>/motohawk_calibration2'
   */
  rtb_Merge_p = CRVLAB_ECU_104_B.s32_DataTypeConversion1 *
    (ECTGain_C_ADC_DataStore());

  /* Sum: '<S32>/Sum2' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S32>/motohawk_calibration3'
   */
  rtb_Sum2 = rtb_Merge_p + (ECTOffset_C_DataStore());

  /* S-Function (motohawk_sfun_data_write): '<S32>/motohawk_data_write' */
  /* Write to Data Storage as scalar: ECT_C */
  {
    ECT_C_DataStore() = rtb_Sum2;
  }

  /* Switch: '<S44>/Switch4' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S44>/motohawk_calibration6'
   *  S-Function (motohawk_sfun_calibration): '<S44>/motohawk_calibration7'
   *  S-Function (motohawk_sfun_data_read): '<S32>/motohawk_data_read2'
   */
  if (Idle_High_on_ECT_DataStore() > 0.5) {
    rtb_Gain4 = (ECT_Low_On_Theshold_DataStore());
  } else {
    rtb_Gain4 = (ECT_Low_Off_Theshold_DataStore());
  }

  /* End of Switch: '<S44>/Switch4' */
  /* Sum: '<S44>/Add2' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S32>/motohawk_calibration1'
   */
  rtb_Merge_p = rtb_Gain4 + (real_T)(IdleSpeed_ECT_Low_SP_DataStore());

  /* RelationalOperator: '<S44>/Relational Operator' incorporates:
   *  S-Function (motohawk_sfun_data_read): '<S32>/motohawk_data_read1'
   */
  rtb_BelowLoTarget = (rtb_Merge_p > ECT_C_DataStore());

  /* DataTypeConversion: '<S44>/Data Type Conversion' */
  rtb_Gain4 = (real_T)rtb_BelowLoTarget;

  /* S-Function (motohawk_sfun_data_write): '<S32>/motohawk_data_write1' */
  /* Write to Data Storage as scalar: Idle_High_on_ECT_cmd */
  {
    Idle_High_on_ECT_cmd_DataStore() = rtb_Gain4;
  }

  /* S-Function Block: <S33>/motohawk_ain1 Resource: AN7M */
  {
    extern NativeError_S AN7M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
    AN7M_AnalogInput_Get(&rtb_DataTypeConversion2_e, NULL);
  }

  /* DataTypeConversion: '<S33>/Data Type Conversion1' */
  CRVLAB_ECU_104_B.s33_DataTypeConversion1 = (real_T)rtb_DataTypeConversion2_e;

  /* Product: '<S33>/Product1' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S33>/motohawk_calibration2'
   */
  rtb_Merge_p = CRVLAB_ECU_104_B.s33_DataTypeConversion1 *
    (EGRGain_Pct_ADC_DataStore());

  /* Sum: '<S33>/Sum2' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S33>/motohawk_calibration3'
   */
  CRVLAB_ECU_104_B.s33_Sum2 = rtb_Merge_p + (EGROffset_Pct_DataStore());

  /* Sum: '<S45>/Sum2' incorporates:
   *  Constant: '<S45>/Constant'
   *  Product: '<S45>/Product'
   *  Product: '<S45>/Product2'
   *  S-Function (motohawk_sfun_calibration): '<S33>/motohawk_calibration1'
   *  Sum: '<S45>/Sum'
   *  UnitDelay: '<S45>/Unit Delay'
   */
  rtb_Sum2_p = (1.0 - (EGRFilterAlpha_DataStore())) *
    CRVLAB_ECU_104_DWork.s45_UnitDelay_DSTATE + CRVLAB_ECU_104_B.s33_Sum2 *
    (EGRFilterAlpha_DataStore());

  /* S-Function (motohawk_sfun_data_write): '<S33>/motohawk_data_write' */
  /* Write to Data Storage as scalar: EGR_Pct */
  {
    EGR_Pct_DataStore() = rtb_Sum2_p;
  }

  /* S-Function Block: <S34>/motohawk_encoder_average_rpm */
  {
    rtb_RPM = GetEncoderResourceAverageRPM();
  }

  /* DataTypeConversion: '<S34>/Data Type Conversion' incorporates:
   *  S-Function (motohawk_sfun_encoder_average_rpm): '<S34>/motohawk_encoder_average_rpm'
   */
  CRVLAB_ECU_104_B.s34_RPM = (real_T)rtb_RPM;

  /* Sum: '<S46>/Sum2' incorporates:
   *  Constant: '<S46>/Constant'
   *  Product: '<S46>/Product'
   *  Product: '<S46>/Product2'
   *  S-Function (motohawk_sfun_calibration): '<S34>/motohawk_calibration1'
   *  Sum: '<S46>/Sum'
   *  UnitDelay: '<S46>/Unit Delay'
   */
  rtb_Sum2_e = (1.0 - (RPMFilterAlpha_DataStore())) *
    CRVLAB_ECU_104_DWork.s46_UnitDelay_DSTATE + CRVLAB_ECU_104_B.s34_RPM *
    (RPMFilterAlpha_DataStore());

  /* S-Function (motohawk_sfun_data_write): '<S34>/motohawk_data_write' */
  /* Write to Data Storage as scalar: EngineSpeed_rpm */
  {
    EngineSpeed_rpm_DataStore() = rtb_Sum2_e;
  }

  /* S-Function Block: <S34>/motohawk_encoder_instant_rpm */
  {
    rtb_motohawk_encoder_instant_rpm = GetEncoderResourceInstantRPM();
  }

  /* DataTypeConversion: '<S34>/Data Type Conversion1' incorporates:
   *  S-Function (motohawk_sfun_encoder_instant_rpm): '<S34>/motohawk_encoder_instant_rpm'
   */
  rtb_DataTypeConversion1 = (real_T)rtb_motohawk_encoder_instant_rpm;

  /* S-Function (motohawk_sfun_data_write): '<S34>/motohawk_data_write1' */
  /* Write to Data Storage as scalar: InstantSpeed_rpm */
  {
    InstantSpeed_rpm_DataStore() = rtb_DataTypeConversion1;
  }

  /* DataTypeConversion: '<S34>/Data Type Conversion3' incorporates:
   *  Gain: '<S34>/Gain'
   *  S-Function (motohawk_sfun_calibration): '<S34>/motohawk_calibration2'
   */
  rtb_Add_i_idx = 16.0 * (EncoderTDCOffset_DataStore());
  if (rtIsNaN(rtb_Add_i_idx) || rtIsInf(rtb_Add_i_idx)) {
    rtb_Add_i_idx = 0.0;
  } else {
    rtb_Add_i_idx = fmod(rtb_Add_i_idx < 0.0 ? ceil(rtb_Add_i_idx) : floor
                         (rtb_Add_i_idx), 65536.0);
  }

  rtb_DataTypeConversion3_c = (int16_T)(rtb_Add_i_idx < 0.0 ? (int16_T)-(int16_T)
                                        (uint16_T)-rtb_Add_i_idx : (int16_T)
    (uint16_T)rtb_Add_i_idx);

  /* End of DataTypeConversion: '<S34>/Data Type Conversion3' */
  /* S-Function Block: <S34>/motohawk_encoder_offset */
  {
    S_EncoderResourceAttributes encoder_attributes;
    encoder_attributes.uValidAttributesMask = USE_ENC_TDC_OFFSET;
    encoder_attributes.s2TDCOffset = rtb_DataTypeConversion3_c;
    SetResourceAttributes(RES_ENCODER, &encoder_attributes, BEHAVIOUR_ENCODER);
  }

  /* DataTypeConversion: '<S35>/Data Type Conversion' incorporates:
   *  S-Function (motohawk_sfun_data_read): '<S35>/motohawk_data_read'
   */
  rtb_Merge_p = (real_T)((uint16_T)KeySw_ADC_DataStore());

  /* Product: '<S35>/Product1' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S35>/motohawk_calibration'
   */
  rtb_Merge_p *= (KeySwGain_Volt_ADC_DataStore());

  /* Sum: '<S35>/Sum1' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S35>/motohawk_calibration1'
   */
  rtb_Sum1_o = rtb_Merge_p + (KeySwOffset_Volt_DataStore());

  /* S-Function (motohawk_sfun_data_write): '<S35>/motohawk_data_write' */
  /* Write to Data Storage as scalar: KeySw_Volt */
  {
    KeySw_Volt_DataStore() = rtb_Sum1_o;
  }

  /* S-Function Block: <S35>/motohawk_ain1 Resource: DRVP */
  {
    extern NativeError_S DRVP_AnalogInput_Get(uint16_T *adc, uint16_T *status);
    DRVP_AnalogInput_Get(&CRVLAB_ECU_104_B.s35_motohawk_ain1, NULL);
  }

  /* DataTypeConversion: '<S35>/Data Type Conversion1' */
  rtb_Merge_p = (real_T)CRVLAB_ECU_104_B.s35_motohawk_ain1;

  /* Product: '<S35>/Product2' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S35>/motohawk_calibration2'
   */
  rtb_Merge_p *= (DRVPGain_Volt_ADC_DataStore());

  /* Sum: '<S35>/Sum2' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S35>/motohawk_calibration3'
   */
  rtb_Sum2_j = rtb_Merge_p + (DRVPOffset_Volt_DataStore());

  /* S-Function (motohawk_sfun_data_write): '<S35>/motohawk_data_write1' */
  /* Write to Data Storage as scalar: DRVP_Volt */
  {
    DRVP_Volt_DataStore() = rtb_Sum2_j;
  }

  /* S-Function Block: <S36>/motohawk_ain Resource: MAFSensor */
  {
    extern NativeError_S MAFSensor_AnalogInput_Get(uint16_T *adc, uint16_T
      *status);
    MAFSensor_AnalogInput_Get(&CRVLAB_ECU_104_B.s36_motohawk_ain, NULL);
  }

  /* DataTypeConversion: '<S36>/Data Type Conversion1' */
  CRVLAB_ECU_104_B.s36_DataTypeConversion1 = (real_T)
    CRVLAB_ECU_104_B.s36_motohawk_ain;

  /* Product: '<S36>/Product1' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S36>/motohawk_calibration2'
   */
  rtb_Merge_p = CRVLAB_ECU_104_B.s36_DataTypeConversion1 *
    (MAPGain_ADC_DataStore());

  /* Sum: '<S36>/Sum2' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S36>/motohawk_calibration3'
   */
  rtb_Sum2_c = rtb_Merge_p + (MAPOffsetr_DataStore());

  /* S-Function (motohawk_sfun_data_write): '<S36>/motohawk_data_write' */
  /* Write to Data Storage as scalar: MAFP_kPa */
  {
    MAFP_kPa_DataStore() = rtb_Sum2_c;
  }

  /* S-Function Block: <S37>/motohawk_ain1 Resource: AN12M */
  {
    extern NativeError_S AN12M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
    AN12M_AnalogInput_Get(&CRVLAB_ECU_104_B.s37_motohawk_ain1, NULL);
  }

  /* RelationalOperator: '<S47>/Compare' incorporates:
   *  Constant: '<S47>/Constant'
   *  DataTypeConversion: '<S37>/Data Type Conversion'
   */
  rtb_Compare_o5 = ((CRVLAB_ECU_104_B.s37_motohawk_ain1 < 500));

  /* S-Function (motohawk_sfun_data_write): '<S37>/motohawk_data_write' */
  /* Write to Data Storage as scalar: Neutral_On */
  {
    Neutral_On_DataStore() = rtb_Compare_o5;
  }

  /* S-Function (motohawk_sfun_data_read): '<S38>/motohawk_data_read' */
  rtb_BelowLoTarget = Power_On_DataStore();

  /* Product: '<S38>/Product' incorporates:
   *  Constant: '<S38>/Constant'
   */
  rtb_Gain4 = (real_T)rtb_BelowLoTarget * 2.0;

  /* Sum: '<S38>/Add' */
  rtb_Merge_p = rtb_Gain4;

  /* S-Function Block: <S38>/motohawk_ain1 Resource: AN2M */
  {
    extern NativeError_S AN2M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
    AN2M_AnalogInput_Get(&rtb_DataTypeConversion2_e, NULL);
  }

  /* DataTypeConversion: '<S38>/Data Type Conversion1' */
  CRVLAB_ECU_104_B.s38_DataTypeConversion1 = (real_T)rtb_DataTypeConversion2_e;

  /* S-Function Block: <S38>/motohawk_prelookup1 */
  {
    extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
      ordarr[], uint32_T sz, uint16_T prev);
    (Pedal2_ADC_in_Pedal2_ADC_Pedal_Pct_mapIn_DataStore()) =
      CRVLAB_ECU_104_B.s38_DataTypeConversion1;
    (Pedal2_ADC_in_Pedal2_ADC_Pedal_Pct_mapIdx_DataStore()) =
      TablePrelookup_real_T(CRVLAB_ECU_104_B.s38_DataTypeConversion1,
      (Pedal2_ADC_in_Pedal2_ADC_Pedal_Pct_mapIdxArr_DataStore()), 2,
      (Pedal2_ADC_in_Pedal2_ADC_Pedal_Pct_mapIdx_DataStore()));
    rtb_motohawk_prelookup1 =
      (Pedal2_ADC_in_Pedal2_ADC_Pedal_Pct_mapIdx_DataStore());
  }

  /* S-Function Block: <S38>/motohawk_interpolation_1d1 */
  {
    extern real_T TableInterpolation1D_real_T(uint16_T idx, real_T *tbl_data,
      uint32_T sz);
    CRVLAB_ECU_104_B.s38_motohawk_interpolation_1d1 =
      TableInterpolation1D_real_T(rtb_motohawk_prelookup1, (real_T *)
      ((Pedal_Pct_in_Pedal2_ADC_Pedal_Pct_mapTbl_DataStore())), 2);
    (Pedal_Pct_in_Pedal2_ADC_Pedal_Pct_map_DataStore()) =
      CRVLAB_ECU_104_B.s38_motohawk_interpolation_1d1;
  }

  /* S-Function Block: <S38>/motohawk_ain Resource: AN3M */
  {
    extern NativeError_S AN3M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
    AN3M_AnalogInput_Get(&rtb_DataTypeConversion2_e, NULL);
  }

  /* DataTypeConversion: '<S38>/Data Type Conversion' */
  CRVLAB_ECU_104_B.s38_DataTypeConversion = (real_T)rtb_DataTypeConversion2_e;

  /* S-Function Block: <S38>/motohawk_prelookup */
  {
    extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
      ordarr[], uint32_T sz, uint16_T prev);
    (Pedal_ADC_in_Pedal_ADC_Pedal_Pct_mapIn_DataStore()) =
      CRVLAB_ECU_104_B.s38_DataTypeConversion;
    (Pedal_ADC_in_Pedal_ADC_Pedal_Pct_mapIdx_DataStore()) =
      TablePrelookup_real_T(CRVLAB_ECU_104_B.s38_DataTypeConversion,
      (Pedal_ADC_in_Pedal_ADC_Pedal_Pct_mapIdxArr_DataStore()), 2,
      (Pedal_ADC_in_Pedal_ADC_Pedal_Pct_mapIdx_DataStore()));
    rtb_motohawk_prelookup = (Pedal_ADC_in_Pedal_ADC_Pedal_Pct_mapIdx_DataStore());
  }

  /* S-Function Block: <S38>/motohawk_interpolation_1d */
  {
    extern real_T TableInterpolation1D_real_T(uint16_T idx, real_T *tbl_data,
      uint32_T sz);
    CRVLAB_ECU_104_B.s38_motohawk_interpolation_1d = TableInterpolation1D_real_T
      (rtb_motohawk_prelookup, (real_T *)
       ((Pedal_Pct_in_Pedal_ADC_Pedal_Pct_mapTbl_DataStore())), 2);
    (Pedal_Pct_in_Pedal_ADC_Pedal_Pct_map_DataStore()) =
      CRVLAB_ECU_104_B.s38_motohawk_interpolation_1d;
  }

  /* Sum: '<S49>/Add' */
  rtb_Gain4 = CRVLAB_ECU_104_B.s38_motohawk_interpolation_1d1 -
    CRVLAB_ECU_104_B.s38_motohawk_interpolation_1d;

  /* Abs: '<S49>/Abs' */
  rtb_Gain4 = fabs(rtb_Gain4);

  /* Switch: '<S49>/Switch' incorporates:
   *  RelationalOperator: '<S49>/Relational Operator'
   *  S-Function (motohawk_sfun_calibration): '<S49>/motohawk_calibration1'
   */
  if (rtb_Gain4 < (PedalDiff_Pct_DataStore())) {
    rtb_Switch1_d = CRVLAB_ECU_104_B.s38_motohawk_interpolation_1d1;
  } else {
    /* Switch: '<S49>/Switch1' incorporates:
     *  RelationalOperator: '<S49>/Relational Operator1'
     */
    if (CRVLAB_ECU_104_B.s38_motohawk_interpolation_1d1 <
        CRVLAB_ECU_104_B.s38_motohawk_interpolation_1d) {
      rtb_Switch1_d = CRVLAB_ECU_104_B.s38_motohawk_interpolation_1d1;
    } else {
      rtb_Switch1_d = CRVLAB_ECU_104_B.s38_motohawk_interpolation_1d;
    }

    /* End of Switch: '<S49>/Switch1' */
  }

  /* End of Switch: '<S49>/Switch' */

  /* MultiPortSwitch: '<S38>/Multiport Switch' incorporates:
   *  Product: '<S38>/Product1'
   *  Product: '<S38>/Product2'
   *  S-Function (motohawk_sfun_calibration): '<S38>/motohawk_calibration'
   *  S-Function (motohawk_sfun_calibration): '<S38>/motohawk_calibration1'
   */
  switch ((int32_T)rtb_Merge_p) {
   case 0:
    rtb_Gain4 = rtb_Switch1_d;
    break;

   case 1:
    rtb_Gain4 = rtb_Switch1_d * (FuelGain_DataStore());
    break;

   case 2:
    rtb_Gain4 = rtb_Switch1_d * (PowerGain_DataStore());
    break;

   default:
    rtb_Gain4 = rtb_Switch1_d;
    break;
  }

  /* End of MultiPortSwitch: '<S38>/Multiport Switch' */
  /* Sum: '<S48>/Sum2' incorporates:
   *  Constant: '<S48>/Constant'
   *  Product: '<S48>/Product'
   *  Product: '<S48>/Product2'
   *  S-Function (motohawk_sfun_calibration): '<S38>/motohawk_calibration2'
   *  Sum: '<S48>/Sum'
   *  UnitDelay: '<S48>/Unit Delay'
   */
  rtb_Sum2_h = (1.0 - (PedalFilterAlpha_DataStore())) *
    CRVLAB_ECU_104_DWork.s48_UnitDelay_DSTATE + rtb_Gain4 *
    (PedalFilterAlpha_DataStore());

  /* Saturate: '<S38>/Saturation' */
  rtb_Saturation = rtb_Sum2_h >= 100.0 ? 100.0 : rtb_Sum2_h <= 0.0 ? 0.0 :
    rtb_Sum2_h;

  /* Product: '<S38>/Divide' incorporates:
   *  Constant: '<S38>/Constant1'
   *  Sum: '<S38>/Add1'
   *  UnitDelay: '<S38>/Unit Delay'
   */
  rtb_Gain4 = (rtb_Saturation - CRVLAB_ECU_104_DWork.s38_UnitDelay_DSTATE) /
    0.01;

  /* MinMax: '<S50>/MinMax' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S38>/motohawk_calibration5'
   */
  rtb_Gain4 = (rtb_Gain4 >= (PedalRateMin_Pct_s_DataStore())) || rtIsNaN
    ((PedalRateMin_Pct_s_DataStore())) ? rtb_Gain4 :
    (PedalRateMin_Pct_s_DataStore());

  /* MinMax: '<S50>/MinMax1' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S38>/motohawk_calibration6'
   */
  rtb_MinMax1 = (rtb_Gain4 <= (PedalRateMax_Pct_s_DataStore())) || rtIsNaN
    ((PedalRateMax_Pct_s_DataStore())) ? rtb_Gain4 :
    (PedalRateMax_Pct_s_DataStore());

  /* S-Function (motohawk_sfun_data_write): '<S38>/motohawk_data_write1' */
  /* Write to Data Storage as scalar: PedalRate_Pct_s */
  {
    PedalRate_Pct_s_DataStore() = rtb_MinMax1;
  }

  /* S-Function (motohawk_sfun_data_write): '<S38>/motohawk_data_write2' */
  /* Write to Data Storage as scalar: Pedal_Pct_Raw */
  {
    Pedal_Pct_Raw_DataStore() = rtb_Saturation;
  }

  /* S-Function Block: <S39>/motohawk_ain1 Resource: AN4M */
  {
    extern NativeError_S AN4M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
    AN4M_AnalogInput_Get(&CRVLAB_ECU_104_B.s39_motohawk_ain1, NULL);
  }

  /* DataTypeConversion: '<S39>/Data Type Conversion' */
  rtb_Merge_p = (real_T)CRVLAB_ECU_104_B.s39_motohawk_ain1;

  /* Product: '<S39>/Product1' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S39>/motohawk_calibration'
   */
  rtb_Merge_p *= (RailPGain_MPa_DataStore());

  /* Sum: '<S39>/Sum1' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S39>/motohawk_calibration1'
   */
  CRVLAB_ECU_104_B.s39_Sum1 = rtb_Merge_p + (RailPOffset_MPa_DataStore());

  /* Sum: '<S51>/Sum2' incorporates:
   *  Constant: '<S51>/Constant'
   *  Product: '<S51>/Product'
   *  Product: '<S51>/Product2'
   *  S-Function (motohawk_sfun_calibration): '<S39>/motohawk_calibration2'
   *  Sum: '<S51>/Sum'
   *  UnitDelay: '<S51>/Unit Delay'
   */
  rtb_Sum2_d = (1.0 - (RailPFilterAlpha_DataStore())) *
    CRVLAB_ECU_104_DWork.s51_UnitDelay_DSTATE + CRVLAB_ECU_104_B.s39_Sum1 *
    (RailPFilterAlpha_DataStore());

  /* S-Function (motohawk_sfun_data_write): '<S39>/motohawk_data_write' */
  /* Write to Data Storage as scalar: RailP_MPa */
  {
    RailP_MPa_DataStore() = rtb_Sum2_d;
  }

  /* S-Function (motohawk_sfun_data_write): '<S39>/motohawk_data_write1' */
  /* Write to Data Storage as scalar: RailP_Instant_MPa */
  {
    RailP_Instant_MPa_DataStore() = CRVLAB_ECU_104_B.s39_Sum1;
  }

  /* S-Function Block: <S40>/motohawk_ain1 Resource: AN6M */
  {
    extern NativeError_S AN6M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
    AN6M_AnalogInput_Get(&rtb_DataTypeConversion2_e, NULL);
  }

  /* DataTypeConversion: '<S40>/Data Type Conversion1' */
  CRVLAB_ECU_104_B.s40_DataTypeConversion1 = (real_T)rtb_DataTypeConversion2_e;

  /* Product: '<S40>/Product2' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S40>/motohawk_calibration4'
   */
  rtb_Merge_p = CRVLAB_ECU_104_B.s40_DataTypeConversion1 *
    (TPSGain_Pct_ADC_DataStore());

  /* Sum: '<S40>/Sum2' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S40>/motohawk_calibration5'
   */
  rtb_Sum2_b = rtb_Merge_p + (TPSOffset_Pct_DataStore());

  /* S-Function (motohawk_sfun_data_write): '<S40>/motohawk_data_write' */
  /* Write to Data Storage as scalar: TPS_Pct */
  {
    TPS_Pct_DataStore() = rtb_Sum2_b;
  }

  /* Logic: '<S24>/Logical Operator5' incorporates:
   *  S-Function (motohawk_sfun_data_read): '<S24>/motohawk_data_read25'
   */
  rtb_LogicalOperator5 = !DieselLock_On_DataStore();

  /* Switch: '<S54>/Switch4' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S54>/motohawk_calibration6'
   *  S-Function (motohawk_sfun_calibration): '<S54>/motohawk_calibration7'
   *  S-Function (motohawk_sfun_data_read): '<S24>/motohawk_data_read27'
   */
  if (DDF_On_DataStore()) {
    rtb_Gain4 = (ECT_C_On_Theshold_DataStore());
  } else {
    rtb_Gain4 = (ECT_C_Off_Theshold_DataStore());
  }

  /* End of Switch: '<S54>/Switch4' */
  /* Sum: '<S54>/Add2' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S24>/motohawk_calibration13'
   */
  rtb_Merge_p = rtb_Gain4 + (ECTset_DataStore());

  /* RelationalOperator: '<S54>/Relational Operator' incorporates:
   *  S-Function (motohawk_sfun_data_read): '<S24>/motohawk_data_read14'
   */
  rtb_RelationalOperator_d = (rtb_Merge_p < ECT_C_DataStore());

  /* Logic: '<S24>/Logical Operator3' incorporates:
   *  S-Function (motohawk_sfun_data_read): '<S24>/motohawk_data_read20'
   */
  rtb_LogicalOperator3_a = !Normal_On_DataStore();

  /* Switch: '<S52>/Switch4' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S52>/motohawk_calibration6'
   *  S-Function (motohawk_sfun_calibration): '<S52>/motohawk_calibration7'
   *  S-Function (motohawk_sfun_data_read): '<S24>/motohawk_data_read27'
   */
  if (DDF_On_DataStore()) {
    rtb_Gain4 = (Cng_P_On_Theshold_DataStore());
  } else {
    rtb_Gain4 = (Cng_P_Off_Theshold_DataStore());
  }

  /* End of Switch: '<S52>/Switch4' */
  /* Sum: '<S52>/Add2' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S24>/motohawk_calibration15'
   */
  rtb_Merge_p = rtb_Gain4 + (CNGPLow_kPa_DataStore());

  /* RelationalOperator: '<S52>/Relational Operator' incorporates:
   *  S-Function (motohawk_sfun_data_read): '<S24>/motohawk_data_read19'
   */
  rtb_RelationalOperator_h = (rtb_Merge_p < CNGP_kPa_DataStore());

  /* S-Function Block: <S56>/motohawk_delta_time */
  {
    uint32_T delta;
    extern uint32_T Timer_FreeRunningCounter_GetDeltaUpdateReference_us(uint32_T
      * pReference_lower32Bits, uint32_T *pReference_upper32Bits);
    delta = Timer_FreeRunningCounter_GetDeltaUpdateReference_us
      (&CRVLAB_ECU_104_DWork.s56_motohawk_delta_time_DWORK1, NULL);
    rtb_motohawk_delta_time = ((real_T) delta) * 0.000001;
  }

  /* Switch: '<S56>/Switch' incorporates:
   *  Constant: '<S56>/Constant'
   *  RelationalOperator: '<S24>/Relational Operator1'
   *  S-Function (motohawk_sfun_calibration): '<S24>/motohawk_calibration14'
   *  S-Function (motohawk_sfun_data_read): '<S24>/motohawk_data_read26'
   *  S-Function (motohawk_sfun_data_read): '<S56>/motohawk_data_read'
   *  S-Function (motohawk_sfun_delta_time): '<S56>/motohawk_delta_time'
   *  Sum: '<S56>/Sum'
   */
  if (EngineSpeed_rpm_DataStore() > (DDF_On_rpm_DataStore())) {
    rtb_Gain4 = rtb_motohawk_delta_time + TimeSinceDDFEnabledl_DataStore();
  } else {
    rtb_Gain4 = 0.0;
  }

  /* End of Switch: '<S56>/Switch' */
  /* Logic: '<S24>/Logical Operator4' incorporates:
   *  RelationalOperator: '<S24>/Relational Operator5'
   *  S-Function (motohawk_sfun_calibration): '<S24>/motohawk_calibration3'
   */
  rtb_LogicalOperator4 = (rtb_LogicalOperator5 && rtb_RelationalOperator_d &&
    rtb_LogicalOperator3_a && rtb_RelationalOperator_h && (rtb_Gain4 >=
    (TimeBeforeOnDDF_DataStore())));

  /* Sum: '<S24>/Add' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S24>/motohawk_calibration18'
   *  S-Function (motohawk_sfun_data_read): '<S24>/motohawk_data_read22'
   */
  rtb_Merge_p = (EngineSpeedIdleOffset_DataStore()) + IdleSpeedSP_rpm_DataStore();

  /* Stateflow: '<S24>/Chart2' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S24>/motohawk_calibration16'
   *  S-Function (motohawk_sfun_calibration): '<S24>/motohawk_calibration17'
   *  S-Function (motohawk_sfun_calibration): '<S24>/motohawk_calibration19'
   *  S-Function (motohawk_sfun_calibration): '<S24>/motohawk_calibration20'
   *  S-Function (motohawk_sfun_calibration): '<S24>/motohawk_calibration21'
   *  S-Function (motohawk_sfun_calibration): '<S24>/motohawk_calibration22'
   *  S-Function (motohawk_sfun_data_read): '<S24>/motohawk_data_read18'
   *  S-Function (motohawk_sfun_data_read): '<S24>/motohawk_data_read21'
   *  S-Function (motohawk_sfun_data_read): '<S24>/motohawk_data_read23'
   *  S-Function (motohawk_sfun_data_read): '<S24>/motohawk_data_read24'
   */

  /* Gateway: Model/2 Stateflow
     Chart/Chart2 */
  /* During: Model/2 Stateflow
     Chart/Chart2 */
  if (CRVLAB_ECU_104_DWork.s53_is_active_c1_CRVLAB_ECU_104 == 0) {
    /* Entry: Model/2 Stateflow
       Chart/Chart2 */
    CRVLAB_ECU_104_DWork.s53_is_active_c1_CRVLAB_ECU_104 = 1U;

    /* Transition: '<S53>:14' */
    CRVLAB_ECU_104_DWork.s53_is_c1_CRVLAB_ECU_104 = CRVLAB_ECU_104_IN_Stall;

    /* Entry 'Stall': '<S53>:13' */
    CRVLAB_ECU_104_B.s53_State = 1.0;
  } else {
    switch (CRVLAB_ECU_104_DWork.s53_is_c1_CRVLAB_ECU_104) {
     case CRVLAB_ECU_104_IN_Crank:
      /* During 'Crank': '<S53>:11' */
      if ((EngineSpeed_rpm_DataStore() < (Stall_rpm_DataStore())) || ((real_T)
           StallLock_On_DataStore() > 0.5)) {
        /* Transition: '<S53>:16' */
        CRVLAB_ECU_104_DWork.s53_is_c1_CRVLAB_ECU_104 = CRVLAB_ECU_104_IN_Stall;

        /* Entry 'Stall': '<S53>:13' */
        CRVLAB_ECU_104_B.s53_State = 1.0;
      } else {
        if (EngineSpeed_rpm_DataStore() > (EngineSpeedCrankingToIdle_DataStore()))
        {
          /* Transition: '<S53>:100' */
          CRVLAB_ECU_104_DWork.s53_is_c1_CRVLAB_ECU_104 = CRVLAB_ECU_104_IN_Run;

          /* Transition: '<S53>:102' */
          CRVLAB_ECU_104_DWork.s53_is_Run = CRVLAB_ECU_104_IN_RunDiesel;

          /* Entry 'RunDiesel': '<S53>:10' */
          CRVLAB_ECU_104_B.s53_State = 3.0;
        }
      }
      break;

     case CRVLAB_ECU_104_IN_Cut:
      /* During 'Cut': '<S53>:103' */
      if ((Pedal_Pct_DataStore() > (PedalHigh_DataStore())) && ((real_T)
           IdleLock_On_DataStore() < 0.5)) {
        /* Transition: '<S53>:106' */
        CRVLAB_ECU_104_DWork.s53_is_Cut = (uint8_T)
          CRVLAB_ECU_104_IN_NO_ACTIVE_CHILD;
        CRVLAB_ECU_104_DWork.s53_is_c1_CRVLAB_ECU_104 = CRVLAB_ECU_104_IN_Run;

        /* Transition: '<S53>:102' */
        CRVLAB_ECU_104_DWork.s53_is_Run = CRVLAB_ECU_104_IN_RunDiesel;

        /* Entry 'RunDiesel': '<S53>:10' */
        CRVLAB_ECU_104_B.s53_State = 3.0;
      } else if ((EngineSpeed_rpm_DataStore() < (Stall_rpm_DataStore())) ||
                 ((real_T)StallLock_On_DataStore() > 0.5)) {
        /* Transition: '<S53>:108' */
        CRVLAB_ECU_104_DWork.s53_is_Cut = (uint8_T)
          CRVLAB_ECU_104_IN_NO_ACTIVE_CHILD;
        CRVLAB_ECU_104_DWork.s53_is_c1_CRVLAB_ECU_104 = CRVLAB_ECU_104_IN_Stall;

        /* Entry 'Stall': '<S53>:13' */
        CRVLAB_ECU_104_B.s53_State = 1.0;
      } else {
        switch (CRVLAB_ECU_104_DWork.s53_is_Cut) {
         case CRVLAB_ECU_104_IN_FuelCut:
          /* During 'FuelCut': '<S53>:97' */
          if ((EngineSpeed_rpm_DataStore() < rtb_Merge_p) || ((real_T)
               IdleLock_On_DataStore() > 0.5)) {
            /* Transition: '<S53>:93' */
            CRVLAB_ECU_104_DWork.s53_is_Cut = CRVLAB_ECU_104_IN_Idle;

            /* Entry 'Idle': '<S53>:91' */
            CRVLAB_ECU_104_B.s53_State = 5.0;
          }
          break;

         case CRVLAB_ECU_104_IN_Idle:
          /* During 'Idle': '<S53>:91' */
          if ((EngineSpeed_rpm_DataStore() >= rtb_Merge_p) && ((real_T)
               IdleLock_On_DataStore() < 0.5)) {
            /* Transition: '<S53>:94' */
            CRVLAB_ECU_104_DWork.s53_is_Cut = CRVLAB_ECU_104_IN_FuelCut;

            /* Entry 'FuelCut': '<S53>:97' */
            CRVLAB_ECU_104_B.s53_State = 6.0;
          }
          break;

         default:
          /* Transition: '<S53>:123' */
          CRVLAB_ECU_104_DWork.s53_is_Cut = CRVLAB_ECU_104_IN_Idle;

          /* Entry 'Idle': '<S53>:91' */
          CRVLAB_ECU_104_B.s53_State = 5.0;
          break;
        }
      }
      break;

     case CRVLAB_ECU_104_IN_Run:
      /* During 'Run': '<S53>:98' */
      if ((EngineSpeed_rpm_DataStore() < (Stall_rpm_DataStore())) || ((real_T)
           StallLock_On_DataStore() > 0.5)) {
        /* Transition: '<S53>:101' */
        CRVLAB_ECU_104_DWork.s53_is_Run = (uint8_T)
          CRVLAB_ECU_104_IN_NO_ACTIVE_CHILD;
        CRVLAB_ECU_104_DWork.s53_is_c1_CRVLAB_ECU_104 = CRVLAB_ECU_104_IN_Stall;

        /* Entry 'Stall': '<S53>:13' */
        CRVLAB_ECU_104_B.s53_State = 1.0;
      } else if ((Pedal_Pct_DataStore() < (PedalLow_DataStore())) || ((real_T)
                  IdleLock_On_DataStore() > 0.5)) {
        /* Transition: '<S53>:105' */
        CRVLAB_ECU_104_DWork.s53_is_Run = (uint8_T)
          CRVLAB_ECU_104_IN_NO_ACTIVE_CHILD;
        CRVLAB_ECU_104_DWork.s53_is_c1_CRVLAB_ECU_104 = CRVLAB_ECU_104_IN_Cut;

        /* Transition: '<S53>:123' */
        CRVLAB_ECU_104_DWork.s53_is_Cut = CRVLAB_ECU_104_IN_Idle;

        /* Entry 'Idle': '<S53>:91' */
        CRVLAB_ECU_104_B.s53_State = 5.0;
      } else {
        switch (CRVLAB_ECU_104_DWork.s53_is_Run) {
         case CRVLAB_ECU_104_IN_RunDDF:
          /* During 'RunDDF': '<S53>:26' */
          if (((real_T)rtb_LogicalOperator4 < 0.5) || (EngineSpeed_rpm_DataStore
               () > (ddf2diesel_rpm_DataStore()))) {
            /* Transition: '<S53>:25' */
            CRVLAB_ECU_104_DWork.s53_is_Run = CRVLAB_ECU_104_IN_RunDiesel;

            /* Entry 'RunDiesel': '<S53>:10' */
            CRVLAB_ECU_104_B.s53_State = 3.0;
          }
          break;

         case CRVLAB_ECU_104_IN_RunDiesel:
          /* During 'RunDiesel': '<S53>:10' */
          if (((real_T)rtb_LogicalOperator4 >= 0.5) &&
              (EngineSpeed_rpm_DataStore() <= (diesel2ddf_rpm_DataStore()))) {
            /* Transition: '<S53>:21' */
            CRVLAB_ECU_104_DWork.s53_is_Run = CRVLAB_ECU_104_IN_RunDDF;

            /* Entry 'RunDDF': '<S53>:26' */
            CRVLAB_ECU_104_B.s53_State = 4.0;
          }
          break;

         default:
          /* Transition: '<S53>:102' */
          CRVLAB_ECU_104_DWork.s53_is_Run = CRVLAB_ECU_104_IN_RunDiesel;

          /* Entry 'RunDiesel': '<S53>:10' */
          CRVLAB_ECU_104_B.s53_State = 3.0;
          break;
        }
      }
      break;

     case CRVLAB_ECU_104_IN_Stall:
      /* During 'Stall': '<S53>:13' */
      if ((real_T)StallLock_On_DataStore() > 0.5) {
        /* Transition: '<S53>:127' */
        CRVLAB_ECU_104_DWork.s53_is_c1_CRVLAB_ECU_104 = CRVLAB_ECU_104_IN_Stall;

        /* Entry 'Stall': '<S53>:13' */
        CRVLAB_ECU_104_B.s53_State = 1.0;
      } else {
        if (EngineSpeed_rpm_DataStore() > (Stall_rpm_DataStore())) {
          /* Transition: '<S53>:15' */
          CRVLAB_ECU_104_DWork.s53_is_c1_CRVLAB_ECU_104 =
            CRVLAB_ECU_104_IN_Crank;

          /* Entry 'Crank': '<S53>:11' */
          CRVLAB_ECU_104_B.s53_State = 2.0;
        }
      }
      break;

     default:
      /* Transition: '<S53>:14' */
      CRVLAB_ECU_104_DWork.s53_is_c1_CRVLAB_ECU_104 = CRVLAB_ECU_104_IN_Stall;

      /* Entry 'Stall': '<S53>:13' */
      CRVLAB_ECU_104_B.s53_State = 1.0;
      break;
    }
  }

  /* End of Stateflow: '<S24>/Chart2' */

  /* DataTypeConversion: '<S24>/Data Type Conversion1' */
  if (rtIsNaN(CRVLAB_ECU_104_B.s53_State) || rtIsInf(CRVLAB_ECU_104_B.s53_State))
  {
    rtb_Add_i_idx = 0.0;
  } else {
    rtb_Add_i_idx = fmod(floor(CRVLAB_ECU_104_B.s53_State), 256.0);
  }

  rtb_motohawk_data_read1_nr = (uint8_T)(rtb_Add_i_idx < 0.0 ? (uint8_T)(int32_T)
    (int8_T)-(int8_T)(uint8_T)-rtb_Add_i_idx : (uint8_T)rtb_Add_i_idx);

  /* End of DataTypeConversion: '<S24>/Data Type Conversion1' */

  /* S-Function (motohawk_sfun_data_write): '<S24>/motohawk_data_write4' */
  /* Write to Data Storage as scalar: State */
  {
    State_DataStore() = rtb_motohawk_data_read1_nr;
  }

  /* S-Function (motohawk_sfun_data_write): '<S24>/motohawk_data_write5' */
  /* Write to Data Storage as scalar: DDF_On_Cmd */
  {
    DDF_On_Cmd_DataStore() = rtb_LogicalOperator4;
  }

  /* Saturate: '<S56>/Saturation' */
  rtb_Saturation_e = rtb_Gain4 >= 16000.0 ? 16000.0 : rtb_Gain4 <= 0.0 ? 0.0 :
    rtb_Gain4;

  /* S-Function (motohawk_sfun_data_write): '<S56>/motohawk_data_write' */
  /* Write to Data Storage as scalar: TimeSinceDDFEnabledl */
  {
    TimeSinceDDFEnabledl_DataStore() = rtb_Saturation_e;
  }

  /* S-Function (motohawk_sfun_data_read): '<S24>/motohawk_data_read11' */
  rtb_motohawk_data_read11 = EngineSpeed_rpm_DataStore();

  /* S-Function Block: <S24>/motohawk_prelookup3 */
  {
    extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
      ordarr[], uint32_T sz, uint16_T prev);
    (EngineSpeed_rpm_for_Pedal_PrefilterIn_DataStore()) =
      rtb_motohawk_data_read11;
    (EngineSpeed_rpm_for_Pedal_PrefilterIdx_DataStore()) = TablePrelookup_real_T
      (rtb_motohawk_data_read11,
       (EngineSpeed_rpm_for_Pedal_PrefilterIdxArr_DataStore()), 10,
       (EngineSpeed_rpm_for_Pedal_PrefilterIdx_DataStore()));
    rtb_motohawk_prelookup3 = (EngineSpeed_rpm_for_Pedal_PrefilterIdx_DataStore());
  }

  /* S-Function (motohawk_sfun_data_read): '<S24>/motohawk_data_read12' */
  rtb_motohawk_data_read12 = Pedal_Pct_Raw_DataStore();

  /* S-Function Block: <S24>/motohawk_prelookup2 */
  {
    extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
      ordarr[], uint32_T sz, uint16_T prev);
    (Pedal_pct_for_Pedal_PrefilterIn_DataStore()) = rtb_motohawk_data_read12;
    (Pedal_pct_for_Pedal_PrefilterIdx_DataStore()) = TablePrelookup_real_T
      (rtb_motohawk_data_read12, (Pedal_pct_for_Pedal_PrefilterIdxArr_DataStore()),
       11, (Pedal_pct_for_Pedal_PrefilterIdx_DataStore()));
    rtb_motohawk_prelookup2 = (Pedal_pct_for_Pedal_PrefilterIdx_DataStore());
  }

  /* S-Function Block: <S24>/motohawk_interpolation_2d1 */
  {
    extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
      real_T *map_data, uint32_T row_sz, uint32_T col_sz);
    rtb_Gain4 = TableInterpolation2D_real_T(rtb_motohawk_prelookup3,
      rtb_motohawk_prelookup2, (real_T *) ((Pedal_PrefilterMap_DataStore())), 10,
      11);
    (Pedal_Prefilter_DataStore()) = rtb_Gain4;
  }

  /* MinMax: '<S55>/MinMax' incorporates:
   *  Constant: '<S24>/Constant'
   */
  rtb_Gain4 = rtb_Gain4 >= 0.0 ? rtb_Gain4 : 0.0;

  /* S-Function (motohawk_sfun_data_read): '<S24>/motohawk_data_read16' */
  rtb_motohawk_data_read16 = MAFP_kPa_DataStore();

  /* Switch: '<S24>/Switch' incorporates:
   *  S-Function (motohawk_sfun_data_read): '<S24>/motohawk_data_read13'
   *  S-Function (motohawk_sfun_data_read): '<S24>/motohawk_data_read15'
   *  S-Function (motohawk_sfun_data_read): '<S24>/motohawk_data_read16'
   *  S-Function (motohawk_sfun_data_read): '<S24>/motohawk_data_read17'
   *  S-Function (motohawk_sfun_interpolation_2d): '<S24>/motohawk_interpolation_2d2'
   *  S-Function (motohawk_sfun_interpolation_2d): '<S24>/motohawk_interpolation_2d3'
   *  S-Function (motohawk_sfun_prelookup): '<S24>/motohawk_prelookup1'
   *  S-Function (motohawk_sfun_prelookup): '<S24>/motohawk_prelookup4'
   *  S-Function (motohawk_sfun_prelookup): '<S24>/motohawk_prelookup5'
   *  S-Function (motohawk_sfun_prelookup): '<S24>/motohawk_prelookup6'
   */
  if (DDF_On_DataStore()) {
    /* S-Function Block: <S24>/motohawk_prelookup1 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (MAP_kPa_for_Pedal_SmokeLimitDDFIn_DataStore()) = rtb_motohawk_data_read16;
      (MAP_kPa_for_Pedal_SmokeLimitDDFIdx_DataStore()) = TablePrelookup_real_T
        (rtb_motohawk_data_read16,
         (MAP_kPa_for_Pedal_SmokeLimitDDFIdxArr_DataStore()), 11,
         (MAP_kPa_for_Pedal_SmokeLimitDDFIdx_DataStore()));
      rtb_motohawk_prelookup1_ga = (MAP_kPa_for_Pedal_SmokeLimitDDFIdx_DataStore
                                    ());
    }

    /* S-Function (motohawk_sfun_data_read): '<S24>/motohawk_data_read13' */
    rtb_motohawk_data_read13_o = EngineSpeed_rpm_DataStore();

    /* S-Function Block: <S24>/motohawk_prelookup4 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_Pedal_SmokeLimitDDFIn_DataStore()) =
        rtb_motohawk_data_read13_o;
      (EngineSpeed_rpm_for_Pedal_SmokeLimitDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read13_o,
        (EngineSpeed_rpm_for_Pedal_SmokeLimitDDFIdxArr_DataStore()), 10,
        (EngineSpeed_rpm_for_Pedal_SmokeLimitDDFIdx_DataStore()));
      rtb_motohawk_prelookup4_oj =
        (EngineSpeed_rpm_for_Pedal_SmokeLimitDDFIdx_DataStore());
    }

    /* S-Function Block: <S24>/motohawk_interpolation_2d2 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d2_m = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup4_oj, rtb_motohawk_prelookup1_ga, (real_T *)
         ((Pedal_SmokeLimitDDFMap_DataStore())), 10, 11);
      (Pedal_SmokeLimitDDF_DataStore()) = rtb_motohawk_interpolation_2d2_m;
    }

    rtb_Gain2 = rtb_motohawk_interpolation_2d2_m;
  } else {
    /* S-Function Block: <S24>/motohawk_prelookup5 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (MAP_kPa_for_Pedal_SmokeLimitIn_DataStore()) = rtb_motohawk_data_read16;
      (MAP_kPa_for_Pedal_SmokeLimitIdx_DataStore()) = TablePrelookup_real_T
        (rtb_motohawk_data_read16, (MAP_kPa_for_Pedal_SmokeLimitIdxArr_DataStore
          ()), 11, (MAP_kPa_for_Pedal_SmokeLimitIdx_DataStore()));
      rtb_motohawk_prelookup5_g = (MAP_kPa_for_Pedal_SmokeLimitIdx_DataStore());
    }

    /* S-Function (motohawk_sfun_data_read): '<S24>/motohawk_data_read15' */
    rtb_motohawk_data_read15_i = EngineSpeed_rpm_DataStore();

    /* S-Function Block: <S24>/motohawk_prelookup6 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_Pedal_SmokeLimitIn_DataStore()) =
        rtb_motohawk_data_read15_i;
      (EngineSpeed_rpm_for_Pedal_SmokeLimitIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read15_i,
        (EngineSpeed_rpm_for_Pedal_SmokeLimitIdxArr_DataStore()), 10,
        (EngineSpeed_rpm_for_Pedal_SmokeLimitIdx_DataStore()));
      rtb_motohawk_prelookup6_j =
        (EngineSpeed_rpm_for_Pedal_SmokeLimitIdx_DataStore());
    }

    /* S-Function Block: <S24>/motohawk_interpolation_2d3 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d3 = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup6_j, rtb_motohawk_prelookup5_g, (real_T *)
         ((Pedal_SmokeLimitMap_DataStore())), 10, 11);
      (Pedal_SmokeLimit_DataStore()) = rtb_motohawk_interpolation_2d3;
    }

    rtb_Gain2 = rtb_motohawk_interpolation_2d3;
  }

  /* End of Switch: '<S24>/Switch' */

  /* MinMax: '<S55>/MinMax1' */
  rtb_MinMax1_f = (rtb_Gain4 <= rtb_Gain2) || rtIsNaN(rtb_Gain2) ? rtb_Gain4 :
    rtb_Gain2;

  /* S-Function (motohawk_sfun_data_write): '<S24>/motohawk_data_write3' */
  /* Write to Data Storage as scalar: Pedal_Pct */
  {
    Pedal_Pct_DataStore() = rtb_MinMax1_f;
  }

  /* S-Function (motohawk_sfun_data_read): '<S57>/motohawk_data_read1' */
  rtb_motohawk_data_read1_g = EngineSpeed_rpm_DataStore();

  /* S-Function (motohawk_sfun_data_read): '<S57>/motohawk_data_read2' */
  rtb_motohawk_data_read2_o = Pedal_Pct_DataStore();

  /* Switch: '<S68>/Switch2' incorporates:
   *  Constant: '<S68>/Constant1'
   */
  CRVLAB_ECU_104_B.s68_Switch2 = 1.0;

  /* S-Function (motohawk_sfun_data_read): '<S57>/motohawk_data_read4' */
  rtb_motohawk_data_read4_m = AC_On_DataStore();

  /* MultiPortSwitch: '<S57>/MultiSwitch' incorporates:
   *  Constant: '<S57>/Constant'
   *  Constant: '<S57>/Constant3'
   *  Product: '<S57>/Product2'
   *  Product: '<S57>/Product3'
   *  S-Function (motohawk_sfun_data_read): '<S57>/motohawk_data_read'
   *  S-Function (motohawk_sfun_data_read): '<S57>/motohawk_data_read1'
   *  S-Function (motohawk_sfun_data_read): '<S57>/motohawk_data_read2'
   *  S-Function (motohawk_sfun_interpolation_2d): '<S57>/motohawk_interpolation_2d2'
   *  S-Function (motohawk_sfun_prelookup): '<S57>/motohawk_prelookup1'
   *  S-Function (motohawk_sfun_prelookup): '<S57>/motohawk_prelookup4'
   */
  switch (((uint8_T)State_DataStore())) {
   case 1:
    rtb_Gain2 = 0.0;
    break;

   case 2:
    /* Switch: '<S57>/Switch' incorporates:
     *  Constant: '<S57>/Constant1'
     */
    rtb_Gain2 = 0.0;
    break;

   case 3:
    rtb_Gain2 = 0.0;
    break;

   case 4:
    /* Sum: '<S57>/Add1' incorporates:
     *  Constant: '<S57>/Constant10'
     *  Product: '<S57>/Product4'
     *  S-Function (motohawk_sfun_calibration): '<S57>/motohawk_calibration2'
     *  S-Function (motohawk_sfun_data_read): '<S57>/motohawk_data_read6'
     */
    rtb_Merge_p = PedalRate_Pct_s_DataStore() * (PedalRateGain_DataStore()) +
      1.0;

    /* S-Function Block: <S57>/motohawk_prelookup1 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (Pedal_pct_for_CNGFueling_ms_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read2_o;
      (Pedal_pct_for_CNGFueling_ms_in_RunDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read2_o,
        (Pedal_pct_for_CNGFueling_ms_in_RunDDFIdxArr_DataStore()), 11,
        (Pedal_pct_for_CNGFueling_ms_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup1_a =
        (Pedal_pct_for_CNGFueling_ms_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S57>/motohawk_prelookup4 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_CNGFueling_ms_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read1_g;
      (EngineSpeed_rpm_for_CNGFueling_ms_in_RunDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_g,
        (EngineSpeed_rpm_for_CNGFueling_ms_in_RunDDFIdxArr_DataStore()), 10,
        (EngineSpeed_rpm_for_CNGFueling_ms_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup4_oq =
        (EngineSpeed_rpm_for_CNGFueling_ms_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S57>/motohawk_interpolation_2d2 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d2_h = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup4_oq, rtb_motohawk_prelookup1_a, (real_T *)
         ((CNGFueling_ms_in_RunDDFMap_DataStore())), 10, 11);
      (CNGFueling_ms_in_RunDDF_DataStore()) = rtb_motohawk_interpolation_2d2_h;
    }

    rtb_Gain2 = rtb_motohawk_interpolation_2d2_h * CRVLAB_ECU_104_B.s68_Switch2 *
      rtb_Merge_p;
    break;

   case 5:
    /* MultiPortSwitch: '<S69>/Multiport Switch' incorporates:
     *  Constant: '<S69>/Constant'
     *  Product: '<S69>/Product'
     *  S-Function (motohawk_sfun_calibration): '<S69>/motohawk_calibration1'
     *  S-Function (motohawk_sfun_calibration): '<S69>/motohawk_calibration2'
     *  S-Function (motohawk_sfun_calibration): '<S69>/motohawk_calibration3'
     *  S-Function (motohawk_sfun_calibration): '<S69>/motohawk_calibration4'
     *  S-Function (motohawk_sfun_data_read): '<S57>/motohawk_data_read5'
     *  Sum: '<S69>/Add'
     */
    switch (rtb_motohawk_data_read4_m * 2 + DDF_On_DataStore()) {
     case 0:
      rtb_Merge_p = (CNGFuelingIdleACOffDDFOff_ms_DataStore());
      break;

     case 1:
      rtb_Merge_p = (CNGFuelingIdleACOffDDFOn_ms_DataStore());
      break;

     case 2:
      rtb_Merge_p = (CNGFuelingIdleACOnDDFOff_ms_DataStore());
      break;

     default:
      rtb_Merge_p = (CNGFuelingIdleACOnDDFOn_ms_DataStore());
      break;
    }

    /* End of MultiPortSwitch: '<S69>/Multiport Switch' */
    rtb_Gain2 = rtb_Merge_p * CRVLAB_ECU_104_B.s68_Switch2;
    break;

   default:
    /* Logic: '<S57>/Logical Operator1' incorporates:
     *  S-Function (motohawk_sfun_data_read): '<S57>/motohawk_data_read8'
     */
    rtb_LogicalOperator5 = !Neutral_On_DataStore();

    /* Switch: '<S57>/Switch1' incorporates:
     *  Constant: '<S57>/Constant2'
     *  Logic: '<S57>/Logical Operator'
     *  S-Function (motohawk_sfun_calibration): '<S57>/motohawk_calibration3'
     *  S-Function (motohawk_sfun_data_read): '<S57>/motohawk_data_read7'
     *  S-Function (motohawk_sfun_data_read): '<S57>/motohawk_data_read9'
     */
    if (FCasIdle_On_DataStore() && rtb_LogicalOperator5 && DDF_On_DataStore()) {
      rtb_Gain2 = (FuelCutAsIdle_CNG_ms_DataStore());
    } else {
      rtb_Gain2 = 0.0;
    }

    /* End of Switch: '<S57>/Switch1' */
    break;
  }

  /* End of MultiPortSwitch: '<S57>/MultiSwitch' */

  /* Product: '<S57>/Divide' incorporates:
   *  Constant: '<S57>/Constant4'
   *  Constant: '<S57>/Constant5'
   *  Product: '<S57>/Product'
   *  Product: '<S57>/Product1'
   *  S-Function (motohawk_sfun_data_read): '<S57>/motohawk_data_read1'
   */
  rtb_Merge_p = rtb_Gain2 * rtb_motohawk_data_read1_g * 360.0 / 60000.0;

  /* Product: '<S70>/Product' incorporates:
   *  Constant: '<S70>/Constant'
   */
  rtb_Gain4 = (real_T)rtb_motohawk_data_read4_m * 2.0;

  /* MultiPortSwitch: '<S70>/Multiport Switch' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S70>/motohawk_calibration1'
   *  S-Function (motohawk_sfun_calibration): '<S70>/motohawk_calibration2'
   *  S-Function (motohawk_sfun_calibration): '<S70>/motohawk_calibration3'
   *  S-Function (motohawk_sfun_calibration): '<S70>/motohawk_calibration4'
   *  S-Function (motohawk_sfun_data_read): '<S57>/motohawk_data_read5'
   *  Sum: '<S70>/Add'
   */
  switch ((int32_T)(rtb_Gain4 + (real_T)DDF_On_DataStore())) {
   case 0:
    rtb_Switch1_d = (CNGEOIIdleACOffDDFOff_degBTDC_DataStore());
    break;

   case 1:
    rtb_Switch1_d = (CNGEOIIdleACOffDDFOn_degBTDC_DataStore());
    break;

   case 2:
    rtb_Switch1_d = (CNGEOIIdleACOnDDFOff_degBTDC_DataStore());
    break;

   default:
    rtb_Switch1_d = (CNGEOIIdleACOnDDFOn_degBTDC_DataStore());
    break;
  }

  /* End of MultiPortSwitch: '<S70>/Multiport Switch' */

  /* MultiPortSwitch: '<S57>/MultiSwitch1' incorporates:
   *  Constant: '<S57>/Constant6'
   *  Constant: '<S57>/Constant7'
   *  S-Function (motohawk_sfun_calibration): '<S57>/motohawk_calibration1'
   *  S-Function (motohawk_sfun_data_read): '<S57>/motohawk_data_read'
   *  S-Function (motohawk_sfun_data_read): '<S57>/motohawk_data_read1'
   *  S-Function (motohawk_sfun_data_read): '<S57>/motohawk_data_read2'
   *  S-Function (motohawk_sfun_interpolation_2d): '<S57>/motohawk_interpolation_2d4'
   *  S-Function (motohawk_sfun_prelookup): '<S57>/motohawk_prelookup5'
   *  S-Function (motohawk_sfun_prelookup): '<S57>/motohawk_prelookup8'
   */
  switch (((uint8_T)State_DataStore())) {
   case 1:
    CRVLAB_ECU_104_B.s57_MultiSwitch1 = 240.0;
    break;

   case 2:
    CRVLAB_ECU_104_B.s57_MultiSwitch1 = (CNGEOIinCrank_degBTDC_DataStore());
    break;

   case 3:
    CRVLAB_ECU_104_B.s57_MultiSwitch1 = 240.0;
    break;

   case 4:
    /* S-Function Block: <S57>/motohawk_prelookup5 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (Pedal_pct_for_CNGEOI_degBTDC_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read2_o;
      (Pedal_pct_for_CNGEOI_degBTDC_in_RunDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read2_o,
        (Pedal_pct_for_CNGEOI_degBTDC_in_RunDDFIdxArr_DataStore()), 11,
        (Pedal_pct_for_CNGEOI_degBTDC_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup5_jm =
        (Pedal_pct_for_CNGEOI_degBTDC_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S57>/motohawk_prelookup8 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_CNGEOI_degBTDC_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read1_g;
      (EngineSpeed_rpm_for_CNGEOI_degBTDC_in_RunDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_g,
        (EngineSpeed_rpm_for_CNGEOI_degBTDC_in_RunDDFIdxArr_DataStore()), 10,
        (EngineSpeed_rpm_for_CNGEOI_degBTDC_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup8 =
        (EngineSpeed_rpm_for_CNGEOI_degBTDC_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S57>/motohawk_interpolation_2d4 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d4 = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup8, rtb_motohawk_prelookup5_jm, (real_T *)
         ((CNGEOI_degBTDC_in_RunDDFMap_DataStore())), 10, 11);
      (CNGEOI_degBTDC_in_RunDDF_DataStore()) = rtb_motohawk_interpolation_2d4;
    }

    CRVLAB_ECU_104_B.s57_MultiSwitch1 = rtb_motohawk_interpolation_2d4;
    break;

   case 5:
    CRVLAB_ECU_104_B.s57_MultiSwitch1 = rtb_Switch1_d;
    break;

   default:
    CRVLAB_ECU_104_B.s57_MultiSwitch1 = rtb_Switch1_d;
    break;
  }

  /* End of MultiPortSwitch: '<S57>/MultiSwitch1' */

  /* Sum: '<S57>/Add' */
  rtb_Add_n = rtb_Merge_p + CRVLAB_ECU_104_B.s57_MultiSwitch1;

  /* S-Function (motohawk_sfun_data_write): '<S57>/motohawk_data_write' */
  /* Write to Data Storage as scalar: CNGSOI_degBTDC */
  {
    CNGSOI_degBTDC_DataStore() = rtb_Add_n;
  }

  /* S-Function (motohawk_sfun_data_write): '<S57>/motohawk_data_write1' */
  /* Write to Data Storage as scalar: CNGFueling_ms */
  {
    CNGFueling_ms_DataStore() = rtb_Gain2;
  }

  /* S-Function Block: <S57>/motohawk_prelookup2 */
  {
    extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
      ordarr[], uint32_T sz, uint16_T prev);
    (EngineSpeed_rpm_for_CNGFueling_ms_in_CrankDDFIn_DataStore()) =
      rtb_motohawk_data_read1_g;
    (EngineSpeed_rpm_for_CNGFueling_ms_in_CrankDDFIdx_DataStore()) =
      TablePrelookup_real_T(rtb_motohawk_data_read1_g,
      (EngineSpeed_rpm_for_CNGFueling_ms_in_CrankDDFIdxArr_DataStore()), 8,
      (EngineSpeed_rpm_for_CNGFueling_ms_in_CrankDDFIdx_DataStore()));
    rtb_motohawk_prelookup2_k =
      (EngineSpeed_rpm_for_CNGFueling_ms_in_CrankDDFIdx_DataStore());
  }

  /* S-Function Block: <S57>/motohawk_interpolation_1d1 */
  {
    extern real_T TableInterpolation1D_real_T(uint16_T idx, real_T *tbl_data,
      uint32_T sz);
    rtb_Gain2 = TableInterpolation1D_real_T(rtb_motohawk_prelookup2_k, (real_T *)
      ((CNGFueling_ms_in_CrankDDFTbl_DataStore())), 8);
    (CNGFueling_ms_in_CrankDDF_DataStore()) = rtb_Gain2;
  }

  /* S-Function (motohawk_sfun_data_read): '<S58>/motohawk_data_read1' */
  rtb_motohawk_data_read1_df = EngineSpeed_rpm_DataStore();

  /* S-Function (motohawk_sfun_data_read): '<S58>/motohawk_data_read2' */
  rtb_motohawk_data_read2_c = Pedal_Pct_DataStore();

  /* MultiPortSwitch: '<S58>/MultiSwitch' incorporates:
   *  Constant: '<S58>/Constant'
   *  Constant: '<S58>/Constant1'
   *  Product: '<S58>/Product'
   *  S-Function (motohawk_sfun_calibration): '<S58>/motohawk_calibration2'
   *  S-Function (motohawk_sfun_calibration): '<S58>/motohawk_calibration3'
   *  S-Function (motohawk_sfun_calibration): '<S58>/motohawk_calibration4'
   *  S-Function (motohawk_sfun_data_read): '<S58>/motohawk_data_read'
   *  S-Function (motohawk_sfun_data_read): '<S58>/motohawk_data_read1'
   *  S-Function (motohawk_sfun_data_read): '<S58>/motohawk_data_read2'
   *  S-Function (motohawk_sfun_interpolation_2d): '<S58>/motohawk_interpolation_2d1'
   *  S-Function (motohawk_sfun_prelookup): '<S58>/motohawk_prelookup2'
   *  S-Function (motohawk_sfun_prelookup): '<S58>/motohawk_prelookup3'
   *  Sum: '<S58>/Add'
   */
  switch (((uint8_T)State_DataStore())) {
   case 1:
    rtb_Merge_p = 0.0;
    break;

   case 2:
    rtb_Merge_p = 0.0;
    break;

   case 3:
    /* S-Function Block: <S58>/motohawk_prelookup2 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (Pedal_pct_for_EGR_Pct_in_RunDieselIn_DataStore()) =
        rtb_motohawk_data_read2_c;
      (Pedal_pct_for_EGR_Pct_in_RunDieselIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read2_c,
        (Pedal_pct_for_EGR_Pct_in_RunDieselIdxArr_DataStore()), 11,
        (Pedal_pct_for_EGR_Pct_in_RunDieselIdx_DataStore()));
      rtb_motohawk_prelookup2_ab =
        (Pedal_pct_for_EGR_Pct_in_RunDieselIdx_DataStore());
    }

    /* S-Function Block: <S58>/motohawk_prelookup3 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_EGR_Pct_in_RunDieselIn_DataStore()) =
        rtb_motohawk_data_read1_df;
      (EngineSpeed_rpm_for_EGR_Pct_in_RunDieselIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_df,
        (EngineSpeed_rpm_for_EGR_Pct_in_RunDieselIdxArr_DataStore()), 10,
        (EngineSpeed_rpm_for_EGR_Pct_in_RunDieselIdx_DataStore()));
      rtb_motohawk_prelookup3_g =
        (EngineSpeed_rpm_for_EGR_Pct_in_RunDieselIdx_DataStore());
    }

    /* S-Function Block: <S58>/motohawk_interpolation_2d1 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d1_b = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup3_g, rtb_motohawk_prelookup2_ab, (real_T *)
         ((EGR_Pct_in_RunDieselMap_DataStore())), 10, 11);
      (EGR_Pct_in_RunDiesel_DataStore()) = rtb_motohawk_interpolation_2d1_b;
    }

    rtb_Merge_p = rtb_motohawk_interpolation_2d1_b;
    break;

   case 4:
    /* S-Function Block: <S58>/motohawk_prelookup1 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (Pedal_pct_for_EGR_Pct_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read2_c;
      (Pedal_pct_for_EGR_Pct_in_RunDDFIdx_DataStore()) = TablePrelookup_real_T
        (rtb_motohawk_data_read2_c,
         (Pedal_pct_for_EGR_Pct_in_RunDDFIdxArr_DataStore()), 11,
         (Pedal_pct_for_EGR_Pct_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup1_em = (Pedal_pct_for_EGR_Pct_in_RunDDFIdx_DataStore
                                    ());
    }

    /* S-Function Block: <S58>/motohawk_prelookup4 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_EGR_Pct_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read1_df;
      (EngineSpeed_rpm_for_EGR_Pct_in_RunDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_df,
        (EngineSpeed_rpm_for_EGR_Pct_in_RunDDFIdxArr_DataStore()), 10,
        (EngineSpeed_rpm_for_EGR_Pct_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup4_l =
        (EngineSpeed_rpm_for_EGR_Pct_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S58>/motohawk_interpolation_2d2 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d2_d = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup4_l, rtb_motohawk_prelookup1_em, (real_T *)
         ((EGR_Pct_in_RunDDFMap_DataStore())), 10, 11);
      (EGR_Pct_in_RunDDF_DataStore()) = rtb_motohawk_interpolation_2d2_d;
    }

    rtb_Merge_p = rtb_motohawk_interpolation_2d2_d * (real_T)
      (EGRSPDDFGain_DataStore()) + (real_T)(EGRSPDDFOffset_Pct_DataStore());
    break;

   case 5:
    /* S-Function (motohawk_sfun_data_read): '<S58>/motohawk_data_read6' */
    rtb_motohawk_data_read6_c = AC_On_DataStore();

    /* MultiPortSwitch: '<S71>/Multiport Switch' incorporates:
     *  Constant: '<S71>/Constant'
     *  Product: '<S71>/Product'
     *  S-Function (motohawk_sfun_calibration): '<S71>/motohawk_calibration1'
     *  S-Function (motohawk_sfun_calibration): '<S71>/motohawk_calibration2'
     *  S-Function (motohawk_sfun_calibration): '<S71>/motohawk_calibration3'
     *  S-Function (motohawk_sfun_calibration): '<S71>/motohawk_calibration4'
     *  S-Function (motohawk_sfun_data_read): '<S58>/motohawk_data_read5'
     *  Sum: '<S71>/Add'
     */
    switch (rtb_motohawk_data_read6_c * 2 + DDF_On_DataStore()) {
     case 0:
      rtb_Merge_p = (EGRdleACOffDDFOffPct_DataStore());
      break;

     case 1:
      rtb_Merge_p = (EGRdleACOffDDFOnPct_DataStore());
      break;

     case 2:
      rtb_Merge_p = (EGRdleACOnDDFOffPct_DataStore());
      break;

     default:
      rtb_Merge_p = (EGRdleACOnDDFOnPct_DataStore());
      break;
    }

    /* End of MultiPortSwitch: '<S71>/Multiport Switch' */
    break;

   default:
    rtb_Merge_p = (EGRSPFuelcut_Pct_DataStore());
    break;
  }

  /* End of MultiPortSwitch: '<S58>/MultiSwitch' */
  /* If: '<S72>/If' incorporates:
   *  Inport: '<S73>/In1'
   *  Inport: '<S74>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S72>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S72>/override_enable'
   */
  if ((EGRSPOverride_Pct_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S72>/NewValue' incorporates:
     *  ActionPort: '<S73>/Action Port'
     */
    rtb_Merge_n = (EGRSPOverride_Pct_new_DataStore());

    /* End of Outputs for SubSystem: '<S72>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S72>/OldValue' incorporates:
     *  ActionPort: '<S74>/Action Port'
     */
    rtb_Merge_n = rtb_Merge_p;

    /* End of Outputs for SubSystem: '<S72>/OldValue' */
  }

  /* End of If: '<S72>/If' */

  /* S-Function (motohawk_sfun_data_write): '<S58>/motohawk_data_write' */
  /* Write to Data Storage as scalar: EGRSP_Pct */
  {
    EGRSP_Pct_DataStore() = rtb_Merge_n;
  }

  /* Stateflow: '<S59>/Chart' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S59>/motohawk_calibration2'
   *  S-Function (motohawk_sfun_data_read): '<S59>/motohawk_data_read1'
   */

  /* Gateway: Model/3 Set-point
     Generator/IdleSpeedSP_rpm/Chart */
  /* During: Model/3 Set-point
     Generator/IdleSpeedSP_rpm/Chart */
  if (CRVLAB_ECU_104_DWork.s75_is_active_c2_CRVLAB_ECU_104 == 0) {
    /* Entry: Model/3 Set-point
       Generator/IdleSpeedSP_rpm/Chart */
    CRVLAB_ECU_104_DWork.s75_is_active_c2_CRVLAB_ECU_104 = 1U;

    /* Transition: '<S75>:7' */
    CRVLAB_ECU_104_DWork.s75_is_c2_CRVLAB_ECU_104 = CRVLAB_ECU_104_IN_normal;

    /* Entry 'normal': '<S75>:6' */
    CRVLAB_ECU_104_B.s75_speedsp = 750.0;
  } else {
    switch (CRVLAB_ECU_104_DWork.s75_is_c2_CRVLAB_ECU_104) {
     case CRVLAB_ECU_104_IN_cranktoidle:
      /* During 'cranktoidle': '<S75>:12' */
      if (CRVLAB_ECU_104_B.s75_speedsp < 750.0) {
        /* Transition: '<S75>:15' */
        CRVLAB_ECU_104_DWork.s75_is_c2_CRVLAB_ECU_104 = CRVLAB_ECU_104_IN_normal;

        /* Entry 'normal': '<S75>:6' */
        CRVLAB_ECU_104_B.s75_speedsp = 750.0;
      } else {
        CRVLAB_ECU_104_B.s75_speedsp = CRVLAB_ECU_104_B.s75_speedsp -
          (IdleSpeedReduce_i_DataStore());
      }
      break;

     case CRVLAB_ECU_104_IN_normal:
      /* During 'normal': '<S75>:6' */
      if (EngineSpeed_rpm_DataStore() < 200.0) {
        /* Transition: '<S75>:9' */
        CRVLAB_ECU_104_DWork.s75_is_c2_CRVLAB_ECU_104 = CRVLAB_ECU_104_IN_stall;

        /* Entry 'stall': '<S75>:8' */
        CRVLAB_ECU_104_B.s75_speedsp = 0.0;
      }
      break;

     case CRVLAB_ECU_104_IN_stall:
      /* During 'stall': '<S75>:8' */
      if (EngineSpeed_rpm_DataStore() > 700.0) {
        /* Transition: '<S75>:11' */
        CRVLAB_ECU_104_DWork.s75_is_c2_CRVLAB_ECU_104 =
          CRVLAB_ECU_104_IN_cranktoidle;

        /* Entry 'cranktoidle': '<S75>:12' */
        CRVLAB_ECU_104_B.s75_speedsp = 1000.0;
      }
      break;

     default:
      /* Transition: '<S75>:7' */
      CRVLAB_ECU_104_DWork.s75_is_c2_CRVLAB_ECU_104 = CRVLAB_ECU_104_IN_normal;

      /* Entry 'normal': '<S75>:6' */
      CRVLAB_ECU_104_B.s75_speedsp = 750.0;
      break;
    }
  }

  /* End of Stateflow: '<S59>/Chart' */
  /* Switch: '<S59>/Switch' incorporates:
   *  Sum: '<S59>/Add'
   */
  rtb_Merge_p = CRVLAB_ECU_104_B.s75_speedsp;

  /* Switch: '<S59>/Switch2' */
  if (rtb_Merge_p > 800.0) {
    rtb_Gain2 = rtb_Merge_p;
  } else {
    /* Switch: '<S59>/Switch1' incorporates:
     *  S-Function (motohawk_sfun_calibration): '<S59>/motohawk_calibration1'
     *  S-Function (motohawk_sfun_data_read): '<S59>/motohawk_data_read2'
     */
    if (Idle_High_on_ECT_cmd_DataStore() > 0.5) {
      rtb_Switch1_b = (IdleSpeedSP_ECT_Low_rpm_DataStore());
    } else {
      rtb_Switch1_b = (real32_T)rtb_Merge_p;
    }

    /* End of Switch: '<S59>/Switch1' */
    rtb_Gain2 = rtb_Switch1_b;
  }

  /* End of Switch: '<S59>/Switch2' */

  /* S-Function (motohawk_sfun_data_write): '<S59>/motohawk_data_write' */
  /* Write to Data Storage as scalar: IdleSpeedSP_rpm */
  {
    IdleSpeedSP_rpm_DataStore() = rtb_Gain2;
  }

  /* S-Function (motohawk_sfun_data_write): '<S59>/motohawk_data_write1' incorporates:
   *  S-Function (motohawk_sfun_data_read): '<S59>/motohawk_data_read2'
   */
  /* Write to Data Storage as scalar: Idle_High_on_ECT */
  {
    Idle_High_on_ECT_DataStore() = Idle_High_on_ECT_cmd_DataStore();
  }

  /* S-Function (motohawk_sfun_data_read): '<S76>/motohawk_data_read4' */
  rtb_motohawk_data_read4 = ECT_C_DataStore();

  /* S-Function (motohawk_sfun_data_read): '<S60>/motohawk_data_read1' */
  rtb_motohawk_data_read1_o = EngineSpeed_rpm_DataStore();

  /* S-Function Block: <S60>/motohawk_prelookup */
  {
    extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
      ordarr[], uint32_T sz, uint16_T prev);
    (EngineSpeed_rpm_for_MainFueling_ms_in_CrankIn_DataStore()) =
      rtb_motohawk_data_read1_o;
    (EngineSpeed_rpm_for_MainFueling_ms_in_CrankIdx_DataStore()) =
      TablePrelookup_real_T(rtb_motohawk_data_read1_o,
      (EngineSpeed_rpm_for_MainFueling_ms_in_CrankIdxArr_DataStore()), 8,
      (EngineSpeed_rpm_for_MainFueling_ms_in_CrankIdx_DataStore()));
    rtb_motohawk_prelookup_k =
      (EngineSpeed_rpm_for_MainFueling_ms_in_CrankIdx_DataStore());
  }

  /* S-Function Block: <S60>/motohawk_interpolation_1d */
  {
    extern real_T TableInterpolation1D_real_T(uint16_T idx, real_T *tbl_data,
      uint32_T sz);
    rtb_motohawk_interpolation_1d = TableInterpolation1D_real_T
      (rtb_motohawk_prelookup_k, (real_T *)
       ((MainFueling_ms_in_CrankTbl_DataStore())), 8);
    (MainFueling_ms_in_Crank_DataStore()) = rtb_motohawk_interpolation_1d;
  }

  /* S-Function (motohawk_sfun_data_read): '<S60>/motohawk_data_read2' */
  rtb_motohawk_data_read2_k = Pedal_Pct_DataStore();

  /* MultiPortSwitch: '<S60>/MultiSwitch' incorporates:
   *  Product: '<S60>/Product'
   *  Product: '<S60>/Product1'
   *  S-Function (motohawk_sfun_calibration): '<S60>/motohawk_calibration2'
   *  S-Function (motohawk_sfun_data_read): '<S60>/motohawk_data_read'
   *  S-Function (motohawk_sfun_data_read): '<S60>/motohawk_data_read1'
   *  S-Function (motohawk_sfun_data_read): '<S60>/motohawk_data_read2'
   *  S-Function (motohawk_sfun_interpolation_1d): '<S60>/motohawk_interpolation_1d'
   *  S-Function (motohawk_sfun_interpolation_2d): '<S60>/motohawk_interpolation_2d1'
   *  S-Function (motohawk_sfun_interpolation_2d): '<S60>/motohawk_interpolation_2d2'
   *  S-Function (motohawk_sfun_prelookup): '<S60>/motohawk_prelookup'
   *  S-Function (motohawk_sfun_prelookup): '<S60>/motohawk_prelookup1'
   *  S-Function (motohawk_sfun_prelookup): '<S60>/motohawk_prelookup2'
   *  S-Function (motohawk_sfun_prelookup): '<S60>/motohawk_prelookup3'
   *  S-Function (motohawk_sfun_prelookup): '<S60>/motohawk_prelookup4'
   *  Switch: '<S60>/Switch'
   */
  switch (((uint8_T)State_DataStore())) {
   case 1:
    rtb_Merge_p = (MainFuelingStall_ms_DataStore());
    break;

   case 2:
    rtb_Merge_p = rtb_motohawk_interpolation_1d;
    break;

   case 3:
    /* S-Function Block: <S60>/motohawk_prelookup2 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (Pedal_pct_for_MainFueling_ms_in_RunDieselIn_DataStore()) =
        rtb_motohawk_data_read2_k;
      (Pedal_pct_for_MainFueling_ms_in_RunDieselIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read2_k,
        (Pedal_pct_for_MainFueling_ms_in_RunDieselIdxArr_DataStore()), 11,
        (Pedal_pct_for_MainFueling_ms_in_RunDieselIdx_DataStore()));
      rtb_motohawk_prelookup2_j =
        (Pedal_pct_for_MainFueling_ms_in_RunDieselIdx_DataStore());
    }

    /* S-Function Block: <S60>/motohawk_prelookup3 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_MainFueling_ms_in_RunDieselIn_DataStore()) =
        rtb_motohawk_data_read1_o;
      (EngineSpeed_rpm_for_MainFueling_ms_in_RunDieselIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_o,
        (EngineSpeed_rpm_for_MainFueling_ms_in_RunDieselIdxArr_DataStore()), 10,
                              
        (EngineSpeed_rpm_for_MainFueling_ms_in_RunDieselIdx_DataStore()));
      rtb_motohawk_prelookup3_bf =
        (EngineSpeed_rpm_for_MainFueling_ms_in_RunDieselIdx_DataStore());
    }

    /* S-Function Block: <S60>/motohawk_interpolation_2d1 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d1_js = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup3_bf, rtb_motohawk_prelookup2_j, (real_T *)
         ((MainFueling_ms_in_RunDieselMap_DataStore())), 10, 11);
      (MainFueling_ms_in_RunDiesel_DataStore()) =
        rtb_motohawk_interpolation_2d1_js;
    }

    rtb_Merge_p = rtb_motohawk_interpolation_2d1_js;
    break;

   case 4:
    /* S-Function Block: <S60>/motohawk_prelookup1 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (Pedal_pct_for_MainFueling_ms_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read2_k;
      (Pedal_pct_for_MainFueling_ms_in_RunDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read2_k,
        (Pedal_pct_for_MainFueling_ms_in_RunDDFIdxArr_DataStore()), 11,
        (Pedal_pct_for_MainFueling_ms_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup1_g =
        (Pedal_pct_for_MainFueling_ms_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S60>/motohawk_prelookup4 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_MainFueling_ms_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read1_o;
      (EngineSpeed_rpm_for_MainFueling_ms_in_RunDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_o,
        (EngineSpeed_rpm_for_MainFueling_ms_in_RunDDFIdxArr_DataStore()), 10,
        (EngineSpeed_rpm_for_MainFueling_ms_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup4_h =
        (EngineSpeed_rpm_for_MainFueling_ms_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S60>/motohawk_interpolation_2d2 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d2 = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup4_h, rtb_motohawk_prelookup1_g, (real_T *)
         ((MainFueling_ms_in_RunDDFMap_DataStore())), 10, 11);
      (MainFueling_ms_in_RunDDF_DataStore()) = rtb_motohawk_interpolation_2d2;
    }

    rtb_Merge_p = rtb_motohawk_interpolation_2d2;
    break;

   case 5:
    /* S-Function (motohawk_sfun_data_read): '<S60>/motohawk_data_read8' */
    rtb_motohawk_data_read8_mn = AC_On_DataStore();

    /* MultiPortSwitch: '<S77>/Multiport Switch' incorporates:
     *  Constant: '<S77>/Constant'
     *  Product: '<S77>/Product'
     *  S-Function (motohawk_sfun_calibration): '<S77>/motohawk_calibration1'
     *  S-Function (motohawk_sfun_calibration): '<S77>/motohawk_calibration2'
     *  S-Function (motohawk_sfun_calibration): '<S77>/motohawk_calibration3'
     *  S-Function (motohawk_sfun_calibration): '<S77>/motohawk_calibration4'
     *  S-Function (motohawk_sfun_data_read): '<S60>/motohawk_data_read5'
     *  Sum: '<S77>/Add'
     */
    switch (rtb_motohawk_data_read8_mn * 2 + DDF_On_DataStore()) {
     case 0:
      rtb_Merge_p = (MainFuelingIdleACOffDDFOff_ms_DataStore());
      break;

     case 1:
      rtb_Merge_p = (MainFuelingIdleACOffDDFOn_ms_DataStore());
      break;

     case 2:
      rtb_Merge_p = (MainFuelingIdleACOnDDFOff_ms_DataStore());
      break;

     default:
      rtb_Merge_p = (MainFuelingIdleACOnDDFOn_ms_DataStore());
      break;
    }

    /* End of MultiPortSwitch: '<S77>/Multiport Switch' */
    break;

   default:
    /* Switch: '<S60>/Switch1' incorporates:
     *  Constant: '<S60>/Constant'
     *  Logic: '<S60>/Logical Operator'
     *  Logic: '<S60>/Logical Operator1'
     *  S-Function (motohawk_sfun_calibration): '<S60>/motohawk_calibration1'
     *  S-Function (motohawk_sfun_calibration): '<S60>/motohawk_calibration3'
     *  S-Function (motohawk_sfun_data_read): '<S60>/motohawk_data_read4'
     *  S-Function (motohawk_sfun_data_read): '<S60>/motohawk_data_read6'
     */
    if ((FuelCutAsIdle_On_DataStore()) && (!Neutral_On_DataStore()) &&
        DDF_On_DataStore()) {
      rtb_Merge_p = (FuelCutAsIdle_Main_ms_DataStore());
    } else {
      rtb_Merge_p = 0.0;
    }

    /* End of Switch: '<S60>/Switch1' */
    break;
  }

  /* End of MultiPortSwitch: '<S60>/MultiSwitch' */

  /* Switch: '<S60>/Switch2' incorporates:
   *  S-Function (motohawk_sfun_data_read): '<S60>/motohawk_data_read'
   *  Sum: '<S60>/Add2'
   */
  if (((uint8_T)State_DataStore()) > 2) {
    /* Switch: '<S76>/Switch1' incorporates:
     *  Product: '<S76>/Product1'
     *  Product: '<S76>/Product2'
     *  S-Function (motohawk_sfun_calibration): '<S76>/motohawk_calibration4'
     *  S-Function (motohawk_sfun_calibration): '<S76>/motohawk_calibration5'
     *  S-Function (motohawk_sfun_data_read): '<S76>/motohawk_data_read7'
     */
    if (DDF_On_DataStore()) {
      /* S-Function Block: <S76>/motohawk_prelookup6 */
      {
        extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
          ordarr[], uint32_T sz, uint16_T prev);
        (ECT_C_in_ECT_C_MainFueling_OffsetDDF_mapIn_DataStore()) =
          rtb_motohawk_data_read4;
        (ECT_C_in_ECT_C_MainFueling_OffsetDDF_mapIdx_DataStore()) =
          TablePrelookup_real_T(rtb_motohawk_data_read4,
          (ECT_C_in_ECT_C_MainFueling_OffsetDDF_mapIdxArr_DataStore()), 10,
          (ECT_C_in_ECT_C_MainFueling_OffsetDDF_mapIdx_DataStore()));
        rtb_motohawk_prelookup6_f =
          (ECT_C_in_ECT_C_MainFueling_OffsetDDF_mapIdx_DataStore());
      }

      /* S-Function Block: <S76>/motohawk_interpolation_1d2 */
      {
        extern real_T TableInterpolation1D_real_T(uint16_T idx, real_T *tbl_data,
          uint32_T sz);
        rtb_motohawk_interpolation_1d2_m = TableInterpolation1D_real_T
          (rtb_motohawk_prelookup6_f, (real_T *)
           ((MainFueling_Offset_in_ECT_C_MainFueling_OffsetDDF_mapTbl_DataStore())),
           10);
        (MainFueling_Offset_in_ECT_C_MainFueling_OffsetDDF_map_DataStore()) =
          rtb_motohawk_interpolation_1d2_m;
      }

      rtb_Switch1_d = (real_T)(MainFueling_ECT_Offset_DDFGain_DataStore()) *
        rtb_motohawk_interpolation_1d2_m;
    } else {
      /* S-Function Block: <S76>/motohawk_prelookup7 */
      {
        extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
          ordarr[], uint32_T sz, uint16_T prev);
        (ECT_C_in_ECT_C_MainFueling_Offset_mapIn_DataStore()) =
          rtb_motohawk_data_read4;
        (ECT_C_in_ECT_C_MainFueling_Offset_mapIdx_DataStore()) =
          TablePrelookup_real_T(rtb_motohawk_data_read4,
          (ECT_C_in_ECT_C_MainFueling_Offset_mapIdxArr_DataStore()), 10,
          (ECT_C_in_ECT_C_MainFueling_Offset_mapIdx_DataStore()));
        rtb_motohawk_prelookup7_k =
          (ECT_C_in_ECT_C_MainFueling_Offset_mapIdx_DataStore());
      }

      /* S-Function Block: <S76>/motohawk_interpolation_1d3 */
      {
        extern real_T TableInterpolation1D_real_T(uint16_T idx, real_T *tbl_data,
          uint32_T sz);
        rtb_motohawk_interpolation_1d3_i = TableInterpolation1D_real_T
          (rtb_motohawk_prelookup7_k, (real_T *)
           ((MainFueling_Offset_in_ECT_C_MainFueling_Offset_mapTbl_DataStore())),
           10);
        (MainFueling_Offset_in_ECT_C_MainFueling_Offset_map_DataStore()) =
          rtb_motohawk_interpolation_1d3_i;
      }

      rtb_Switch1_d = (real_T)(MainFueling_ECT_Offset_Gain_DataStore()) *
        rtb_motohawk_interpolation_1d3_i;
    }

    /* End of Switch: '<S76>/Switch1' */
    rtb_Gain2 = rtb_Switch1_d + rtb_Merge_p;
  } else {
    rtb_Gain2 = rtb_Merge_p;
  }

  /* End of Switch: '<S60>/Switch2' */

  /* S-Function (motohawk_sfun_data_write): '<S60>/motohawk_data_write' */
  /* Write to Data Storage as scalar: MainFueling_ms */
  {
    MainFueling_ms_DataStore() = rtb_Gain2;
  }

  /* S-Function (motohawk_sfun_data_write): '<S60>/motohawk_data_write1' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S60>/motohawk_calibration1'
   */
  /* Write to Data Storage as scalar: FCasIdle_On */
  {
    FCasIdle_On_DataStore() = (FuelCutAsIdle_On_DataStore());
  }

  /* S-Function Block: <S60>/motohawk_prelookup5 */
  {
    extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
      ordarr[], uint32_T sz, uint16_T prev);
    (EngineSpeed_rpm_for_MainFueling_ms_in_CrankDDFIn_DataStore()) =
      rtb_motohawk_data_read1_o;
    (EngineSpeed_rpm_for_MainFueling_ms_in_CrankDDFIdx_DataStore()) =
      TablePrelookup_real_T(rtb_motohawk_data_read1_o,
      (EngineSpeed_rpm_for_MainFueling_ms_in_CrankDDFIdxArr_DataStore()), 8,
      (EngineSpeed_rpm_for_MainFueling_ms_in_CrankDDFIdx_DataStore()));
    rtb_motohawk_prelookup5 =
      (EngineSpeed_rpm_for_MainFueling_ms_in_CrankDDFIdx_DataStore());
  }

  /* S-Function Block: <S60>/motohawk_interpolation_1d1 */
  {
    extern real_T TableInterpolation1D_real_T(uint16_T idx, real_T *tbl_data,
      uint32_T sz);
    rtb_Gain2 = TableInterpolation1D_real_T(rtb_motohawk_prelookup5, (real_T *)
      ((MainFueling_ms_in_CrankDDFTbl_DataStore())), 8);
    (MainFueling_ms_in_CrankDDF_DataStore()) = rtb_Gain2;
  }

  /* S-Function (motohawk_sfun_data_read): '<S78>/motohawk_data_read4' */
  rtb_motohawk_data_read4_j = ECT_C_DataStore();

  /* S-Function (motohawk_sfun_data_read): '<S61>/motohawk_data_read1' */
  rtb_motohawk_data_read1_n = EngineSpeed_rpm_DataStore();

  /* S-Function Block: <S61>/motohawk_prelookup */
  {
    extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
      ordarr[], uint32_T sz, uint16_T prev);
    (EngineSpeed_rpm_for_MainSOI_degBTDC_in_CrankIn_DataStore()) =
      rtb_motohawk_data_read1_n;
    (EngineSpeed_rpm_for_MainSOI_degBTDC_in_CrankIdx_DataStore()) =
      TablePrelookup_real_T(rtb_motohawk_data_read1_n,
      (EngineSpeed_rpm_for_MainSOI_degBTDC_in_CrankIdxArr_DataStore()), 8,
      (EngineSpeed_rpm_for_MainSOI_degBTDC_in_CrankIdx_DataStore()));
    rtb_motohawk_prelookup_o =
      (EngineSpeed_rpm_for_MainSOI_degBTDC_in_CrankIdx_DataStore());
  }

  /* S-Function Block: <S61>/motohawk_interpolation_1d */
  {
    extern real_T TableInterpolation1D_real_T(uint16_T idx, real_T *tbl_data,
      uint32_T sz);
    rtb_motohawk_interpolation_1d_p = TableInterpolation1D_real_T
      (rtb_motohawk_prelookup_o, (real_T *)
       ((MainSOI_degBTDC_in_CrankTbl_DataStore())), 8);
    (MainSOI_degBTDC_in_Crank_DataStore()) = rtb_motohawk_interpolation_1d_p;
  }

  /* S-Function (motohawk_sfun_data_read): '<S61>/motohawk_data_read2' */
  rtb_motohawk_data_read2_cb = Pedal_Pct_DataStore();

  /* S-Function (motohawk_sfun_data_read): '<S61>/motohawk_data_read6' */
  rtb_BelowLoTarget = AC_On_DataStore();

  /* Product: '<S79>/Product' incorporates:
   *  Constant: '<S79>/Constant'
   */
  rtb_Gain2 = (real_T)rtb_BelowLoTarget * 2.0;

  /* MultiPortSwitch: '<S79>/Multiport Switch' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S79>/motohawk_calibration1'
   *  S-Function (motohawk_sfun_calibration): '<S79>/motohawk_calibration2'
   *  S-Function (motohawk_sfun_calibration): '<S79>/motohawk_calibration3'
   *  S-Function (motohawk_sfun_calibration): '<S79>/motohawk_calibration4'
   *  S-Function (motohawk_sfun_data_read): '<S61>/motohawk_data_read5'
   *  Sum: '<S79>/Add'
   */
  switch ((int32_T)(rtb_Gain2 + (real_T)DDF_On_DataStore())) {
   case 0:
    rtb_Merge_p = (MainSOIIdleACOffDDFOff_degBTDC_DataStore());
    break;

   case 1:
    rtb_Merge_p = (MainSOIIdleACOffDDFOn_degBTDC_DataStore());
    break;

   case 2:
    rtb_Merge_p = (MainSOIIdleACOnDDFOff_degBTDC_DataStore());
    break;

   default:
    rtb_Merge_p = (MainSOIIdleACOnDDFOn_degBTDC_DataStore());
    break;
  }

  /* End of MultiPortSwitch: '<S79>/Multiport Switch' */

  /* MultiPortSwitch: '<S61>/MultiSwitch' incorporates:
   *  Product: '<S61>/Product'
   *  S-Function (motohawk_sfun_calibration): '<S61>/motohawk_calibration1'
   *  S-Function (motohawk_sfun_calibration): '<S61>/motohawk_calibration2'
   *  S-Function (motohawk_sfun_calibration): '<S61>/motohawk_calibration3'
   *  S-Function (motohawk_sfun_data_read): '<S61>/motohawk_data_read'
   *  S-Function (motohawk_sfun_data_read): '<S61>/motohawk_data_read1'
   *  S-Function (motohawk_sfun_data_read): '<S61>/motohawk_data_read2'
   *  S-Function (motohawk_sfun_interpolation_1d): '<S61>/motohawk_interpolation_1d'
   *  S-Function (motohawk_sfun_interpolation_2d): '<S61>/motohawk_interpolation_2d1'
   *  S-Function (motohawk_sfun_prelookup): '<S61>/motohawk_prelookup'
   *  S-Function (motohawk_sfun_prelookup): '<S61>/motohawk_prelookup2'
   *  S-Function (motohawk_sfun_prelookup): '<S61>/motohawk_prelookup3'
   *  Sum: '<S61>/Add'
   *  Switch: '<S61>/Switch'
   */
  switch (((uint8_T)State_DataStore())) {
   case 1:
    rtb_Merge_p = (MainSOIStall_degBTDC_DataStore());
    break;

   case 2:
    rtb_Merge_p = rtb_motohawk_interpolation_1d_p;
    break;

   case 3:
    /* S-Function Block: <S61>/motohawk_prelookup2 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (Pedal_pct_for_MainSOI_degBTDC_in_RunDieselIn_DataStore()) =
        rtb_motohawk_data_read2_cb;
      (Pedal_pct_for_MainSOI_degBTDC_in_RunDieselIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read2_cb,
        (Pedal_pct_for_MainSOI_degBTDC_in_RunDieselIdxArr_DataStore()), 11,
        (Pedal_pct_for_MainSOI_degBTDC_in_RunDieselIdx_DataStore()));
      rtb_motohawk_prelookup2_f =
        (Pedal_pct_for_MainSOI_degBTDC_in_RunDieselIdx_DataStore());
    }

    /* S-Function Block: <S61>/motohawk_prelookup3 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_MainSOI_degBTDC_in_RunDieselIn_DataStore()) =
        rtb_motohawk_data_read1_n;
      (EngineSpeed_rpm_for_MainSOI_degBTDC_in_RunDieselIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_n,
        (EngineSpeed_rpm_for_MainSOI_degBTDC_in_RunDieselIdxArr_DataStore()), 10,
                              
        (EngineSpeed_rpm_for_MainSOI_degBTDC_in_RunDieselIdx_DataStore()));
      rtb_motohawk_prelookup3_k =
        (EngineSpeed_rpm_for_MainSOI_degBTDC_in_RunDieselIdx_DataStore());
    }

    /* S-Function Block: <S61>/motohawk_interpolation_2d1 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d1_j = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup3_k, rtb_motohawk_prelookup2_f, (real_T *)
         ((MainSOI_degBTDC_in_RunDieselMap_DataStore())), 10, 11);
      (MainSOI_degBTDC_in_RunDiesel_DataStore()) =
        rtb_motohawk_interpolation_2d1_j;
    }

    rtb_Merge_p = rtb_motohawk_interpolation_2d1_j;
    break;

   case 4:
    /* S-Function Block: <S61>/motohawk_prelookup1 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (Pedal_pct_for_MainSOI_degBTDC_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read2_cb;
      (Pedal_pct_for_MainSOI_degBTDC_in_RunDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read2_cb,
        (Pedal_pct_for_MainSOI_degBTDC_in_RunDDFIdxArr_DataStore()), 11,
        (Pedal_pct_for_MainSOI_degBTDC_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup1_ka =
        (Pedal_pct_for_MainSOI_degBTDC_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S61>/motohawk_prelookup4 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_MainSOI_degBTDC_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read1_n;
      (EngineSpeed_rpm_for_MainSOI_degBTDC_in_RunDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_n,
        (EngineSpeed_rpm_for_MainSOI_degBTDC_in_RunDDFIdxArr_DataStore()), 10,
        (EngineSpeed_rpm_for_MainSOI_degBTDC_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup4_n =
        (EngineSpeed_rpm_for_MainSOI_degBTDC_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S61>/motohawk_interpolation_2d2 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d2_e = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup4_n, rtb_motohawk_prelookup1_ka, (real_T *)
         ((MainSOI_degBTDC_in_RunDDFMap_DataStore())), 10, 11);
      (MainSOI_degBTDC_in_RunDDF_DataStore()) = rtb_motohawk_interpolation_2d2_e;
    }

    rtb_Merge_p = rtb_motohawk_interpolation_2d2_e * (real_T)
      (MainSOISPDDFGain_DataStore()) + (real_T)
      (MainSOISPDDFOffset_degBTDC_DataStore());
    break;

   case 5:
    break;
  }

  /* End of MultiPortSwitch: '<S61>/MultiSwitch' */

  /* Switch: '<S61>/Switch1' incorporates:
   *  S-Function (motohawk_sfun_data_read): '<S61>/motohawk_data_read'
   *  Sum: '<S61>/Add2'
   */
  if (((uint8_T)State_DataStore()) > 2) {
    /* Switch: '<S78>/Switch1' incorporates:
     *  Product: '<S78>/Product1'
     *  Product: '<S78>/Product2'
     *  S-Function (motohawk_sfun_calibration): '<S78>/motohawk_calibration4'
     *  S-Function (motohawk_sfun_calibration): '<S78>/motohawk_calibration5'
     *  S-Function (motohawk_sfun_data_read): '<S78>/motohawk_data_read7'
     */
    if (DDF_On_DataStore()) {
      /* S-Function Block: <S78>/motohawk_prelookup6 */
      {
        extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
          ordarr[], uint32_T sz, uint16_T prev);
        (ECT_C_in_ECT_C_MainSOI_OffsetDDF_mapIn_DataStore()) =
          rtb_motohawk_data_read4_j;
        (ECT_C_in_ECT_C_MainSOI_OffsetDDF_mapIdx_DataStore()) =
          TablePrelookup_real_T(rtb_motohawk_data_read4_j,
          (ECT_C_in_ECT_C_MainSOI_OffsetDDF_mapIdxArr_DataStore()), 10,
          (ECT_C_in_ECT_C_MainSOI_OffsetDDF_mapIdx_DataStore()));
        rtb_motohawk_prelookup6_d =
          (ECT_C_in_ECT_C_MainSOI_OffsetDDF_mapIdx_DataStore());
      }

      /* S-Function Block: <S78>/motohawk_interpolation_1d2 */
      {
        extern real_T TableInterpolation1D_real_T(uint16_T idx, real_T *tbl_data,
          uint32_T sz);
        rtb_motohawk_interpolation_1d2_e = TableInterpolation1D_real_T
          (rtb_motohawk_prelookup6_d, (real_T *)
           ((MainSOI_Offset_in_ECT_C_MainSOI_OffsetDDF_mapTbl_DataStore())), 10);
        (MainSOI_Offset_in_ECT_C_MainSOI_OffsetDDF_map_DataStore()) =
          rtb_motohawk_interpolation_1d2_e;
      }

      rtb_Switch1_d = (real_T)(MainSOI_ECT_Offset_DDFGain_DataStore()) *
        rtb_motohawk_interpolation_1d2_e;
    } else {
      /* S-Function Block: <S78>/motohawk_prelookup7 */
      {
        extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
          ordarr[], uint32_T sz, uint16_T prev);
        (ECT_C_in_ECT_C_MainSOI_Offset_mapIn_DataStore()) =
          rtb_motohawk_data_read4_j;
        (ECT_C_in_ECT_C_MainSOI_Offset_mapIdx_DataStore()) =
          TablePrelookup_real_T(rtb_motohawk_data_read4_j,
          (ECT_C_in_ECT_C_MainSOI_Offset_mapIdxArr_DataStore()), 10,
          (ECT_C_in_ECT_C_MainSOI_Offset_mapIdx_DataStore()));
        rtb_motohawk_prelookup7_p =
          (ECT_C_in_ECT_C_MainSOI_Offset_mapIdx_DataStore());
      }

      /* S-Function Block: <S78>/motohawk_interpolation_1d3 */
      {
        extern real_T TableInterpolation1D_real_T(uint16_T idx, real_T *tbl_data,
          uint32_T sz);
        rtb_motohawk_interpolation_1d3_e = TableInterpolation1D_real_T
          (rtb_motohawk_prelookup7_p, (real_T *)
           ((MainSOI_Offset_in_ECT_C_MainSOI_Offset_mapTbl_DataStore())), 10);
        (MainSOI_Offset_in_ECT_C_MainSOI_Offset_map_DataStore()) =
          rtb_motohawk_interpolation_1d3_e;
      }

      rtb_Switch1_d = (real_T)(MainSOI_ECT_Offset_Gain_DataStore()) *
        rtb_motohawk_interpolation_1d3_e;
    }

    /* End of Switch: '<S78>/Switch1' */
    rtb_Gain2 = rtb_Switch1_d + rtb_Merge_p;
  } else {
    rtb_Gain2 = rtb_Merge_p;
  }

  /* End of Switch: '<S61>/Switch1' */

  /* S-Function (motohawk_sfun_data_write): '<S61>/motohawk_data_write' */
  /* Write to Data Storage as scalar: MainSOI_degBTDC */
  {
    MainSOI_degBTDC_DataStore() = rtb_Gain2;
  }

  /* S-Function Block: <S61>/motohawk_prelookup5 */
  {
    extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
      ordarr[], uint32_T sz, uint16_T prev);
    (EngineSpeed_rpm_for_MainSOI_degBTDC_in_CrankDDFIn_DataStore()) =
      rtb_motohawk_data_read1_n;
    (EngineSpeed_rpm_for_MainSOI_degBTDC_in_CrankDDFIdx_DataStore()) =
      TablePrelookup_real_T(rtb_motohawk_data_read1_n,
      (EngineSpeed_rpm_for_MainSOI_degBTDC_in_CrankDDFIdxArr_DataStore()), 8,
      (EngineSpeed_rpm_for_MainSOI_degBTDC_in_CrankDDFIdx_DataStore()));
    rtb_motohawk_prelookup5_i =
      (EngineSpeed_rpm_for_MainSOI_degBTDC_in_CrankDDFIdx_DataStore());
  }

  /* S-Function Block: <S61>/motohawk_interpolation_1d1 */
  {
    extern real_T TableInterpolation1D_real_T(uint16_T idx, real_T *tbl_data,
      uint32_T sz);
    rtb_Gain2 = TableInterpolation1D_real_T(rtb_motohawk_prelookup5_i, (real_T *)
      ((MainSOI_degBTDC_in_CrankDDFTbl_DataStore())), 8);
    (MainSOI_degBTDC_in_CrankDDF_DataStore()) = rtb_Gain2;
  }

  /* S-Function (motohawk_sfun_data_read): '<S62>/motohawk_data_read1' */
  rtb_motohawk_data_read1_l = EngineSpeed_rpm_DataStore();

  /* S-Function Block: <S62>/motohawk_prelookup */
  {
    extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
      ordarr[], uint32_T sz, uint16_T prev);
    (EngineSpeed_rpm_for_PilotFueling_ms_in_CrankIn_DataStore()) =
      rtb_motohawk_data_read1_l;
    (EngineSpeed_rpm_for_PilotFueling_ms_in_CrankIdx_DataStore()) =
      TablePrelookup_real_T(rtb_motohawk_data_read1_l,
      (EngineSpeed_rpm_for_PilotFueling_ms_in_CrankIdxArr_DataStore()), 8,
      (EngineSpeed_rpm_for_PilotFueling_ms_in_CrankIdx_DataStore()));
    rtb_motohawk_prelookup_b =
      (EngineSpeed_rpm_for_PilotFueling_ms_in_CrankIdx_DataStore());
  }

  /* S-Function Block: <S62>/motohawk_interpolation_1d */
  {
    extern real_T TableInterpolation1D_real_T(uint16_T idx, real_T *tbl_data,
      uint32_T sz);
    rtb_motohawk_interpolation_1d_e = TableInterpolation1D_real_T
      (rtb_motohawk_prelookup_b, (real_T *)
       ((PilotFueling_ms_in_CrankTbl_DataStore())), 8);
    (PilotFueling_ms_in_Crank_DataStore()) = rtb_motohawk_interpolation_1d_e;
  }

  /* S-Function (motohawk_sfun_data_read): '<S62>/motohawk_data_read2' */
  rtb_motohawk_data_read2_ce = Pedal_Pct_DataStore();

  /* MultiPortSwitch: '<S62>/MultiSwitch' incorporates:
   *  Constant: '<S62>/Constant'
   *  Constant: '<S62>/Constant2'
   *  Product: '<S62>/Product'
   *  S-Function (motohawk_sfun_calibration): '<S62>/motohawk_calibration2'
   *  S-Function (motohawk_sfun_calibration): '<S62>/motohawk_calibration3'
   *  S-Function (motohawk_sfun_data_read): '<S62>/motohawk_data_read'
   *  S-Function (motohawk_sfun_data_read): '<S62>/motohawk_data_read1'
   *  S-Function (motohawk_sfun_interpolation_1d): '<S62>/motohawk_interpolation_1d'
   *  S-Function (motohawk_sfun_prelookup): '<S62>/motohawk_prelookup'
   *  Sum: '<S62>/Add'
   *  Switch: '<S62>/Switch1'
   */
  switch (((uint8_T)State_DataStore())) {
   case 1:
    rtb_Gain2 = 0.0;
    break;

   case 2:
    rtb_Gain2 = rtb_motohawk_interpolation_1d_e;
    break;

   case 3:
    /* Switch: '<S62>/Switch' incorporates:
     *  Constant: '<S62>/Constant1'
     *  S-Function (motohawk_sfun_data_read): '<S62>/motohawk_data_read1'
     *  S-Function (motohawk_sfun_data_read): '<S62>/motohawk_data_read2'
     *  S-Function (motohawk_sfun_interpolation_2d): '<S62>/motohawk_interpolation_2d1'
     *  S-Function (motohawk_sfun_prelookup): '<S62>/motohawk_prelookup2'
     *  S-Function (motohawk_sfun_prelookup): '<S62>/motohawk_prelookup3'
     */
    if (rtb_motohawk_data_read1_l > 3500.0) {
      rtb_Gain2 = 0.0;
    } else {
      /* S-Function Block: <S62>/motohawk_prelookup2 */
      {
        extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
          ordarr[], uint32_T sz, uint16_T prev);
        (Pedal_pct_for_PilotFueling_ms_in_RunDieselIn_DataStore()) =
          rtb_motohawk_data_read2_ce;
        (Pedal_pct_for_PilotFueling_ms_in_RunDieselIdx_DataStore()) =
          TablePrelookup_real_T(rtb_motohawk_data_read2_ce,
          (Pedal_pct_for_PilotFueling_ms_in_RunDieselIdxArr_DataStore()), 11,
          (Pedal_pct_for_PilotFueling_ms_in_RunDieselIdx_DataStore()));
        rtb_motohawk_prelookup2_o =
          (Pedal_pct_for_PilotFueling_ms_in_RunDieselIdx_DataStore());
      }

      /* S-Function Block: <S62>/motohawk_prelookup3 */
      {
        extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
          ordarr[], uint32_T sz, uint16_T prev);
        (EngineSpeed_rpm_for_PilotFueling_ms_in_RunDieselIn_DataStore()) =
          rtb_motohawk_data_read1_l;
        (EngineSpeed_rpm_for_PilotFueling_ms_in_RunDieselIdx_DataStore()) =
          TablePrelookup_real_T(rtb_motohawk_data_read1_l,
          (EngineSpeed_rpm_for_PilotFueling_ms_in_RunDieselIdxArr_DataStore()),
          10, (EngineSpeed_rpm_for_PilotFueling_ms_in_RunDieselIdx_DataStore()));
        rtb_motohawk_prelookup3_b =
          (EngineSpeed_rpm_for_PilotFueling_ms_in_RunDieselIdx_DataStore());
      }

      /* S-Function Block: <S62>/motohawk_interpolation_2d1 */
      {
        extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T
          col_in, real_T *map_data, uint32_T row_sz, uint32_T col_sz);
        rtb_motohawk_interpolation_2d1_c = TableInterpolation2D_real_T
          (rtb_motohawk_prelookup3_b, rtb_motohawk_prelookup2_o, (real_T *)
           ((PilotFueling_ms_in_RunDieselMap_DataStore())), 10, 11);
        (PilotFueling_ms_in_RunDiesel_DataStore()) =
          rtb_motohawk_interpolation_2d1_c;
      }

      rtb_Gain2 = rtb_motohawk_interpolation_2d1_c;
    }

    /* End of Switch: '<S62>/Switch' */
    break;

   case 4:
    /* S-Function Block: <S62>/motohawk_prelookup1 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (Pedal_pct_for_PilotFueling_ms_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read2_ce;
      (Pedal_pct_for_PilotFueling_ms_in_RunDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read2_ce,
        (Pedal_pct_for_PilotFueling_ms_in_RunDDFIdxArr_DataStore()), 11,
        (Pedal_pct_for_PilotFueling_ms_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup1_f =
        (Pedal_pct_for_PilotFueling_ms_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S62>/motohawk_prelookup4 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_PilotFueling_ms_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read1_l;
      (EngineSpeed_rpm_for_PilotFueling_ms_in_RunDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_l,
        (EngineSpeed_rpm_for_PilotFueling_ms_in_RunDDFIdxArr_DataStore()), 10,
        (EngineSpeed_rpm_for_PilotFueling_ms_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup4_f =
        (EngineSpeed_rpm_for_PilotFueling_ms_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S62>/motohawk_interpolation_2d2 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d2_m3 = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup4_f, rtb_motohawk_prelookup1_f, (real_T *)
         ((PilotFueling_ms_in_RunDDFMap_DataStore())), 10, 11);
      (PilotFueling_ms_in_RunDDF_DataStore()) =
        rtb_motohawk_interpolation_2d2_m3;
    }

    rtb_Gain2 = rtb_motohawk_interpolation_2d2_m3 * (real_T)
      (PilotFuelingSPDDFGain_DataStore()) + (real_T)
      (PilotFuelingSPDDFOffset_ms_DataStore());
    break;

   case 5:
    /* S-Function (motohawk_sfun_data_read): '<S62>/motohawk_data_read8' */
    rtb_motohawk_data_read8_f = AC_On_DataStore();

    /* MultiPortSwitch: '<S80>/Multiport Switch' incorporates:
     *  Constant: '<S80>/Constant'
     *  Product: '<S80>/Product'
     *  S-Function (motohawk_sfun_calibration): '<S80>/motohawk_calibration1'
     *  S-Function (motohawk_sfun_calibration): '<S80>/motohawk_calibration2'
     *  S-Function (motohawk_sfun_calibration): '<S80>/motohawk_calibration3'
     *  S-Function (motohawk_sfun_calibration): '<S80>/motohawk_calibration4'
     *  S-Function (motohawk_sfun_data_read): '<S62>/motohawk_data_read5'
     *  Sum: '<S80>/Add'
     */
    switch (rtb_motohawk_data_read8_f * 2 + DDF_On_DataStore()) {
     case 0:
      rtb_Gain2 = (PilotIdleFuelingACOffDDFOff_ms_DataStore());
      break;

     case 1:
      rtb_Gain2 = (PilotIdleFuelingACOffDDFOn_ms_DataStore());
      break;

     case 2:
      rtb_Gain2 = (PilotIdleFuelingACOnDDFOff_ms_DataStore());
      break;

     default:
      rtb_Gain2 = (PilotIdleFuelingACOnDDFOn_ms_DataStore());
      break;
    }

    /* End of MultiPortSwitch: '<S80>/Multiport Switch' */
    break;

   default:
    rtb_Gain2 = 0.0;
    break;
  }

  /* End of MultiPortSwitch: '<S62>/MultiSwitch' */

  /* S-Function (motohawk_sfun_data_write): '<S62>/motohawk_data_write' */
  /* Write to Data Storage as scalar: PilotFueling_ms */
  {
    PilotFueling_ms_DataStore() = rtb_Gain2;
  }

  /* S-Function (motohawk_sfun_data_read): '<S81>/motohawk_data_read4' */
  rtb_motohawk_data_read4_p = ECT_C_DataStore();

  /* S-Function (motohawk_sfun_data_read): '<S63>/motohawk_data_read1' */
  rtb_motohawk_data_read1_nl = EngineSpeed_rpm_DataStore();

  /* S-Function (motohawk_sfun_data_read): '<S63>/motohawk_data_read2' */
  rtb_motohawk_data_read2_e = Pedal_Pct_DataStore();

  /* MultiPortSwitch: '<S63>/MultiSwitch' incorporates:
   *  Constant: '<S63>/Constant'
   *  Constant: '<S63>/Constant1'
   *  Product: '<S63>/Product'
   *  S-Function (motohawk_sfun_calibration): '<S63>/motohawk_calibration2'
   *  S-Function (motohawk_sfun_calibration): '<S63>/motohawk_calibration3'
   *  S-Function (motohawk_sfun_data_read): '<S63>/motohawk_data_read'
   *  S-Function (motohawk_sfun_data_read): '<S63>/motohawk_data_read1'
   *  S-Function (motohawk_sfun_interpolation_1d): '<S63>/motohawk_interpolation_1d'
   *  S-Function (motohawk_sfun_prelookup): '<S63>/motohawk_prelookup'
   *  Sum: '<S63>/Add'
   */
  switch (((uint8_T)State_DataStore())) {
   case 1:
    rtb_Merge_p = 0.0;
    break;

   case 2:
    /* S-Function Block: <S63>/motohawk_prelookup */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_PilotSOI_degBTDC_in_CrankIn_DataStore()) =
        rtb_motohawk_data_read1_nl;
      (EngineSpeed_rpm_for_PilotSOI_degBTDC_in_CrankIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_nl,
        (EngineSpeed_rpm_for_PilotSOI_degBTDC_in_CrankIdxArr_DataStore()), 8,
        (EngineSpeed_rpm_for_PilotSOI_degBTDC_in_CrankIdx_DataStore()));
      rtb_motohawk_prelookup_c =
        (EngineSpeed_rpm_for_PilotSOI_degBTDC_in_CrankIdx_DataStore());
    }

    /* S-Function Block: <S63>/motohawk_interpolation_1d */
    {
      extern real_T TableInterpolation1D_real_T(uint16_T idx, real_T *tbl_data,
        uint32_T sz);
      rtb_motohawk_interpolation_1d_fh = TableInterpolation1D_real_T
        (rtb_motohawk_prelookup_c, (real_T *)
         ((PilotSOI_degBTDC_in_CrankTbl_DataStore())), 8);
      (PilotSOI_degBTDC_in_Crank_DataStore()) = rtb_motohawk_interpolation_1d_fh;
    }

    rtb_Merge_p = rtb_motohawk_interpolation_1d_fh;
    break;

   case 3:
    /* Switch: '<S63>/Switch' incorporates:
     *  Constant: '<S63>/Constant2'
     *  S-Function (motohawk_sfun_data_read): '<S63>/motohawk_data_read1'
     *  S-Function (motohawk_sfun_data_read): '<S63>/motohawk_data_read2'
     *  S-Function (motohawk_sfun_interpolation_2d): '<S63>/motohawk_interpolation_2d1'
     *  S-Function (motohawk_sfun_prelookup): '<S63>/motohawk_prelookup2'
     *  S-Function (motohawk_sfun_prelookup): '<S63>/motohawk_prelookup3'
     */
    if (rtb_motohawk_data_read1_nl > 3500.0) {
      rtb_Merge_p = 34.0;
    } else {
      /* S-Function Block: <S63>/motohawk_prelookup2 */
      {
        extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
          ordarr[], uint32_T sz, uint16_T prev);
        (Pedal_pct_for_PilotSOI_degBTDC_in_RunDieselIn_DataStore()) =
          rtb_motohawk_data_read2_e;
        (Pedal_pct_for_PilotSOI_degBTDC_in_RunDieselIdx_DataStore()) =
          TablePrelookup_real_T(rtb_motohawk_data_read2_e,
          (Pedal_pct_for_PilotSOI_degBTDC_in_RunDieselIdxArr_DataStore()), 11,
          (Pedal_pct_for_PilotSOI_degBTDC_in_RunDieselIdx_DataStore()));
        rtb_motohawk_prelookup2_kb4 =
          (Pedal_pct_for_PilotSOI_degBTDC_in_RunDieselIdx_DataStore());
      }

      /* S-Function Block: <S63>/motohawk_prelookup3 */
      {
        extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
          ordarr[], uint32_T sz, uint16_T prev);
        (EngineSpeed_rpm_for_PilotSOI_degBTDC_in_RunDieselIn_DataStore()) =
          rtb_motohawk_data_read1_nl;
        (EngineSpeed_rpm_for_PilotSOI_degBTDC_in_RunDieselIdx_DataStore()) =
          TablePrelookup_real_T(rtb_motohawk_data_read1_nl,
          (EngineSpeed_rpm_for_PilotSOI_degBTDC_in_RunDieselIdxArr_DataStore()),
          10, (EngineSpeed_rpm_for_PilotSOI_degBTDC_in_RunDieselIdx_DataStore()));
        rtb_motohawk_prelookup3_ce =
          (EngineSpeed_rpm_for_PilotSOI_degBTDC_in_RunDieselIdx_DataStore());
      }

      /* S-Function Block: <S63>/motohawk_interpolation_2d1 */
      {
        extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T
          col_in, real_T *map_data, uint32_T row_sz, uint32_T col_sz);
        rtb_motohawk_interpolation_2d1_p = TableInterpolation2D_real_T
          (rtb_motohawk_prelookup3_ce, rtb_motohawk_prelookup2_kb4, (real_T *)
           ((PilotSOI_degBTDC_in_RunDieselMap_DataStore())), 10, 11);
        (PilotSOI_degBTDC_in_RunDiesel_DataStore()) =
          rtb_motohawk_interpolation_2d1_p;
      }

      rtb_Merge_p = rtb_motohawk_interpolation_2d1_p;
    }

    /* End of Switch: '<S63>/Switch' */
    break;

   case 4:
    /* S-Function Block: <S63>/motohawk_prelookup1 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (Pedal_pct_for_PilotSOI_degBTDC_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read2_e;
      (Pedal_pct_for_PilotSOI_degBTDC_in_RunDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read2_e,
        (Pedal_pct_for_PilotSOI_degBTDC_in_RunDDFIdxArr_DataStore()), 11,
        (Pedal_pct_for_PilotSOI_degBTDC_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup1_im =
        (Pedal_pct_for_PilotSOI_degBTDC_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S63>/motohawk_prelookup4 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_PilotSOI_degBTDC_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read1_nl;
      (EngineSpeed_rpm_for_PilotSOI_degBTDC_in_RunDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_nl,
        (EngineSpeed_rpm_for_PilotSOI_degBTDC_in_RunDDFIdxArr_DataStore()), 10,
                              
        (EngineSpeed_rpm_for_PilotSOI_degBTDC_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup4_p =
        (EngineSpeed_rpm_for_PilotSOI_degBTDC_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S63>/motohawk_interpolation_2d2 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d2_i = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup4_p, rtb_motohawk_prelookup1_im, (real_T *)
         ((PilotSOI_degBTDC_in_RunDDFMap_DataStore())), 10, 11);
      (PilotSOI_degBTDC_in_RunDDF_DataStore()) =
        rtb_motohawk_interpolation_2d2_i;
    }

    rtb_Merge_p = rtb_motohawk_interpolation_2d2_i * (real_T)
      (PilotSOISPDDFGain_DataStore()) + (real_T)
      (PilotSOISPDDFOffset_degBTDC_DataStore());
    break;

   case 5:
    /* S-Function (motohawk_sfun_data_read): '<S63>/motohawk_data_read6' */
    rtb_motohawk_data_read6_gv = AC_On_DataStore();

    /* MultiPortSwitch: '<S82>/Multiport Switch' incorporates:
     *  Constant: '<S82>/Constant'
     *  Product: '<S82>/Product'
     *  S-Function (motohawk_sfun_calibration): '<S82>/motohawk_calibration1'
     *  S-Function (motohawk_sfun_calibration): '<S82>/motohawk_calibration2'
     *  S-Function (motohawk_sfun_calibration): '<S82>/motohawk_calibration3'
     *  S-Function (motohawk_sfun_calibration): '<S82>/motohawk_calibration4'
     *  S-Function (motohawk_sfun_data_read): '<S63>/motohawk_data_read5'
     *  Sum: '<S82>/Add'
     */
    switch (rtb_motohawk_data_read6_gv * 2 + DDF_On_DataStore()) {
     case 0:
      rtb_Merge_p = (PilotSOIIdleACOffDDFOff_degBTDC_DataStore());
      break;

     case 1:
      rtb_Merge_p = (PilotSOIIdleACOffDDFOn_degBTDC_DataStore());
      break;

     case 2:
      rtb_Merge_p = (PilotSOIIdleACOnDDFOff_degBTDC_DataStore());
      break;

     default:
      rtb_Merge_p = (PilotSOIIdleACOnDDFOn_degBTDC_DataStore());
      break;
    }

    /* End of MultiPortSwitch: '<S82>/Multiport Switch' */
    break;

   default:
    rtb_Merge_p = 34.0;
    break;
  }

  /* End of MultiPortSwitch: '<S63>/MultiSwitch' */

  /* Switch: '<S63>/Switch2' incorporates:
   *  S-Function (motohawk_sfun_data_read): '<S63>/motohawk_data_read'
   *  Sum: '<S63>/Add2'
   */
  if (((uint8_T)State_DataStore()) > 2) {
    /* Switch: '<S81>/Switch1' incorporates:
     *  Product: '<S81>/Product1'
     *  Product: '<S81>/Product2'
     *  S-Function (motohawk_sfun_calibration): '<S81>/motohawk_calibration4'
     *  S-Function (motohawk_sfun_calibration): '<S81>/motohawk_calibration5'
     *  S-Function (motohawk_sfun_data_read): '<S81>/motohawk_data_read7'
     */
    if (DDF_On_DataStore()) {
      /* S-Function Block: <S81>/motohawk_prelookup6 */
      {
        extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
          ordarr[], uint32_T sz, uint16_T prev);
        (ECT_C_in_ECT_C_PilotSOI_OffsetDDF_mapIn_DataStore()) =
          rtb_motohawk_data_read4_p;
        (ECT_C_in_ECT_C_PilotSOI_OffsetDDF_mapIdx_DataStore()) =
          TablePrelookup_real_T(rtb_motohawk_data_read4_p,
          (ECT_C_in_ECT_C_PilotSOI_OffsetDDF_mapIdxArr_DataStore()), 10,
          (ECT_C_in_ECT_C_PilotSOI_OffsetDDF_mapIdx_DataStore()));
        rtb_motohawk_prelookup6 =
          (ECT_C_in_ECT_C_PilotSOI_OffsetDDF_mapIdx_DataStore());
      }

      /* S-Function Block: <S81>/motohawk_interpolation_1d2 */
      {
        extern real_T TableInterpolation1D_real_T(uint16_T idx, real_T *tbl_data,
          uint32_T sz);
        rtb_motohawk_interpolation_1d2_a = TableInterpolation1D_real_T
          (rtb_motohawk_prelookup6, (real_T *)
           ((PilotSOI_Offset_in_ECT_C_PilotSOI_OffsetDDF_mapTbl_DataStore())),
           10);
        (PilotSOI_Offset_in_ECT_C_PilotSOI_OffsetDDF_map_DataStore()) =
          rtb_motohawk_interpolation_1d2_a;
      }

      rtb_Switch1_d = (real_T)(PilotSOI_ECT_Offset_DDFGain_DataStore()) *
        rtb_motohawk_interpolation_1d2_a;
    } else {
      /* S-Function Block: <S81>/motohawk_prelookup7 */
      {
        extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
          ordarr[], uint32_T sz, uint16_T prev);
        (ECT_C_in_ECT_C_PilotSOI_Offset_mapIn_DataStore()) =
          rtb_motohawk_data_read4_p;
        (ECT_C_in_ECT_C_PilotSOI_Offset_mapIdx_DataStore()) =
          TablePrelookup_real_T(rtb_motohawk_data_read4_p,
          (ECT_C_in_ECT_C_PilotSOI_Offset_mapIdxArr_DataStore()), 10,
          (ECT_C_in_ECT_C_PilotSOI_Offset_mapIdx_DataStore()));
        rtb_motohawk_prelookup7 =
          (ECT_C_in_ECT_C_PilotSOI_Offset_mapIdx_DataStore());
      }

      /* S-Function Block: <S81>/motohawk_interpolation_1d3 */
      {
        extern real_T TableInterpolation1D_real_T(uint16_T idx, real_T *tbl_data,
          uint32_T sz);
        rtb_motohawk_interpolation_1d3 = TableInterpolation1D_real_T
          (rtb_motohawk_prelookup7, (real_T *)
           ((PilotSOI_Offset_in_ECT_C_PilotSOI_Offset_mapTbl_DataStore())), 10);
        (PilotSOI_Offset_in_ECT_C_PilotSOI_Offset_map_DataStore()) =
          rtb_motohawk_interpolation_1d3;
      }

      rtb_Switch1_d = (real_T)(PilotSOI_ECT_Offset_Gain_DataStore()) *
        rtb_motohawk_interpolation_1d3;
    }

    /* End of Switch: '<S81>/Switch1' */
    rtb_Gain2 = rtb_Switch1_d + rtb_Merge_p;
  } else {
    rtb_Gain2 = rtb_Merge_p;
  }

  /* End of Switch: '<S63>/Switch2' */

  /* S-Function (motohawk_sfun_data_write): '<S63>/motohawk_data_write' */
  /* Write to Data Storage as scalar: PilotSOI_degBTDC */
  {
    PilotSOI_degBTDC_DataStore() = rtb_Gain2;
  }

  /* S-Function (motohawk_sfun_data_read): '<S64>/motohawk_data_read1' */
  rtb_motohawk_data_read1_n4 = EngineSpeed_rpm_DataStore();

  /* S-Function Block: <S64>/motohawk_prelookup */
  {
    extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
      ordarr[], uint32_T sz, uint16_T prev);
    (EngineSpeed_rpm_for_PreFueling_ms_in_CrankIn_DataStore()) =
      rtb_motohawk_data_read1_n4;
    (EngineSpeed_rpm_for_PreFueling_ms_in_CrankIdx_DataStore()) =
      TablePrelookup_real_T(rtb_motohawk_data_read1_n4,
      (EngineSpeed_rpm_for_PreFueling_ms_in_CrankIdxArr_DataStore()), 8,
      (EngineSpeed_rpm_for_PreFueling_ms_in_CrankIdx_DataStore()));
    rtb_motohawk_prelookup_n =
      (EngineSpeed_rpm_for_PreFueling_ms_in_CrankIdx_DataStore());
  }

  /* S-Function Block: <S64>/motohawk_interpolation_1d */
  {
    extern real_T TableInterpolation1D_real_T(uint16_T idx, real_T *tbl_data,
      uint32_T sz);
    rtb_motohawk_interpolation_1d_g = TableInterpolation1D_real_T
      (rtb_motohawk_prelookup_n, (real_T *)
       ((PreFueling_ms_in_CrankTbl_DataStore())), 8);
    (PreFueling_ms_in_Crank_DataStore()) = rtb_motohawk_interpolation_1d_g;
  }

  /* S-Function (motohawk_sfun_data_read): '<S64>/motohawk_data_read2' */
  rtb_motohawk_data_read2_l = Pedal_Pct_DataStore();

  /* MultiPortSwitch: '<S64>/MultiSwitch' incorporates:
   *  Constant: '<S64>/Constant'
   *  Constant: '<S64>/Constant2'
   *  Product: '<S64>/Product'
   *  S-Function (motohawk_sfun_calibration): '<S64>/motohawk_calibration2'
   *  S-Function (motohawk_sfun_calibration): '<S64>/motohawk_calibration3'
   *  S-Function (motohawk_sfun_data_read): '<S64>/motohawk_data_read'
   *  S-Function (motohawk_sfun_data_read): '<S64>/motohawk_data_read1'
   *  S-Function (motohawk_sfun_interpolation_1d): '<S64>/motohawk_interpolation_1d'
   *  S-Function (motohawk_sfun_prelookup): '<S64>/motohawk_prelookup'
   *  Sum: '<S64>/Add'
   *  Switch: '<S64>/Switch1'
   */
  switch (((uint8_T)State_DataStore())) {
   case 1:
    rtb_Gain2 = 0.0;
    break;

   case 2:
    rtb_Gain2 = rtb_motohawk_interpolation_1d_g;
    break;

   case 3:
    /* Switch: '<S64>/Switch' incorporates:
     *  Constant: '<S64>/Constant1'
     *  S-Function (motohawk_sfun_data_read): '<S64>/motohawk_data_read1'
     *  S-Function (motohawk_sfun_data_read): '<S64>/motohawk_data_read2'
     *  S-Function (motohawk_sfun_interpolation_2d): '<S64>/motohawk_interpolation_2d1'
     *  S-Function (motohawk_sfun_prelookup): '<S64>/motohawk_prelookup2'
     *  S-Function (motohawk_sfun_prelookup): '<S64>/motohawk_prelookup3'
     */
    if (rtb_motohawk_data_read1_n4 > 2000.0) {
      rtb_Gain2 = 0.0;
    } else {
      /* S-Function Block: <S64>/motohawk_prelookup2 */
      {
        extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
          ordarr[], uint32_T sz, uint16_T prev);
        (Pedal_pct_for_PreFueling_ms_in_RunDieselIn_DataStore()) =
          rtb_motohawk_data_read2_l;
        (Pedal_pct_for_PreFueling_ms_in_RunDieselIdx_DataStore()) =
          TablePrelookup_real_T(rtb_motohawk_data_read2_l,
          (Pedal_pct_for_PreFueling_ms_in_RunDieselIdxArr_DataStore()), 11,
          (Pedal_pct_for_PreFueling_ms_in_RunDieselIdx_DataStore()));
        rtb_motohawk_prelookup2_l =
          (Pedal_pct_for_PreFueling_ms_in_RunDieselIdx_DataStore());
      }

      /* S-Function Block: <S64>/motohawk_prelookup3 */
      {
        extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
          ordarr[], uint32_T sz, uint16_T prev);
        (EngineSpeed_rpm_for_PreFueling_ms_in_RunDieselIn_DataStore()) =
          rtb_motohawk_data_read1_n4;
        (EngineSpeed_rpm_for_PreFueling_ms_in_RunDieselIdx_DataStore()) =
          TablePrelookup_real_T(rtb_motohawk_data_read1_n4,
          (EngineSpeed_rpm_for_PreFueling_ms_in_RunDieselIdxArr_DataStore()), 10,
                                
          (EngineSpeed_rpm_for_PreFueling_ms_in_RunDieselIdx_DataStore()));
        rtb_motohawk_prelookup3_a =
          (EngineSpeed_rpm_for_PreFueling_ms_in_RunDieselIdx_DataStore());
      }

      /* S-Function Block: <S64>/motohawk_interpolation_2d1 */
      {
        extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T
          col_in, real_T *map_data, uint32_T row_sz, uint32_T col_sz);
        rtb_motohawk_interpolation_2d1_op = TableInterpolation2D_real_T
          (rtb_motohawk_prelookup3_a, rtb_motohawk_prelookup2_l, (real_T *)
           ((PreFueling_ms_in_RunDieselMap_DataStore())), 10, 11);
        (PreFueling_ms_in_RunDiesel_DataStore()) =
          rtb_motohawk_interpolation_2d1_op;
      }

      rtb_Gain2 = rtb_motohawk_interpolation_2d1_op;
    }

    /* End of Switch: '<S64>/Switch' */
    break;

   case 4:
    /* S-Function Block: <S64>/motohawk_prelookup1 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (Pedal_pct_for_PreFueling_ms_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read2_l;
      (Pedal_pct_for_PreFueling_ms_in_RunDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read2_l,
        (Pedal_pct_for_PreFueling_ms_in_RunDDFIdxArr_DataStore()), 11,
        (Pedal_pct_for_PreFueling_ms_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup1_ep =
        (Pedal_pct_for_PreFueling_ms_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S64>/motohawk_prelookup4 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_PreFueling_ms_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read1_n4;
      (EngineSpeed_rpm_for_PreFueling_ms_in_RunDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_n4,
        (EngineSpeed_rpm_for_PreFueling_ms_in_RunDDFIdxArr_DataStore()), 10,
        (EngineSpeed_rpm_for_PreFueling_ms_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup4_b =
        (EngineSpeed_rpm_for_PreFueling_ms_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S64>/motohawk_interpolation_2d2 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d2_k = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup4_b, rtb_motohawk_prelookup1_ep, (real_T *)
         ((PreFueling_ms_in_RunDDFMap_DataStore())), 10, 11);
      (PreFueling_ms_in_RunDDF_DataStore()) = rtb_motohawk_interpolation_2d2_k;
    }

    rtb_Gain2 = rtb_motohawk_interpolation_2d2_k * (real_T)
      (PreFuelingSPDDFGain_DataStore()) + (real_T)
      (PreFuelingSPDDFOffset_ms_DataStore());
    break;

   case 5:
    /* S-Function (motohawk_sfun_data_read): '<S64>/motohawk_data_read8' */
    rtb_motohawk_data_read8_e = AC_On_DataStore();

    /* MultiPortSwitch: '<S83>/Multiport Switch' incorporates:
     *  Constant: '<S83>/Constant'
     *  Product: '<S83>/Product'
     *  S-Function (motohawk_sfun_calibration): '<S83>/motohawk_calibration1'
     *  S-Function (motohawk_sfun_calibration): '<S83>/motohawk_calibration2'
     *  S-Function (motohawk_sfun_calibration): '<S83>/motohawk_calibration3'
     *  S-Function (motohawk_sfun_calibration): '<S83>/motohawk_calibration4'
     *  S-Function (motohawk_sfun_data_read): '<S64>/motohawk_data_read5'
     *  Sum: '<S83>/Add'
     */
    switch (rtb_motohawk_data_read8_e * 2 + DDF_On_DataStore()) {
     case 0:
      rtb_Gain2 = (PreFuelingIdleACOffDDFOff_ms_DataStore());
      break;

     case 1:
      rtb_Gain2 = (PreFuelingIdleACOffDDFOn_ms_DataStore());
      break;

     case 2:
      rtb_Gain2 = (PreFuelingIdleACOnDDFOff_ms_DataStore());
      break;

     default:
      rtb_Gain2 = (PreFuelingIdleACOnDDFOn_ms_DataStore());
      break;
    }

    /* End of MultiPortSwitch: '<S83>/Multiport Switch' */
    break;

   default:
    rtb_Gain2 = 0.0;
    break;
  }

  /* End of MultiPortSwitch: '<S64>/MultiSwitch' */

  /* S-Function (motohawk_sfun_data_write): '<S64>/motohawk_data_write' */
  /* Write to Data Storage as scalar: PreFueling_ms */
  {
    PreFueling_ms_DataStore() = rtb_Gain2;
  }

  /* S-Function (motohawk_sfun_data_read): '<S65>/motohawk_data_read1' */
  rtb_motohawk_data_read1_o0 = EngineSpeed_rpm_DataStore();

  /* S-Function (motohawk_sfun_data_read): '<S65>/motohawk_data_read2' */
  rtb_motohawk_data_read2_ca = Pedal_Pct_DataStore();

  /* MultiPortSwitch: '<S65>/MultiSwitch' incorporates:
   *  Constant: '<S65>/Constant'
   *  Constant: '<S65>/Constant1'
   *  Product: '<S65>/Product'
   *  S-Function (motohawk_sfun_calibration): '<S65>/motohawk_calibration2'
   *  S-Function (motohawk_sfun_calibration): '<S65>/motohawk_calibration3'
   *  S-Function (motohawk_sfun_data_read): '<S65>/motohawk_data_read'
   *  S-Function (motohawk_sfun_data_read): '<S65>/motohawk_data_read1'
   *  S-Function (motohawk_sfun_interpolation_1d): '<S65>/motohawk_interpolation_1d'
   *  S-Function (motohawk_sfun_prelookup): '<S65>/motohawk_prelookup'
   *  Sum: '<S65>/Add'
   */
  switch (((uint8_T)State_DataStore())) {
   case 1:
    rtb_Gain2 = 0.0;
    break;

   case 2:
    /* S-Function Block: <S65>/motohawk_prelookup */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_PreSOI_degBTDC_in_CrankIn_DataStore()) =
        rtb_motohawk_data_read1_o0;
      (EngineSpeed_rpm_for_PreSOI_degBTDC_in_CrankIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_o0,
        (EngineSpeed_rpm_for_PreSOI_degBTDC_in_CrankIdxArr_DataStore()), 8,
        (EngineSpeed_rpm_for_PreSOI_degBTDC_in_CrankIdx_DataStore()));
      rtb_motohawk_prelookup_f =
        (EngineSpeed_rpm_for_PreSOI_degBTDC_in_CrankIdx_DataStore());
    }

    /* S-Function Block: <S65>/motohawk_interpolation_1d */
    {
      extern real_T TableInterpolation1D_real_T(uint16_T idx, real_T *tbl_data,
        uint32_T sz);
      rtb_motohawk_interpolation_1d_f = TableInterpolation1D_real_T
        (rtb_motohawk_prelookup_f, (real_T *)
         ((PreSOI_degBTDC_in_CrankTbl_DataStore())), 8);
      (PreSOI_degBTDC_in_Crank_DataStore()) = rtb_motohawk_interpolation_1d_f;
    }

    rtb_Gain2 = rtb_motohawk_interpolation_1d_f;
    break;

   case 3:
    /* Switch: '<S65>/Switch' incorporates:
     *  Constant: '<S65>/Constant3'
     *  S-Function (motohawk_sfun_data_read): '<S65>/motohawk_data_read1'
     *  S-Function (motohawk_sfun_data_read): '<S65>/motohawk_data_read2'
     *  S-Function (motohawk_sfun_interpolation_2d): '<S65>/motohawk_interpolation_2d1'
     *  S-Function (motohawk_sfun_prelookup): '<S65>/motohawk_prelookup2'
     *  S-Function (motohawk_sfun_prelookup): '<S65>/motohawk_prelookup3'
     */
    if (rtb_motohawk_data_read1_o0 > 2000.0) {
      rtb_Gain2 = 38.0;
    } else {
      /* S-Function Block: <S65>/motohawk_prelookup2 */
      {
        extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
          ordarr[], uint32_T sz, uint16_T prev);
        (Pedal_pct_for_PreSOI_degBTDC_in_RunDieselIn_DataStore()) =
          rtb_motohawk_data_read2_ca;
        (Pedal_pct_for_PreSOI_degBTDC_in_RunDieselIdx_DataStore()) =
          TablePrelookup_real_T(rtb_motohawk_data_read2_ca,
          (Pedal_pct_for_PreSOI_degBTDC_in_RunDieselIdxArr_DataStore()), 11,
          (Pedal_pct_for_PreSOI_degBTDC_in_RunDieselIdx_DataStore()));
        rtb_motohawk_prelookup2_a =
          (Pedal_pct_for_PreSOI_degBTDC_in_RunDieselIdx_DataStore());
      }

      /* S-Function Block: <S65>/motohawk_prelookup3 */
      {
        extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
          ordarr[], uint32_T sz, uint16_T prev);
        (EngineSpeed_rpm_for_PreSOI_degBTDC_in_RunDieselIn_DataStore()) =
          rtb_motohawk_data_read1_o0;
        (EngineSpeed_rpm_for_PreSOI_degBTDC_in_RunDieselIdx_DataStore()) =
          TablePrelookup_real_T(rtb_motohawk_data_read1_o0,
          (EngineSpeed_rpm_for_PreSOI_degBTDC_in_RunDieselIdxArr_DataStore()),
          10, (EngineSpeed_rpm_for_PreSOI_degBTDC_in_RunDieselIdx_DataStore()));
        rtb_motohawk_prelookup3_c =
          (EngineSpeed_rpm_for_PreSOI_degBTDC_in_RunDieselIdx_DataStore());
      }

      /* S-Function Block: <S65>/motohawk_interpolation_2d1 */
      {
        extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T
          col_in, real_T *map_data, uint32_T row_sz, uint32_T col_sz);
        rtb_motohawk_interpolation_2d1_k = TableInterpolation2D_real_T
          (rtb_motohawk_prelookup3_c, rtb_motohawk_prelookup2_a, (real_T *)
           ((PreSOI_degBTDC_in_RunDieselMap_DataStore())), 10, 11);
        (PreSOI_degBTDC_in_RunDiesel_DataStore()) =
          rtb_motohawk_interpolation_2d1_k;
      }

      rtb_Gain2 = rtb_motohawk_interpolation_2d1_k;
    }

    /* End of Switch: '<S65>/Switch' */
    break;

   case 4:
    /* S-Function Block: <S65>/motohawk_prelookup1 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (Pedal_pct_for_PreSOI_degBTDC_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read2_ca;
      (Pedal_pct_for_PreSOI_degBTDC_in_RunDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read2_ca,
        (Pedal_pct_for_PreSOI_degBTDC_in_RunDDFIdxArr_DataStore()), 11,
        (Pedal_pct_for_PreSOI_degBTDC_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup1_k =
        (Pedal_pct_for_PreSOI_degBTDC_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S65>/motohawk_prelookup4 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_PreSOI_degBTDC_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read1_o0;
      (EngineSpeed_rpm_for_PreSOI_degBTDC_in_RunDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_o0,
        (EngineSpeed_rpm_for_PreSOI_degBTDC_in_RunDDFIdxArr_DataStore()), 10,
        (EngineSpeed_rpm_for_PreSOI_degBTDC_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup4_o =
        (EngineSpeed_rpm_for_PreSOI_degBTDC_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S65>/motohawk_interpolation_2d2 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d2_l = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup4_o, rtb_motohawk_prelookup1_k, (real_T *)
         ((PreSOI_degBTDC_in_RunDDFMap_DataStore())), 10, 11);
      (PreSOI_degBTDC_in_RunDDF_DataStore()) = rtb_motohawk_interpolation_2d2_l;
    }

    rtb_Gain2 = rtb_motohawk_interpolation_2d2_l * (real_T)
      (PreSOISPDDFGain_DataStore()) + (real_T)
      (PreSOISPDDFOffset_degBTDC_DataStore());
    break;

   case 5:
    /* S-Function (motohawk_sfun_data_read): '<S65>/motohawk_data_read6' */
    rtb_motohawk_data_read6_j = AC_On_DataStore();

    /* MultiPortSwitch: '<S84>/Multiport Switch' incorporates:
     *  Constant: '<S84>/Constant'
     *  Product: '<S84>/Product'
     *  S-Function (motohawk_sfun_calibration): '<S84>/motohawk_calibration1'
     *  S-Function (motohawk_sfun_calibration): '<S84>/motohawk_calibration2'
     *  S-Function (motohawk_sfun_calibration): '<S84>/motohawk_calibration3'
     *  S-Function (motohawk_sfun_calibration): '<S84>/motohawk_calibration4'
     *  S-Function (motohawk_sfun_data_read): '<S65>/motohawk_data_read5'
     *  Sum: '<S84>/Add'
     */
    switch (rtb_motohawk_data_read6_j * 2 + DDF_On_DataStore()) {
     case 0:
      rtb_Gain2 = (PreSOIIdleACOffDDFOff_degBTDC_DataStore());
      break;

     case 1:
      rtb_Gain2 = (PreSOIIdleACOffDDFOn_degBTDC_DataStore());
      break;

     case 2:
      rtb_Gain2 = (PreSOIIdleACOnDDFOff_degBTDC_DataStore());
      break;

     default:
      rtb_Gain2 = (PreSOIIdleACOnDDFOn_degBTDC_DataStore());
      break;
    }

    /* End of MultiPortSwitch: '<S84>/Multiport Switch' */
    break;

   default:
    rtb_Gain2 = 38.0;
    break;
  }

  /* End of MultiPortSwitch: '<S65>/MultiSwitch' */

  /* S-Function (motohawk_sfun_data_write): '<S65>/motohawk_data_write' */
  /* Write to Data Storage as scalar: PreSOI_degBTDC */
  {
    PreSOI_degBTDC_DataStore() = rtb_Gain2;
  }

  /* S-Function (motohawk_sfun_data_read): '<S66>/motohawk_data_read1' */
  rtb_motohawk_data_read1_m4 = EngineSpeed_rpm_DataStore();

  /* S-Function Block: <S66>/motohawk_prelookup */
  {
    extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
      ordarr[], uint32_T sz, uint16_T prev);
    (EngineSpeed_rpm_for_RailPSP_MPa_in_CrankIn_DataStore()) =
      rtb_motohawk_data_read1_m4;
    (EngineSpeed_rpm_for_RailPSP_MPa_in_CrankIdx_DataStore()) =
      TablePrelookup_real_T(rtb_motohawk_data_read1_m4,
      (EngineSpeed_rpm_for_RailPSP_MPa_in_CrankIdxArr_DataStore()), 8,
      (EngineSpeed_rpm_for_RailPSP_MPa_in_CrankIdx_DataStore()));
    rtb_motohawk_prelookup_h =
      (EngineSpeed_rpm_for_RailPSP_MPa_in_CrankIdx_DataStore());
  }

  /* S-Function Block: <S66>/motohawk_interpolation_1d */
  {
    extern real_T TableInterpolation1D_real_T(uint16_T idx, real_T *tbl_data,
      uint32_T sz);
    rtb_motohawk_interpolation_1d_k = TableInterpolation1D_real_T
      (rtb_motohawk_prelookup_h, (real_T *) ((RailPSP_MPa_in_CrankTbl_DataStore())),
       8);
    (RailPSP_MPa_in_Crank_DataStore()) = rtb_motohawk_interpolation_1d_k;
  }

  /* Switch: '<S66>/Switch' incorporates:
   *  S-Function (motohawk_sfun_data_read): '<S66>/motohawk_data_read1'
   *  S-Function (motohawk_sfun_interpolation_1d): '<S66>/motohawk_interpolation_1d'
   *  S-Function (motohawk_sfun_prelookup): '<S66>/motohawk_prelookup'
   */
  rtb_Merge_p = rtb_motohawk_interpolation_1d_k;

  /* S-Function (motohawk_sfun_data_read): '<S66>/motohawk_data_read2' */
  rtb_motohawk_data_read2_j = Pedal_Pct_DataStore();

  /* S-Function Block: <S66>/motohawk_prelookup4 */
  {
    extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
      ordarr[], uint32_T sz, uint16_T prev);
    (EngineSpeed_rpm_for_RailP_MPa_in_RunDDFIn_DataStore()) =
      rtb_motohawk_data_read1_m4;
    (EngineSpeed_rpm_for_RailP_MPa_in_RunDDFIdx_DataStore()) =
      TablePrelookup_real_T(rtb_motohawk_data_read1_m4,
      (EngineSpeed_rpm_for_RailP_MPa_in_RunDDFIdxArr_DataStore()), 10,
      (EngineSpeed_rpm_for_RailP_MPa_in_RunDDFIdx_DataStore()));
    rtb_motohawk_prelookup4 =
      (EngineSpeed_rpm_for_RailP_MPa_in_RunDDFIdx_DataStore());
  }

  /* S-Function Block: <S66>/motohawk_prelookup1 */
  {
    extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
      ordarr[], uint32_T sz, uint16_T prev);
    (Pedal_pct_for_RailP_MPa_in_RunDDFIn_DataStore()) =
      rtb_motohawk_data_read2_j;
    (Pedal_pct_for_RailP_MPa_in_RunDDFIdx_DataStore()) = TablePrelookup_real_T
      (rtb_motohawk_data_read2_j,
       (Pedal_pct_for_RailP_MPa_in_RunDDFIdxArr_DataStore()), 11,
       (Pedal_pct_for_RailP_MPa_in_RunDDFIdx_DataStore()));
    rtb_motohawk_prelookup1_e = (Pedal_pct_for_RailP_MPa_in_RunDDFIdx_DataStore());
  }

  /* S-Function Block: <S66>/motohawk_interpolation_2d2 */
  {
    extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
      real_T *map_data, uint32_T row_sz, uint32_T col_sz);
    rtb_Gain2 = TableInterpolation2D_real_T(rtb_motohawk_prelookup4,
      rtb_motohawk_prelookup1_e, (real_T *) ((RailP_MPa_in_RunDDFMap_DataStore())),
      10, 11);
    (RailP_MPa_in_RunDDF_DataStore()) = rtb_Gain2;
  }

  /* Product: '<S66>/Product' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S66>/motohawk_calibration3'
   */
  rtb_Gain4 = rtb_Gain2 * (real_T)(RailPSPDDFGain_DataStore());

  /* Sum: '<S66>/Add' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S66>/motohawk_calibration2'
   */
  rtb_Gain2 = rtb_Gain4 + (real_T)(RailPSPDDFOffset_MPa_DataStore());

  /* MultiPortSwitch: '<S66>/MultiSwitch' incorporates:
   *  Constant: '<S66>/Constant'
   *  S-Function (motohawk_sfun_calibration): '<S66>/motohawk_calibration1'
   *  S-Function (motohawk_sfun_data_read): '<S66>/motohawk_data_read'
   *  S-Function (motohawk_sfun_data_read): '<S66>/motohawk_data_read1'
   *  S-Function (motohawk_sfun_data_read): '<S66>/motohawk_data_read2'
   *  S-Function (motohawk_sfun_interpolation_2d): '<S66>/motohawk_interpolation_2d1'
   *  S-Function (motohawk_sfun_prelookup): '<S66>/motohawk_prelookup2'
   *  S-Function (motohawk_sfun_prelookup): '<S66>/motohawk_prelookup3'
   */
  switch (((uint8_T)State_DataStore())) {
   case 1:
    rtb_Switch1_d = 0.0;
    break;

   case 2:
    rtb_Switch1_d = rtb_Merge_p;
    break;

   case 3:
    /* S-Function Block: <S66>/motohawk_prelookup2 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (Pedal_pct_for_RailP_MPa_in_RunDieselIn_DataStore()) =
        rtb_motohawk_data_read2_j;
      (Pedal_pct_for_RailP_MPa_in_RunDieselIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read2_j,
        (Pedal_pct_for_RailP_MPa_in_RunDieselIdxArr_DataStore()), 11,
        (Pedal_pct_for_RailP_MPa_in_RunDieselIdx_DataStore()));
      rtb_motohawk_prelookup2_kb =
        (Pedal_pct_for_RailP_MPa_in_RunDieselIdx_DataStore());
    }

    /* S-Function Block: <S66>/motohawk_prelookup3 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_RailP_MPa_in_RunDieselIn_DataStore()) =
        rtb_motohawk_data_read1_m4;
      (EngineSpeed_rpm_for_RailP_MPa_in_RunDieselIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_m4,
        (EngineSpeed_rpm_for_RailP_MPa_in_RunDieselIdxArr_DataStore()), 10,
        (EngineSpeed_rpm_for_RailP_MPa_in_RunDieselIdx_DataStore()));
      rtb_motohawk_prelookup3_e =
        (EngineSpeed_rpm_for_RailP_MPa_in_RunDieselIdx_DataStore());
    }

    /* S-Function Block: <S66>/motohawk_interpolation_2d1 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d1_o = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup3_e, rtb_motohawk_prelookup2_kb, (real_T *)
         ((RailP_MPa_in_RunDieselMap_DataStore())), 10, 11);
      (RailP_MPa_in_RunDiesel_DataStore()) = rtb_motohawk_interpolation_2d1_o;
    }

    rtb_Switch1_d = rtb_motohawk_interpolation_2d1_o;
    break;

   case 4:
    rtb_Switch1_d = rtb_Gain2;
    break;

   case 5:
    rtb_Switch1_d = (RailPSPIdle_MPa_DataStore());
    break;

   default:
    rtb_Switch1_d = rtb_Gain2;
    break;
  }

  /* End of MultiPortSwitch: '<S66>/MultiSwitch' */

  /* Stateflow: '<S66>/Chart' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S66>/motohawk_calibration7'
   *  S-Function (motohawk_sfun_data_read): '<S66>/motohawk_data_read'
   */

  /* Gateway: Model/3 Set-point
     Generator/RailPSP_MPa/Chart */
  /* During: Model/3 Set-point
     Generator/RailPSP_MPa/Chart */
  if (CRVLAB_ECU_104_DWork.s85_is_active_c3_CRVLAB_ECU_104 == 0) {
    /* Entry: Model/3 Set-point
       Generator/RailPSP_MPa/Chart */
    CRVLAB_ECU_104_DWork.s85_is_active_c3_CRVLAB_ECU_104 = 1U;

    /* Transition: '<S85>:7' */
    CRVLAB_ECU_104_DWork.s85_is_c3_CRVLAB_ECU_104 = CRVLAB_ECU_104_IN_normal;
  } else {
    switch (CRVLAB_ECU_104_DWork.s85_is_c3_CRVLAB_ECU_104) {
     case CRVLAB_ECU_104_IN_cranktoidle:
      /* During 'cranktoidle': '<S85>:12' */
      if (CRVLAB_ECU_104_B.s85_railsp < rtb_Switch1_d) {
        /* Transition: '<S85>:15' */
        CRVLAB_ECU_104_DWork.s85_is_c3_CRVLAB_ECU_104 = CRVLAB_ECU_104_IN_normal;
      } else {
        CRVLAB_ECU_104_B.s85_railsp = CRVLAB_ECU_104_B.s85_railsp -
          (RailPReduce_i_DataStore());
      }
      break;

     case CRVLAB_ECU_104_IN_normal:
      /* During 'normal': '<S85>:6' */
      if (((uint8_T)State_DataStore()) == 2) {
        /* Transition: '<S85>:9' */
        CRVLAB_ECU_104_DWork.s85_is_c3_CRVLAB_ECU_104 = CRVLAB_ECU_104_IN_stall;
      } else {
        CRVLAB_ECU_104_B.s85_railsp = rtb_Switch1_d;
      }
      break;

     case CRVLAB_ECU_104_IN_stall:
      /* During 'stall': '<S85>:8' */
      if (((uint8_T)State_DataStore()) > 2) {
        /* Transition: '<S85>:11' */
        CRVLAB_ECU_104_DWork.s85_is_c3_CRVLAB_ECU_104 =
          CRVLAB_ECU_104_IN_cranktoidle;

        /* Entry 'cranktoidle': '<S85>:12' */
        CRVLAB_ECU_104_B.s85_railsp = rtb_Merge_p;
      } else {
        CRVLAB_ECU_104_B.s85_railsp = rtb_Merge_p;
      }
      break;

     default:
      /* Transition: '<S85>:7' */
      CRVLAB_ECU_104_DWork.s85_is_c3_CRVLAB_ECU_104 = CRVLAB_ECU_104_IN_normal;
      break;
    }
  }

  /* End of Stateflow: '<S66>/Chart' */
  /* Sum: '<S86>/Sum2' incorporates:
   *  Constant: '<S86>/Constant'
   *  Product: '<S86>/Product'
   *  Product: '<S86>/Product2'
   *  S-Function (motohawk_sfun_calibration): '<S66>/motohawk_calibration4'
   *  Sum: '<S86>/Sum'
   *  UnitDelay: '<S86>/Unit Delay'
   */
  rtb_Sum2_cw = (1.0 - (RailPSPFilterAlpha_DataStore())) *
    CRVLAB_ECU_104_DWork.s86_UnitDelay_DSTATE + CRVLAB_ECU_104_B.s85_railsp *
    (RailPSPFilterAlpha_DataStore());

  /* If: '<S88>/If' incorporates:
   *  Inport: '<S89>/In1'
   *  Inport: '<S90>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S88>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S88>/override_enable'
   */
  if ((RailPSPOverride_MPa_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S88>/NewValue' incorporates:
     *  ActionPort: '<S89>/Action Port'
     */
    rtb_Merge_p = (RailPSPOverride_MPa_new_DataStore());

    /* End of Outputs for SubSystem: '<S88>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S88>/OldValue' incorporates:
     *  ActionPort: '<S90>/Action Port'
     */
    rtb_Merge_p = rtb_Sum2_cw;

    /* End of Outputs for SubSystem: '<S88>/OldValue' */
  }

  /* End of If: '<S88>/If' */

  /* MinMax: '<S87>/MinMax' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S66>/motohawk_calibration5'
   */
  rtb_Gain2 = (rtb_Merge_p >= (RailPSPMin_MPa_DataStore())) || rtIsNaN
    ((RailPSPMin_MPa_DataStore())) ? rtb_Merge_p : (RailPSPMin_MPa_DataStore());

  /* MinMax: '<S87>/MinMax1' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S66>/motohawk_calibration6'
   */
  rtb_MinMax1_d = (rtb_Gain2 <= (RailPSPMax_MPa_DataStore())) || rtIsNaN
    ((RailPSPMax_MPa_DataStore())) ? rtb_Gain2 : (RailPSPMax_MPa_DataStore());

  /* S-Function (motohawk_sfun_data_write): '<S66>/motohawk_data_write' */
  /* Write to Data Storage as scalar: RailPSP_MPa */
  {
    RailPSP_MPa_DataStore() = rtb_MinMax1_d;
  }

  /* S-Function Block: <S66>/motohawk_prelookup5 */
  {
    extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
      ordarr[], uint32_T sz, uint16_T prev);
    (EngineSpeed_rpm_for_RailPSP_MPa_in_CrankDDFIn_DataStore()) =
      rtb_motohawk_data_read1_m4;
    (EngineSpeed_rpm_for_RailPSP_MPa_in_CrankDDFIdx_DataStore()) =
      TablePrelookup_real_T(rtb_motohawk_data_read1_m4,
      (EngineSpeed_rpm_for_RailPSP_MPa_in_CrankDDFIdxArr_DataStore()), 8,
      (EngineSpeed_rpm_for_RailPSP_MPa_in_CrankDDFIdx_DataStore()));
    rtb_motohawk_prelookup5_f =
      (EngineSpeed_rpm_for_RailPSP_MPa_in_CrankDDFIdx_DataStore());
  }

  /* S-Function Block: <S66>/motohawk_interpolation_1d1 */
  {
    extern real_T TableInterpolation1D_real_T(uint16_T idx, real_T *tbl_data,
      uint32_T sz);
    rtb_Gain2 = TableInterpolation1D_real_T(rtb_motohawk_prelookup5_f, (real_T *)
      ((RailPSP_MPa_in_CrankDDFTbl_DataStore())), 8);
    (RailPSP_MPa_in_CrankDDF_DataStore()) = rtb_Gain2;
  }

  /* S-Function (motohawk_sfun_data_read): '<S67>/motohawk_data_read1' */
  rtb_motohawk_data_read1_j = EngineSpeed_rpm_DataStore();

  /* S-Function (motohawk_sfun_data_read): '<S67>/motohawk_data_read2' */
  rtb_motohawk_data_read2_jc = Pedal_Pct_DataStore();

  /* MultiPortSwitch: '<S67>/MultiSwitch' incorporates:
   *  Constant: '<S67>/Constant'
   *  Constant: '<S67>/Constant1'
   *  Constant: '<S67>/Constant3'
   *  Product: '<S67>/Product'
   *  S-Function (motohawk_sfun_calibration): '<S67>/motohawk_calibration1'
   *  S-Function (motohawk_sfun_calibration): '<S67>/motohawk_calibration2'
   *  S-Function (motohawk_sfun_calibration): '<S67>/motohawk_calibration3'
   *  S-Function (motohawk_sfun_data_read): '<S67>/motohawk_data_read'
   *  S-Function (motohawk_sfun_data_read): '<S67>/motohawk_data_read1'
   *  S-Function (motohawk_sfun_data_read): '<S67>/motohawk_data_read2'
   *  S-Function (motohawk_sfun_interpolation_2d): '<S67>/motohawk_interpolation_2d1'
   *  S-Function (motohawk_sfun_prelookup): '<S67>/motohawk_prelookup2'
   *  S-Function (motohawk_sfun_prelookup): '<S67>/motohawk_prelookup3'
   *  Sum: '<S67>/Add'
   */
  switch (((uint8_T)State_DataStore())) {
   case 1:
    rtb_Merge_p = 0.0;
    break;

   case 2:
    rtb_Merge_p = 100.0;
    break;

   case 3:
    /* S-Function Block: <S67>/motohawk_prelookup2 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (Pedal_pct_for_TPS_Pct_in_RunDieselIn_DataStore()) =
        rtb_motohawk_data_read2_jc;
      (Pedal_pct_for_TPS_Pct_in_RunDieselIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read2_jc,
        (Pedal_pct_for_TPS_Pct_in_RunDieselIdxArr_DataStore()), 11,
        (Pedal_pct_for_TPS_Pct_in_RunDieselIdx_DataStore()));
      rtb_motohawk_prelookup2_ke =
        (Pedal_pct_for_TPS_Pct_in_RunDieselIdx_DataStore());
    }

    /* S-Function Block: <S67>/motohawk_prelookup3 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_TPS_Pct_in_RunDieselIn_DataStore()) =
        rtb_motohawk_data_read1_j;
      (EngineSpeed_rpm_for_TPS_Pct_in_RunDieselIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_j,
        (EngineSpeed_rpm_for_TPS_Pct_in_RunDieselIdxArr_DataStore()), 10,
        (EngineSpeed_rpm_for_TPS_Pct_in_RunDieselIdx_DataStore()));
      rtb_motohawk_prelookup3_d =
        (EngineSpeed_rpm_for_TPS_Pct_in_RunDieselIdx_DataStore());
    }

    /* S-Function Block: <S67>/motohawk_interpolation_2d1 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d1 = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup3_d, rtb_motohawk_prelookup2_ke, (real_T *)
         ((TPS_Pct_in_RunDieselMap_DataStore())), 10, 11);
      (TPS_Pct_in_RunDiesel_DataStore()) = rtb_motohawk_interpolation_2d1;
    }

    rtb_Merge_p = rtb_motohawk_interpolation_2d1;
    break;

   case 4:
    /* S-Function Block: <S67>/motohawk_prelookup1 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (Pedal_pct_for_TPS_Pct_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read2_jc;
      (Pedal_pct_for_TPS_Pct_in_RunDDFIdx_DataStore()) = TablePrelookup_real_T
        (rtb_motohawk_data_read2_jc,
         (Pedal_pct_for_TPS_Pct_in_RunDDFIdxArr_DataStore()), 11,
         (Pedal_pct_for_TPS_Pct_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup1_m = (Pedal_pct_for_TPS_Pct_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S67>/motohawk_prelookup4 */
    {
      extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
        ordarr[], uint32_T sz, uint16_T prev);
      (EngineSpeed_rpm_for_TPS_Pct_in_RunDDFIn_DataStore()) =
        rtb_motohawk_data_read1_j;
      (EngineSpeed_rpm_for_TPS_Pct_in_RunDDFIdx_DataStore()) =
        TablePrelookup_real_T(rtb_motohawk_data_read1_j,
        (EngineSpeed_rpm_for_TPS_Pct_in_RunDDFIdxArr_DataStore()), 10,
        (EngineSpeed_rpm_for_TPS_Pct_in_RunDDFIdx_DataStore()));
      rtb_motohawk_prelookup4_m =
        (EngineSpeed_rpm_for_TPS_Pct_in_RunDDFIdx_DataStore());
    }

    /* S-Function Block: <S67>/motohawk_interpolation_2d2 */
    {
      extern real_T TableInterpolation2D_real_T(uint16_T row_in, uint16_T col_in,
        real_T *map_data, uint32_T row_sz, uint32_T col_sz);
      rtb_motohawk_interpolation_2d2_j = TableInterpolation2D_real_T
        (rtb_motohawk_prelookup4_m, rtb_motohawk_prelookup1_m, (real_T *)
         ((TPS_Pct_in_RunDDFMap_DataStore())), 10, 11);
      (TPS_Pct_in_RunDDF_DataStore()) = rtb_motohawk_interpolation_2d2_j;
    }

    rtb_Merge_p = rtb_motohawk_interpolation_2d2_j * (real_T)
      (TPSSPDDFGain_DataStore()) + (real_T)(TPSSPDDFOffset_Pct_DataStore());
    break;

   case 5:
    rtb_Merge_p = (TPSSPIdle_Pct_DataStore());
    break;

   default:
    rtb_Merge_p = 36.0;
    break;
  }

  /* End of MultiPortSwitch: '<S67>/MultiSwitch' */
  /* If: '<S91>/If' incorporates:
   *  Inport: '<S92>/In1'
   *  Inport: '<S93>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S91>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S91>/override_enable'
   */
  if ((TPSSPOverride_Pct_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S91>/NewValue' incorporates:
     *  ActionPort: '<S92>/Action Port'
     */
    rtb_Merge_k = (TPSSPOverride_Pct_new_DataStore());

    /* End of Outputs for SubSystem: '<S91>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S91>/OldValue' incorporates:
     *  ActionPort: '<S93>/Action Port'
     */
    rtb_Merge_k = rtb_Merge_p;

    /* End of Outputs for SubSystem: '<S91>/OldValue' */
  }

  /* End of If: '<S91>/If' */

  /* S-Function (motohawk_sfun_data_write): '<S67>/motohawk_data_write' */
  /* Write to Data Storage as scalar: TPSSP_Pct */
  {
    TPSSP_Pct_DataStore() = rtb_Merge_k;
  }

  /* Logic: '<S26>/Logical Operator3' incorporates:
   *  S-Function (motohawk_sfun_data_read): '<S26>/motohawk_data_read11'
   */
  rtb_LogicalOperator5 = !Key_On_DataStore();

  /* Logic: '<S26>/Logical Operator1' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S26>/motohawk_calibration5'
   */
  rtb_LogicalOperator5 = (rtb_LogicalOperator5 && (StallNoRailP_On_DataStore()));

  /* Logic: '<S98>/Logical Operator1' incorporates:
   *  Constant: '<S125>/Constant'
   *  RelationalOperator: '<S125>/Compare'
   *  S-Function (motohawk_sfun_calibration): '<S98>/motohawk_calibration14'
   *  S-Function (motohawk_sfun_data_read): '<S26>/motohawk_data_read'
   */
  rtb_RelationalOperator_d = ((RailPCrankFixedDC_On_DataStore()) && (((uint8_T)
    State_DataStore()) == 2));

  /* RelationalOperator: '<S122>/Compare' incorporates:
   *  Constant: '<S122>/Constant'
   *  S-Function (motohawk_sfun_data_read): '<S26>/motohawk_data_read'
   */
  rtb_Compare_h = (((uint8_T)State_DataStore()) == 2);

  /* Switch: '<S98>/Switch2' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S98>/motohawk_calibration11'
   */
  if (rtb_Compare_h > 0) {
    rtb_Gain2 = (RailP_Crank_k_p_DataStore());
  } else {
    /* Switch: '<S98>/Switch3' incorporates:
     *  S-Function (motohawk_sfun_calibration): '<S98>/motohawk_calibration10'
     *  S-Function (motohawk_sfun_calibration): '<S98>/motohawk_calibration9'
     *  S-Function (motohawk_sfun_data_read): '<S98>/motohawk_data_read4'
     */
    if (DDF_On_DataStore()) {
      rtb_Gain2 = (RailP_DDF_k_p_DataStore());
    } else {
      rtb_Gain2 = (RailP_Diesel_k_p_DataStore());
    }

    /* End of Switch: '<S98>/Switch3' */
  }

  /* End of Switch: '<S98>/Switch2' */

  /* S-Function (motohawk_sfun_data_read): '<S26>/motohawk_data_read3' */
  rtb_motohawk_data_read3 = RailPSP_MPa_DataStore();

  /* Sum: '<S98>/Sum4' incorporates:
   *  S-Function (motohawk_sfun_data_read): '<S26>/motohawk_data_read2'
   *  S-Function (motohawk_sfun_data_read): '<S26>/motohawk_data_read3'
   */
  rtb_Sum4 = rtb_motohawk_data_read3 - RailP_MPa_DataStore();

  /* Product: '<S98>/Product2' */
  CRVLAB_ECU_104_B.s98_Product2 = rtb_Gain2 * rtb_Sum4;

  /* Switch: '<S98>/Switch4' incorporates:
   *  Constant: '<S123>/Constant'
   *  RelationalOperator: '<S123>/Compare'
   *  S-Function (motohawk_sfun_calibration): '<S98>/motohawk_calibration12'
   *  S-Function (motohawk_sfun_data_read): '<S26>/motohawk_data_read'
   */
  if ((((uint8_T)State_DataStore()) == 2) > 0) {
    rtb_Gain2 = (RailP_Crank_k_i_DataStore());
  } else {
    /* Switch: '<S98>/Switch1' incorporates:
     *  S-Function (motohawk_sfun_calibration): '<S98>/motohawk_calibration1'
     *  S-Function (motohawk_sfun_calibration): '<S98>/motohawk_calibration8'
     *  S-Function (motohawk_sfun_data_read): '<S98>/motohawk_data_read4'
     */
    if (DDF_On_DataStore()) {
      rtb_Gain2 = (RailP_DDF_k_i_DataStore());
    } else {
      rtb_Gain2 = (RailP_Diesel_k_i_DataStore());
    }

    /* End of Switch: '<S98>/Switch1' */
  }

  /* End of Switch: '<S98>/Switch4' */

  /* UnitDelay: '<S98>/Unit Delay1' */
  rtb_UnitDelay1_h = CRVLAB_ECU_104_DWork.s98_UnitDelay1_DSTATE;

  /* Product: '<S98>/Product3' incorporates:
   *  UnitDelay: '<S98>/Unit Delay1'
   */
  CRVLAB_ECU_104_B.s98_Product3 = rtb_Gain2 *
    CRVLAB_ECU_104_DWork.s98_UnitDelay1_DSTATE;

  /* Sum: '<S98>/Sum2' incorporates:
   *  UnitDelay: '<S98>/Unit Delay'
   */
  rtb_Merge_p = rtb_Sum4 - CRVLAB_ECU_104_DWork.s98_UnitDelay_DSTATE;

  /* S-Function Block: <S98>/motohawk_delta_time */
  {
    uint32_T delta;
    extern uint32_T Timer_FreeRunningCounter_GetDeltaUpdateReference_us(uint32_T
      * pReference_lower32Bits, uint32_T *pReference_upper32Bits);
    delta = Timer_FreeRunningCounter_GetDeltaUpdateReference_us
      (&CRVLAB_ECU_104_DWork.s98_motohawk_delta_time_DWORK1, NULL);
    rtb_motohawk_delta_time_m = ((real_T) delta) * 0.000001;
  }

  /* Product: '<S98>/Product4' incorporates:
   *  Product: '<S98>/Product'
   *  S-Function (motohawk_sfun_calibration): '<S98>/motohawk_calibration2'
   *  S-Function (motohawk_sfun_delta_time): '<S98>/motohawk_delta_time'
   */
  CRVLAB_ECU_104_B.s98_Product4 = rtb_Merge_p / rtb_motohawk_delta_time_m *
    (RailP_k_d_DataStore());

  /* Sum: '<S98>/Add' */
  rtb_Merge_p = (CRVLAB_ECU_104_B.s98_Product2 + CRVLAB_ECU_104_B.s98_Product3)
    + CRVLAB_ECU_104_B.s98_Product4;

  /* S-Function (motohawk_sfun_data_read): '<S98>/motohawk_data_read10' */
  rtb_motohawk_data_read10 = InstantSpeed_rpm_DataStore();

  /* S-Function Block: <S98>/motohawk_prelookup1 */
  {
    extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
      ordarr[], uint32_T sz, uint16_T prev);
    (Speed_RPM_for_RailP_Speed_Offset_DCIn_DataStore()) =
      rtb_motohawk_data_read10;
    (Speed_RPM_for_RailP_Speed_Offset_DCIdx_DataStore()) = TablePrelookup_real_T
      (rtb_motohawk_data_read10,
       (Speed_RPM_for_RailP_Speed_Offset_DCIdxArr_DataStore()), 11,
       (Speed_RPM_for_RailP_Speed_Offset_DCIdx_DataStore()));
    rtb_motohawk_prelookup1_i =
      (Speed_RPM_for_RailP_Speed_Offset_DCIdx_DataStore());
  }

  /* S-Function Block: <S98>/motohawk_interpolation_1d2 */
  {
    extern real_T TableInterpolation1D_real_T(uint16_T idx, real_T *tbl_data,
      uint32_T sz);
    rtb_motohawk_interpolation_1d2 = TableInterpolation1D_real_T
      (rtb_motohawk_prelookup1_i, (real_T *)
       ((RailP_Speed_Offset_DCTbl_DataStore())), 11);
    (RailP_Speed_Offset_DC_DataStore()) = rtb_motohawk_interpolation_1d2;
  }

  /* S-Function Block: <S98>/motohawk_prelookup5 */
  {
    extern uint16_T TablePrelookup_real_T(real_T in, const volatile real_T
      ordarr[], uint32_T sz, uint16_T prev);
    (RailPSP_MPa_for_RailP_Mean_DCIn_DataStore()) = rtb_motohawk_data_read3;
    (RailPSP_MPa_for_RailP_Mean_DCIdx_DataStore()) = TablePrelookup_real_T
      (rtb_motohawk_data_read3, (RailPSP_MPa_for_RailP_Mean_DCIdxArr_DataStore()),
       14, (RailPSP_MPa_for_RailP_Mean_DCIdx_DataStore()));
    rtb_motohawk_prelookup5_j = (RailPSP_MPa_for_RailP_Mean_DCIdx_DataStore());
  }

  /* S-Function Block: <S98>/motohawk_interpolation_1d1 */
  {
    extern real_T TableInterpolation1D_real_T(uint16_T idx, real_T *tbl_data,
      uint32_T sz);
    rtb_motohawk_interpolation_1d1 = TableInterpolation1D_real_T
      (rtb_motohawk_prelookup5_j, (real_T *) ((RailP_Mean_DCTbl_DataStore())),
       14);
    (RailP_Mean_DC_DataStore()) = rtb_motohawk_interpolation_1d1;
  }

  /* Sum: '<S98>/Sum5' incorporates:
   *  S-Function (motohawk_sfun_data_read): '<S26>/motohawk_data_read3'
   *  S-Function (motohawk_sfun_data_read): '<S98>/motohawk_data_read10'
   *  S-Function (motohawk_sfun_interpolation_1d): '<S98>/motohawk_interpolation_1d1'
   *  S-Function (motohawk_sfun_interpolation_1d): '<S98>/motohawk_interpolation_1d2'
   *  S-Function (motohawk_sfun_prelookup): '<S98>/motohawk_prelookup1'
   *  S-Function (motohawk_sfun_prelookup): '<S98>/motohawk_prelookup5'
   */
  rtb_Sum5 = (rtb_Merge_p + rtb_motohawk_interpolation_1d2) +
    rtb_motohawk_interpolation_1d1;

  /* Switch: '<S98>/Switch5' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S98>/motohawk_calibration13'
   */
  if (rtb_RelationalOperator_d) {
    rtb_Merge_p = (RailP_Crank_Fixed_DC_DataStore());
  } else {
    /* MinMax: '<S127>/MinMax' incorporates:
     *  S-Function (motohawk_sfun_calibration): '<S98>/motohawk_calibration5'
     */
    rtb_Add_i_idx = (rtb_Sum5 >= (RailP_u_min_DataStore())) || rtIsNaN
      ((RailP_u_min_DataStore())) ? rtb_Sum5 : (RailP_u_min_DataStore());

    /* MinMax: '<S127>/MinMax1' incorporates:
     *  MinMax: '<S127>/MinMax'
     *  S-Function (motohawk_sfun_calibration): '<S98>/motohawk_calibration6'
     */
    rtb_Merge_p = (rtb_Add_i_idx <= (RailP_u_max_DataStore())) || rtIsNaN
      ((RailP_u_max_DataStore())) ? rtb_Add_i_idx : (RailP_u_max_DataStore());
  }

  /* End of Switch: '<S98>/Switch5' */
  /* If: '<S128>/If' incorporates:
   *  Inport: '<S129>/In1'
   *  Inport: '<S130>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S128>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S128>/override_enable'
   */
  if ((RailP_u_Override_Absolute_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S128>/NewValue' incorporates:
     *  ActionPort: '<S129>/Action Port'
     */
    CRVLAB_ECU_104_B.s128_Merge = (RailP_u_Override_Absolute_new_DataStore());

    /* End of Outputs for SubSystem: '<S128>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S128>/OldValue' incorporates:
     *  ActionPort: '<S130>/Action Port'
     */
    CRVLAB_ECU_104_B.s128_Merge = rtb_Merge_p;

    /* End of Outputs for SubSystem: '<S128>/OldValue' */
  }

  /* End of If: '<S128>/If' */

  /* Switch: '<S26>/Switch2' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S26>/motohawk_calibration3'
   */
  if (rtb_LogicalOperator5) {
    rtb_Gain2 = (KeyOffRailP_DC_DataStore());
  } else {
    rtb_Gain2 = CRVLAB_ECU_104_B.s128_Merge;
  }

  /* End of Switch: '<S26>/Switch2' */

  /* S-Function (motohawk_sfun_data_write): '<S26>/motohawk_data_write' */
  /* Write to Data Storage as scalar: RailP_DC */
  {
    RailP_DC_DataStore() = rtb_Gain2;
  }

  /* Logic: '<S26>/Logical Operator2' incorporates:
   *  S-Function (motohawk_sfun_data_read): '<S26>/motohawk_data_read12'
   */
  rtb_LogicalOperator5 = !Key_On_DataStore();

  /* Logic: '<S26>/Logical Operator' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S26>/motohawk_calibration4'
   */
  rtb_LogicalOperator5 = (rtb_LogicalOperator5 && (StallNoThrottle_On_DataStore()));

  /* Sum: '<S99>/Sum4' incorporates:
   *  S-Function (motohawk_sfun_data_read): '<S26>/motohawk_data_read4'
   *  S-Function (motohawk_sfun_data_read): '<S26>/motohawk_data_read5'
   */
  rtb_Sum4_e = TPS_Pct_DataStore() - TPSSP_Pct_DataStore();

  /* Product: '<S99>/Product2' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S99>/motohawk_calibration'
   */
  CRVLAB_ECU_104_B.s99_Product2 = (Throttle_k_p_DataStore()) * rtb_Sum4_e;

  /* UnitDelay: '<S99>/Unit Delay1' */
  rtb_UnitDelay1_c = CRVLAB_ECU_104_DWork.s99_UnitDelay1_DSTATE;

  /* Product: '<S99>/Product3' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S99>/motohawk_calibration1'
   *  UnitDelay: '<S99>/Unit Delay1'
   */
  CRVLAB_ECU_104_B.s99_Product3 = (Throttle_k_i_DataStore()) *
    CRVLAB_ECU_104_DWork.s99_UnitDelay1_DSTATE;

  /* Sum: '<S99>/Sum2' incorporates:
   *  UnitDelay: '<S99>/Unit Delay'
   */
  rtb_Merge_p = rtb_Sum4_e - CRVLAB_ECU_104_DWork.s99_UnitDelay_DSTATE;

  /* S-Function Block: <S99>/motohawk_delta_time */
  {
    uint32_T delta;
    extern uint32_T Timer_FreeRunningCounter_GetDeltaUpdateReference_us(uint32_T
      * pReference_lower32Bits, uint32_T *pReference_upper32Bits);
    delta = Timer_FreeRunningCounter_GetDeltaUpdateReference_us
      (&CRVLAB_ECU_104_DWork.s99_motohawk_delta_time_DWORK1, NULL);
    rtb_motohawk_delta_time_p = ((real_T) delta) * 0.000001;
  }

  /* Product: '<S99>/Product4' incorporates:
   *  Product: '<S99>/Product'
   *  S-Function (motohawk_sfun_calibration): '<S99>/motohawk_calibration2'
   *  S-Function (motohawk_sfun_delta_time): '<S99>/motohawk_delta_time'
   */
  CRVLAB_ECU_104_B.s99_Product4 = rtb_Merge_p / rtb_motohawk_delta_time_p *
    (Throttle_k_d_DataStore());

  /* Sum: '<S99>/Add' */
  rtb_Merge_p = (CRVLAB_ECU_104_B.s99_Product2 + CRVLAB_ECU_104_B.s99_Product3)
    + CRVLAB_ECU_104_B.s99_Product4;

  /* Sum: '<S99>/Sum5' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S99>/motohawk_calibration7'
   */
  rtb_Gain2 = rtb_Merge_p + (Throttle_Mean_DC_DataStore());

  /* MinMax: '<S133>/MinMax' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S99>/motohawk_calibration5'
   */
  rtb_Gain4 = (rtb_Gain2 >= (Throttle_u_min_DataStore())) || rtIsNaN
    ((Throttle_u_min_DataStore())) ? rtb_Gain2 : (Throttle_u_min_DataStore());

  /* MinMax: '<S133>/MinMax1' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S99>/motohawk_calibration6'
   */
  rtb_Sum_g = (rtb_Gain4 <= (Throttle_u_max_DataStore())) || rtIsNaN
    ((Throttle_u_max_DataStore())) ? rtb_Gain4 : (Throttle_u_max_DataStore());

  /* If: '<S134>/If' incorporates:
   *  Inport: '<S135>/In1'
   *  Inport: '<S136>/In1'
   *  MinMax: '<S133>/MinMax1'
   *  S-Function (motohawk_sfun_calibration): '<S134>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S134>/override_enable'
   */
  if ((Throttle_u_Override_Absolute_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S134>/NewValue' incorporates:
     *  ActionPort: '<S135>/Action Port'
     */
    CRVLAB_ECU_104_B.s134_Merge = (Throttle_u_Override_Absolute_new_DataStore());

    /* End of Outputs for SubSystem: '<S134>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S134>/OldValue' incorporates:
     *  ActionPort: '<S136>/Action Port'
     */
    CRVLAB_ECU_104_B.s134_Merge = rtb_Sum_g;

    /* End of Outputs for SubSystem: '<S134>/OldValue' */
  }

  /* End of If: '<S134>/If' */

  /* Switch: '<S26>/Switch3' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S26>/motohawk_calibration2'
   */
  if (rtb_LogicalOperator5) {
    rtb_Gain4 = (KeyOffThrottle_ADC_DataStore());
  } else {
    rtb_Gain4 = CRVLAB_ECU_104_B.s134_Merge;
  }

  /* End of Switch: '<S26>/Switch3' */

  /* S-Function (motohawk_sfun_data_write): '<S26>/motohawk_data_write1' */
  /* Write to Data Storage as scalar: Throttle_ADC */
  {
    Throttle_ADC_DataStore() = rtb_Gain4;
  }

  /* Sum: '<S94>/Sum4' incorporates:
   *  S-Function (motohawk_sfun_data_read): '<S26>/motohawk_data_read6'
   *  S-Function (motohawk_sfun_data_read): '<S26>/motohawk_data_read7'
   */
  rtb_Sum4_b = EGRSP_Pct_DataStore() - EGR_Pct_DataStore();

  /* Product: '<S94>/Product2' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S94>/motohawk_calibration'
   */
  CRVLAB_ECU_104_B.s94_Product2 = (EGR_k_p_DataStore()) * rtb_Sum4_b;

  /* UnitDelay: '<S94>/Unit Delay1' */
  rtb_UnitDelay1_g = CRVLAB_ECU_104_DWork.s94_UnitDelay1_DSTATE;

  /* Product: '<S94>/Product3' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S94>/motohawk_calibration1'
   *  UnitDelay: '<S94>/Unit Delay1'
   */
  CRVLAB_ECU_104_B.s94_Product3 = (EGR_k_i_DataStore()) *
    CRVLAB_ECU_104_DWork.s94_UnitDelay1_DSTATE;

  /* Sum: '<S94>/Sum2' incorporates:
   *  UnitDelay: '<S94>/Unit Delay'
   */
  rtb_Merge_p = rtb_Sum4_b - CRVLAB_ECU_104_DWork.s94_UnitDelay_DSTATE;

  /* S-Function Block: <S94>/motohawk_delta_time */
  {
    uint32_T delta;
    extern uint32_T Timer_FreeRunningCounter_GetDeltaUpdateReference_us(uint32_T
      * pReference_lower32Bits, uint32_T *pReference_upper32Bits);
    delta = Timer_FreeRunningCounter_GetDeltaUpdateReference_us
      (&CRVLAB_ECU_104_DWork.s94_motohawk_delta_time_DWORK1, NULL);
    rtb_motohawk_delta_time_a = ((real_T) delta) * 0.000001;
  }

  /* Product: '<S94>/Product4' incorporates:
   *  Product: '<S94>/Product'
   *  S-Function (motohawk_sfun_calibration): '<S94>/motohawk_calibration2'
   *  S-Function (motohawk_sfun_delta_time): '<S94>/motohawk_delta_time'
   */
  CRVLAB_ECU_104_B.s94_Product4 = rtb_Merge_p / rtb_motohawk_delta_time_a *
    (EGR_k_d_DataStore());

  /* Sum: '<S94>/Add' */
  rtb_Merge_p = (CRVLAB_ECU_104_B.s94_Product2 + CRVLAB_ECU_104_B.s94_Product3)
    + CRVLAB_ECU_104_B.s94_Product4;

  /* Sum: '<S94>/Sum5' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S94>/motohawk_calibration7'
   */
  rtb_Sum5_i = rtb_Merge_p + (EGR_Mean_DC_DataStore());

  /* Switch: '<S94>/Switch2' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S94>/motohawk_calibration10'
   *  S-Function (motohawk_sfun_calibration): '<S94>/motohawk_calibration9'
   */
  if ((EGR_FixSP_On_DataStore())) {
    rtb_Merge_p = (EGR_FixSP_ADC_DataStore());
  } else {
    /* MinMax: '<S102>/MinMax' incorporates:
     *  S-Function (motohawk_sfun_calibration): '<S94>/motohawk_calibration5'
     */
    rtb_Add_i_idx = (rtb_Sum5_i >= (EGR_u_min_DataStore())) || rtIsNaN
      ((EGR_u_min_DataStore())) ? rtb_Sum5_i : (EGR_u_min_DataStore());

    /* MinMax: '<S102>/MinMax1' incorporates:
     *  MinMax: '<S102>/MinMax'
     *  S-Function (motohawk_sfun_calibration): '<S94>/motohawk_calibration6'
     */
    rtb_Merge_p = (rtb_Add_i_idx <= (EGR_u_max_DataStore())) || rtIsNaN
      ((EGR_u_max_DataStore())) ? rtb_Add_i_idx : (EGR_u_max_DataStore());
  }

  /* End of Switch: '<S94>/Switch2' */
  /* If: '<S103>/If' incorporates:
   *  Inport: '<S104>/In1'
   *  Inport: '<S105>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S103>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S103>/override_enable'
   */
  if ((EGR_u_Override_Absolute_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S103>/NewValue' incorporates:
     *  ActionPort: '<S104>/Action Port'
     */
    CRVLAB_ECU_104_B.s103_Merge = (EGR_u_Override_Absolute_new_DataStore());

    /* End of Outputs for SubSystem: '<S103>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S103>/OldValue' incorporates:
     *  ActionPort: '<S105>/Action Port'
     */
    CRVLAB_ECU_104_B.s103_Merge = rtb_Merge_p;

    /* End of Outputs for SubSystem: '<S103>/OldValue' */
  }

  /* End of If: '<S103>/If' */

  /* S-Function (motohawk_sfun_data_write): '<S26>/motohawk_data_write2' */
  /* Write to Data Storage as scalar: EGR_ADC */
  {
    EGR_ADC_DataStore() = CRVLAB_ECU_104_B.s103_Merge;
  }

  /* S-Function (motohawk_sfun_data_read): '<S26>/motohawk_data_read15' */
  rtb_motohawk_data_read15 = RailPSP_MPa_DataStore();

  /* Switch: '<S96>/Switch' incorporates:
   *  Constant: '<S96>/Constant5'
   *  S-Function (motohawk_sfun_data_read): '<S26>/motohawk_data_read13'
   */
  if (MainFueling_ms_DataStore() >= 0.2) {
    /* Sum: '<S96>/Add2' incorporates:
     *  S-Function (motohawk_sfun_calibration): '<S96>/motohawk_calibration2'
     */
    rtb_Merge_p = MainFueling_ms_DataStore() -
      (RailP_Correction_Main_Injector_Deadzone_DataStore());

    /* MinMax: '<S115>/MinMax' incorporates:
     *  Constant: '<S96>/Constant2'
     */
    rtb_Add_i_idx = rtb_Merge_p >= 0.0 ? rtb_Merge_p : 0.0;

    /* MinMax: '<S115>/MinMax1' incorporates:
     *  Constant: '<S96>/Constant2'
     *  MinMax: '<S115>/MinMax'
     */
    rtb_Sum_g = (rtb_Add_i_idx <= rtb_Merge_p) || rtIsNaN(rtb_Merge_p) ?
      rtb_Add_i_idx : rtb_Merge_p;

    /* Gain: '<S96>/Gain' incorporates:
     *  MinMax: '<S115>/MinMax1'
     */
    rtb_Merge_p = -rtb_Sum_g;

    /* MinMax: '<S114>/MinMax' incorporates:
     *  Constant: '<S96>/Constant1'
     */
    rtb_Add_i_idx = rtb_motohawk_data_read15 >= 5.0 ? rtb_motohawk_data_read15 :
      5.0;

    /* MinMax: '<S114>/MinMax1' incorporates:
     *  Constant: '<S96>/Constant1'
     *  MinMax: '<S114>/MinMax'
     */
    rtb_Sum_b = (rtb_Add_i_idx <= rtb_motohawk_data_read15) || rtIsNaN
      (rtb_motohawk_data_read15) ? rtb_Add_i_idx : rtb_motohawk_data_read15;

    /* Sum: '<S96>/Add1' incorporates:
     *  MinMax: '<S114>/MinMax1'
     *  S-Function (motohawk_sfun_data_read): '<S26>/motohawk_data_read14'
     */
    rtb_Switch1_d = rtb_Sum_b - RailP_Instant_MPa_DataStore();

    /* Switch: '<S96>/Switch2' incorporates:
     *  S-Function (motohawk_sfun_calibration): '<S96>/motohawk_calibration1'
     *  S-Function (motohawk_sfun_calibration): '<S96>/motohawk_calibration6'
     */
    if (rtb_Switch1_d > 0.0) {
      rtb_Add4 = (RailP_Correction_Main_Gain_Pos_DataStore());
    } else {
      rtb_Add4 = (RailP_Correction_Main_Gain_Neg_DataStore());
    }

    /* End of Switch: '<S96>/Switch2' */

    /* MinMax: '<S113>/MinMax' incorporates:
     *  MinMax: '<S114>/MinMax1'
     *  MinMax: '<S115>/MinMax1'
     *  Product: '<S96>/Divide'
     *  S-Function (motohawk_sfun_calibration): '<S96>/motohawk_calibration4'
     */
    rtb_Add_i_idx = rtb_Sum_g * rtb_Add4 * rtb_Switch1_d / rtb_Sum_b;
    rtb_Add_i_idx = (rtb_Add_i_idx >= (RailP_Correction_Main_Min_DataStore())) ||
      rtIsNaN((RailP_Correction_Main_Min_DataStore())) ? rtb_Add_i_idx :
      (RailP_Correction_Main_Min_DataStore());

    /* MinMax: '<S113>/MinMax1' incorporates:
     *  MinMax: '<S113>/MinMax'
     *  S-Function (motohawk_sfun_calibration): '<S96>/motohawk_calibration3'
     */
    rtb_Sum_g = (rtb_Add_i_idx <= (RailP_Correction_Main_Max_DataStore())) ||
      rtIsNaN((RailP_Correction_Main_Max_DataStore())) ? rtb_Add_i_idx :
      (RailP_Correction_Main_Max_DataStore());

    /* MinMax: '<S116>/MinMax' incorporates:
     *  MinMax: '<S113>/MinMax1'
     */
    rtb_Add_i_idx = (rtb_Sum_g >= rtb_Merge_p) || rtIsNaN(rtb_Merge_p) ?
      rtb_Sum_g : rtb_Merge_p;

    /* MinMax: '<S116>/MinMax1' incorporates:
     *  MinMax: '<S113>/MinMax1'
     *  MinMax: '<S116>/MinMax'
     */
    CRVLAB_ECU_104_B.s96_Switch = (rtb_Add_i_idx <= rtb_Sum_g) || rtIsNaN
      (rtb_Sum_g) ? rtb_Add_i_idx : rtb_Sum_g;
  } else {
    CRVLAB_ECU_104_B.s96_Switch = 0.0;
  }

  /* End of Switch: '<S96>/Switch' */

  /* Sum: '<S26>/Add1' incorporates:
   *  S-Function (motohawk_sfun_data_read): '<S26>/motohawk_data_read13'
   */
  rtb_Switch1_d = MainFueling_ms_DataStore() + CRVLAB_ECU_104_B.s96_Switch;

  /* Switch: '<S95>/Switch3' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S95>/motohawk_calibration10'
   *  S-Function (motohawk_sfun_calibration): '<S95>/motohawk_calibration4'
   *  S-Function (motohawk_sfun_data_read): '<S95>/motohawk_data_read4'
   */
  if (DDF_On_DataStore()) {
    rtb_Gain4 = (IdleSpeed_DDF_k_p_DataStore());
  } else {
    rtb_Gain4 = (IdleSpeed_Diesel_k_p_DataStore());
  }

  /* End of Switch: '<S95>/Switch3' */

  /* Gain: '<S95>/Gain' */
  rtb_Merge_p = 0.001 * rtb_Gain4;

  /* Switch: '<S26>/Switch1' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S26>/motohawk_calibration1'
   *  S-Function (motohawk_sfun_data_read): '<S26>/motohawk_data_read1'
   *  S-Function (motohawk_sfun_data_read): '<S26>/motohawk_data_read10'
   */
  if ((InstantSpdForIdle_On_DataStore())) {
    rtb_Gain4 = InstantSpeed_rpm_DataStore();
  } else {
    rtb_Gain4 = EngineSpeed_rpm_DataStore();
  }

  /* End of Switch: '<S26>/Switch1' */

  /* Sum: '<S95>/Sum4' incorporates:
   *  S-Function (motohawk_sfun_data_read): '<S26>/motohawk_data_read8'
   */
  rtb_Sum4_a = IdleSpeedSP_rpm_DataStore() - rtb_Gain4;

  /* Product: '<S95>/Product2' */
  CRVLAB_ECU_104_B.s95_Product2 = rtb_Merge_p * rtb_Sum4_a;

  /* Switch: '<S95>/Switch2' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S95>/motohawk_calibration1'
   *  S-Function (motohawk_sfun_calibration): '<S95>/motohawk_calibration9'
   *  S-Function (motohawk_sfun_data_read): '<S95>/motohawk_data_read4'
   */
  if (DDF_On_DataStore()) {
    rtb_Gain4 = (IdleSpeed_DDF_k_i_DataStore());
  } else {
    rtb_Gain4 = (IdleSpeed_Diesel_k_i_DataStore());
  }

  /* End of Switch: '<S95>/Switch2' */

  /* UnitDelay: '<S95>/Unit Delay1' */
  rtb_UnitDelay1_b = CRVLAB_ECU_104_DWork.s95_UnitDelay1_DSTATE;

  /* Product: '<S95>/Product3' incorporates:
   *  Gain: '<S95>/Gain1'
   *  UnitDelay: '<S95>/Unit Delay1'
   */
  CRVLAB_ECU_104_B.s95_Product3 = 0.001 * rtb_Gain4 *
    CRVLAB_ECU_104_DWork.s95_UnitDelay1_DSTATE;

  /* Sum: '<S95>/Sum2' incorporates:
   *  UnitDelay: '<S95>/Unit Delay'
   */
  rtb_Merge_p = rtb_Sum4_a - CRVLAB_ECU_104_DWork.s95_UnitDelay_DSTATE;

  /* S-Function Block: <S95>/motohawk_delta_time */
  {
    uint32_T delta;
    extern uint32_T Timer_FreeRunningCounter_GetDeltaUpdateReference_us(uint32_T
      * pReference_lower32Bits, uint32_T *pReference_upper32Bits);
    delta = Timer_FreeRunningCounter_GetDeltaUpdateReference_us
      (&CRVLAB_ECU_104_DWork.s95_motohawk_delta_time_DWORK1, NULL);
    rtb_motohawk_delta_time_b = ((real_T) delta) * 0.000001;
  }

  /* Product: '<S95>/Product4' incorporates:
   *  Product: '<S95>/Product'
   *  S-Function (motohawk_sfun_calibration): '<S95>/motohawk_calibration2'
   *  S-Function (motohawk_sfun_delta_time): '<S95>/motohawk_delta_time'
   */
  CRVLAB_ECU_104_B.s95_Product4 = rtb_Merge_p / rtb_motohawk_delta_time_b *
    (IdleSpeed_k_d_DataStore());

  /* Sum: '<S95>/Add' */
  rtb_Add_p = (CRVLAB_ECU_104_B.s95_Product2 + CRVLAB_ECU_104_B.s95_Product3) +
    CRVLAB_ECU_104_B.s95_Product4;

  /* RelationalOperator: '<S107>/Compare' incorporates:
   *  Constant: '<S107>/Constant'
   *  S-Function (motohawk_sfun_data_read): '<S26>/motohawk_data_read'
   */
  rtb_Compare_h = (((uint8_T)State_DataStore()) == 5);

  /* Switch: '<S95>/Switch1' incorporates:
   *  Constant: '<S95>/Constant1'
   *  Logic: '<S95>/Logical Operator1'
   *  S-Function (motohawk_sfun_calibration): '<S95>/motohawk_calibration7'
   */
  if ((rtb_Compare_h != 0) && ((IdleSpeed_on_DataStore()) != 0.0)) {
    /* MinMax: '<S109>/MinMax' incorporates:
     *  S-Function (motohawk_sfun_calibration): '<S95>/motohawk_calibration5'
     */
    rtb_Add_i_idx = (rtb_Add_p >= (IdleSpeed_u_min_DataStore())) || rtIsNaN
      ((IdleSpeed_u_min_DataStore())) ? rtb_Add_p : (IdleSpeed_u_min_DataStore());

    /* MinMax: '<S109>/MinMax1' incorporates:
     *  MinMax: '<S109>/MinMax'
     *  S-Function (motohawk_sfun_calibration): '<S95>/motohawk_calibration6'
     */
    rtb_Merge_p = (rtb_Add_i_idx <= (IdleSpeed_u_max_DataStore())) || rtIsNaN
      ((IdleSpeed_u_max_DataStore())) ? rtb_Add_i_idx :
      (IdleSpeed_u_max_DataStore());
  } else {
    rtb_Merge_p = 0.0;
  }

  /* End of Switch: '<S95>/Switch1' */
  /* If: '<S110>/If' incorporates:
   *  Inport: '<S111>/In1'
   *  Inport: '<S112>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S110>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S110>/override_enable'
   */
  if ((IdleSpeed_u_Override_Absolute_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S110>/NewValue' incorporates:
     *  ActionPort: '<S111>/Action Port'
     */
    CRVLAB_ECU_104_B.s110_Merge = (IdleSpeed_u_Override_Absolute_new_DataStore());

    /* End of Outputs for SubSystem: '<S110>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S110>/OldValue' incorporates:
     *  ActionPort: '<S112>/Action Port'
     */
    CRVLAB_ECU_104_B.s110_Merge = rtb_Merge_p;

    /* End of Outputs for SubSystem: '<S110>/OldValue' */
  }

  /* End of If: '<S110>/If' */

  /* Sum: '<S26>/Add2' */
  rtb_Add2_bt = CRVLAB_ECU_104_B.s110_Merge + rtb_Switch1_d;

  /* S-Function (motohawk_sfun_data_write): '<S26>/motohawk_data_write3' */
  /* Write to Data Storage as scalar: MainFueling_ms */
  {
    MainFueling_ms_DataStore() = rtb_Add2_bt;
  }

  /* Switch: '<S97>/Switch' incorporates:
   *  Constant: '<S97>/Constant2'
   *  S-Function (motohawk_sfun_data_read): '<S26>/motohawk_data_read18'
   */
  if (PilotFueling_ms_DataStore() >= 0.2) {
    /* Sum: '<S97>/Add2' incorporates:
     *  S-Function (motohawk_sfun_calibration): '<S97>/motohawk_calibration2'
     */
    rtb_Merge_p = PilotFueling_ms_DataStore() -
      (RailP_Correction_Pilot_Injector_Deadzone_DataStore());

    /* MinMax: '<S119>/MinMax' incorporates:
     *  Constant: '<S97>/Constant5'
     */
    rtb_Add_i_idx = rtb_Merge_p >= 0.0 ? rtb_Merge_p : 0.0;

    /* MinMax: '<S119>/MinMax1' incorporates:
     *  Constant: '<S97>/Constant5'
     *  MinMax: '<S119>/MinMax'
     */
    rtb_Sum_g = (rtb_Add_i_idx <= rtb_Merge_p) || rtIsNaN(rtb_Merge_p) ?
      rtb_Add_i_idx : rtb_Merge_p;

    /* Gain: '<S97>/Gain' incorporates:
     *  MinMax: '<S119>/MinMax1'
     */
    rtb_Merge_p = -rtb_Sum_g;

    /* MinMax: '<S118>/MinMax' incorporates:
     *  Constant: '<S97>/Constant1'
     */
    rtb_Add_i_idx = rtb_motohawk_data_read15 >= 5.0 ? rtb_motohawk_data_read15 :
      5.0;

    /* MinMax: '<S118>/MinMax1' incorporates:
     *  Constant: '<S97>/Constant1'
     *  MinMax: '<S118>/MinMax'
     */
    rtb_Sum_b = (rtb_Add_i_idx <= rtb_motohawk_data_read15) || rtIsNaN
      (rtb_motohawk_data_read15) ? rtb_Add_i_idx : rtb_motohawk_data_read15;

    /* Sum: '<S97>/Add1' incorporates:
     *  MinMax: '<S118>/MinMax1'
     *  S-Function (motohawk_sfun_data_read): '<S26>/motohawk_data_read14'
     */
    rtb_Switch1_d = rtb_Sum_b - RailP_Instant_MPa_DataStore();

    /* Switch: '<S97>/Switch2' incorporates:
     *  S-Function (motohawk_sfun_calibration): '<S97>/motohawk_calibration1'
     *  S-Function (motohawk_sfun_calibration): '<S97>/motohawk_calibration6'
     */
    if (rtb_Switch1_d > 0.0) {
      rtb_Add4 = (RailP_Correction_Pilot_Gain_Pos_DataStore());
    } else {
      rtb_Add4 = (RailP_Correction_Pilot_Gain_Neg_DataStore());
    }

    /* End of Switch: '<S97>/Switch2' */

    /* MinMax: '<S117>/MinMax' incorporates:
     *  MinMax: '<S118>/MinMax1'
     *  MinMax: '<S119>/MinMax1'
     *  Product: '<S97>/Divide'
     *  S-Function (motohawk_sfun_calibration): '<S97>/motohawk_calibration4'
     */
    rtb_Add_i_idx = rtb_Sum_g * rtb_Add4 * rtb_Switch1_d / rtb_Sum_b;
    rtb_Add_i_idx = (rtb_Add_i_idx >= (RailP_Correction_Pilot_Min_DataStore())) ||
      rtIsNaN((RailP_Correction_Pilot_Min_DataStore())) ? rtb_Add_i_idx :
      (RailP_Correction_Pilot_Min_DataStore());

    /* MinMax: '<S117>/MinMax1' incorporates:
     *  MinMax: '<S117>/MinMax'
     *  S-Function (motohawk_sfun_calibration): '<S97>/motohawk_calibration3'
     */
    rtb_Sum_g = (rtb_Add_i_idx <= (RailP_Correction_Pilot_Max_DataStore())) ||
      rtIsNaN((RailP_Correction_Pilot_Max_DataStore())) ? rtb_Add_i_idx :
      (RailP_Correction_Pilot_Max_DataStore());

    /* MinMax: '<S120>/MinMax' incorporates:
     *  MinMax: '<S117>/MinMax1'
     */
    rtb_Add_i_idx = (rtb_Sum_g >= rtb_Merge_p) || rtIsNaN(rtb_Merge_p) ?
      rtb_Sum_g : rtb_Merge_p;

    /* MinMax: '<S120>/MinMax1' incorporates:
     *  MinMax: '<S117>/MinMax1'
     *  MinMax: '<S120>/MinMax'
     */
    CRVLAB_ECU_104_B.s97_Switch = (rtb_Add_i_idx <= rtb_Sum_g) || rtIsNaN
      (rtb_Sum_g) ? rtb_Add_i_idx : rtb_Sum_g;
  } else {
    CRVLAB_ECU_104_B.s97_Switch = 0.0;
  }

  /* End of Switch: '<S97>/Switch' */
  /* RelationalOperator: '<S26>/Relational Operator' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S26>/motohawk_calibration8'
   */
  rtb_LogicalOperator5 = (CRVLAB_ECU_104_B.s110_Merge >
    (High_Idle_Main_ms_DataStore()));

  /* Switch: '<S26>/Switch6' incorporates:
   *  Constant: '<S26>/Constant'
   *  Logic: '<S26>/Logical Operator4'
   *  RelationalOperator: '<S26>/Relational Operator1'
   *  S-Function (motohawk_sfun_calibration): '<S26>/motohawk_calibration10'
   *  S-Function (motohawk_sfun_calibration): '<S26>/motohawk_calibration9'
   *  S-Function (motohawk_sfun_data_read): '<S26>/motohawk_data_read18'
   */
  if (rtb_LogicalOperator5 && (PilotFueling_ms_DataStore() <
       (Low_Pilot_ms_DataStore()))) {
    rtb_Gain4 = (HighLoad_Pilot_ms_DataStore());
  } else {
    rtb_Gain4 = 0.0;
  }

  /* End of Switch: '<S26>/Switch6' */

  /* Sum: '<S26>/Add5' incorporates:
   *  S-Function (motohawk_sfun_data_read): '<S26>/motohawk_data_read18'
   */
  rtb_Add5 = (CRVLAB_ECU_104_B.s97_Switch + PilotFueling_ms_DataStore()) +
    rtb_Gain4;

  /* S-Function (motohawk_sfun_data_write): '<S26>/motohawk_data_write4' */
  /* Write to Data Storage as scalar: PilotFueling_ms */
  {
    PilotFueling_ms_DataStore() = rtb_Add5;
  }

  /* RelationalOperator: '<S100>/Compare' incorporates:
   *  Constant: '<S100>/Constant'
   *  S-Function (motohawk_sfun_data_read): '<S26>/motohawk_data_read'
   */
  rtb_Compare_h = (((uint8_T)State_DataStore()) == 1);

  /* Logic: '<S94>/Logical Operator' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S94>/motohawk_calibration3'
   */
  rtb_LogicalOperator5 = (((EGR_reset_DataStore()) != 0.0) || (rtb_Compare_h !=
    0));

  /* MinMax: '<S101>/MinMax' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S94>/motohawk_calibration5'
   */
  rtb_Gain4 = (rtb_Sum5_i >= (EGR_u_min_DataStore())) || rtIsNaN
    ((EGR_u_min_DataStore())) ? rtb_Sum5_i : (EGR_u_min_DataStore());

  /* MinMax: '<S101>/MinMax1' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S94>/motohawk_calibration6'
   */
  rtb_Add_i_idx = (rtb_Gain4 <= (EGR_u_max_DataStore())) || rtIsNaN
    ((EGR_u_max_DataStore())) ? rtb_Gain4 : (EGR_u_max_DataStore());

  /* Switch: '<S94>/Switch' incorporates:
   *  Constant: '<S94>/Constant'
   */
  if (rtb_LogicalOperator5) {
    rtb_Gain4 = 0.0;
  } else {
    rtb_Gain4 = rtb_UnitDelay1_g;
  }

  /* End of Switch: '<S94>/Switch' */

  /* Sum: '<S94>/Sum' incorporates:
   *  MinMax: '<S101>/MinMax1'
   *  Product: '<S94>/Product1'
   *  Product: '<S94>/Product5'
   *  S-Function (motohawk_sfun_calibration): '<S94>/motohawk_calibration4'
   *  S-Function (motohawk_sfun_delta_time): '<S94>/motohawk_delta_time'
   *  Sum: '<S94>/Sum1'
   *  Sum: '<S94>/Sum3'
   */
  rtb_Sum5_i = ((rtb_Add_i_idx - rtb_Sum5_i) * (EGR_k_w_DataStore()) +
                rtb_Sum4_b) * rtb_motohawk_delta_time_a + rtb_Gain4;

  /* RelationalOperator: '<S106>/Compare' incorporates:
   *  Constant: '<S106>/Constant'
   *  S-Function (motohawk_sfun_data_read): '<S26>/motohawk_data_read'
   */
  rtb_Compare_h = (((uint8_T)State_DataStore()) != 5);

  /* Logic: '<S95>/Logical Operator' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S95>/motohawk_calibration3'
   */
  rtb_LogicalOperator5 = (((IdleSpeed_reset_DataStore()) != 0.0) ||
    (rtb_Compare_h != 0));

  /* MinMax: '<S108>/MinMax' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S95>/motohawk_calibration5'
   */
  rtb_Gain4 = (rtb_Add_p >= (IdleSpeed_u_min_DataStore())) || rtIsNaN
    ((IdleSpeed_u_min_DataStore())) ? rtb_Add_p : (IdleSpeed_u_min_DataStore());

  /* MinMax: '<S108>/MinMax1' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S95>/motohawk_calibration6'
   */
  rtb_Add_i_idx = (rtb_Gain4 <= (IdleSpeed_u_max_DataStore())) || rtIsNaN
    ((IdleSpeed_u_max_DataStore())) ? rtb_Gain4 : (IdleSpeed_u_max_DataStore());

  /* Switch: '<S95>/Switch' incorporates:
   *  Constant: '<S95>/Constant'
   */
  if (rtb_LogicalOperator5) {
    rtb_Gain4 = 0.0;
  } else {
    rtb_Gain4 = rtb_UnitDelay1_b;
  }

  /* End of Switch: '<S95>/Switch' */

  /* Sum: '<S95>/Sum' incorporates:
   *  MinMax: '<S108>/MinMax1'
   *  Product: '<S95>/Product1'
   *  Product: '<S95>/Product5'
   *  S-Function (motohawk_sfun_calibration): '<S95>/motohawk_calibration8'
   *  S-Function (motohawk_sfun_delta_time): '<S95>/motohawk_delta_time'
   *  Sum: '<S95>/Sum1'
   *  Sum: '<S95>/Sum3'
   */
  rtb_Sum_b = ((rtb_Add_i_idx - rtb_Add_p) * (IdleSpeed_k_w_DataStore()) +
               rtb_Sum4_a) * rtb_motohawk_delta_time_b + rtb_Gain4;

  /* RelationalOperator: '<S121>/Compare' incorporates:
   *  Constant: '<S121>/Constant'
   *  S-Function (motohawk_sfun_data_read): '<S26>/motohawk_data_read'
   */
  rtb_Compare_h = (((uint8_T)State_DataStore()) == 1);

  /* RelationalOperator: '<S124>/Compare' incorporates:
   *  Constant: '<S124>/Constant'
   *  S-Function (motohawk_sfun_data_read): '<S26>/motohawk_data_read'
   */
  rtb_Compare_mc = (((uint8_T)State_DataStore()) == 6);

  /* Logic: '<S98>/Logical Operator' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S98>/motohawk_calibration3'
   */
  rtb_LogicalOperator5 = (((RailP_reset_DataStore()) != 0.0) || (rtb_Compare_h
    != 0) || (rtb_Compare_mc != 0));

  /* MinMax: '<S126>/MinMax' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S98>/motohawk_calibration5'
   */
  rtb_Gain4 = (rtb_Sum5 >= (RailP_u_min_DataStore())) || rtIsNaN
    ((RailP_u_min_DataStore())) ? rtb_Sum5 : (RailP_u_min_DataStore());

  /* MinMax: '<S126>/MinMax1' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S98>/motohawk_calibration6'
   */
  rtb_Add_i_idx = (rtb_Gain4 <= (RailP_u_max_DataStore())) || rtIsNaN
    ((RailP_u_max_DataStore())) ? rtb_Gain4 : (RailP_u_max_DataStore());

  /* Switch: '<S98>/Switch' incorporates:
   *  Constant: '<S98>/Constant'
   */
  if (rtb_LogicalOperator5) {
    rtb_Gain4 = 0.0;
  } else {
    rtb_Gain4 = rtb_UnitDelay1_h;
  }

  /* End of Switch: '<S98>/Switch' */

  /* Sum: '<S98>/Sum' incorporates:
   *  MinMax: '<S126>/MinMax1'
   *  Product: '<S98>/Product1'
   *  Product: '<S98>/Product5'
   *  S-Function (motohawk_sfun_calibration): '<S98>/motohawk_calibration4'
   *  S-Function (motohawk_sfun_delta_time): '<S98>/motohawk_delta_time'
   *  Sum: '<S98>/Sum1'
   *  Sum: '<S98>/Sum3'
   */
  rtb_Sum_g = ((rtb_Add_i_idx - rtb_Sum5) * (RailP_k_w_DataStore()) + rtb_Sum4) *
    rtb_motohawk_delta_time_m + rtb_Gain4;

  /* RelationalOperator: '<S131>/Compare' incorporates:
   *  Constant: '<S131>/Constant'
   *  S-Function (motohawk_sfun_data_read): '<S26>/motohawk_data_read'
   */
  rtb_Compare_h = (((uint8_T)State_DataStore()) == 1);

  /* Logic: '<S99>/Logical Operator' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S99>/motohawk_calibration3'
   */
  rtb_LogicalOperator5 = (((Throttle_reset_DataStore()) != 0.0) ||
    (rtb_Compare_h != 0));

  /* MinMax: '<S132>/MinMax' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S99>/motohawk_calibration5'
   */
  rtb_Gain4 = (rtb_Gain2 >= (Throttle_u_min_DataStore())) || rtIsNaN
    ((Throttle_u_min_DataStore())) ? rtb_Gain2 : (Throttle_u_min_DataStore());

  /* Sum: '<S99>/Sum1' incorporates:
   *  MinMax: '<S132>/MinMax1'
   *  S-Function (motohawk_sfun_calibration): '<S99>/motohawk_calibration6'
   */
  rtb_Merge_p = ((rtb_Gain4 <= (Throttle_u_max_DataStore())) || rtIsNaN
                 ((Throttle_u_max_DataStore())) ? rtb_Gain4 :
                 (Throttle_u_max_DataStore())) - rtb_Gain2;

  /* Switch: '<S99>/Switch' incorporates:
   *  Constant: '<S99>/Constant'
   */
  if (rtb_LogicalOperator5) {
    rtb_Gain2 = 0.0;
  } else {
    rtb_Gain2 = rtb_UnitDelay1_c;
  }

  /* End of Switch: '<S99>/Switch' */

  /* Sum: '<S99>/Sum' incorporates:
   *  Product: '<S99>/Product1'
   *  Product: '<S99>/Product5'
   *  S-Function (motohawk_sfun_calibration): '<S99>/motohawk_calibration4'
   *  S-Function (motohawk_sfun_delta_time): '<S99>/motohawk_delta_time'
   *  Sum: '<S99>/Sum3'
   */
  rtb_Add4 = (rtb_Merge_p * (Throttle_k_w_DataStore()) + rtb_Sum4_e) *
    rtb_motohawk_delta_time_p + rtb_Gain2;

  /* S-Function (motohawk_sfun_data_read): '<S137>/motohawk_data_read1' */
  rtb_motohawk_data_read1_na = CNGSOI_degBTDC_DataStore();

  /* If: '<S151>/If' incorporates:
   *  Inport: '<S158>/In1'
   *  Inport: '<S159>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S151>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S151>/override_enable'
   */
  if ((CNGSOIOverride_degBTDC_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S151>/NewValue' incorporates:
     *  ActionPort: '<S158>/Action Port'
     */
    rtb_Merge_p = (CNGSOIOverride_degBTDC_new_DataStore());

    /* End of Outputs for SubSystem: '<S151>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S151>/OldValue' incorporates:
     *  ActionPort: '<S159>/Action Port'
     */
    rtb_Merge_p = rtb_motohawk_data_read1_na;

    /* End of Outputs for SubSystem: '<S151>/OldValue' */
  }

  /* End of If: '<S151>/If' */

  /* Gain: '<S137>/Gain' */
  rtb_Gain2 = 16.0 * rtb_Merge_p;

  /* DataTypeConversion: '<S137>/Data Type Conversion' */
  if (rtb_Gain2 < 32768.0) {
    if (rtb_Gain2 >= -32768.0) {
      rtb_DataTypeConversion4 = (int16_T)rtb_Gain2;
    } else {
      rtb_DataTypeConversion4 = MIN_int16_T;
    }
  } else {
    rtb_DataTypeConversion4 = MAX_int16_T;
  }

  /* End of DataTypeConversion: '<S137>/Data Type Conversion' */

  /* S-Function (motohawk_sfun_data_read): '<S137>/motohawk_data_read2' */
  rtb_motohawk_data_read2_ir = CNGFueling_ms_DataStore();

  /* If: '<S152>/If' incorporates:
   *  Inport: '<S160>/In1'
   *  Inport: '<S161>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S152>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S152>/override_enable'
   */
  if ((CNGFuelingOverride_ms_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S152>/NewValue' incorporates:
     *  ActionPort: '<S160>/Action Port'
     */
    rtb_Merge_p = (CNGFuelingOverride_ms_new_DataStore());

    /* End of Outputs for SubSystem: '<S152>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S152>/OldValue' incorporates:
     *  ActionPort: '<S161>/Action Port'
     */
    rtb_Merge_p = rtb_motohawk_data_read2_ir;

    /* End of Outputs for SubSystem: '<S152>/OldValue' */
  }

  /* End of If: '<S152>/If' */

  /* Switch: '<S145>/Switch2' incorporates:
   *  Constant: '<S145>/Constant1'
   *  S-Function (motohawk_sfun_calibration): '<S145>/motohawk_calibration5'
   */
  if (rtb_Merge_p > 3.0) {
    rtb_Gain2 = (CNG_inj1_trim_DataStore());
  } else {
    rtb_Gain2 = 0.0;
  }

  /* End of Switch: '<S145>/Switch2' */
  /* Product: '<S145>/Product' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S145>/motohawk_calibration1'
   */
  rtb_Gain4 = rtb_Gain2 * (real_T)(CNG_trim_Enable_DataStore());

  /* Gain: '<S137>/Gain2' incorporates:
   *  Sum: '<S145>/Add'
   */
  rtb_Gain2 = (rtb_Merge_p + rtb_Gain4) * 1000.0;

  /* DataTypeConversion: '<S137>/Data Type Conversion8' */
  if (rtb_Gain2 < 4.294967296E+9) {
    if (rtb_Gain2 >= 0.0) {
      rtb_DataTypeConversion5 = (uint32_T)rtb_Gain2;
    } else {
      rtb_DataTypeConversion5 = 0U;
    }
  } else {
    rtb_DataTypeConversion5 = MAX_uint32_T;
  }

  /* End of DataTypeConversion: '<S137>/Data Type Conversion8' */

  /* Saturate: '<S137>/Saturation1' */
  rtb_Saturation1 = rtb_DataTypeConversion5 <= 20000U ? rtb_DataTypeConversion5 :
    20000U;

  /* Gain: '<S137>/Gain6' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S137>/motohawk_calibration1'
   */
  rtb_Gain2 = 1000.0 * (Gas_Peak_Duration_DataStore());

  /* DataTypeConversion: '<S137>/Data Type Conversion2' */
  if (rtb_Gain2 < 65536.0) {
    if (rtb_Gain2 >= 0.0) {
      rtb_DataTypeConversion2_e = (uint16_T)rtb_Gain2;
    } else {
      rtb_DataTypeConversion2_e = 0U;
    }
  } else {
    rtb_DataTypeConversion2_e = MAX_uint16_T;
  }

  /* End of DataTypeConversion: '<S137>/Data Type Conversion2' */
  /* Gain: '<S137>/Gain4' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S137>/motohawk_calibration2'
   */
  rtb_Gain2 = 1000.0 * (Gas_Mech_Offset_DataStore());

  /* DataTypeConversion: '<S137>/Data Type Conversion3' */
  if (rtb_Gain2 < 4.294967296E+9) {
    if (rtb_Gain2 >= 0.0) {
      rtb_DataTypeConversion5 = (uint32_T)rtb_Gain2;
    } else {
      rtb_DataTypeConversion5 = 0U;
    }
  } else {
    rtb_DataTypeConversion5 = MAX_uint32_T;
  }

  /* End of DataTypeConversion: '<S137>/Data Type Conversion3' */
  /* Logic: '<S137>/Logical Operator2' incorporates:
   *  S-Function (motohawk_sfun_data_read): '<S137>/motohawk_data_read12'
   */
  rtb_LogicalOperator5 = !Key_On_DataStore();

  /* Logic: '<S137>/Logical Operator' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S137>/motohawk_calibration4'
   */
  rtb_LogicalOperator5 = (rtb_LogicalOperator5 && (StallNoCNG_On_DataStore()));

  /* S-Function Block: <S149>/motohawk_delta_time */
  {
    uint32_T delta;
    extern uint32_T Timer_FreeRunningCounter_GetDeltaUpdateReference_us(uint32_T
      * pReference_lower32Bits, uint32_T *pReference_upper32Bits);
    delta = Timer_FreeRunningCounter_GetDeltaUpdateReference_us
      (&CRVLAB_ECU_104_DWork.s149_motohawk_delta_time_DWORK1, NULL);
    rtb_motohawk_delta_time_pa = ((real_T) delta) * 0.000001;
  }

  /* S-Function (motohawk_sfun_data_read): '<S137>/motohawk_data_read' */
  rtb_motohawk_data_read1_nr = State_DataStore();

  /* RelationalOperator: '<S137>/Relational Operator1' incorporates:
   *  Constant: '<S137>/Constant'
   */
  rtb_BelowLoTarget = (rtb_motohawk_data_read1_nr >= 2);

  /* Switch: '<S149>/Switch' incorporates:
   *  Constant: '<S149>/Constant'
   *  S-Function (motohawk_sfun_data_read): '<S149>/motohawk_data_read'
   *  S-Function (motohawk_sfun_delta_time): '<S149>/motohawk_delta_time'
   *  Sum: '<S149>/Sum'
   */
  if (rtb_BelowLoTarget) {
    rtb_Gain2 = rtb_motohawk_delta_time_pa + TimeSinceCngInjEnabledl_DataStore();
  } else {
    rtb_Gain2 = 0.0;
  }

  /* End of Switch: '<S149>/Switch' */
  /* Logic: '<S137>/Logical Operator1' incorporates:
   *  RelationalOperator: '<S137>/Relational Operator5'
   *  S-Function (motohawk_sfun_calibration): '<S137>/motohawk_calibration3'
   */
  CRVLAB_ECU_104_B.s137_LogicalOperator1 = ((rtb_LogicalOperator5 || (rtb_Gain2 >=
    (TimeBeforeOnGasInj_DataStore()))));

  /* If: '<S150>/If' incorporates:
   *  Inport: '<S156>/In1'
   *  Inport: '<S157>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S150>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S150>/override_enable'
   */
  if ((CngInj1_Enabled_Override_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S150>/NewValue' incorporates:
     *  ActionPort: '<S156>/Action Port'
     */
    rtb_Merge_kz = (CngInj1_Enabled_Override_new_DataStore());

    /* End of Outputs for SubSystem: '<S150>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S150>/OldValue' incorporates:
     *  ActionPort: '<S157>/Action Port'
     */
    rtb_Merge_kz = CRVLAB_ECU_104_B.s137_LogicalOperator1;

    /* End of Outputs for SubSystem: '<S150>/OldValue' */
  }

  /* End of If: '<S150>/If' */

  /* S-Function Block: <S137>/motohawk_injector */
  {
    S_SeqOutPulseTiming TimingObj;
    E_SeqOutCond seq_enable;
    uint32_T elec_duration;
    boolean_T enable;
    S_SeqOutAttributes DynamicObj;

    /* INJ1D Injector Behavior */
    if ((rtb_DataTypeConversion2_e) !=
        (CRVLAB_ECU_104_DWork.s137_motohawk_injector_DWORK2)) {
      SetSeqOutInjPeakTime((E_ModuleResource)
                           (CRVLAB_ECU_104_DWork.s137_motohawk_injector_IWORK),
                           rtb_DataTypeConversion2_e);
      CRVLAB_ECU_104_DWork.s137_motohawk_injector_DWORK2 =
        rtb_DataTypeConversion2_e;
    }

    enable = (rtb_Merge_kz && (GetEncoderResourceInstantRPM() > 10));
    if ((enable) != (CRVLAB_ECU_104_DWork.s137_motohawk_injector_DWORK1)) {
      seq_enable = (enable) ? SEQ_ENABLED : SEQ_DISNEXT;
      SetSeqOutCond((E_ModuleResource)
                    (CRVLAB_ECU_104_DWork.s137_motohawk_injector_IWORK), 0,
                    seq_enable);
      CRVLAB_ECU_104_DWork.s137_motohawk_injector_DWORK1 = enable;
    }

    /* INJ1D Primary Pulse */
    elec_duration = (rtb_Saturation1) + (rtb_DataTypeConversion5);
    DynamicObj.uValidAttributesMask = USE_SEQ_TIMING ;

    /* Primary Pulse */
    DynamicObj.u1AffectedPulse = 0;
    DynamicObj.TimingObj.s2StartAngle = rtb_DataTypeConversion4;
    DynamicObj.TimingObj.u4Duration = elec_duration;
    DynamicObj.TimingObj.eCtrlMode = DURATION_CTRL;
    SetResourceAttributes((E_ModuleResource)
                          (CRVLAB_ECU_104_DWork.s137_motohawk_injector_IWORK),
                          &DynamicObj, BEHAVIOUR_INJ_SEQ);
  }

  /* Switch: '<S146>/Switch2' incorporates:
   *  Constant: '<S146>/Constant1'
   *  S-Function (motohawk_sfun_calibration): '<S146>/motohawk_calibration5'
   */
  if (rtb_Merge_p > 3.0) {
    rtb_Gain4 = (CNG_inj2_trim_DataStore());
  } else {
    rtb_Gain4 = 0.0;
  }

  /* End of Switch: '<S146>/Switch2' */

  /* S-Function (motohawk_sfun_data_read): '<S146>/motohawk_data_read11' */
  rtb_BelowLoTarget = CNG_trim_Enable_Cmd_DataStore();

  /* DataTypeConversion: '<S137>/Data Type Conversion9' incorporates:
   *  Gain: '<S137>/Gain3'
   *  Product: '<S146>/Product'
   *  Sum: '<S146>/Add'
   */
  rtb_Add_i_idx = (rtb_Gain4 * (real_T)rtb_BelowLoTarget + rtb_Merge_p) * 1000.0;
  if (rtb_Add_i_idx < 4.294967296E+9) {
    if (rtb_Add_i_idx >= 0.0) {
      u = (uint32_T)rtb_Add_i_idx;
    } else {
      u = 0U;
    }
  } else {
    u = MAX_uint32_T;
  }

  /* End of DataTypeConversion: '<S137>/Data Type Conversion9' */

  /* Saturate: '<S137>/Saturation2' incorporates:
   *  DataTypeConversion: '<S137>/Data Type Conversion9'
   */
  rtb_Saturation2 = u <= 20000U ? u : 20000U;

  /* If: '<S153>/If' incorporates:
   *  Inport: '<S162>/In1'
   *  Inport: '<S163>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S153>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S153>/override_enable'
   */
  if ((CngInj2_Enabled_Override_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S153>/NewValue' incorporates:
     *  ActionPort: '<S162>/Action Port'
     */
    rtb_Merge_e = (CngInj2_Enabled_Override_new_DataStore());

    /* End of Outputs for SubSystem: '<S153>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S153>/OldValue' incorporates:
     *  ActionPort: '<S163>/Action Port'
     */
    rtb_Merge_e = CRVLAB_ECU_104_B.s137_LogicalOperator1;

    /* End of Outputs for SubSystem: '<S153>/OldValue' */
  }

  /* End of If: '<S153>/If' */

  /* S-Function Block: <S137>/motohawk_injector1 */
  {
    S_SeqOutPulseTiming TimingObj;
    E_SeqOutCond seq_enable;
    uint32_T elec_duration;
    boolean_T enable;
    S_SeqOutAttributes DynamicObj;

    /* INJ2D Injector Behavior */
    if ((rtb_DataTypeConversion2_e) !=
        (CRVLAB_ECU_104_DWork.s137_motohawk_injector1_DWORK2)) {
      SetSeqOutInjPeakTime((E_ModuleResource)
                           (CRVLAB_ECU_104_DWork.s137_motohawk_injector1_IWORK),
                           rtb_DataTypeConversion2_e);
      CRVLAB_ECU_104_DWork.s137_motohawk_injector1_DWORK2 =
        rtb_DataTypeConversion2_e;
    }

    enable = (rtb_Merge_e && (GetEncoderResourceInstantRPM() > 10));
    if ((enable) != (CRVLAB_ECU_104_DWork.s137_motohawk_injector1_DWORK1)) {
      seq_enable = (enable) ? SEQ_ENABLED : SEQ_DISNEXT;
      SetSeqOutCond((E_ModuleResource)
                    (CRVLAB_ECU_104_DWork.s137_motohawk_injector1_IWORK), 0,
                    seq_enable);
      CRVLAB_ECU_104_DWork.s137_motohawk_injector1_DWORK1 = enable;
    }

    /* INJ2D Primary Pulse */
    elec_duration = (rtb_Saturation2) + (rtb_DataTypeConversion5);
    DynamicObj.uValidAttributesMask = USE_SEQ_TIMING ;

    /* Primary Pulse */
    DynamicObj.u1AffectedPulse = 0;
    DynamicObj.TimingObj.s2StartAngle = rtb_DataTypeConversion4;
    DynamicObj.TimingObj.u4Duration = elec_duration;
    DynamicObj.TimingObj.eCtrlMode = DURATION_CTRL;
    SetResourceAttributes((E_ModuleResource)
                          (CRVLAB_ECU_104_DWork.s137_motohawk_injector1_IWORK),
                          &DynamicObj, BEHAVIOUR_INJ_SEQ);
  }

  /* Switch: '<S147>/Switch2' incorporates:
   *  Constant: '<S147>/Constant1'
   *  S-Function (motohawk_sfun_calibration): '<S147>/motohawk_calibration5'
   */
  if (rtb_Merge_p > 3.0) {
    rtb_Switch1_d = (CNG_inj3_trim_DataStore());
  } else {
    rtb_Switch1_d = 0.0;
  }

  /* End of Switch: '<S147>/Switch2' */

  /* S-Function (motohawk_sfun_data_read): '<S147>/motohawk_data_read11' */
  rtb_BelowLoTarget = CNG_trim_Enable_Cmd_DataStore();

  /* Product: '<S147>/Product' */
  rtb_Gain4 = rtb_Switch1_d * (real_T)rtb_BelowLoTarget;

  /* DataTypeConversion: '<S137>/Data Type Conversion10' incorporates:
   *  Gain: '<S137>/Gain5'
   *  Sum: '<S147>/Add'
   */
  rtb_Add_i_idx = (rtb_Merge_p + rtb_Gain4) * 1000.0;
  if (rtb_Add_i_idx < 4.294967296E+9) {
    if (rtb_Add_i_idx >= 0.0) {
      u = (uint32_T)rtb_Add_i_idx;
    } else {
      u = 0U;
    }
  } else {
    u = MAX_uint32_T;
  }

  /* End of DataTypeConversion: '<S137>/Data Type Conversion10' */

  /* Saturate: '<S137>/Saturation3' incorporates:
   *  DataTypeConversion: '<S137>/Data Type Conversion10'
   */
  rtb_Saturation3 = u <= 20000U ? u : 20000U;

  /* If: '<S154>/If' incorporates:
   *  Inport: '<S164>/In1'
   *  Inport: '<S165>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S154>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S154>/override_enable'
   */
  if ((CngInj3_Enabled_Override_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S154>/NewValue' incorporates:
     *  ActionPort: '<S164>/Action Port'
     */
    rtb_Merge_ei = (CngInj3_Enabled_Override_new_DataStore());

    /* End of Outputs for SubSystem: '<S154>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S154>/OldValue' incorporates:
     *  ActionPort: '<S165>/Action Port'
     */
    rtb_Merge_ei = CRVLAB_ECU_104_B.s137_LogicalOperator1;

    /* End of Outputs for SubSystem: '<S154>/OldValue' */
  }

  /* End of If: '<S154>/If' */

  /* S-Function Block: <S137>/motohawk_injector2 */
  {
    S_SeqOutPulseTiming TimingObj;
    E_SeqOutCond seq_enable;
    uint32_T elec_duration;
    boolean_T enable;
    S_SeqOutAttributes DynamicObj;

    /* INJ3D Injector Behavior */
    if ((rtb_DataTypeConversion2_e) !=
        (CRVLAB_ECU_104_DWork.s137_motohawk_injector2_DWORK2)) {
      SetSeqOutInjPeakTime((E_ModuleResource)
                           (CRVLAB_ECU_104_DWork.s137_motohawk_injector2_IWORK),
                           rtb_DataTypeConversion2_e);
      CRVLAB_ECU_104_DWork.s137_motohawk_injector2_DWORK2 =
        rtb_DataTypeConversion2_e;
    }

    enable = (rtb_Merge_ei && (GetEncoderResourceInstantRPM() > 10));
    if ((enable) != (CRVLAB_ECU_104_DWork.s137_motohawk_injector2_DWORK1)) {
      seq_enable = (enable) ? SEQ_ENABLED : SEQ_DISNEXT;
      SetSeqOutCond((E_ModuleResource)
                    (CRVLAB_ECU_104_DWork.s137_motohawk_injector2_IWORK), 0,
                    seq_enable);
      CRVLAB_ECU_104_DWork.s137_motohawk_injector2_DWORK1 = enable;
    }

    /* INJ3D Primary Pulse */
    elec_duration = (rtb_Saturation3) + (rtb_DataTypeConversion5);
    DynamicObj.uValidAttributesMask = USE_SEQ_TIMING ;

    /* Primary Pulse */
    DynamicObj.u1AffectedPulse = 0;
    DynamicObj.TimingObj.s2StartAngle = rtb_DataTypeConversion4;
    DynamicObj.TimingObj.u4Duration = elec_duration;
    DynamicObj.TimingObj.eCtrlMode = DURATION_CTRL;
    SetResourceAttributes((E_ModuleResource)
                          (CRVLAB_ECU_104_DWork.s137_motohawk_injector2_IWORK),
                          &DynamicObj, BEHAVIOUR_INJ_SEQ);
  }

  /* Switch: '<S148>/Switch2' incorporates:
   *  Constant: '<S148>/Constant1'
   *  S-Function (motohawk_sfun_calibration): '<S148>/motohawk_calibration5'
   */
  if (rtb_Merge_p > 3.0) {
    rtb_Switch1_d = (CNG_inj4_trim_DataStore());
  } else {
    rtb_Switch1_d = 0.0;
  }

  /* End of Switch: '<S148>/Switch2' */

  /* S-Function (motohawk_sfun_data_read): '<S148>/motohawk_data_read11' */
  rtb_BelowLoTarget = CNG_trim_Enable_Cmd_DataStore();

  /* Product: '<S148>/Product' */
  rtb_Gain4 = rtb_Switch1_d * (real_T)rtb_BelowLoTarget;

  /* DataTypeConversion: '<S137>/Data Type Conversion11' incorporates:
   *  Gain: '<S137>/Gain7'
   *  Sum: '<S148>/Add'
   */
  rtb_Add_i_idx = (rtb_Merge_p + rtb_Gain4) * 1000.0;
  if (rtb_Add_i_idx < 4.294967296E+9) {
    if (rtb_Add_i_idx >= 0.0) {
      u = (uint32_T)rtb_Add_i_idx;
    } else {
      u = 0U;
    }
  } else {
    u = MAX_uint32_T;
  }

  /* End of DataTypeConversion: '<S137>/Data Type Conversion11' */

  /* Saturate: '<S137>/Saturation4' incorporates:
   *  DataTypeConversion: '<S137>/Data Type Conversion11'
   */
  rtb_Saturation4 = u <= 20000U ? u : 20000U;

  /* If: '<S155>/If' incorporates:
   *  Inport: '<S166>/In1'
   *  Inport: '<S167>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S155>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S155>/override_enable'
   */
  if ((CngInj4_Enabled_Override_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S155>/NewValue' incorporates:
     *  ActionPort: '<S166>/Action Port'
     */
    rtb_Merge_h = (CngInj4_Enabled_Override_new_DataStore());

    /* End of Outputs for SubSystem: '<S155>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S155>/OldValue' incorporates:
     *  ActionPort: '<S167>/Action Port'
     */
    rtb_Merge_h = CRVLAB_ECU_104_B.s137_LogicalOperator1;

    /* End of Outputs for SubSystem: '<S155>/OldValue' */
  }

  /* End of If: '<S155>/If' */

  /* S-Function Block: <S137>/motohawk_injector3 */
  {
    S_SeqOutPulseTiming TimingObj;
    E_SeqOutCond seq_enable;
    uint32_T elec_duration;
    boolean_T enable;
    S_SeqOutAttributes DynamicObj;

    /* INJ4D Injector Behavior */
    if ((rtb_DataTypeConversion2_e) !=
        (CRVLAB_ECU_104_DWork.s137_motohawk_injector3_DWORK2)) {
      SetSeqOutInjPeakTime((E_ModuleResource)
                           (CRVLAB_ECU_104_DWork.s137_motohawk_injector3_IWORK),
                           rtb_DataTypeConversion2_e);
      CRVLAB_ECU_104_DWork.s137_motohawk_injector3_DWORK2 =
        rtb_DataTypeConversion2_e;
    }

    enable = (rtb_Merge_h && (GetEncoderResourceInstantRPM() > 10));
    if ((enable) != (CRVLAB_ECU_104_DWork.s137_motohawk_injector3_DWORK1)) {
      seq_enable = (enable) ? SEQ_ENABLED : SEQ_DISNEXT;
      SetSeqOutCond((E_ModuleResource)
                    (CRVLAB_ECU_104_DWork.s137_motohawk_injector3_IWORK), 0,
                    seq_enable);
      CRVLAB_ECU_104_DWork.s137_motohawk_injector3_DWORK1 = enable;
    }

    /* INJ4D Primary Pulse */
    elec_duration = (rtb_Saturation4) + (rtb_DataTypeConversion5);
    DynamicObj.uValidAttributesMask = USE_SEQ_TIMING ;

    /* Primary Pulse */
    DynamicObj.u1AffectedPulse = 0;
    DynamicObj.TimingObj.s2StartAngle = rtb_DataTypeConversion4;
    DynamicObj.TimingObj.u4Duration = elec_duration;
    DynamicObj.TimingObj.eCtrlMode = DURATION_CTRL;
    SetResourceAttributes((E_ModuleResource)
                          (CRVLAB_ECU_104_DWork.s137_motohawk_injector3_IWORK),
                          &DynamicObj, BEHAVIOUR_INJ_SEQ);
  }

  /* S-Function (motohawk_sfun_data_write): '<S145>/motohawk_data_write2' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S145>/motohawk_calibration1'
   */
  /* Write to Data Storage as scalar: CNG_trim_Enable_Cmd */
  {
    CNG_trim_Enable_Cmd_DataStore() = (CNG_trim_Enable_DataStore());
  }

  /* Saturate: '<S149>/Saturation' */
  rtb_Saturation_ea = rtb_Gain2 >= 16000.0 ? 16000.0 : rtb_Gain2 <= 0.0 ? 0.0 :
    rtb_Gain2;

  /* S-Function (motohawk_sfun_data_write): '<S149>/motohawk_data_write' */
  /* Write to Data Storage as scalar: TimeSinceCngInjEnabledl */
  {
    TimeSinceCngInjEnabledl_DataStore() = rtb_Saturation_ea;
  }

  /* S-Function (motohawk_sfun_data_write): '<S137>/motohawk_data_write2' incorporates:
   *  S-Function (motohawk_sfun_data_read): '<S137>/motohawk_data_read11'
   */
  /* Write to Data Storage as scalar: DDF_On */
  {
    DDF_On_DataStore() = DDF_On_Cmd_DataStore();
  }

  /* DataTypeConversion: '<S138>/Data Type Conversion' incorporates:
   *  Constant: '<S138>/Constant1'
   *  Product: '<S138>/Product'
   *  S-Function (motohawk_sfun_data_read): '<S138>/motohawk_data_read1'
   */
  rtb_Add_i_idx = RailP_DC_DataStore() * 40.96;
  if (rtIsNaN(rtb_Add_i_idx) || rtIsInf(rtb_Add_i_idx)) {
    rtb_Add_i_idx = 0.0;
  } else {
    rtb_Add_i_idx = fmod(floor(rtb_Add_i_idx), 65536.0);
  }

  rtb_DataTypeConversion_l = (int16_T)(rtb_Add_i_idx < 0.0 ? (int16_T)-(int16_T)
                                       (uint16_T)-rtb_Add_i_idx : (int16_T)
    (uint16_T)rtb_Add_i_idx);

  /* End of DataTypeConversion: '<S138>/Data Type Conversion' */
  /* DataTypeConversion: '<S138>/Data Type Conversion1' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S138>/motohawk_calibration2'
   */
  if (rtIsNaNF((RailPFreq_cHz_DataStore())) || rtIsInfF((RailPFreq_cHz_DataStore
        ()))) {
    rtb_Switch1_b = 0.0F;
  } else {
    rtb_Switch1_b = (real32_T)fmod((real32_T)floor((RailPFreq_cHz_DataStore())),
      4.2949673E+9F);
  }

  rtb_DataTypeConversion1_fy = rtb_Switch1_b < 0.0F ? (uint32_T)-(int32_T)
    (uint32_T)-rtb_Switch1_b : (uint32_T)rtb_Switch1_b;

  /* End of DataTypeConversion: '<S138>/Data Type Conversion1' */

  /* DataTypeConversion: '<S138>/Data Type Conversion2' incorporates:
   *  Constant: '<S138>/Constant3'
   */
  rtb_DataTypeConversion2_g = TRUE;

  /* S-Function (motohawk_sfun_data_read): '<S139>/motohawk_data_read1' */
  rtb_motohawk_data_read1_d3 = PreSOI_degBTDC_DataStore();

  /* If: '<S180>/If' incorporates:
   *  Inport: '<S201>/In1'
   *  Inport: '<S202>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S180>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S180>/override_enable'
   */
  if ((Pre_SOI_Override_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S180>/NewValue' incorporates:
     *  ActionPort: '<S201>/Action Port'
     */
    CRVLAB_ECU_104_B.s180_Merge = (Pre_SOI_Override_new_DataStore());

    /* End of Outputs for SubSystem: '<S180>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S180>/OldValue' incorporates:
     *  ActionPort: '<S202>/Action Port'
     */
    CRVLAB_ECU_104_B.s180_Merge = rtb_motohawk_data_read1_d3;

    /* End of Outputs for SubSystem: '<S180>/OldValue' */
  }

  /* End of If: '<S180>/If' */

  /* RelationalOperator: '<S174>/Relational Operator' incorporates:
   *  Abs: '<S174>/Abs'
   *  Constant: '<S174>/Constant'
   */
  rtb_LogicalOperator5 = (fabs(CRVLAB_ECU_104_B.s180_Merge) < 0.001);

  /* S-Function (motohawk_sfun_data_read): '<S139>/motohawk_data_read4' */
  rtb_motohawk_data_read4_l = PreFueling_ms_DataStore();

  /* If: '<S177>/If' incorporates:
   *  Inport: '<S195>/In1'
   *  Inport: '<S196>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S177>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S177>/override_enable'
   */
  if ((Pre_Duration_Override_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S177>/NewValue' incorporates:
     *  ActionPort: '<S195>/Action Port'
     */
    CRVLAB_ECU_104_B.s177_Merge = (Pre_Duration_Override_new_DataStore());

    /* End of Outputs for SubSystem: '<S177>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S177>/OldValue' incorporates:
     *  ActionPort: '<S196>/Action Port'
     */
    CRVLAB_ECU_104_B.s177_Merge = rtb_motohawk_data_read4_l;

    /* End of Outputs for SubSystem: '<S177>/OldValue' */
  }

  /* End of If: '<S177>/If' */

  /* Switch: '<S174>/Switch' incorporates:
   *  Abs: '<S174>/Abs1'
   *  Constant: '<S174>/Constant1'
   *  Constant: '<S174>/Constant2'
   *  Logic: '<S174>/Logical Operator'
   *  RelationalOperator: '<S174>/Relational Operator1'
   *  Sum: '<S174>/Add'
   */
  if (rtb_LogicalOperator5 && (fabs(CRVLAB_ECU_104_B.s177_Merge) < 0.001)) {
    rtb_Switch1_d = 100.0 + CRVLAB_ECU_104_B.s180_Merge;
  } else {
    rtb_Switch1_d = CRVLAB_ECU_104_B.s180_Merge;
  }

  /* End of Switch: '<S174>/Switch' */

  /* Gain: '<S168>/Gain' */
  rtb_Gain2 = 16.0 * rtb_Switch1_d;

  /* DataTypeConversion: '<S168>/Data Type Conversion' */
  if (rtb_Gain2 < 32768.0) {
    if (rtb_Gain2 >= -32768.0) {
      rtb_DataTypeConversion4 = (int16_T)rtb_Gain2;
    } else {
      rtb_DataTypeConversion4 = MIN_int16_T;
    }
  } else {
    rtb_DataTypeConversion4 = MAX_int16_T;
  }

  /* End of DataTypeConversion: '<S168>/Data Type Conversion' */

  /* S-Function (motohawk_sfun_data_read): '<S139>/motohawk_data_read2' */
  rtb_motohawk_data_read2_g = PilotSOI_degBTDC_DataStore();

  /* If: '<S181>/If' incorporates:
   *  Inport: '<S203>/In1'
   *  Inport: '<S204>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S181>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S181>/override_enable'
   */
  if ((Pilot_SOI_Override_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S181>/NewValue' incorporates:
     *  ActionPort: '<S203>/Action Port'
     */
    CRVLAB_ECU_104_B.s181_Merge = (Pilot_SOI_Override_new_DataStore());

    /* End of Outputs for SubSystem: '<S181>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S181>/OldValue' incorporates:
     *  ActionPort: '<S204>/Action Port'
     */
    CRVLAB_ECU_104_B.s181_Merge = rtb_motohawk_data_read2_g;

    /* End of Outputs for SubSystem: '<S181>/OldValue' */
  }

  /* End of If: '<S181>/If' */

  /* Abs: '<S175>/Abs' */
  rtb_Gain2 = fabs(CRVLAB_ECU_104_B.s181_Merge);

  /* RelationalOperator: '<S175>/Relational Operator' incorporates:
   *  Constant: '<S175>/Constant'
   */
  rtb_LogicalOperator5 = (rtb_Gain2 < 0.001);

  /* S-Function (motohawk_sfun_data_read): '<S139>/motohawk_data_read5' */
  rtb_motohawk_data_read5_a = PilotFueling_ms_DataStore();

  /* If: '<S178>/If' incorporates:
   *  Inport: '<S197>/In1'
   *  Inport: '<S198>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S178>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S178>/override_enable'
   */
  if ((Pilot_Duration_Override_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S178>/NewValue' incorporates:
     *  ActionPort: '<S197>/Action Port'
     */
    CRVLAB_ECU_104_B.s178_Merge = (Pilot_Duration_Override_new_DataStore());

    /* End of Outputs for SubSystem: '<S178>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S178>/OldValue' incorporates:
     *  ActionPort: '<S198>/Action Port'
     */
    CRVLAB_ECU_104_B.s178_Merge = rtb_motohawk_data_read5_a;

    /* End of Outputs for SubSystem: '<S178>/OldValue' */
  }

  /* End of If: '<S178>/If' */

  /* Abs: '<S175>/Abs1' */
  rtb_Gain2 = fabs(CRVLAB_ECU_104_B.s178_Merge);

  /* Switch: '<S175>/Switch' incorporates:
   *  Constant: '<S175>/Constant1'
   *  Constant: '<S175>/Constant2'
   *  Logic: '<S175>/Logical Operator'
   *  RelationalOperator: '<S175>/Relational Operator1'
   *  Sum: '<S175>/Add'
   */
  if (rtb_LogicalOperator5 && (rtb_Gain2 < 0.001)) {
    rtb_Gain2 = 90.0 + CRVLAB_ECU_104_B.s181_Merge;
  } else {
    rtb_Gain2 = CRVLAB_ECU_104_B.s181_Merge;
  }

  /* End of Switch: '<S175>/Switch' */

  /* Gain: '<S168>/Gain2' */
  rtb_Gain4 = 16.0 * rtb_Gain2;

  /* DataTypeConversion: '<S168>/Data Type Conversion2' */
  if (rtb_Gain4 < 32768.0) {
    if (rtb_Gain4 >= -32768.0) {
      rtb_DataTypeConversion2_f_0 = (int16_T)rtb_Gain4;
    } else {
      rtb_DataTypeConversion2_f_0 = MIN_int16_T;
    }
  } else {
    rtb_DataTypeConversion2_f_0 = MAX_int16_T;
  }

  /* End of DataTypeConversion: '<S168>/Data Type Conversion2' */

  /* S-Function (motohawk_sfun_data_read): '<S139>/motohawk_data_read3' */
  rtb_motohawk_data_read3_h = MainSOI_degBTDC_DataStore();

  /* If: '<S182>/If' incorporates:
   *  Inport: '<S205>/In1'
   *  Inport: '<S206>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S182>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S182>/override_enable'
   */
  if ((Main_SOI_Override_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S182>/NewValue' incorporates:
     *  ActionPort: '<S205>/Action Port'
     */
    CRVLAB_ECU_104_B.s182_Merge = (Main_SOI_Override_new_DataStore());

    /* End of Outputs for SubSystem: '<S182>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S182>/OldValue' incorporates:
     *  ActionPort: '<S206>/Action Port'
     */
    CRVLAB_ECU_104_B.s182_Merge = rtb_motohawk_data_read3_h;

    /* End of Outputs for SubSystem: '<S182>/OldValue' */
  }

  /* End of If: '<S182>/If' */

  /* Abs: '<S176>/Abs' */
  rtb_Gain4 = fabs(CRVLAB_ECU_104_B.s182_Merge);

  /* RelationalOperator: '<S176>/Relational Operator' incorporates:
   *  Constant: '<S176>/Constant'
   */
  rtb_LogicalOperator5 = (rtb_Gain4 < 0.001);

  /* S-Function (motohawk_sfun_data_read): '<S139>/motohawk_data_read6' */
  rtb_motohawk_data_read6_g = MainFueling_ms_DataStore();

  /* If: '<S179>/If' incorporates:
   *  Inport: '<S199>/In1'
   *  Inport: '<S200>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S179>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S179>/override_enable'
   */
  if ((Main_Duration_Override_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S179>/NewValue' incorporates:
     *  ActionPort: '<S199>/Action Port'
     */
    CRVLAB_ECU_104_B.s179_Merge = (Main_Duration_Override_new_DataStore());

    /* End of Outputs for SubSystem: '<S179>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S179>/OldValue' incorporates:
     *  ActionPort: '<S200>/Action Port'
     */
    CRVLAB_ECU_104_B.s179_Merge = rtb_motohawk_data_read6_g;

    /* End of Outputs for SubSystem: '<S179>/OldValue' */
  }

  /* End of If: '<S179>/If' */

  /* Abs: '<S176>/Abs1' */
  rtb_Gain4 = fabs(CRVLAB_ECU_104_B.s179_Merge);

  /* Switch: '<S176>/Switch' incorporates:
   *  Constant: '<S176>/Constant1'
   *  Constant: '<S176>/Constant2'
   *  Logic: '<S176>/Logical Operator'
   *  RelationalOperator: '<S176>/Relational Operator1'
   *  Sum: '<S176>/Add'
   */
  if (rtb_LogicalOperator5 && (rtb_Gain4 < 0.001)) {
    rtb_Gain4 = -100.0 + CRVLAB_ECU_104_B.s182_Merge;
  } else {
    rtb_Gain4 = CRVLAB_ECU_104_B.s182_Merge;
  }

  /* End of Switch: '<S176>/Switch' */

  /* Gain: '<S168>/Gain4' */
  rtb_motohawk_data_read1_mk = 16.0 * rtb_Gain4;

  /* SignalConversion: '<S168>/TmpSignal ConversionAtMux PSPInport2' incorporates:
   *  DataTypeConversion: '<S168>/Data Type Conversion2'
   */
  rtb_TmpSignalConversionAtMuxPSPInport2[0] = rtb_DataTypeConversion4;
  rtb_TmpSignalConversionAtMuxPSPInport2[1] = rtb_DataTypeConversion2_f_0;

  /* DataTypeConversion: '<S168>/Data Type Conversion4' */
  if (rtb_motohawk_data_read1_mk < 32768.0) {
    if (rtb_motohawk_data_read1_mk >= -32768.0) {
      rtb_DataTypeConversion2_f_0 = (int16_T)rtb_motohawk_data_read1_mk;
    } else {
      rtb_DataTypeConversion2_f_0 = MIN_int16_T;
    }
  } else {
    rtb_DataTypeConversion2_f_0 = MAX_int16_T;
  }

  /* End of DataTypeConversion: '<S168>/Data Type Conversion4' */

  /* SignalConversion: '<S168>/TmpSignal ConversionAtMux PSPInport2' incorporates:
   *  DataTypeConversion: '<S168>/Data Type Conversion4'
   */
  rtb_TmpSignalConversionAtMuxPSPInport2[2] = rtb_DataTypeConversion2_f_0;

  /* Switch: '<S168>/Switch2' incorporates:
   *  Constant: '<S168>/Constant'
   *  S-Function (motohawk_sfun_calibration): '<S168>/motohawk_calibration4'
   */
  if (CRVLAB_ECU_104_B.s177_Merge > 0.15) {
    rtb_Add_i_idx = (Inj1Trim_DataStore());
  } else {
    rtb_Add_i_idx = 0.0;
  }

  if (CRVLAB_ECU_104_B.s178_Merge > 0.15) {
    rtb_UnitDelay1_h = (Inj1Trim_DataStore());
  } else {
    rtb_UnitDelay1_h = 0.0;
  }

  if (CRVLAB_ECU_104_B.s179_Merge > 0.15) {
    rtb_Sum5 = (Inj1Trim_DataStore());
  } else {
    rtb_Sum5 = 0.0;
  }

  /* End of Switch: '<S168>/Switch2' */
  /* Gain: '<S168>/Gain1' incorporates:
   *  Product: '<S168>/Product'
   *  S-Function (motohawk_sfun_calibration): '<S168>/motohawk_calibration1'
   *  Sum: '<S168>/Add'
   */
  rtb_motohawk_data_read1_mk = (rtb_Add_i_idx * (real_T)
    (Diesel_trim_Enable_DataStore()) + CRVLAB_ECU_104_B.s177_Merge) * 1000.0;

  /* DataTypeConversion: '<S168>/Data Type Conversion1' */
  if (rtb_motohawk_data_read1_mk < 4.294967296E+9) {
    if (rtb_motohawk_data_read1_mk >= 0.0) {
      u = (uint32_T)rtb_motohawk_data_read1_mk;
    } else {
      u = 0U;
    }
  } else {
    u = MAX_uint32_T;
  }

  /* End of DataTypeConversion: '<S168>/Data Type Conversion1' */

  /* Gain: '<S168>/Gain3' incorporates:
   *  Product: '<S168>/Product'
   *  S-Function (motohawk_sfun_calibration): '<S168>/motohawk_calibration1'
   *  Sum: '<S168>/Add'
   */
  rtb_motohawk_data_read1_mk = (rtb_UnitDelay1_h * (real_T)
    (Diesel_trim_Enable_DataStore()) + CRVLAB_ECU_104_B.s178_Merge) * 1000.0;

  /* DataTypeConversion: '<S168>/Data Type Conversion3' */
  if (rtb_motohawk_data_read1_mk < 4.294967296E+9) {
    if (rtb_motohawk_data_read1_mk >= 0.0) {
      rtb_DataTypeConversion5 = (uint32_T)rtb_motohawk_data_read1_mk;
    } else {
      rtb_DataTypeConversion5 = 0U;
    }
  } else {
    rtb_DataTypeConversion5 = MAX_uint32_T;
  }

  /* End of DataTypeConversion: '<S168>/Data Type Conversion3' */

  /* Gain: '<S168>/Gain5' incorporates:
   *  Product: '<S168>/Product'
   *  S-Function (motohawk_sfun_calibration): '<S168>/motohawk_calibration1'
   *  Sum: '<S168>/Add'
   */
  rtb_motohawk_data_read1_mk = (rtb_Sum5 * (real_T)(Diesel_trim_Enable_DataStore
                                 ()) + CRVLAB_ECU_104_B.s179_Merge) * 1000.0;

  /* DataTypeConversion: '<S168>/Data Type Conversion5' */
  if (rtb_motohawk_data_read1_mk < 4.294967296E+9) {
    if (rtb_motohawk_data_read1_mk >= 0.0) {
      rtb_DataTypeConversion1_f_0 = (uint32_T)rtb_motohawk_data_read1_mk;
    } else {
      rtb_DataTypeConversion1_f_0 = 0U;
    }
  } else {
    rtb_DataTypeConversion1_f_0 = MAX_uint32_T;
  }

  /* End of DataTypeConversion: '<S168>/Data Type Conversion5' */

  /* Saturate: '<S168>/Saturation' incorporates:
   *  DataTypeConversion: '<S168>/Data Type Conversion1'
   *  DataTypeConversion: '<S168>/Data Type Conversion5'
   */
  rtb_Saturation_ek[0] = u <= 2000U ? u : 2000U;
  rtb_Saturation_ek[1] = rtb_DataTypeConversion5 <= 2000U ?
    rtb_DataTypeConversion5 : 2000U;
  rtb_Saturation_ek[2] = rtb_DataTypeConversion1_f_0 <= 2000U ?
    rtb_DataTypeConversion1_f_0 : 2000U;

  /* Logic: '<S139>/Logical Operator2' incorporates:
   *  S-Function (motohawk_sfun_data_read): '<S139>/motohawk_data_read12'
   */
  rtb_LogicalOperator5 = !Key_On_DataStore();

  /* Logic: '<S139>/Logical Operator' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S139>/motohawk_calibration4'
   */
  rtb_LogicalOperator5 = (rtb_LogicalOperator5 && (StallNoDiesel_On_DataStore()));

  /* RelationalOperator: '<S172>/Compare' incorporates:
   *  Constant: '<S172>/Constant'
   *  S-Function (motohawk_sfun_data_read): '<S139>/motohawk_data_read'
   */
  rtb_Compare_h = (((uint8_T)State_DataStore()) == 1);

  /* RelationalOperator: '<S173>/Compare' incorporates:
   *  Constant: '<S173>/Constant'
   *  S-Function (motohawk_sfun_data_read): '<S139>/motohawk_data_read'
   */
  rtb_Compare_mc = (((uint8_T)State_DataStore()) == 2);

  /* Switch: '<S139>/Switch2' incorporates:
   *  Constant: '<S139>/Constant1'
   *  Constant: '<S139>/Constant3'
   *  Logic: '<S139>/Logical Operator1'
   *  Logic: '<S139>/Logical Operator3'
   *  RelationalOperator: '<S139>/Relational Operator'
   *  S-Function (motohawk_sfun_calibration): '<S139>/motohawk_calibration1'
   *  S-Function (motohawk_sfun_data_read): '<S139>/motohawk_data_read7'
   */
  if (rtb_LogicalOperator5 || (rtb_Compare_h != 0) || ((rtb_Compare_mc != 0) &&
       (RailP_MPa_DataStore() <= (RailP_Crank_Min_DataStore())))) {
    rtb_BelowLoTarget = FALSE;
  } else {
    rtb_BelowLoTarget = TRUE;
  }

  /* End of Switch: '<S139>/Switch2' */
  /* Logic: '<S168>/Logical Operator' incorporates:
   *  Constant: '<S168>/Constant1'
   *  RelationalOperator: '<S168>/Relational Operator'
   *  S-Function (motohawk_sfun_encoder_num_cylinders): '<S168>/motohawk_encoder_num_cylinders'
   */
  rtb_LogicalOperator5 = (rtb_BelowLoTarget && (1 <= ((uint8_T)
    (EncoderNumCylinders_DataStore()))));

  /* If: '<S183>/If' incorporates:
   *  Inport: '<S184>/In1'
   *  Inport: '<S185>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S183>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S183>/override_enable'
   */
  if ((Inj1Enabled_Override_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S183>/NewValue' incorporates:
     *  ActionPort: '<S184>/Action Port'
     */
    CRVLAB_ECU_104_B.s183_Merge = (Inj1Enabled_Override_new_DataStore());

    /* End of Outputs for SubSystem: '<S183>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S183>/OldValue' incorporates:
     *  ActionPort: '<S185>/Action Port'
     */
    CRVLAB_ECU_104_B.s183_Merge = rtb_LogicalOperator5;

    /* End of Outputs for SubSystem: '<S183>/OldValue' */
  }

  /* End of If: '<S183>/If' */

  /* S-Function (motohawk_sfun_data_write): '<S168>/motohawk_data_write2' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S168>/motohawk_calibration1'
   */
  /* Write to Data Storage as scalar: Diesel_trim_Enable_Cmd */
  {
    Diesel_trim_Enable_Cmd_DataStore() = (Diesel_trim_Enable_DataStore());
  }

  /* S-Function (motohawk_sfun_probe): '<S168>/motohawk_probe2' */
  (NoCyl_DataStore()) = (EncoderNumCylinders_DataStore());

  /* Gain: '<S169>/Gain' */
  rtb_motohawk_data_read1_mk = 16.0 * rtb_Switch1_d;

  /* DataTypeConversion: '<S169>/Data Type Conversion' */
  if (rtb_motohawk_data_read1_mk < 32768.0) {
    if (rtb_motohawk_data_read1_mk >= -32768.0) {
      rtb_DataTypeConversion_h_0 = (int16_T)rtb_motohawk_data_read1_mk;
    } else {
      rtb_DataTypeConversion_h_0 = MIN_int16_T;
    }
  } else {
    rtb_DataTypeConversion_h_0 = MAX_int16_T;
  }

  /* End of DataTypeConversion: '<S169>/Data Type Conversion' */

  /* Gain: '<S169>/Gain2' */
  rtb_motohawk_data_read1_mk = 16.0 * rtb_Gain2;

  /* DataTypeConversion: '<S169>/Data Type Conversion2' */
  if (rtb_motohawk_data_read1_mk < 32768.0) {
    if (rtb_motohawk_data_read1_mk >= -32768.0) {
      rtb_DataTypeConversion2_f_0 = (int16_T)rtb_motohawk_data_read1_mk;
    } else {
      rtb_DataTypeConversion2_f_0 = MIN_int16_T;
    }
  } else {
    rtb_DataTypeConversion2_f_0 = MAX_int16_T;
  }

  /* End of DataTypeConversion: '<S169>/Data Type Conversion2' */

  /* Gain: '<S169>/Gain4' */
  rtb_motohawk_data_read1_mk = 16.0 * rtb_Gain4;

  /* DataTypeConversion: '<S169>/Data Type Conversion4' */
  if (rtb_motohawk_data_read1_mk < 32768.0) {
    if (rtb_motohawk_data_read1_mk >= -32768.0) {
      rtb_DataTypeConversion4 = (int16_T)rtb_motohawk_data_read1_mk;
    } else {
      rtb_DataTypeConversion4 = MIN_int16_T;
    }
  } else {
    rtb_DataTypeConversion4 = MAX_int16_T;
  }

  /* End of DataTypeConversion: '<S169>/Data Type Conversion4' */

  /* SignalConversion: '<S169>/TmpSignal ConversionAtMux PSPInport2' incorporates:
   *  DataTypeConversion: '<S169>/Data Type Conversion'
   *  DataTypeConversion: '<S169>/Data Type Conversion2'
   */
  rtb_TmpSignalConversionAtMuxPSPInport2_l[0] = rtb_DataTypeConversion_h_0;
  rtb_TmpSignalConversionAtMuxPSPInport2_l[1] = rtb_DataTypeConversion2_f_0;
  rtb_TmpSignalConversionAtMuxPSPInport2_l[2] = rtb_DataTypeConversion4;

  /* Switch: '<S169>/Switch2' incorporates:
   *  Constant: '<S169>/Constant'
   *  S-Function (motohawk_sfun_calibration): '<S169>/motohawk_calibration4'
   */
  if (CRVLAB_ECU_104_B.s177_Merge > 0.15) {
    rtb_Add_i_idx = (Inj2Trim_DataStore());
  } else {
    rtb_Add_i_idx = 0.0;
  }

  if (CRVLAB_ECU_104_B.s178_Merge > 0.15) {
    rtb_UnitDelay1_h = (Inj2Trim_DataStore());
  } else {
    rtb_UnitDelay1_h = 0.0;
  }

  if (CRVLAB_ECU_104_B.s179_Merge > 0.15) {
    rtb_Sum5 = (Inj2Trim_DataStore());
  } else {
    rtb_Sum5 = 0.0;
  }

  /* End of Switch: '<S169>/Switch2' */

  /* S-Function (motohawk_sfun_data_read): '<S169>/motohawk_data_read11' */
  rtb_AboveHiTarget = Diesel_trim_Enable_Cmd_DataStore();

  /* Gain: '<S169>/Gain1' incorporates:
   *  Product: '<S169>/Product'
   *  Sum: '<S169>/Add'
   */
  rtb_motohawk_data_read1_mk = (rtb_Add_i_idx * (real_T)rtb_AboveHiTarget +
    CRVLAB_ECU_104_B.s177_Merge) * 1000.0;

  /* DataTypeConversion: '<S169>/Data Type Conversion1' */
  if (rtb_motohawk_data_read1_mk < 4.294967296E+9) {
    if (rtb_motohawk_data_read1_mk >= 0.0) {
      rtb_DataTypeConversion1_f_0 = (uint32_T)rtb_motohawk_data_read1_mk;
    } else {
      rtb_DataTypeConversion1_f_0 = 0U;
    }
  } else {
    rtb_DataTypeConversion1_f_0 = MAX_uint32_T;
  }

  /* End of DataTypeConversion: '<S169>/Data Type Conversion1' */

  /* Gain: '<S169>/Gain3' incorporates:
   *  Product: '<S169>/Product'
   *  Sum: '<S169>/Add'
   */
  rtb_motohawk_data_read1_mk = (rtb_UnitDelay1_h * (real_T)rtb_AboveHiTarget +
    CRVLAB_ECU_104_B.s178_Merge) * 1000.0;

  /* DataTypeConversion: '<S169>/Data Type Conversion3' */
  if (rtb_motohawk_data_read1_mk < 4.294967296E+9) {
    if (rtb_motohawk_data_read1_mk >= 0.0) {
      u = (uint32_T)rtb_motohawk_data_read1_mk;
    } else {
      u = 0U;
    }
  } else {
    u = MAX_uint32_T;
  }

  /* End of DataTypeConversion: '<S169>/Data Type Conversion3' */

  /* Gain: '<S169>/Gain5' incorporates:
   *  Product: '<S169>/Product'
   *  Sum: '<S169>/Add'
   */
  rtb_motohawk_data_read1_mk = (rtb_Sum5 * (real_T)rtb_AboveHiTarget +
    CRVLAB_ECU_104_B.s179_Merge) * 1000.0;

  /* DataTypeConversion: '<S169>/Data Type Conversion5' */
  if (rtb_motohawk_data_read1_mk < 4.294967296E+9) {
    if (rtb_motohawk_data_read1_mk >= 0.0) {
      rtb_DataTypeConversion5 = (uint32_T)rtb_motohawk_data_read1_mk;
    } else {
      rtb_DataTypeConversion5 = 0U;
    }
  } else {
    rtb_DataTypeConversion5 = MAX_uint32_T;
  }

  /* End of DataTypeConversion: '<S169>/Data Type Conversion5' */

  /* Saturate: '<S169>/Saturation' incorporates:
   *  DataTypeConversion: '<S169>/Data Type Conversion1'
   *  DataTypeConversion: '<S169>/Data Type Conversion3'
   */
  rtb_Saturation_o[0] = rtb_DataTypeConversion1_f_0 <= 2000U ?
    rtb_DataTypeConversion1_f_0 : 2000U;
  rtb_Saturation_o[1] = u <= 2000U ? u : 2000U;
  rtb_Saturation_o[2] = rtb_DataTypeConversion5 <= 2000U ?
    rtb_DataTypeConversion5 : 2000U;

  /* Logic: '<S169>/Logical Operator' incorporates:
   *  Constant: '<S169>/Constant1'
   *  RelationalOperator: '<S169>/Relational Operator'
   *  S-Function (motohawk_sfun_encoder_num_cylinders): '<S169>/motohawk_encoder_num_cylinders'
   */
  rtb_LogicalOperator5 = (rtb_BelowLoTarget && (2 <= ((uint8_T)
    (EncoderNumCylinders_DataStore()))));

  /* If: '<S186>/If' incorporates:
   *  Inport: '<S187>/In1'
   *  Inport: '<S188>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S186>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S186>/override_enable'
   */
  if ((Inj2Enabled_Override_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S186>/NewValue' incorporates:
     *  ActionPort: '<S187>/Action Port'
     */
    CRVLAB_ECU_104_B.s186_Merge = (Inj2Enabled_Override_new_DataStore());

    /* End of Outputs for SubSystem: '<S186>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S186>/OldValue' incorporates:
     *  ActionPort: '<S188>/Action Port'
     */
    CRVLAB_ECU_104_B.s186_Merge = rtb_LogicalOperator5;

    /* End of Outputs for SubSystem: '<S186>/OldValue' */
  }

  /* End of If: '<S186>/If' */

  /* Gain: '<S170>/Gain' */
  rtb_motohawk_data_read1_mk = 16.0 * rtb_Switch1_d;

  /* DataTypeConversion: '<S170>/Data Type Conversion' */
  if (rtb_motohawk_data_read1_mk < 32768.0) {
    if (rtb_motohawk_data_read1_mk >= -32768.0) {
      rtb_DataTypeConversion_h_0 = (int16_T)rtb_motohawk_data_read1_mk;
    } else {
      rtb_DataTypeConversion_h_0 = MIN_int16_T;
    }
  } else {
    rtb_DataTypeConversion_h_0 = MAX_int16_T;
  }

  /* End of DataTypeConversion: '<S170>/Data Type Conversion' */

  /* Gain: '<S170>/Gain2' */
  rtb_motohawk_data_read1_mk = 16.0 * rtb_Gain2;

  /* DataTypeConversion: '<S170>/Data Type Conversion2' */
  if (rtb_motohawk_data_read1_mk < 32768.0) {
    if (rtb_motohawk_data_read1_mk >= -32768.0) {
      rtb_DataTypeConversion2_f_0 = (int16_T)rtb_motohawk_data_read1_mk;
    } else {
      rtb_DataTypeConversion2_f_0 = MIN_int16_T;
    }
  } else {
    rtb_DataTypeConversion2_f_0 = MAX_int16_T;
  }

  /* End of DataTypeConversion: '<S170>/Data Type Conversion2' */

  /* Gain: '<S170>/Gain4' */
  rtb_motohawk_data_read1_mk = 16.0 * rtb_Gain4;

  /* DataTypeConversion: '<S170>/Data Type Conversion4' */
  if (rtb_motohawk_data_read1_mk < 32768.0) {
    if (rtb_motohawk_data_read1_mk >= -32768.0) {
      rtb_DataTypeConversion4 = (int16_T)rtb_motohawk_data_read1_mk;
    } else {
      rtb_DataTypeConversion4 = MIN_int16_T;
    }
  } else {
    rtb_DataTypeConversion4 = MAX_int16_T;
  }

  /* End of DataTypeConversion: '<S170>/Data Type Conversion4' */

  /* SignalConversion: '<S170>/TmpSignal ConversionAtMux PSPInport2' incorporates:
   *  DataTypeConversion: '<S170>/Data Type Conversion'
   *  DataTypeConversion: '<S170>/Data Type Conversion2'
   */
  rtb_TmpSignalConversionAtMuxPSPInport2_f[0] = rtb_DataTypeConversion_h_0;
  rtb_TmpSignalConversionAtMuxPSPInport2_f[1] = rtb_DataTypeConversion2_f_0;
  rtb_TmpSignalConversionAtMuxPSPInport2_f[2] = rtb_DataTypeConversion4;

  /* Switch: '<S170>/Switch2' incorporates:
   *  Constant: '<S170>/Constant'
   *  S-Function (motohawk_sfun_calibration): '<S170>/motohawk_calibration4'
   */
  if (CRVLAB_ECU_104_B.s177_Merge > 0.15) {
    rtb_Add_i_idx = (Inj3Trim_DataStore());
  } else {
    rtb_Add_i_idx = 0.0;
  }

  if (CRVLAB_ECU_104_B.s178_Merge > 0.15) {
    rtb_UnitDelay1_h = (Inj3Trim_DataStore());
  } else {
    rtb_UnitDelay1_h = 0.0;
  }

  if (CRVLAB_ECU_104_B.s179_Merge > 0.15) {
    rtb_Sum5 = (Inj3Trim_DataStore());
  } else {
    rtb_Sum5 = 0.0;
  }

  /* End of Switch: '<S170>/Switch2' */

  /* S-Function (motohawk_sfun_data_read): '<S170>/motohawk_data_read11' */
  rtb_AboveHiTarget = Diesel_trim_Enable_Cmd_DataStore();

  /* Gain: '<S170>/Gain1' incorporates:
   *  Product: '<S170>/Product'
   *  Sum: '<S170>/Add'
   */
  rtb_motohawk_data_read1_mk = (rtb_Add_i_idx * (real_T)rtb_AboveHiTarget +
    CRVLAB_ECU_104_B.s177_Merge) * 1000.0;

  /* DataTypeConversion: '<S170>/Data Type Conversion1' */
  if (rtb_motohawk_data_read1_mk < 4.294967296E+9) {
    if (rtb_motohawk_data_read1_mk >= 0.0) {
      rtb_DataTypeConversion1_f_0 = (uint32_T)rtb_motohawk_data_read1_mk;
    } else {
      rtb_DataTypeConversion1_f_0 = 0U;
    }
  } else {
    rtb_DataTypeConversion1_f_0 = MAX_uint32_T;
  }

  /* End of DataTypeConversion: '<S170>/Data Type Conversion1' */

  /* Gain: '<S170>/Gain3' incorporates:
   *  Product: '<S170>/Product'
   *  Sum: '<S170>/Add'
   */
  rtb_motohawk_data_read1_mk = (rtb_UnitDelay1_h * (real_T)rtb_AboveHiTarget +
    CRVLAB_ECU_104_B.s178_Merge) * 1000.0;

  /* DataTypeConversion: '<S170>/Data Type Conversion3' */
  if (rtb_motohawk_data_read1_mk < 4.294967296E+9) {
    if (rtb_motohawk_data_read1_mk >= 0.0) {
      u = (uint32_T)rtb_motohawk_data_read1_mk;
    } else {
      u = 0U;
    }
  } else {
    u = MAX_uint32_T;
  }

  /* End of DataTypeConversion: '<S170>/Data Type Conversion3' */

  /* Gain: '<S170>/Gain5' incorporates:
   *  Product: '<S170>/Product'
   *  Sum: '<S170>/Add'
   */
  rtb_motohawk_data_read1_mk = (rtb_Sum5 * (real_T)rtb_AboveHiTarget +
    CRVLAB_ECU_104_B.s179_Merge) * 1000.0;

  /* DataTypeConversion: '<S170>/Data Type Conversion5' */
  if (rtb_motohawk_data_read1_mk < 4.294967296E+9) {
    if (rtb_motohawk_data_read1_mk >= 0.0) {
      rtb_DataTypeConversion5 = (uint32_T)rtb_motohawk_data_read1_mk;
    } else {
      rtb_DataTypeConversion5 = 0U;
    }
  } else {
    rtb_DataTypeConversion5 = MAX_uint32_T;
  }

  /* End of DataTypeConversion: '<S170>/Data Type Conversion5' */

  /* Saturate: '<S170>/Saturation' incorporates:
   *  DataTypeConversion: '<S170>/Data Type Conversion1'
   *  DataTypeConversion: '<S170>/Data Type Conversion3'
   */
  rtb_Saturation_i[0] = rtb_DataTypeConversion1_f_0 <= 2000U ?
    rtb_DataTypeConversion1_f_0 : 2000U;
  rtb_Saturation_i[1] = u <= 2000U ? u : 2000U;
  rtb_Saturation_i[2] = rtb_DataTypeConversion5 <= 2000U ?
    rtb_DataTypeConversion5 : 2000U;

  /* Logic: '<S170>/Logical Operator' incorporates:
   *  Constant: '<S170>/Constant1'
   *  RelationalOperator: '<S170>/Relational Operator'
   *  S-Function (motohawk_sfun_encoder_num_cylinders): '<S170>/motohawk_encoder_num_cylinders'
   */
  rtb_LogicalOperator5 = (rtb_BelowLoTarget && (3 <= ((uint8_T)
    (EncoderNumCylinders_DataStore()))));

  /* If: '<S189>/If' incorporates:
   *  Inport: '<S190>/In1'
   *  Inport: '<S191>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S189>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S189>/override_enable'
   */
  if ((Inj3Enabled_Override_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S189>/NewValue' incorporates:
     *  ActionPort: '<S190>/Action Port'
     */
    CRVLAB_ECU_104_B.s189_Merge = (Inj3Enabled_Override_new_DataStore());

    /* End of Outputs for SubSystem: '<S189>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S189>/OldValue' incorporates:
     *  ActionPort: '<S191>/Action Port'
     */
    CRVLAB_ECU_104_B.s189_Merge = rtb_LogicalOperator5;

    /* End of Outputs for SubSystem: '<S189>/OldValue' */
  }

  /* End of If: '<S189>/If' */

  /* Gain: '<S171>/Gain' */
  rtb_Switch1_d *= 16.0;

  /* Gain: '<S171>/Gain2' */
  rtb_Gain2 *= 16.0;

  /* Gain: '<S171>/Gain4' */
  rtb_Gain4 *= 16.0;

  /* DataTypeConversion: '<S171>/Data Type Conversion4' */
  if (rtb_Gain4 < 32768.0) {
    if (rtb_Gain4 >= -32768.0) {
      rtb_DataTypeConversion4 = (int16_T)rtb_Gain4;
    } else {
      rtb_DataTypeConversion4 = MIN_int16_T;
    }
  } else {
    rtb_DataTypeConversion4 = MAX_int16_T;
  }

  /* End of DataTypeConversion: '<S171>/Data Type Conversion4' */

  /* DataTypeConversion: '<S171>/Data Type Conversion' */
  if (rtb_Switch1_d < 32768.0) {
    if (rtb_Switch1_d >= -32768.0) {
      rtb_DataTypeConversion2_f_0 = (int16_T)rtb_Switch1_d;
    } else {
      rtb_DataTypeConversion2_f_0 = MIN_int16_T;
    }
  } else {
    rtb_DataTypeConversion2_f_0 = MAX_int16_T;
  }

  /* End of DataTypeConversion: '<S171>/Data Type Conversion' */

  /* SignalConversion: '<S171>/TmpSignal ConversionAtMux PSPInport2' incorporates:
   *  DataTypeConversion: '<S171>/Data Type Conversion'
   */
  rtb_TmpSignalConversionAtMuxPSPInport2_m[0] = rtb_DataTypeConversion2_f_0;

  /* DataTypeConversion: '<S171>/Data Type Conversion2' */
  if (rtb_Gain2 < 32768.0) {
    if (rtb_Gain2 >= -32768.0) {
      rtb_DataTypeConversion2_f_0 = (int16_T)rtb_Gain2;
    } else {
      rtb_DataTypeConversion2_f_0 = MIN_int16_T;
    }
  } else {
    rtb_DataTypeConversion2_f_0 = MAX_int16_T;
  }

  /* End of DataTypeConversion: '<S171>/Data Type Conversion2' */

  /* SignalConversion: '<S171>/TmpSignal ConversionAtMux PSPInport2' incorporates:
   *  DataTypeConversion: '<S171>/Data Type Conversion2'
   */
  rtb_TmpSignalConversionAtMuxPSPInport2_m[1] = rtb_DataTypeConversion2_f_0;
  rtb_TmpSignalConversionAtMuxPSPInport2_m[2] = rtb_DataTypeConversion4;

  /* Switch: '<S171>/Switch2' incorporates:
   *  Constant: '<S171>/Constant'
   *  S-Function (motohawk_sfun_calibration): '<S171>/motohawk_calibration4'
   */
  if (CRVLAB_ECU_104_B.s177_Merge > 0.15) {
    rtb_Add_i_idx = (Inj4Trim_DataStore());
  } else {
    rtb_Add_i_idx = 0.0;
  }

  if (CRVLAB_ECU_104_B.s178_Merge > 0.15) {
    rtb_UnitDelay1_h = (Inj4Trim_DataStore());
  } else {
    rtb_UnitDelay1_h = 0.0;
  }

  if (CRVLAB_ECU_104_B.s179_Merge > 0.15) {
    rtb_Sum5 = (Inj4Trim_DataStore());
  } else {
    rtb_Sum5 = 0.0;
  }

  /* End of Switch: '<S171>/Switch2' */

  /* S-Function (motohawk_sfun_data_read): '<S171>/motohawk_data_read11' */
  rtb_AboveHiTarget = Diesel_trim_Enable_Cmd_DataStore();

  /* Gain: '<S171>/Gain1' incorporates:
   *  Product: '<S171>/Product'
   *  Sum: '<S171>/Add'
   */
  rtb_motohawk_data_read1_mk = (rtb_Add_i_idx * (real_T)rtb_AboveHiTarget +
    CRVLAB_ECU_104_B.s177_Merge) * 1000.0;

  /* DataTypeConversion: '<S171>/Data Type Conversion1' */
  if (rtb_motohawk_data_read1_mk < 4.294967296E+9) {
    if (rtb_motohawk_data_read1_mk >= 0.0) {
      rtb_DataTypeConversion1_f_0 = (uint32_T)rtb_motohawk_data_read1_mk;
    } else {
      rtb_DataTypeConversion1_f_0 = 0U;
    }
  } else {
    rtb_DataTypeConversion1_f_0 = MAX_uint32_T;
  }

  /* End of DataTypeConversion: '<S171>/Data Type Conversion1' */

  /* Gain: '<S171>/Gain3' incorporates:
   *  Product: '<S171>/Product'
   *  Sum: '<S171>/Add'
   */
  rtb_motohawk_data_read1_mk = (rtb_UnitDelay1_h * (real_T)rtb_AboveHiTarget +
    CRVLAB_ECU_104_B.s178_Merge) * 1000.0;

  /* DataTypeConversion: '<S171>/Data Type Conversion3' */
  if (rtb_motohawk_data_read1_mk < 4.294967296E+9) {
    if (rtb_motohawk_data_read1_mk >= 0.0) {
      u = (uint32_T)rtb_motohawk_data_read1_mk;
    } else {
      u = 0U;
    }
  } else {
    u = MAX_uint32_T;
  }

  /* End of DataTypeConversion: '<S171>/Data Type Conversion3' */

  /* Gain: '<S171>/Gain5' incorporates:
   *  Product: '<S171>/Product'
   *  Sum: '<S171>/Add'
   */
  rtb_motohawk_data_read1_mk = (rtb_Sum5 * (real_T)rtb_AboveHiTarget +
    CRVLAB_ECU_104_B.s179_Merge) * 1000.0;

  /* DataTypeConversion: '<S171>/Data Type Conversion5' */
  if (rtb_motohawk_data_read1_mk < 4.294967296E+9) {
    if (rtb_motohawk_data_read1_mk >= 0.0) {
      rtb_DataTypeConversion5 = (uint32_T)rtb_motohawk_data_read1_mk;
    } else {
      rtb_DataTypeConversion5 = 0U;
    }
  } else {
    rtb_DataTypeConversion5 = MAX_uint32_T;
  }

  /* End of DataTypeConversion: '<S171>/Data Type Conversion5' */

  /* Saturate: '<S171>/Saturation' incorporates:
   *  DataTypeConversion: '<S171>/Data Type Conversion1'
   *  DataTypeConversion: '<S171>/Data Type Conversion3'
   */
  rtb_Saturation_od[0] = rtb_DataTypeConversion1_f_0 <= 2000U ?
    rtb_DataTypeConversion1_f_0 : 2000U;
  rtb_Saturation_od[1] = u <= 2000U ? u : 2000U;
  rtb_Saturation_od[2] = rtb_DataTypeConversion5 <= 2000U ?
    rtb_DataTypeConversion5 : 2000U;

  /* Logic: '<S171>/Logical Operator' incorporates:
   *  Constant: '<S171>/Constant1'
   *  RelationalOperator: '<S171>/Relational Operator'
   *  S-Function (motohawk_sfun_encoder_num_cylinders): '<S171>/motohawk_encoder_num_cylinders'
   */
  rtb_LogicalOperator5 = (rtb_BelowLoTarget && (4 <= ((uint8_T)
    (EncoderNumCylinders_DataStore()))));

  /* If: '<S192>/If' incorporates:
   *  Inport: '<S193>/In1'
   *  Inport: '<S194>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S192>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S192>/override_enable'
   */
  if ((Inj4Enabled_Override_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S192>/NewValue' incorporates:
     *  ActionPort: '<S193>/Action Port'
     */
    CRVLAB_ECU_104_B.s192_Merge = (Inj4Enabled_Override_new_DataStore());

    /* End of Outputs for SubSystem: '<S192>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S192>/OldValue' incorporates:
     *  ActionPort: '<S194>/Action Port'
     */
    CRVLAB_ECU_104_B.s192_Merge = rtb_LogicalOperator5;

    /* End of Outputs for SubSystem: '<S192>/OldValue' */
  }

  /* End of If: '<S192>/If' */

  /* S-Function (motohawk_sfun_data_read): '<S140>/motohawk_data_read1' */
  rtb_motohawk_data_read1_mk = EGR_ADC_DataStore();

  /* DataTypeConversion: '<S140>/Data Type Conversion1' */
  if (rtIsNaN(rtb_motohawk_data_read1_mk) || rtIsInf(rtb_motohawk_data_read1_mk))
  {
    rtb_Add_i_idx = 0.0;
  } else {
    rtb_Add_i_idx = fmod(floor(rtb_motohawk_data_read1_mk), 65536.0);
  }

  rtb_DataTypeConversion1_p = (int16_T)(rtb_Add_i_idx < 0.0 ? (int16_T)-(int16_T)
                                        (uint16_T)-rtb_Add_i_idx : (int16_T)
    (uint16_T)rtb_Add_i_idx);

  /* End of DataTypeConversion: '<S140>/Data Type Conversion1' */
  /* DataTypeConversion: '<S140>/Data Type Conversion3' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S140>/motohawk_calibration4'
   */
  if (rtIsNaNF((EGRFreq_cHz_DataStore())) || rtIsInfF((EGRFreq_cHz_DataStore())))
  {
    rtb_Switch1_b = 0.0F;
  } else {
    rtb_Switch1_b = (real32_T)fmod((real32_T)floor((EGRFreq_cHz_DataStore())),
      4.2949673E+9F);
  }

  rtb_DataTypeConversion3 = rtb_Switch1_b < 0.0F ? (uint32_T)-(int32_T)(uint32_T)
    -rtb_Switch1_b : (uint32_T)rtb_Switch1_b;

  /* End of DataTypeConversion: '<S140>/Data Type Conversion3' */

  /* S-Function (motohawk_sfun_data_read): '<S141>/motohawk_data_read1' */
  rtb_motohawk_data_read1_nr = State_DataStore();

  /* RelationalOperator: '<S213>/Compare' incorporates:
   *  Constant: '<S213>/Constant'
   */
  rtb_Compare_h = (rtb_motohawk_data_read1_nr == 2);

  /* Logic: '<S208>/Logical Operator1' incorporates:
   *  Constant: '<S208>/Constant1'
   *  RelationalOperator: '<S208>/Relational Operator2'
   *  S-Function (motohawk_sfun_data_read): '<S141>/motohawk_data_read7'
   */
  CRVLAB_ECU_104_B.s208_LogicalOperator1 = (((rtb_Compare_h != 0) &&
    (ECT_C_DataStore() < 85.0)));

  /* If: '<S210>/If' incorporates:
   *  Inport: '<S215>/In1'
   *  Inport: '<S216>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S210>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S210>/override_enable'
   */
  if ((Glow_Relay_Override_Absolute_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S210>/NewValue' incorporates:
     *  ActionPort: '<S215>/Action Port'
     */
    CRVLAB_ECU_104_B.s210_Merge = (Glow_Relay_Override_Absolute_new_DataStore());

    /* End of Outputs for SubSystem: '<S210>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S210>/OldValue' incorporates:
     *  ActionPort: '<S216>/Action Port'
     */
    CRVLAB_ECU_104_B.s210_Merge = CRVLAB_ECU_104_B.s208_LogicalOperator1;

    /* End of Outputs for SubSystem: '<S210>/OldValue' */
  }

  /* End of If: '<S210>/If' */

  /* RelationalOperator: '<S214>/Compare' incorporates:
   *  Constant: '<S214>/Constant'
   */
  CRVLAB_ECU_104_B.s214_Compare = ((rtb_motohawk_data_read1_nr > 2));

  /* If: '<S212>/If' incorporates:
   *  Inport: '<S219>/In1'
   *  Inport: '<S220>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S212>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S212>/override_enable'
   */
  if ((StarterCutRelay_Override_Absolute_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S212>/NewValue' incorporates:
     *  ActionPort: '<S219>/Action Port'
     */
    rtb_Compare_h = ((uint8_T)(StarterCutRelay_Override_Absolute_new_DataStore()));

    /* End of Outputs for SubSystem: '<S212>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S212>/OldValue' incorporates:
     *  ActionPort: '<S220>/Action Port'
     */
    rtb_Compare_h = CRVLAB_ECU_104_B.s214_Compare;

    /* End of Outputs for SubSystem: '<S212>/OldValue' */
  }

  /* End of If: '<S212>/If' */

  /* DataTypeConversion: '<S141>/Data Type Conversion1' */
  CRVLAB_ECU_104_B.s141_DataTypeConversion1 = ((rtb_Compare_h != 0));

  /* RelationalOperator: '<S207>/Relational Operator1' incorporates:
   *  Constant: '<S207>/Constant'
   */
  rtb_LogicalOperator5 = (rtb_motohawk_data_read1_nr > 2);

  /* If: '<S211>/If' incorporates:
   *  Inport: '<S217>/In1'
   *  Inport: '<S218>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S211>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S211>/override_enable'
   */
  if ((Gas_Relay_Override_Absolute_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S211>/NewValue' incorporates:
     *  ActionPort: '<S217>/Action Port'
     */
    CRVLAB_ECU_104_B.s211_Merge = (Gas_Relay_Override_Absolute_new_DataStore());

    /* End of Outputs for SubSystem: '<S211>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S211>/OldValue' incorporates:
     *  ActionPort: '<S218>/Action Port'
     */
    CRVLAB_ECU_104_B.s211_Merge = rtb_LogicalOperator5;

    /* End of Outputs for SubSystem: '<S211>/OldValue' */
  }

  /* End of If: '<S211>/If' */

  /* DataTypeConversion: '<S142>/Data Type Conversion1' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S142>/motohawk_calibration'
   */
  if (rtIsNaN((TACHDC_ADC_DataStore())) || rtIsInf((TACHDC_ADC_DataStore()))) {
    rtb_Add_i_idx = 0.0;
  } else {
    rtb_Add_i_idx = fmod(floor((TACHDC_ADC_DataStore())), 65536.0);
  }

  rtb_DataTypeConversion1_c = (int16_T)(rtb_Add_i_idx < 0.0 ? (int16_T)-(int16_T)
                                        (uint16_T)-rtb_Add_i_idx : (int16_T)
    (uint16_T)rtb_Add_i_idx);

  /* End of DataTypeConversion: '<S142>/Data Type Conversion1' */

  /* S-Function (motohawk_sfun_data_read): '<S142>/motohawk_data_read1' */
  rtb_motohawk_data_read1_mk = EngineSpeed_rpm_DataStore();

  /* DataTypeConversion: '<S142>/Data Type Conversion2' incorporates:
   *  Product: '<S142>/Product'
   *  S-Function (motohawk_sfun_calibration): '<S142>/motohawk_calibration1'
   */
  rtb_Add_i_idx = rtb_motohawk_data_read1_mk * (real_T)
    (TACHFrequencyGain_DataStore());
  if (rtIsNaN(rtb_Add_i_idx) || rtIsInf(rtb_Add_i_idx)) {
    rtb_Add_i_idx = 0.0;
  } else {
    rtb_Add_i_idx = fmod(floor(rtb_Add_i_idx), 4.294967296E+9);
  }

  rtb_DataTypeConversion2 = rtb_Add_i_idx < 0.0 ? (uint32_T)-(int32_T)(uint32_T)
    -rtb_Add_i_idx : (uint32_T)rtb_Add_i_idx;

  /* End of DataTypeConversion: '<S142>/Data Type Conversion2' */

  /* RelationalOperator: '<S221>/LT' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S143>/motohawk_calibration1'
   *  S-Function (motohawk_sfun_data_read): '<S143>/motohawk_data_read2'
   */
  rtb_AboveHiTarget = (EngineSpeed_rpm_DataStore() >=
                       (swirloff_high_rpm_DataStore()));

  /* RelationalOperator: '<S221>/LT1' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S143>/motohawk_calibration3'
   *  S-Function (motohawk_sfun_data_read): '<S143>/motohawk_data_read2'
   */
  rtb_BelowLoTarget = (EngineSpeed_rpm_DataStore() <= (swirloff_rpm_DataStore()));

  /* CombinatorialLogic: '<S223>/Combinatorial  Logic' */
  {
    uint_T rowidx= 0;

    /* Compute the truth table row index corresponding to the input */
    rowidx = (rowidx << 1) + (uint_T)(rtb_AboveHiTarget != 0);
    rowidx = (rowidx << 1) + (uint_T)(rtb_BelowLoTarget != 0);

    /* Copy the appropriate row of the table into the block output vector */
    rtb_CombinatorialLogic[0] =
      CRVLAB_ECU_104_ConstP.CombinatorialLogic_table[rowidx];
    rtb_CombinatorialLogic[1] =
      CRVLAB_ECU_104_ConstP.CombinatorialLogic_table[rowidx + 4];
  }

  /* Switch: '<S223>/Switch1' incorporates:
   *  UnitDelay: '<S223>/Unit Delay'
   */
  if (rtb_CombinatorialLogic[1]) {
    rtb_LogicalOperator5 = rtb_CombinatorialLogic[0];
  } else {
    rtb_LogicalOperator5 = CRVLAB_ECU_104_DWork.s223_UnitDelay_DSTATE;
  }

  /* End of Switch: '<S223>/Switch1' */

  /* Switch: '<S221>/Switch2' incorporates:
   *  Logic: '<S221>/Logical Operator'
   *  S-Function (motohawk_sfun_calibration): '<S143>/motohawk_calibration1'
   *  S-Function (motohawk_sfun_calibration): '<S143>/motohawk_calibration3'
   */
  if (!rtb_LogicalOperator5) {
    rtb_motohawk_data_read1_mk = (swirloff_high_rpm_DataStore());
  } else {
    rtb_motohawk_data_read1_mk = (swirloff_rpm_DataStore());
  }

  /* End of Switch: '<S221>/Switch2' */

  /* RelationalOperator: '<S143>/Relational Operator1' incorporates:
   *  S-Function (motohawk_sfun_data_read): '<S143>/motohawk_data_read2'
   */
  rtb_RelationalOperator_d = (EngineSpeed_rpm_DataStore() <=
    rtb_motohawk_data_read1_mk);

  /* If: '<S222>/If' incorporates:
   *  Inport: '<S224>/In1'
   *  Inport: '<S225>/In1'
   *  S-Function (motohawk_sfun_calibration): '<S222>/new_value'
   *  S-Function (motohawk_sfun_calibration): '<S222>/override_enable'
   */
  if ((SwirlOveride_ovr_DataStore())) {
    /* Outputs for IfAction SubSystem: '<S222>/NewValue' incorporates:
     *  ActionPort: '<S224>/Action Port'
     */
    CRVLAB_ECU_104_B.s222_Merge = (SwirlOveride_new_DataStore());

    /* End of Outputs for SubSystem: '<S222>/NewValue' */
  } else {
    /* Outputs for IfAction SubSystem: '<S222>/OldValue' incorporates:
     *  ActionPort: '<S225>/Action Port'
     */
    CRVLAB_ECU_104_B.s222_Merge = rtb_RelationalOperator_d;

    /* End of Outputs for SubSystem: '<S222>/OldValue' */
  }

  /* End of If: '<S222>/If' */

  /* S-Function (motohawk_sfun_data_read): '<S144>/motohawk_data_read1' */
  rtb_motohawk_data_read1_mk = Throttle_ADC_DataStore();

  /* DataTypeConversion: '<S144>/Data Type Conversion1' */
  if (rtIsNaN(rtb_motohawk_data_read1_mk) || rtIsInf(rtb_motohawk_data_read1_mk))
  {
    rtb_Add_i_idx = 0.0;
  } else {
    rtb_Add_i_idx = fmod(floor(rtb_motohawk_data_read1_mk), 65536.0);
  }

  rtb_DataTypeConversion1_pt = (int16_T)(rtb_Add_i_idx < 0.0 ? (int16_T)
    -(int16_T)(uint16_T)-rtb_Add_i_idx : (int16_T)(uint16_T)rtb_Add_i_idx);

  /* End of DataTypeConversion: '<S144>/Data Type Conversion1' */
  /* DataTypeConversion: '<S144>/Data Type Conversion2' incorporates:
   *  S-Function (motohawk_sfun_calibration): '<S144>/motohawk_calibration'
   */
  if (rtIsNaNF((ThrottleFreq_cHz_DataStore())) || rtIsInfF
      ((ThrottleFreq_cHz_DataStore()))) {
    rtb_Switch1_b = 0.0F;
  } else {
    rtb_Switch1_b = (real32_T)fmod((real32_T)floor((ThrottleFreq_cHz_DataStore())),
      4.2949673E+9F);
  }

  rtb_DataTypeConversion2_f = rtb_Switch1_b < 0.0F ? (uint32_T)-(int32_T)
    (uint32_T)-rtb_Switch1_b : (uint32_T)rtb_Switch1_b;

  /* End of DataTypeConversion: '<S144>/Data Type Conversion2' */

  /* Update for UnitDelay: '<S45>/Unit Delay' */
  CRVLAB_ECU_104_DWork.s45_UnitDelay_DSTATE = rtb_Sum2_p;

  /* Update for UnitDelay: '<S46>/Unit Delay' */
  CRVLAB_ECU_104_DWork.s46_UnitDelay_DSTATE = rtb_Sum2_e;

  /* Update for UnitDelay: '<S48>/Unit Delay' */
  CRVLAB_ECU_104_DWork.s48_UnitDelay_DSTATE = rtb_Sum2_h;

  /* Update for UnitDelay: '<S38>/Unit Delay' */
  CRVLAB_ECU_104_DWork.s38_UnitDelay_DSTATE = rtb_Saturation;

  /* Update for UnitDelay: '<S51>/Unit Delay' */
  CRVLAB_ECU_104_DWork.s51_UnitDelay_DSTATE = rtb_Sum2_d;

  /* Update for UnitDelay: '<S86>/Unit Delay' */
  CRVLAB_ECU_104_DWork.s86_UnitDelay_DSTATE = rtb_Sum2_cw;

  /* Update for UnitDelay: '<S98>/Unit Delay1' */
  CRVLAB_ECU_104_DWork.s98_UnitDelay1_DSTATE = rtb_Sum_g;

  /* Update for UnitDelay: '<S98>/Unit Delay' */
  CRVLAB_ECU_104_DWork.s98_UnitDelay_DSTATE = rtb_Sum4;

  /* Update for UnitDelay: '<S99>/Unit Delay1' */
  CRVLAB_ECU_104_DWork.s99_UnitDelay1_DSTATE = rtb_Add4;

  /* Update for UnitDelay: '<S99>/Unit Delay' */
  CRVLAB_ECU_104_DWork.s99_UnitDelay_DSTATE = rtb_Sum4_e;

  /* Update for UnitDelay: '<S94>/Unit Delay1' */
  CRVLAB_ECU_104_DWork.s94_UnitDelay1_DSTATE = rtb_Sum5_i;

  /* Update for UnitDelay: '<S94>/Unit Delay' */
  CRVLAB_ECU_104_DWork.s94_UnitDelay_DSTATE = rtb_Sum4_b;

  /* Update for UnitDelay: '<S95>/Unit Delay1' */
  CRVLAB_ECU_104_DWork.s95_UnitDelay1_DSTATE = rtb_Sum_b;

  /* Update for UnitDelay: '<S95>/Unit Delay' */
  CRVLAB_ECU_104_DWork.s95_UnitDelay_DSTATE = rtb_Sum4_a;

  /* Update for S-Function (motohawk_sfun_pwm): '<S138>/motohawk_pwm2' */

  /* S-Function Block: H1_PWMOutput */
  H1_PWMOutput_PWMOutput_Set(rtb_DataTypeConversion1_fy,
    rtb_DataTypeConversion_l, 0, rtb_DataTypeConversion2_g);

  /* S-Function Block: <S168>/Mux PSP MuxPSP1 */
  {
    extern void MuxPSP1_MuxPSP_HardStart_SoftDuration_Set(uint8_T const state[],
      boolean_T use_state_min, int16_T const start_angle[], boolean_T
      use_start_angle_min, uint32_T const duration[], boolean_T use_dur_min,
      uint32_T const* min_dur);
    MuxPSP1_MuxPSP_HardStart_SoftDuration_Set((&CRVLAB_ECU_104_B.s183_Merge), 1,
      (rtb_TmpSignalConversionAtMuxPSPInport2), 0, (rtb_Saturation_ek), 0, NULL);
  }

  /* S-Function Block: <S169>/Mux PSP MuxPSP2 */
  {
    extern void MuxPSP2_MuxPSP_HardStart_SoftDuration_Set(uint8_T const state[],
      boolean_T use_state_min, int16_T const start_angle[], boolean_T
      use_start_angle_min, uint32_T const duration[], boolean_T use_dur_min,
      uint32_T const* min_dur);
    MuxPSP2_MuxPSP_HardStart_SoftDuration_Set((&CRVLAB_ECU_104_B.s186_Merge), 1,
      (rtb_TmpSignalConversionAtMuxPSPInport2_l), 0, (rtb_Saturation_o), 0, NULL);
  }

  /* S-Function Block: <S170>/Mux PSP MuxPSP3 */
  {
    extern void MuxPSP3_MuxPSP_HardStart_SoftDuration_Set(uint8_T const state[],
      boolean_T use_state_min, int16_T const start_angle[], boolean_T
      use_start_angle_min, uint32_T const duration[], boolean_T use_dur_min,
      uint32_T const* min_dur);
    MuxPSP3_MuxPSP_HardStart_SoftDuration_Set((&CRVLAB_ECU_104_B.s189_Merge), 1,
      (rtb_TmpSignalConversionAtMuxPSPInport2_f), 0, (rtb_Saturation_i), 0, NULL);
  }

  /* S-Function Block: <S171>/Mux PSP MuxPSP4 */
  {
    extern void MuxPSP4_MuxPSP_HardStart_SoftDuration_Set(uint8_T const state[],
      boolean_T use_state_min, int16_T const start_angle[], boolean_T
      use_start_angle_min, uint32_T const duration[], boolean_T use_dur_min,
      uint32_T const* min_dur);
    MuxPSP4_MuxPSP_HardStart_SoftDuration_Set((&CRVLAB_ECU_104_B.s192_Merge), 1,
      (rtb_TmpSignalConversionAtMuxPSPInport2_m), 0, (rtb_Saturation_od), 0,
      NULL);
  }

  /* Update for S-Function (motohawk_sfun_pwm): '<S140>/motohawk_pwm' */

  /* S-Function Block: LSD9_PWMOutput */
  LSD9_PWMOutput_PWMOutput_Set(rtb_DataTypeConversion3,
    rtb_DataTypeConversion1_p, 0, 1);

  /* Update for S-Function (motohawk_sfun_dout): '<S141>/motohawk_dout1' */

  /* S-Function Block: DOut3750p0001 */
  {
    DOut3750p0001_DiscreteOutput_Set(CRVLAB_ECU_104_B.s210_Merge);
  }

  /* Update for S-Function (motohawk_sfun_dout): '<S141>/motohawk_dout2' */

  /* S-Function Block: DOut3751p0001 */
  {
    DOut3751p0001_DiscreteOutput_Set(CRVLAB_ECU_104_B.s141_DataTypeConversion1);
  }

  /* Update for S-Function (motohawk_sfun_dout): '<S141>/motohawk_dout3' */

  /* S-Function Block: DOut3752p0001 */
  {
    DOut3752p0001_DiscreteOutput_Set(CRVLAB_ECU_104_B.s211_Merge);
  }

  /* Update for S-Function (motohawk_sfun_pwm): '<S142>/motohawk_pwm' */

  /* S-Function Block: TACH_PWMOutput */
  TACH_PWMOutput_PWMOutput_Set(rtb_DataTypeConversion2,
    rtb_DataTypeConversion1_c, 0, 1);

  /* Update for S-Function (motohawk_sfun_dout): '<S143>/motohawk_dout' */

  /* S-Function Block: DOut3800p0001 */
  {
    DOut3800p0001_DiscreteOutput_Set(CRVLAB_ECU_104_B.s222_Merge);
  }

  /* Update for UnitDelay: '<S223>/Unit Delay' */
  CRVLAB_ECU_104_DWork.s223_UnitDelay_DSTATE = rtb_LogicalOperator5;

  /* Update for S-Function (motohawk_sfun_pwm): '<S144>/motohawk_pwm' */

  /* S-Function Block: LSD10_PWMOutput */
  LSD10_PWMOutput_PWMOutput_Set(rtb_DataTypeConversion2_f,
    rtb_DataTypeConversion1_pt, 0, 1);
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
