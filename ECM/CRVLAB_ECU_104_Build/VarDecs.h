#ifndef VARDECS_H
#define VARDECS_H

/* Name: VarDecEnum_0 ClassID:ENUMDEC EnumDflt:"Unknown" */
typedef enum {
  VarDecEnum_0_0 = 0,                  /* EnumTxt:"Run" */
  VarDecEnum_0_1 = 1,                  /* EnumTxt:"Reboot" */
} VarDecEnum_0;

/* Name: VarDecEnum_1 ClassID:ENUMDEC EnumDflt:"Unknown" */
typedef enum {
  VarDecEnum_1_0 = 0,                  /* EnumTxt:"Pass-Through" */
  VarDecEnum_1_1 = 1,                  /* EnumTxt:"Override" */
} VarDecEnum_1;

/* Name: VarDecEnum_2 ClassID:ENUMDEC EnumDflt:"Unknown" */
typedef enum {
  VarDecEnum_2_0 = 0,                  /* EnumTxt:"Off" */
  VarDecEnum_2_1 = 1,                  /* EnumTxt:"On" */
} VarDecEnum_2;

/* Name: VarDecEnum_3 ClassID:ENUMDEC EnumDflt:"Unknown" */
typedef enum {
  VarDecEnum_3_0 = 0,                  /* EnumTxt:"Run" */
  VarDecEnum_3_1 = 1,                  /* EnumTxt:"Restore To Defaults" */
} VarDecEnum_3;

/* Name: VarDecEnum_4 ClassID:ENUMDEC EnumDflt:"Unknown" */
typedef enum {
  VarDecEnum_4_0 = 0,                  /* EnumTxt:"Run" */
  VarDecEnum_4_1 = 1,                  /* EnumTxt:"Store To EEPROM" */
} VarDecEnum_4;

/* Name: VarDecEnum_5 ClassID:ENUMDEC EnumDflt:"Unknown" */
typedef enum {
  VarDecEnum_5_0 = 0,                  /* EnumTxt:"Disabled" */
  VarDecEnum_5_1 = 1,                  /* EnumTxt:"Enabled" */
} VarDecEnum_5;

/* Name: VarDecEnum_6 ClassID:ENUMDEC EnumDflt:"Unknown" */
typedef enum {
  VarDecEnum_6_0 = 0,                  /* EnumTxt:"Disable on low ECUP" */
  VarDecEnum_6_1 = 1,                  /* EnumTxt:"Keep alive" */
} VarDecEnum_6;

/* Name: VarDecEnum_7 ClassID:ENUMDEC EnumDflt:"Unknown" */
typedef enum {
  VarDecEnum_7_0 = 0,                  /* EnumTxt:"Rising Edge" */
  VarDecEnum_7_1 = 1,                  /* EnumTxt:"Falling Edge" */
} VarDecEnum_7;

/* Name: VarDecEnum_8 ClassID:ENUMDEC EnumDflt:"Unknown" */
typedef enum {
  VarDecEnum_8_0 = 0,                  /* EnumTxt:"Weak Pullup" */
  VarDecEnum_8_1 = 1,                  /* EnumTxt:"Strong Pullup" */
} VarDecEnum_8;

/* Name: VarDecEnum_9 ClassID:ENUMDEC EnumDflt:"Unknown" */
typedef enum {
  VarDecEnum_9_0 = 0,                  /* EnumTxt:"DieselLock" */
  VarDecEnum_9_1 = 1,                  /* EnumTxt:"IdleLock" */
  VarDecEnum_9_2 = 2,                  /* EnumTxt:"StallLock" */
} VarDecEnum_9;

/* Name: VarDecEnum_10 ClassID:ENUMDEC EnumDflt:"Unknown" */
typedef enum {
  VarDecEnum_10_0 = 0,                 /* EnumTxt:"-" */
} VarDecEnum_10;

/* Name: VarDecEnum_11 ClassID:ENUMDEC EnumDflt:"Unknown" */
typedef enum {
  VarDecEnum_11__1 = -1,               /* EnumTxt:"(None)" */
  VarDecEnum_11_36 = 36,               /* EnumTxt:"EST 1" */
  VarDecEnum_11_37 = 37,               /* EnumTxt:"EST 2" */
  VarDecEnum_11_38 = 38,               /* EnumTxt:"EST 3" */
  VarDecEnum_11_39 = 39,               /* EnumTxt:"EST 4" */
  VarDecEnum_11_40 = 40,               /* EnumTxt:"EST 5" */
  VarDecEnum_11_41 = 41,               /* EnumTxt:"EST 6" */
  VarDecEnum_11_42 = 42,               /* EnumTxt:"EST 7" */
  VarDecEnum_11_43 = 43,               /* EnumTxt:"EST 8" */
  VarDecEnum_11_53 = 53,               /* EnumTxt:"ECUP" */
  VarDecEnum_11_54 = 54,               /* EnumTxt:"DRVP" */
  VarDecEnum_11_55 = 55,               /* EnumTxt:"XDRP" */
  VarDecEnum_11_56 = 56,               /* EnumTxt:"AN1M" */
  VarDecEnum_11_57 = 57,               /* EnumTxt:"AN2M" */
  VarDecEnum_11_58 = 58,               /* EnumTxt:"AN3M" */
  VarDecEnum_11_59 = 59,               /* EnumTxt:"AN4M" */
  VarDecEnum_11_60 = 60,               /* EnumTxt:"AN5M" */
  VarDecEnum_11_61 = 61,               /* EnumTxt:"AN6M" */
  VarDecEnum_11_62 = 62,               /* EnumTxt:"AN7M" */
  VarDecEnum_11_63 = 63,               /* EnumTxt:"AN8M" */
  VarDecEnum_11_64 = 64,               /* EnumTxt:"AN9M" */
  VarDecEnum_11_65 = 65,               /* EnumTxt:"AN10M" */
  VarDecEnum_11_66 = 66,               /* EnumTxt:"AN11M" */
  VarDecEnum_11_67 = 67,               /* EnumTxt:"AN12M" */
  VarDecEnum_11_68 = 68,               /* EnumTxt:"AN13M" */
} VarDecEnum_11;

/* Name: VarDecEnum_12 ClassID:ENUMDEC EnumDflt:"Unknown" */
typedef enum {
  VarDecEnum_12_0 = 0,                 /* EnumTxt:"5V" */
  VarDecEnum_12_1 = 1,                 /* EnumTxt:"2.7V" */
} VarDecEnum_12;

/* Name: VarDecEnum_13 ClassID:ENUMDEC EnumDflt:"Unknown" */
typedef enum {
  VarDecEnum_13__1 = -1,               /* EnumTxt:"(None)" */
  VarDecEnum_13_12 = 12,               /* EnumTxt:"FINJ1" */
  VarDecEnum_13_13 = 13,               /* EnumTxt:"FINJ2" */
  VarDecEnum_13_14 = 14,               /* EnumTxt:"FINJ3" */
  VarDecEnum_13_15 = 15,               /* EnumTxt:"FINJ4" */
  VarDecEnum_13_24 = 24,               /* EnumTxt:"TACH" */
  VarDecEnum_13_29 = 29,               /* EnumTxt:"LSD5" */
  VarDecEnum_13_30 = 30,               /* EnumTxt:"LSD6" */
  VarDecEnum_13_32 = 32,               /* EnumTxt:"LSD8" */
  VarDecEnum_13_33 = 33,               /* EnumTxt:"LSD9" */
  VarDecEnum_13_34 = 34,               /* EnumTxt:"LSD10" */
  VarDecEnum_13_36 = 36,               /* EnumTxt:"EST 1" */
  VarDecEnum_13_37 = 37,               /* EnumTxt:"EST 2" */
  VarDecEnum_13_38 = 38,               /* EnumTxt:"EST 3" */
  VarDecEnum_13_39 = 39,               /* EnumTxt:"EST 4" */
  VarDecEnum_13_40 = 40,               /* EnumTxt:"EST 5" */
  VarDecEnum_13_41 = 41,               /* EnumTxt:"EST 6" */
  VarDecEnum_13_42 = 42,               /* EnumTxt:"EST 7" */
  VarDecEnum_13_43 = 43,               /* EnumTxt:"EST 8" */
  VarDecEnum_13_52 = 52,               /* EnumTxt:"MPRD" */
  VarDecEnum_13_125 = 125,             /* EnumTxt:"LSD11" */
  VarDecEnum_13_134 = 134,             /* EnumTxt:"H1+" */
  VarDecEnum_13_135 = 135,             /* EnumTxt:"H1-" */
} VarDecEnum_13;

#endif
