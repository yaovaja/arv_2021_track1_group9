#ifndef COMMON_INCLUDE_H
#define COMMON_INCLUDE_H

/*---- INCLUDE FILES --------------------------------------------------------------------------------------*/
#include <stdlib.h>
#include <string.h>
#include <typedefn.h>
#include <rtwtypes.h>

/*---- TYPEDEFS -------------------------------------------------------------------------------------------*/

/* index_T used for prelookup index/fraction values */
typedef uint16_T index_T;

#include "MotoCoder.h"

/* fault_T used as an index to reference a Fault */
typedef uint32_T fault_T;

/* DataStorage uses matrix types for each built-in type */
typedef struct {
  uint32_T numCols;
  real_T data[1];
} matrix_real_T;

typedef struct {
  uint32_T numCols;
  real32_T data[1];
} matrix_real32_T;

typedef struct {
  uint32_T numCols;
  int8_T data[1];
} matrix_int8_T;

typedef struct {
  uint32_T numCols;
  uint8_T data[1];
} matrix_uint8_T;

typedef struct {
  uint32_T numCols;
  int16_T data[1];
} matrix_int16_T;

typedef struct {
  uint32_T numCols;
  uint16_T data[1];
} matrix_uint16_T;

typedef struct {
  uint32_T numCols;
  int32_T data[1];
} matrix_int32_T;

typedef struct {
  uint32_T numCols;
  uint32_T data[1];
} matrix_uint32_T;

typedef struct {
  uint32_T numCols;
  boolean_T data[1];
} matrix_boolean_T;

typedef struct {
  uint32_T numCols;
  reference_T data[1];
} matrix_reference_T;

typedef struct {
  uint32_T numCols;
  struct_reference_T data[1];
} matrix_struct_reference_T;

/*---- CUSTOM TYPEDEF CODE ---------------------------------------------------------------------------------*/
#define ECT_CTooHigh_S_DataStore()     (CRVLAB_ECU_104_B.s226_motohawk_fault_status_o1)
#define ECT_CTooHigh_S_DataStore_ELEMS() (1)
#define ECT_CTooHigh_S_DataStore_BYTES() sizeof(CRVLAB_ECU_104_B.s226_motohawk_fault_status_o1)
#define ECT_CTooHigh_S_DataStore_ROWS() (1)
#define ECT_CTooHigh_S_DataStore_COLS() (1)
#define ECT_CTooHigh_A_DataStore()     (CRVLAB_ECU_104_B.s226_motohawk_fault_status_o2)
#define ECT_CTooHigh_A_DataStore_ELEMS() (1)
#define ECT_CTooHigh_A_DataStore_BYTES() sizeof(CRVLAB_ECU_104_B.s226_motohawk_fault_status_o2)
#define ECT_CTooHigh_A_DataStore_ROWS() (1)
#define ECT_CTooHigh_A_DataStore_COLS() (1)
#define ECT_CTooHigh_O_DataStore()     (CRVLAB_ECU_104_B.s226_motohawk_fault_status_o3)
#define ECT_CTooHigh_O_DataStore_ELEMS() (1)
#define ECT_CTooHigh_O_DataStore_BYTES() sizeof(CRVLAB_ECU_104_B.s226_motohawk_fault_status_o3)
#define ECT_CTooHigh_O_DataStore_ROWS() (1)
#define ECT_CTooHigh_O_DataStore_COLS() (1)
#define AC_ADC_DataStore()             (CRVLAB_ECU_104_B.s29_motohawk_ain1)
#define AC_ADC_DataStore_ELEMS()       (1)
#define AC_ADC_DataStore_BYTES()       sizeof(CRVLAB_ECU_104_B.s29_motohawk_ain1)
#define AC_ADC_DataStore_ROWS()        (1)
#define AC_ADC_DataStore_COLS()        (1)
#define Power_ADC_DataStore()          (CRVLAB_ECU_104_B.s31_motohawk_ain1)
#define Power_ADC_DataStore_ELEMS()    (1)
#define Power_ADC_DataStore_BYTES()    sizeof(CRVLAB_ECU_104_B.s31_motohawk_ain1)
#define Power_ADC_DataStore_ROWS()     (1)
#define Power_ADC_DataStore_COLS()     (1)
#define Normal_ADC_DataStore()         (CRVLAB_ECU_104_B.s31_motohawk_ain3)
#define Normal_ADC_DataStore_ELEMS()   (1)
#define Normal_ADC_DataStore_BYTES()   sizeof(CRVLAB_ECU_104_B.s31_motohawk_ain3)
#define Normal_ADC_DataStore_ROWS()    (1)
#define Normal_ADC_DataStore_COLS()    (1)
#define ECT_ADC_DataStore()            (CRVLAB_ECU_104_B.s32_DataTypeConversion1)
#define ECT_ADC_DataStore_ELEMS()      (1)
#define ECT_ADC_DataStore_BYTES()      sizeof(CRVLAB_ECU_104_B.s32_DataTypeConversion1)
#define ECT_ADC_DataStore_ROWS()       (1)
#define ECT_ADC_DataStore_COLS()       (1)
#define EGRBeforeFilt_Pct_DataStore()  (CRVLAB_ECU_104_B.s33_Sum2)
#define EGRBeforeFilt_Pct_DataStore_ELEMS() (1)
#define EGRBeforeFilt_Pct_DataStore_BYTES() sizeof(CRVLAB_ECU_104_B.s33_Sum2)
#define EGRBeforeFilt_Pct_DataStore_ROWS() (1)
#define EGRBeforeFilt_Pct_DataStore_COLS() (1)
#define EGRProbe_ADC_DataStore()       (CRVLAB_ECU_104_B.s33_DataTypeConversion1)
#define EGRProbe_ADC_DataStore_ELEMS() (1)
#define EGRProbe_ADC_DataStore_BYTES() sizeof(CRVLAB_ECU_104_B.s33_DataTypeConversion1)
#define EGRProbe_ADC_DataStore_ROWS()  (1)
#define EGRProbe_ADC_DataStore_COLS()  (1)
#define RPMBeforeFilt_rpm_DataStore()  (CRVLAB_ECU_104_B.s34_RPM)
#define RPMBeforeFilt_rpm_DataStore_ELEMS() (1)
#define RPMBeforeFilt_rpm_DataStore_BYTES() sizeof(CRVLAB_ECU_104_B.s34_RPM)
#define RPMBeforeFilt_rpm_DataStore_ROWS() (1)
#define RPMBeforeFilt_rpm_DataStore_COLS() (1)
#define DRVP_ADC_DataStore()           (CRVLAB_ECU_104_B.s35_motohawk_ain1)
#define DRVP_ADC_DataStore_ELEMS()     (1)
#define DRVP_ADC_DataStore_BYTES()     sizeof(CRVLAB_ECU_104_B.s35_motohawk_ain1)
#define DRVP_ADC_DataStore_ROWS()      (1)
#define DRVP_ADC_DataStore_COLS()      (1)
#define MAP_ADC_raw_DataStore()        (CRVLAB_ECU_104_B.s36_motohawk_ain)
#define MAP_ADC_raw_DataStore_ELEMS()  (1)
#define MAP_ADC_raw_DataStore_BYTES()  sizeof(CRVLAB_ECU_104_B.s36_motohawk_ain)
#define MAP_ADC_raw_DataStore_ROWS()   (1)
#define MAP_ADC_raw_DataStore_COLS()   (1)
#define MAP_ADC_DataStore()            (CRVLAB_ECU_104_B.s36_DataTypeConversion1)
#define MAP_ADC_DataStore_ELEMS()      (1)
#define MAP_ADC_DataStore_BYTES()      sizeof(CRVLAB_ECU_104_B.s36_DataTypeConversion1)
#define MAP_ADC_DataStore_ROWS()       (1)
#define MAP_ADC_DataStore_COLS()       (1)
#define Neutral_ADC_DataStore()        (CRVLAB_ECU_104_B.s37_motohawk_ain1)
#define Neutral_ADC_DataStore_ELEMS()  (1)
#define Neutral_ADC_DataStore_BYTES()  sizeof(CRVLAB_ECU_104_B.s37_motohawk_ain1)
#define Neutral_ADC_DataStore_ROWS()   (1)
#define Neutral_ADC_DataStore_COLS()   (1)
#define Pedal2_ADC_DataStore()         (CRVLAB_ECU_104_B.s38_DataTypeConversion1)
#define Pedal2_ADC_DataStore_ELEMS()   (1)
#define Pedal2_ADC_DataStore_BYTES()   sizeof(CRVLAB_ECU_104_B.s38_DataTypeConversion1)
#define Pedal2_ADC_DataStore_ROWS()    (1)
#define Pedal2_ADC_DataStore_COLS()    (1)
#define Pedal1_ADC_DataStore()         (CRVLAB_ECU_104_B.s38_DataTypeConversion)
#define Pedal1_ADC_DataStore_ELEMS()   (1)
#define Pedal1_ADC_DataStore_BYTES()   sizeof(CRVLAB_ECU_104_B.s38_DataTypeConversion)
#define Pedal1_ADC_DataStore_ROWS()    (1)
#define Pedal1_ADC_DataStore_COLS()    (1)
#define Pedal1_Pct_DataStore()         (CRVLAB_ECU_104_B.s38_motohawk_interpolation_1d)
#define Pedal1_Pct_DataStore_ELEMS()   (1)
#define Pedal1_Pct_DataStore_BYTES()   sizeof(CRVLAB_ECU_104_B.s38_motohawk_interpolation_1d)
#define Pedal1_Pct_DataStore_ROWS()    (1)
#define Pedal1_Pct_DataStore_COLS()    (1)
#define Pedal2_Pct_DataStore()         (CRVLAB_ECU_104_B.s38_motohawk_interpolation_1d1)
#define Pedal2_Pct_DataStore_ELEMS()   (1)
#define Pedal2_Pct_DataStore_BYTES()   sizeof(CRVLAB_ECU_104_B.s38_motohawk_interpolation_1d1)
#define Pedal2_Pct_DataStore_ROWS()    (1)
#define Pedal2_Pct_DataStore_COLS()    (1)
#define RailP_ADC_DataStore()          (CRVLAB_ECU_104_B.s39_motohawk_ain1)
#define RailP_ADC_DataStore_ELEMS()    (1)
#define RailP_ADC_DataStore_BYTES()    sizeof(CRVLAB_ECU_104_B.s39_motohawk_ain1)
#define RailP_ADC_DataStore_ROWS()     (1)
#define RailP_ADC_DataStore_COLS()     (1)
#define RailPBeforeFilt_MPa_DataStore() (CRVLAB_ECU_104_B.s39_Sum1)
#define RailPBeforeFilt_MPa_DataStore_ELEMS() (1)
#define RailPBeforeFilt_MPa_DataStore_BYTES() sizeof(CRVLAB_ECU_104_B.s39_Sum1)
#define RailPBeforeFilt_MPa_DataStore_ROWS() (1)
#define RailPBeforeFilt_MPa_DataStore_COLS() (1)
#define TPS_ADC_DataStore()            (CRVLAB_ECU_104_B.s40_DataTypeConversion1)
#define TPS_ADC_DataStore_ELEMS()      (1)
#define TPS_ADC_DataStore_BYTES()      sizeof(CRVLAB_ECU_104_B.s40_DataTypeConversion1)
#define TPS_ADC_DataStore_ROWS()       (1)
#define TPS_ADC_DataStore_COLS()       (1)
#define CNGEOI_degBTDC_DataStore()     (CRVLAB_ECU_104_B.s57_MultiSwitch1)
#define CNGEOI_degBTDC_DataStore_ELEMS() (1)
#define CNGEOI_degBTDC_DataStore_BYTES() sizeof(CRVLAB_ECU_104_B.s57_MultiSwitch1)
#define CNGEOI_degBTDC_DataStore_ROWS() (1)
#define CNGEOI_degBTDC_DataStore_COLS() (1)
#define CNGFuelingPotenGain_DataStore() (CRVLAB_ECU_104_B.s68_Switch2)
#define CNGFuelingPotenGain_DataStore_ELEMS() (1)
#define CNGFuelingPotenGain_DataStore_BYTES() sizeof(CRVLAB_ECU_104_B.s68_Switch2)
#define CNGFuelingPotenGain_DataStore_ROWS() (1)
#define CNGFuelingPotenGain_DataStore_COLS() (1)
#define EGR_u_kp_DataStore()           (CRVLAB_ECU_104_B.s94_Product2)
#define EGR_u_kp_DataStore_ELEMS()     (1)
#define EGR_u_kp_DataStore_BYTES()     sizeof(CRVLAB_ECU_104_B.s94_Product2)
#define EGR_u_kp_DataStore_ROWS()      (1)
#define EGR_u_kp_DataStore_COLS()      (1)
#define EGR_u_ki_DataStore()           (CRVLAB_ECU_104_B.s94_Product3)
#define EGR_u_ki_DataStore_ELEMS()     (1)
#define EGR_u_ki_DataStore_BYTES()     sizeof(CRVLAB_ECU_104_B.s94_Product3)
#define EGR_u_ki_DataStore_ROWS()      (1)
#define EGR_u_ki_DataStore_COLS()      (1)
#define EGR_u_kd_DataStore()           (CRVLAB_ECU_104_B.s94_Product4)
#define EGR_u_kd_DataStore_ELEMS()     (1)
#define EGR_u_kd_DataStore_BYTES()     sizeof(CRVLAB_ECU_104_B.s94_Product4)
#define EGR_u_kd_DataStore_ROWS()      (1)
#define EGR_u_kd_DataStore_COLS()      (1)
#define EGR_u_DataStore()              (CRVLAB_ECU_104_B.s103_Merge)
#define EGR_u_DataStore_ELEMS()        (1)
#define EGR_u_DataStore_BYTES()        sizeof(CRVLAB_ECU_104_B.s103_Merge)
#define EGR_u_DataStore_ROWS()         (1)
#define EGR_u_DataStore_COLS()         (1)
#define IdleSpeed_u_kp_DataStore()     (CRVLAB_ECU_104_B.s95_Product2)
#define IdleSpeed_u_kp_DataStore_ELEMS() (1)
#define IdleSpeed_u_kp_DataStore_BYTES() sizeof(CRVLAB_ECU_104_B.s95_Product2)
#define IdleSpeed_u_kp_DataStore_ROWS() (1)
#define IdleSpeed_u_kp_DataStore_COLS() (1)
#define IdleSpeed_u_ki_DataStore()     (CRVLAB_ECU_104_B.s95_Product3)
#define IdleSpeed_u_ki_DataStore_ELEMS() (1)
#define IdleSpeed_u_ki_DataStore_BYTES() sizeof(CRVLAB_ECU_104_B.s95_Product3)
#define IdleSpeed_u_ki_DataStore_ROWS() (1)
#define IdleSpeed_u_ki_DataStore_COLS() (1)
#define IdleSpeed_u_kd_DataStore()     (CRVLAB_ECU_104_B.s95_Product4)
#define IdleSpeed_u_kd_DataStore_ELEMS() (1)
#define IdleSpeed_u_kd_DataStore_BYTES() sizeof(CRVLAB_ECU_104_B.s95_Product4)
#define IdleSpeed_u_kd_DataStore_ROWS() (1)
#define IdleSpeed_u_kd_DataStore_COLS() (1)
#define IdleSpeed_u_DataStore()        (CRVLAB_ECU_104_B.s110_Merge)
#define IdleSpeed_u_DataStore_ELEMS()  (1)
#define IdleSpeed_u_DataStore_BYTES()  sizeof(CRVLAB_ECU_104_B.s110_Merge)
#define IdleSpeed_u_DataStore_ROWS()   (1)
#define IdleSpeed_u_DataStore_COLS()   (1)
#define MainFueling_Correction_ms_DataStore() (CRVLAB_ECU_104_B.s96_Switch)
#define MainFueling_Correction_ms_DataStore_ELEMS() (1)
#define MainFueling_Correction_ms_DataStore_BYTES() sizeof(CRVLAB_ECU_104_B.s96_Switch)
#define MainFueling_Correction_ms_DataStore_ROWS() (1)
#define MainFueling_Correction_ms_DataStore_COLS() (1)
#define PilotFueling_Correction_ms_DataStore() (CRVLAB_ECU_104_B.s97_Switch)
#define PilotFueling_Correction_ms_DataStore_ELEMS() (1)
#define PilotFueling_Correction_ms_DataStore_BYTES() sizeof(CRVLAB_ECU_104_B.s97_Switch)
#define PilotFueling_Correction_ms_DataStore_ROWS() (1)
#define PilotFueling_Correction_ms_DataStore_COLS() (1)
#define RailP_u_kp_DataStore()         (CRVLAB_ECU_104_B.s98_Product2)
#define RailP_u_kp_DataStore_ELEMS()   (1)
#define RailP_u_kp_DataStore_BYTES()   sizeof(CRVLAB_ECU_104_B.s98_Product2)
#define RailP_u_kp_DataStore_ROWS()    (1)
#define RailP_u_kp_DataStore_COLS()    (1)
#define RailP_u_ki_DataStore()         (CRVLAB_ECU_104_B.s98_Product3)
#define RailP_u_ki_DataStore_ELEMS()   (1)
#define RailP_u_ki_DataStore_BYTES()   sizeof(CRVLAB_ECU_104_B.s98_Product3)
#define RailP_u_ki_DataStore_ROWS()    (1)
#define RailP_u_ki_DataStore_COLS()    (1)
#define RailP_u_kd_DataStore()         (CRVLAB_ECU_104_B.s98_Product4)
#define RailP_u_kd_DataStore_ELEMS()   (1)
#define RailP_u_kd_DataStore_BYTES()   sizeof(CRVLAB_ECU_104_B.s98_Product4)
#define RailP_u_kd_DataStore_ROWS()    (1)
#define RailP_u_kd_DataStore_COLS()    (1)
#define RailP_u_DataStore()            (CRVLAB_ECU_104_B.s128_Merge)
#define RailP_u_DataStore_ELEMS()      (1)
#define RailP_u_DataStore_BYTES()      sizeof(CRVLAB_ECU_104_B.s128_Merge)
#define RailP_u_DataStore_ROWS()       (1)
#define RailP_u_DataStore_COLS()       (1)
#define Throttle_u_kp_DataStore()      (CRVLAB_ECU_104_B.s99_Product2)
#define Throttle_u_kp_DataStore_ELEMS() (1)
#define Throttle_u_kp_DataStore_BYTES() sizeof(CRVLAB_ECU_104_B.s99_Product2)
#define Throttle_u_kp_DataStore_ROWS() (1)
#define Throttle_u_kp_DataStore_COLS() (1)
#define Throttle_u_ki_DataStore()      (CRVLAB_ECU_104_B.s99_Product3)
#define Throttle_u_ki_DataStore_ELEMS() (1)
#define Throttle_u_ki_DataStore_BYTES() sizeof(CRVLAB_ECU_104_B.s99_Product3)
#define Throttle_u_ki_DataStore_ROWS() (1)
#define Throttle_u_ki_DataStore_COLS() (1)
#define Throttle_u_kd_DataStore()      (CRVLAB_ECU_104_B.s99_Product4)
#define Throttle_u_kd_DataStore_ELEMS() (1)
#define Throttle_u_kd_DataStore_BYTES() sizeof(CRVLAB_ECU_104_B.s99_Product4)
#define Throttle_u_kd_DataStore_ROWS() (1)
#define Throttle_u_kd_DataStore_COLS() (1)
#define Throttle_u_DataStore()         (CRVLAB_ECU_104_B.s134_Merge)
#define Throttle_u_DataStore_ELEMS()   (1)
#define Throttle_u_DataStore_BYTES()   sizeof(CRVLAB_ECU_104_B.s134_Merge)
#define Throttle_u_DataStore_ROWS()    (1)
#define Throttle_u_DataStore_COLS()    (1)
#define CngInjEnabled_DataStore()      (CRVLAB_ECU_104_B.s137_LogicalOperator1)
#define CngInjEnabled_DataStore_ELEMS() (1)
#define CngInjEnabled_DataStore_BYTES() sizeof(CRVLAB_ECU_104_B.s137_LogicalOperator1)
#define CngInjEnabled_DataStore_ROWS() (1)
#define CngInjEnabled_DataStore_COLS() (1)
#define Inj1Enabled_DataStore()        (CRVLAB_ECU_104_B.s183_Merge)
#define Inj1Enabled_DataStore_ELEMS()  (1)
#define Inj1Enabled_DataStore_BYTES()  sizeof(CRVLAB_ECU_104_B.s183_Merge)
#define Inj1Enabled_DataStore_ROWS()   (1)
#define Inj1Enabled_DataStore_COLS()   (1)
#define Inj2Enabled_DataStore()        (CRVLAB_ECU_104_B.s186_Merge)
#define Inj2Enabled_DataStore_ELEMS()  (1)
#define Inj2Enabled_DataStore_BYTES()  sizeof(CRVLAB_ECU_104_B.s186_Merge)
#define Inj2Enabled_DataStore_ROWS()   (1)
#define Inj2Enabled_DataStore_COLS()   (1)
#define Inj3Enabled_DataStore()        (CRVLAB_ECU_104_B.s189_Merge)
#define Inj3Enabled_DataStore_ELEMS()  (1)
#define Inj3Enabled_DataStore_BYTES()  sizeof(CRVLAB_ECU_104_B.s189_Merge)
#define Inj3Enabled_DataStore_ROWS()   (1)
#define Inj3Enabled_DataStore_COLS()   (1)
#define Inj4Enabled_DataStore()        (CRVLAB_ECU_104_B.s192_Merge)
#define Inj4Enabled_DataStore_ELEMS()  (1)
#define Inj4Enabled_DataStore_BYTES()  sizeof(CRVLAB_ECU_104_B.s192_Merge)
#define Inj4Enabled_DataStore_ROWS()   (1)
#define Inj4Enabled_DataStore_COLS()   (1)
#define MainFuelingProbe_ms_DataStore() (CRVLAB_ECU_104_B.s179_Merge)
#define MainFuelingProbe_ms_DataStore_ELEMS() (1)
#define MainFuelingProbe_ms_DataStore_BYTES() sizeof(CRVLAB_ECU_104_B.s179_Merge)
#define MainFuelingProbe_ms_DataStore_ROWS() (1)
#define MainFuelingProbe_ms_DataStore_COLS() (1)
#define PreSOIProbe_degBTDC_DataStore() (CRVLAB_ECU_104_B.s180_Merge)
#define PreSOIProbe_degBTDC_DataStore_ELEMS() (1)
#define PreSOIProbe_degBTDC_DataStore_BYTES() sizeof(CRVLAB_ECU_104_B.s180_Merge)
#define PreSOIProbe_degBTDC_DataStore_ROWS() (1)
#define PreSOIProbe_degBTDC_DataStore_COLS() (1)
#define PilotSOIProbe_degBTDC_DataStore() (CRVLAB_ECU_104_B.s181_Merge)
#define PilotSOIProbe_degBTDC_DataStore_ELEMS() (1)
#define PilotSOIProbe_degBTDC_DataStore_BYTES() sizeof(CRVLAB_ECU_104_B.s181_Merge)
#define PilotSOIProbe_degBTDC_DataStore_ROWS() (1)
#define PilotSOIProbe_degBTDC_DataStore_COLS() (1)
#define MainSOIProbe_degBTDC_DataStore() (CRVLAB_ECU_104_B.s182_Merge)
#define MainSOIProbe_degBTDC_DataStore_ELEMS() (1)
#define MainSOIProbe_degBTDC_DataStore_BYTES() sizeof(CRVLAB_ECU_104_B.s182_Merge)
#define MainSOIProbe_degBTDC_DataStore_ROWS() (1)
#define MainSOIProbe_degBTDC_DataStore_COLS() (1)
#define PreFuelingProbe_ms_DataStore() (CRVLAB_ECU_104_B.s177_Merge)
#define PreFuelingProbe_ms_DataStore_ELEMS() (1)
#define PreFuelingProbe_ms_DataStore_BYTES() sizeof(CRVLAB_ECU_104_B.s177_Merge)
#define PreFuelingProbe_ms_DataStore_ROWS() (1)
#define PreFuelingProbe_ms_DataStore_COLS() (1)
#define PilotFuelingProbe_ms_DataStore() (CRVLAB_ECU_104_B.s178_Merge)
#define PilotFuelingProbe_ms_DataStore_ELEMS() (1)
#define PilotFuelingProbe_ms_DataStore_BYTES() sizeof(CRVLAB_ECU_104_B.s178_Merge)
#define PilotFuelingProbe_ms_DataStore_ROWS() (1)
#define PilotFuelingProbe_ms_DataStore_COLS() (1)
#define GlowRelayOn_DataStore()        (CRVLAB_ECU_104_B.s210_Merge)
#define GlowRelayOn_DataStore_ELEMS()  (1)
#define GlowRelayOn_DataStore_BYTES()  sizeof(CRVLAB_ECU_104_B.s210_Merge)
#define GlowRelayOn_DataStore_ROWS()   (1)
#define GlowRelayOn_DataStore_COLS()   (1)
#define StarterCutOn_DataStore()       (CRVLAB_ECU_104_B.s141_DataTypeConversion1)
#define StarterCutOn_DataStore_ELEMS() (1)
#define StarterCutOn_DataStore_BYTES() sizeof(CRVLAB_ECU_104_B.s141_DataTypeConversion1)
#define StarterCutOn_DataStore_ROWS()  (1)
#define StarterCutOn_DataStore_COLS()  (1)
#define Gas_Relay_Enabled_DataStore()  (CRVLAB_ECU_104_B.s211_Merge)
#define Gas_Relay_Enabled_DataStore_ELEMS() (1)
#define Gas_Relay_Enabled_DataStore_BYTES() sizeof(CRVLAB_ECU_104_B.s211_Merge)
#define Gas_Relay_Enabled_DataStore_ROWS() (1)
#define Gas_Relay_Enabled_DataStore_COLS() (1)
#define GlowPlugRelayOn_DataStore()    (CRVLAB_ECU_104_B.s208_LogicalOperator1)
#define GlowPlugRelayOn_DataStore_ELEMS() (1)
#define GlowPlugRelayOn_DataStore_BYTES() sizeof(CRVLAB_ECU_104_B.s208_LogicalOperator1)
#define GlowPlugRelayOn_DataStore_ROWS() (1)
#define GlowPlugRelayOn_DataStore_COLS() (1)
#define StarterCutRelayOn_DataStore()  (CRVLAB_ECU_104_B.s214_Compare)
#define StarterCutRelayOn_DataStore_ELEMS() (1)
#define StarterCutRelayOn_DataStore_BYTES() sizeof(CRVLAB_ECU_104_B.s214_Compare)
#define StarterCutRelayOn_DataStore_ROWS() (1)
#define StarterCutRelayOn_DataStore_COLS() (1)
#define SwirlOn_DataStore()            (CRVLAB_ECU_104_B.s222_Merge)
#define SwirlOn_DataStore_ELEMS()      (1)
#define SwirlOn_DataStore_BYTES()      sizeof(CRVLAB_ECU_104_B.s222_Merge)
#define SwirlOn_DataStore_ROWS()       (1)
#define SwirlOn_DataStore_COLS()       (1)
#include "Application.h"

/*---- END OF FILE ----------------------------------------------------------------------------------------*/
#endif                                 /* COMMON_INCLUDE_H */
