/*
 * File: CRVLAB_ECU_104_data.c
 *
 * Code generated for Simulink model 'CRVLAB_ECU_104'.
 *
 * Model version                  : 1.1463
 * Simulink Coder version         : 8.0 (R2011a) 09-Mar-2011
 * TLC version                    : 8.0 (Feb  3 2011)
 * C/C++ source code generated on : Fri Sep 21 14:23:19 2018
 *
 * Target selection: motohawk_ert_rtw.tlc
 * Embedded hardware selection: Specified
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "CRVLAB_ECU_104.h"
#include "CRVLAB_ECU_104_private.h"

/* Constant parameters (auto storage) */
const ConstParam_CRVLAB_ECU_104 CRVLAB_ECU_104_ConstP = {
  /* Computed Parameter: CombinatorialLogic_table
   * Referenced by: '<S223>/Combinatorial  Logic'
   */
  { 0, 0, 1, 0, 0, 1, 1, 0 }
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
