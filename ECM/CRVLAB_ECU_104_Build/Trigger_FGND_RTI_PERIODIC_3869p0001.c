/*
 * Trigger_FGND_RTI_PERIODIC_3869p0001.c
 *
 * Code generation for model "CRVLAB_ECU_104.mdl".
 *
 * Model version              : 1.1463
 * Simulink Coder version : 8.0 (R2011a) 09-Mar-2011
 * C source code generated on : Fri Sep 21 14:23:19 2018
 *
 * Target selection: motohawk_ert_rtw.tlc
 * Embedded hardware selection: Specified
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "CRVLAB_ECU_104.h"
#include "CRVLAB_ECU_104_private.h"

void Trigger_FGND_RTI_PERIODIC_3869p0001(void)
{
  {
    CRVLAB_ECU_104_Model();
  }
}
