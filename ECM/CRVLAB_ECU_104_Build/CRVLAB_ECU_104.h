/*
 * File: CRVLAB_ECU_104.h
 *
 * Code generated for Simulink model 'CRVLAB_ECU_104'.
 *
 * Model version                  : 1.1463
 * Simulink Coder version         : 8.0 (R2011a) 09-Mar-2011
 * TLC version                    : 8.0 (Feb  3 2011)
 * C/C++ source code generated on : Fri Sep 21 14:23:19 2018
 *
 * Target selection: motohawk_ert_rtw.tlc
 * Embedded hardware selection: Specified
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_CRVLAB_ECU_104_h_
#define RTW_HEADER_CRVLAB_ECU_104_h_
#ifndef CRVLAB_ECU_104_COMMON_INCLUDES_
# define CRVLAB_ECU_104_COMMON_INCLUDES_
#include <stddef.h>
#include <math.h>
#include <string.h>
#include "rtwtypes.h"
#include "Application.h"
#include "rt_nonfinite.h"
#endif                                 /* CRVLAB_ECU_104_COMMON_INCLUDES_ */

#include "CRVLAB_ECU_104_types.h"

/* Child system includes */
#include "Model.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((void*) 0)
#endif

/* Block signals (auto storage) */
typedef struct {
  real_T s32_DataTypeConversion1;      /* '<S32>/Data Type Conversion1' */
  real_T s33_DataTypeConversion1;      /* '<S33>/Data Type Conversion1' */
  real_T s33_Sum2;                     /* '<S33>/Sum2' */
  real_T s34_RPM;                      /* '<S34>/Data Type Conversion' */
  real_T s36_DataTypeConversion1;      /* '<S36>/Data Type Conversion1' */
  real_T s38_DataTypeConversion1;      /* '<S38>/Data Type Conversion1' */
  real_T s38_motohawk_interpolation_1d1;/* '<S38>/motohawk_interpolation_1d1' */
  real_T s38_DataTypeConversion;       /* '<S38>/Data Type Conversion' */
  real_T s38_motohawk_interpolation_1d;/* '<S38>/motohawk_interpolation_1d' */
  real_T s39_Sum1;                     /* '<S39>/Sum1' */
  real_T s40_DataTypeConversion1;      /* '<S40>/Data Type Conversion1' */
  real_T s68_Switch2;                  /* '<S68>/Switch2' */
  real_T s57_MultiSwitch1;             /* '<S57>/MultiSwitch1' */
  real_T s98_Product2;                 /* '<S98>/Product2' */
  real_T s98_Product3;                 /* '<S98>/Product3' */
  real_T s98_Product4;                 /* '<S98>/Product4' */
  real_T s128_Merge;                   /* '<S128>/Merge' */
  real_T s99_Product2;                 /* '<S99>/Product2' */
  real_T s99_Product3;                 /* '<S99>/Product3' */
  real_T s99_Product4;                 /* '<S99>/Product4' */
  real_T s134_Merge;                   /* '<S134>/Merge' */
  real_T s94_Product2;                 /* '<S94>/Product2' */
  real_T s94_Product3;                 /* '<S94>/Product3' */
  real_T s94_Product4;                 /* '<S94>/Product4' */
  real_T s103_Merge;                   /* '<S103>/Merge' */
  real_T s96_Switch;                   /* '<S96>/Switch' */
  real_T s95_Product2;                 /* '<S95>/Product2' */
  real_T s95_Product3;                 /* '<S95>/Product3' */
  real_T s95_Product4;                 /* '<S95>/Product4' */
  real_T s110_Merge;                   /* '<S110>/Merge' */
  real_T s97_Switch;                   /* '<S97>/Switch' */
  real_T s180_Merge;                   /* '<S180>/Merge' */
  real_T s177_Merge;                   /* '<S177>/Merge' */
  real_T s181_Merge;                   /* '<S181>/Merge' */
  real_T s178_Merge;                   /* '<S178>/Merge' */
  real_T s182_Merge;                   /* '<S182>/Merge' */
  real_T s179_Merge;                   /* '<S179>/Merge' */
  real_T s85_railsp;                   /* '<S66>/Chart' */
  real_T s75_speedsp;                  /* '<S59>/Chart' */
  real_T s53_State;                    /* '<S24>/Chart2' */
  uint16_T s29_motohawk_ain1;          /* '<S29>/motohawk_ain1' */
  uint16_T s31_motohawk_ain1;          /* '<S31>/motohawk_ain1' */
  uint16_T s31_motohawk_ain3;          /* '<S31>/motohawk_ain3' */
  uint16_T s35_motohawk_ain1;          /* '<S35>/motohawk_ain1' */
  uint16_T s36_motohawk_ain;           /* '<S36>/motohawk_ain' */
  uint16_T s37_motohawk_ain1;          /* '<S37>/motohawk_ain1' */
  uint16_T s39_motohawk_ain1;          /* '<S39>/motohawk_ain1' */
  uint8_T s214_Compare;                /* '<S214>/Compare' */
  boolean_T s226_motohawk_fault_status_o1;/* '<S226>/motohawk_fault_status' */
  boolean_T s226_motohawk_fault_status_o2;/* '<S226>/motohawk_fault_status' */
  boolean_T s226_motohawk_fault_status_o3;/* '<S226>/motohawk_fault_status' */
  boolean_T s137_LogicalOperator1;     /* '<S137>/Logical Operator1' */
  boolean_T s183_Merge;                /* '<S183>/Merge' */
  boolean_T s186_Merge;                /* '<S186>/Merge' */
  boolean_T s189_Merge;                /* '<S189>/Merge' */
  boolean_T s192_Merge;                /* '<S192>/Merge' */
  boolean_T s208_LogicalOperator1;     /* '<S208>/Logical Operator1' */
  boolean_T s210_Merge;                /* '<S210>/Merge' */
  boolean_T s141_DataTypeConversion1;  /* '<S141>/Data Type Conversion1' */
  boolean_T s211_Merge;                /* '<S211>/Merge' */
  boolean_T s222_Merge;                /* '<S222>/Merge' */
} BlockIO_CRVLAB_ECU_104;

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  real_T s45_UnitDelay_DSTATE;         /* '<S45>/Unit Delay' */
  real_T s46_UnitDelay_DSTATE;         /* '<S46>/Unit Delay' */
  real_T s48_UnitDelay_DSTATE;         /* '<S48>/Unit Delay' */
  real_T s38_UnitDelay_DSTATE;         /* '<S38>/Unit Delay' */
  real_T s51_UnitDelay_DSTATE;         /* '<S51>/Unit Delay' */
  real_T s86_UnitDelay_DSTATE;         /* '<S86>/Unit Delay' */
  real_T s98_UnitDelay1_DSTATE;        /* '<S98>/Unit Delay1' */
  real_T s98_UnitDelay_DSTATE;         /* '<S98>/Unit Delay' */
  real_T s99_UnitDelay1_DSTATE;        /* '<S99>/Unit Delay1' */
  real_T s99_UnitDelay_DSTATE;         /* '<S99>/Unit Delay' */
  real_T s94_UnitDelay1_DSTATE;        /* '<S94>/Unit Delay1' */
  real_T s94_UnitDelay_DSTATE;         /* '<S94>/Unit Delay' */
  real_T s95_UnitDelay1_DSTATE;        /* '<S95>/Unit Delay1' */
  real_T s95_UnitDelay_DSTATE;         /* '<S95>/Unit Delay' */
  uint32_T s56_motohawk_delta_time_DWORK1;/* '<S56>/motohawk_delta_time' */
  uint32_T s98_motohawk_delta_time_DWORK1;/* '<S98>/motohawk_delta_time' */
  uint32_T s99_motohawk_delta_time_DWORK1;/* '<S99>/motohawk_delta_time' */
  uint32_T s94_motohawk_delta_time_DWORK1;/* '<S94>/motohawk_delta_time' */
  uint32_T s95_motohawk_delta_time_DWORK1;/* '<S95>/motohawk_delta_time' */
  uint32_T s149_motohawk_delta_time_DWORK1;/* '<S149>/motohawk_delta_time' */
  int_T s137_motohawk_injector_IWORK;  /* '<S137>/motohawk_injector' */
  int_T s137_motohawk_injector1_IWORK; /* '<S137>/motohawk_injector1' */
  int_T s137_motohawk_injector2_IWORK; /* '<S137>/motohawk_injector2' */
  int_T s137_motohawk_injector3_IWORK; /* '<S137>/motohawk_injector3' */
  uint16_T s4_UnitDelay_DSTATE;        /* '<S4>/Unit Delay' */
  uint16_T s4_UnitDelay1_DSTATE;       /* '<S4>/Unit Delay1' */
  uint16_T s137_motohawk_injector_DWORK2;/* '<S137>/motohawk_injector' */
  uint16_T s137_motohawk_injector1_DWORK2;/* '<S137>/motohawk_injector1' */
  uint16_T s137_motohawk_injector2_DWORK2;/* '<S137>/motohawk_injector2' */
  uint16_T s137_motohawk_injector3_DWORK2;/* '<S137>/motohawk_injector3' */
  boolean_T s223_UnitDelay_DSTATE;     /* '<S223>/Unit Delay' */
  boolean_T s5_UnitDelay_DSTATE;       /* '<S5>/Unit Delay' */
  boolean_T s15_UnitDelay_DSTATE;      /* '<S15>/Unit Delay' */
  boolean_T s3_UnitDelay5_DSTATE;      /* '<S3>/Unit Delay5' */
  boolean_T s3_UnitDelay2_DSTATE;      /* '<S3>/Unit Delay2' */
  boolean_T s3_UnitDelay_DSTATE;       /* '<S3>/Unit Delay' */
  boolean_T s3_UnitDelay1_DSTATE;      /* '<S3>/Unit Delay1' */
  boolean_T s3_UnitDelay3_DSTATE;      /* '<S3>/Unit Delay3' */
  boolean_T s3_UnitDelay4_DSTATE;      /* '<S3>/Unit Delay4' */
  uint8_T s85_is_active_c3_CRVLAB_ECU_104;/* '<S66>/Chart' */
  uint8_T s85_is_c3_CRVLAB_ECU_104;    /* '<S66>/Chart' */
  uint8_T s75_is_active_c2_CRVLAB_ECU_104;/* '<S59>/Chart' */
  uint8_T s75_is_c2_CRVLAB_ECU_104;    /* '<S59>/Chart' */
  uint8_T s53_is_active_c1_CRVLAB_ECU_104;/* '<S24>/Chart2' */
  uint8_T s53_is_c1_CRVLAB_ECU_104;    /* '<S24>/Chart2' */
  uint8_T s53_is_Run;                  /* '<S24>/Chart2' */
  uint8_T s53_is_Cut;                  /* '<S24>/Chart2' */
  boolean_T s137_motohawk_injector_DWORK1;/* '<S137>/motohawk_injector' */
  boolean_T s137_motohawk_injector1_DWORK1;/* '<S137>/motohawk_injector1' */
  boolean_T s137_motohawk_injector2_DWORK1;/* '<S137>/motohawk_injector2' */
  boolean_T s137_motohawk_injector3_DWORK1;/* '<S137>/motohawk_injector3' */
} D_Work_CRVLAB_ECU_104;

/* Zero-crossing (trigger) state */
typedef struct {
  ZCSigState ShutdownpoweronECU565128_Trig_ZCE;/* '<S3>/Shutdown power on ECU565-128' */
  ZCSigState SaveNVVarsonetickbeforeMPRDoff_Trig_ZCE;/* '<S3>/Save NV Vars one tick before MPRD off' */
  ZCSigState ProcessorReboot_Trig_ZCE; /* '<S3>/Processor Reboot' */
  ZCSigState PostShutdowntwoticksbeforeMPRDoff_Trig_ZCE;/* '<S3>/Post Shutdown two ticks before MPRD off' */
  ZCSigState Clear_Trig_ZCE;           /* '<S7>/Clear' */
  ZCSigState Clear_Trig_ZCE_j;         /* '<S6>/Clear' */
} PrevZCSigStates_CRVLAB_ECU_104;

/* Constant parameters (auto storage) */
typedef struct {
  /* Computed Parameter: CombinatorialLogic_table
   * Referenced by: '<S223>/Combinatorial  Logic'
   */
  boolean_T CombinatorialLogic_table[8];
} ConstParam_CRVLAB_ECU_104;

/* Real-time Model Data Structure */
struct RT_MODEL_CRVLAB_ECU_104 {
  const char_T *errorStatus;
};

/* Block signals (auto storage) */
extern BlockIO_CRVLAB_ECU_104 CRVLAB_ECU_104_B;

/* Block states (auto storage) */
extern D_Work_CRVLAB_ECU_104 CRVLAB_ECU_104_DWork;

/* Constant parameters (auto storage) */
extern const ConstParam_CRVLAB_ECU_104 CRVLAB_ECU_104_ConstP;

/* External data declarations for dependent source files */

/* Zero-crossing (trigger) state */
extern PrevZCSigStates_CRVLAB_ECU_104 CRVLAB_ECU_104_PrevZCSigState;

/* Model entry point functions */
extern void CRVLAB_ECU_104_initialize(boolean_T firstTime);
extern void CRVLAB_ECU_104_step(void);
extern void CRVLAB_ECU_104_terminate(void);

/* Real-time Model object */
extern struct RT_MODEL_CRVLAB_ECU_104 *const CRVLAB_ECU_104_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : CRVLAB_ECU_104
 * '<S1>'   : CRVLAB_ECU_104/Main Power Relay
 * '<S2>'   : CRVLAB_ECU_104/Model
 * '<S3>'   : CRVLAB_ECU_104/Main Power Relay/Background
 * '<S4>'   : CRVLAB_ECU_104/Main Power Relay/Background/Delay
 * '<S5>'   : CRVLAB_ECU_104/Main Power Relay/Background/Display Variable Event Boolean
 * '<S6>'   : CRVLAB_ECU_104/Main Power Relay/Background/Display Variable Event Function-Call
 * '<S7>'   : CRVLAB_ECU_104/Main Power Relay/Background/Display Variable Event Function-Call1
 * '<S8>'   : CRVLAB_ECU_104/Main Power Relay/Background/Post Shutdown two ticks before MPRD off
 * '<S9>'   : CRVLAB_ECU_104/Main Power Relay/Background/Processor Reboot
 * '<S10>'  : CRVLAB_ECU_104/Main Power Relay/Background/Save NV Vars one tick before MPRD off
 * '<S11>'  : CRVLAB_ECU_104/Main Power Relay/Background/Shutdown power on ECU565-128
 * '<S12>'  : CRVLAB_ECU_104/Main Power Relay/Background/motohawk_override_abs
 * '<S13>'  : CRVLAB_ECU_104/Main Power Relay/Background/motohawk_restore_nvmem
 * '<S14>'  : CRVLAB_ECU_104/Main Power Relay/Background/motohawk_store_nvmem
 * '<S15>'  : CRVLAB_ECU_104/Main Power Relay/Background/Delay/MotoHawk S-R Flip-Flop
 * '<S16>'  : CRVLAB_ECU_104/Main Power Relay/Background/Display Variable Event Function-Call/Clear
 * '<S17>'  : CRVLAB_ECU_104/Main Power Relay/Background/Display Variable Event Function-Call1/Clear
 * '<S18>'  : CRVLAB_ECU_104/Main Power Relay/Background/Post Shutdown two ticks before MPRD off/Post Shutdown two ticks before MPRD off
 * '<S19>'  : CRVLAB_ECU_104/Main Power Relay/Background/Save NV Vars one tick before MPRD off/Save NV Vars one tick before MPRD off
 * '<S20>'  : CRVLAB_ECU_104/Main Power Relay/Background/Shutdown power on ECU565-128/Shutdown power on ECU565-128
 * '<S21>'  : CRVLAB_ECU_104/Main Power Relay/Background/motohawk_override_abs/NewValue
 * '<S22>'  : CRVLAB_ECU_104/Main Power Relay/Background/motohawk_override_abs/OldValue
 * '<S23>'  : CRVLAB_ECU_104/Model/1 Sensor
 * '<S24>'  : CRVLAB_ECU_104/Model/2 Stateflow Chart
 * '<S25>'  : CRVLAB_ECU_104/Model/3 Set-point Generator
 * '<S26>'  : CRVLAB_ECU_104/Model/4 Control Algorithm
 * '<S27>'  : CRVLAB_ECU_104/Model/5 Actuator
 * '<S28>'  : CRVLAB_ECU_104/Model/6 Safety
 * '<S29>'  : CRVLAB_ECU_104/Model/1 Sensor/AC_On
 * '<S30>'  : CRVLAB_ECU_104/Model/1 Sensor/CNGP_kPa
 * '<S31>'  : CRVLAB_ECU_104/Model/1 Sensor/DriverInputs
 * '<S32>'  : CRVLAB_ECU_104/Model/1 Sensor/ECT_C
 * '<S33>'  : CRVLAB_ECU_104/Model/1 Sensor/EGR_Pct
 * '<S34>'  : CRVLAB_ECU_104/Model/1 Sensor/EngineSpeed_rpm
 * '<S35>'  : CRVLAB_ECU_104/Model/1 Sensor/KeySw_Volt
 * '<S36>'  : CRVLAB_ECU_104/Model/1 Sensor/MAF_kg_hr
 * '<S37>'  : CRVLAB_ECU_104/Model/1 Sensor/Neutral_On
 * '<S38>'  : CRVLAB_ECU_104/Model/1 Sensor/Pedal_Pct
 * '<S39>'  : CRVLAB_ECU_104/Model/1 Sensor/RailP_MPa1
 * '<S40>'  : CRVLAB_ECU_104/Model/1 Sensor/TPS_Pct
 * '<S41>'  : CRVLAB_ECU_104/Model/1 Sensor/AC_On/Compare To Constant
 * '<S42>'  : CRVLAB_ECU_104/Model/1 Sensor/DriverInputs/Compare To Constant1
 * '<S43>'  : CRVLAB_ECU_104/Model/1 Sensor/DriverInputs/Compare To Constant3
 * '<S44>'  : CRVLAB_ECU_104/Model/1 Sensor/ECT_C/ECT Low
 * '<S45>'  : CRVLAB_ECU_104/Model/1 Sensor/EGR_Pct/Low-pass filter
 * '<S46>'  : CRVLAB_ECU_104/Model/1 Sensor/EngineSpeed_rpm/Low-pass filter
 * '<S47>'  : CRVLAB_ECU_104/Model/1 Sensor/Neutral_On/Compare To Constant
 * '<S48>'  : CRVLAB_ECU_104/Model/1 Sensor/Pedal_Pct/Low-pass filter
 * '<S49>'  : CRVLAB_ECU_104/Model/1 Sensor/Pedal_Pct/Pedal redundancy
 * '<S50>'  : CRVLAB_ECU_104/Model/1 Sensor/Pedal_Pct/Saturation2
 * '<S51>'  : CRVLAB_ECU_104/Model/1 Sensor/RailP_MPa1/Low-pass filter
 * '<S52>'  : CRVLAB_ECU_104/Model/2 Stateflow Chart/CNG Prressure1
 * '<S53>'  : CRVLAB_ECU_104/Model/2 Stateflow Chart/Chart2
 * '<S54>'  : CRVLAB_ECU_104/Model/2 Stateflow Chart/ECT C1
 * '<S55>'  : CRVLAB_ECU_104/Model/2 Stateflow Chart/Saturation2
 * '<S56>'  : CRVLAB_ECU_104/Model/2 Stateflow Chart/Time Since Enabled (With Input)3
 * '<S57>'  : CRVLAB_ECU_104/Model/3 Set-point Generator/CNGSOI_degBTDC CNGFueling_ms
 * '<S58>'  : CRVLAB_ECU_104/Model/3 Set-point Generator/EGRSP_Pct
 * '<S59>'  : CRVLAB_ECU_104/Model/3 Set-point Generator/IdleSpeedSP_rpm
 * '<S60>'  : CRVLAB_ECU_104/Model/3 Set-point Generator/MainFueling_ms
 * '<S61>'  : CRVLAB_ECU_104/Model/3 Set-point Generator/MainSOI_degBTDC
 * '<S62>'  : CRVLAB_ECU_104/Model/3 Set-point Generator/PilotFueling_ms
 * '<S63>'  : CRVLAB_ECU_104/Model/3 Set-point Generator/PilotSOI_degBTDC
 * '<S64>'  : CRVLAB_ECU_104/Model/3 Set-point Generator/PreFueling_ms
 * '<S65>'  : CRVLAB_ECU_104/Model/3 Set-point Generator/PreSOI_degBTDC
 * '<S66>'  : CRVLAB_ECU_104/Model/3 Set-point Generator/RailPSP_MPa
 * '<S67>'  : CRVLAB_ECU_104/Model/3 Set-point Generator/TPSSP_Pct
 * '<S68>'  : CRVLAB_ECU_104/Model/3 Set-point Generator/CNGSOI_degBTDC CNGFueling_ms/PotenGain
 * '<S69>'  : CRVLAB_ECU_104/Model/3 Set-point Generator/CNGSOI_degBTDC CNGFueling_ms/Subsystem
 * '<S70>'  : CRVLAB_ECU_104/Model/3 Set-point Generator/CNGSOI_degBTDC CNGFueling_ms/Subsystem1
 * '<S71>'  : CRVLAB_ECU_104/Model/3 Set-point Generator/EGRSP_Pct/Subsystem1
 * '<S72>'  : CRVLAB_ECU_104/Model/3 Set-point Generator/EGRSP_Pct/motohawk_override_abs7
 * '<S73>'  : CRVLAB_ECU_104/Model/3 Set-point Generator/EGRSP_Pct/motohawk_override_abs7/NewValue
 * '<S74>'  : CRVLAB_ECU_104/Model/3 Set-point Generator/EGRSP_Pct/motohawk_override_abs7/OldValue
 * '<S75>'  : CRVLAB_ECU_104/Model/3 Set-point Generator/IdleSpeedSP_rpm/Chart
 * '<S76>'  : CRVLAB_ECU_104/Model/3 Set-point Generator/MainFueling_ms/MainFueling_ECT_Offset
 * '<S77>'  : CRVLAB_ECU_104/Model/3 Set-point Generator/MainFueling_ms/Subsystem
 * '<S78>'  : CRVLAB_ECU_104/Model/3 Set-point Generator/MainSOI_degBTDC/MainSOI_ECT_Offset
 * '<S79>'  : CRVLAB_ECU_104/Model/3 Set-point Generator/MainSOI_degBTDC/Subsystem1
 * '<S80>'  : CRVLAB_ECU_104/Model/3 Set-point Generator/PilotFueling_ms/Subsystem
 * '<S81>'  : CRVLAB_ECU_104/Model/3 Set-point Generator/PilotSOI_degBTDC/PilotSOI_ECT_Offset
 * '<S82>'  : CRVLAB_ECU_104/Model/3 Set-point Generator/PilotSOI_degBTDC/Subsystem1
 * '<S83>'  : CRVLAB_ECU_104/Model/3 Set-point Generator/PreFueling_ms/Subsystem
 * '<S84>'  : CRVLAB_ECU_104/Model/3 Set-point Generator/PreSOI_degBTDC/Subsystem1
 * '<S85>'  : CRVLAB_ECU_104/Model/3 Set-point Generator/RailPSP_MPa/Chart
 * '<S86>'  : CRVLAB_ECU_104/Model/3 Set-point Generator/RailPSP_MPa/Low-pass filter
 * '<S87>'  : CRVLAB_ECU_104/Model/3 Set-point Generator/RailPSP_MPa/Saturation2
 * '<S88>'  : CRVLAB_ECU_104/Model/3 Set-point Generator/RailPSP_MPa/motohawk_override_abs7
 * '<S89>'  : CRVLAB_ECU_104/Model/3 Set-point Generator/RailPSP_MPa/motohawk_override_abs7/NewValue
 * '<S90>'  : CRVLAB_ECU_104/Model/3 Set-point Generator/RailPSP_MPa/motohawk_override_abs7/OldValue
 * '<S91>'  : CRVLAB_ECU_104/Model/3 Set-point Generator/TPSSP_Pct/motohawk_override_abs7
 * '<S92>'  : CRVLAB_ECU_104/Model/3 Set-point Generator/TPSSP_Pct/motohawk_override_abs7/NewValue
 * '<S93>'  : CRVLAB_ECU_104/Model/3 Set-point Generator/TPSSP_Pct/motohawk_override_abs7/OldValue
 * '<S94>'  : CRVLAB_ECU_104/Model/4 Control Algorithm/EGR Controller
 * '<S95>'  : CRVLAB_ECU_104/Model/4 Control Algorithm/Idle Speed Controller
 * '<S96>'  : CRVLAB_ECU_104/Model/4 Control Algorithm/MainFueling_RailP_Compensate
 * '<S97>'  : CRVLAB_ECU_104/Model/4 Control Algorithm/PilotFueling_RailP_Compensate
 * '<S98>'  : CRVLAB_ECU_104/Model/4 Control Algorithm/RailP Controller
 * '<S99>'  : CRVLAB_ECU_104/Model/4 Control Algorithm/Throttle Controller
 * '<S100>' : CRVLAB_ECU_104/Model/4 Control Algorithm/EGR Controller/Compare To Constant
 * '<S101>' : CRVLAB_ECU_104/Model/4 Control Algorithm/EGR Controller/Saturation1
 * '<S102>' : CRVLAB_ECU_104/Model/4 Control Algorithm/EGR Controller/Saturation2
 * '<S103>' : CRVLAB_ECU_104/Model/4 Control Algorithm/EGR Controller/motohawk_override_abs
 * '<S104>' : CRVLAB_ECU_104/Model/4 Control Algorithm/EGR Controller/motohawk_override_abs/NewValue
 * '<S105>' : CRVLAB_ECU_104/Model/4 Control Algorithm/EGR Controller/motohawk_override_abs/OldValue
 * '<S106>' : CRVLAB_ECU_104/Model/4 Control Algorithm/Idle Speed Controller/Compare To Constant
 * '<S107>' : CRVLAB_ECU_104/Model/4 Control Algorithm/Idle Speed Controller/Compare To Constant1
 * '<S108>' : CRVLAB_ECU_104/Model/4 Control Algorithm/Idle Speed Controller/Saturation1
 * '<S109>' : CRVLAB_ECU_104/Model/4 Control Algorithm/Idle Speed Controller/Saturation2
 * '<S110>' : CRVLAB_ECU_104/Model/4 Control Algorithm/Idle Speed Controller/motohawk_override_abs
 * '<S111>' : CRVLAB_ECU_104/Model/4 Control Algorithm/Idle Speed Controller/motohawk_override_abs/NewValue
 * '<S112>' : CRVLAB_ECU_104/Model/4 Control Algorithm/Idle Speed Controller/motohawk_override_abs/OldValue
 * '<S113>' : CRVLAB_ECU_104/Model/4 Control Algorithm/MainFueling_RailP_Compensate/Saturation1
 * '<S114>' : CRVLAB_ECU_104/Model/4 Control Algorithm/MainFueling_RailP_Compensate/Saturation2
 * '<S115>' : CRVLAB_ECU_104/Model/4 Control Algorithm/MainFueling_RailP_Compensate/Saturation3
 * '<S116>' : CRVLAB_ECU_104/Model/4 Control Algorithm/MainFueling_RailP_Compensate/Saturation4
 * '<S117>' : CRVLAB_ECU_104/Model/4 Control Algorithm/PilotFueling_RailP_Compensate/Saturation1
 * '<S118>' : CRVLAB_ECU_104/Model/4 Control Algorithm/PilotFueling_RailP_Compensate/Saturation2
 * '<S119>' : CRVLAB_ECU_104/Model/4 Control Algorithm/PilotFueling_RailP_Compensate/Saturation3
 * '<S120>' : CRVLAB_ECU_104/Model/4 Control Algorithm/PilotFueling_RailP_Compensate/Saturation4
 * '<S121>' : CRVLAB_ECU_104/Model/4 Control Algorithm/RailP Controller/Compare To Constant
 * '<S122>' : CRVLAB_ECU_104/Model/4 Control Algorithm/RailP Controller/Compare To Constant1
 * '<S123>' : CRVLAB_ECU_104/Model/4 Control Algorithm/RailP Controller/Compare To Constant2
 * '<S124>' : CRVLAB_ECU_104/Model/4 Control Algorithm/RailP Controller/Compare To Constant3
 * '<S125>' : CRVLAB_ECU_104/Model/4 Control Algorithm/RailP Controller/Compare To Constant4
 * '<S126>' : CRVLAB_ECU_104/Model/4 Control Algorithm/RailP Controller/Saturation1
 * '<S127>' : CRVLAB_ECU_104/Model/4 Control Algorithm/RailP Controller/Saturation2
 * '<S128>' : CRVLAB_ECU_104/Model/4 Control Algorithm/RailP Controller/motohawk_override_abs
 * '<S129>' : CRVLAB_ECU_104/Model/4 Control Algorithm/RailP Controller/motohawk_override_abs/NewValue
 * '<S130>' : CRVLAB_ECU_104/Model/4 Control Algorithm/RailP Controller/motohawk_override_abs/OldValue
 * '<S131>' : CRVLAB_ECU_104/Model/4 Control Algorithm/Throttle Controller/Compare To Constant
 * '<S132>' : CRVLAB_ECU_104/Model/4 Control Algorithm/Throttle Controller/Saturation1
 * '<S133>' : CRVLAB_ECU_104/Model/4 Control Algorithm/Throttle Controller/Saturation2
 * '<S134>' : CRVLAB_ECU_104/Model/4 Control Algorithm/Throttle Controller/motohawk_override_abs
 * '<S135>' : CRVLAB_ECU_104/Model/4 Control Algorithm/Throttle Controller/motohawk_override_abs/NewValue
 * '<S136>' : CRVLAB_ECU_104/Model/4 Control Algorithm/Throttle Controller/motohawk_override_abs/OldValue
 * '<S137>' : CRVLAB_ECU_104/Model/5 Actuator/CNG Injectors1
 * '<S138>' : CRVLAB_ECU_104/Model/5 Actuator/Common Rail
 * '<S139>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1
 * '<S140>' : CRVLAB_ECU_104/Model/5 Actuator/EGR
 * '<S141>' : CRVLAB_ECU_104/Model/5 Actuator/Relays
 * '<S142>' : CRVLAB_ECU_104/Model/5 Actuator/SpeedoMeter
 * '<S143>' : CRVLAB_ECU_104/Model/5 Actuator/Swirl
 * '<S144>' : CRVLAB_ECU_104/Model/5 Actuator/Throttle
 * '<S145>' : CRVLAB_ECU_104/Model/5 Actuator/CNG Injectors1/CNG1_Trim
 * '<S146>' : CRVLAB_ECU_104/Model/5 Actuator/CNG Injectors1/CNG2_Trim
 * '<S147>' : CRVLAB_ECU_104/Model/5 Actuator/CNG Injectors1/CNG3_Trim
 * '<S148>' : CRVLAB_ECU_104/Model/5 Actuator/CNG Injectors1/CNG4_Trim
 * '<S149>' : CRVLAB_ECU_104/Model/5 Actuator/CNG Injectors1/Time Since Enabled (With Input)3
 * '<S150>' : CRVLAB_ECU_104/Model/5 Actuator/CNG Injectors1/motohawk_override_abs1
 * '<S151>' : CRVLAB_ECU_104/Model/5 Actuator/CNG Injectors1/motohawk_override_abs2
 * '<S152>' : CRVLAB_ECU_104/Model/5 Actuator/CNG Injectors1/motohawk_override_abs3
 * '<S153>' : CRVLAB_ECU_104/Model/5 Actuator/CNG Injectors1/motohawk_override_abs4
 * '<S154>' : CRVLAB_ECU_104/Model/5 Actuator/CNG Injectors1/motohawk_override_abs5
 * '<S155>' : CRVLAB_ECU_104/Model/5 Actuator/CNG Injectors1/motohawk_override_abs6
 * '<S156>' : CRVLAB_ECU_104/Model/5 Actuator/CNG Injectors1/motohawk_override_abs1/NewValue
 * '<S157>' : CRVLAB_ECU_104/Model/5 Actuator/CNG Injectors1/motohawk_override_abs1/OldValue
 * '<S158>' : CRVLAB_ECU_104/Model/5 Actuator/CNG Injectors1/motohawk_override_abs2/NewValue
 * '<S159>' : CRVLAB_ECU_104/Model/5 Actuator/CNG Injectors1/motohawk_override_abs2/OldValue
 * '<S160>' : CRVLAB_ECU_104/Model/5 Actuator/CNG Injectors1/motohawk_override_abs3/NewValue
 * '<S161>' : CRVLAB_ECU_104/Model/5 Actuator/CNG Injectors1/motohawk_override_abs3/OldValue
 * '<S162>' : CRVLAB_ECU_104/Model/5 Actuator/CNG Injectors1/motohawk_override_abs4/NewValue
 * '<S163>' : CRVLAB_ECU_104/Model/5 Actuator/CNG Injectors1/motohawk_override_abs4/OldValue
 * '<S164>' : CRVLAB_ECU_104/Model/5 Actuator/CNG Injectors1/motohawk_override_abs5/NewValue
 * '<S165>' : CRVLAB_ECU_104/Model/5 Actuator/CNG Injectors1/motohawk_override_abs5/OldValue
 * '<S166>' : CRVLAB_ECU_104/Model/5 Actuator/CNG Injectors1/motohawk_override_abs6/NewValue
 * '<S167>' : CRVLAB_ECU_104/Model/5 Actuator/CNG Injectors1/motohawk_override_abs6/OldValue
 * '<S168>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/3 Pulse (MuxPSP)1
 * '<S169>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/3 Pulse (MuxPSP)2
 * '<S170>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/3 Pulse (MuxPSP)3
 * '<S171>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/3 Pulse (MuxPSP)4
 * '<S172>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/Compare To Constant
 * '<S173>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/Compare To Constant1
 * '<S174>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/Move SOI Far Away
 * '<S175>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/Move SOI Far Away1
 * '<S176>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/Move SOI Far Away2
 * '<S177>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/motohawk_override_abs
 * '<S178>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/motohawk_override_abs1
 * '<S179>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/motohawk_override_abs10
 * '<S180>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/motohawk_override_abs7
 * '<S181>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/motohawk_override_abs8
 * '<S182>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/motohawk_override_abs9
 * '<S183>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/3 Pulse (MuxPSP)1/motohawk_override_abs
 * '<S184>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/3 Pulse (MuxPSP)1/motohawk_override_abs/NewValue
 * '<S185>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/3 Pulse (MuxPSP)1/motohawk_override_abs/OldValue
 * '<S186>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/3 Pulse (MuxPSP)2/motohawk_override_abs
 * '<S187>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/3 Pulse (MuxPSP)2/motohawk_override_abs/NewValue
 * '<S188>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/3 Pulse (MuxPSP)2/motohawk_override_abs/OldValue
 * '<S189>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/3 Pulse (MuxPSP)3/motohawk_override_abs
 * '<S190>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/3 Pulse (MuxPSP)3/motohawk_override_abs/NewValue
 * '<S191>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/3 Pulse (MuxPSP)3/motohawk_override_abs/OldValue
 * '<S192>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/3 Pulse (MuxPSP)4/motohawk_override_abs
 * '<S193>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/3 Pulse (MuxPSP)4/motohawk_override_abs/NewValue
 * '<S194>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/3 Pulse (MuxPSP)4/motohawk_override_abs/OldValue
 * '<S195>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/motohawk_override_abs/NewValue
 * '<S196>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/motohawk_override_abs/OldValue
 * '<S197>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/motohawk_override_abs1/NewValue
 * '<S198>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/motohawk_override_abs1/OldValue
 * '<S199>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/motohawk_override_abs10/NewValue
 * '<S200>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/motohawk_override_abs10/OldValue
 * '<S201>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/motohawk_override_abs7/NewValue
 * '<S202>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/motohawk_override_abs7/OldValue
 * '<S203>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/motohawk_override_abs8/NewValue
 * '<S204>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/motohawk_override_abs8/OldValue
 * '<S205>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/motohawk_override_abs9/NewValue
 * '<S206>' : CRVLAB_ECU_104/Model/5 Actuator/Diesel Injectors1/motohawk_override_abs9/OldValue
 * '<S207>' : CRVLAB_ECU_104/Model/5 Actuator/Relays/Gas Relay Enabled
 * '<S208>' : CRVLAB_ECU_104/Model/5 Actuator/Relays/Glow Relay Enabled
 * '<S209>' : CRVLAB_ECU_104/Model/5 Actuator/Relays/Starter Cut Relay Enabled
 * '<S210>' : CRVLAB_ECU_104/Model/5 Actuator/Relays/motohawk_override_abs1
 * '<S211>' : CRVLAB_ECU_104/Model/5 Actuator/Relays/motohawk_override_abs2
 * '<S212>' : CRVLAB_ECU_104/Model/5 Actuator/Relays/motohawk_override_abs4
 * '<S213>' : CRVLAB_ECU_104/Model/5 Actuator/Relays/Glow Relay Enabled/Cranking
 * '<S214>' : CRVLAB_ECU_104/Model/5 Actuator/Relays/Starter Cut Relay Enabled/After Cranking
 * '<S215>' : CRVLAB_ECU_104/Model/5 Actuator/Relays/motohawk_override_abs1/NewValue
 * '<S216>' : CRVLAB_ECU_104/Model/5 Actuator/Relays/motohawk_override_abs1/OldValue
 * '<S217>' : CRVLAB_ECU_104/Model/5 Actuator/Relays/motohawk_override_abs2/NewValue
 * '<S218>' : CRVLAB_ECU_104/Model/5 Actuator/Relays/motohawk_override_abs2/OldValue
 * '<S219>' : CRVLAB_ECU_104/Model/5 Actuator/Relays/motohawk_override_abs4/NewValue
 * '<S220>' : CRVLAB_ECU_104/Model/5 Actuator/Relays/motohawk_override_abs4/OldValue
 * '<S221>' : CRVLAB_ECU_104/Model/5 Actuator/Swirl/Hysterisis
 * '<S222>' : CRVLAB_ECU_104/Model/5 Actuator/Swirl/motohawk_override_abs
 * '<S223>' : CRVLAB_ECU_104/Model/5 Actuator/Swirl/Hysterisis/Hysteresis
 * '<S224>' : CRVLAB_ECU_104/Model/5 Actuator/Swirl/motohawk_override_abs/NewValue
 * '<S225>' : CRVLAB_ECU_104/Model/5 Actuator/Swirl/motohawk_override_abs/OldValue
 * '<S226>' : CRVLAB_ECU_104/Model/6 Safety/ECT_C Too High
 * '<S227>' : CRVLAB_ECU_104/Model/6 Safety/ECT_C Too High/motohawk_override_abs
 * '<S228>' : CRVLAB_ECU_104/Model/6 Safety/ECT_C Too High/motohawk_override_abs/NewValue
 * '<S229>' : CRVLAB_ECU_104/Model/6 Safety/ECT_C Too High/motohawk_override_abs/OldValue
 */
#endif                                 /* RTW_HEADER_CRVLAB_ECU_104_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
