/*
 * MotoHawk_IO.h
 *
 */

#ifndef MOTOHAWK_IO_H
#define MOTOHAWK_IO_H
#include "CommonInclude.h"

/* S-Function Block: <S3>/motohawk_ain5 Resource: ECUP */
NativeError_S ECUP_AnalogInput_Get(uint16_T *adc, uint16_T *status);
NativeError_S ECUP_AnalogInput_Create(void);

/* S-Function Block: <S3>/motohawk_dout Resource: MPRD */
extern NativeError_S DOut135p001_DiscreteOutput_Set(boolean_T in);
extern NativeError_S DOut135p001_DiscreteOutputPushPull_Set(int8_T in);
extern NativeError_S DOut135p001_DiscreteOutput_Create(void);

/* S-Function Block: <S29>/motohawk_ain1 Resource: AN8M */
NativeError_S AN8M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
NativeError_S AN8M_AnalogInput_Create(void);

/* S-Function Block: <S30>/motohawk_ain5 Resource: AN10M */
NativeError_S AN10M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
NativeError_S AN10M_AnalogInput_Create(void);

/* S-Function Block: <S31>/motohawk_ain1 Resource: AN13M */
NativeError_S AN13M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
NativeError_S AN13M_AnalogInput_Create(void);

/* S-Function Block: <S31>/motohawk_ain3 Resource: AN11M */
NativeError_S AN11M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
NativeError_S AN11M_AnalogInput_Create(void);

/* S-Function Block: <S31>/motohawk_ain4 Resource: AN9M */
NativeError_S AN9M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
NativeError_S AN9M_AnalogInput_Create(void);

/* S-Function Block: <S32>/motohawk_ain Resource: AN5M */
NativeError_S AN5M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
NativeError_S AN5M_AnalogInput_Create(void);

/* S-Function Block: <S33>/motohawk_ain1 Resource: AN7M */
NativeError_S AN7M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
NativeError_S AN7M_AnalogInput_Create(void);

/* S-Function Block: <S35>/motohawk_ain1 Resource: DRVP */
NativeError_S DRVP_AnalogInput_Get(uint16_T *adc, uint16_T *status);
NativeError_S DRVP_AnalogInput_Create(void);

/* S-Function Block: <S36>/motohawk_ain Resource: MAFSensor */
NativeError_S MAFSensor_AnalogInput_Get(uint16_T *adc, uint16_T *status);
NativeError_S MAFSensor_AnalogInput_Create(void);

/* S-Function Block: <S37>/motohawk_ain1 Resource: AN12M */
NativeError_S AN12M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
NativeError_S AN12M_AnalogInput_Create(void);

/* S-Function Block: <S38>/motohawk_ain1 Resource: AN2M */
NativeError_S AN2M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
NativeError_S AN2M_AnalogInput_Create(void);

/* S-Function Block: <S38>/motohawk_ain Resource: AN3M */
NativeError_S AN3M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
NativeError_S AN3M_AnalogInput_Create(void);

/* S-Function Block: <S39>/motohawk_ain1 Resource: AN4M */
NativeError_S AN4M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
NativeError_S AN4M_AnalogInput_Create(void);

/* S-Function Block: <S40>/motohawk_ain1 Resource: AN6M */
NativeError_S AN6M_AnalogInput_Get(uint16_T *adc, uint16_T *status);
NativeError_S AN6M_AnalogInput_Create(void);

/* S-Function Block: <S138>/motohawk_pwm2 Resource: H1_PWMOutput */
void H1_PWMOutput_PWMOutput_Create(void);
void H1_PWMOutput_PWMOutput_Set(uint32_T freq, int16_T duty, boolean_T brake,
  boolean_T enable);
void H1_PWMOutput_PWMOutput_Stop(void);

/* S-Function Block: <S140>/motohawk_pwm Resource: LSD9_PWMOutput */
void LSD9_PWMOutput_PWMOutput_Create(void);
void LSD9_PWMOutput_PWMOutput_Set(uint32_T freq, int16_T duty, boolean_T brake,
  boolean_T enable);
void LSD9_PWMOutput_PWMOutput_Stop(void);

/* S-Function Block: <S141>/motohawk_dout1 Resource: LSD6 */
extern NativeError_S DOut3750p0001_DiscreteOutput_Set(boolean_T in);
extern NativeError_S DOut3750p0001_DiscreteOutputPushPull_Set(int8_T in);
extern NativeError_S DOut3750p0001_DiscreteOutput_Create(void);

/* S-Function Block: <S141>/motohawk_dout2 Resource: LSD5 */
extern NativeError_S DOut3751p0001_DiscreteOutput_Set(boolean_T in);
extern NativeError_S DOut3751p0001_DiscreteOutputPushPull_Set(int8_T in);
extern NativeError_S DOut3751p0001_DiscreteOutput_Create(void);

/* S-Function Block: <S141>/motohawk_dout3 Resource: LSD11 */
extern NativeError_S DOut3752p0001_DiscreteOutput_Set(boolean_T in);
extern NativeError_S DOut3752p0001_DiscreteOutputPushPull_Set(int8_T in);
extern NativeError_S DOut3752p0001_DiscreteOutput_Create(void);

/* S-Function Block: <S142>/motohawk_pwm Resource: TACH_PWMOutput */
void TACH_PWMOutput_PWMOutput_Create(void);
void TACH_PWMOutput_PWMOutput_Set(uint32_T freq, int16_T duty, boolean_T brake,
  boolean_T enable);
void TACH_PWMOutput_PWMOutput_Stop(void);

/* S-Function Block: <S143>/motohawk_dout Resource: LSD8 */
extern NativeError_S DOut3800p0001_DiscreteOutput_Set(boolean_T in);
extern NativeError_S DOut3800p0001_DiscreteOutputPushPull_Set(int8_T in);
extern NativeError_S DOut3800p0001_DiscreteOutput_Create(void);

/* S-Function Block: <S144>/motohawk_pwm Resource: LSD10_PWMOutput */
void LSD10_PWMOutput_PWMOutput_Create(void);
void LSD10_PWMOutput_PWMOutput_Set(uint32_T freq, int16_T duty, boolean_T brake,
  boolean_T enable);
void LSD10_PWMOutput_PWMOutput_Stop(void);
extern void MuxPSP1_MuxPSP_HardStart_SoftDuration_Set(uint8_T const state[],
  boolean_T use_state_min, int16_T const start_angle[], boolean_T
  use_start_angle_min, uint32_T const duration[], boolean_T use_dur_min,
  uint32_T const* min_dur);
extern void MuxPSP1_MuxPSP_Create(void);
extern void MuxPSP2_MuxPSP_HardStart_SoftDuration_Set(uint8_T const state[],
  boolean_T use_state_min, int16_T const start_angle[], boolean_T
  use_start_angle_min, uint32_T const duration[], boolean_T use_dur_min,
  uint32_T const* min_dur);
extern void MuxPSP2_MuxPSP_Create(void);
extern void MuxPSP3_MuxPSP_HardStart_SoftDuration_Set(uint8_T const state[],
  boolean_T use_state_min, int16_T const start_angle[], boolean_T
  use_start_angle_min, uint32_T const duration[], boolean_T use_dur_min,
  uint32_T const* min_dur);
extern void MuxPSP3_MuxPSP_Create(void);
extern void MuxPSP4_MuxPSP_HardStart_SoftDuration_Set(uint8_T const state[],
  boolean_T use_state_min, int16_T const start_angle[], boolean_T
  use_start_angle_min, uint32_T const duration[], boolean_T use_dur_min,
  uint32_T const* min_dur);
extern void MuxPSP4_MuxPSP_Create(void);

#endif
