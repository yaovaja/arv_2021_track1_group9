/*
 * File: CRVLAB_ECU_104_private.h
 *
 * Code generated for Simulink model 'CRVLAB_ECU_104'.
 *
 * Model version                  : 1.1463
 * Simulink Coder version         : 8.0 (R2011a) 09-Mar-2011
 * TLC version                    : 8.0 (Feb  3 2011)
 * C/C++ source code generated on : Fri Sep 21 14:23:19 2018
 *
 * Target selection: motohawk_ert_rtw.tlc
 * Embedded hardware selection: Specified
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_CRVLAB_ECU_104_private_h_
#define RTW_HEADER_CRVLAB_ECU_104_private_h_
#include "rtwtypes.h"
#define CALL_EVENT                     (-1)
#ifndef __RTWTYPES_H__
#error This file requires rtwtypes.h to be included
#else
#ifdef TMWTYPES_PREVIOUSLY_INCLUDED
#error This file requires rtwtypes.h to be included before tmwtypes.h
#else

/* Check for inclusion of an incorrect version of rtwtypes.h */
#ifndef RTWTYPES_ID_C08S16I32L32N32F1
#error This code was generated with a different "rtwtypes.h" than the file included
#endif                                 /* RTWTYPES_ID_C08S16I32L32N32F1 */
#endif                                 /* TMWTYPES_PREVIOUSLY_INCLUDED */
#endif                                 /* __RTWTYPES_H__ */

extern boolean_T GetFaultActionStatus(uint32_T action);
extern boolean_T GetFaultActionStatus(uint32_T action);
extern boolean_T GetFaultActionStatus(uint32_T action);
extern boolean_T IsFaultSuspected(uint32_T fault);
extern boolean_T IsFaultActive(uint32_T fault);
extern boolean_T IsFaultOccurred(uint32_T fault);

#endif                                 /* RTW_HEADER_CRVLAB_ECU_104_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
