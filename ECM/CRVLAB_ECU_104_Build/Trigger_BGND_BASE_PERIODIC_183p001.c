/*
 * Trigger_BGND_BASE_PERIODIC_183p001.c
 *
 * Code generation for model "CRVLAB_ECU_104.mdl".
 *
 * Model version              : 1.1463
 * Simulink Coder version : 8.0 (R2011a) 09-Mar-2011
 * C source code generated on : Fri Sep 21 14:23:19 2018
 *
 * Target selection: motohawk_ert_rtw.tlc
 * Embedded hardware selection: Specified
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "CRVLAB_ECU_104.h"
#include "CRVLAB_ECU_104_private.h"

void Trigger_BGND_BASE_PERIODIC_183p001(void)
{
  {
    /* Output and update for function-call system: '<S1>/Background' */
    {
      /* local block i/o variables */
      uint16_T rtb_motohawk_ain5;
      boolean_T rtb_RelationalOperator;
      boolean_T rtb_Merge;
      boolean_T rtb_LogicalOperator;
      boolean_T rtb_UnitDelay_k;
      uint16_T rtb_to65535_d;
      boolean_T rtb_LogicalOperator_e;
      uint16_T rtb_to65535_j;
      boolean_T rtb_LogicalOperator2_b;
      boolean_T rtb_LogicalOperator_k;
      boolean_T rtb_UnitDelay_a;
      boolean_T rtb_UnitDelay1;
      uint32_T tmp;

      /* Logic: '<S5>/Logical Operator' incorporates:
       *  Logic: '<S5>/Logical Operator1'
       *  S-Function (motohawk_sfun_calibration): '<S5>/motohawk_calibration'
       *  UnitDelay: '<S5>/Unit Delay'
       */
      rtb_LogicalOperator = ((!(ProcessorReboot_DataStore())) &&
        CRVLAB_ECU_104_DWork.s5_UnitDelay_DSTATE);

      /* UnitDelay: '<S15>/Unit Delay' */
      rtb_UnitDelay_k = CRVLAB_ECU_104_DWork.s15_UnitDelay_DSTATE;

      /* S-Function Block: <S3>/motohawk_ain5 Resource: ECUP */
      {
        extern NativeError_S ECUP_AnalogInput_Get(uint16_T *adc, uint16_T
          *status);
        ECUP_AnalogInput_Get(&rtb_motohawk_ain5, NULL);
      }

      /* RelationalOperator: '<S3>/Relational Operator' incorporates:
       *  S-Function (motohawk_sfun_ain): '<S3>/motohawk_ain5'
       *  S-Function (motohawk_sfun_calibration): '<S3>/motohawk_calibration'
       */
      rtb_RelationalOperator = (rtb_motohawk_ain5 >= ((uint16_T)
        (ECUP_Threshold_DataStore())));

      /* Switch: '<S4>/Switch' incorporates:
       *  Constant: '<S4>/Constant1'
       *  Sum: '<S4>/Sum'
       */
      if (rtb_RelationalOperator) {
        /* Sum: '<S4>/Sum' incorporates:
         *  DataTypeConversion: '<S4>/Data Type Conversion'
         *  UnitDelay: '<S4>/Unit Delay'
         */
        tmp = (uint32_T)CRVLAB_ECU_104_DWork.s4_UnitDelay_DSTATE + (uint32_T)
          rtb_RelationalOperator;
        if (tmp > 65535U) {
          tmp = 65535U;
        }

        /* End of Sum: '<S4>/Sum' */
        rtb_to65535_d = (uint16_T)tmp;
      } else {
        rtb_to65535_d = 0U;
      }

      /* End of Switch: '<S4>/Switch' */

      /* Logic: '<S4>/Logical Operator' */
      rtb_LogicalOperator_e = !rtb_RelationalOperator;

      /* Switch: '<S4>/Switch1' incorporates:
       *  Constant: '<S4>/Constant3'
       *  Sum: '<S4>/Sum1'
       */
      if (rtb_LogicalOperator_e) {
        /* Sum: '<S4>/Sum1' incorporates:
         *  UnitDelay: '<S4>/Unit Delay1'
         */
        tmp = (uint32_T)CRVLAB_ECU_104_DWork.s4_UnitDelay1_DSTATE + 1U;
        if (tmp > 65535U) {
          tmp = 65535U;
        }

        /* End of Sum: '<S4>/Sum1' */
        rtb_to65535_j = (uint16_T)tmp;
      } else {
        rtb_to65535_j = 0U;
      }

      /* End of Switch: '<S4>/Switch1' */

      /* Logic: '<S15>/Logical Operator2' incorporates:
       *  Constant: '<S4>/Constant2'
       *  Constant: '<S4>/Constant4'
       *  Logic: '<S15>/Logical Operator'
       *  Logic: '<S15>/Logical Operator1'
       *  Logic: '<S4>/Logical Operator1'
       *  Logic: '<S4>/Logical Operator2'
       *  RelationalOperator: '<S4>/Relational Operator1'
       *  RelationalOperator: '<S4>/Relational Operator2'
       */
      rtb_LogicalOperator2_b = ((rtb_UnitDelay_k || (rtb_RelationalOperator &&
        (rtb_to65535_d >= 2))) && (!(rtb_LogicalOperator_e && (rtb_to65535_j >=
        50))));

      /* Logic: '<S3>/Logical Operator' incorporates:
       *  Logic: '<S3>/Logical Operator1'
       *  Logic: '<S3>/Logical Operator2'
       *  S-Function (motohawk_sfun_data_read): '<S3>/motohawk_data_read'
       */
      rtb_LogicalOperator_k = ((!rtb_LogicalOperator) && (rtb_LogicalOperator2_b
        || MPRD_KeepAlive_DataStore()));

      /* Outputs for Triggered SubSystem: '<S3>/Post Shutdown two ticks before MPRD off' incorporates:
       *  TriggerPort: '<S8>/Trigger'
       */
      if ((!rtb_LogicalOperator_k) &&
          (CRVLAB_ECU_104_PrevZCSigState.PostShutdowntwoticksbeforeMPRDoff_Trig_ZCE
           != ZERO_ZCSIG)) {
        /* Outputs for Function Call SubSystem: '<S8>/Post Shutdown two ticks before MPRD off' */

        /* S-Function Block: <S18>/motohawk_event_call */
        {
          /* post the event */
          PostEvent(SHUTDOWN_EVENT);
        }

        /* End of Outputs for SubSystem: '<S8>/Post Shutdown two ticks before MPRD off' */
      }

      CRVLAB_ECU_104_PrevZCSigState.PostShutdowntwoticksbeforeMPRDoff_Trig_ZCE =
        (uint8_T)(rtb_LogicalOperator_k ? (int32_T)POS_ZCSIG : (int32_T)
                  ZERO_ZCSIG);

      /* End of Outputs for SubSystem: '<S3>/Post Shutdown two ticks before MPRD off' */

      /* UnitDelay: '<S3>/Unit Delay2' */
      rtb_UnitDelay_k = CRVLAB_ECU_104_DWork.s3_UnitDelay2_DSTATE;

      /* Outputs for Enabled and Triggered SubSystem: '<S3>/Processor Reboot' incorporates:
       *  EnablePort: '<S9>/Enable'
       *  TriggerPort: '<S9>/Trigger'
       */
      /* UnitDelay: '<S3>/Unit Delay5' incorporates:
       *  UnitDelay: '<S3>/Unit Delay2'
       */
      if (CRVLAB_ECU_104_DWork.s3_UnitDelay5_DSTATE) {
        /* Outputs for Enabled and Triggered SubSystem: '<S3>/Processor Reboot' incorporates:
         *  EnablePort: '<S9>/Enable'
         *  TriggerPort: '<S9>/Trigger'
         */
        if ((!CRVLAB_ECU_104_DWork.s3_UnitDelay2_DSTATE) &&
            (CRVLAB_ECU_104_PrevZCSigState.ProcessorReboot_Trig_ZCE !=
             ZERO_ZCSIG)) {
          /* S-Function Block: <S9>/Force Module Reset Force Module Reset */
          {
            extern NativeError_S ModuleSupport_Reset(void);
            ModuleSupport_Reset();
          }
        }

        CRVLAB_ECU_104_PrevZCSigState.ProcessorReboot_Trig_ZCE = (uint8_T)
          (rtb_UnitDelay_k ? (int32_T)POS_ZCSIG : (int32_T)ZERO_ZCSIG);
      }

      /* End of UnitDelay: '<S3>/Unit Delay5' */
      CRVLAB_ECU_104_PrevZCSigState.ProcessorReboot_Trig_ZCE = (uint8_T)
        (rtb_UnitDelay_k ? (int32_T)POS_ZCSIG : (int32_T)ZERO_ZCSIG);

      /* End of Outputs for SubSystem: '<S3>/Processor Reboot' */

      /* UnitDelay: '<S3>/Unit Delay' */
      rtb_UnitDelay_a = CRVLAB_ECU_104_DWork.s3_UnitDelay_DSTATE;

      /* Outputs for Triggered SubSystem: '<S3>/Save NV Vars one tick before MPRD off' incorporates:
       *  TriggerPort: '<S10>/Trigger'
       */
      if ((!CRVLAB_ECU_104_DWork.s3_UnitDelay_DSTATE) &&
          (CRVLAB_ECU_104_PrevZCSigState.SaveNVVarsonetickbeforeMPRDoff_Trig_ZCE
           != ZERO_ZCSIG)) {
        /* Outputs for Function Call SubSystem: '<S10>/Save NV Vars one tick before MPRD off' */

        /* S-Function (motohawk_sfun_store_nvmem): '<S19>/motohawk_event_call' */
        {
          extern void NonVolatile_StoreNonVolatile(void);
          NonVolatile_StoreNonVolatile();
        }

        /* End of Outputs for SubSystem: '<S10>/Save NV Vars one tick before MPRD off' */
      }

      /* End of UnitDelay: '<S3>/Unit Delay' */
      CRVLAB_ECU_104_PrevZCSigState.SaveNVVarsonetickbeforeMPRDoff_Trig_ZCE =
        (uint8_T)(rtb_UnitDelay_a ? (int32_T)POS_ZCSIG : (int32_T)ZERO_ZCSIG);

      /* End of Outputs for SubSystem: '<S3>/Save NV Vars one tick before MPRD off' */

      /* UnitDelay: '<S3>/Unit Delay1' */
      rtb_UnitDelay1 = CRVLAB_ECU_104_DWork.s3_UnitDelay1_DSTATE;

      /* Outputs for Triggered SubSystem: '<S3>/Shutdown power on ECU565-128' incorporates:
       *  TriggerPort: '<S11>/Trigger'
       */
      if ((!CRVLAB_ECU_104_DWork.s3_UnitDelay1_DSTATE) &&
          (CRVLAB_ECU_104_PrevZCSigState.ShutdownpoweronECU565128_Trig_ZCE !=
           ZERO_ZCSIG)) {
        /* Outputs for Function Call SubSystem: '<S11>/Shutdown power on ECU565-128' */

        /* S-Function Block: <S20>/motohawk_shutdown_power */
        {
          /* Shut off power on PCM-128.  Other modules will ignore this call */
          SendCloseOSMessage();
        }

        /* End of Outputs for SubSystem: '<S11>/Shutdown power on ECU565-128' */
      }

      /* End of UnitDelay: '<S3>/Unit Delay1' */
      CRVLAB_ECU_104_PrevZCSigState.ShutdownpoweronECU565128_Trig_ZCE = (uint8_T)
        (rtb_UnitDelay1 ? (int32_T)POS_ZCSIG : (int32_T)ZERO_ZCSIG);

      /* End of Outputs for SubSystem: '<S3>/Shutdown power on ECU565-128' */

      /* If: '<S12>/If' incorporates:
       *  Inport: '<S21>/In1'
       *  Inport: '<S22>/In1'
       *  Logic: '<S3>/Logical Operator3'
       *  S-Function (motohawk_sfun_calibration): '<S12>/new_value'
       *  S-Function (motohawk_sfun_calibration): '<S12>/override_enable'
       */
      if ((MPRD_ovr_DataStore())) {
        /* Outputs for IfAction SubSystem: '<S12>/NewValue' incorporates:
         *  ActionPort: '<S21>/Action Port'
         */
        rtb_Merge = (MPRD_new_DataStore());

        /* End of Outputs for SubSystem: '<S12>/NewValue' */
      } else {
        /* Outputs for IfAction SubSystem: '<S12>/OldValue' incorporates:
         *  ActionPort: '<S22>/Action Port'
         */
        rtb_Merge = (rtb_LogicalOperator_k || rtb_UnitDelay_k);

        /* End of Outputs for SubSystem: '<S12>/OldValue' */
      }

      /* End of If: '<S12>/If' */

      /* S-Function (motohawk_sfun_data_write): '<S3>/motohawk_data_write' */
      /* Write to Data Storage as scalar: MPRD */
      {
        MPRD_DataStore() = rtb_Merge;
      }

      /* S-Function (motohawk_sfun_data_write): '<S3>/motohawk_data_write1' */
      /* Write to Data Storage as scalar: KeySw_ADC */
      {
        KeySw_ADC_DataStore() = rtb_motohawk_ain5;
      }

      /* S-Function (motohawk_sfun_data_write): '<S3>/motohawk_data_write2' */
      /* Write to Data Storage as scalar: Key_On */
      {
        Key_On_DataStore() = rtb_RelationalOperator;
      }

      /* UnitDelay: '<S3>/Unit Delay3' */
      rtb_UnitDelay_k = CRVLAB_ECU_104_DWork.s3_UnitDelay3_DSTATE;

      /* UnitDelay: '<S3>/Unit Delay4' */
      rtb_LogicalOperator_e = CRVLAB_ECU_104_DWork.s3_UnitDelay4_DSTATE;

      /* Outputs for Triggered SubSystem: '<S6>/Clear' incorporates:
       *  TriggerPort: '<S16>/Trigger'
       */
      /* S-Function (motohawk_sfun_calibration): '<S6>/motohawk_calibration' */
      if ((!(RestoreNVFactoryDefaults_DataStore())) &&
          (CRVLAB_ECU_104_PrevZCSigState.Clear_Trig_ZCE_j != ZERO_ZCSIG)) {
        /* Outputs for Function Call SubSystem: '<S3>/motohawk_restore_nvmem' */

        /* S-Function (motohawk_sfun_restore_nvmem): '<S13>/motohawk_event_call' */
        /* S-Function Restore Factory Defaults */
        {
          NonVolatile_RestoreDefaultNonVolatile();
          NonVolatile_NonVolatileStatus_Reset();
        }

        /* End of Outputs for SubSystem: '<S3>/motohawk_restore_nvmem' */
      }

      CRVLAB_ECU_104_PrevZCSigState.Clear_Trig_ZCE_j = (uint8_T)
        ((RestoreNVFactoryDefaults_DataStore()) ? (int32_T)POS_ZCSIG : (int32_T)
         ZERO_ZCSIG);

      /* End of S-Function (motohawk_sfun_calibration): '<S6>/motohawk_calibration' */

      /* Outputs for Triggered SubSystem: '<S7>/Clear' incorporates:
       *  TriggerPort: '<S17>/Trigger'
       */
      /* S-Function (motohawk_sfun_calibration): '<S7>/motohawk_calibration' */
      if ((!(StoreNV_DataStore())) &&
          (CRVLAB_ECU_104_PrevZCSigState.Clear_Trig_ZCE != ZERO_ZCSIG)) {
        /* Outputs for Function Call SubSystem: '<S3>/motohawk_store_nvmem' */

        /* S-Function (motohawk_sfun_store_nvmem): '<S14>/motohawk_event_call' */
        {
          extern void NonVolatile_StoreNonVolatile(void);
          NonVolatile_StoreNonVolatile();
        }

        /* End of Outputs for SubSystem: '<S3>/motohawk_store_nvmem' */
      }

      CRVLAB_ECU_104_PrevZCSigState.Clear_Trig_ZCE = (uint8_T)
        ((StoreNV_DataStore()) ? (int32_T)POS_ZCSIG : (int32_T)ZERO_ZCSIG);

      /* End of S-Function (motohawk_sfun_calibration): '<S7>/motohawk_calibration' */

      /* Update for UnitDelay: '<S5>/Unit Delay' incorporates:
       *  S-Function (motohawk_sfun_calibration): '<S5>/motohawk_calibration'
       */
      CRVLAB_ECU_104_DWork.s5_UnitDelay_DSTATE = (ProcessorReboot_DataStore());

      /* Update for UnitDelay: '<S15>/Unit Delay' */
      CRVLAB_ECU_104_DWork.s15_UnitDelay_DSTATE = rtb_LogicalOperator2_b;

      /* Update for UnitDelay: '<S4>/Unit Delay' */
      CRVLAB_ECU_104_DWork.s4_UnitDelay_DSTATE = rtb_to65535_d;

      /* Update for UnitDelay: '<S4>/Unit Delay1' */
      CRVLAB_ECU_104_DWork.s4_UnitDelay1_DSTATE = rtb_to65535_j;

      /* Update for UnitDelay: '<S3>/Unit Delay5' */
      CRVLAB_ECU_104_DWork.s3_UnitDelay5_DSTATE = rtb_LogicalOperator_e;

      /* Update for UnitDelay: '<S3>/Unit Delay2' */
      CRVLAB_ECU_104_DWork.s3_UnitDelay2_DSTATE = rtb_UnitDelay1;

      /* Update for UnitDelay: '<S3>/Unit Delay' */
      CRVLAB_ECU_104_DWork.s3_UnitDelay_DSTATE = rtb_LogicalOperator_k;

      /* Update for UnitDelay: '<S3>/Unit Delay1' */
      CRVLAB_ECU_104_DWork.s3_UnitDelay1_DSTATE = rtb_UnitDelay_a;

      /* Update for S-Function (motohawk_sfun_dout): '<S3>/motohawk_dout' */

      /* S-Function Block: DOut135p001 */
      {
        DOut135p001_DiscreteOutput_Set(rtb_Merge);
      }

      /* Update for UnitDelay: '<S3>/Unit Delay3' */
      CRVLAB_ECU_104_DWork.s3_UnitDelay3_DSTATE = rtb_LogicalOperator;

      /* Update for UnitDelay: '<S3>/Unit Delay4' */
      CRVLAB_ECU_104_DWork.s3_UnitDelay4_DSTATE = rtb_UnitDelay_k;
    }
  }
}
