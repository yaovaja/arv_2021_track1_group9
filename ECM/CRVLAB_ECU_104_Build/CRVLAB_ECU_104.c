/*
 * File: CRVLAB_ECU_104.c
 *
 * Code generated for Simulink model 'CRVLAB_ECU_104'.
 *
 * Model version                  : 1.1463
 * Simulink Coder version         : 8.0 (R2011a) 09-Mar-2011
 * TLC version                    : 8.0 (Feb  3 2011)
 * C/C++ source code generated on : Fri Sep 21 14:23:19 2018
 *
 * Target selection: motohawk_ert_rtw.tlc
 * Embedded hardware selection: Specified
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "CRVLAB_ECU_104.h"
#include "CRVLAB_ECU_104_private.h"

/* Block signals (auto storage) */
BlockIO_CRVLAB_ECU_104 CRVLAB_ECU_104_B;

/* Block states (auto storage) */
D_Work_CRVLAB_ECU_104 CRVLAB_ECU_104_DWork;

/* Previous zero-crossings (trigger) states */
PrevZCSigStates_CRVLAB_ECU_104 CRVLAB_ECU_104_PrevZCSigState;

/* Real-time model */
RT_MODEL_CRVLAB_ECU_104 CRVLAB_ECU_104_M_;
RT_MODEL_CRVLAB_ECU_104 *const CRVLAB_ECU_104_M = &CRVLAB_ECU_104_M_;

/* Model step function */
void CRVLAB_ECU_104_step(void)
{
  /* (no output/update code required) */
}

/* Model initialize function */
void CRVLAB_ECU_104_initialize(boolean_T firstTime)
{
  (void)firstTime;

  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize error status */
  rtmSetErrorStatus(CRVLAB_ECU_104_M, (NULL));

  /* block I/O */
  (void) memset(((void *) &CRVLAB_ECU_104_B), 0,
                sizeof(BlockIO_CRVLAB_ECU_104));

  {
    CRVLAB_ECU_104_B.s32_DataTypeConversion1 = 0.0;
    CRVLAB_ECU_104_B.s33_DataTypeConversion1 = 0.0;
    CRVLAB_ECU_104_B.s33_Sum2 = 0.0;
    CRVLAB_ECU_104_B.s34_RPM = 0.0;
    CRVLAB_ECU_104_B.s36_DataTypeConversion1 = 0.0;
    CRVLAB_ECU_104_B.s38_DataTypeConversion1 = 0.0;
    CRVLAB_ECU_104_B.s38_motohawk_interpolation_1d1 = 0.0;
    CRVLAB_ECU_104_B.s38_DataTypeConversion = 0.0;
    CRVLAB_ECU_104_B.s38_motohawk_interpolation_1d = 0.0;
    CRVLAB_ECU_104_B.s39_Sum1 = 0.0;
    CRVLAB_ECU_104_B.s40_DataTypeConversion1 = 0.0;
    CRVLAB_ECU_104_B.s68_Switch2 = 0.0;
    CRVLAB_ECU_104_B.s57_MultiSwitch1 = 0.0;
    CRVLAB_ECU_104_B.s98_Product2 = 0.0;
    CRVLAB_ECU_104_B.s98_Product3 = 0.0;
    CRVLAB_ECU_104_B.s98_Product4 = 0.0;
    CRVLAB_ECU_104_B.s128_Merge = 0.0;
    CRVLAB_ECU_104_B.s99_Product2 = 0.0;
    CRVLAB_ECU_104_B.s99_Product3 = 0.0;
    CRVLAB_ECU_104_B.s99_Product4 = 0.0;
    CRVLAB_ECU_104_B.s134_Merge = 0.0;
    CRVLAB_ECU_104_B.s94_Product2 = 0.0;
    CRVLAB_ECU_104_B.s94_Product3 = 0.0;
    CRVLAB_ECU_104_B.s94_Product4 = 0.0;
    CRVLAB_ECU_104_B.s103_Merge = 0.0;
    CRVLAB_ECU_104_B.s96_Switch = 0.0;
    CRVLAB_ECU_104_B.s95_Product2 = 0.0;
    CRVLAB_ECU_104_B.s95_Product3 = 0.0;
    CRVLAB_ECU_104_B.s95_Product4 = 0.0;
    CRVLAB_ECU_104_B.s110_Merge = 0.0;
    CRVLAB_ECU_104_B.s97_Switch = 0.0;
    CRVLAB_ECU_104_B.s180_Merge = 0.0;
    CRVLAB_ECU_104_B.s177_Merge = 0.0;
    CRVLAB_ECU_104_B.s181_Merge = 0.0;
    CRVLAB_ECU_104_B.s178_Merge = 0.0;
    CRVLAB_ECU_104_B.s182_Merge = 0.0;
    CRVLAB_ECU_104_B.s179_Merge = 0.0;
    CRVLAB_ECU_104_B.s85_railsp = 0.0;
    CRVLAB_ECU_104_B.s75_speedsp = 0.0;
    CRVLAB_ECU_104_B.s53_State = 0.0;
  }

  /* states (dwork) */
  (void) memset((void *)&CRVLAB_ECU_104_DWork, 0,
                sizeof(D_Work_CRVLAB_ECU_104));
  CRVLAB_ECU_104_DWork.s45_UnitDelay_DSTATE = 0.0;
  CRVLAB_ECU_104_DWork.s46_UnitDelay_DSTATE = 0.0;
  CRVLAB_ECU_104_DWork.s48_UnitDelay_DSTATE = 0.0;
  CRVLAB_ECU_104_DWork.s38_UnitDelay_DSTATE = 0.0;
  CRVLAB_ECU_104_DWork.s51_UnitDelay_DSTATE = 0.0;
  CRVLAB_ECU_104_DWork.s86_UnitDelay_DSTATE = 0.0;
  CRVLAB_ECU_104_DWork.s98_UnitDelay1_DSTATE = 0.0;
  CRVLAB_ECU_104_DWork.s98_UnitDelay_DSTATE = 0.0;
  CRVLAB_ECU_104_DWork.s99_UnitDelay1_DSTATE = 0.0;
  CRVLAB_ECU_104_DWork.s99_UnitDelay_DSTATE = 0.0;
  CRVLAB_ECU_104_DWork.s94_UnitDelay1_DSTATE = 0.0;
  CRVLAB_ECU_104_DWork.s94_UnitDelay_DSTATE = 0.0;
  CRVLAB_ECU_104_DWork.s95_UnitDelay1_DSTATE = 0.0;
  CRVLAB_ECU_104_DWork.s95_UnitDelay_DSTATE = 0.0;

  /* Start for S-Function (motohawk_sfun_trigger): '<Root>/motohawk_trigger' */
  CRVLAB_ECU_104_Model_Start();

  /* Start for function-call system: '<S1>/Background' */

  /* Start for Triggered SubSystem: '<S6>/Clear' incorporates:
   *  Start for S-Function (fcncallgen): '<S16>/Function-Call Generator'
   *  Start for SubSystem: '<S3>/motohawk_restore_nvmem'
   */

  /* Start for Triggered SubSystem: '<S7>/Clear' incorporates:
   *  Start for S-Function (fcncallgen): '<S17>/Function-Call Generator'
   *  Start for SubSystem: '<S3>/motohawk_store_nvmem'
   */
  CRVLAB_ECU_104_PrevZCSigState.ShutdownpoweronECU565128_Trig_ZCE = ZERO_ZCSIG;
  CRVLAB_ECU_104_PrevZCSigState.SaveNVVarsonetickbeforeMPRDoff_Trig_ZCE =
    ZERO_ZCSIG;
  CRVLAB_ECU_104_PrevZCSigState.ProcessorReboot_Trig_ZCE = ZERO_ZCSIG;
  CRVLAB_ECU_104_PrevZCSigState.PostShutdowntwoticksbeforeMPRDoff_Trig_ZCE =
    ZERO_ZCSIG;
  CRVLAB_ECU_104_PrevZCSigState.Clear_Trig_ZCE = ZERO_ZCSIG;
  CRVLAB_ECU_104_PrevZCSigState.Clear_Trig_ZCE_j = ZERO_ZCSIG;

  /* InitializeConditions for S-Function (motohawk_sfun_trigger): '<Root>/motohawk_trigger' */
  CRVLAB_ECU_104_Model_Init();
}

/* Model terminate function */
void CRVLAB_ECU_104_terminate(void)
{
  /* (no terminate code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
