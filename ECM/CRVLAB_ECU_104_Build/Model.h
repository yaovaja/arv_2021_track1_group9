/*
 * File: Model.h
 *
 * Code generated for Simulink model 'CRVLAB_ECU_104'.
 *
 * Model version                  : 1.1463
 * Simulink Coder version         : 8.0 (R2011a) 09-Mar-2011
 * TLC version                    : 8.0 (Feb  3 2011)
 * C/C++ source code generated on : Fri Sep 21 14:23:19 2018
 *
 * Target selection: motohawk_ert_rtw.tlc
 * Embedded hardware selection: Specified
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_Model_h_
#define RTW_HEADER_Model_h_
#ifndef CRVLAB_ECU_104_COMMON_INCLUDES_
# define CRVLAB_ECU_104_COMMON_INCLUDES_
#include <stddef.h>
#include <math.h>
#include <string.h>
#include "rtwtypes.h"
#include "Application.h"
#include "rt_nonfinite.h"
#endif                                 /* CRVLAB_ECU_104_COMMON_INCLUDES_ */

#include "CRVLAB_ECU_104_types.h"

extern void CRVLAB_ECU_104_Model_Init(void);
extern void CRVLAB_ECU_104_Model_Start(void);
extern void CRVLAB_ECU_104_Model(void);

#endif                                 /* RTW_HEADER_Model_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
