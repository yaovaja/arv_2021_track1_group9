/*
 * File: CRVLAB_ECU_104_types.h
 *
 * Code generated for Simulink model 'CRVLAB_ECU_104'.
 *
 * Model version                  : 1.1463
 * Simulink Coder version         : 8.0 (R2011a) 09-Mar-2011
 * TLC version                    : 8.0 (Feb  3 2011)
 * C/C++ source code generated on : Fri Sep 21 14:23:19 2018
 *
 * Target selection: motohawk_ert_rtw.tlc
 * Embedded hardware selection: Specified
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_CRVLAB_ECU_104_types_h_
#define RTW_HEADER_CRVLAB_ECU_104_types_h_

/* Forward declaration for rtModel */
typedef struct RT_MODEL_CRVLAB_ECU_104 RT_MODEL_CRVLAB_ECU_104;

#endif                                 /* RTW_HEADER_CRVLAB_ECU_104_types_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
