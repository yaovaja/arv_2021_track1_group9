%Gas Temperature 
GT_ADC = [...
93.18 235.52 512 878.59 1007.62 1024 1228.8
];
GT_Celcius = [...
120 80 40 0 -40 -40 -40
];

%Fuel Temperature 
FT_ADC = [...
170.67 241.13 384 606.04 727.62 872.3 971.49
];
FT_Celcius = [...
100 80 60 40 20 0 -20
];

%Pre Compressor Temperature 
PCT_ADC = [...
170.67 241.13 384 606.04 727.62 872.3 971.49
];
PCT_Celcius = [...
100 80 60 40 20 0 -20
];

%Air Intake Manifold Temperature
Tint_ADC = [...
170.67 241.13 384 606.04 727.62 872.3 971.49
];
Tint_Celcius = [...
100 80 60 40 20 0 -20
];


